<?php

/**
 *  Class name : Auth
 *  Create date : 18-07-2018,
 *  author : Corner stone solution
 *  Description : this class used for set cookie, check authentication, and set session
 *
 */

namespace Admin\Auth;

include APPPATH . 'Classes/finance_planner/admin.php';
include APPPATH . 'Classes/finance_planner/jwt_helper.php';

use AdminClass;

class Auth extends AdminClass\admin {

    public function __construct() {
        $adminClass = new AdminClass\admin();
    }

    private $ocs_token;

    /*
     * Check admin is logged in
     */

    public function getOcsToken() {
        return $this->ocs_token;
    }

    public function setOcsToken($token) {
        $this->ocs_token = $token;
    }

    /*
     * check username and password authentication
     */

    public function check_auth() {
        $CI = & get_instance();
        $Obj_JWT = new \JWT();

        $where = array('username' => $this->getUsername(), 'archive' => 0);
        // using query check username
        $result = $CI->basic_model->get_row('admin', $column = array('id', 'concat(firstname," ",lastname) as full_name', 'password', 'gender', 'status'), $where);
        if (!empty($result)) {
            $checkAccess = false;
            // check password using PASSWORD_BCRYPT method
            if (password_verify($this->getPassword(), $result->password)) {

                // check user active or not
                if ($result->status) {
                    //set token
                    $this->setAdminid($result->id);
                    // if admin id = 1 mean its super admin then access to this portal
                    if ($this->getAdminId() == 1) {
                        $checkAccess = true;
                    } else {
                        $checkAccess = $CI->admin_model->get_admin_based_roles($this->getAdminId(),$result);
                    }

                    if($checkAccess){
                      $token = array(DATE_TIME . $result->id);
                      $JWT_Token = $Obj_JWT->encode($token, JWT_SECRET_KEY);

                      $this->setOcsToken($JWT_Token);
                      $this->setAdminLogin();
                      $response = array('token' => $JWT_Token, 'fullname' => $result->full_name, 'status' => true, 'success' => system_msgs('success_login'));
                    } else {
                        $response = array('status' => false, 'error' => system_msgs('dont_have_portal_access'));
                    }
                } else {
                    $response = array('status' => false, 'error' => system_msgs('account_not_active'));
                }
            } else {
                $response = array('status' => false, 'error' => system_msgs('wrong_username_password'));
            }
        } else {
            $response = array('status' => false, 'error' => system_msgs('wrong_username_password'));
        }

        return $response;
    }

    /*
     *  here verify reset password token
     */

    public function verify_token() {
        $CI = & get_instance();
        $where = array('id' => $this->getAdminid(), 'token' => $this->getOcsToken());
        $result = $CI->basic_model->get_record_where('admin', array('firstname', 'lastname'), $where);
        return $result;
    }

    /*
     * reset password of admin
     */

    public function reset_password() {
        $CI = & get_instance();
        $encry_password = password_hash($this->getPassword(), PASSWORD_BCRYPT);

        $userData = array('password' => $encry_password, 'token' => '');
        $result = $CI->basic_model->update_records('admin', $userData, $where = array('id' => $this->getAdminid()));
        return $result;
    }

    /*
     * here insert entry of login
     */

    public function setAdminLogin() {
        $CI = & get_instance();

        $response = $CI->basic_model->get_row('admin_login', $columns = array('updated', 'adminId', 'token'), $where = array('adminId' => $this->getAdminid()));
        if (!empty($response)) {
            $CI->basic_model->update_records('admin_login', $columns = array('updated' => DATE_TIME, 'pin' => '', 'token' => $this->ocs_token, 'ip_address' => get_client_ip_server()), $where = array('adminId' => $this->getAdminid()));
        } else {
            $CI->basic_model->insert_records('admin_login', $data = array('token' => $this->ocs_token, 'adminId' => $this->getAdminid(), 'updated' => DATE_TIME, 'pin' => '', 'ip_address' => get_client_ip_server()), $multiple = FALSE);
        }
    }

    /*
     * unset user login token in db
     */

    public function unsetAdminLogin($token = false) {
        $CI = & get_instance();

        // check optional paramter and private ocs_token
        $token = (!empty($token)) ? $token : $this->ocs_token;
        $where = array('token' => $token);
        $tokenDetails = $CI->basic_model->get_row('admin_login', array('id','adminId'),$where);
        if($tokenDetails) {
            $wherePinRemove = array('token_id' => $tokenDetails->id, 'adminId' => $tokenDetails->adminId);
            $CI->basic_model->delete_records('admin_pin_token', $wherePinRemove);
        }
        $CI->basic_model->delete_records('admin_login', $where);
    }

    public function checkCurrentPin() {
        $CI = & get_instance();

        $result = $CI->basic_model->get_row('admin', $column = array('pin'), $where = array('id' => $this->getAdminid()));
        if (!empty($result->pin)) {
            if (password_verify($this->getPin(), $result->pin)) {

                return $result->pin;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function verfiy_pin() {
        $Obj_JWT = new \JWT();
        $CI = & get_instance();
        $tableWithoutPrefix = 'admin_pin_token';

        $pin = $this->checkCurrentPin();
        $pinType = $this->getPinType();
        $ocsTokenDetail = $this->checkAuthAdminLoginToken();

        if (!empty($pin) && !empty($pinType) && !empty($ocsTokenDetail)) {
            $token = array(DATE_TIME . $pin.$pinType);
            $JWT_Token = $Obj_JWT->encode($token, JWT_SECRET_KEY);
            $ocsTokenId = $ocsTokenDetail->id;

            //check  pintype get rows exits for this user
            $row_exists = $this->pinTokenTypeRowExists(array('token_type' => $pinType, 'token_id' => $ocsTokenId));
            if(!empty($row_exists)) {
                $where = array('adminId' => $this->getAdminid(), 'token_type' => $pinType, 'token_id' => $ocsTokenId, 'id' => $row_exists->id);
                $CI->basic_model->update_records($tableWithoutPrefix, array('pin' => $JWT_Token, 'updated' => DATE_TIME), $where);
            }else{
                $CI->basic_model->insert_records($tableWithoutPrefix,  array('pin' => $JWT_Token, 'adminId' => $this->getAdminid(), 'token_id' => $ocsTokenId , 'token_type' => $pinType, 'created' => DATE_TIME, 'updated' => DATE_TIME, 'ip_address' => get_client_ip_server()), $multiple = FALSE);
            }

            // update token
            $response = array('token' => $JWT_Token, 'status' => true, 'success' => system_msgs('token_verfied'));
        } else {
            $response = array('status' => false, 'error' => system_msgs('encorrect_pin'));
        }

        return $response;
    }

    public function verifyCurrentPassword() {
        $CI = & get_instance();

        $where = array('id' => $this->getAdminid());
        $result = $CI->basic_model->get_row('admin', $column = array('id', 'password'), $where);

        if (!empty($result)) {
            // check password using PASSWORD_BCRYPT method
            if (password_verify($this->getPassword(), $result->password)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function checkAuthToken() {
        $CI = & get_instance();

        $where = array('id' => $this->getAdminid(), 'token' => $this->getToken());
        return $result = $CI->basic_model->get_row('admin', $column = array('id', 'token'), $where);
    }

    public function checkAuthAdminLoginToken($extaColumn = array()) {
        $CI = & get_instance();
        $column = array('id', 'token');
        $column = !empty($extaColumn) && is_array($extaColumn) ? array_merge($column, $extaColumn) :  $column;
        $where = array('adminId' => $this->getAdminid(), 'token' => $this->getToken());

        return $result = $CI->basic_model->get_row('admin_login', $column, $where);
    }

    public function pinTokenTypeRowExists($dataArr = array('token_type'=>0), $extaColumn = array()) {
        $CI = & get_instance();
        $tableWithoutPrefix = 'admin_pin_token';
        $adminId =  $this->getAdminid();
        $tokenType = isset($dataArr['token_type']) ? (int) $dataArr['token_type'] : 0;
        $tokenId = isset($dataArr['token_id']) ? (int) $dataArr['token_id'] : 0;
        $column = array('id', 'pin');
        $column = !empty($extaColumn) && is_array($extaColumn) ? array_merge($column, $extaColumn) :  $column;
        $where = array('adminId' => $adminId, 'token_type' => $tokenType, 'token_id' => $tokenId);

        return $result = $CI->basic_model->get_row($tableWithoutPrefix, $column, $where);
    }

}
