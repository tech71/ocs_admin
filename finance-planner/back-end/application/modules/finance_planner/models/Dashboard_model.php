<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function __construct() {
// Call the CI_Model constructor
        parent::__construct();
    }

    public function get_counts_bystatus($where){
      $table_name='finance_invoice';
      $this->db->select('count(*) as count');
      $this->db->where($where);
      $result = $this->db->get(TBL_PREFIX.$table_name);
      return $result->row();
    }

    public function get_graph_monthly(){
      $sql = "select count(*) as total_count, MONTH(created) as m from tbl_finance_invoice where status='1'";
      $result = $this->db->query($sql)->row();
      $data= array();
      $row = array();
      $months = array('1','2','3','4','5','6','7','8','9','10','11','12');
      foreach ($months as  $value) {
        if($value==$result->m){
          $data[] =  $result->total_count;
        } else {
          $data[] = 0;
        }
        $row['graph_month']= $months;
        $row['graph_qty']=   $data ;
      }
      return $row;
    }

    public function get_processed_invoice($reqData){
     $where = "updated > DATE_SUB(CURDATE(), INTERVAL 1 DAY)";
     $table_name='finance_invoice';
     $this->db->select('count(*) as count');
     $this->db->where($where);
     $result = $this->db->get(TBL_PREFIX.$table_name);
     return $result->row();
   }

    public function get_members_summary($reqData){
      $limit = '10';
      $orderBy = '';
      $direction = '';
      $tbl = TBL_PREFIX . 'finance_invoice';
      $tbl3 = TBL_PREFIX . 'crm_participant';
      $this->db->select(array($tbl . '.id ',$tbl.'.status',
      $tbl3.'.firstname',  $tbl3.'.lastname', $tbl3.'.id as ocs_id'

      ));
      $this->db->join($tbl3, $tbl3.'.id = '.$tbl.'.participant_id', 'left');
      $this->db->from($tbl);
      $this->db->order_by($orderBy, $direction);
      $this->db->group_by('id');
      // $this->db->limit($limit, ($page * $limit));

      $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
      // var_dump($this->db->last_query());
      $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

      if ($dt_filtered_total % $limit == 0) {
          $dt_filtered_total = ($dt_filtered_total / $limit);
      } else {
          $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
      }

      $dataResult = array();

      if (!empty($query->result())) {
        $pending = 0;
        $paid = 0;
        $duplicate = 0;
        $follow_up = 0;
          foreach ($query->result() as $val) {
              $row = array();
              $status ="";
              switch($val->status){
                  case 1: $paid++; break;
                  case 2: $duplicate++; break;
                  case 3: $follow_up++; break;
                  case 4: $pending++; break;

              }
              $row['id'] = $val->id;
              $row['ocisid'] = $val->ocs_id;
              $row['member_name'] = $val->firstname.' '.$val->lastname;
              $row['funds'] = 1000;
              $row['pending'] = $pending;
              $row['follow_up'] = $follow_up;
              $row['duplicate'] = $duplicate;
              $row['paid'] = $paid;


              $dataResult[] = $row;
          }
    }

    $return = array('count' => $dt_filtered_total, 'data' => $dataResult);
    return $return;
  }
}
