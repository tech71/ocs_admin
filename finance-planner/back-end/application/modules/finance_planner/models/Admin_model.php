<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function list_role_dataquery($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'id';
            $direction = 'asc';
        }

        if (!empty($filter)) {
            $this->db->like('id', $filter);
            $this->db->or_like('name', $filter);
            $this->db->or_like('created', $filter);
        }

        $colowmn = array('id', 'name', 'status', 'created', 'archive');
        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $colowmn)), false);
        $this->db->from(TBL_PREFIX . 'role');

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));
        $this->db->where('archive', 0);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());



        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;


        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $row = array();
                $row['id'] = $val->id;
                $row['name'] = $val->name;
                $row['created'] = date('d-m-y', strtotime($val->created));
                $row['action'] = '';
                $row['archive'] = $val->archive;
                $row['status'] = $val->status;
                $dataResult[] = $row;
            }
        }


        $return = array('count' => $dt_filtered_total, 'data' => $dataResult);


        return $return;
    }

    //this method is used Recruitment side also to check duplicate email
    public function check_dublicate_email($email, $adminId) {
        $this->db->select(array('email'));
        $this->db->from('tbl_admin_email');
        $this->db->where(array('email' => $email));

        if (!empty($adminId))
            $this->db->where('adminId !=', $adminId);

        $query = $this->db->get();
        return $query->result();
    }

    public function list_admins_dataquery($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'tbl_admin.id';
            $direction = 'desc';
        }
        $where = '';
        $sWhere = $where;

        $src_columns = array('username', 'firstname', 'lastname', 'tbl_admin_phone.phone', 'tbl_admin_email.email', 'gender');
        if (!empty($filter->search)) {
            $this->db->group_start();
            $search_value = $filter->search;

            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $this->db->or_like($serch_column[0], $search_value);
                } else {
                    $this->db->or_like($column_search, $search_value);
                }
            }
            $this->db->group_end();
        }

        if (!empty($filter->search_by)) {
            if ($filter->search_by == 'archive_only') {
                $this->db->where('tbl_admin.archive', 1);
            } elseif ($filter->search_by == 'inactive_only') {
                $this->db->where('status', 0);
            } elseif ($filter->search_by == 'active_only') {
                $this->db->where('status', 1);
                $this->db->where('tbl_admin.archive', 0);
            }
        } else {
            $this->db->where('tbl_admin.archive', 0);
            $this->db->where_in('status', [0, 1]);
        }

        $colowmn = array('tbl_admin.id', 'username', 'firstname', 'lastname', 'status', 'gender', 'tbl_admin_phone.phone', 'tbl_admin_email.email', 'tbl_admin.archive');

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $colowmn)), false);
        $this->db->from(TBL_PREFIX . 'admin');

        $this->db->join(TBL_PREFIX . 'admin_email', TBL_PREFIX . 'admin_email' . '.adminId = ' . TBL_PREFIX . 'admin' . '.id AND ' . TBL_PREFIX . 'admin_email' . '.primary_email = 1', 'left');
        $this->db->join(TBL_PREFIX . 'admin_phone', TBL_PREFIX . 'admin_phone' . '.adminId = ' . TBL_PREFIX . 'admin' . '.id AND ' . TBL_PREFIX . 'admin_phone' . '.primary_phone = 1', 'left');

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        if (!empty($sWhere))
            $this->db->where($sWhere, null, false);

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

//        last_query();

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;


        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        $return = array('count' => $dt_filtered_total, 'data' => $dataResult);

        return $return;
    }

    public function get_admin_permission($adminId, $pemission_key = false) {
        if ($adminId == 1 && $pemission_key)
            return true;

        $tbl_admin_role = TBL_PREFIX . 'admin_role';

        $this->db->select(array('roleId'));
        $this->db->from($tbl_admin_role);
        $this->db->where(array('adminId' => $adminId));

        $role_query = $this->db->get();
        $rols_data = $role_query->result_array();


        if (!empty($rols_data) || $adminId == 1) {

            $tbl_permission = TBL_PREFIX . 'permission';
            $tbl_role_permission = TBL_PREFIX . 'role_permission';

            $this->db->select(array('tbl_permission.permission'));
            $this->db->from($tbl_permission);

            $this->db->join($tbl_role_permission, $tbl_role_permission . '.permission = ' . $tbl_permission . '.id', 'inner');

            if ($adminId != 1) {
                $temp_roleIds = array_column((array) $rols_data, 'roleId');
                $roleIds = implode(', ', $temp_roleIds);

                $this->db->where_in('tbl_role_permission.roleId', $temp_roleIds);
            }

            if ($pemission_key) {
                $this->db->where(array($tbl_permission . '.permission' => $pemission_key));
            }

            $permission_query = $this->db->get();
            return $permission_query->result();
        }
    }

    public function get_admin_based_roles($adminId, $all) {
        $tbl_admin_role = TBL_PREFIX . 'admin_role';
        $tbl_role = TBL_PREFIX . 'role';
        $role_id = '13';
        $tbl_role_id = $tbl_role . '.id';

        $join = ($all) ? 'left' : 'inner';

        $this->db->select(array($tbl_role . '.id', $tbl_role . '.name', $tbl_admin_role . '.adminId as access'));
        $this->db->from($tbl_role);

        if ($adminId == 1) {
            $this->db->join($tbl_admin_role, $tbl_role . '.id = ' . $tbl_admin_role . '.roleId', $join);
        } else {
            $this->db->join($tbl_admin_role, $tbl_role . '.id = ' . $tbl_admin_role . '.roleId AND ' . $tbl_admin_role . '.adminId = ' . $adminId, $join);
        }
        $this->db->where($tbl_role_id,$role_id);
        $role_query = $this->db->get();
        $rols_data = $role_query->row();


        if (!empty($rols_data)) {
          //  foreach ($rols_data as $val) {
                if (!empty($rols_data->access) && $rols_data->access > 0) {
                    $rols_data->access = true;
                } else {
                  $rols_data->access = false;
                }
          //  }
        }
        return $rols_data->access;
    }

    public function get_admin_details($adminId) {
        $tbl_admin = TBL_PREFIX . 'admin';

        $this->db->select(array('id', 'username', 'firstname', 'lastname', 'position', 'department'));
        $this->db->from($tbl_admin);
        $this->db->where(array('id' => $adminId));
        $query = $this->db->get();
        $result = (array) $query->row();

        return $result;
    }

    function get_admin_phone_number($adminId) {
        $tbl_admin_phone = TBL_PREFIX . 'admin_phone';

        $this->db->select(array('phone as name', 'primary_phone'));
        $this->db->from($tbl_admin_phone);
        $this->db->where(array('adminId' => $adminId));
        $query = $this->db->get();
        return $result = (array) $query->result();
    }

    function get_admin_email($adminId) {
        $tbl_admin_email = TBL_PREFIX . 'admin_email';

        $this->db->select(array('email as name', 'primary_email'));
        $this->db->from($tbl_admin_email);
        $this->db->where(array('adminId' => $adminId));
        $query = $this->db->get();
        return $result = (array) $query->result();
    }

    public function get_all_loges($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        $moduleArr = array('admin' => 1, 'participant' => 2, 'member' => 3, 'schedule' => 4, 'fms' => 5, 'house' => 6, 'organisation' => 7, 'imail' => 8);


        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                if ($sorted[0]->id == 'time') {
                    $orderBy = $sorted[0]->id;
                } else {
                    $orderBy = 'tbl_logs.' . $sorted[0]->id;
                }
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'id';
            $direction = 'desc';
        }

        if (!empty($filter->search_box)) {
            $this->db->like('created_by', date($filter->search_box));
            $this->db->or_like('title', $filter->search_box);
        }

        if (!empty($filter->on_date)) {
            $this->db->where("Date(created)", date('Y-m-d', strtotime($filter->on_date)));
        } else {
            if (!empty($filter->start_date)) {
                $this->db->where("Date(created) >", date('Y-m-d', strtotime($filter->start_date)));
            }if (!empty($filter->end_date)) {
                $this->db->where("Date(created) <", date('Y-m-d', strtotime($filter->end_date)));
            }
        }

        $colowmn = array('id', 'created_by', 'title', "DATE_FORMAT(created, '%d/%m/%Y') as created", "DATE_FORMAT(created, '%H:%i %p') as time");

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $colowmn)), false);
        $this->db->from(TBL_PREFIX . 'logs');

        if (!empty($filter->module)) {
            $this->db->where('module', $moduleArr[$filter->module]);
        }

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
// last_query();

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;


        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        $return = array('status' => true, 'count' => $dt_filtered_total, 'data' => $dataResult);

        return $return;
    }

}
