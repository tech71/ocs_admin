<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    public function participant_invoices($reqData){
      $limit = '10';
      $orderBy = '';
      $direction = '';
      $tbl = TBL_PREFIX . 'finance_invoice';
      $tbl3 = TBL_PREFIX . 'crm_participant';
      $participant_id = $reqData->data;
      $where = array($tbl3 . '.id' => $participant_id);
      $this->db->select(array($tbl . '.id ',$tbl.'.invoice_number',$tbl.'.due_date',  $tbl.'.participant_email',
      $tbl.'.account_number', $tbl.'.invoice_date',$tbl.'.company_name',$tbl.'.invoice_amount',$tbl.'.status',
      $tbl3.'.firstname',  $tbl3.'.lastname', $tbl3.'.id as ocs_id'

      ));
      $this->db->join($tbl3, $tbl3.'.id = '.$tbl.'.participant_id', 'left');
      $this->db->from($tbl);
      $this->db->where($where);
      $this->db->order_by($orderBy, $direction);
      $this->db->group_by('id');
      // $this->db->limit($limit, ($page * $limit));

      $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
      // var_dump($this->db->last_query());
      $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

      if ($dt_filtered_total % $limit == 0) {
          $dt_filtered_total = ($dt_filtered_total / $limit);
      } else {
          $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
      }

      $dataResult = array();

      if (!empty($query->result())) {
          foreach ($query->result() as $val) {
              $row = array();
              $status ="";
              switch($val->status){
                  case 1: $status = 'Paid'; break;
                  case 2: $status = 'Duplicate'; break;
                  case 3: $status = 'followUp'; break;
                  case 4: $status = 'inQueue'; break;

              }
              $row['id'] = $val->id;
              $row['ocisid'] = $val->ocs_id;
              $row['ndisid'] = $val->id;
              $row['company_name'] = $val->company_name;
              $row['biller'] = $val->company_name;
              $row['lastname'] = $val->firstname;
              $row['firstname'] = $val->lastname;
              $row['full_name'] = $val->firstname.' '.$val->lastname;
              $row['due_date'] = $val->due_date;
              $row['duedate'] = $val->due_date;
              $row['invoice_amount'] = $val->invoice_amount;
              $row['total'] = $val->invoice_amount;
              $row['invoice_number'] = $val->invoice_number;
              $row['invoice'] = $val->invoice_number;
              $row['participant_email'] = $val->participant_email;
              $row['status'] = $status;

              $dataResult[] = $row;
          }
      }

      $return = array('count' => $dt_filtered_total, 'data' => $dataResult);
      return $return;
    }

    public function invoice_list($reqData) {
            $limit = '10';
            $orderBy = '';
            $direction = '';
            $tbl = TBL_PREFIX . 'finance_invoice';
            $tbl3 = TBL_PREFIX . 'crm_participant';
            $type = $reqData->data;
            $where = ($type == 0)?'':array($tbl . '.status' => $type);
            $sWhere = ($tbl.'.participant_id > 0');
            $this->db->select(array($tbl . '.id ',$tbl.'.invoice_number',$tbl.'.due_date',  $tbl.'.participant_email',
            $tbl.'.account_number', $tbl.'.invoice_date',$tbl.'.company_name',$tbl.'.invoice_amount',$tbl.'.status',
            $tbl3.'.firstname',  $tbl3.'.lastname', $tbl3.'.id as ocs_id'

            ));
            $this->db->join($tbl3, $tbl3.'.id = '.$tbl.'.participant_id', 'right');
            $this->db->from($tbl);
            if(!empty($where))
            $this->db->where($where);
            $this->db->where($sWhere);
            $this->db->order_by($orderBy, $direction);
            $this->db->group_by('id');
            // $this->db->limit($limit, ($page * $limit));

            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            // var_dump($this->db->last_query());
            $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

            if ($dt_filtered_total % $limit == 0) {
                $dt_filtered_total = ($dt_filtered_total / $limit);
            } else {
                $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
            }

            $dataResult = array();

            if (!empty($query->result())) {
                foreach ($query->result() as $val) {
                    $row = array();
                    $status ="";
                    switch($val->status){
                        case 1: $status = 'Paid'; break;
                        case 2: $status = 'Duplicate'; break;
                        case 3: $status = 'followUp'; break;
                        case 4: $status = 'inQueue'; break;

                    }
                    $row['id'] = $val->id;
                    $row['ocisid'] = $val->ocs_id;
                    $row['ndisid'] = $val->id;
                    $row['company_name'] = $val->company_name;
                    $row['biller'] = $val->company_name;
                    $row['lastname'] = $val->firstname;
                    $row['firstname'] = $val->lastname;
                    $row['due_date'] = $val->due_date;
                    $row['duedate'] = $val->due_date;
                    $row['invoice_amount'] = $val->invoice_amount;
                    $row['total'] = $val->invoice_amount;
                    $row['invoice_number'] = $val->invoice_number;
                    $row['invoice'] = $val->invoice_number;
                    $row['status'] = $status;

                    $dataResult[] = $row;
                }
            }

            $return = array('count' => $dt_filtered_total, 'data' => $dataResult);
            return $return;
        }

        public function invoice_details($reqData) {
          if(!empty($reqData)){
            $invoiceId = $reqData;
           $tbl = TBL_PREFIX . 'finance_invoice';
           $tbl3 = TBL_PREFIX . 'crm_participant';
           $Where = array($tbl . '.id' => $invoiceId);
           $this->db->select(array($tbl . '.id as invoice_id',$tbl.'.invoice_number',$tbl.'.due_date',  $tbl.'.participant_email',
           $tbl.'.account_number', $tbl.'.invoice_date',$tbl.'.company_name',$tbl.'.invoice_amount',$tbl1.'.html_url',
           $tbl3.'.firstname',  $tbl3.'.lastname', $tbl3.'.id as ocs_id',$tbl3.'.houseId as funds',$tbl3.'.status'

           ));
           $this->db->join($tbl3, $tbl3.'.id = '.$tbl.'.participant_id', 'left');
           $this->db->from($tbl);
           $this->db->where($Where);
           $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
           $dataResult = array();
           $row1 = array();
           $row=array();



           if (!empty($query->result())) {
             $row1['validate'] =  $this->pdftohtmlDataValidation($query->row());

             foreach ($query->result() as $val) {



               $row['id'] = $val->invoice_id;
               $row['company_name'] = $val->company_name;
               $row['invoice_date'] =  date("d/m/Y", strtotime($val->invoice_date));
               $row1['due_date'] =  date("d/m/Y", strtotime($val->due_date));
               $row['invoice_amount'] = $val->invoice_amount;
               $row['invoice_number'] = $val->invoice_number;
               $row['account_number'] = $val->account_number;
               $row['status'] = $val->status;
              $row['funds'] = $val->funds;

              $row1['participant_firstname'] = array('value' => $val->ocs_id,'label' => $val->firstname);
              $row1['Fullname'] = $val->firstname.' '.$val->lastname;
               $row1['participant_lastname'] = $val->lastname;
               $row1['participant_ocs_id'] = $val->ocs_id;
               $row1['participant_ocs_email_acc'] = $val->participant_email;
               $urls = explode(',',$val->html_url);
               foreach($urls as $url){
                 $row1['file_contents'][]= $this->highlight($url,$row);
               }

               $row['html_url'] = $val->html_url;
               $dataResult = array_merge($row,$row1);
               //$dataResult[] = $row1;
               }

           }
           return $dataResult;
         }
        }
        public function highlight($url='', $words='')
        {

          $content  = file_get_contents(FCPATH.'assets/pdf/' .$url);
          $content =  htmlspecialchars_decode($content);
          $html = str_get_html($content);
          if(!$html->find('img[draggable="false"]',0)){
            $content =preg_replace('/'.preg_quote('src="' ,'/').'/', 'draggable="false" src="'.base_url().'assets/pdf/', $content);
          }
          if(!empty($words)){
            foreach($words as $k=>$v){
              if(strlen($content) > 0 && strlen($v) > 0){
                switch($k){
                  case 'company_name':
                  if(!$html->find('span[id=company_name]',0)){
                    $content =preg_replace('/'.preg_quote($v ,'/').'/', "<span id='company_name' style='background:lightgreen'>$v</span>", $content);
                  }
                break;
                  case 'invoice_date':
                  if(!$html->find('span[id=invoice_date]',0)){
                    $content =preg_replace('/'.preg_quote($v ,'/').'/', "<span id='invoice_date' style='background:lightgreen'>$v</span>", $content);
                  }
                  break;
                  case 'due_date':
                  if(!$html->find('span[id=due_date]',0)){
                    $content =preg_replace('/'.preg_quote($v ,'/').'/', "<span id='due_date' style='background:lightgreen'>$v</span>", $content);
                  }
                  break;
                  case 'invoice_amount':
                  if(!$html->find('span[id=invoice_amount]',0)){
                    $content =preg_replace('/'.preg_quote($v ,'/').'/', "<span id='invoice_amount' style='background:lightgreen'>$v</span>", $content);
                  }
                  break;
                  case 'invoice_number':
                  if(!$html->find('span[id=invoice_number]',0)){
                    $content =preg_replace('/'.preg_quote($v ,'/').'/', "<span id='invoice_number' style='background:lightgreen'>$v</span>", $content);
                  }
                  break;
                    }
                  }

                }
            }
            $contents= array();
      			$contents['pages'][] = array('Page1');
      			$contents['contents'][] = array($content);
            file_put_contents(FCPATH.'assets/pdf/' .$url,$content);
            return $contents;
            }


        public function pdftohtmlDataValidation($data) {
              $response = array();
             $participant_details =$this->basic_model->get_row($table_name ='crm_participant', $columns = array('firstname, lastname, id'), $id_array = array('id'=>$data->ocs_id));
             // Last Name
             if($data->firstname==$participant_details->firstname){
               $response['participant_firstname'] = array('error'=>true, 'message'=>'Particpant Name Not Match');
             }
             else{
               $response['participant_firstname'] = array('error'=>false);
             }

             // Company Name
             $participant_vendor =$this->basic_model->get_row($table_name ='vendor', $columns = array('name'), $id_array = array('crm_participant_id'=>$data->ocs_id));

            if($data->company_name!=$participant_vendor->name){
                $response['company_name'] = array('error'=>true, 'message'=>'Company Name does Not Match');
             }
             else{
               $response['company_name'] = array('error'=>false);
             }
             // Here need to check available fund once data available in funds table
             if($data->invoice_amount<=500){
                $response['invoice_amount'] = array('error'=>true, 'message'=>'Not Sufficient Available Funds');
             }
             else{
               $response['invoice_amount'] = array('error'=>false);
             }
             //Have to Chhhe Biller, Need an Discussion
             if($data->invoice_number){
                $response['invoice_number'] = array('error'=>true, 'message'=>'Not Known Biller');
             }
             else{
               $response['invoice_number'] = array('error'=>false);
             }
            return $response;
        }
        public function status_change($invoice_status,$id,$invoicedetails,$adminId=1){
         $tbl = 'finance_invoice';
         switch($invoice_status){
             case 'Paid': $status = 1;
               if(!empty($invoicedetails)){
                 $res = $this->invoice_update($invoicedetails);
                 $data = array('title'=>'Invoice moved to Paid','category'=>'Paid','user'=>$adminId);
                 $res= $this->basic_model->insert_records('finance_audit_logs',$data);
                 if(empty($res)){
                   return;
                 }
               }
             break;
             case 'Duplicate': $status = 2;
             if(!empty($invoicedetails)){
               $res = $this->invoice_update($invoicedetails);
               $data = array('title'=>'Invoice moved to Duplicate','category'=>'Duplicate','user'=>$adminId);
               $res= $this->basic_model->insert_records('finance_audit_logs',$data);
               if(empty($res)){
                 return;
               }
             }break;
             case 'followUp': $status = 3;
             $data = array('title'=>'Invoice moved to followUp','category'=>'followUp','user'=>$adminId);
             $res= $this->basic_model->insert_records('finance_audit_logs',$data);
              break;
             case 'inQueue': $status = 4;
             $data = array('title'=>'Invoice moved to inQueue','category'=>'inQueue','user'=>$adminId);
             $res= $this->basic_model->insert_records('finance_audit_logs',$data);
              break;
           }
           $data = array('status'=>$status);
           $where = array('id'=>$id);
           $response = $this->basic_model->update_records($tbl,$data,$where);
           return $response;
       }
       public function invoice_update($invoicedetails){
         $tbl = 'finance_invoice';
         $invoice_date= $invoicedetails->invoice_date;
         $due_date= $invoicedetails->due_date;
         $company_name= $invoicedetails->company_name;
         $invoice_number= $invoicedetails->invoice_number;
         $invoice_amount= $invoicedetails->invoice_amount;
         // $biller_code= $invoicedetails->biller_code;
         // $reference_no= $invoicedetails->reference_no;
         // $po_number= $invoicedetails->po_number;
         $id= $invoicedetails->id;
         $data = array('invoice_date'=>$invoice_date,'company_name'=>$company_name,'due_date'=>$due_date,'invoice_number'=>$invoice_number,'invoice_amount'=>$invoice_amount);
         $where = array('id'=>$id);
         $res = $this->basic_model->update_records($tbl,$data,$where);
         return $res;
       }

    public function add_dispute_note($reqData){

        $result = $this->basic_model->insert_records('finance_dispute_note',$reqData);
        if (!empty($result)) {
         return true;
        } else {
         return false;
        }

    }

    public function get_list_dispute_note(){
      $result = $this->basic_model->get_record_where_orderby('finance_dispute_note', $column = '', $where = '', $orderby = 'id', $direction = 'DESC');
      if (!empty($result)) {
        $dnotes = array();
        foreach ($result as $key => $value) {
           $dnotes[]= array(
             'user' => $value->contact_name,
             'type' => $value->contact_method,
             'date' => date('d-m-Y',strtotime($value->created)),
             'notes' => $value->notes
           );
        }
       return $dnotes;
      } else {
       return false;
      }
    }

    public function get_participant_name($post_data) {
             $tbl = TBL_PREFIX . 'crm_participant';
             $tbl2 = TBL_PREFIX . 'crm_participant_email';
             // $this->db->or_where("(MATCH (firstname) AGAINST ('$post_data *'))", NULL, FALSE);
            // $this->db->or_where("(MATCH (lastname) AGAINST ('$post_data *'))", NULL, FALSE);
            $this->db->select($tbl.".firstname,".$tbl.".lastname,".$tbl.".id as ocs_id,".$tbl2.".email");
             $this->db->join($tbl2, $tbl2.'.crm_participant_id = '.$tbl.'.id', 'left');
            $this->db->or_where($tbl.".firstname LIKE  '%$post_data%' ");
            $this->db->or_where($tbl.".lastname LIKE  '%$post_data%' ");
             $this->db->where($tbl.'.archive =', 0);
             $this->db->where($tbl.'.status=', 1);
             $this->db->where($tbl2.'.primary_email=', 1);

             $query = $this->db->get(TBL_PREFIX . 'crm_participant');
             $query->result();
             $participant_rows = array();
             if (!empty($query->result())) {
                 foreach ($query->result() as $val) {
                     $participant_rows[] = array('value' => $val->ocs_id,'label' => $val->firstname,'email'=>$val->email,'lastname' => $val->lastname);
                  }
             }
             return $participant_rows;
         }
       public function get_company_name($post_data) {
             $tbl = TBL_PREFIX . 'vendor';
             $this->db->select($tbl.".name,".$tbl.".id as id,");
             $this->db->or_where($tbl.".name LIKE  '%$post_data%' ");
             $this->db->where($tbl.'.status=', 1);

             $query = $this->db->get(TBL_PREFIX . 'vendor');
             $query->result();
             $company_rows = array();
             if (!empty($query->result())) {
                 foreach ($query->result() as $val) {
                     $company_rows[] = array('value' => $val->id,'label' => $val->name,);
                  }
             }
             return $company_rows;
         }
       public function get_participant_id() {
             $tbl = TBL_PREFIX . 'crm_participant';
             $tbl2 = TBL_PREFIX . 'crm_participant_email';
             // $this->db->or_where("(MATCH (firstname) AGAINST ('$post_data *'))", NULL, FALSE);
            // $this->db->or_where("(MATCH (lastname) AGAINST ('$post_data *'))", NULL, FALSE);,'firstname' => $val->firstname
            $this->db->select($tbl.".firstname,".$tbl.".lastname,".$tbl.".id as ocs_id,".$tbl2.".email");
             $this->db->join($tbl2, $tbl2.'.crm_participant_id = '.$tbl.'.id', 'left');
             $this->db->where($tbl.'.archive =', 0);
             $this->db->where($tbl.'.status=', 1);
             $this->db->where($tbl2.'.primary_email=', 1);

             $query = $this->db->get(TBL_PREFIX . 'crm_participant');
             $query->result();
             $participant_rows = array();
             if (!empty($query->result())) {
                 foreach ($query->result() as $val) {
                     $participant_rows[] = array('label' => $val->ocs_id,'value' => $val->firstname,'email'=>$val->email,'lastname' => $val->lastname);
                  }
             }
             return $participant_rows;
         }
  public function list_audit_logs($reqData) {
    $limit = $reqData->pageSize;
    $page = $reqData->page;
    $sorted = $reqData->sorted;
    $filter = $reqData->filtered;
    $orderBy = array();
    $direction = '';

    if(!empty(request_handler()))
    {
        $tbl_1 = TBL_PREFIX . 'finance_audit_logs';
        $tbl_2 = TBL_PREFIX . 'admin';
        $dt_query = $this->db->select(array($tbl_1.'.title',$tbl_1.'.id ',$tbl_1.'.category',$tbl_1.'.created',"CONCAT(tbl_admin.firstname,' ',tbl_admin.lastname) AS user_name",$tbl_1.'.user'));

        $this->db->from($tbl_1);
        $this->db->join('tbl_admin', 'tbl_finance_audit_logs.user = tbl_admin.id', 'inner');
        $this->db->where($tbl_2.'.archive=',"0");

        //pr($sorted);
        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                if ($sorted[0]->id) {
                    $orderBy = $sorted[0]->id;
                } else {
                    $orderBy = 'tbl_admin.' . $sorted[0]->id;
                }
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'tbl_admin.id';
            $direction = 'desc';
        }
        if(!empty($filter)){


          if (!empty($filter->filterVal))
          {

              $this->db->group_start();
              $src_columns = array($tbl_1.'.title',$tbl_1.'.category',$tbl_1.'.user');

              for ($i = 0; $i < count($src_columns); $i++)
              {
                  $column_search = $src_columns[$i];
                  if (strstr($column_search, "as") !== false) {
                      $serch_column = explode(" as ", $column_search);
                      $this->db->or_like($serch_column[0],$filter->filterVal);
                  } else {
                      $this->db->or_like($column_search,$filter->filterVal);
                  }
              }
              $this->db->group_end();
          }
          if (!empty($filter->categoryVal)) {
              $this->db->where('tbl_finance_audit_logs.category', $filter->categoryVal);
          }
          if (!empty($filter->fromDate)) {
              $this->db->where('DATE(tbl_finance_audit_logs.created)>="'.$filter->fromDate.'"');
          }
          if (!empty($filter->toDate)) {
              $this->db->where('DATE(tbl_finance_audit_logs.created)<="'.$filter->toDate.'"');
          }
        }
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));
        $query = $this->db->get();
        // echo $this->db->last_query();
        //last_query();

        $dt_filtered_total = $all_count =  $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $x = $query->result_array();

        $site_ary = array();
        foreach ($x as $key => $value)
        {

            $temp['user_name'] = $value['user_name'];
            $temp['user'] = $value['user'];
            $temp['category'] = $value['category'];
            $temp['title'] = $value['title'];
            $temp['created'] = $value['created']!='0000-00-00 00:00:00'?date('d/m/Y',strtotime($value['created'])):'';
            $site_ary[] = $temp;
        }
        $return = array('data' => $site_ary,'count' => $dt_filtered_total, 'status'=>true);
        return $return;
      }
    }

  }
