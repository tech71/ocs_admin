<?php



defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class Dashboard extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Dashboard_model');
    }

    function get_graph_data(){
      $reqData = request_handler();
      $reqData = $reqData->data;
        if (!empty($reqData)) {
          $all  = $this->Dashboard_model->get_graph_monthly();
          if(!empty($all)){
              $response = array('status'=>true,'data'=>$all);
          }else {
              $response = array('status'=>false,'data'=>[]);
          }
          echo json_encode($response);
        }
    }

    function all_typeofinvoice_count(){
      $reqData = request_handler();
      $reqData = $reqData->data;
      if (!empty($reqData)) {
      $types = array('Pending','Followup','Duplicate');
      $all = array();
        foreach($types as $type){
          switch ($type) {
            case 'Pending':
              $where['status'] = '4';
              $all[$type]  = $this->Dashboard_model->get_counts_bystatus($where);
              break;
            case 'Followup':
              $where['status'] = '3';
              $all[$type]  = $this->Dashboard_model->get_counts_bystatus($where);
              break;
            case 'Duplicate':
              $where['status'] = '2';
              $all[$type] = $this->Dashboard_model->get_counts_bystatus($where);
              break;
            default:
              break;
          }
        }
      $response = array('status'=>true,'data'=>$all);
      } else  {
      $response = array('status'=>false,'data'=>[]);
      }
     echo json_encode($response);
    }

    function get_members_summary(){
     $reqData = request_handler();
     $reqData = $reqData->data;
     if (!empty($reqData)) {
       $data = $this->Dashboard_model->get_members_summary($reqData);
       $response = array('status'=>true,'data'=>$data['data']);
     }
     else{
       $response = array('status'=>false,'data'=>[]);
     }
     echo json_encode($response);
   }

   function get_processed_invoice(){
      $reqData = request_handler();
      $reqData = $reqData->data;
      if (!empty($reqData)) {
        $data = $this->Dashboard_model->get_processed_invoice($reqData);

        $response = array('status'=>true,'data'=>$data);
      }
      else{
        $response = array('status'=>false,'data'=>[]);
      }
      echo json_encode($response);
    }
  }
