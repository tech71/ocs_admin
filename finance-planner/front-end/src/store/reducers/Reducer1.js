import * as actionType from '../actions';

const initialState = {
    sidebarState: '',
    loginState: false,
    loginError:'',
    loginSuccess:'',
    summary:[],
    processedInvoice:[],
    loading:false,
    graphdata:[],
    forgot_password:false,

}


const reducer = (state = initialState, action) => {
    switch (action.type) {

        case actionType.SIDEBAROPEN:
            // console.log('sidebaropen') ;

            const prevSideState = state.sidebarState;
            if (prevSideState === action.pos) {
                return {
                    ...state,
                    sidebarState: ''
                }

            }
            else {
                return {
                    ...state,
                    sidebarState: action.pos
                }

            }

        case actionType.SIDEBARCLOSE:
            // console.log('logout') ;
            return {
                sidebarState: ''
            }

        case actionType.LOGOUT:
            // console.log('logout') ;
            return {
                sidebarState: '',
                loginState: false
            }
        case actionType.FORGOT_PASSWORD:
            // console.log('logout') ;
            return {
                sidebarState: '',
                loginState: true
            }
        case actionType.LOGIN:

            return {
                loginState: false
            }

        case actionType.LOGIN_SUCCESS:

            return {
                loginState: true,
                loginSuccess:action.success
            }
        case actionType.LOGIN_FAILURE:

                return {
                loginState: false,
                loginError:action.error
        }

        case actionType.COUNT_SUCCESS:
                return {
                  ...state,
                countData:action.countData
        }
        case actionType.MEMBERS_SUMMARY:
                return {
                  ...state,
                summary:action.summary
        }

        case actionType.PROCESSED_INVOICE:
                return {
                  ...state,
                processedInvoice:action.processedInvoice
        }

        case actionType.GRAPH_DATA:
               return {
                 ...state,
               graphdata:action.graphdata
       }

        case actionType.LOADERSTATE:
        return {
          ...state,
        loading:action.loadState
        }

        default :


    }

    return state;

};



export default reducer;
