import * as actionType from '../actions';
const initialState = {
  allInvoice: [],
  pendingInvoice:[],
  followUpInvoice:[],
  invoicedetails:[],
  participantInvoiceDetails:[],
  loading: false,
  error: null,InvoiceStatus:'',
  fetchInvoice:[],
  disputeNotes:[],
  disputeError:[], participnatId:[],auditLogs:[]
};
export default function invoiceReducer(state = initialState, action) {

  switch(action.type) {
    case actionType.INVOICE_STATUS:
            return {
              ...state,
            InvoiceStatus:action.InvoiceStatus
    }
    case actionType.FETCH_INVOICE:
            return {
              ...state,
            FetchInvoice:action.FetchInvoice
    }
    case actionType.AUDIT_LOGS:
            return {
              ...state,
            auditLogs:action.auditLogs
    }
    case actionType.PARTICIPANT_ID:
     return {
         ...state,
         CrmParticipantId:action.CrmParticipantId
     }
    case actionType.FETCH_ALL_INVOICE:
      return {
        ...state,
        loading: false,
        allInvoice: action.allInvoice
      };
    case actionType.FETCH_PENDING_INVOICE:
      return {
        ...state,
        loading: false,
        pendingInvoice: action.pendingInvoice
      };
    case actionType.FETCH_DUPLICATE_INVOICE:
      return {
        ...state,
        loading: false,
        duplicateInvoice: action.duplicateInvoice
      };
    case actionType.FETCH_FOLLOW_UP_INVOICE:
      return {
        ...state,
        loading: false,
        followUpInvoice: action.followUpInvoice
      };
    case actionType.FETCH_PARTICIPANT_INVOICE:
      return {
        ...state,
        loading: false,
        participantInvoiceDetails: action.participant_invoice
      };
    case actionType.INVOICE_DETAILS:
      return {
        ...state,
        loading: false,
        invoicedetails: action.invoicedetails
      };
    case actionType.PDF_DATA:
     return {
         ...state,
         pdfData:action.data
     }
     case actionType.DISPUTE_NOTE:
      return {
          ...state,
          disputeSuccess:action.disputeNoteSuccess,
          disputeError:action.disputeNoteError
      }
      case actionType.GET_DISPUTE_NOTES:
       return {
           ...state,
           disputeNotes:action.disputeAllNotes
       }
    default:
      return state;
  }
}
