import React, { Component } from 'react';
import { Link,Redirect } from 'react-router-dom';
import Select from 'react-select-plus';
import DatePicker from "react-datepicker";

import PdfViewer from './utils/PdfViewer';

import moment from 'moment';
import AuthWrapper from '../hoc/AuthWrapper';
import NotifyPopUp from './utils/NotifyPopUp';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { invoiceDetails, pdfData, loader,invoice_status } from '../store/actions';


class ViewPendingInvoices extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterVal: '',
            startDate: null,
            processModal: false,
            paidPopUp: false,
            followPopUp: false, invoicedetails: [],
            redirect:false
        }

    }

    componentWillMount() {

        this.props.loader(true);
        this.props.invoiceDetails(this.props.match.params.id)
            .then(json => {
                this.setState({ invoicedetails: json.data },()=>this.props.loader(false));
                this.props.pdfData(json.data);
            });



    }

    selectChange = (selectedOption, fieldname) => {

    var state = this.state.invoicedetails;
    state[fieldname] = selectedOption;
    state[fieldname + '_error'] = false;

    this.setState(state);

  }
    status_update = (status)=>{
      this.props.invoice_status(status,this.props.match.params.id,this.state.invoicedetails);
      switch(status){
        case 'followUp':this.setState({ followPopUp: true });
        break;
        case 'Paid':this.setState({ paidPopUp: true });
        break;
      }

    }
    componentDidCatch(error) {
        console.log(error);
    }

    render() {
        if(this.state.redirect){
        return(  <Redirect to='/pendinginvoices'/>);
        }
        const invoice_details = this.state.invoicedetails;
        let company_name_error = (typeof (invoice_details.validate) != 'undefined') ? invoice_details.validate.company_name.error : false;
        let firstname_error = (typeof (invoice_details.validate) != 'undefined') ? invoice_details.validate.participant_firstname.error : false;
        let invoice_number_error = (typeof (invoice_details.validate) != 'undefined') ? invoice_details.validate.invoice_number.error : false;
        let invoice_amount_error = (typeof (invoice_details.validate) != 'undefined') ? invoice_details.validate.invoice_amount.error : false;

        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        const html = '<div>Example HTML string</div>';



        return (

            <AuthWrapper>


                <div className=" back_col_cmn-">
                    <Link to='/pendinginvoices'><span className="icon icon-back1-ie"></span></Link>
                </div>

                <div className="main_heading_cmn-">
                    <h1>
                        <span>1/467 Pending Invoice</span>
                        <Link to='/pendinginvoices/viewpaidinvoice'>
                            <button className="btn hdng_btn cmn-btn1">Skip</button>
                        </Link>
                        <Link to='#'>
                            <button className="btn hdng_btn cmn-btn2">Exit</button>
                        </Link>
                    </h1>
                </div>

                <div className='row'>

                    <div className='col-xl-7 col-lg-12 col-12'>

                        <PdfViewer />
                        <div className='clearfix'></div>

                        <div className='mr_t_15 pd_tb_15 d-flex justify-content-center'>
                            <span className=' flwUp_btn frm_bt23' onClick={() => {this.status_update('followUp')   }}>Follow Up</span>
                            <button className=' cmn-btn3 frm_bt23' disabled={(invoice_details.funds>=invoice_details.invoice_amount)?false:true} onClick={() => { this.status_update('Paid') }} >Pay Invoice <i className='icon icon-accept-approve2-ie'></i></button>
                        </div>


                    </div>


                    <div className='col-xl-5 col-lg-12 col-12'>

                        <div className='fieldArea'>

                            <div className='mini_profile__ d-flex'>

                                <div className='pro_ic1'>
                                    <i className='icon icon-userm1-ie'></i>
                                </div>

                                <div className='prf_cntPart'>
                                    <h4 className='cmn_clr1'><strong>{invoice_details.Fullname}</strong></h4>
                                    <div>
                                        <h5 className='avl_fnd'>
                                            <strong>Avail Funds :</strong>
                                            <span>${invoice_details.funds}</span>
                                        </h5>
                                    </div>

                                    <div className='d-flex align-items-center'>
                                        <select className='cstmSlctHt stats_slct' >
                                            <option selected={(invoice_details.status==1)?"selected":""}>Active</option>
                                            <option selected={(invoice_details.status==0)?"selected":""}>Inactive</option>
                                        </select>

                                        <label className='cstmChekie clrTpe2'>
                                            <input type="checkbox" className=" " />
                                            <div className="chkie"></div>
                                        </label>

                                    </div>


                                </div>

                            </div>
                            {/* mini_profile__ ends */}

                            <div className='bor_tb  pd_tb_10' >
                                <strong>Member Details</strong>
                            </div>


                            <div className='row pd_tb_15'>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>OCS Mail Account</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" className="csForm_control completed" name="" readOnly value={invoice_details.participant_ocs_email_acc || ''} />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>OCS ID</label>
                                        <div className='csaddOn_fld1__'>
                                            <input type="text" className="csForm_control completed" name="" readOnly value={invoice_details.participant_ocs_id || ''} />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>First Name</label>
                                        <div className='csaddOn_fld1__'>
                                            <div className='cstm_select search_filter grnbrdr_slct'>
                                                <input type="text" name="first_name" className='csForm_control completed' readOnly value={invoice_details.participant_firstname || ''} />
                                                {  // <Select name="view_by_status "
                                                    //     required={true} simpleValue={true}
                                                    //     searchable={true} Clearable={false}
                                                    //     placeholder=""
                                                    //     onChange={(e) => this.setState({ filterVal: e })}
                                                    //     value={invoice_details.participant_firstname}
                                                    //     options={options}
                                                    //
                                                    // />
                                                }</div>
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Match System Data</label>
                                        <div className={(firstname_error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                            <input type="text" className={(firstname_error) ? "csForm_control errorIn2" : "csForm_control completed"} name="" />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Last Name</label>
                                        <div className='csaddOn_fld1__'>
                                            <div className='cstm_select search_filter grnbrdr_slct'>
                                                <input type="text" name="last_name" className="csForm_control completed" readOnly value={invoice_details.participant_lastname || ''} />
                                                {  // <Select name="view_by_status "
                                                    //     required={true} simpleValue={true}
                                                    //     searchable={true} Clearable={false}
                                                    //     placeholder=""
                                                    //     onChange={(e) => this.setState({ filterVal: e })}
                                                    //     value={invoice_details.participant_lastname}
                                                    //
                                                    // />
                                                }</div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            {/* row ends */}

                            <div className='bor_tb  pd_tb_10' >
                                <strong>Account Details</strong>
                            </div>

                            <div className='row pd_tb_15'>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Invoice Date</label>
                                        <div className='datewid_100'>
                                            <DatePicker
                                                className="csForm_control  grnbrdr_date"
                                                dateFormat="DD/MM/YYYY"
                                                utcOffset={0}
                                                selected={moment(invoice_details.invoice_date, 'DD/MM/YYYY').valueOf()}
                                                value={invoice_details.invoice_date}
                                                onChange={(e) =>this.selectChange(moment(e).format("DD/MM/YYYY"),'invoice_date')}
                                                dateFormat="DD/MM/YYYY"
                                                name="tempDueDate"
                                                placeholderText={'DD/MM/YYYY'}
                                            />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Due Date</label>
                                        <div className='datewid_100'>
                                            <DatePicker
                                                utcOffset={0}
                                                className="csForm_control  grnbrdr_date"
                                                selected={moment(invoice_details.due_date, 'DD/MM/YYYY').valueOf()}
                                                value={invoice_details.due_date}
                                                onChange={(e) =>this.selectChange(moment(e).format("DD/MM/YYYY"),'due_date')}
                                                dateFormat="DD/MM/YYYY"
                                                name="tempDueDate"
                                                placeholderText={'DD/MM/YY'}
                                            />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Company Name</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" className="csForm_control filled" name="" onChange={(e) =>this.selectChange(e.target.value,'company_name')} value={invoice_details.company_name || ''} />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Matches Xero Supplier</label>
                                        <div className={(company_name_error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                            <input type="text" className={(company_name_error) ? "csForm_control errorIn2" : "csForm_control completed"} name="" />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Invoice No.</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" className="csForm_control filled" name="" onChange={(e) =>this.selectChange(e.target.value,'invoice_number')} value={invoice_details.invoice_number || ''} />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Known Biller</label>
                                        <div className={(invoice_number_error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                            <input type="text" className={(invoice_number_error) ? "csForm_control errorIn2" : "csForm_control completed"} name="" />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Invoice Amount</label>
                                        <div className='csaddOn_fld1__ checkedIc1'>
                                            <input type="text" className="csForm_control filled" name="" onChange={(e) =>this.selectChange(e.target.value,'invoice_amount')} value={invoice_details.invoice_amount || ''} />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Sufficient Available Balance</label>
                                        <div className={(invoice_amount_error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                            <input type="text" className={(invoice_amount_error) ? "csForm_control errorIn2" : "csForm_control completed"} name="" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                            {/* row ends */}


                            <div className='row'>

                                <div className='col-12'>

                                    <div className='inpLabel1'><strong>Payment Type</strong></div>
                                    <ul className='py_typeUL'>
                                        <li className='active'>Bank</li>
                                        <li>B Pay</li>
                                        <li>AUS Post</li>
                                    </ul>

                                </div>

                            </div>
                            {/* row ends */}

                            <div className='row'>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Biller Code</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" onChange={(e) =>this.selectChange(e.target.value,'biller_code')} className="csForm_control filled" name="" />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Reference Number</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" className="csForm_control filled" name="" onChange={(e) =>this.selectChange(e.target.value,'reference_no')}/>
                                        </div>
                                    </div>
                                </div>


                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>PO Number</label>
                                        <div className='csaddOn_fld1__ notAvail'>
                                            <input type="text" className="csForm_control " name="" value='NA' onChange={(e) =>this.selectChange(e.target.value,'po_number')} />
                                        </div>
                                    </div>
                                </div>


                            </div>
                            {/* row ends

                            <div className='bor_tb  pd_tb_10 mr_t_15' >
                                <strong>Member Details</strong>
                            </div>

                            <div className='row pd_tb_15' >

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>OCS Mail Account</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" className="csForm_control completed" name="" />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>OCS ID</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" className="csForm_control completed" name="" />
                                        </div>
                                    </div>
                                </div>


                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>First Name</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" className="csForm_control filled" name="" />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Match System Data</label>
                                        <div className='csaddOn_fld1__ checkedIc1'>
                                            <input type="text" className="csForm_control completed" name="" />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Last Name</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" className="csForm_control filled" name="" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                            {/* row ends */}

                        </div>

                        <div className={'processModal ' + (this.state.processModal ? 'show' : '')}>
                            <div className='modalDialog1'>
                                <h4>Document Awaiting processing click to process now</h4>
                                <button className='cmn-btn3 scn_btn'>Scan Now</button>
                            </div>
                        </div>



                    </div>
                    {/* col-6 ends */}

                </div>
                {/* row ends */}

                <NotifyPopUp
                    notifyColor='green'
                    show={this.state.paidPopUp}
                    Close={() => { this.setState({ paidPopUp: false,redirect:true }) }}
                    iconLef={'icon-approved2-ie'}
                    btnName={"Undo"}
                >

                    Invoice <span className='txt_clr'>{invoice_details.invoice_number}</span> marked as paid

                </NotifyPopUp>


                <NotifyPopUp
                    notifyColor='yellow'
                    show={this.state.followPopUp}
                    Close={() => { this.setState({ followPopUp: false,redirect:true}) }}
                    iconLef={"icon-error-icons"}
                    btnName={"Undo"}
                >

                    Invoice <span className='txt_clr'>{invoice_details.invoice_number}</span> moved to follow up

                </NotifyPopUp>

            </AuthWrapper>

        );
    }
}



const mapStateToProps = state => {
    return {
        invoice_details: state.InvoiceReducer.invoicedetails,
        pdfData: state.InvoiceReducer.data,
        // InvoiceStatus:InvoiceStatus

    }
};
const mapDispatchtoProps = (dispatch) => bindActionCreators({
    invoiceDetails, pdfData, loader,invoice_status
}, dispatch)
export default connect(mapStateToProps, mapDispatchtoProps)(ViewPendingInvoices);
