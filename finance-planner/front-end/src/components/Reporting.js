import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';


import AuthWrapper from '../hoc/AuthWrapper';
import * as dummy from '../hoc/DummyDatas';


class Reporting extends Component {


    render() {
        const jobsData2 = {
            labels: ['', '', '', '', '', '', ''],
            datasets: [
                {
                    label: 'New',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: '#000',
                    borderColor: '#000',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: '#000',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: '#000',
                    pointHoverBorderColor: '#000',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [65, 59, 80, 81, 56, 55, 40],

                },
                {
                    label: 'Overall',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: '#737373',
                    borderColor: '#737373',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: '#737373',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: '#737373',
                    pointHoverBorderColor: '#737373',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [50, 35, 58, 74, 20, 55, 100]
                }
            ]
        };


        return (

            <AuthWrapper>

                        <div className=" back_col_cmn-">
                            <span onClick={() =>  window.history.back()} className="icon icon-back1-ie"></span>
                        </div>

                        <div className="main_heading_cmn-">
                            <h1>
                                <span>Reporting</span>
                                
                            </h1>
                        </div>

                        <div className='invStatus bor_bot1 mr_t_15'>
                            Invoices Processed
                        </div>

                        <div className='compoBoxDiv__ d-flex row'>

                            <div className='col-sm-12'>
                                <div className='compBox'>
                                    <p className='grpMes cmn_clr1'><strong>Qty</strong></p>
                                    <div className='grph_dv'>
                                        <Bar data={dummy.Recruitmentdata} height={350} legend={null}
                                            options={{
                                                maintainAspectRatio: false
                                            }}
                                        />
                                    </div>
                                    <p className='grpMes'><strong>Month</strong></p>


                                    <div className='row'>

                                        <div className='col-md-3'></div>
                                        <div className='col-md-6'>
                                            <div className='grph_optGrp d-flex align-items-center justify-content-center'>
                                                <span><strong>Select:</strong></span>
                                                <button className='cmn-btn1 pd_bts active'>Paid</button>
                                                <button className='cmn-btn1 pd_bts'>Duplicate</button>
                                                <button className='cmn-btn1 pd_bts'>Automated</button>
                                            </div>
                                        </div>
                                       
                                    </div>


                                </div>
                            </div>
                        </div>
                        {/* compoBoxDiv__ ends */}



                        <div className='compoBoxDiv__ d-flex row'>

                            <div className='col-lg-4 mr_b_15'>
                                <div className='compBox'>
                                    <p className='text-center'><strong>YTD Automated</strong></p>
                                    

                                    <div className='row align-items-center'>
                                        <div className='col-xl-5 col-lg-12 col-5 text-center'>
                                            {/* <h1 className=''><strong>141</strong></h1> */}
                                            <div>
                                                <Line data={jobsData2} legend={null} height={250} />
                                            </div>
                                            
                                        </div>
                                        <div className='col-xl-7 col-lg-12 col-7'>
                                            <div className='gr_desc1__'>Total Invoices Successfully Automated this year</div>
                                            <h3>.</h3>

                                            <div><strong>Year to Date</strong></div>
                                            <ul className='opt_ul '>
                                                <li>.</li>
                                                <li>.</li>
                                                <li>.</li>
                                            </ul>
                                        </div>
                                    
                                    </div>
                                    
                                </div>
                            </div>

                            <div className='col-lg-4 mr_b_15'>
                                <div className='compBox '>
                                    <p className='text-center'><strong>Duplicated Invoices</strong></p>
                                    <div className='row align-items-center'>
                                        <div className='col-xl-5 col-lg-12 col-5 text-center'>
                                            <h1 className='amt2__'><strong>100</strong></h1>
                                            
                                        </div>
                                        <div className='col-xl-7 col-lg-12 col-7'>
                                            <div className='gr_desc1__'>Total Invoices marked as duplicate this year</div>
                                            <h3>.</h3>
                                            <div><strong>Year to Date</strong></div>
                                            <ul className='opt_ul '>
                                                <li>.</li>
                                                <li>.</li>
                                                <li>.</li>
                                            </ul>
                                        </div>
                                    
                                    </div>
                                    
                                   
                                </div>
                            </div>

                            <div className='col-lg-4 mr_b_15'>
                                <div className='compBox'>
                                    <p className='text-center'><strong>Successful Xero Syncs</strong></p>
                                    <div className='row align-items-center'>
                                        <div className='col-xl-5 col-lg-12 col-5 text-center'>
                                            <h1 className='amt2__'><strong>54</strong></h1>
                                            
                                        </div>
                                        <div className='col-xl-7 col-lg-12 col-7'>
                                            <div className='gr_desc1__'>Total Invoices Synced to Xero this year</div>
                                            <h3>.</h3>
                                            <div><strong>Year to Date</strong></div>
                                            <ul className='opt_ul '>
                                                <li>.</li>
                                                <li>.</li>
                                                <li>.</li>
                                            </ul>
                                        </div>
                                    
                                    </div>
                                   
                                </div>
                            </div>


                        </div>
                        {/* compoBoxDiv__ ends */}


                        <div className='invStatus bor_bot1 mr_t_15'>
                            Invoices Automated
                        </div>

                        <div className='compoBoxDiv__ d-flex row'>

                            <div className='col-sm-12'>
                                <div className='compBox'>
                                    <p className='grpMes cmn_clr1'><strong>Qty</strong></p>
                                    <div className='grph_dv'>
                                        <Bar data={dummy.Recruitmentdata} height={350} legend={null}
                                            options={{
                                                maintainAspectRatio: false
                                            }}
                                        />
                                    </div>
                                    <p className='grpMes'><strong>Month</strong></p>


                                    <div className='row'>

                                        <div className='col-md-3'></div>
                                        <div className='col-md-6'>
                                            <div className='grph_optGrp d-flex align-items-center justify-content-center'>
                                                <span><strong>Select:</strong></span>
                                                <button className='cmn-btn1 pd_bts active'>Paid</button>
                                                <button className='cmn-btn1 pd_bts'>Duplicate</button>
                                                <button className='cmn-btn1 pd_bts'>Automated</button>
                                            </div>
                                        </div>
                                       
                                    </div>


                                </div>
                            </div>
                        </div>
                        {/* compoBoxDiv__ ends */}

            </AuthWrapper>

        );
    }
}





export default Reporting;
