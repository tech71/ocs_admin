import React, { Component } from 'react';
import Select from 'react-select-plus';
import { connect } from 'react-redux';
// import { Redirect } from 'react-router-dom';
import * as actionType from '../../store/actions';
import { logout,check_loginTime } from '../../service/common.js';
import { ROUTER_PATH, BASE_URL } from '../../config.js';


class Header extends Component {


    constructor() {
        super();
        this.state = {
            filterVal: '',
            // logout: false
        }
    }
    componentDidMount() {
      setInterval(() => {
          check_loginTime();
      }, 1000*60)
  }

    // signOut = () => {

    //     this.props.logOut();
    //     this.setState({logout: true})

    // }


    signOut = () =>{
        logout(ROUTER_PATH);
    }


    render() {


        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        // const {logout} = this.state;
        // if(logout){
        //         console.log('its logout')
        //     return <Redirect to="/"/>
        // }



        return (

            <React.Fragment>

                <div
                    className={'backdrop ' + (this.props.sidebarState === 'left' || this.props.sidebarState === 'right'?'show':'')}
                    onClick={() => this.props.closeSidebar()}
                >
                </div>
                <header className='main_header'>

                    <div className='row align-items-center'>

                        <div className='col-3 d-flex'>
                            <i className='icon icon-bars hdrs_ics'></i>
                        </div>

                        <div className='col-6'>

                            <div className='cstm_select header_filter search_filter'>

                                <Select name="view_by_status "
                                    required={true} simpleValue={true}
                                    searchable={true} Clearable={false}
                                    placeholder="Search"
                                    options={options}
                                    onChange={(e) => this.setState({ filterVal: e })}
                                    value={this.state.filterVal}

                                />

                            </div>

                        </div>

                        <div className='col-3 text-right'>

                            <div className='right_ul'>


                                 <span className='Ulopts' onClick={() => this.props.sideBarOpen('info', 'left' )}>
                                    <i className='icon icon-info hdrs_ics'></i>

                                </span>
                                <span className='Ulopts'  onClick={() => this.props.sideBarOpen('notification', 'right')}>
                                    <i className='icon icon-notification-icons hdrs_ics'></i>
                                    <span className='notiCircle'>12</span>
                                </span>

                                <span className='Ulopts' onClick={this.signOut}>

                                    <i className='icon icon-share hdrs_ics'></i>

                                </span>

                            </div>


                        </div>

                    </div>

                </header>
                {/* header menu */}


                <nav className={'sidebar left ' + (this.props.sidebarState === 'left' ? 'toggle_left' : '')}>

                </nav>
                {/* left sidebar */}


                <nav className={'sidebar right ' + (this.props.sidebarState === 'right' ? 'toggle_right' : '')}>

                </nav>
                {/* right sidebar */}

            </React.Fragment>

        );
    }
}

const mapStateToProps = state => {
    return {
        sidebarState: state.Reducer1.sidebarState

    };
};

const mapDispatchToProps = dispatch => {
    return {
        sideBarOpen: (dataAb, pos) => dispatch({ type: actionType.SIDEBAROPEN, pos:pos, data:dataAb}),
        logOut:() => dispatch({type:actionType.LOGOUT}),
        closeSidebar: ()=> dispatch({type:actionType.SIDEBARCLOSE})
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
