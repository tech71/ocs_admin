import React, { Component } from 'react';
import Draggable from 'react-draggable';
import axios from 'axios';
import html2canvas from 'html2canvas';
import { $ } from 'jquery';
import ReactHtmlParser from 'react-html-parser';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { pdfData } from '../../store/actions';
const postData = (data) => {
    return new Promise((resolve, reject) => {
        fetch('http://localhost/girRepo2/finance-planner/back-end/ocr/index.php', {
            method: 'POST',
            body: data
        }).then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
            }).catch((error) => {
                reject(error);
                console.error(error);
            });
    });
}

class PdfViewer extends Component {


    constructor(props) {
        super(props);
        this.myRef = React.createRef()
        this.state = {
            zoomState: 0,
            content: null,
            pdf: null,
            documentsList: [],
            selectedDocument: 1,
            pageLength: null,
            selectedPage: 1,
            loading: false,
            boxSize: {
                x: 50,
                y: 20,
            },
            left: 0,
            top: 0,
            x: 0,
            y: 0,
            sourceX: '',
            sourceY: ''

        }
    }

    // componentWillReceiveProps(nextProps){
    //   let data = nextProps.contents.file_contents;
    //   console.log(data[0].contents.length);
    //   this.setState({
    //       content: data[0].contents[0],
    //       pdf: data,
    //       documentsList: data,
    //       pageLength: data.length
    //     }, () =>this.setState({loading:false }))
    //
    // }
    componentDidUpdate(props) {
        if (props.pdfData != this.props.pdfData  ) {

            this.getPosition();
        }
    }
    getPosition = () => {
      if(document.getElementById('invoice_amount') !=null){
        document.getElementById('selector').style.display='block';
        let invoice_amount = document.getElementById('invoice_amount').getBoundingClientRect();
        var original = document.getElementById('original');
        this.setState({ left: invoice_amount.left - original.getBoundingClientRect().left, top: invoice_amount.top - original.getBoundingClientRect().top });
      }
      else{
        document.getElementById('selector').style.display='none';

      }
    }
    screenshot = () => {

        var original = document.getElementById('original');

        html2canvas(original, {
            x: this.state.x + original.getBoundingClientRect().left,
            y: this.state.y + original.getBoundingClientRect().top + window.pageYOffset,
            width: this.state.boxSize.x,
            height: this.state.boxSize.y,
            removeContainer: true,
            canvas: null
        }).then((canvas) => {

            var img = canvas.toDataURL("image/png");
            const formData = new FormData();
            formData.append('data', img);
            postData(formData)
                .then((response) => {
                    if (response.status) {
                        this.setState({ convert_text: response.data });
                    }

                });
        });
    }



    componentDidMount() {
        this.res();
    }

    res = () => {
        let inputArea;
        var mousePosition;
        var offset = [0, 0];
        var isDown = false;

        var selector = document.getElementById('selector');
        var result = document.getElementsByClassName('container')[0];
        var textDiv = document.getElementById('#original');

        selector.addEventListener('mousedown', (e) => {
            isDown = true;
            offset = [
                selector.offsetLeft - e.clientX,
                selector.offsetTop - e.clientY,
            ];
        });

        document.addEventListener('mouseup', (event) => {
            isDown = false;
            mousePosition = {
                x: event.clientX,
                y: event.clientY
            };
            this.setState({ x: selector.offsetLeft, y: selector.offsetTop });
        });

        document.addEventListener('mousemove', (event) => {
            if (isDown) {
                mousePosition = {
                    x: event.clientX,
                    y: event.clientY

                };
                selector.style.left = (mousePosition.x + offset[0]) + 'px';
                selector.style.top = (mousePosition.y + offset[1]) + 'px';
            }
        }, true);
    }



    zoomHandler = (key) => {
        const zoomState = this.state.zoomState;
        if (key === 'zoomIn') {
            if (zoomState !== 100) {
                this.setState({ zoomState: zoomState + 10 })
            }

        }
        else {
            if (zoomState !== 0) {
                this.setState({ zoomState: zoomState - 10 })
            }
            if (zoomState === 10) {
                this.myRef.current.scrollTo(0, 0);
            }
        }
    }


    pageChangeHandler = (key) => {
        const pdf = this.state.pdf;
        const selectedPage = this.state.selectedPage;
        const selectedIndex = selectedPage - 1;
        this.myRef.current.scrollTo(0, 0);

        if (key == 'prev') {
            if (selectedIndex > 0) {
                this.setState({
                    content: pdf[selectedIndex - 1].contents,
                    selectedPage: selectedPage - 1,
                    selectedDocument: 1,
                    zoomState: 0
                },()=>  this.getPosition())
            }
        }
        if (key === 'next') {
            if (selectedPage < pdf.length) {
                this.setState({

                    content: pdf[selectedIndex + 1].contents,
                    selectedPage: selectedPage + 1,
                    selectedDocument:selectedPage + 1,
                    zoomState: 0
                },()=>  this.getPosition())
            }
        }
    }

    documentChangeHandler = (key) => {
        const documentsList = this.state.documentsList;
        this.myRef.current.scrollTo(0, 0);
        if (this.state.selectedDocument !== key + 1) {
            if (key <= documentsList.length) {
                this.setState({
                    pdf: documentsList[key],
                    content: documentsList[key].contents[0],
                    selectedDocument: key + 1,
                    selectedPage: key + 1,
                    zoomState: 0
                },()=>  this.getPosition())
            }
        }
        else {
            this.setState({
                selectedPage: 1,
                content: documentsList[key].contents[0],
                zoomState: 0
            },()=>  this.getPosition())
        }


    }

    componentWillReceiveProps(props){

        if (typeof (props.pdfData) != 'undefined') {

            let data = props.pdfData.file_contents;
            let content = data[0].contents[0];
            let pdf = data;
            let documentsList = data;
            let pageLength = data.length;

            this.setState({
                content:content,
                pdf:pdf,
                documentsList:documentsList,
                pageLength:pageLength
            })

        }


    }


    render() {

        return (

            <React.Fragment>



                <button onClick={this.screenshot}> tak scrin </button>
                <input name="result" readOnly={true} value={this.state.convert_text} />

                <div className='pdf_box mr_t_15'>
                    <div className='sidePart'>
                        <ul className='file_ul'>
                            {this.state.documentsList.map((docs, i) => {
                                return (
                                    <li
                                        key={i}
                                        className={this.state.selectedDocument == (i + 1) ? 'active' : ' '}
                                        onClick={() => this.documentChangeHandler(i)}
                                    >
                                        <h6><strong>Document Title</strong></h6>
                                        <i className='icon icon-file-icons'></i>
                                    </li>
                                );
                            })}
                        </ul>
                    </div>

                    <div className='pdf_part'>

                        <div className='pdfContentARea' ref={this.myRef}>
                            <Draggable
                                disabled={this.state.zoomState === 0}
                                position={(this.state.zoomState === 0 ? { x: 0, y: 0 } : null)}
                            >
                                <div className='dragArea' style={{ cursor: this.state.zoomState > 0 ? 'grab' : 'default' }}>
                                    <div id="original"
                                        className='zoomArea'
                                        style={{ transform: 'scale(1.' + ((this.state.zoomState === 100) ? 99 : this.state.zoomState) + ')', background: '#f2f2ec' }}
                                    >

                                        {ReactHtmlParser(this.state.content)}


                                        {/* <h2>gdf</h2> */}
                                    </div>

                                    <div className="container" ></div>
                                    <div id="selector" style={{ left: this.state.left + 'px', top: this.state.top + 'px', height: this.state.boxSize.y + 'px', width: this.state.boxSize.x + 'px' }}></div>
                                </div>
                            </Draggable>
                        </div>


                        <div className='footerPdf'>

                            <div className='zoomPanel'>
                                <i className={'icon icon-add2-ie ' + (this.state.zoomState === 100 ? 'disable' : ' ')} onClick={() => this.zoomHandler('zoomIn')}></i>
                                <span>{this.state.zoomState}%</span>
                                <i className={'icon icon-remove2-ie ' + (this.state.zoomState === 0 ? 'disable' : ' ')} onClick={() => this.zoomHandler('zoomOut')}></i>
                            </div>

                            <div className='paginationPdf'>
                                <span className='pg__'>Pages</span>
                                <i
                                    className={'icon icon-arrow-1-left arrws ' + (this.state.selectedPage == 1 ? ' disable' : ' ')}
                                    onClick={() => this.pageChangeHandler('prev')}
                                ></i>
                                <div className='pg_num'>
                                    {this.state.selectedPage} of {this.state.pageLength}
                                </div>
                                <i
                                    className={'icon icon-arrow-1-right arrws ' + (this.state.pageLength == this.state.selectedPage ? ' disable' : ' ')}
                                    onClick={() => this.pageChangeHandler('next')}
                                ></i>
                            </div>

                        </div>



                    </div>



                </div>

            </React.Fragment>

        );
    }
}
// + (this.state.pdf.contents.length == this.state.selectedPage? ' disable':' ')



const mapStateToProps = state => {
    return {
        sidebarState: state.Reducer1.sidebarState,
        invoice_details: state.InvoiceReducer.invoicedetails,
        pdfData: state.InvoiceReducer.pdfData
    };
};


export default connect(mapStateToProps)(PdfViewer);
