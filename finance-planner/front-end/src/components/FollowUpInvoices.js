import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Select from 'react-select-plus';
import ReactTable from "react-table";
import AuthWrapper from '../hoc/AuthWrapper';
import { connect } from 'react-redux';
import  {followUpInvoices} from '../store/actions';

class FollowUpInvoices extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterVal: ''
        }
    }

    componentWillMount() {
      this.props.followUpInvoices();
     }
    render() {
        const { followUpInvoice } =   this.props;
          const hdrColumn = [
            {

                Header: (props) =>
                    <label className='cstmChekie clrTpe2'>
                        <input type="checkbox" className=" "  />
                        <div className="chkie"></div>
                    </label>,
                accessor: "",
                width: 70,
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <label className='cstmChekie clrTpe3'>
                        <input type="checkbox" className=" "  />
                        <div className="chkie"></div>
                    </label>,

            },
            { Header: 'OCS ID', accessor: 'ocisid' },
            { Header: 'NDIS ID', accessor: 'ndisid' },
            { Header: 'Last Name', accessor: 'lastname' },
            { Header: 'First Name', accessor: 'firstname' },
            { Header: 'Invoice No.', accessor: 'invoice' },
            { Header: 'Due Date', accessor: 'duedate' },
            { Header: 'Invoice Total', accessor: 'total' },
            { Header: 'Biller', accessor: 'biller' },
            {
                Header: 'Status',
                accessor: 'status',
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <div className='stat_grp'>
                    {
                      (props.original.status === 'duplicate') ? <span className='status duplicate'>Duplicate</span>
                      :
                      (props.original.status === 'inQueue') ? <span className='status inQueue'>In Queue</span>
                      :
                      (props.original.status === 'followUp')? <span className='status followUp'>Follow Up</span>
                      :' '  }

                    </div>
            },
            {
                Header: '',
                accessor: 'id',
                sortable: false,
                filterable: false,
                width:50,
                Cell: (props) =>
                   <div>
                       <Link to={'/followup/viewfollowup/'+props.value}>
                            <i className='icon icon-view see_ic'></i>
                       </Link>
                   </div>
            }

        ]

        const dashboardData = [
            {
                ocisid: '896574',
                ndisid: 'ND15874',
                lastname: 'Maccarthy',
                firstname: 'Rose',
                invoice: '5698741',
                duedate: '23/15/18',
                total: '$897457',
                biller: 'Telestra'
            },
            {
                ocisid: '896574',
                ndisid: 'ND15874',
                lastname: 'Maccarthy',
                firstname: 'Rose',
                invoice: '5698741',
                duedate: '23/15/18',
                total: '$897457',
                biller: 'Telestra'
            },
            {
                ocisid: '896574',
                ndisid: 'ND15874',
                lastname: 'Maccarthy',
                firstname: 'Rose',
                invoice: '5698741',
                duedate: '23/15/18',
                total: '$897457',
                biller: 'Telestra'
            },
            {
                ocisid: '896574',
                ndisid: 'ND15874',
                lastname: 'Maccarthy',
                firstname: 'Rose',
                invoice: '5698741',
                duedate: '23/15/18',
                total: '$897457',
                biller: 'Telestra'
            },
            {
                ocisid: '896574',
                ndisid: 'ND15874',
                lastname: 'Maccarthy',
                firstname: 'Rose',
                invoice: '5698741',
                duedate: '23/15/18',
                total: '$897457',
                biller: 'Telestra'
            }



        ]

        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];


        return (

            <AuthWrapper>

                        <div className=" back_col_cmn-">
                            <span onClick={() =>  window.history.back()} className="icon icon-back1-ie"></span>
                        </div>

                        <div className="main_heading_cmn-">
                            <h1>
                                <span>Follow Up Invoice</span>
                                <Link to='#'>
                                    <button className="btn hdng_btn cmn-btn1">Run Planner</button>
                                </Link>
                            </h1>
                        </div>

                        <div className='row pd_t_15'>
                            <div className='col-6'>

                                <div className='cstm_select search_filter brdr_slct'>

                                    <Select name="view_by_status "
                                        required={true} simpleValue={true}
                                        searchable={true} Clearable={false}
                                        placeholder="| Names, numbers, dates & status'"
                                        options={options}
                                        onChange={(e) => this.setState({ filterVal: e })}
                                        value={this.state.filterVal}

                                    />

                                </div>

                            </div>

                            <div className='col-3'>

                                <div className='cstm_select  slct_cstm2 bg_grey'>

                                    <Select name="view_by_status "
                                        simpleValue={true}
                                        searchable={false} Clearable={false}
                                        placeholder="Filter By Status"
                                        options={options}
                                        onChange={(e) => this.setState({ categoryVal: e })}
                                        value={this.state.categoryVal}

                                    />

                                </div>

                            </div>

                        </div>


                        <div className="data_table_cmn dashboard_Table pending_invTable text-center bor_top1 pd_tb_15" >
                            <ReactTable
                                data={followUpInvoice}
                                defaultPageSize={10}
                                minRows={2}
                                className="-striped -highlight"
                                previousText={<span className="icon icon-arrow-1-left previous"></span>}
                                nextText={<span className="icon icon-arrow-1-right next"></span>}
                                columns={hdrColumn}
                            />
                        </div>
                        {/* table comp ends */}

            </AuthWrapper>

        );
    }
}




const mapStateToProps = state => {
return {  followUpInvoice: state.InvoiceReducer.followUpInvoice

  }
};
export default connect(mapStateToProps,{followUpInvoices})(FollowUpInvoices);

// export default FollowUpInvoices;
