import React, { Component } from 'react';
import Select from 'react-select-plus';
import ReactTable from "react-table";
import AuthWrapper from '../hoc/AuthWrapper';
import DatePicker from "react-datepicker";
import { connect } from 'react-redux';
import  {list_audit_logs} from '../store/actions';
import moment from 'moment';
class AuditLogs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterVal: '',
            categoryVal:'',
            fromDate:'',
            toDate:'',
            loading:false,
            startDate: null,auditLogs:[]
        }
          this.reactTable = React.createRef();
    }
    fetchData = (state, instance) => {
      // function for fetch data from database
      this.setState({ loading: true });
      this.props.list_audit_logs(
          state.pageSize,
          state.page,
          state.sorted,
          state.filtered
      ).then(res => {

          this.setState({
              auditLogs: res,
              pages: 1,
              loading: false
          });

      });
  }

  searchData = (key, value) => {
        var srch_ary = { filterVal: this.state.filterVal,categoryVal: this.state.categoryVal,fromDate: this.state.fromDate,toDate: this.state.toDate };

        srch_ary[key] = value;
        this.setState(srch_ary);
        this.setState({filtered: srch_ary});
    }
    render() {

        const hdrColumn = [
            {

                Header: (props) =>
                    <label className='cstmChekie clrTpe2'>
                        <input type="checkbox" className=" "  />
                        <div className="chkie"></div>
                    </label>,
                accessor: "",
                width: 70,
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <label className='cstmChekie clrTpe3'>
                        <input type="checkbox" className=" "  />
                        <div className="chkie"></div>
                    </label>,

            },
            { Header: 'Audit Logs', accessor: 'title' },
            { Header: 'Date', accessor: 'created' },
            { Header: 'Category', accessor: 'category' },
            { Header: 'User', accessor: 'user' },
            {
                Header: 'Status',
                accessor: 'status',
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <div className='stat_grp'>
                        <span className='status dismiss'>View</span>
                        <span className='status dismiss'>Dismiss</span>
                    </div>
            }


        ]

        const dashboardData = [
            {
                log: 'Invoice moved to follow up',
                date: '11/29/2019',
                category: 'Category',
                user: 'GT0456',
            },
            {
                log: 'Invoice moved to follow up',
                date: '11/29/2019',
                category: 'Category',
                user: 'GT0456',
            },
            {
                log: 'Invoice moved to follow up',
                date: '11/29/2019',
                category: 'Category',
                user: 'GT0456',
            },
            {
                log: 'Invoice moved to follow up',
                date: '11/29/2019',
                category: 'Category',
                user: 'GT0456',
            }


        ]

        var options = [
            { value: '', label: '--Select--' },
            { value: 'Pending', label: 'Pending' },
            { value: 'Paid', label: 'Paid' },
            { value: 'Duplicate', label: 'Duplicate' },
            { value: 'followUp', label: 'followUp' }
        ];


        return (

            <AuthWrapper>

                        <div className=" back_col_cmn-">
                            <span onClick={() =>  window.history.back()} className="icon icon-back1-ie"></span>
                        </div>

                        <div className="main_heading_cmn-">
                            <h1>
                                <span>Audit Logs</span>

                            </h1>
                        </div>

                        <div className='row pd_t_15'>

                            <div className='col-xl-5 col-lg-7 col-7 mr_b_15_min'>

                                <div className='cstm_select search_filter brdr_slct '>
                                <input type="text" name="view_by_status" value={this.state.filterVal || ''} onChange={(e) => this.searchData( 'filterVal', e.target.value )}  />
                                  <button  type="submit"><span className="icon icon-search1-ie"></span></button>
                                  {
                                    // <Select name="view_by_status "
                                    //     required={true} simpleValue={true}
                                    //     searchable={true} Clearable={false}
                                    //     placeholder=""
                                    //     options={this.state.searchItems}
                                    //     onChange={(e) => this.setState('filterVal', e )}
                                    //     value={this.state.filterVal}
                                    //
                                    // />
}
                                </div>

                            </div>

                            <div className='col-xl-3 col-lg-5 col-5 mr_b_15_min'>

                                <div className='cstm_select  slct_cstm2 bg_grey'>

                                    <Select name="view_by_status "
                                        simpleValue={true}
                                        searchable={false} Clearable={false}
                                        placeholder="Status"
                                        options={options}
                                        onChange={(e) => this.searchData( 'categoryVal', e )}
                                        value={this.state.categoryVal}

                                    />

                                </div>

                            </div>

                            <div className='col-xl-2 col-lg-4 col-4'>
                                <div className='d-flex align-items-center'>
                                    <label className='pd_r_10'>From</label>
                                    <DatePicker

                                        className="csForm_control"
                                        selected={this.state.fromDate}
                                        value={this.state.fromDate}
                                        onChange={(e) => this.searchData( 'fromDate', moment(e).format("YYYY-MM-DD") )}
                                        dateFormat="DD/MM/YYYY"
                                        name="tempDueDate"
                                        placeholderText={'DD/MM/YY'}
                                    />
                                </div>
                            </div>

                            <div className='col-xl-2 col-lg-4 col-4'>
                                <div className='d-flex align-items-center'>
                                    <label className='pd_r_10'>To</label>
                                    <DatePicker

                                        className="csForm_control"
                                        selected={this.state.toDate}
                                        value={this.state.toDate}
                                        onChange={(e) => this.searchData( 'toDate', moment(e).format("YYYY-MM-DD") )}
                                        dateFormat="DD/MM/YYYY"
                                        name="tempDueDate"
                                        placeholderText={'DD/MM/YY'}
                                    />
                                </div>
                            </div>

                        </div>


                        <div className="data_table_cmn dashboard_Table pending_invTable text-center bor_top1 pd_tb_15" >
{
//                         <ReactTable
//      columns={hdrColumn}
//    data={this.props.auditLogs}
//
//    loading={this.state.loading}
//    onFetchData={this.fetchData}
//    className="-striped -highlight"
//    previousText={<span className="icon icon-arrow-1-left previous"></span>}
//    nextText={<span className="icon icon-arrow-1-right next"></span>}
//
// />
}
                            <ReactTable
                              //  ref={this.reactTable}
                                data={this.props.auditLogs}
                                onFetchData={this.fetchData}
                               defaultPageSize={10}

                                filtered={this.state.filtered}
                                minRows={2}
                                loading={this.state.loading}
                                className="-striped -highlight"
                                previousText={<span className="icon icon-arrow-1-left previous"></span>}
                                nextText={<span className="icon icon-arrow-1-right next"></span>}
                                columns={hdrColumn}
                                showPagination={true}
                            />
                        </div>
                        {/* table comp ends */}


            </AuthWrapper>

        );
    }
}


const mapStateToProps = state => {

return {  auditLogs: state.InvoiceReducer.auditLogs  }
};



export default connect(mapStateToProps,{list_audit_logs})(AuditLogs);
