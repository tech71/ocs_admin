import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import DatePicker from "react-datepicker";
import ReactTable from "react-table";
import Select from 'react-select-plus';
import moment from 'moment';
import AuthWrapper from '../hoc/AuthWrapper';
import Modal from './utils/Modal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { invoiceDetails, pdfData, loader, createDisputeNotes, getAllDisputeNotes,getOptionsCrmParticipantId} from '../store/actions';
import {getOptionsParticipant,getOptionsCompany} from '../service/common.js';
import PdfViewer from './utils/PdfViewer';


class ViewFollowUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterVal: '',
            startDate: null,
            processModal: false,
            invoicedetails:[], invoicedetails: [],
            disputeDetails: { reason:'', raised:'', provider:'', method:'', notes:'', invoice_id:this.props.match.params.id},
            countLength:''
        }
    }


    componentWillMount() {
      this.props.loader(true);
      this.props.getOptionsCrmParticipantId();
      this.props.invoiceDetails(this.props.match.params.id)
          .then(json => {
              this.setState({ invoicedetails: json.data },()=>this.props.loader(false));
              this.props.pdfData(json.data);
          });
      this.props.getAllDisputeNotes();
     }

   selectChange = (selectedOption, fieldname) => {
   var state = this.state.invoicedetails;
   state[fieldname] = selectedOption;
   state[fieldname + '_error'] = false;
   this.setState(state);
   }
   selectChanges = (selectedOption, fieldname) => {

     var state = this.state.invoicedetails;
     state[fieldname] = selectedOption;
     state['participant_ocs_email_acc']= selectedOption.email;
     state['participant_lastname']= selectedOption.lastname;
     state['participant_ocs_id']={"label":selectedOption.value,'value':selectedOption.value};
     state[fieldname + '_error'] = false;

     this.setState(state);

   }
     selectSearch = (selectedOption, fieldname) => {

       console.log(selectedOption);
       // selectedOption = selectedOption[0]
     var state = this.state.invoicedetails;
     state[fieldname] =selectedOption.name;
     state['participant_ocs_email_acc']= selectedOption.email;
     state['participant_lastname']= selectedOption.lastname;
     state['participant_firstname']= {"label":selectedOption.value,'value':selectedOption.label};
     state['participant_ocs_id']= {"label":selectedOption.label,'value':selectedOption.label};
     state[fieldname + '_error'] = false;

     this.setState(state);

   }
  componentDidCatch(error) {
      console.log(error);
  }

  onHandleDispute = (e) => {
    e.preventDefault();
    this.props.createDisputeNotes(this.state.disputeDetails).then(res => {
      if(res.status){
            this.setState({ disputeDetails: { reason:'', raised:'', provider:'', method:'', notes:'', invoice_id:this.props.match.params.id}});
      }
    });

  }

  handleShareholderNameChange = (stateKey, fieldtkey, fieldValue) => {
    var state = {};
    var tempField = {};

    if(fieldtkey=='notes'){
        var fieldLength = fieldValue.length.toString();
        this.setState({countLength:fieldLength});
        if(fieldLength>=1000){
          var str = fieldValue.substring(0, 1000);
          this.setState({countLength:str.length.toString()});
          fieldValue=str;
        }
    }
    var List = this.state[stateKey];
    List[fieldtkey] = fieldValue
    state[stateKey] = List;
    this.setState(state);
  }

  selectContactMethod = (selectedOption, fieldname) => {
    var state = this.state.disputeDetails;
    state[fieldname] = {'label':selectedOption,'value':selectedOption};
    state[fieldname + '_error'] = false;
    this.setState(state);
  }

  selectDisputeRaised = (selectedOption, fieldname) => {
  var state = this.state.disputeDetails;
  state[fieldname] = selectedOption;
  state[fieldname + '_error'] = false;
  this.setState(state);
  }




    render() {
      let participantId = (typeof(this.props.CrmParticipantId)!='undefined')? this.props.CrmParticipantId:[{}];
      var options = [
        { label: 'Email Send', value: 'Email Send'},
        { label: 'Phone Call', value: 'Phone Call'},
        { label: 'Post Send', value: 'Post Send'},
        { label: 'Message Send', value: 'Message Send'}
      ];

      const invoice_details = this.state.invoicedetails;
      let company_name_error = (typeof(invoice_details.validate)!='undefined')? invoice_details.validate.company_name.error:false;
      let firstname_error = (typeof(invoice_details.validate)!='undefined')? invoice_details.validate.participant_firstname.error:false;
      let invoice_number_error = (typeof(invoice_details.validate)!='undefined')? invoice_details.validate.invoice_number.error:false;
      let invoice_amount_error = (typeof(invoice_details.validate)!='undefined')? invoice_details.validate.invoice_amount.error:false;





        const hdrColumn = [
            {

                Header: (props) =>
                    <label className='cstmChekie clrTpe2'>
                        <input type="checkbox" className=" " />
                        <div className="chkie"></div>
                    </label>,
                accessor: "",
                width: 70,
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <label className='cstmChekie clrTpe3'>
                        <input type="checkbox" className=" " />
                        <div className="chkie"></div>
                    </label>,

            },
            { Header: 'Type', accessor: 'type' },
            { Header: 'Due Date', accessor: 'date' },
            { Header: 'User', accessor: 'user' },
            {
                expander: true,
                Header: () => <strong></strong>,
                width: 40,
                headerStyle: { border: "0px solid #fff" },
                Expander: ({ isExpanded, ...rest }) =>
                    <div className="rec-table-icon">
                        {isExpanded
                            ? <i className="icon icon-arrow-down icn_ar1"></i>
                            : <i className="icon icon-arrow-right icn_ar1"></i>}
                    </div>,
                style: {
                    cursor: "pointer",
                    fontSize: 25,
                    padding: "0",
                    textAlign: "center",
                    userSelect: "none"
                },

            }


        ]

        let alldisputeNotesData = this.props.allDisputeNotes;
        let alldisputeErrors = this.props.disputeErrorMsg;

        const subComponentDataMapper = row => {
          let data = row.original;
         return (
           <div className='dispSubComp__'>
               <p>{data.notes}</p>
               <i className='icon icon-view2-ie vw_dips'></i>
           </div>
          );
        }


        return (

            <AuthWrapper>

              <div className=" back_col_cmn-">
                    <Link to='/pendinginvoices'><span className="icon icon-back1-ie"></span></Link>
                </div>

                <div className="main_heading_cmn-">
                    <h1>
                        <span>Follow Up Invoice: #4575</span>

                            <button onClick={()=>this.setState({resolveFollowModal:true})} className="btn hdng_btn cmn-btn4">Follow Up</button>

                    </h1>
                </div>

                <div className='row'>

                    <div className='col-xl-7 col-lg-12 col-12'>
                      {  // <div className='pdf_box mr_tb_15'>
                      // </div>
                    }
                          <PdfViewer />
                    </div>

                    <div className='col-xl-5 col-lg-12 col-12'>

                        <div className='fieldArea pd_tb_15'>


                            <div className='bor_tb  pd_tb_10 mr_t_15' >
                                <strong>Account Details</strong>
                            </div>

                            <div className='row pd_tb_15'>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Invoice Date</label>
                                        <div className='datewid_100'>
                                            <DatePicker
                                                className="csForm_control no_bor"
                                                selected={moment(invoice_details.invoice_date, 'DD/MM/YYYY').valueOf()}
                                                value={invoice_details.invoice_date}
                                                onChange={(e) =>this.selectChange(moment(e).format("DD/MM/YYYY"),'invoice_date')}
                                                dateFormat="DD/MM/YYYY"
                                                name="tempDueDate"
                                                placeholderText={'DD/MM/YY'}
                                            />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Due Date</label>
                                        <div className='datewid_100'>
                                            <DatePicker
                                                className="csForm_control  no_bor"
                                                selected={moment(invoice_details.due_date, 'DD/MM/YYYY').valueOf()}
                                                value={invoice_details.due_date}
                                                onChange={(e) =>this.selectChange(moment(e).format("DD/MM/YYYY"),'due_date')}
                                                dateFormat="DD/MM/YYYY"
                                                name="tempDueDate"
                                                placeholderText={'DD/MM/YY'}
                                            />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Company Name</label>
                                        <div className='csaddOn_fld1__ '>
                                        {//<input type="text" className="csForm_control no_bor" name="" value={invoice_details.company_name || ''} onChange={(e) =>this.selectChange(e.target.value,'company_name')}/>
                                      }  <Select.Async
                                              cache={false}
                                              clearable={false}
                                              name="company_name" required={true}
                                              value={invoice_details.company_name}
                                              loadOptions={getOptionsCompany}
                                              placeholder='Search'
                                              onChange={(e)=>this.selectChanges(e,'company_name')}
                                          />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Matches Xero Supplier</label>
                                        <div className={(company_name_error)?'csaddOn_fld1__ errorIc':'csaddOn_fld1__ checkedIc1'}>
                                            <input type="text" className={(company_name_error)?"csForm_control no_bor errorIn2":"csForm_control no_bor completed"} name="" />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Invoice Amount</label>
                                        <div className='csaddOn_fld1__'>
                                            <input type="text" className="csForm_control no_bor" name="" value={invoice_details.invoice_amount || ''} onChange={(e) =>this.selectChange(e.target.value,'invoice_amount')}/>
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Amount Paid</label>
                                        <div className='csaddOn_fld1__'>
                                            <input type="text" className="csForm_control no_bor" name="" readOnly/>
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Account Number</label>
                                        <div className='csaddOn_fld1__'>
                                            <input type="text" className="csForm_control no_bor" name="" onChange={(e) =>this.selectChange(e.target.value,'account_no')}/>
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>PO Number</label>
                                        <div className='csaddOn_fld1__ notAvail'>
                                            <input type="text" className="csForm_control " name="" value='NA' readOnly />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-12'>

                                    <div className='inpLabel1'><strong>Payment Type</strong></div>
                                    <ul className='py_typeUL'>
                                        <li className='active'>Bank</li>
                                        <li>B Pay</li>
                                        <li>AUS Post</li>
                                    </ul>

                                </div>


                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Biller Code</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" className="csForm_control no_bor" name="" />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Reference Number</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" className="csForm_control no_bor" name="" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                            {/* row ends */}

                            <div className='bor_tb  pd_tb_10 mr_t_15' >
                                <strong>Member Details</strong>
                            </div>

                            <div className='row pd_tb_15' >



                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>First Name</label>
                                        <div className='csaddOn_fld1__ '>
                                        {  // <input type="text" className="csForm_control no_bor" name="" value={invoice_details.participant_firstname || ''}/>
                                      }
                                      <Select.Async
                                            cache={false}
                                            clearable={false}
                                            name="participant_firstname" required={true}
                                            value={invoice_details.participant_firstname}
                                            loadOptions={getOptionsParticipant}
                                            placeholder='Search'
                                            onChange={(e)=>this.selectChanges(e,'participant_firstname')}
                                        />
                                        </div>
                                    </div>
                                </div>


                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Last Name</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" className="csForm_control no_bor" name="" readOnly value={invoice_details.participant_lastname || ''}/>
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Matched Member</label>
                                        <div className={(firstname_error)?'csaddOn_fld1__ errorIc':'csaddOn_fld1__ checkedIc1'}>
                                            <input type="text" className={(firstname_error)?"csForm_control no_bor errorIn2":"csForm_control no_bor completed"} name="" />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>OCS ID</label>
                                        <div className='csaddOn_fld1__ checkedIc1'>
                                        <Select name="view_by_status "
                                        required={true} simpleValue={false}
                                        searchable={true} Clearable={false}
                                        placeholder=""
                                        options={participantId}
                                        onChange={(e) => this.selectSearch(e,'participant_ocs_id')}
                                        value={invoice_details.participant_ocs_id}

                                    />
                                      {  // <input type="text" className="csForm_control no_bor" name="" value={invoice_details.participant_ocs_id || ''}/>
                                    }
                                        </div>
                                    </div>
                                </div>


                            </div>
                            {/* row ends */}

                        </div>


                    </div>
                    {/* col-6 ends */}

                </div>
                {/* row ends */}


                <div className='row '>

                    <div className='col-xl-7 col-lg-12 col-12'>

                        <div className='disputeDetl_box'>
                          <b className="alert-success">{(this.props.disputeSuccessMsg)?this.props.disputeSuccessMsg:''}</b>
                        {// <b className="alert-danger">{(this.props.disputeErrorMsg)?this.props.disputeErrorMsg:''}</b><br/>
                        }
                            <div className='bor_tb  pd_tb_10 mr_t_15' >
                                <strong>Dispute Details   </strong>
                            </div>


                            <div className='row pd_tb_15'>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Reason for Dispute</label>
                                        <div className='csaddOn_fld1__'>
                                            <input type="text" name="d_reason"
                                            onChange={(evt) => this.handleShareholderNameChange('disputeDetails', 'reason', evt.target.value)}
                                            value={this.state.disputeDetails.reason}
                                            className="csForm_control no_bor" name="" />
                                        </div>
                                        <p className="alert-danger">{(alldisputeErrors  && alldisputeErrors.reason)?alldisputeErrors.reason:''}</p>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Dispute Raised</label>
                                        <div className='csaddOn_fld1__'>

                                        <DatePicker
                                            className="csForm_control  no_bor"
                                            dateFormat="DD/MM/YYYY"
                                            utcOffset={0}
                                            value={this.state.disputeDetails.raised}
                                            onChange={(e) =>this.selectDisputeRaised(moment(e).format("DD/MM/YYYY"),'raised')}
                                            dateFormat="DD/MM/YYYY"
                                            name="tempDueDate"
                                            placeholderText={'DD/MM/YYYY'}
                                        />
                                        <p className="alert-danger">{(alldisputeErrors  && alldisputeErrors.raised)?alldisputeErrors.raised:''}</p>

                                        {  // <input type="text"
                                            // onChange={(evt) => this.handleShareholderNameChange('disputeDetails', 'raised', evt.target.value)}
                                            // className="csForm_control no_bor" name="" />
                                          }
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Contact Name at Provider</label>
                                        <div className='csaddOn_fld1__'>
                                            <input type="text"
                                            value={this.state.disputeDetails.provider}
                                            onChange={(evt) => this.handleShareholderNameChange('disputeDetails', 'provider', evt.target.value)}
                                            className="csForm_control no_bor" name="" />
                                            <p className="alert-danger">{(alldisputeErrors  && alldisputeErrors.provider)?alldisputeErrors.provider:''}</p>
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Contact Method</label>
                                        <div className='csaddOn_fld1__'>
                                            <Select name="view_by_status "
                                                required={true} simpleValue={true}
                                                searchable={true} Clearable={false}
                                                placeholder="| Email, Phone, Post & Message"
                                                options={options}
                                                onChange={(e) => this.selectContactMethod(e,'method')}
                                                value={this.state.disputeDetails.method}

                                            />
                                            <p className="alert-danger">{(alldisputeErrors  && alldisputeErrors.method)?alldisputeErrors.method:''}</p>
                                            {// <input type="text"
                                            // onChange={(evt) => this.handleShareholderNameChange('disputeDetails', 'method', evt.target.value)}
                                            // className="csForm_control no_bor" name="" />
                                           }
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-12 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Dispute Notes</label>
                                        <div className='csaddOn_fld1__'>
                                            <textarea className='csForm_control no_bor'
                                            onChange={(evt) => this.handleShareholderNameChange('disputeDetails', 'notes', evt.target.value)}
                                            value={this.state.disputeDetails.notes}
                                            ></textarea>
                                            (Maximum characters: 1000) Count: {(this.state.countLength)?this.state.countLength:'0'}
                                        </div>
                                        <p className="alert-danger">{(alldisputeErrors  && alldisputeErrors.notes)?alldisputeErrors.notes:''}</p>
                                    </div>
                                </div>



                                <div className='col-lg-12 col-12 text-right'>

                                    <input type='button' className="btn cmn-btn2 dis_btns"  onClick={this.onHandleDispute}   value='Add Note' />
                                    <input type='button' className="btn cmn-btn1 dis_btns" value='Resolve' />


                                </div>




                            </div>


                        </div>
                        {/* disputeDetl_box ends */}

                    </div>

                    <div className='col-xl-5 col-lg-12 col-12'>

                        <div className='disNotBox1__'>
                            <div className='hdngDs'>Dispute Notes</div>
                            <div className="data_table_cmn text-center dispute_table " >
                                <ReactTable
                                    data={alldisputeNotesData}
                                    showPagination={false}
                                    defaultPageSize={10}
                                    minRows={2}
                                    className="-striped -highlight"
                                    previousText={<span className="icon icon-arrow-1-left previous"></span>}
                                    nextText={<span className="icon icon-arrow-1-right next"></span>}
                                    columns={hdrColumn}
                                    SubComponent={subComponentDataMapper}
                                />
                            </div>
                            {/* table comp ends */}

                        </div>

                    </div>


                </div>
                {/* row ends */}


                <Modal show={this.state.resolveFollowModal} >

                    <div className='modalDialog_mini cstmDialog' >
                        <div className='mr_b_15 hdngModal'>
                            <h5 className='hdngCont'><strong>Resolve Follow Up </strong></h5>
                            <i className='icon icon-close2-ie closeIc' onClick={() => this.setState({resolveFollowModal:false})}></i>
                        </div>

                        <div className='text-right mr_t_60'>
                            <button className='btn cmn-btn2 dis_btns2 '>
                                <span>
                                    Deny Invoice
                                    <i className='icon icon-close2-ie'></i>
                                </span>
                            </button>
                            <button className='btn cmn-btn1 dis_btns2'>
                                <span>
                                    Pay
                                    <i className='icon icon-accept-approve1-ie'></i>
                                </span>
                            </button>

                        </div>
                    </div>

                </Modal>


            </AuthWrapper>

        );
    }
}




const mapStateToProps = state => {
    return {
        invoice_details: state.InvoiceReducer.invoicedetails,
        pdfData: state.InvoiceReducer.data,
        disputeSuccessMsg: state.InvoiceReducer.disputeSuccess,
        disputeErrorMsg: state.InvoiceReducer.disputeError,
        allDisputeNotes: state.InvoiceReducer.disputeNotes,
        CrmParticipantId: state.InvoiceReducer.CrmParticipantId
    }
};
const mapDispatchtoProps = (dispatch) => bindActionCreators({
    invoiceDetails, pdfData, loader, createDisputeNotes, getAllDisputeNotes,getOptionsCrmParticipantId
}, dispatch)
export default connect(mapStateToProps, mapDispatchtoProps)(ViewFollowUp);
