import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Bar } from 'react-chartjs-2';
import Select from 'react-select-plus';
import ReactTable from "react-table";
import AuthWrapper from '../hoc/AuthWrapper';
import { connect } from 'react-redux';
import * as CT from '../hoc/Color';
import  {dashboardCountData,get_members_summary,get_processed_invoice,fetch_invoice,get_graph_data} from '../store/actions';

class Dashboard extends Component {


    constructor(props) {
        super(props);
        this.state = {
            startDate: null
        };

    }

    componentWillMount() {
      this.props.dashboardCountData();
      this.props.get_members_summary();
      this.props.get_processed_invoice();
      this.props.get_graph_data();
     }

    render() {
          const  {dashboardCount,membersData,processedInvoice,graphData} =   this.props;

        const Graphdata = {
            labels: ['July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
            datasets: [{
               data: typeof(graphData)!= 'undefined' &&  typeof(graphData.graph_qty)!='undefined'?graphData.graph_qty:[],
               backgroundColor: [CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1],

            }]

        };

        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        const hdrColumn = [
            {Header:'OCS ID', accessor:'ocisid'},
            {Header:'Member Name', accessor:'member_name'},
            {Header:'Available Funds', accessor:'funds'},
            {Header:'Pending', accessor:'pending',Cell: (props) =><span className='tb_vals1_'>{props.original.pending}</span>},
            {Header:'Follow Up', accessor:'follow_up',Cell: (props) =><span className='tb_vals1_'>{props.original.follow_up}</span>},
            {Header:'Duplicate', accessor:'duplicate',Cell: (props) =><span className='tb_vals1_'>{props.original.duplicate}</span>},
            {Header:'Paid', accessor:'paid',Cell: (props) =><span className='tb_vals1_'>{props.original.paid}</span>},

        ]

        const dashboardData = [
            {
                id: '165498',
                name: 'Adam Maccarthy',
                funds:'$15256',
                pending:'10',
                followUp:'0',
                duplicate:'2',
                paid:'52'

            },
            {
                id: '165498',
                name: 'Adam Maccarthy',
                funds:'$15256',
                pending:'5',
                followUp:'15',
                duplicate:'20',
                paid:'100'

            },
            {
                id: '165498',
                name: 'Adam Maccarthy',
                funds:'$15256',
                pending:'52',
                followUp:'25',
                duplicate:'85',
                paid:'40'

            },
            {
                id: '165498',
                name: 'Adam Maccarthy',
                funds:'$15256',
                pending:'17',
                followUp:'5',
                duplicate:'10',
                paid:'512'

            },
            {
                id: '165498',
                name: 'Adam Maccarthy',
                funds:'$15256',
                pending:'15',
                followUp:'65',
                duplicate:'97',
                paid:'52'

            }


        ]


        return (

            <AuthWrapper>
                        <div className=" back_col_cmn-">
                            <Link to={'#'}><span className="icon icon-back1-ie"></span></Link>
                        </div>

                        <div className="main_heading_cmn-">
                            <h1>
                                <span>Dashboard</span>
                                <Link to='#'>
                                    <button className="btn hdng_btn cmn-btn1" onClick={()=>this.props.fetch_invoice()}>Fetch Invoice <i className='icon icon-add2-ie'></i></button>
                                </Link>
                            </h1>
                        </div>

                        <div className='invStatus bor_bot1'>
                            Invoices Processed
                        </div>

                        <div className='compoBoxDiv__ d-flex row'>

                            <div className='col-sm-12'>
                                <div className='compBox'>
                                    <p className='grpMes cmn_clr1'><strong>Qty</strong></p>
                                    <div className='grph_dv'>
                                        <Bar data={Graphdata} height={350} legend={null}
                                            options={{
                                                maintainAspectRatio: false
                                            }}
                                        />
                                    </div>
                                    <p className='grpMes'><strong>Month</strong></p>


                                    <div className='row'>

                                        <div className='col-md-3'></div>
                                        <div className='col-md-6'>
                                            <div className='grph_optGrp d-flex align-items-center justify-content-center'>
                                                <span><strong>Select:</strong></span>
                                                <button className='cmn-btn1 pd_bts active'>Paid</button>
                                                <button className='cmn-btn1 pd_bts'>Duplicate</button>
                                                <button className='cmn-btn1 pd_bts'>Automated</button>
                                            </div>
                                        </div>
                                        <div className='col-md-3'>
                                            <ul className='grph_opts2'>
                                                <li className='active'>Quantity</li>
                                                <li>Dollar Value</li>
                                            </ul>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                        {/* compoBoxDiv__ ends */}


                        <div className='compoBoxDiv__ d-flex row justify-content-center'>

                            <div className='col-lg-4'>
                                <div className='compBox text-center colored'>
                                    <p><strong>Invoices Processed last 24 hrs</strong></p>
                                    <h1 className='amts'>{typeof(processedInvoice)!='undefined'?processedInvoice.count:0}</h1>

                                </div>
                            </div>


                        </div>
                        {/* compoBoxDiv__ ends */}

                        <div className='compoBoxDiv__ d-flex row'>

                            <div className='col-lg-4'>
                                <div className='compBox text-center'>
                                    <p><strong>Pending Invoices</strong></p>
                                    <h1 className='amts'>{typeof(dashboardCount)!='undefined'?dashboardCount.Pending.count:0}</h1>
                                    <Link to={'/pendinginvoices'}>
                                        <button className='cmn-btn1 vw_btns'>Start</button>
                                    </Link>
                                </div>
                            </div>

                            <div className='col-lg-4'>
                                <div className='compBox text-center'>
                                    <p><strong>Duplicate Invoices</strong></p>
                                    <h1 className='amts'>{typeof(dashboardCount)!='undefined'?dashboardCount.Duplicate.count:0}</h1>
                                    <Link to={'/duplicateinvoices'}>
                                        <button className='cmn-btn1 vw_btns'>Start</button>
                                    </Link>
                                </div>
                            </div>

                            <div className='col-lg-4'>
                                <div className='compBox text-center'>
                                    <p><strong>Invoices to follow up</strong></p>
                                    <h1 className='amts'>{typeof(dashboardCount)!='undefined'?dashboardCount.Followup.count:0}</h1>
                                    <Link to={'/followup'}>
                                        <button className='cmn-btn1 vw_btns'>Start</button>
                                    </Link>
                                </div>
                            </div>


                        </div>
                        {/* compoBoxDiv__ ends */}

                        <h1 className='hdng2'><strong>Summary of Invoices by Members</strong></h1>

                        <div className='row'>

                            <div className='col-7'>

                                <div className='cstm_select search_filter brdr_slct'>

                                    <Select
                                        name="view_by_status "
                                        required={true} simpleValue={true}
                                        searchable={true} Clearable={false}
                                        placeholder="| Search for Users, Dates and Categories"
                                        options={options}
                                        onChange={(e) => this.setState({ filterVal: e })}
                                        value={this.state.filterVal}
                                    />

                                </div>

                            </div>




                        </div>
                        {/* row ends */}

                        <div className="data_table_cmn dashboard_Table text-center">
                            <ReactTable
                                data={membersData}
                                defaultPageSize={10}
                                minRows={1}
                                className="-striped -highlight"
                                previousText={<span className="icon icon-arrow-1-left previous"></span>}
                                nextText={<span className="icon icon-arrow-1-right next"></span>}
                                columns={hdrColumn}
                            />
                        </div>
                        {/* table comp ends */}

            </AuthWrapper>

        );
    }
}




const mapStateToProps = state => {
// console.log(state);
return {
  dashboardCount: state.Reducer1.countData,
  membersData: state.Reducer1.summary,
  processedInvoice: state.Reducer1.processedInvoice,
  graphData:state.Reducer1.graphdata
  }
};
const mapDispatchToProps = {
  dashboardCountData,
  get_members_summary,
  get_processed_invoice,
  get_graph_data
}

  export default connect(mapStateToProps,{dashboardCountData,get_members_summary,get_processed_invoice,fetch_invoice, get_graph_data})(Dashboard);
// export default Dashboard;
