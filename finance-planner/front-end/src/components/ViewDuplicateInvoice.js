import React, { Component } from 'react';
import { Link,Redirect } from 'react-router-dom';
import DatePicker from "react-datepicker";
import moment from 'moment';
import NotifyPopUp from './utils/NotifyPopUp';
import AuthWrapper from '../hoc/AuthWrapper';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { invoiceDetails, pdfData, loader,invoice_status } from '../store/actions';
import PdfViewer from './utils/PdfViewer';


class ViewDuplicateInvoice extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterVal: '',
            startDate: null,
            processModal: false, invoicedetails: [],
            redirect:false
        }
    }
    componentWillMount() {
      this.props.loader(true);
      this.props.invoiceDetails(this.props.match.params.id)
          .then(json => {
              this.setState({ invoicedetails: json.data },()=>this.props.loader(false));
              this.props.pdfData(json.data);
          });

   }
   selectChange = (selectedOption, fieldname) => {

     var state = this.state.invoicedetails;
     state[fieldname] = selectedOption;
     state[fieldname + '_error'] = false;

     this.setState(state);

   }
   status_update = (status)=>{
     this.props.invoice_status(status,this.props.match.params.id,this.state.invoicedetails);
     switch(status){
       case 'Duplicate':this.setState({ Duplicate: true });
       break;
       case 'inQueue':this.setState({ inQueue: true });
       break;

     }

   }
    render() {
      if(this.state.redirect){
      return(  <Redirect to='/duplicateinvoices'/>);
      }
      const  invoice_details  = this.state.invoicedetails;
      let company_name_error = (typeof (invoice_details.validate) != 'undefined') ? invoice_details.validate.company_name.error : false;
      let firstname_error = (typeof (invoice_details.validate) != 'undefined') ? invoice_details.validate.participant_firstname.error : false;
      let invoice_number_error = (typeof (invoice_details.validate) != 'undefined') ? invoice_details.validate.invoice_number.error : false;
      let invoice_amount_error = (typeof (invoice_details.validate) != 'undefined') ? invoice_details.validate.invoice_amount.error : false;



        return (

            <AuthWrapper>



                <div className=" back_col_cmn-">
                    <Link to='/pendinginvoices'><span className="icon icon-back1-ie"></span></Link>
                </div>

                <div className="main_heading_cmn-">
                    <h1>
                        <span>1/20 Duplicate Invoice</span>

                    </h1>
                </div>

                <div className='row pd_tb_15'>

                    <div className='col-xl-6 col-lg-12 col-12'>

                        <div className='bor_tb_grey pd_tb_10 d-flex align-items-center hdnfFlx23' >
                            <span><strong className='cmn_clr1 '>Invoice #3458</strong></span>
                            <button className='cmn-btn3 pd_btns2'>Paid</button>
                        </div>

                        <div className='pdf_box mr_t_15'>
                            <PdfViewer contents={invoice_details} />
                            <div className='clearfix'></div>
                        </div>

                    </div>


                    <div className='fieldArea pd_tb_15'>

                        <div className='dupliAssesment__'></div>

                        <div className='bor_tb  pd_tb_10' >
                            <strong>Account Details</strong>
                        </div>

                        <div className='row pd_tb_15'>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Invoice Date</label>
                                    <div className='datewid_100'>
                                        <DatePicker
                                            className="csForm_control filled"
                                            selected={moment(invoice_details.invoice_date, 'DD/MM/YYYY').valueOf()}
                                            value={invoice_details.invoice_date}
                                            onChange={(e) => this.setState({ invoicedetails:[{invoice_date:e}] })}
                                            dateFormat="DD/MM/YYYY"
                                            name="tempDueDate"
                                            placeholderText={'DD/MM/YY'}
                                        />
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Due Date</label>
                                    <div className='datewid_100'>
                                        <DatePicker
                                            className="csForm_control  filled"
                                            selected={moment(invoice_details.due_date, 'DD/MM/YYYY').valueOf()}
                                            value={invoice_details.due_date}
                                            onChange={(e) => this.setState({ invoicedetails:[{due_date:e}] })}
                                            dateFormat="DD/MM/YYYY"
                                            name="tempDueDate"
                                            placeholderText={'DD/MM/YY'}
                                        />
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Company Name</label>
                                    <div className='csaddOn_fld1__ '>
                                        <input type="text" className="csForm_control filled" name="" value={invoice_details.company_name || ''} />
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Matches Xero Supplier</label>
                                    <div className={(company_name_error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                        <input type="text" className={(company_name_error) ? "csForm_control errorIn2" : "csForm_control completed"} name="" />

                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Account No.</label>
                                    <div className='csaddOn_fld1__ '>
                                        <input type="text" className="csForm_control filled" name="" value={invoice_details.account_number || ''} />
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Known Biller</label>
                                    <div className={(invoice_number_error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                        <input type="text" className={(invoice_number_error) ? "csForm_control errorIn2" : "csForm_control completed"} name="" />

                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Invoice Amount</label>
                                    <div className='csaddOn_fld1__ checkedIc1'>
                                        <input type="text" className="csForm_control filled" name="" value={invoice_details.invoice_amount || ''} />
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Sufficient Available Balance</label>
                                    <div className={(invoice_amount_error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                        <input type="text" className={(invoice_amount_error) ? "csForm_control errorIn2" : "csForm_control completed"} name="" />
                                    </div>
                                </div>
                            </div>

                        </div>
                        {/* row ends */}


                        <div className='row'>

                            <div className='col-12'>

                                <div className='inpLabel1'>
                                    <strong>Payment Type</strong>
                                </div>
                                <ul className='py_typeUL'>
                                    <li className='active'>Bank</li>
                                    <li>B Pay</li>
                                    <li>AUS Post</li>
                                </ul>

                            </div>

                        </div>
                        {/* row ends */}

                        <div className='row'>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Biller Code</label>
                                    <div className='csaddOn_fld1__ '>
                                        <input type="text" className="csForm_control filled" name="" />
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Reference Number</label>
                                    <div className='csaddOn_fld1__ '>
                                        <input type="text" className="csForm_control filled" name="" />
                                    </div>
                                </div>
                            </div>


                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>PO Number</label>
                                    <div className='csaddOn_fld1__ notAvail'>
                                        <input type="text" className="csForm_control " name="" value='NA' readOnly />
                                    </div>
                                </div>
                            </div>


                        </div>
                        {/* row ends */}


                        <div className='bor_tb  pd_tb_10 mr_t_15' >
                            <strong>Member Details</strong>
                        </div>

                        <div className='row pd_tb_15' >

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>OCS Mail Account</label>
                                    <div className='csaddOn_fld1__ '>
                                        <input type="text" className="csForm_control completed" name="" value={invoice_details.participant_ocs_email_acc || ''} />
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>OCS ID</label>
                                    <div className='csaddOn_fld1__ '>
                                        <input type="text" className="csForm_control completed" name="" value={invoice_details.participant_ocs_id || ''} />
                                    </div>
                                </div>
                            </div>


                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>First Name</label>
                                    <div className='csaddOn_fld1__ '>
                                        <input type="text" className="csForm_control filled" name="" value={invoice_details.participant_firstname || ''} />
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Match System Data</label>
                                    <div className={(firstname_error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                        <input type="text" className={(firstname_error) ? "csForm_control errorIn2" : "csForm_control completed"} name="" />
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Last Name</label>
                                    <div className='csaddOn_fld1__ '>
                                        <input type="text" className="csForm_control filled" name="" value={invoice_details.participant_lastname || ''} />
                                    </div>
                                </div>
                            </div>

                        </div>
                        {/* row ends */}

                    </div>
                    {/* fieldArea ends */}


                </div>


                <div className='col-xl-6 col-lg-12 col-12 bor_l_grey'>

                    <div className='bor_tb_grey pd_tb_10 d-flex align-items-center hdnfFlx23' >
                        <span><strong className='cmn_clr1 '>Invoice #3458</strong></span>
                        <button className='cmn-btn5 pd_btns2'>Duplicate</button>
                    </div>

                    <div className='pdf_box mr_t_15'></div>



                    <div className='fieldArea pd_tb_15'>


                        <div className='dupliAssesment__'>

                            <div className='head_dup__'>
                                <div className='name_hd'>
                                    <h5>Duplicate Assessment Correct?</h5>
                                </div>
                                <div>
                                    <button className='btn cmn-btn6 ases_bts' onClick={() => { this.status_update('inQueue') }}>Return to Due</button>
                                    <button className='btn cmn-btn7 ases_bts' onClick={() => { this.status_update('Duplicate') }}>Mark Duplicate</button>
                                </div>
                            </div>
                            <div className='assess_body__'>
                                <i className='icon icon-warning2-ie wrng_ico'></i>
                                <div className=''>
                                    <h6>Denotes Duplicated Data</h6>
                                    <div>Compare Invoices and Selection above Date</div>
                                </div>
                            </div>

                        </div>
                        {/* dupliAssesment__ ends */}

                        <div className='bor_tb  pd_tb_10' >
                            <strong>Account Details</strong>
                        </div>

                        <div className='row pd_tb_15'>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Invoice Date</label>
                                    <div className='datewid_100 csaddOn_fld1__ errorIc'>
                                        <DatePicker
                                            className="csForm_control  errorIn2"
                                            selected={moment(invoice_details.invoice_date, 'DD/MM/YYYY').valueOf()}
                                            value={invoice_details.invoice_date}
                                            onChange={(e) =>this.selectChange(moment(e).format("DD/MM/YYYY"),'invoice_date')}
                                            dateFormat="DD/MM/YYYY"
                                            name="tempDueDate"
                                            placeholderText={'DD/MM/YY'}
                                        />
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Due Date</label>
                                    <div className='datewid_100 csaddOn_fld1__ errorIc'>
                                        <DatePicker
                                            className="csForm_control errorIn2"
                                            selected={moment(invoice_details.due_date, 'DD/MM/YYYY').valueOf()}
                                            value={invoice_details.due_date}
                                            onChange={(e) =>this.selectChange(moment(e).format("DD/MM/YYYY"),'due_date')}
                                            dateFormat="DD/MM/YYYY"
                                            name="tempDueDate"
                                            placeholderText={'DD/MM/YY'}
                                        />
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Company Name</label>
                                    <div className='csaddOn_fld1__  errorIc'>
                                        <input type="text" className="csForm_control errorIn2" name="" onChange={(e) =>this.selectChange(e.target.value,'company_name')}/>
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Matches Xero Supplier</label>
                                    <div className='csaddOn_fld1__ checkedIc1'>
                                        <input type="text" className="csForm_control completed" name="" />
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Account No.</label>
                                    <div className='csaddOn_fld1__ errorIc'>
                                        <input type="text" className="csForm_control errorIn2" name="" onChange={(e) =>this.selectChange(e.target.value,'account_number')}/>
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Known Biller</label>
                                    <div className='csaddOn_fld1__ checkedIc1'>
                                        <input type="text" className="csForm_control completed" name="" />
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Invoice Amount</label>
                                    <div className='csaddOn_fld1__ errorIc'>
                                        <input type="text" className="csForm_control errorIn2" name="" onChange={(e) =>this.selectChange(e.target.value,'invoice_amount')}/>
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Sufficient Available Balance</label>
                                    <div className='csaddOn_fld1__ checkedIc1'>
                                        <input type="text" className="csForm_control completed" name="" />
                                    </div>
                                </div>
                            </div>

                        </div>
                        {/* row ends */}


                        <div className='row'>

                            <div className='col-12'>

                                <div className='inpLabel1'><strong>Payment Type</strong></div>
                                <ul className='py_typeUL'>
                                    <li className='active'>Bank</li>
                                    <li>B Pay</li>
                                    <li>AUS Post</li>
                                </ul>

                            </div>

                        </div>
                        {/* row ends */}

                        <div className='row'>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Biller Code</label>
                                    <div className='csaddOn_fld1__ errorIc'>
                                        <input type="text" className="csForm_control errorIn2" name="" onChange={(e) =>this.selectChange(e.target.value,'biller_code')}/>
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Reference Number</label>
                                    <div className='csaddOn_fld1__ errorIc'>
                                        <input type="text" className="csForm_control errorIn2" name="" onChange={(e) =>this.selectChange(e.target.value,'reference_no')}/>
                                    </div>
                                </div>
                            </div>


                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>PO Number</label>
                                    <div className='csaddOn_fld1__ notAvail'>
                                        <input type="text" className="csForm_control " name="" value='NA' readOnly />
                                    </div>
                                </div>
                            </div>


                        </div>
                        {/* row ends */}

                        <div className='bor_tb  pd_tb_10 mr_t_15' >
                            <strong>Member Details</strong>
                        </div>

                        <div className='row pd_tb_15' >

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>OCS Mail Account</label>
                                    <div className='csaddOn_fld1__ '>
                                        <input type="text" className="csForm_control completed" name="" />
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>OCS ID</label>
                                    <div className='csaddOn_fld1__ '>
                                        <input type="text" className="csForm_control completed" name="" />
                                    </div>
                                </div>
                            </div>


                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>First Name</label>
                                    <div className='csaddOn_fld1__ errorIc'>
                                        <input type="text" className="csForm_control errorIn2" name="" />
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Match System Data</label>
                                    <div className='csaddOn_fld1__ checkedIc1'>
                                        <input type="text" className="csForm_control completed" name="" />
                                    </div>
                                </div>
                            </div>

                            <div className='col-lg-6 col-12'>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Last Name</label>
                                    <div className='csaddOn_fld1__ errorIc'>
                                        <input type="text" className="csForm_control errorIn2" name="" />
                                    </div>
                                </div>
                            </div>

                        </div>
                        {/* row ends */}

                    </div>
                    {/* fieldArea ends */}

                </div>
                {/* col-6 ends */}

                <NotifyPopUp
                   notifyColor='red'
                   show={this.state.Duplicate}
                   Close={() => { this.setState({ Duplicate: false,redirect:true }) }}
                   iconLef={'icon-approved2-ie'}
                   btnName={"Undo"}
               >

                   Invoice <span className='txt_clr'>{invoice_details.invoice_number}</span> marked as Duplicate

               </NotifyPopUp>


               <NotifyPopUp
                   notifyColor='yellow'
                   show={this.state.inQueue}
                   Close={() => { this.setState({ inQueue: false,redirect:true}) }}
                   iconLef={"icon-error-icons"}
                   btnName={"Undo"}
               >

                   Invoice <span className='txt_clr'>{invoice_details.invoice_number}</span> moved to Pending

               </NotifyPopUp>



            </AuthWrapper>

        );
    }
}




const mapStateToProps = state => {
    return {
        invoice_details: state.InvoiceReducer.invoicedetails,
        pdfData: state.InvoiceReducer.data

    }
};
const mapDispatchtoProps = (dispatch) => bindActionCreators({
    invoiceDetails, pdfData, loader,invoice_status
}, dispatch)
export default connect(mapStateToProps, mapDispatchtoProps)(ViewDuplicateInvoice);
