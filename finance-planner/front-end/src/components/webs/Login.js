import React, { Component } from 'react';

import { Link, Redirect } from 'react-router-dom';
// import { connect } from 'react-redux';
import { ROUTER_PATH, BASE_URL } from '../../config.js';
import jQuery from "jquery";
import axios from 'axios';
import moment from 'moment-timezone';
import '../../service/jquery.validate.js';
import { setRemeber, getRemeber, setLoginTIme } from '../../service/common.js';
import { connect } from 'react-redux';
import { login } from '../../store/actions';


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect:false,
            remember: '',
        }
    }


    handleChange = (e) => {
        var state = {};
        this.setState({ error: '' });
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }


  loginSubmit = (e) => {
       e.preventDefault();
      jQuery('#login').validate()
      if (jQuery('#login').valid() && !this.state.loading) {
      try {
        let { username, password } = this.state;
        (this.state.remember == 1) ? setRemeber({username: this.state.username, password: this.state.password,remember:this.state.remember}) : setRemeber({username: '', password: '',remember:''});
        this.props.login(this.state)
        .then((res) => {
            if(this.props.loginState){
                setTimeout(() => {
                    this.setState({redirect:true});
                }, 500)
            }
            
        });
      }
      catch(ex){
        this.state.error = "Unable to connect to server";
        console.log("error", ex);
      }
    }
  }

  componentDidMount(){
   var loginCookie = getRemeber();
   this.setState({username: loginCookie.username});
     this.setState({password: loginCookie.password});
     this.setState({remember: loginCookie.remember});
 }


    render() {
    if(this.state.redirect){
        return (<Redirect to="/dashboard" />);
     }
        return (

            <React.Fragment>
            {//this.state.redirect ? <Redirect to="/dashboard" /> : ''
            }

               <div className='Login_page row '>
                    <div className='col-12' >

                        <div className='log_faceIc'><i className='icon icon-userm1-ie'></i></div>
                        <h1 className='cmn_clr1 loginHdng1__'>Hello there,<br /> Welcome to the Plan Management Module</h1>

                        <div className='login_box '>
                            <form id="login" className="login-form"    onSubmit={this.loginSubmit}>

                                <div className='row'>

                                    <div className='col-sm-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Username:</label>
                                            <input type="text" className="csForm_control" name="username" placeholder="Username" value={this.state.username || ''} onChange={this.handleChange} data-rule-required="true" />
                                        </div>

                                        <div className="csform-group">
                                            <label className='inpLabel1'>Password:</label>
                                            <input type="password" className="csForm_control" name="password" placeholder="Password" value={this.state.password || ''} onChange={this.handleChange} data-rule-required="true" />
                                        </div>

                                    </div>
                                    <div className='col-sm-12'>
                                        <label className='d-flex align-items-center rmbrDv__ mr_t_15'>
                                            <label className='cstmChekie clrTpe2'>
                                                <input type="checkbox" className=" " name="remember"  value={this.state.remember || ''}  checked={(this.state.remember)} onChange={this.handleChange}/>
                                                <div className="chkie"></div>
                                            </label>
                                            <span> Remember Me</span>
                                        </label>
                                    </div>
                                    <div className='col-sm-12'><br/>
                                        <input type='submit' className='cmn-btn1 btn btn-block' value='Logon' />
                                    </div>

                                    <div className='col-sm-12'>
                                        {this.props.loginSuccess? <div className='validMsgBox__ successMsg1'><i className="icon icon-input-type-check"></i><span>{this.props.loginSuccess}</span></div>:''}
                                        {this.props.loginError?<div className='validMsgBox__ errorMsg1'><i className="icon icon-alert"></i><span>{this.props.loginError}</span></div>:''}

                                    </div>
                                    <div className='col-sm-12 text-center'>
                                        <p className='forgot_lnk__'>
                                            <Link to={ROUTER_PATH + 'forgot_password'}>Forgot Password?</Link>
                                        </p>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </React.Fragment>

        );
    }
}

const mapStateToProps = (state) => {
return  {
  loginState: state.Reducer1.loginState,
  loginSuccess:state.Reducer1.loginSuccess,
  loginError:state.Reducer1.loginError
  }
};
export default connect(mapStateToProps,{login})(Login);
