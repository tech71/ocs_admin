
import * as CT from './Color';

export const options = [
    { value: 'one', label: 'One' },
    { value: 'two', label: 'Two' }
];

export const Recruitmentdata = {
    labels: ['July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
    datasets: [{
        data: ['50', '40', '55', '85', '75', '80', '85', '90', '80', '70', '50', '40', '0', '100'],
        backgroundColor: [CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1],

    }]

};