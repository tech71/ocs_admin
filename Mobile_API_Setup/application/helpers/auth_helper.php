<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include APPPATH . 'helpers/jwt_helper.php';

if (!function_exists('password_encrpt')) {

    function password_encrpt($string) {
        $encry_password = password_hash($string, PASSWORD_BCRYPT);
        return $encry_password;
    }

}
if (!function_exists('password_validate')) {

    function password_validate($pass_string, $hash_key) {
        $response = false;
        if (password_verify($pass_string, $hash_key)) {
            $response = true;
        } else {
            $response = false;
        }
        return $response;
    }

}
if (!function_exists('generate_token')) {

    function generate_token($token_value) {
        $Obj_JWT = new \JWT();
        $JWT_Token = $Obj_JWT->encode($token_value, 'secret_server_key');
        return $JWT_Token;
    }

}

if (!function_exists('auth_login_status')) {

    function auth_login_status($login_detail) {
        $login_status = false;
        $CI = & get_instance();
        $CI->load->model('api_model');
        try {
            $response = array();
            if (empty($login_detail)) {
                $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
                echo json_encode($response);
                exit;
            } elseif (isValidJson($login_detail)) {
                //	$arrPayload=json_decode($login_detail);			
                $member_id = '';
                $member_token = '';
                if (isset($login_detail->memberid)) {
                    $member_id = $login_detail->memberid;
                }
                if (isset($login_detail->token)) {
                    $member_token = $login_detail->token;
                }

                $currunt_date = date("Y-m-d H:i:s");

                if (empty($member_token) || empty($member_id)) {
                    $response = array('status' => false, 'error' => system_msgs('empty_token_member'));
                    echo json_encode($response);
                    exit;
                } else {
                    //Set value in class property objects
                    require_once APPPATH . 'Classes/API/member/MemberLogin.php';
                    $objMemberLogin = new MemberLoginClass\MemberLogin();
                    $objMemberLogin->setMemberid($member_id);
                    $objMemberLogin->setMemberToken($member_token);
                    $objMemberLogin->setLoginUpdate($currunt_date);

                    $response = $CI->api_model->member_login_status($objMemberLogin);
                    if (!$response) {
                        #$response = array('status' => false, 'error' => system_msgs('mismatch_token_member'));
                        $response = array('status' => false, 'error' => 'Token expired or Mismatch');
                        echo json_encode($response);
                        exit;
                    }
                }
            } else {
                $response = array('status' => false, 'error' => system_msgs('invalid_json'));
                echo json_encode($response);
                exit;
            }
        } catch (Exception $e) {
            
        }
    }

}
if (!function_exists('isValidJson')) {

    function isValidJson($data = NULL) {
        if (!empty($data)) {
            @json_decode($data);
            return (json_last_error() === JSON_ERROR_NONE);
        }
        return false;
    }

}

if (!function_exists('api_request_handler')) {

    function api_request_handler() {
        $request_body = file_get_contents('php://input');
        $request_body = json_decode($request_body);

        if (empty($request_body) || empty($request_body->data)) {
            echo json_encode(array('status' => false, 'error' => system_msgs('INVALID_INPUT')));
            exit;
        } elseif (!isValidJson($request_body)) {
            echo json_encode(array('status' => false, 'error' => system_msgs('INVALID_JSON')));
            exit;
        } else {

            return $request_body->data;
        }
    }

}