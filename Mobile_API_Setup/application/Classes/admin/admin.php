<?php

/*
 * Filename: Admin.php
 * Desc: Deatils of Admin
 * @author YDT <yourdevelopmentteam.com.au>
 */

namespace AdminClass;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use classRoles as adminRoles;

require_once APPPATH . 'Classes/admin/role.php';

/*
 * Class: Admin
 * Desc: 2 arrays($admin_email, $admin_phone) Variables and Getter and Setter Methods for Admin
 * Created: 07-08-2018
 */

class Admin extends adminRoles\Roles {

    /**
     * @var adminid
     * @access private
     * @vartype: integer
     */
    private $adminid;

    /**
     * @var companyid
     * @access private
     * @vartype: smallint
     */
    private $companyid;

    /**
     * @var username
     * @access private
     * @vartype: varchar
     */
    private $username;

    /**
     * @var password
     * @access private
     * @vartype: varchar
     */
    private $password;

    /**
     * @var email
     * @access private
     * @vartype: varchar
     */
    private $primary_email;
    private $secondary_emails = array();

    /**
     * @var pin
     * @access private
     * @vartype: varchar
     */
    private $pin;

    /**
     * @var firstname
     * @access private
     * @vartype: varchar
     */
    private $firstname;

    /**
     * @var lastname
     * @access private
     * @vartype: varchar
     */
    private $lastname;

    /**
     * @var phone
     * @access private
     * @vartype: varchar
     */
    private $primary_phone;
    private $secondary_phone = array();

    /**
     * @var position
     * @access private
     * @vartype: varchar
     */
    private $position;

    /**
     * @var department
     * @access private
     * @vartype: varchar
     */
    private $department;

    /**
     * @var status
     * @access private
     * @vartype: tinyint
     */
    private $status;

    /**
     * @var background
     * @access private
     * @vartype: varchar
     */
    private $background;

    /**
     * @var gender
     * @access private
     * @vartype: tinyint
     */
    private $gender;

    /**
     * @array admin_email
     * @access private
     * @vartype: integer|varchar|tinyint
     */
    private $admin_email = array();

    /**
     * @array admin_phone
     * @access private
     * @vartype: integer|varchar|tinyint
     */
    private $admin_phone = array();

    /**
     * @function getAdminid
     * @access public
     * @returns $adminid integer
     * Get Admin Id
     */
//    private $userId;
//    private $username;
//    private $password;
//    private $email;
//    private $pin;
//    private $firstname;
//    private $lastname;
//    private $phone;
//    private $position;
//    private $department;
//    private $status;
//    private $token;
//    private $background;
//    private $gender;
//    // set username
//    public function setUsername($username) {
//        $this->username = $username;
//    }
//
//    // get username
//    public function getUsername() {
//        return $this->username;
//    }
//
//    // set password
//    public function setPassword($password) {
//        $this->password = $password;
//    }
//
//    // get password
//    public function getPassword() {
//        return $this->password;
//    }
//
//    // set userid
//    public function setUserId($userId) {
//        $this->userId = $userId;
//    }
//
//    // get userid
//    public function getUserId() {
//        return $this->userId;
//    }
//
//    // set userid
//    public function setToken($token) {
//        $this->token = $token;
//    }
//
//    // get userid
//    public function getToken() {
//        return $this->token;
//    }

    public function getAdminid() {
        return $this->adminid;
    }

    /**
     * @function setAdminid
     * @access public
     * @param $adminid integer 
     * Set Admin Id
     */
    public function setAdminid($adminid) {
        $this->adminid = $adminid;
    }

    /**
     * @function getCompanyid
     * @access public
     * @returns $companyid smallint
     * Get Company Id
     */
    public function getCompanyid() {
        return $this->companyid;
    }

    /**
     * @function setCompanyid
     * @access public
     * @param $companyid smallint 
     * Set Company Id
     */
    public function setCompanyid($companyid) {
        $this->companyid = $companyid;
    }

    /**
     * @function getUsername
     * @access public
     * @returns $username varchar
     * Get Username
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * @function setUsername
     * @access public
     * @param $username varchar 
     * Set Username
     */
    public function setUsername($username) {
        $this->username = $username;
    }

    /**
     * @function getPassword
     * @access public
     * @returns $password varchar
     * Get Password
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @function setPassword
     * @access public
     * @param $password varchar 
     * Set Password
     */
    public function setPassword($password) {
        $this->password = $password;
    }

    /**
     * @function getEmail
     * @access public
     * @returns $email varchar
     * Get Email
     */
    public function getPrimaryEmail() {
        return $this->primary_email;
    }

    /**
     * @function setEmail
     * @access public
     * @param $email varchar 
     * Set Email
     */
    public function setPrimaryEmail($email) {
        $this->primary_email = $email;
    }

    /**
     * @function getPin
     * @access public
     * @returns $pin varchar
     * Get Pin
     */
    public function getPin() {
        return $this->pin;
    }

    /**
     * @function setPin
     * @access public
     * @param $pin varchar 
     * Set Pin
     */
    public function setPin($pin) {
        $this->pin = $pin;
    }

    /**
     * @function getFirstname
     * @access public
     * @returns $firstname varchar
     * Get Firstname
     */
    public function getFirstname() {
        return $this->firstname;
    }

    /**
     * @function setFirstname
     * @access public
     * @param $firstname varchar 
     * Set Firstname
     */
    public function setFirstname($firstname) {
        $this->firstname = $firstname;
    }

    /**
     * @function getLastname
     * @access public
     * @returns $lastname varchar
     * Get Lastname
     */
    public function getLastname() {
        return $this->lastname;
    }

    /**
     * @function setLastname
     * @access public
     * @param $lastname varchar 
     * Set Lastname
     */
    public function setLastname($lastname) {
        $this->lastname = $lastname;
    }

    /**
     * @function getPhone
     * @access public
     * @returns $phone varchar
     * Get Phone
     */
    public function getPrimaryPhone() {
        return $this->primary_phone;
    }

    /**
     * @function setPhone
     * @access public
     * @param $phone varchar 
     * Set Phone
     */
    public function setPrimaryPhone($phone) {
        $this->primary_phone = $phone;
    }

    public function setSecondaryEmails($seconday_email) {
        $this->secondary_emails = $seconday_email;
    }

    public function setSecondaryPhone($secondary_phone) {
        $this->secondary_phones = $secondary_phone;
    }

    /**
     * @function getPosition
     * @access public
     * @returns $position varchar
     * Get Position
     */
    public function getPosition() {
        return $this->position;
    }

    /**
     * @function setPosition
     * @access public
     * @param $position varchar 
     * Set Position
     */
    public function setPosition($position) {
        $this->position = $position;
    }

    /**
     * @function getDepartment
     * @access public
     * @returns $department varchar
     * Get Department
     */
    public function getDepartment() {
        return $this->department;
    }

    /**
     * @function setDepartment
     * @access public
     * @param $department varchar 
     * Set Department
     */
    public function setDepartment($department) {
        $this->department = $department;
    }

    /**
     * @function getStatus
     * @access public
     * @returns $status tinyint
     * Get Status
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @function setStatus
     * @access public
     * @param $status tinyint 
     * Set Status
     */
    public function setStatus($status) {
        $this->status = $status;
    }

    /**
     * @function getBackground
     * @access public
     * @returns $background varchar
     * Get Background
     */
    public function getBackground() {
        return $this->background;
    }

    /**
     * @function setBackground
     * @access public
     * @param $background varchar 
     * Set Background
     */
    public function setBackground($background) {
        $this->background = $background;
    }

    /**
     * @function getGender
     * @access public
     * @returns $gender tinyint
     * Get Gender
     */
    public function getGender() {
        return $this->gender;
    }

    /**
     * @function setGender
     * @access public
     * @param $gender tinyint 
     * Set Gender
     */
    public function setGender($gender) {
        $this->gender = $gender;
    }

    /**
     * @function getAdminEmail
     * @access public
     * @returns $admin_email integer|varchar|tinyint
     * Get AdminEmail
     */
    public function getAdminEmail() {
        return $this->admin_email;
    }

    /**
     * @function setAdminEmail
     * @access public
     * @param $adminEmail integer|varchar|tinyint 
     * Set AdminEmail
     */
    public function setAdminEmail($adminEmail) {
        $this->admin_email = $adminEmail;
    }

    /**
     * @function getAdminPhone
     * @access public
     * @returns $admin_phone integer|varchar|tinyint
     * Get AdminPhone
     */
    public function getAdminPhone() {
        return $this->admin_phone;
    }

    /**
     * @function setAdminPhone
     * @access public
     * @param $adminPhone integer|varchar|tinyint 
     * Set AdminPhone
     */
    public function setAdminPhone($adminPhone) {
        $this->admin_phone = $adminPhone;
    }

    public function genratePin() {
        $pin = mt_rand(1000, 9999);
        $this->setPin($pin);

        return $pin;
    }

    public function createUser() {
        $CI = & get_instance();
        $encry_password = password_hash($this->password, PASSWORD_BCRYPT);

        $user_data = array('firstname' => $this->firstname, 'lastname' => $this->lastname, 'phone' => $this->primary_phone, 'position' => $this->position, 'department' => $this->department, 'email' => $this->primary_email, 'username' => $this->username, 'password' => $encry_password);

        $adminID = $CI->basic_model->insert_records('admin', $user_data, $multiple = FALSE);
        $this->setAdminid($adminID);

        return $adminID;
    }

    public function insertSecondyPhone() {
        $CI = & get_instance();
        if (count($this->secondary_phones) > 1) {
            foreach ($this->secondary_phones as $key => $val) {
                if ($key != 0) {
                    $addional_phone_number[] = array('phone' => $val->name, 'adminId' => $this->adminid, 'primary_phone' => 2);
                }
            }

            $CI->basic_model->insert_records('admin_phone', $addional_phone_number, $multiple = true);
        }
    }

    public function insertSecondyEmail() {
        $CI = & get_instance();

        if (count($this->secondary_emails) > 1) {
            foreach ($this->secondary_emails as $key => $val) {
                if ($key != 0) {
                    $addional_email[] = array('email' => $val->name, 'adminId' => $this->adminid, 'primary_email' => 2);
                }
            }

            $CI->basic_model->insert_records('admin_email', $addional_email, $multiple = true);
        }
    }

    public function insertRoleToAdmin() {
        $CI = & get_instance();
        $pin_access = false;

        if (!empty($this->getRoles())) {
            foreach ($this->getRoles() as $val) {
                if ((!empty($val->access)) && $val->access == 1) {
                    $pin_access = ($val->id == 1 || $val->id == 7) ? true : false;
                    $temp_roles[] = array('roleId' => $val->id, 'adminId' => $this->adminid);
                }
            }

            if ($pin_access) {
                // first genrate pin
                $this->genratePin();
                // assing pin to admin and update pin in database
                $this->updatePinAdmin();
            }


            $CI->basic_model->insert_records('admin_role', $temp_roles, $multiple = true);
        }
    }

    
    public function updatePinAdmin() {
        $CI = & get_instance();
        $encrypted_pin = password_hash($this->pin, PASSWORD_BCRYPT);
        $CI->basic_model->update_records('admin', array('pin' => $encrypted_pin), $multiple = false);
    }

}
