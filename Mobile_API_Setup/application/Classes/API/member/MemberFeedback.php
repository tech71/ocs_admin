<?php
/**
 *  Class name : Auth
 *  Create date : 18-07-2018,
 *  author : Corner stone solution
 *  Description : this class used for set cookie, check authentication, and set session
 * 
 */

namespace MemberFeedbackClass;

class MemberFeedback {

    public function __construct() {
        
    }
	
	private $memberid;
	private $rating;
	private $feedback;
	private $createddate;
	  
    public function getMemberId(){ return $this->memberid; }
    public function setMemberId($memberid){$this->memberid = $memberid;}

	public function getRating(){ return $this->rating; }
    public function setRating($rating){$this->rating = $rating;}
	
	public function getFeedback(){ return $this->feedback; }
    public function setFeedback($feedback){$this->feedback = $feedback;}
	
	public function getCreatedDate(){ return $this->createddate; }
    public function setCreatedDate($createddate){$this->createddate = $createddate;}
		
	public function create_feedback() {        
		$CI = & get_instance();
		$CI->load->model('Api_model');
		return $CI->api_model->insert_feedback($this);		
    }
}
