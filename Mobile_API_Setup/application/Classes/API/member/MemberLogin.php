<?php

/**
 *  Class name : Auth
 *  Create date : 18-07-2018,
 *  author : Corner stone solution
 *  Description : this class used for set cookie, check authentication, and set session
 * 
 */

namespace MemberLoginClass;

include APPPATH . 'Classes/API/member/Member.php';

use MemberClass;

class MemberLogin extends MemberClass\Member {

    public function __construct() {
        $memberClass = new MemberClass\member();
    }

    private $membertoken;
    private $loginupdateddate;
    private $logincreateddate;

    public function getMemberToken() {
        return $this->membertoken;
    }

    public function setMemberToken($membertoken) {
        $this->membertoken = $membertoken;
    }

    public function getLoginUpdate() {
        return $this->loginupdateddate;
    }

    public function setLoginUpdate($loginupdateddate) {
        $this->loginupdateddate = $loginupdateddate;
    }

    public function getLoginCreated() {
        return $this->logincreateddate;
    }

    public function setLoginCreated($logincreateddate) {
        $this->logincreateddate = $logincreateddate;
    }

    public function check_auth() {
        $CI = & get_instance();
        $CI->load->model('Api_model');
        $login_attempt = 2;

        $memberrecord = $CI->Api_model->valid_member($this);

        //return $memberpin;
        $getPassword = $this->getPin();
        //	return $user_details = password_encrpt($getPassword);
        $response = array();
        if (!empty($memberrecord)) {
            if ($memberrecord->loginattempt < $login_attempt) {
                if (!$memberrecord->archive) {
                    if ($memberrecord->status) {
                        if ($memberrecord->enable_app_access) {
                            if (password_validate($getPassword, $memberrecord->pin)) {
                                // Member Id Exist please check password					
                                $token = array();
                                $token['current_time'] = time();
                                $token['id'] = $this->getMemberid();

                                /* Remove all token of login member */
                                $CI->Api_model->remove_member_tokens($this);

                                $JWT_Token = generate_token($token);
                                $this->setMemberToken($JWT_Token);
                                $responseToken = $CI->Api_model->insert_member_token($this);
                                if ($responseToken > 0) {
                                    $this->setloginattempt(-1);
                                    $responseLoginAttempt = $CI->Api_model->update_login_attempt($this);
                                    $response = array('token' => $JWT_Token, 'status' => true, 'success' => system_msgs('success_login'), 'user_data' => array('name' => $memberrecord->name, 'user_id' => $memberrecord->id, 'gender' => $memberrecord->gender));
                                } else {
                                    // token not insert in tbl_member_login
                                    #$response = array('status' => false,'data'=>'inactive token'.$responseToken, 'error' => system_msgs('wrong_username_password'));
                                    $response = array('status' => false, 'error' => 'inactive token' . $responseToken, 'data' => '');
                                }
                            } else {
                                // status inactive member
                                $this->setloginattempt($memberrecord->loginattempt);
                                $responseLoginAttempt = $CI->Api_model->update_login_attempt($this);
                                $response = array('status' => false, 'data' => 'inactive member', 'error' => system_msgs('wrong_username_password'));
//                                $response = array('status' => false, 'data' => '', 'error' => 'Member is Inactive');
                            }
                        } else {
                            #$response = array('status' => false,'data'=>'Disable App Access', 'error' => system_msgs('wrong_username_password'));
                            $response = array('status' => false, 'data' => '', 'error' => 'Disable App Access');
                        }
                    } else {
                        // status inactive member
                        #$response = array('status' => false,'data'=>'inactive', 'error' => system_msgs('wrong_username_password'));
                        $response = array('status' => false, 'data' => '', 'error' => 'User is Inactive');
                    }
                } else {
                    // Archive delete user inactive member
                    #$response = array('status' => false,'data'=>'archive', 'error' => system_msgs('wrong_username_password'));
                    $response = array('status' => false, 'data' => '', 'error' => 'User is deleted');
                }
            } else {
                $responsePrimaryPhone = $CI->Api_model->get_member_primary_phone($this);
                $responsePrimaryEmail = $CI->Api_model->get_member_primary_email($this);
                $response_json = ['phone' => $responsePrimaryPhone->phone, 'email' => $responsePrimaryEmail->email];
                // Login attempt failed greater then 3
                $response = array('status' => false, 'error' => system_msgs('login_attempt'), 'data' => $response_json, 'reset_login' => true);
            }
        } else {
            // Member Not Exist
            #$response = array('status' => false,'data'=>'Member Not Exist', 'error' => system_msgs('wrong_username_password'));
            $response = array('status' => false, 'data' => '', 'error' => 'Member Not Exist');
        }
        return $response;
    }

    public function check_its_valid_otp() {
        $CI = & get_instance();
        $CI->load->model('api_model');

        $res_check = $CI->api_model->get_member_otp($this);

        $response = [];
        if (!empty($res_check)) {
            if (!empty($res_check->otp)) {
                if ((int) $res_check->otp === (int) $this->getOtp()) {
                    $diff = (strtotime(DATE_TIME) - strtotime($res_check->otp_expire_time));

                    if ($diff > 60 * 10) {
                        $response = array('status' => false, 'data' => '', 'error' => 'Otp expire');
                    } else {
                        $response = array('status' => true);
                    }
                } else {
                    $response = array('status' => false, 'data' => '', 'error' => 'Invalid otp');
                }
            } else {
                $response = array('status' => false, 'data' => '', 'error' => 'Invalid otp');
            }
        } else {
            $response = array('status' => false, 'data' => '', 'error' => 'Invalid member id');
        }

        return $response;
    }

    public function check_login_attampt() {
        $CI = & get_instance();
        $CI->load->model('api_model');

        $where = ['id' => $this->getMemberid()];
        $res_check = $CI->basic_model->get_row('member', ['loginattempt'], $where);
        if (!empty($res_check)) {
            if ($res_check->loginattempt == 2) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
