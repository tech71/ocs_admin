<?php

class Member_model extends CI_Model 

{

	public function member_list($reqData)

	{
		//request_handler();
		
		
		$limit = $reqData->pageSize;

		$page = $reqData->page;

		$sorted = $reqData->sorted;

		$filter = $reqData->filtered;

		$orderBy = '';

		$direction = '';



		$src_columns = array("tbl_member.id","tbl_member.firstname","tbl_member.lastname","tbl_member_phone.phone","tbl_member_email.email");



		if (!empty($sorted)) {

			if (!empty($sorted[0]->id)) {

				$orderBy = $sorted[0]->id;

				$direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';

			}

		} else {

			$orderBy = 'tbl_member.id';

			$direction = 'ASC';

		}



		$where = '';

		$where = "tbl_member.archive = 0";

		$sWhere = $where;





		if (!empty($filter))

		{

			if($filter->include_inactive){

				$status_con = "0,1";

				$this->db->where_in('tbl_member.status',$status_con,false);

			}

			else

			{

				$status_con = 1;

				$this->db->where_in('tbl_member.status',$status_con);

			}





			$where = $where . " AND (";

			$sWhere = " (" . $where;



			$search_value = $filter->srch_box;

			if(!empty($search_value))

			{

				for ( $i=0 ; $i<count($src_columns) ; $i++ )

				{

					$column_search = $src_columns[$i];

					if(strstr($column_search, "as") !== false) 

					{

						$serch_column = explode(" as ", $column_search);

						$sWhere .= $serch_column[0] . " LIKE '%" . $this->db->escape_like_str($search_value) . "%' OR ";

					}

					else

					{

						$sWhere .= $column_search . " LIKE '%" . $this->db->escape_like_str($search_value) . "%' OR ";

					}	

				}

				$sWhere = substr_replace($sWhere, "", -3);

				$sWhere .= '))';

			}

		}

		else

		{

			$status_con = 1;

			$this->db->where_in('tbl_member.status',$status_con);

		}



		$select_column = array('tbl_member.id as OCS_id', 'tbl_member.firstname', 'tbl_member.lastname','tbl_member_address.street','tbl_member_address.city','tbl_member_address.postal','tbl_member_address.state','tbl_member_phone.phone','tbl_member.gender','tbl_state.name as state_name');



		$dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);

		$this->db->from(TBL_PREFIX . 'member');



		$this->db->join('tbl_member_address', 'tbl_member_address.memberId = tbl_member.id AND tbl_member_address.primary_address = 1', 'left');

		$this->db->join('tbl_member_phone', 'tbl_member_phone.memberId = tbl_member.id AND tbl_member_phone.primary_phone = 1', 'left');

		$this->db->join('tbl_member_email', 'tbl_member_email.memberId = tbl_member.id AND tbl_member_email.primary_email = 1', 'left');

		$this->db->join('tbl_state', 'tbl_state.id = tbl_member_address.state', 'left');



		$this->db->order_by($orderBy, $direction);

		$this->db->limit($limit, ($page * $limit));



		$this->db->where($sWhere, null, false);

		$query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

		//last_query();die;

		$dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;



		if ($dt_filtered_total % $limit == 0) {

			$dt_filtered_total = ($dt_filtered_total / $limit);

		} else {

			$dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;

		}



		$dataResult = array();

		if (!empty($query->result())) {

			foreach ($query->result() as $val) {

				$row = array();

				$row['ocs_id'] = $val->OCS_id;

				$row['firstname'] = $val->firstname.' '.$val->lastname;

				#$complete_address = '';



				$street = isset($val->street)?$val->street:'';

				$city = isset($val->city)?$val->city:'';

				$postal = isset($val->postal)?$val->postal:'';

				$state = isset($val->state_name)?$val->state_name:'';

				

				$row['street'] = $street;

				$row['city'] = $city;

				$row['postal'] = $postal;

				$row['state'] = $state;

				

				$row['department'] = 'Department';

				$row['phone'] = $val->phone;

				$row['gender'] = isset($val->gender) && $val->gender == 1?'Male':'Female';;

				$dataResult[] = $row;

			}

		}



		$return = array('count' => $dt_filtered_total, 'data' => $dataResult);

		return $return;	
	

	}



	public function get_member_about()

	{		

		if(!empty(request_handler()))

		{

			$request = request_handler();

			$memberId = $request->data->member_id;



			$tbl_1 = TBL_PREFIX . 'member';

			$tbl_2 = TBL_PREFIX . 'member_address';

			$tbl_3 = TBL_PREFIX . 'member_phone';

			$tbl_4 = TBL_PREFIX . 'member_email';

			$tbl_5 = TBL_PREFIX . 'member_kin';

			$tbl_6 = TBL_PREFIX . 'member_phone';



			$this->db->select("CONCAT($tbl_1.firstname,' ',$tbl_1.middlename, ' ', $tbl_1.lastname) AS user_name", FALSE);



			$dt_query = $this->db->select(array($tbl_1.'.enable_app_access',$tbl_1.'.id as ocs_id',$tbl_1.'.firstname',$tbl_1.'.lastname',$tbl_1.'.companyId',$tbl_1.'.middlename',$tbl_1.'.profile_image',$tbl_1.'.prefer_contact',$tbl_1.'.dob',$tbl_1.'.gender',$tbl_1.'.status',$tbl_2.'.street',$tbl_2.'.city',$tbl_2.'.postal',$tbl_2.'.state',$tbl_2.'.primary_address','tbl_state.name as statename',$tbl_4.'.email'));



			$this->db->from($tbl_1);

			$this->db->join($tbl_2, 'tbl_member_address.memberId = tbl_member.id AND tbl_member_address.primary_address = 1', 'left');

			$this->db->join($tbl_4, 'tbl_member_email.memberId = tbl_member.id AND tbl_member_email.primary_email = 1', 'left');

			$this->db->join('tbl_state', 'tbl_state.id = tbl_member_address.state', 'left');

			$sWhere = array($tbl_1.'.id'=>$memberId);

			$this->db->where($sWhere, null, false);

			$query = $this->db->get();

			$x = $query->row_array();



			$user_ary = array();

			$user_ary['basic_detail']['full_name'] = $x['user_name'];

			$user_ary['basic_detail']['ocs_id'] = $x['ocs_id'];

			$user_ary['basic_detail']['first_name'] = $x['firstname'];

			$user_ary['basic_detail']['companyId'] = $x['companyId'];

			$user_ary['basic_detail']['prefer_contact'] = !empty($x['prefer_contact']) ?$x['prefer_contact']:'';

			$user_ary['basic_detail']['profile_image'] = $x['profile_image'];

			$user_ary['basic_detail']['email'] = $x['email'];

			$user_ary['basic_detail']['enable_app_access'] = $x['enable_app_access'];

			$user_ary['basic_detail']['gender'] = !empty($x['gender']) && $x['gender'] == 1?'Male':'Female';

			$user_ary['basic_detail']['status'] = $x['status'];

			$user_ary['basic_detail']['dob'] = !empty($x['dob']) && $x['dob']!='0000-00-00'?date('Y-m-d',strtotime($x['dob'])):'';



			if(isset($x['primary_address']))

			{

				$user_ary['basic_detail']['primary_address'] = $x['street'].', '.$x['city'].', '.$x['postal'].' '.$x['statename'];

			}



			/*Get keen*/

			$dt_query = $this->db->select(array($tbl_5.'.relation',$tbl_5.'.phone',$tbl_5.'.email',$tbl_5.'.primary_kin'));

			$this->db->select("CONCAT($tbl_5.firstname, ' ', $tbl_5.lastname) AS name", FALSE);



			$this->db->from($tbl_5);

			$sWhere = array($tbl_5.'.memberId'=>$memberId);

			$this->db->where($sWhere, null, false);

			$query = $this->db->get();

			$y = $query->result_array();

			if(!empty($y))

			{

				$user_ary['basic_detail']['kin_ary'] =$y;

			}



			/*Get phone no*/

			$dt_query = $this->db->select(array($tbl_6.'.phone',$tbl_6.'.primary_phone'));

			$this->db->from($tbl_6);

			$sWhere = array($tbl_6.'.memberId'=>$memberId);

			$this->db->where($sWhere, null, false);
			$this->db->order_by($tbl_6.'.primary_phone','ASC');
			$query = $this->db->get();

			$z = $query->result_array();

			if(!empty($z))

			{

				$user_ary['basic_detail']['phone_no_ary'] =$z;

			}

			return $user_ary;

		}

	}



	public function get_member_profile()

	{

		if(!empty(request_handler()))

		{

			$request = request_handler();

			$memberId = $request->data->member_id;



			$tbl_1 = TBL_PREFIX . 'member';

			$this->db->select("CONCAT($tbl_1.firstname,' ',$tbl_1.middlename, ' ', $tbl_1.lastname) AS user_name", FALSE);



			$dt_query = $this->db->select(array($tbl_1.'.enable_app_access',$tbl_1.'.id as ocs_id',$tbl_1.'.firstname',$tbl_1.'.lastname',$tbl_1.'.companyId',$tbl_1.'.middlename',$tbl_1.'.profile_image',$tbl_1.'.prefer_contact',$tbl_1.'.dob',$tbl_1.'.gender',$tbl_1.'.status'));



			$this->db->from($tbl_1);

			$sWhere = array($tbl_1.'.id'=>$memberId);

			$this->db->where($sWhere, null, false);

			$query = $this->db->get();

			$x = $query->row_array();



			$user_ary = array();

			$user_ary['basic_detail']['full_name'] = $x['user_name'];

			$user_ary['basic_detail']['ocs_id'] = $x['ocs_id'];

			$user_ary['basic_detail']['first_name'] = $x['firstname'];

			$user_ary['basic_detail']['companyId'] = $x['companyId'];

			$user_ary['basic_detail']['profile_image'] = $x['profile_image'];

			$user_ary['basic_detail']['enable_app_access'] = $x['enable_app_access'];

			$user_ary['basic_detail']['gender'] = !empty($x['gender']) && $x['gender'] == 1?'Male':'Female';

			$user_ary['basic_detail']['status'] = $x['status'];

			$user_ary['basic_detail']['dob'] = !empty($x['dob']) && $x['dob']!='0000-00-00'?date('Y-m-d',strtotime($x['dob'])):'';

			return $user_ary;

		}

	}



	public function member_qualification()

	{

		if(!empty(request_handler()))

		{

			$request = request_handler();

			$memberId = $request->data->member_id;

			$view_by = $request->data->view_by;



			if($view_by == 'Archive')

				$this->db->where('archive',1);

			else

				$this->db->where('archive',0);



			$tbl_1 = TBL_PREFIX . 'member_qualification';

			#$tbl_2 = TBL_PREFIX . 'member';

			$dt_query = $this->db->select(array($tbl_1.'.id',$tbl_1.'.expiry',$tbl_1.'.title',$tbl_1.'.created',$tbl_1.'.can_delete',$tbl_1.'.filename',"DATEDIFF(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(expiry)), '%Y-%m-%d'),CURDATE()) AS days_left"));

			$sWhere = array($tbl_1.'.memberId'=>$memberId);

			$this->db->from($tbl_1);

			#$this->db->join($tbl_2, 'tbl_member.id = tbl_member_qualification.memberId', 'left');

			$this->db->where($sWhere, null, false);

			$query = $this->db->get();

			$x = $query->result_array();

			$data_ary =array();

			if(!empty($x))

			{

				foreach ($x as $key => $value)

				{

					$temp_ary['id'] = $value['id'];		

					$temp_ary['days_left'] = $value['days_left'];	

					$temp_ary['expiry'] = $value['expiry'];		

					$temp_ary['title'] = $value['title'];		

					$temp_ary['created'] = $value['created'];		

					$temp_ary['can_delete'] = $value['can_delete'];		

					$temp_ary['filename'] = $value['filename'];	

					if($value['days_left'] < 0 || $value['days_left'] < 5)	

						$class_name = 'bg_dark_Pink';

					else if($value['days_left'] > 0 && $value['days_left'] < 30)

						$class_name = 'bg_dark_yello';

					else

						$class_name = '';



					$temp_ary['class_name'] = $class_name;	

					$data_ary[] = 	$temp_ary;

				}

			}

			

			if(!empty($data_ary))

				return $data_ary;

			else

				return array();

		}

	}



	public function get_member_preference()

	{

		if(!empty(request_handler()))

		{

			$request = request_handler();

			$memberId = $request->data->member_id;



			$tbl_1 = TBL_PREFIX . 'member_place';

			$tbl_2 = TBL_PREFIX . 'place';

			

			#$dt_query = $this->db->select(array($tbl_1.'.placeId',$tbl_2.'.name'));

			$dt_query = $this->db->select("GROUP_CONCAT($tbl_2.name) as places");

			

			$this->db->from($tbl_1);

			$this->db->join($tbl_2, 'tbl_place.id = tbl_member_place.placeId', 'left');

			$sWhere = array($tbl_1.'.memberId'=>$memberId);

			$this->db->where($sWhere, null, false);

			$query = $this->db->get();

			$x = $query->row_array();



			$main_ary = array();

			$main_ary['places'] = isset($x['places'])?$x['places']:'';



			/*get activity*/

			$tbl_1 = TBL_PREFIX . 'member_activity';

			$tbl_2 = TBL_PREFIX . 'activity';

			

			$dt_query = $this->db->select("GROUP_CONCAT($tbl_2.name) as activity");

			

			$this->db->from($tbl_1);

			$this->db->join($tbl_2, 'tbl_activity.id = tbl_member_activity.activityId', 'left');

			$sWhere = array($tbl_1.'.memberId'=>$memberId);

			$this->db->where($sWhere, null, false);

			$query = $this->db->get();

			$y = $query->row_array();



			$main_ary['activity'] = isset($y['activity'])?$y['activity']:'';

			return $main_ary;

		}

	}



	/*public function save_member_availability()

	{

		if(!empty(request_handler()))

		{

			$request = request_handler();

			$memberId = $request->data->member_id;



			

		}

	}*/

}





