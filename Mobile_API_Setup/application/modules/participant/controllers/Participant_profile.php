<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class Participant_profile extends CI_Controller {

    function __construct() {

        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('participant_model');
        if (!$this->session->userdata('username')) {
            // redirect('');
        }
    }

    public function participant_app_access() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $this->basic_model->update_records('participant', $data = array('portal_access' => $reqData->status), $where = array('id' => $reqData->participant_id));
            echo json_encode(array('status' => true));
        }
    }

    public function change_active_deactive() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $this->basic_model->update_records('participant', $data = array('status' => $reqData->status), $where = array('id' => $reqData->participant_id));
            echo json_encode(array('status' => true));
        }
    }

    public function get_participant_profile() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $participantID = $reqData->data->id;

            $result = $this->participant_model->participant_profile($participantID);
            if (!empty($result)) {
                $result = $result[0];
                echo json_encode(array('status' => true, 'data' => $result));
            }
        }
    }

    public function get_participant_about() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $participantId = $reqData->data->id;

            $result = $this->participant_model->participant_about($participantId);
            if (!empty($result)) {
                echo json_encode(array('status' => true, 'data' => $result));
            } else {
                echo json_encode(array('status' => false, 'error' => 'Sorry no data found'));
            }
        }
    }

    public function get_participant_health() {
        echo json_encode(array('status' => true, 'data' => ''));
    }

    public function get_participant_plans() {
        echo json_encode(array('status' => true, 'data' => ''));
    }

    public function get_participant_docs() {
        echo json_encode(array('status' => true, 'data' => ''));
    }

    public function get_participant_fsm() {
        echo json_encode(array('status' => true, 'data' => ''));
    }

    public function get_participant_sites() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $participantID = $reqData->data->id;
            $result = $this->participant_model->participant_sites($participantID);

            if ($result) {
                echo json_encode(array('status' => true, 'data' => $result));
            } else {
                echo json_encode(array('status' => false, 'error' => 'Sorry no data found'));
            }
        }
    }

    public function get_participant_strategies_care_notes() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $participantID = $reqData->data->id;
            $type = $reqData->data->type;
            $result = $this->participant_model->strategies_care_notes($participantID, $type);
            $res = array('care_notes' => [], 'strategies' => []);
//            last_query();
            if ($result) {
                foreach ($result as $val) {
                    $val->created = date('d/m/y', strtotime($val->created));
                    $res[$val->categories][] = $val;
                }

                echo json_encode(array('status' => true, 'data' => $res));
            } else {
                echo json_encode(array('status' => true, 'data' => $res));
            }
        }
    }

    public function create_cares_notes() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $data = $reqData->data;

            $update_data = array('title' => $data->title, 'content' => $data->content, 'categories' => $data->categories, 'updated' => DATE_TIME, 'participantID' => $data->participantID);
            if (!empty($data->notesID) && $data->notesID > 0) {
                $result = $this->basic_model->update_records('participant_about_care', $update_data, $where = array('id' => $data->notesID));
            } else {
                $update_data['created'] = DATE_TIME;
                $result = $this->basic_model->insert_records('participant_about_care', $update_data, $multiple = FALSE);
            }

            echo json_encode(array('status' => true, 'data' => $result));
        }
    }

}
