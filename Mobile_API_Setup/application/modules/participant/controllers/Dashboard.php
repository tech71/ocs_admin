<?php

use classRoles as adminRoles;

defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class Dashboard extends CI_Controller {

    function __construct() {



        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Participant_model');
        if (!$this->session->userdata('username')) {
            // redirect('');
        }
    }

    public function index() {
        $this->render_view('components/admin//dashboard/AppDashboard');
    }

    public function list_participant() {
        $this->load->model('Participant_model');
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $response = $this->Participant_model->participant_list($reqData);

            echo json_encode($response);
        }
    }

    public function get_state() {
        $reqData = request_handler();
        $response = $this->basic_model->get_record_where('state', $column = array('name', 'id'), $where = array('archive' => 0));
        foreach ($response as $val) {
            $state[] = array('label' => $val->name, 'value' => $val->id);
        }
        echo json_encode(array('status' => true, 'data' => $state));
    }

    public function create_participant() {

        // Read Php file here
        //save data response json

        $reqData = request_handler();
        $requestData = (array) $reqData->data;
        $first_step_data = (array) json_decode($requestData['step_first']);

        $participant_data = array_merge($first_step_data, $requestData);


        global $globalparticipant;
        require_once APPPATH . 'Classes/participant/Participant.php';
        $objparticipant = new ParticipantClass\Participant();

        require_once APPPATH . 'Classes/participant/ParticipantCareRequirement.php';
        $objParticipantCareRequirment = new classParticipantCareRequirement\ParticipantCareRequirement();

        $response = $this->validate_participant_data($participant_data);


        if (!empty($response['status'])) {
            // set particicpant
            $temp = $this->setParticipantvalues($objparticipant, $participant_data);
            if ($temp) {
                $globalparticipant = $objparticipant;

                // create participant
                $participant_id = $objparticipant->AddParticipant();

                if ($participant_id > 0) {
                    // for address
                    if (!empty($participant_data['AddressInput'])) {
                        foreach ($participant_data['AddressInput'] as $key => $val) {

                            $prim_sec = ($key == 0) ? 1 : 2;
                            $address[] = array('participantId' => $participant_id, 'street' => $val->street, 'postal' => $val->postal, 'state' => $val->state, 'site_category' => $val->department, 'primary_address' => $prim_sec);
                        }
                        $this->basic_model->insert_records('participant_address', $address, $multiple = true);
                    }

                    // kin details
                    if (!empty($participant_data['kindetails'])) {
                        $kin_details = array();

                        foreach ($participant_data['kindetails'] as $val) {
                            $kin_details[] = array('participantId' => $participant_id, 'firstname' => $val->name, 'lastname' => $val->lastname, 'phone' => $val->contact, 'email' => $val->email, 'relation' => $val->relation);
                        }

                        if (!empty($kin_details))
                            $this->basic_model->insert_records('participant_kin', $kin_details, $multiple = true);
                    }

                    // oc service data
                    if (!empty($participant_data['os_Services'])) {
                        $oc_service = array();

                        foreach ($participant_data['os_Services'] as $val) {
                            if ($val->active == 1) {
                                $oc_service[] = array('participantId' => $participant_id, 'oc_service' => $val->id);
                            }
                        }

                        if (!empty($oc_service))
                            $this->basic_model->insert_records('participant_oc_services', $oc_service, $multiple = true);
                    }

                    // about care person not required
                    if (!empty($participant_data['CarersInput'])) {
                        $book_type_user = array();

                        foreach ($participant_data['CarersInput'] as $val) {
                            $book_type_user[] = array('participantId' => $participant_id, 'gender' => $val->Gender, 'ethnicity' => $val->Ethnicity, 'religious' => $val->Religious);
                        }

                        if (!empty($book_type_user))
                            $this->basic_model->insert_records('participant_care_not_tobook', $book_type_user, $multiple = true);
                    }

                    // require_assistance insert data
                    if (!empty($participant_data['require_assistance'])) {
                        $required_assitance = array();

                        foreach ($participant_data['require_assistance'] as $val) {
                            if ($val->active == 1) {
                                $required_assitance[] = array('participantId' => $participant_id, 'assistanceId' => $val->id);
                            }
                        }

                        if (!empty($required_assitance))
                            $this->basic_model->insert_records('participant_assistance', $required_assitance, $multiple = true);
                    }

                    // support_required
                    if (!empty($participant_data['support_required'])) {
                        $support_required = array();

                        foreach ($participant_data['support_required'] as $val) {
                            if ($val->active == 1) {
                                $support_required[] = array('participantId' => $participant_id, 'support_required' => $val->id);
                            }
                        }

                        if (!empty($support_required))
                            $this->basic_model->insert_records('participant_support_required', $support_required, $multiple = true);
                    }
                    
                    if(!empty($participant_data['participantcareinfo'])){
                        $this->basic_model->insert_records('participant_about_care', array('content' => $participant_data['participantcareinfo'], 'categories' => 'care_notes', 'primary_key' => $participant_id) , $multiple = false);
                    }

                    // participant care requirement
                    $objParticipantCareRequirment->setParticipantid($participant_id);
                    $objParticipantCareRequirment->setDiagnosisPrimary($participant_data['formaldiagnosisprimary']);
                    $objParticipantCareRequirment->setDiagnosisSecondary(!empty($participant_data['formaldiagnosissecondary']) ? $participant_data['formaldiagnosissecondary'] : '' );
                    $objParticipantCareRequirment->setParticipantCare($participant_data['participantcareinfo']);
                    $objParticipantCareRequirment->setCognition($participant_data['participantCognition']);
                    $objParticipantCareRequirment->setCommunication($participant_data['participantCommunication']);
                    $objParticipantCareRequirment->setEnglish($participant_data['participantenglish']);
                    $objParticipantCareRequirment->setPreferredLanguage($participant_data['participantPreferredlang']);
                    $objParticipantCareRequirment->setLinguisticInterpreter($participant_data['participantLinguistic']);
                    $objParticipantCareRequirment->setHearingInterpreter($participant_data['participantHearing']);
                    $objParticipantCareRequirment->setRequireAssistanceOther($participant_data['participantReqAssistanceOther']);
                    $objParticipantCareRequirment->setSupportRequireOther($participant_data['participantsupportother']);

                    $objParticipantCareRequirment->create_care_requirement();
                }

                if ($participant_id > 0) {
                    $response = array('status' => true);
                } else {
                    $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
                }
            } else {
                $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
            }
        } else {
            $response = array('status' => false, 'error' => $response['error']);
        }

        echo json_encode($response);
    }

    function participant_username() {
        global $globalparticipant;
        $user_cnt = $globalparticipant->participant_UserName();
        $return = true;
        if ($user_cnt > 0) {
            $this->form_validation->set_message('participant_username', 'User Name Allready Exist');
            $return = false;
        }
        return $return;
    }

    // set class properties values in Participant class
    function setParticipantvalues($objparticipant, $participant_data) {

        try {
            $EmailInput = $participant_data['EmailInput'];
            if (is_array($EmailInput) && !empty($EmailInput)) {
                foreach ($EmailInput as $key => $participantemail) {
                    $emails = array();
                    $emails['email'] = $participantemail->name;
                    if ($key == 0)
                        $emails['type'] = '1';
                    else
                        $emails['type'] = '2';

                    $array_email[] = $emails;
                }
            }

            // Set contact values
            $array_phone = array();
            $PhoneInput = $participant_data['PhoneInput'];
            if (is_array($PhoneInput) && !empty($PhoneInput)) {
                foreach ($PhoneInput as $key => $participantephone) {
                    $phones = array();
                    $phones['phone'] = $participantephone->name;
                    if ($key == 0)
                        $phones['type'] = '1';
                    else
                        $phones['type'] = '2';

                    $array_phone[] = $phones;
                }
            }

            $participant_data['referral'] = !empty($participant_data['referral']) ? $participant_data['referral'] : 0;
            $objparticipant->setUserName($participant_data['username']);
            $objparticipant->setFirstname($participant_data['firstname']);
            $objparticipant->setMiddlename(!empty($participant_data['middlename']) ? $participant_data['middlename'] : '');
            $objparticipant->setLastname($participant_data['lastname']);
            $objparticipant->setGender($participant_data['gender']);
            $objparticipant->setPreferredname(!empty($participant_data['Preferredname']) ? $participant_data['Preferredname'] : '');
            $objparticipant->setDob($participant_data['Dob']);

            $objparticipant->setNdisNum(!empty($participant_data['Preferredndis']) ? $participant_data['Preferredndis'] : '');
            $objparticipant->setMedicareNum(!empty($participant_data['Preferredmedicare']) ? $participant_data['Preferredmedicare'] : '');
            $objparticipant->setCrnNum(!empty($participant_data['Preferredcrn']) ? $participant_data['Preferredcrn'] : '');

            $objparticipant->setLivingSituation($participant_data['ParticipantSituation']);
            $objparticipant->setAboriginalTsi($participant_data['ParticipantTSI']);
            $objparticipant->setOcDepartments(!empty($participant_data['AssignOcDepartment']) ? $participant_data['AssignOcDepartment'] : '');
            $objparticipant->setHouseid((!empty($participant_data['AssignOcHouse']) ? $participant_data['AssignOcHouse'] : ''));
            $objparticipant->setCreated(DATE_TIME);
            $objparticipant->setStatus(1);
            $objparticipant->setReferral(($participant_data['referral'] == 2) ? 0 : 1);

            if ($participant_data['referral'] == 1) {
                $objparticipant->setParticipantRelation($participant_data['refferalRelation']);
                $objparticipant->setReferralFirstName($participant_data['Referrer_first_name']);
                $objparticipant->setReferralLastName($participant_data['Referrer_last_name']);
                $objparticipant->setReferralEmail($participant_data['Referrer_email']);
                $objparticipant->setReferralPhone($participant_data['Referrer_phone']);
            }

            $objparticipant->setArchive(1);
            $objparticipant->setPortalAccess(1);
            $objparticipant->setParticipantEmail($array_email);
            $objparticipant->setParticipantPhone($array_phone);
        } catch (Exception $e) {
            return false;
        }
        return $objparticipant;
    }

    function validate_participant_data($participant_data) {
        try {
            $validation_rules = array(
                array('field' => 'PhoneInput[]', 'label' => 'participant Phone', 'rules' => 'callback_check_participant_number'),
                array('field' => 'EmailInput[]', 'label' => 'participant Email', 'rules' => 'callback_check_participant_email_address'),
                array('field' => 'AddressInput[]', 'label' => 'Address', 'rules' => 'callback_check_participant_address'),
                array('field' => 'username', 'label' => 'username', 'rules' => 'callback_check_participant_username_already_exist'),
                array('field' => 'firstname', 'label' => 'Firs name', 'rules' => 'required'),
                array('field' => 'lastname', 'label' => 'Last name', 'rules' => 'required'),
                array('field' => 'gender', 'label' => 'gender', 'rules' => 'required'),
                array('field' => 'Dob', 'label' => 'Dob', 'rules' => 'required'),
//                array('field' => 'Preferredname', 'label' => 'Preferred name', 'rules' => 'required'),
//                array('field' => 'Preferredndis', 'label' => 'Preferred ndis', 'rules' => 'required'),
//                array('field' => 'Preferredmedicare', 'label' => 'Preferred medicare', 'rules' => 'required'),
//                array('field' => 'Preferredcrn', 'label' => 'Preferred crn', 'rules' => 'required'),
                array('field' => 'Preferredcontacttype', 'label' => 'Preferred contact type', 'rules' => 'required'),
                array('field' => 'ParticipantSituation', 'label' => 'Participant Situation', 'rules' => 'required'),
                array('field' => 'ParticipantTSI', 'label' => 'Participant TSI', 'rules' => 'required'),
                array('field' => 'formaldiagnosisprimary', 'label' => 'formal diagnosis primary TSI', 'rules' => 'required'),
                array('field' => 'participantCognition', 'label' => 'participant Cognition', 'rules' => 'required'),
                array('field' => 'participantCommunication', 'label' => 'participant Communication', 'rules' => 'required'),
                array('field' => 'participantenglish', 'label' => 'participant english', 'rules' => 'required'),
                array('field' => 'participantPreferredlang', 'label' => 'participant Preferred language', 'rules' => 'required'),
                array('field' => 'participantLinguistic', 'label' => 'participant Linguistic', 'rules' => 'required'),
                array('field' => 'participantHearing', 'label' => 'participant Hearing', 'rules' => 'required'),
                array('field' => 'CarersInput[]', 'label' => 'gender', 'rules' => 'callback_check_carers_data'),
            );

            $this->form_validation->set_data($participant_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }

        return $return;
    }

    function check_participant_number($phone_numbers) {
        if (!empty($phone_numbers)) {

            foreach ($phone_numbers as $key => $val) {
                if ($key == 'primary_phone')
                    continue;

                if (empty($val)) {
                    $this->form_validation->set_message('check_participant_number', 'phone number can not be empty');
                    return false;
                }
            }
        } else {

            $this->form_validation->set_message('check_participant_number', 'phone number can not be empty');
            return false;
        }
        return true;
    }

    function check_participant_email_address($email_address) {
        if (!empty($email_address)) {
            foreach ($email_address as $key => $val) {
                if ($key == 'primary_email')
                    continue;

                if (empty($val)) {
                    $this->form_validation->set_message('check_participant_email_address', 'participant email can not empty');
                    return false;
                } elseif (!filter_var($val, FILTER_VALIDATE_EMAIL)) {
                    $this->form_validation->set_message('check_participant_email_address', 'participant email not valid');
                    return false;
                }
            }
        } else {
            $this->form_validation->set_message('check_participant_email_address', 'participant email can not empty');
            return false;
        }
        return true;
    }

    function check_participant_address($address) {
        if (!empty($address)) {
            if (empty($address->street)) {
                $this->form_validation->set_message('check_participant_address', 'participant primary address can not be empty');
                return false;
            } elseif (empty($address->state)) {
                $this->form_validation->set_message('check_participant_address', 'participant state can not be empty');
                return false;
            } elseif (empty($address->postal)) {
                $this->form_validation->set_message('check_participant_address', 'participant pincode can not be empty');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_participant_address', 'participant address can not empty');
            return false;
        }
        return true;
    }

    public function check_carers_data($caters_input) {
        if (!empty($caters_input)) {
            if (empty($caters_input->Gender)) {
                $this->form_validation->set_message('check_carers_data', 'Carers (Members) gender can not be empty');
                return false;
            }
            if (empty($caters_input->Ethnicity)) {
                $this->form_validation->set_message('check_carers_data', 'Carers (Members) Ethnicity can not be empty');
                return false;
            }
            if (empty($caters_input->Religious)) {
                $this->form_validation->set_message('check_carers_data', 'Carers (Members) Religious can not be empty');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_carers_data', 'Carers (Members) can not empty');
            return false;
        }

        return true;
    }

    function check_participant_username_already_exist($sever_end_username = false, $server_ocs_id = false) {
        if ($this->input->get('username') || $sever_end_username) {
            $username = ($this->input->get('username')) ? $this->input->get('username') : $sever_end_username;
            $ocs_id = ($this->input->get('id')) ? $this->input->get('id') : $server_ocs_id;


            if ($ocs_id > 0) {
                $where = array('id !=' => $ocs_id);
            }
            $where['username'] = $username;
            $result = $this->basic_model->get_row('participant', array('username'), $where);

            if ($sever_end_username) {
                if (!empty($result)) {
                    $this->form_validation->set_message('check_participant_username_already_exist', 'Partucipant user Name Allready Exist');
                    return false;
                }
                return true;
            }

            if (!empty($result)) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
    }

    public function get_participant_genteral() {
        $reqData = request_handler();

        $response = $this->basic_model->get_record_where('participant_genral', $column = array('name', 'id', 'type'), $where = array('status' => 1));
        foreach ($response as $val) {
            $genral[$val->type][] = array('id' => $val->id, 'label' => $val->name, 'active' => false);
        }
        echo json_encode(array('status' => true, 'data' => $genral));
    }

    public function check_kin_detials($kin_detailObject) {
        $kin_details = (array) $kin_detailObject;

        if (empty($kin_details['relation']) && empty($kin_details['phone']) && empty($kin_details['email']) && empty($kin_details['firstname']) && empty($kin_details['lastname'])) {
            return true;
        } else {
            if (empty($kin_details['relation'])) {
                $this->form_validation->set_message('check_kin_detials', 'kin relation can not be empty');
                return false;
            } elseif (Empty($kin_details['phone'])) {
                $this->form_validation->set_message('check_kin_detials', 'kin phone can not be empty');
                return false;
            } elseif (Empty($kin_details['email'])) {
                $this->form_validation->set_message('check_kin_detials', 'kin email can not be empty');
                return false;
            } elseif (Empty($kin_details['firstname'])) {
                $this->form_validation->set_message('check_kin_detials', 'kin firname can not be empty');
                return false;
            } elseif (Empty($kin_details['lastname'])) {
                $this->form_validation->set_message('check_kin_detials', 'kin lastname can not be empty');
                return false;
            }
        }
    }

    public function update_participant_about() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $participant_data = (array) $reqData->data;

            $validation_rules = array(
                array('field' => 'phones[]', 'label' => 'participant Phone', 'rules' => 'callback_check_participant_number'),
                array('field' => 'emails[]', 'label' => 'participant Email', 'rules' => 'callback_check_participant_email_address'),
                array('field' => 'address[]', 'label' => 'Address', 'rules' => 'callback_check_participant_address'),
                array('field' => 'username', 'label' => 'username', 'rules' => 'callback_check_participant_username_already_exist[' . $participant_data['ocs_id'] . ']'),
                array('field' => 'kin_detials[]', 'label' => 'Address', 'rules' => 'callback_check_kin_detials'),
                array('field' => 'firstname', 'label' => 'Firs name', 'rules' => 'required'),
                array('field' => 'lastname', 'label' => 'Last name', 'rules' => 'required'),
                array('field' => 'gender', 'label' => 'gender', 'rules' => 'required'),
                array('field' => 'dob', 'label' => 'dob', 'rules' => 'required'),
                array('field' => 'prefer_contact', 'label' => 'Preferred contact type', 'rules' => 'required'),
            );

            $this->form_validation->set_data($participant_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $this->Participant_model->participant_update($participant_data);
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        }
        echo json_encode($return);
    }

}
