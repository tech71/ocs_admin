<?php

namespace FmsClass;

//include('FmsNotes.php'); 
include APPPATH . 'Classes/fms/FmsNotes.php';
/*
 * Filename: Fms.php
 * Desc: Fms of Members, end and start time of shift
 * @author YDT <yourdevelopmentteam.com.au>
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * Class: Fms
 * Desc: Class Has 5 Arrays, variables ans setter and getter methods of shifts
 * Created: 25-10-2018
 */

class Fms extends \FmsNotesClass\FmsNotes {

    public $CI;

    function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('Shift_roster_model');
    }

    private $id;
    private $companyId;
    private $event_date;
    private $shiftId;
    private $initiated_by;
    private $initiated_type;
    private $created;
    private $escalate_to_incident;
    private $address;
    private $suburb;
    private $postal;
    private $state;
    private $status;
    private $category;
    private $against_category;
    private $against_by;

    function setId($id) {
        $this->id = $id;
    }

    function getId() {
        return $this->id;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }

    function getCompanyId() {
        return $this->companyId;
    }

    function setEvent_date($event_date) {
        $this->event_date = $event_date;
    }

    function getEvent_date() {
        return $this->event_date;
    }

    function setShiftId($shiftId) {
        $this->shiftId = $shiftId;
    }

    function getShiftId() {
        return $this->shiftId;
    }

    function setInitiated_by($initiated_by) {
        $this->initiated_by = $initiated_by;
    }

    function getInitiated_by() {
        return $this->initiated_by;
    }

    function setInitiated_type($initiated_type) {
        $this->initiated_type = $initiated_type;
    }

    function getInitiated_type() {
        return $this->initiated_type;
    }

    function setCreated($created) {
        $this->created = $created;
    }

    function getCreated() {
        return $this->created;
    }

    function setEscalate_to_incident($escalate_to_incident) {
        $this->escalate_to_incident = $escalate_to_incident;
    }

    function getEscalate_to_incident() {
        return $this->escalate_to_incident;
    }

    function setAddress($address) {
        $this->address = $address;
    }

    function getAddress() {
        return $this->address;
    }

    function setSuburb($suburb) {
        $this->suburb = $suburb;
    }

    function getSuburb() {
        return $this->suburb;
    }

    function setPostal($postal) {
        $this->postal = $postal;
    }

    function getPostal() {
        return $this->postal;
    }

    function setState($state) {
        $this->state = $state;
    }

    function getState() {
        return $this->state;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function getStatus() {
        return $this->status;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function getCategory() {
        return $this->category;
    }

    // set FMS Location class property	
    private $FmsLocation = array();

    public function getFmsLocation() {
        return $this->FmsLocation;
    }

    public function setFmsLocation($FmsLocation) {
        $this->FmsLocation = $FmsLocation;
    }

    function setAgainst_category($against_category) {
        $this->against_category = $against_category;
    }

    function getAgainst_category() {
        return $this->against_category;
    }

    function setAgainst_by($against_by) {
        $this->against_by = $against_by;
    }

    function getAgainst_by() {
        return $this->against_by;
    }

    // set FMS Location class property	
    private $FmsNotes = array();

    public function getFmsNotes() {
        return $this->FmsNotes;
    }

    public function setFmsNotes($FmsNotes) {
        $this->FmsNotes = $FmsNotes;
    }

    function create_case() {
        $CI = & get_instance();
        $CI->load->model('Fms_model');
        return $CI->Fms_model->create_case($this);
    }

    function get_feedback_case_details() {
        $CI = & get_instance();
        $CI->load->model('Fms_model');
        return $CI->Fms_model->getFeedbackCaseDetails($this);
    }

    function get_feedback_case_region_details() {
        $CI = & get_instance();
        $CI->load->model('Fms_model');
        return $CI->Fms_model->getFeedbackCaseRegionDetails($this);
    }

    function get_feedback_case_note_details() {
        $CI = & get_instance();
        $CI->load->model('Fms_model');
        return $CI->Fms_model->getFeedbackCaseNoteDetails($this);
    }

    function getOngoingFeedback() {
        return $this->CI->Shift_roster_model->get_ongoing_feedback($this);
    }

    function insert_location() {
        return $this->CI->Shift_roster_model->insert_fms_locations($this->getFmsLocation());
    }

}
