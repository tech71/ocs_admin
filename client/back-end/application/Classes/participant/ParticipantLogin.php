<?php

/**
 *  Class name : Auth
 *  Create date : 18-07-2018,
 *  author : Corner stone solution
 *  Description : this class used for set cookie, check authentication, and set session
 * 
 */

namespace ParticipantLoginClass;

include APPPATH . 'Classes/participant/Participant.php';

use ParticipantClass;

class ParticipantLogin extends ParticipantClass\Participant {

    public function __construct() {
        $ParticipantClass = new ParticipantClass\Participant();
    }

    private $Participanttoken;
    private $loginupdateddate;
    private $logincreateddate;

    public function getParticipantToken() {
        return $this->Participanttoken;
    }

    public function setParticipantToken($Participanttoken) {
        $this->Participanttoken = $Participanttoken;
    }

    public function getLoginUpdate() {
        return $this->loginupdateddate;
    }

    public function setLoginUpdate($loginupdateddate) {
        $this->loginupdateddate = $loginupdateddate;
    }

    public function getLoginCreated() {
        return $this->logincreateddate;
    }

    public function setLoginCreated($logincreateddate) {
        $this->logincreateddate = $logincreateddate;
    }

     public function check_auth() {

        $CI = & get_instance();
        $CI->load->model('Participant_model');
        $login_attempt = 3;
        //Get first user info by user name		
        $participantRecord = $CI->Participant_model->valid_participant($this);
        $getPassword = $this->getClientPassword();
        //return $user_details = password_encrpt($getPassword);
        $response = array();
		
        if (!empty($participantRecord) > 0){
			
            if ($participantRecord->loginattempt < $login_attempt){
				if($participantRecord->archive==0){
				    if (password_validate($getPassword, $participantRecord->password)) {
                        if ($participantRecord->portal_access == 1) {
                            if ($participantRecord->status) {
                                // if success user name password then generate token and insert token in tbl_participant_login table
                                $this->setParticipantid($participantRecord->id);
                                $crt_date = $this->getLoginCreated();
                                $crt_date = $crt_date . $this->getParticipantid();
    
                                $generatetoken = generate_token($crt_date . DATE_TIME);
    
                                $this->setParticipantToken($generatetoken);
                                $responseToken = $CI->Participant_model->insert_participant_token($this);
                                if ($responseToken > 0) {
                                    $this->setLoginattempt(-1);
                                    $responseLoginAttempt = $CI->Participant_model->update_login_attempt($this);
    
                                    //Success									
                                    $response = array('token' => $generatetoken, 'ocs_id' => $this->getParticipantid(), 'status' => true, 'success' => system_msgs('success_login'));
                                } else {
                                    // token not insert in tbl_member_login
                                    $response = array('status' => false, 'error' => system_msgs('wrong_username_password'));
                                }
                            } else {
                                // if client/participant archive(0)
                                $response = array('status' => false, 'error' => system_msgs('dont_have_portal_access'));
                            }
                    } else {
                        // if client/participant access status incative(0)
                        $response = array('status' => false, 'error' => system_msgs('account_not_active'));
                    }
                } else {
                        // if passthru not match
                        $response = array('status' => false, 'error' => system_msgs('wrong_username_password')); 
                }
				}else{
					// if client/participant archive(0)
                   $response = array('status' => false, 'error' => system_msgs('wrong_username_password'));
				}
            } else {
                //if login attempt more then 3 then access disabled by admin
                $response = array('status' => false, 'error' => system_msgs('login_attempt'));
            }
        } else {
            //if user record not found	
            $response = array('status' => false, 'error' => system_msgs('wrong_username_password'));
        }
        return $response;
    }

    public function ResetPassword() {
        $response = array();
        $CI = & get_instance();
        $CI->load->model('Participant_model');
        $participantRecord = $CI->Participant_model->valid_participant($this);
        if (count($participantRecord) > 0) {
            //Success									
            $response = array('token' => $participantRecord->email, 'status' => true, 'success' => system_msgs('success_login'));
        } else {
            // if client/participant archive(0)
            $response = array('status' => false, 'error' => system_msgs('user_not_exist'));
        }
        return $response;
    }

    // when user come at form forgot password
    public function reset_password() {
        $CI = & get_instance();
        $encry_password = password_hash($this->getClientPassword(), PASSWORD_BCRYPT);

        $userData = array('password' => $encry_password, 'token' => '');
        $result = $CI->basic_model->update_records('participant', $userData, $where = array('id' => $this->getParticipantid()));

        return $result;
    }

    public function verify_token() {
        $CI = & get_instance();
        $where = array('id' => $this->getParticipantid(), 'token' => $this->getParticipantToken());
        $result = $CI->basic_model->get_record_where('participant', array('firstname', 'lastname'), $where);
        return $result;
    }

}
