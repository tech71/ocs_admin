<?php

use classRoles as adminRoles;

defined('BASEPATH') OR exit('No direct script access allowed');
global $requestClient;

//class Master extends MX_Controller
class Message_Notification extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation', 'logs_lib'));
        $this->load->model('Message_Notification_model');
        $this->load->helper(array('comman', 'auth'));
        $clientResponse = client_auth_status();

        if (!$clientResponse['status']) {
            echo json_encode($clientResponse);
            exit;
            //redirect('/participant/Auth/login');
        }
    }

    function get_all_messages() {
        $reqData = request_handler();
        $client_id = get_client_id();

        $reqData = json_decode($reqData);
        $result = $this->Message_Notification_model->get_external_messages($reqData, $client_id);

        echo json_encode($result);
    }

    function get_single_message() {
        $reqData = request_handler();
        $client_id = get_client_id();

        if (!empty($reqData->id)) {

            $return = $this->Message_Notification_model->get_single_chat($reqData->id, $client_id);
        } else {
            $return = array('status' => FALSE, 'error' => 'No id found');
        }

        echo json_encode($return);
    }

    function reply_message() {
        $reqData = request_handler();
        $client_id = get_client_id();

        if (!empty($reqData)) {
            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'messageId', 'label' => 'message Id', 'rules' => 'required'),
                array('field' => 'replyAnser', 'label' => 'mail data', 'rules' => 'required'),
                array('field' => 'lastContentId', 'label' => 'last Content Id', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run() === true) {

                $this->Message_Notification_model->reply_mail($reqData, $client_id);

                $response = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function archiveMessage() {
        $reqData = request_handler();
        $participantId = get_client_id();

        if (!empty($reqData)) {
            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'id', 'label' => 'message Id', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run() === true) {

                $where = array('messageId' => $reqData->id, 'userId' => $participantId, 'user_type' => 2);
                $this->basic_model->update_records('external_message_action', array('archive' => 1), $where);

                $response = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }
        } else {
            $response = array('status' => false, 'error' => 'Invalid Request');
        }

        echo json_encode($response);
    }

    public function add_favourite_message() {
        $reqData = request_handler();
        $participantId = get_client_id();

        if (!empty($reqData)) {
            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'is_fav', 'label' => 'favourite status', 'rules' => 'required'),
                array('field' => 'id', 'label' => 'subject', 'id' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $where = array('messageId' => $reqData->id, 'userId' => $participantId, 'user_type' => 2);
                $this->basic_model->update_records('external_message_action', array('is_fav' => $reqData->is_fav), $where);
                $response = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function favourite_message_all() {
        $reqData = request_handler();
        if (!empty($reqData)) {

            foreach ($reqData as $key => $value) {
                if ($value) {
                    $where = ['messageId' => $key, 'userId' => $client_id];
                    $this->basic_model->update_records('external_message_action', array('is_fav' => 1), $where);
                }
            }

            echo json_encode(array('status' => true));
        }
    }

    public function archive_message_all() {
        $reqData = request_handler();
        $client_id = get_client_id();

        if (!empty($reqData)) {

            foreach ($reqData as $key => $value) {
                if ($value) {
                    $where = ['messageId' => $key, 'userId' => $client_id];
                    $this->basic_model->update_records('external_message_action', array('archive' => 1), $where);
                }
            }
            echo json_encode(array('status' => true));
        }
    }

    function get_message_notification_alert() {
        $reqData = request_handler();
        $client_id = get_client_id();

        require_once APPPATH . 'Classes/imail/ExternalMessageContent.php';
        $objExImail = new Imail\externalMessageContent\ExternalMessageContent();
        $objExImail->setUserId($client_id);

        $response = $objExImail->getMessageNotification();
        if (empty($response)) {
            $response = array();
        }
        $return = array('status' => true, 'data' => $response);
        echo json_encode($return);
    }

}
