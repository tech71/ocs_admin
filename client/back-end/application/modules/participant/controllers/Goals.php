<?php

use classRoles as adminRoles;

defined('BASEPATH') OR exit('No direct script access allowed');
global $requestClient;

//class Master extends MX_Controller
class Goals extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation', 'logs_lib'));
        $this->load->helper(array('comman', 'auth'));
        $clientResponse = client_auth_status();
        if (!$clientResponse['status']) {
            echo json_encode($clientResponse);
            exit;
            //redirect('/participant/Auth/login');
        }
    }

    public function add_participant_goal() {
        $reqData = request_handler();
        require_once APPPATH . 'Classes/participant/ParticipantGoal.php';
        $objparticipantgoal = new ParticipantGoalClass\ParticipantGoal();
        $participant_goal_data = array();
        $participant_goal_data['participant'] = get_client_id();
        $participant_goal_data['title'] = $reqData->title;
        $participant_goal_data['start_date'] = $reqData->start_date;
        $participant_goal_data['end_date'] = $reqData->end_date;
        $response_valid = $this->validate_participant_data($participant_goal_data);
        if ($response_valid['status']) {
            //$objparticipantgoal->setParticipantgoalid();
            $objparticipantgoal->setParticipantid($participant_goal_data['participant']);
            $objparticipantgoal->setTitle($participant_goal_data['title']);
            $objparticipantgoal->setStartDate(date("Y-m-d", strtotime($reqData->start_date)));
            $objparticipantgoal->setEndDate(date("Y-m-d", strtotime($reqData->end_date)));
            $objparticipantgoal->setStatus(1);
            $objparticipantgoal->setCreated(date("Y-m-d H:i:s"));
            $response = $objparticipantgoal->AddParticipantGoal($objparticipantgoal);
            if ($response) {
                echo json_encode(array('status' => true));
            } else {
                json_encode(array('status' => false));
            }
        } else {
            echo json_encode($response_valid);
        }
    }

    public function update_participant_goal() {
        $reqData = request_handler();
        require_once APPPATH . 'Classes/participant/ParticipantGoal.php';
        $objparticipantgoal = new ParticipantGoalClass\ParticipantGoal();
        $participant_goal_data = array();
        $participant_goal_data['participant'] = get_client_id();
        $participant_goal_data['title'] = $reqData->title;
        $participant_goal_data['id'] = $reqData->id;
        $participant_goal_data['start_date'] = $reqData->start_date;
        $participant_goal_data['end_date'] = $reqData->end_date;
        $participant_goal_data['status'] = $reqData->status;
        $response_valid = $this->validate_participant_data($participant_goal_data);
        if ($response_valid['status']) {
            $objparticipantgoal->setParticipantgoalid($participant_goal_data['id']);
            $objparticipantgoal->setParticipantid($participant_goal_data['participant']);
            $objparticipantgoal->setTitle($participant_goal_data['title']);
            $objparticipantgoal->setStartDate(date("Y-m-d", strtotime($reqData->start_date)));
            $objparticipantgoal->setEndDate(date("Y-m-d", strtotime($reqData->end_date)));
            $objparticipantgoal->setStatus($reqData->status);
            $objparticipantgoal->setCreated(date("Y-m-d H:i:s"));
            $response = $objparticipantgoal->UpdateParticipantGoal($objparticipantgoal);
            if ($response) {
                echo json_encode(array('status' => true));
            } else {
                echo json_encode(array('status' => false));
            }
        } else {
            echo json_encode($response_valid);
        }
    }

    public function archive_participant_goal() {
        $reqData = request_handler();
        require_once APPPATH . 'Classes/participant/ParticipantGoal.php';
        $objparticipantgoal = new ParticipantGoalClass\ParticipantGoal();
        if (!empty($reqData->id)) {
            $objparticipantgoal->setParticipantgoalid($reqData->id);
            $objparticipantgoal->setParticipantid(get_client_id());
            $response = $objparticipantgoal->ArchiveParticipantGoal($objparticipantgoal);
            if ($response) {
                echo json_encode(array('status' => true));
            } else {
                echo json_encode(array('status' => false));
            }
        } else {
            echo json_encode(array('status' => FALSE, 'error' => 'No id found'));
        }
    }

    function validate_participant_data($participant_goal_data) {
        try {
            $validation_rules = array(
                array('field' => 'title', 'label' => 'goal title', 'rules' => 'trim|required'),
                array('field' => 'date_checked', 'label' => 'Date Checked', 'rules' => 'callback_check_goal_validation[' . json_encode($participant_goal_data) . ']'),
                array('field' => 'participant', 'label' => 'Participant', 'rules' => 'required')
            );
            $this->form_validation->set_data($participant_goal_data);
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }

    function check_goal_validation($start_time, $participant_goaldata) {
        $goalData = json_decode($participant_goaldata);

        if (!empty($goalData)) {
            $strTime = strtotime(DateFormate($goalData->start_date, 'Y-m-d'));
            $endTime = strtotime(DateFormate($goalData->end_date, 'Y-m-d'));
            $strTimeCurrunt = strtotime('Y-m-d');

            if (!empty($strTime)) {
                $id = 0;
                if (array_key_exists("id", $goalData)) {
                    $id = $goalData->id;
                }
                if ($id > 0) {
                    
                } else {
                    if ($strTime >= $strTimeCurrunt) {
                        
                    } else {
                        $this->form_validation->set_message('check_goal_validation', 'Goal start time can not less then current time');
                        return false;
                    }
                }
            } else {
                $this->form_validation->set_message('check_goal_validation', 'Goal start date time required can not empty');
                return false;
            }

            if (!empty($endTime)) {
                
            } else {
                $this->form_validation->set_message('check_goal_validation', 'Goal end date time required can not empty');
                return false;
            }

            if ($strTime > $endTime) {
                $this->form_validation->set_message('check_goal_validation', 'Goal end time not less then start time');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_goal_validation', 'Goal data required can not empty');
            return false;
        }
        return true;
    }

    function get_participant_goal() {
        //$reqData = request_handler();       
        require_once APPPATH . 'Classes/participant/ParticipantGoal.php';
        $objparticipantgoal = new ParticipantGoalClass\ParticipantGoal();
        $objparticipantgoal->setParticipantid(get_client_id());
        $response = $objparticipantgoal->GetParticipantGoal($objparticipantgoal);
        echo json_encode($response);
    }

    function get_participant_goal_history() {
        //$reqData = request_handler();       
        require_once APPPATH . 'Classes/participant/ParticipantGoal.php';
        $objparticipantgoal = new ParticipantGoalClass\ParticipantGoal();
        $objparticipantgoal->setParticipantid(get_client_id());
        $response = $objparticipantgoal->GetParticipantGoalHistory($objparticipantgoal);
        echo json_encode($response);
    }

    function export_goal_report() {

        require_once APPPATH . 'Classes/participant/ParticipantGoal.php';
        $objparticipantgoal = new ParticipantGoalClass\ParticipantGoal();
        $objparticipantgoal->setParticipantid(get_client_id());
        $response = $objparticipantgoal->GetParticipantGoalHistoryReport($objparticipantgoal);

        $this->load->library('m_pdf');
        $pdf = $this->m_pdf->load();
        $html = '<div style="text-align:left; border-bottom:1px solid #000; padding-bottom:10px; margin-bottom:15px;"><img src="./uploads/assets/ocs_logo.svg"></div>';
        $cnt = 0;
        if (!empty($response['data'])) {
            foreach ($response['data'] as $key => $value) {

                $html .= '<div style="border-bottom:1px solid #000; font-size:19px; margin-top:25px;"><b>Title:</b><span style="font-weight:300;">' . $value->title . '</span></div><table width="100%" style="font-family: sans-serif; margin-top:15px; margin-bottom:5px;" ><tr><td><p style="font-size:15px;"><b>Start Date</b> ' . date("d/m/Y", strtotime($value->start_date)) . '</p></td><td align="right"><p style="font-size:15px;"><b>End Date</b> ' . date("d/m/Y", strtotime($value->end_date)) . '</p></td></tr></table>';

                if (!empty($value->rating)) {
                    $html .= '<table border="1" cellpadding="5px"  width="100%" style="border-collapse: collapse; border: 1px solid #880000; font-family: Mono; font-size: 12px; font-family: sans-serif; font "><thead><tr> <th>Date</th> <th>Rating</th></tr></thead><tbody>';
                    foreach ($value->rating as $rating) {
                        $html .= '<tr><td>' . date("d/m/Y", strtotime($rating->created)) . '</td> <td>' . $rating->name . '</td></tr>';
                    }
                    $html .= '</tbody></table>';
                }
            }
            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . date('d-m-Y H:i:s')); // Add a footer
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $rand = rand(100000, 5000000);
            $filename = $rand . '.pdf';
            $pdfFilePath = './archive/' . $filename;
            $pdf->Output($pdfFilePath, 'F');
            echo json_encode(array('status' => true, 'filename' => $filename));
        } else {
            echo json_encode(array('status' => false));
        }
    }

}
