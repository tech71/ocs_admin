<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Goals_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function add_participant_goal($objparticipant_goal) {
        $participant_goal = array();
        $participant_goal['participantId'] = $objparticipant_goal->getParticipantid();
        $participant_goal['title'] = $objparticipant_goal->getTitle();
        $participant_goal['start_date'] = $objparticipant_goal->getStartDate();
        $participant_goal['end_date'] = $objparticipant_goal->getEndDate();
        $participant_goal['created'] = $objparticipant_goal->getCreated();
        $participant_goal['status'] = $objparticipant_goal->getStatus();
        if (IS_APPROVE) {
            $insert_query = $this->db->insert(TBL_PREFIX . 'participant_goal', $participant_goal);
        }

        $participantId = $objparticipant_goal->getParticipantid();
        $approval_area = 'ParticipantGoalAdd';
        $approval_content = json_encode($participant_goal);
        $this->AddApproval($participantId, $approval_area, $approval_content);

        // Notification Add Goal Message
        $arrNotification = notification_msgs('add_new_goal');
        AddNotification($objparticipant_goal->getParticipantid(), $arrNotification['title'], $arrNotification['description']);

        if ($this->db->affected_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function update_participant_goal($objparticipant_goal) {
        $tableParticipant_goal = TBL_PREFIX . 'participant_goal';
        $response = false;
        $update_data = array("title" => $objparticipant_goal->getTitle(), "start_date" => $objparticipant_goal->getStartDate(), "end_date" => $objparticipant_goal->getEndDate());
        if (IS_APPROVE) {
            $this->db->where('id', $objparticipant_goal->getParticipantgoalid());
            $this->db->update($tableParticipant_goal, $update_data);
        }
        $update_data['id'] = $objparticipant_goal->getParticipantgoalid();

        $participantId = $objparticipant_goal->getParticipantid();
        $approval_area = 'ParticipantGoalUpdate';
        $approval_content = json_encode($update_data);
        $this->AddApproval($participantId, $approval_area, $approval_content);

        // Notification Update Goal Message
        $arrNotification = notification_msgs('update_new_goal');
        AddNotification($objparticipant_goal->getParticipantid(), $arrNotification['title'], $arrNotification['description']);
        //if ($this->db->affected_rows() > 0) {
        //  $response = true;
        // }
        return $response = true;
    }

    function archive_participant_goal($objparticipant_goal) {
        $tableParticipant_goal = TBL_PREFIX . 'participant_goal';
        $response = false;
        $update_data = array("status" => '2');
        if (IS_APPROVE) {
            $this->db->where('id', $objparticipant_goal->getParticipantgoalid());
            $this->db->update($tableParticipant_goal, $update_data);
        }
        $participantId = $objparticipant_goal->getParticipantid();
        $approval_area = 'ParticipantGoalArchive';
        $approval_content = json_encode(array("participant_goalid" => $objparticipant_goal->getParticipantgoalid()));
        $this->AddApproval($participantId, $approval_area, $approval_content);

        // Notification Update Goal Message
        $arrNotification = notification_msgs('participant_archive_goal');
        AddNotification($participantId, $arrNotification['title'], $arrNotification['description']);

        return $response = true;
    }

    /* Get all participant goals and rating */

    function get_participant_goal($objparticipant_goal) {
        $tbl_participant_goal = TBL_PREFIX . 'participant_goal';
        $this->db->select(array('id', 'title', 'start_date', 'end_date', 'status'));
        $this->db->from($tbl_participant_goal);
        $this->db->where(array($tbl_participant_goal . '.participantId' => $objparticipant_goal->getParticipantid()));
        $this->db->where($tbl_participant_goal . '.status', '1');
        $query = $this->db->get(); // or die('MySQL Error: ' . $this->db->_error_number());
        $goals = $query->result();

        $filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        if (!empty($goals)) {
            foreach ($goals as $val) {
                $val->rating = $this->get_goals_data($val->id);
            }
        }
        return array('status' => true, 'count' => $filtered_total, 'data' => $goals);
    }

    function get_goals_data($goalId) {
        $tbl_participant_goal_result = TBL_PREFIX . 'participant_goal_result';
        $days_ago = date('Y-m-d', strtotime('-12 days', strtotime(DATE_TIME)));
        $this->db->select(array('rating', 'created'));
        $this->db->from($tbl_participant_goal_result);
        $this->db->where(array($tbl_participant_goal_result . '.goalId' => $goalId));
        $this->db->limit(12);
        // $this->db->where('created <=', DATE_TIME);
        // $this->db->where('created >=', $days_ago);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        return $query->result();
    }

    function get_participant_goal_history($objparticipant_goal) {
        $tbl_participant_goal = TBL_PREFIX . 'participant_goal';
        $this->db->select(array('id', 'title', 'start_date', 'end_date', 'status'));
        $this->db->from($tbl_participant_goal);
        $this->db->where(array($tbl_participant_goal . '.participantId' => $objparticipant_goal->getParticipantid()));
        $this->db->where($tbl_participant_goal . '.status', '1');
        $query = $this->db->get(); // or die('MySQL Error: ' . $this->db->_error_number());
        $goals = $query->result();
        $filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        if (!empty($goals)) {
            foreach ($goals as $val) {
                $val->rating = $this->get_goals_data_history($val->id);
            }
        }
        $last_query = $this->db->last_query();
        return array('status' => true, 'count' => $filtered_total, 'data' => $goals);
        // return $goals;
    }

    function get_goals_data_history($goalId) {
        $tbl_participant_goal_result = TBL_PREFIX . 'participant_goal_result';
        // $days_ago = date('Y-m-d', strtotime('-12 days', strtotime(DATE_TIME)));
        $this->db->select(array('rating', 'created'));
        $this->db->from($tbl_participant_goal_result);
        $this->db->where(array($tbl_participant_goal_result . '.goalId' => $goalId));
        $this->db->limit(2);
        // $this->db->where('created <=', DATE_TIME);
        // $this->db->where('created >=', $days_ago);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        return $query->result();
    }

    function get_participant_goal_history_report($objparticipant_goal) {
        $tbl_participant_goal = TBL_PREFIX . 'participant_goal';
        $this->db->select(array('id', 'title', 'start_date', 'end_date', 'status'));
        $this->db->from($tbl_participant_goal);
        $this->db->where(array($tbl_participant_goal . '.participantId' => $objparticipant_goal->getParticipantid()));
        $this->db->where($tbl_participant_goal . '.status', '1');
        $query = $this->db->get();
        $goals = $query->result();
        $filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        if (!empty($goals)) {
            foreach ($goals as $val) {
                $val->rating = $this->get_goals_data_history_report($val->id);
            }
        }
        $last_query = $this->db->last_query();
        return array('status' => true, 'count' => $filtered_total, 'data' => $goals);
        // return $goals;
    }

    function get_goals_data_history_report($goalId) {
        $tbl_participant_goal_result = TBL_PREFIX . 'participant_goal_result';
        $tbl_goal_rating = TBL_PREFIX . 'goal_rating';

        $this->db->select(array($tbl_goal_rating . '.name', $tbl_participant_goal_result . '.created'));
        $this->db->from($tbl_participant_goal_result);
        $this->db->where(array($tbl_participant_goal_result . '.goalId' => $goalId));
        //$this->db->limit(2);
        $this->db->join($tbl_goal_rating, $tbl_participant_goal_result . '.rating =' . $tbl_goal_rating . '.rating', 'inner');
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        return $query->result();
    }

    public function get_participant_goal_old($objparticipant_goal) {
        $tbl_participant_goal = TBL_PREFIX . 'participant_goal';
        $tbl_goal_rating = TBL_PREFIX . 'participant_goal_result';
        // `participantId`, `goalId`, `shiftId`, `created`, `rating`
        // Get all addressess                    
        $participant_address_columns = array($tbl_participant_goal . '.id,' . $tbl_participant_goal . '.title,' . $tbl_participant_goal . '.start_date,' . $tbl_participant_goal . '.end_date,' . $tbl_participant_goal . '.status,' . $tbl_participant_goal . '.created,' . $tbl_goal_rating . '.rating');
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $participant_address_columns)), false);
        $this->db->from($tbl_participant_goal);
        $this->db->where(array($tbl_participant_goal . '.participantId' => $objparticipant_goal->getParticipantid()));
        $this->db->join($tbl_goal_rating, $tbl_goal_rating . '.goalId =' . $tbl_participant_goal . '.id', 'inner');
        // $query = $this->db->get();
        // $participant_address = $query->result();
        $result = $this->db->get()->result();
        $filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        return array('status' => true, 'count' => $filtered_total, 'data' => $result);
    }

    private function AddApproval($participantId, $approval_area, $approval_content) {
        $tableApproval = TBL_PREFIX . 'approval';
        $ApprovalData = array();
        $ApprovalData['userId'] = $participantId;
        $ApprovalData['approval_area'] = $approval_area;
        $ApprovalData['approval_content'] = $approval_content;
        $ApprovalData['user_type'] = 2;
        $ApprovalData['created'] = DATE_TIME;
        $ApprovalData['status'] = 0;
        $insert_query = $this->db->insert($tableApproval, $ApprovalData);
        if ($this->db->affected_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

}
