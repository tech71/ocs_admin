<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Participant_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function valid_participant($objparticipant) {
        $username = $objparticipant->getUserName();
        $this->db->select(array('id', 'portal_access', 'password', 'archive', 'status', 'loginattempt'));
        //$this->db->where('archive', 0);
        $this->db->from(TBL_PREFIX . 'participant');
        $this->db->where(array('username' => $username));
        //echo $this->db->last_query();		
        return $this->db->get()->row();
    }

    function insert_participant_token($objparticipant) {
        $this->basic_model->delete_records('participant_login', $where = array('participantId' => $objparticipant->getParticipantid()));
        $arrParticipantLogin = array();
        $arrParticipantLogin['participantId'] = $objparticipant->getParticipantid();
        $arrParticipantLogin['token'] = $objparticipant->getParticipantToken();

        $arrParticipantLogin['ip_address'] = get_client_ip_server();
        $arrParticipantLogin['created'] = $objparticipant->getLoginCreated();
        $arrParticipantLogin['updated'] = $objparticipant->getLoginUpdate();
        $insert_query = $this->db->insert(TBL_PREFIX . 'participant_login', $arrParticipantLogin);

        if ($this->db->affected_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function update_login_attempt($objparticipant) {
        $arrMember = array();
        $login_attemp_count = $objparticipant->getLoginattempt();
        $login_attemp_count++;
        $this->db->set('loginattempt', $login_attemp_count);
        $this->db->where('id', $objparticipant->getParticipantid());
        $this->db->update(TBL_PREFIX . 'participant');
        if ($this->db->affected_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    // Remove all tokens before 30 minuts
    function remove_participant_tokens() {
        $this->db->where("updated < (NOW()-INTERVAL 30 MINUTE)");
        $this->db->delete(TBL_PREFIX . 'participant_login');
        //echo $this->db->last_query();
    }

    // Update token time to currun time
    function update_participant_token($objmember) {
        $reposne = false;
        $updated = $objmember->getLoginUpdate();
        $this->db->set('updated', $updated);
        $this->db->where('token', $objmember->getParticipantToken());
        $this->db->update(TBL_PREFIX . 'participant_login');


        return true;
    }

    // After login check login status every request
    function participant_login_status($objparticipant) {
        $reposne = false;

        // verify domain
        verify_server_request();
        // Remove all rows updated time before 20 minuts
        //$this->remove_participant_tokens();				
        // Get details of token members
        //echo $objparticipant.getParticipantid();

        $token = $objparticipant->getParticipantToken();
        $updated = $objparticipant->getLoginUpdate();
        $this->db->select(array('participantId'));
        $this->db->from(TBL_PREFIX . 'participant_login');
        $this->db->where(array('token' => $token));
        $this->db->where(array('ip_address' => get_client_ip_server()));
        $response_status = $this->db->get()->row();

        if (!empty($response_status)) {
            if ($response_status->participantId == $objparticipant->getParticipantid()) {
                // Update token date time
                if ($this->update_participant_token($objparticipant) > 0) {
                    //$reposne = true;
                    $reposne = array('type' => true, 'val' => 'login');
                } else {
                    //$reposne = false;
                    $reposne = array('type' => false, 'val' => $response_status->participantId);
                }
            } else {
                //$reposne = false;
                $reposne = array('type' => false, 'val' => $response_status->participantId);
            }
        } else {
//                    var_dump($response_status);
            $reposne = array('type' => false, 'val' => count($response_status) . 'status');
        }
        return $reposne;
    }

    function ParticipantInfo($objparticipant) {
        $clientId = $objparticipant->getParticipantid();

        $response_array = array();

        // Profile details
        $tbl_participant = TBL_PREFIX . 'participant';
        $tbl_participant_phone = TBL_PREFIX . 'participant_phone';
        $tbl_participant_email = TBL_PREFIX . 'participant_email';
        $tbl_participant_address = TBL_PREFIX . 'participant_address';
        $tbl_participant_kin = TBL_PREFIX . 'participant_kin';

        $tbl_participant_kin_booking = TBL_PREFIX . 'participant_booking_list';

        $tbl_participant_care = TBL_PREFIX . 'participant_care_requirement';
        $tbl_participant_oc = TBL_PREFIX . 'participant_oc_services';
        $tbl_participant_support_required = TBL_PREFIX . 'participant_support_required';
        $tbl_participant_assistance = TBL_PREFIX . 'participant_assistance';
        $tbl_participant_general = TBL_PREFIX . 'participant_genral';

        $Participant_column = array($tbl_participant . '.preferredname', $tbl_participant . '.id as OCSID,' . $tbl_participant . '.ndis_num as NDISNO,' . $tbl_participant . '.prefer_contact as PreferredContact,' . $tbl_participant . '.firstname as firstname,' . $tbl_participant . '.lastname,' . $tbl_participant . '.middlename,' . $tbl_participant . '.dob,' . $tbl_participant . '.crn_num as CRN,' . $tbl_participant . '.medicare_num as medicare,' . $tbl_participant . '.gender');
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $Participant_column)), false);
        $this->db->from($tbl_participant);

        //$this->db->join($tbl_participant_phone, $tbl_participant_phone.'.participantId ='.$tbl_participant.'.id', 'left');
        //$this->db->join($tbl_participant_email, $tbl_participant_email.'.participantId = '.$tbl_participant.'.id', 'left');

        $this->db->where(array($tbl_participant . '.id' => $clientId));
        $response_status = $this->db->get()->row();

        if (!empty($response_status)) {
            $response_array['profile'] = $response_status;

            // Get all Emaill Address
            $participant_email_columns = array($tbl_participant_email . '.id,' . $tbl_participant_email . '.email,' . $tbl_participant_email . '.primary_email');
            $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $participant_email_columns)), false);
            $this->db->from($tbl_participant_email);
            $this->db->where(array($tbl_participant_email . '.participantId' => $clientId));
            $query = $this->db->get();
            $response_array['emails'] = $query->result();

            //Get all Phone contact                     
            $participant_phone_columns = array($tbl_participant_phone . '.id,' . $tbl_participant_phone . '.phone,' . $tbl_participant_phone . '.primary_phone');
            $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $participant_phone_columns)), false);
            $this->db->from($tbl_participant_phone);
            $this->db->where(array($tbl_participant_phone . '.participantId' => $clientId));
            $query = $this->db->get();
            $response_array['phones'] = $query->result();

            // Get all addressess                    
            $participant_address_columns = array($tbl_participant_address . '.id as addressId,' . $tbl_participant_address . '.street,' . $tbl_participant_address . '.city,' . $tbl_participant_address . '.postal,' . $tbl_participant_address . '.state,' . $tbl_participant_address . '.site_category,' . $tbl_participant_address . '.primary_address', 'tbl_state.name as stateName');

            $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $participant_address_columns)), false);
            $this->db->from($tbl_participant_address);
            $this->db->where(array($tbl_participant_address . '.participantId' => $clientId));
            $this->db->join('tbl_state', 'tbl_state' . '.id =' . $tbl_participant_address . '.state', 'inner');
            $query = $this->db->get();
            $participant_address = $query->result();

            if (!empty($participant_address)) {
                foreach ($participant_address as $val) {
                    $val->city = array('value' => $val->city, 'label' => $val->city);
                }
            }
            $response_array['address'] = $participant_address;

            // get kin details                                       
            $participant_kin_columns = array($tbl_participant_kin . '.id as kinId,' . $tbl_participant_kin . '.firstname,' . $tbl_participant_kin . '.lastname,' . $tbl_participant_kin . '.relation,' . $tbl_participant_kin . '.phone,' . $tbl_participant_kin . '.email,' . $tbl_participant_kin . '.primary_kin');
            $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $participant_kin_columns)), false);
            $this->db->from($tbl_participant_kin);
            $this->db->where(array($tbl_participant_kin . '.participantId' => $clientId, 'archive' => 0));
            $query = $this->db->get();
            $response_array['kinInfo'] = $query->result();


            // get kin Booker details 
            //   `participantId`, ``, ``, ``, ``, ``, `created` 

            $participant_booking_columns = array($tbl_participant_kin_booking . '.id as kinbookingId,' . $tbl_participant_kin_booking . '.firstname,' . $tbl_participant_kin_booking . '.lastname,' . $tbl_participant_kin_booking . '.relation,' . $tbl_participant_kin_booking . '.phone,' . $tbl_participant_kin_booking . '.email');
            $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $participant_booking_columns)), false);
            $this->db->from($tbl_participant_kin_booking);
            $this->db->where(array($tbl_participant_kin_booking . '.participantId' => $clientId, $tbl_participant_kin_booking . '.archive' => '0'));
            $query = $this->db->get();
            $response_array['kinBookerInfo'] = $query->result();

            // get care Rquirment details                    
            $participant_care_columns = array($tbl_participant_care . '.id as care_id,' . $tbl_participant_care . '.preferred_language,' . $tbl_participant_care . '.hearing_interpreter,' . $tbl_participant_care . '.linguistic_interpreter as language_interpreter');
            $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $participant_care_columns)), false);
            $this->db->from($tbl_participant_care);
            $this->db->where(array($tbl_participant_care . '.participantId' => $clientId));
            //$query = $this->db->get();

            $response_array['ParticipantCare'] = $this->db->get()->row(); //$query->result();
            //get oc services
            $participant_oc_service = array($tbl_participant_general . '.id as id,' . $tbl_participant_general . '.name, (IF(tbl_participant_oc_services.oc_service >0,TRUE,FALSE)) as service_id');
            $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $participant_oc_service)), false);
            $this->db->from($tbl_participant_oc);
            $this->db->join($tbl_participant_general, $tbl_participant_oc . '.oc_service =' . $tbl_participant_general . '.id AND ' . $tbl_participant_oc . '.participantId =' . $clientId, 'right');
            $where_services = array($tbl_participant_general . '.type' => 'oc_service', $tbl_participant_general . '.status' => '1');
            $this->db->where($where_services);
            $query = $this->db->get();
            $response_array['OcServices'] = $query->result();

            //get Support required         
            $participant_Support = array($tbl_participant_general . '.id as id,' . $tbl_participant_general . '.name, (IF(tbl_participant_support_required.support_required >0,TRUE,FALSE)) as service_id');
            $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $participant_Support)), false);
            $this->db->from($tbl_participant_support_required);
            $this->db->join($tbl_participant_general, $tbl_participant_support_required . '.support_required =' . $tbl_participant_general . '.id AND ' . $tbl_participant_support_required . '.participantId =' . $clientId, 'right');
            $where_services = array($tbl_participant_general . '.type' => 'support', $tbl_participant_general . '.status' => '1');
            $this->db->where($where_services);
            $query = $this->db->get();
            $response_array['Support_Required'] = $query->result();

            // $tbl_participant_assistance //

            $participant_assistance_service = array($tbl_participant_general . '.id as id,' . $tbl_participant_general . '.name, (IF(tbl_participant_assistance.assistanceId >0,TRUE,FALSE)) as service_id');
            $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $participant_assistance_service)), false);
            $this->db->from($tbl_participant_assistance);
            $this->db->join($tbl_participant_general, $tbl_participant_assistance . '.assistanceId =' . $tbl_participant_general . '.id AND ' . $tbl_participant_assistance . '.participantId =' . $clientId, 'right');
            $where_services = array($tbl_participant_general . '.type' => 'assistance', $tbl_participant_general . '.status' => '1');
            $this->db->where($where_services);
            $query = $this->db->get();
            $response_array['Assistance_Services'] = $query->result();

            // mobility
            $participant_assistance_service = array($tbl_participant_general . '.id as id,' . $tbl_participant_general . '.name, (IF(tbl_participant_assistance.assistanceId >0,TRUE,FALSE)) as service_id');
            $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $participant_assistance_service)), false);
            $this->db->from($tbl_participant_assistance);
            $this->db->join($tbl_participant_general, $tbl_participant_assistance . '.assistanceId =' . $tbl_participant_general . '.id AND ' . $tbl_participant_assistance . '.participantId =' . $clientId, 'right');
            $where_services = array($tbl_participant_general . '.type' => 'mobality', $tbl_participant_general . '.status' => '1');
            $this->db->where($where_services);
            $query = $this->db->get();
            $response_array['mobility_services'] = $query->result();
        }
        return $response_array;
    }

    public function ParticipantPreferences($objparticipant) {

        $clientId = $objparticipant->getParticipantid();
        $response_array = array();

        $tbl_participant_activity = TBL_PREFIX . 'participant_activity';
        $tbl_participant_place = TBL_PREFIX . 'participant_place';
        $tbl_activity = TBL_PREFIX . 'activity';
        $tbl_place = TBL_PREFIX . 'place';

        // Get all Participant Preferences Activity
        $participant_activity_columns = array($tbl_activity . '.id, (IFNULL(tbl_participant_activity.type,0)) as type', $tbl_activity . '.name');
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $participant_activity_columns)), false);
        $this->db->from($tbl_participant_activity);
        $this->db->join($tbl_activity, $tbl_participant_activity . '.activityId =' . $tbl_activity . '.id AND ' . $tbl_participant_activity . '.participantId =' . $clientId, 'right');
        $query = $this->db->get();
        //echo $this->db->last_query()
        $response_array['Activity'] = $query->result();

        // Get all Participant Preferences Places 
        $participant_place_columns = array($tbl_place . '.id, (IFNULL(tbl_participant_place.type,0)) as type', $tbl_place . '.name');
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $participant_place_columns)), false);
        $this->db->from($tbl_participant_place);
        $this->db->join($tbl_place, $tbl_participant_place . '.placeId =' . $tbl_place . '.id AND ' . $tbl_participant_place . '.participantId =' . $clientId, 'right');
        $query = $this->db->get();
        $response_array['Place'] = $query->result();
        return $response_array;
    }

    public function update_participant($objparticipant) {
        $reposne = false;

        $tableParticipant = TBL_PREFIX . 'participant';
        $participantId = $objparticipant->getParticipantid();
        $this->db->trans_begin();
        $arrParticipant = array();
        $arrParticipant['firstname'] = $objparticipant->getFirstname();
        $arrParticipant['middlename'] = $objparticipant->getMiddlename();
        $arrParticipant['lastname'] = $objparticipant->getLastname();
        $arrParticipant['dob'] = date('Y-m-d', strtotime($objparticipant->getDob()));
        $arrParticipant['gender'] = $objparticipant->getGender();
        $arrParticipant['prefer_contact'] = $objparticipant->getContactType();
        $arrParticipant['preferredname'] = $objparticipant->getPreferredname();
        $arrParticipant['crn_num'] = $objparticipant->getCrnNum();
        $arrParticipant['ndis_num'] = $objparticipant->getNdisNum();
        $arrParticipant['medicare_num'] = $objparticipant->getMedicareNum();
//        $arrParticipant['prefer_contact'] = $objparticipant->getPreferredname();
        // Here update first Participant info

        $this->db->where('id', $participantId);
        $this->db->update($tableParticipant, $arrParticipant);

        // Insert/Update Participant Kin details		
        $participantKin = $objparticipant->getKinDetails();
        $this->UpdateParticipantKinDetails($participantKin, $participantId);


        // Insert/Update Participant Emails 
        $participantEmail = $objparticipant->getParticipantEmail();
        $this->UpdateParticipantEmails($participantEmail, $participantId);

        // Insert/Update Participant Phone		
        $participantPhone = $objparticipant->getParticipantPhone();
        $this->UpdateParticipantPhones($participantPhone, $participantId);
        /*
          // Insert/Update Participant Address
          $addressDetails=$objparticipant->getParticipantAddresse();
          $this->UpdateParticipantAddresses($addressDetails,$participantId);
         */
        // Commit of rolledback
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
            $reposne = true;
        }
        return $reposne;
    }

    // Done
    public function UpdateParticipantProfile($objparticipant) {
        $tableParticipant = TBL_PREFIX . 'participant';

        $arrParticipant = array();
        $arrParticipant['firstname'] = $objparticipant->getFirstname();
        $arrParticipant['middlename'] = $objparticipant->getMiddlename();
        $arrParticipant['lastname'] = $objparticipant->getLastname();
        $arrParticipant['dob'] = date('Y-m-d', strtotime($objparticipant->getDob()));
        $arrParticipant['gender'] = $objparticipant->getGender();
        $arrParticipant['prefer_contact'] = $objparticipant->getContactType();
        $arrParticipant['preferredname'] = $objparticipant->getPreferredname();
        $arrParticipant['crn_num'] = $objparticipant->getCrnNum();
        $arrParticipant['ndis_num'] = $objparticipant->getNdisNum();
        $arrParticipant['medicare_num'] = $objparticipant->getMedicareNum();

        // Update Profile Info
        if (IS_APPROVE) {
            $this->db->where('id', $objparticipant->getParticipantid());
            $this->db->update($tableParticipant, $arrParticipant);
        }
        // Add Approval table		
        $participantId = $objparticipant->getParticipantid();
        $approval_area = 'ProfileUpdate';
        $approval_content = json_encode($arrParticipant);
        $this->AddApproval($participantId, $approval_area, $approval_content);

        // Notification Message
        $arrNotification = notification_msgs('update_your_detail');
        AddNotification($objparticipant->getParticipantid(), $arrNotification['title'], $arrNotification['description']);
    }

    public function UpdateParticipantPhoneEmail($objparticipant) {
        $this->UpdateParticipantEmails($objparticipant->getParticipantEmail(), $objparticipant->getParticipantid());
        $this->UpdateParticipantPhones($objparticipant->getParticipantPhone(), $objparticipant->getParticipantid());

        $arrNotification = notification_msgs('update_contact_details');
        AddNotification($objparticipant->getParticipantid(), $arrNotification['title'], $arrNotification['description']);
    }

    public function UpdateCareRequirement($objCareRequirement) {

        $reposne = false;
        $participantCareId = $objCareRequirement->getParticipantcarerequirementid();
        $participantId = $objCareRequirement->getParticipantid();

        $tableParticipantCare = TBL_PREFIX . 'participant_care_requirement';
        $tblSupportRequired = TBL_PREFIX . 'participant_support_required';
        $tblAssistance = TBL_PREFIX . 'participant_assistance';
        $tblOcServices = TBL_PREFIX . 'participant_oc_services';
        $arrApprovalCare = array();
        $arrParticipantCare = array();

        $arrParticipantCare['participantId'] = $objCareRequirement->getParticipantid();
        $arrParticipantCare['preferred_language'] = $objCareRequirement->getPreferredLanguage();
        $arrParticipantCare['linguistic_interpreter'] = $objCareRequirement->getLinguisticInterpreter();
        $arrParticipantCare['hearing_interpreter'] = $objCareRequirement->getHearingInterpreter();

        $getAssistance = $objCareRequirement->getRequireAssistanceOther();
        $getSupportRequired = $objCareRequirement->getSupportRequireOther();
        $getOCServices = $objCareRequirement->getOCServices();
        $getMobilityAssistance = $objCareRequirement->getMobilityAssistance();

        // Here update first Participant Careinfo
        $this->db->trans_begin();
        if (IS_APPROVE) {
            if ($participantCareId > 0) {
                $this->db->where('id', $participantCareId);
                $this->db->update($tableParticipantCare, $arrParticipantCare);
                $reposne = true;
            } else {
                $insert_query = $this->db->insert($tableParticipantCare, $arrParticipantCare);
                if ($this->db->affected_rows() > 0) {
                    $reposne = true;
                } else {
                    $reposne = false;
                }
            }
        }

        $arrApprovalCare['ParticipantCare'] = $arrParticipantCare;

        // Care Update Approval table				
        /* 	$approval_area='ParticipantCareUpdate';		
          $approval_content=json_encode($arrParticipantCare);
          $this->AddApproval($participantId,$approval_area,$approval_content);

          // Notification Care Update Message
          $arrNotification=notification_msgs('update_participant_care');
          AddNotification($participantId,$arrNotification['title'],$arrNotification['description']);
         */

        // Remove all SupportRequire, OCServices , Require Assistance
        $whereColumnName = 'participantId';
        if (IS_APPROVE) {
            $this->removeAllRows($tblSupportRequired, $whereColumnName, $participantId);
            // $this->removeAllRows($tblAssistance, $whereColumnName, $participantId);
            // Mobility Type
            $categories = array('assistance', 'mobality');
            $this->db->where($whereColumnName, $participantId);
            $this->db->where_in('type', $categories);
            $this->db->delete($tblAssistance);
            $this->removeAllRows($tblOcServices, $whereColumnName, $participantId);
        }

        // Participant Support Insert
        $arrSupport = array();
        $arrAssistance = array();
        $arrOCServices = array();
        $oprationType = 'insert';
        if (count($getAssistance) > 0) {
            foreach ($getAssistance as $assistance) {

                $arrParticipantassistance = array();
                if ($assistance->service_id == 1) {
                    $arrParticipantassistance['participantId'] = $participantId;
                    $arrParticipantassistance['assistanceId'] = $assistance->id;
                    $arrParticipantassistance['type'] = 'assistance';
                    $arrAssistance[] = $arrParticipantassistance;
                }
            }

            // Add Approval table			
            /* 	$approval_area='AssistanceUpdate';		
              $approval_content=json_encode($arrAssistance);
              $this->AddApproval($participantId,$approval_area,$approval_content);
             */
            $arrApprovalCare['AssistanceUpdate'] = $arrAssistance;

            if (IS_APPROVE) {
                $this->bulk_record_opration($tblAssistance, $oprationType, null, null, $arrAssistance);
            }
            // Notification Assistance Update Message
            //	$arrNotification=notification_msgs('update_assistance_required');			
            //	AddNotification($participantId,$arrNotification['title'],$arrNotification['description']);
        }

        // Mobility 		
        $oprationType = 'insert';
        if (count($getMobilityAssistance) > 0) {
            foreach ($getMobilityAssistance as $assistance) {

                $arrMobilityassistance = array();
                if ($assistance->service_id == 1) {
                    $arrMobilityassistance['participantId'] = $participantId;
                    $arrMobilityassistance['assistanceId'] = $assistance->id;
                    $arrMobilityassistance['type'] = 'mobality';
                    $arrMobilityServices[] = $arrMobilityassistance;
                }
            }

            // Add Approval table			
            /* $approval_area='MobilityUpdate';		
              $approval_content=json_encode($arrMobilityServices);
              $this->AddApproval($participantId,$approval_area,$approval_content);
             */
            $arrApprovalCare['MobilityUpdate'] = $arrMobilityServices;

            if (IS_APPROVE) {
                $this->bulk_record_opration($tblAssistance, $oprationType, null, null, $arrMobilityServices);
            }
            // Notification Assistance Update Message
            //	$arrNotification=notification_msgs('update_mobility_required');			
            //	AddNotification($participantId,$arrNotification['title'],$arrNotification['description']);
        }


        if (count($getSupportRequired) > 0) {
            foreach ($getSupportRequired as $support) {
                $arrParticipantSupport = array();
                if ($support->service_id == 1) {
                    $arrParticipantSupport['participantId'] = $participantId;
                    $arrParticipantSupport['support_required'] = $support->id;
                    $arrSupport[] = $arrParticipantSupport;
                }
            }

            // Add Approval table					
            /* $approval_area='SupportRequiredUpdate';		
              $approval_content=json_encode($arrSupport);
              $this->AddApproval($participantId,$approval_area,$approval_content);
             */
            $arrApprovalCare['SupportRequiredUpdate'] = $arrSupport;

            if (IS_APPROVE) {
                $this->bulk_record_opration($tblSupportRequired, $oprationType, null, null, $arrSupport);
            }
            // Notification Assistance Update Message
            //	$arrNotification=notification_msgs('update_support_required');			
            //	AddNotification($participantId,$arrNotification['title'],$arrNotification['description']);
        }

        if (count($getOCServices) > 0) {
            foreach ($getOCServices as $ocServices) {
                $arrParticipantOC = array();
                if ($ocServices->service_id == 1) {
                    $arrParticipantOC['participantId'] = $participantId; //getParticipantid();
                    $arrParticipantOC['oc_service'] = $ocServices->id;
                    $arrOCServices[] = $arrParticipantOC;
                }
            }

            $arrApprovalCare['OCServicesUpdate'] = $arrOCServices;
            /*
              // Add Approval table
              $approval_area='OCServicesUpdate';
              $approval_content=json_encode($arrOCServices);
              $this->AddApproval($participantId,$approval_area,$approval_content);
             */

            $approval_area = 'CareRequirment';
            $approval_content = json_encode($arrApprovalCare);
            $this->AddApproval($participantId, $approval_area, $approval_content);

            if (IS_APPROVE) {
                $this->bulk_record_opration($tblOcServices, $oprationType, null, null, $arrOCServices);
            }
            // Notification Assistance Update Message
            //	$arrNotification=notification_msgs('update_oc_services');			
            //	AddNotification($participantId,$arrNotification['title'],$arrNotification['description']);
        }
        $arrNotification = notification_msgs('update_care_requirements');
        AddNotification($participantId, $arrNotification['title'], $arrNotification['description']);

        // Commit of rolledback
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $reposne = false;
        } else {
            $this->db->trans_commit();



            $reposne = true;
        }
        return $reposne;
    }

    public function UpdateParticipantPreferences($objParticipant) {
        $reposne = false;
        $tableParticipantActivity = TBL_PREFIX . 'participant_activity';

        $ParticipantId = $objParticipant->getParticipantid();
        $ParticipantActivities = $objParticipant->getParticipantActivity();

        $this->db->trans_begin();
        // Remove all activity and places using Participant id
        if (IS_APPROVE) {
            $this->removeAllParticipantActivity($ParticipantId);
        }
        // Insert/Update Activity 
        $bulk_insert = array();
        if (count($ParticipantActivities) > 0) {
            foreach ($ParticipantActivities as $activity) {
                $arrActivity = array("activityId" => $activity->id, "type" => $activity->type, 'participantId' => $ParticipantId);
                $bulk_insert[] = $arrActivity;
            }
            if (count($bulk_insert) > 0) {

                // Add Approval table							
                $approval_area = 'PreferredActivityUpdate';
                $approval_content = json_encode($bulk_insert);
                $this->AddApproval($ParticipantId, $approval_area, $approval_content);
                if (IS_APPROVE) {
                    $this->bulk_record_opration($tableParticipantActivity, 'insert', null, null, $bulk_insert);
                }
                // Notification Assistance Update Message
                $arrNotification = notification_msgs('update_preferred_activities');
                AddNotification($ParticipantId, $arrNotification['title'], $arrNotification['description']);
            }
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
            $reposne = true;
        }
        return $reposne;
    }

    public function UpdateParticipantPreferencesPlaces($objParticipant) {

        $reposne = false;
        $tableParticipantPlace = TBL_PREFIX . 'participant_place';

        $ParticipantId = $objParticipant->getParticipantid();
        $ParticipantPlaces = $objParticipant->getParticipantPlace();

        $this->db->trans_begin();
        // Remove all activity and places using Participant id
        if (IS_APPROVE) {
            $this->removeAllParticipantPlaces($ParticipantId);
        }
        $bulk_insert = array();
        // Insert All Places selected by Participant
        if (count($ParticipantPlaces) > 0) {
            foreach ($ParticipantPlaces as $places) {

                $arrPlaces = array("placeId" => $places->id, "type" => $places->type, 'participantId' => $ParticipantId);
                $bulk_insert[] = $arrPlaces;
            }
            if (count($bulk_insert) > 0) {

                // Add Approval table							
                $approval_area = 'PreferredPlacesUpdate';
                $approval_content = json_encode($bulk_insert);
                $this->AddApproval($ParticipantId, $approval_area, $approval_content);

                if (IS_APPROVE) {
                    $this->bulk_record_opration($tableParticipantPlace, 'insert', null, null, $bulk_insert);
                }
                // Notification Assistance Update Message
                $arrNotification = notification_msgs('update_preferred_places');
                AddNotification($ParticipantId, $arrNotification['title'], $arrNotification['description']);
            }
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
            $reposne = true;
        }
        return $reposne;
    }

    // =================== Private Methods ===========================
    private function bulk_record_opration($table, $type, $wherekey, $wherevalue, $memberData) {
        if ($type == 'insert') {
            $insert_query = $this->db->insert_batch($table, $memberData);
            $this->db->last_query();
            if ($this->db->affected_rows() > 0) {
                return 1;
            } else {
                return 0;
            }
        } else {
            $this->db->where($wherekey, $wherevalue);
            $this->db->update($table, $memberData);
            $this->db->last_query();
        }
    }

    function UpdateParticipantKinDetails($participantKin) {

        $tableParticipantKin = TBL_PREFIX . 'participant_kin';
        $coulumn = 'participantId';
        $participantId = $participantKin->getParticipantid();
        if (IS_APPROVE) {
            $this->removeAllRows($tableParticipantKin, $coulumn, $participantId);
        }
        $bulk_insert_kin = array();
        $primary_data = 0;
        foreach ($participantKin->getKinDetails() as $kinData) {

            $primary_data++;
            $is_primary = ($primary_data == 1) ? '1' : '2';
            $arrParticipantKin = array();
            $arrParticipantKin['participantId'] = $participantId;
            $arrParticipantKin['firstname'] = $kinData->firstname;
            $arrParticipantKin['lastname'] = $kinData->lastname;
            $arrParticipantKin['relation'] = $kinData->relation;
            $arrParticipantKin['primary_kin'] = $is_primary;
            $arrParticipantKin['email'] = $kinData->email;
            $arrParticipantKin['phone'] = $kinData->phone;
            $arrParticipantKin['id'] = $kinData->kinId;
            $bulk_insert_kin[] = $arrParticipantKin;
        }

        if (count($bulk_insert_kin) > 0) {

            // Add Approval table						
            $approval_area = 'KinUpdate';
            $approval_content = json_encode($bulk_insert_kin);
            $this->AddApproval($participantId, $approval_area, $approval_content);

            if (IS_APPROVE) {
                $oprationType = 'insert';
                $this->bulk_record_opration($tableParticipantKin, $oprationType, null, null, $bulk_insert_kin);
            }
            //insert Notification
            $arrNotification = notification_msgs('update_kin_detail');
            AddNotification($participantId, $arrNotification['title'], $arrNotification['description']);
        }
    }

    private function UpdateParticipantEmails($participantEmail, $participantId) {
        $tableParticipantEmail = TBL_PREFIX . 'participant_email';

        $coulumn = 'participantId';
        if (IS_APPROVE) {
            $this->removeAllRows($tableParticipantEmail, $coulumn, $participantId);
        }
        $primary_data = 0;
        $bulk_insert_email = array();
        foreach ($participantEmail as $email) {
            $primary_data++;
            $is_primary = ($primary_data == 1) ? '1' : '2';
            $arrParticipantEmail = array();
            $arrParticipantEmail['participantId'] = $participantId;
            $arrParticipantEmail['email'] = $email->email;
            $arrParticipantEmail['primary_email'] = $is_primary;
            $bulk_insert_email[] = $arrParticipantEmail;
        }
        if (count($bulk_insert_email) > 0) {



            // Add Approval table

            $approval_area = 'EmailUpdate';
            $approval_content = json_encode($bulk_insert_email);
            $this->AddApproval($participantId, $approval_area, $approval_content);

            if (IS_APPROVE) {
                // Insert Email here
                $oprationType = 'insert';
                $this->bulk_record_opration($tableParticipantEmail, $oprationType, null, null, $bulk_insert_email);
            }
            // Notification Message
            //$arrNotification=notification_msgs('update_email_detail');			
            //	AddNotification($participantId,$arrNotification['title'],$arrNotification['description']);
        }
    }

    public function UpdateParticipantKinBookerDetails($objBooker) {
        $CI = & get_instance();

        $tableParticipantBooking = TBL_PREFIX . 'participant_booking_list';
        $coulumn = 'participantId';
        $participantId = $objBooker->getParticipantid();
        if (IS_APPROVE) {
            $this->removeAllRows($tableParticipantBooking, $coulumn, $participantId);
        }
        $bookerArray = $objBooker->getKinBookerDetails();
        $primary_data = 0;
        $bulk_insert_booker = array();
        foreach ($bookerArray as $booker) {
            $arrParticipantBooker = array();
            $arrParticipantBooker['firstname'] = $booker->getFirstname();
            $arrParticipantBooker['lastname'] = $booker->getLastname();
            $arrParticipantBooker['phone'] = $booker->getPhone();
            $arrParticipantBooker['email'] = $booker->getEmail();
            $arrParticipantBooker['relation'] = $booker->getRelation();
            $arrParticipantBooker['participantId'] = $booker->getParticipantid();
            $arrParticipantBooker['id'] = $booker->getParticipantbookerid();
            $bulk_insert_booker[] = $arrParticipantBooker;
        }


        if (count($bulk_insert_booker) > 0) {

            // Add Approval table			
            $approval_area = 'BookerUpdate';
            $approval_content = json_encode($bulk_insert_booker);
            $this->AddApproval($participantId, $approval_area, $approval_content);

            if (IS_APPROVE) {
                // Insert Email here
                $oprationType = 'insert';
                $this->bulk_record_opration($tableParticipantBooking, $oprationType, null, null, $bulk_insert_booker);
            }
            //insert Notification
            $arrNotification = notification_msgs('update_booker_detail');
            AddNotification($participantId, $arrNotification['title'], $arrNotification['description']);
        }


        /* $bookerList = array('firstname' => $this->firstname, 'lastname' => $this->lastname, 'phone' => $this->phone, 'email' => $this->email, 'relation' => $this->relation, 'participantId' => $this->participantid);
          return $CI->basic_model->insert_records('', $bookerList, $multiple = FALSE); */
    }

    private function UpdateParticipantPhones($participantPhone, $participantId) {
        $tableParticipantPhone = TBL_PREFIX . 'participant_phone';

        $coulumn = 'participantId';
        if (IS_APPROVE) {
            $this->removeAllRows($tableParticipantPhone, $coulumn, $participantId);
        }
        $primary_data = 0;
        $bulk_insert_phone = array();
        foreach ($participantPhone as $phone) {
            $primary_data++;
            $is_primary = ($primary_data == 1) ? '1' : '2';
            $arrParticipantPhone = array();
            $arrParticipantPhone['participantId'] = $participantId;
            $arrParticipantPhone['phone'] = $phone->phone;
            $arrParticipantPhone['primary_phone'] = $is_primary;
            $bulk_insert_phone[] = $arrParticipantPhone;
        }
        if (count($bulk_insert_phone) > 0) {

            // Add Approval table
            $approval_area = 'PhoneUpdate';
            $approval_content = json_encode($bulk_insert_phone);
            $this->AddApproval($participantId, $approval_area, $approval_content);

            if (IS_APPROVE) {
                // Insert Email here
                $oprationType = 'insert';
                $this->bulk_record_opration($tableParticipantPhone, $oprationType, null, null, $bulk_insert_phone);
            }
            //insert Notification
            //	$arrNotification=notification_msgs('update_phone_detail');			
            //	AddNotification($participantId,$arrNotification['title'],$arrNotification['description']);
        }
    }

    public function UpdateParticipantAddresses($addressDetails) {

        $tableParticipantAddress = TBL_PREFIX . 'participant_address';
        $coulumn = 'participantId';
        $participantId = $addressDetails->getParticipantid();
        if (IS_APPROVE) {
            $this->removeAllRows($tableParticipantAddress, $coulumn, $participantId);
        }
        $addressDetail = $addressDetails->getParticipantAddresse();

        $bulk_insert_address = array();
        foreach ($addressDetail as $address) {

            $arrParticipantAddress = array();
            $addressId = $address->getParticipantaddressid();
            $arrParticipantAddress['participantId'] = $address->getParticipantid();
            $arrParticipantAddress['street'] = $address->getStreet();
            $arrParticipantAddress['city'] = $address->getCity();
            $arrParticipantAddress['postal'] = $address->getPostal();
            $arrParticipantAddress['state'] = $address->getState();
            $arrParticipantAddress['site_category'] = $address->getSiteCategory();
            $arrParticipantAddress['primary_address'] = $address->getPrimaryAddress();
            $arrParticipantAddress['lat'] = $address->getLat();
            $arrParticipantAddress['long'] = $address->getLong();

            if ($addressId > 0) {
                // Update code here
                $oprationType = 'update';
                $whereKey = 'id';
                if (IS_APPROVE) {
                    $this->bulk_record_opration($tableParticipantAddress, $oprationType, $whereKey, $addressId, $arrParticipantAddress);
                }
            } else {
                // Insert Code here
                $bulk_insert_address[] = $arrParticipantAddress;
            }
        }
        if (count($bulk_insert_address) > 0) {

            if (IS_APPROVE) {
                $oprationType = 'insert';
                $this->bulk_record_opration($tableParticipantAddress, $oprationType, null, null, $bulk_insert_address);
            }

            // Add Approval table			
            $approval_area = 'AddressUpdate';
            $approval_content = json_encode($bulk_insert_address);
            $this->AddApproval($participantId, $approval_area, $approval_content);

            //insert Notification
            $arrNotification = notification_msgs('update_address_detail');
            AddNotification($participantId, $arrNotification['title'], $arrNotification['description']);
        }
    }

    private function removeAllParticipantActivity($ParticipantId) {
        $tableParticipantActivity = TBL_PREFIX . 'participant_activity';
        $this->db->where('participantId', $ParticipantId);
        $this->db->delete($tableParticipantActivity);
    }

    private function removeAllParticipantPlaces($ParticipantId) {
        $tableParticipantPlace = TBL_PREFIX . 'participant_place';
        $this->db->where('participantId', $ParticipantId);
        $this->db->delete($tableParticipantPlace);
    }

    private function removeAllRows($table, $coulumn, $id) {

        $this->db->where($coulumn, $id);
        $this->db->delete($table);
    }

    private function AddApproval($participantId, $approval_area, $approval_content) {
        $tableApproval = TBL_PREFIX . 'approval';
        $ApprovalData = array();
        $ApprovalData['userId'] = $participantId;
        $ApprovalData['approval_area'] = $approval_area;
        $ApprovalData['approval_content'] = $approval_content;
        $ApprovalData['user_type'] = 2;
        $ApprovalData['created'] = DATE_TIME;
        $ApprovalData['status'] = 0;
        $insert_query = $this->db->insert($tableApproval, $ApprovalData);
        if ($this->db->affected_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    // Get Password participant
    public function get_participant_password($objParticipant) {
        $tableParticipant = TBL_PREFIX . 'participant';
        $this->db->select('password');
        $this->db->from($tableParticipant);
        $this->db->where('id', $objParticipant->getParticipantid());
        return $getEncryptPassword = $this->db->get()->row()->password;
    }

    // Change Password
    public function participant_change_password($objParticipant) {
        $response = false;
        $tableParticipant = TBL_PREFIX . 'participant';
        $this->db->set('password', $objParticipant->getClientPassword());
        $this->db->where('id', $objParticipant->getParticipantid());
        $this->db->update($tableParticipant);

        //Participant change password
        $arrNotification = notification_msgs('update_change_password');
        AddNotification($objParticipant->getParticipantid(), $arrNotification['title'], $arrNotification['description']);


        if ($this->db->affected_rows() > 0) {
            $response = true;
        }
        return $response;
    }

    public function participant_remove_account($objParticipant) {
        $response = false;
        $tableParticipantRemove = TBL_PREFIX . 'participant_remove_account';
        $tableParticipant = TBL_PREFIX . 'participant';

        $arrParticipantRemove = array();
        $arrParticipantRemove['participantId'] = $objParticipant->getParticipantid();
        $arrParticipantRemove['reason'] = $this->db->escape($objParticipant->getReason());
        $arrParticipantRemove['contact'] = $this->db->escape($objParticipant->getContact());
        $insert_query = $this->db->insert($tableParticipantRemove, $arrParticipantRemove);


        //Participant remove account
        $arrNotification = notification_msgs('participant_remove_account');
        AddNotification($objParticipant->getParticipantid(), $arrNotification['title'], $arrNotification['description']);

        if ($this->db->affected_rows() > 0) {
            $response = true;

            $this->db->set('archive', 1);
            $this->db->where('id', $objParticipant->getParticipantid());
            $this->db->update($tableParticipant);

            $this->db->where(array("participantId" => $objParticipant->getParticipantid()));
            $this->db->delete(TBL_PREFIX . 'participant_login');

            $this->db->select('shiftId');
            $this->db->from($table_shift_participant);
            $this->db->where('participantId', $objParticipant->getParticipantid());
            $query = $this->db->get();
            $rows = $query->result();
            $cnt_rows = count($rows);
            if ($cnt_rows < 0) {
                foreach ($rows as $row) {
                    // tbl_shift
                    $shiftId = $row->shiftId;
                    $status = array(1, 2, 3, 7);
                    $this->db->set($table_shift . '.status', 8);
                    $this->db->where($table_shift . '.booked_by', 2);
                    $this->db->where($table_shift . '.id', $shiftId);
                    $this->db->where_in($table_shift . '.status', $status);
                    $this->db->update($table_shift);

                    // tbl_shift_member
                    $status = array(2, 7);
                    $this->db->set($table_shift_member . '.status', 4);
                    $this->db->where($table_shift_member . '.shiftId', $shiftId);
                    $this->db->where_in($table_shift_member . '.status', $status);
                    $this->db->update($table_shift_member);
                }
            }
            // table_participant_roster
            $status = array(1, 2, 3);
            $this->db->set('status', 5);
            $this->db->where('participantId', $objParticipant->getParticipantid());
            $this->db->where_in('status', $status);
            $this->db->update($table_participant_roster);
        }
        return $response;
    }

    public function get_suburb($post_data, $state) {
        $tbl_suburb_state = TBL_PREFIX . 'suburb_state';

        $this->db->select(array('suburb as value', 'suburb as label', 'postcode'));
        $this->db->from($tbl_suburb_state);
        $this->db->where(array('stateId' => $state));
        $this->db->group_by('suburb');
        $this->db->like('suburb', $post_data);
        $query = $this->db->get();

        return $result = $query->result();
    }

    public function get_member_name($post_data) {

        $this->db->or_where("(MATCH (firstname) AGAINST ('$post_data *'))", NULL, FALSE);
        $this->db->or_where("(MATCH (middlename) AGAINST ('$post_data *'))", NULL, FALSE);
        $this->db->or_where("(MATCH (lastname) AGAINST ('$post_data *'))", NULL, FALSE);
        $this->db->where('archive =', 0, FALSE);
        $this->db->where('status=', 1, FALSE);
        $this->db->select("CONCAT(firstname,' ',middlename,' ',lastname) as memberName");
        $this->db->select(array('id'));
        $query = $this->db->get(TBL_PREFIX . 'member');
        //last_query();
        $query->result();
        $participant_rows = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $participant_rows[] = array('label' => $val->memberName, 'value' => $val->id);
            }
        }
        return $participant_rows;
    }

    public function participant_update_profile_image($objParticipant) {
        $response = false;
        $tableParticipant = TBL_PREFIX . 'participant';
        $this->db->set('profile_image', $objParticipant->getFilename());
        $this->db->where('id', $objParticipant->getParticipantid());
        $this->db->update($tableParticipant);

        $imageArray = array('imagename' => $objParticipant->getFilename());

        // Add Approval table		
        //$participantId=$objParticipant->getParticipantid();		
        //$approval_area='ProfileImageUpdate';
        //	$approval_content=json_encode($imageArray);		
        //	$this->AddApproval($participantId,$approval_area,$approval_content);
        //Participant remove account
        $arrNotification = notification_msgs('update_profile_pic');
        AddNotification($objParticipant->getParticipantid(), $arrNotification['title'], $arrNotification['description']);

        if ($this->db->affected_rows() > 0) {
            $response = true;
        }
        return $response;
    }

    // ============================ Client Participant ==========================================
}
