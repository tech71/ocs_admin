import React, { Component } from 'react';
import BlockUi from 'react-block-ui';
import moment from 'moment-timezone';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';

import jQuery from "jquery";
import { ROUTER_PATH, BASE_URL } from '../config.js';
import '../service/jquery.validate.js';
import '../service/custom.js';
import { checkItsLoggedIn, setRemeber, getRemeber, setLoginTIme } from '../service/common.js';


import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Button from 'react-validation/build/button';
import axios from 'axios';

import 'react-block-ui/style.css';

const required = (value) => {
    if (!value.toString().trim().length) {
        return 'This field is required';
    }
};


class login extends Component {

    constructor(props) {
        super(props);
        checkItsLoggedIn();
        this.state = {
            username: '',
            password: '',
            gender: 1,
            remember: '',
            loading: false,
            error: '',
            success: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    handleChange(e) {
        var state = {};
        this.setState({ error: '' });
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }

    validateAll() {
        this.form.validateAll();
    }



    onSubmit = (e) => {
        e.preventDefault();

        jQuery('#login').validate();
        if (jQuery('#login').valid() && !this.state.loading) {

            let msgData = new FormData();
            msgData.append('username', (this.state.username) ? this.state.username : '');
            msgData.append('password', (this.state.password) ? this.state.password : '');
            msgData.append('remember', (this.state.remember) ? this.state.remember : '');
            console.log(msgData);

            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            }
            this.setState({ loading: true });


            axios.post(BASE_URL + 'participant/auth/client_login', msgData, config)

                .then(response => {
                   
                    console.log('response -');
                    console.log(response);
                    this.setState({ loading: false });

                    (this.state.remember == 1) ? setRemeber({ username: this.state.username, password: this.state.password }) : setRemeber({ username: '', password: '' });

                    if (response.data.status) {
                        this.setState({ success: <span><i className="icon icon-input-type-check"></i><div>{response.data.success}</div></span> });
                        localStorage.setItem("ocs_clienttoken603560", response.data.token);
                        localStorage.setItem("ocs_clientid603560", response.data.ocs_id);
                        setLoginTIme(moment());
                     

                        window.location = ROUTER_PATH + 'dashboard';

                    }
                    else {
                        this.setState({ error: <span><i className="icon ocs-warning2-ie"></i><div>{response.data.error}</div></span> });
                    }

                })
                .catch(error => {
                    this.setState({ loading: false });
                });




            // jQuery.ajax({
            //     url: BASE_URL + 'participant/auth/client_login',
            //     type: 'POST',
            //     dataType: 'JSON',
            //     data: {
            //         username: this.state.username,
            //         password: this.state.password,
            //         remember: this.state.remember
            //     },
            //     beforeSend: function () {
            //         this.setState({ loading: true });
            //     }.bind(this),
            //     success: function (data) {
            //         console.log(data);
            //         (this.state.remember == 1) ? setRemeber({ username: this.state.username, password: this.state.password }) : setRemeber({ username: '', password: '' });
            //         this.setState({ loading: false });
            //         if (data.status) {
            //             this.setState({ success: <span><i className="icon icon-input-type-check"></i><div>{data.success}</div></span> });
            //             localStorage.setItem("ocs_clienttoken603560", data.token);
            //             localStorage.setItem("ocs_clientid603560", data.ocs_id);
            //             setLoginTIme(moment());

            //             window.location = ROUTER_PATH + 'dashboard';

            //         } else {
            //             this.setState({ error: <span><i className="icon ocs-warning2-ie"></i><div>{data.error}</div></span> });
            //         }
            //     }.bind(this),
            //     error: function (xhr) {
            //         this.setState({ loading: false });
            //     }.bind(this)
            // });
        }
    }

    componentDidMount() {
        var loginCookie = getRemeber();
        this.setState({ gender: loginCookie.gender });
        this.setState({ username: loginCookie.username });
        this.setState({ password: loginCookie.password });
        this.setState({ remember: (loginCookie.username) ? true : false });
    }

    render() {
        return (
            <div className="margin_bottom_fixe gradient_color">
                <BlockUi tag="div" blocking={this.state.loading}>

                    <section >
                        <div className="container">
                            <div className="row justify-content-md-center justify-content-sm-center">
                                <div className="col-sm-11  col-md-7  col-lg-5">
                                    <div className="logo text-center">
                                        <img className="img-fluid" src={'/assets/images/logo_oncall.svg'} />
                                       
                                    </div>
                                    <div className="limiter">
                                        <div className="login_1">

                                            <div className="Smiley">
                                                <h1>
                                                    <span>Welcome Back&nbsp;<i className="icon icon-smail-big"></i></span>
                                                    Please Login Below
                                        </h1>
                                            </div>
                                            <Form id="login" className="login100-form" ref={c => {
                                                this.form = c
                                            }} onSubmit={this.onSubmit}>
                                                <div className="px-lg-4 px-md-3 px-sm-4 px-3 px-xl-5 pt-5">
                                                    <div className="User">
                                                        <span>
                                                            {/* <img src={'assets/images/client/' + ((this.state.gender == 2) ? 'woman.png' : 'man.png')} className="img-fluid align-self-center" alt="IMG" /> */}
                                                            <i className="icon icon-userm1-ie logie_ic"></i>
                                                        </span>
                                                    </div>
                                                    <div className="input_2">
                                                        <input className="input_3" type="text" name="username" placeholder="Username" value={this.state.username || ''} onChange={this.handleChange} data-rule-required="true" />
                                                    </div>
                                                    <div className="input_2">
                                                        <input className="input_3 required" type="password" name="password" placeholder="Password" value={this.state.password || ''} onChange={this.handleChange} data-rule-required="true" />
                                                    </div>
                                                    <div className="login_but">
                                                        <button className="but_login" onClick={this.validateAll.bind(this)} >
                                                            Submit
                                            </button>
                                                    </div>
                                                    <div className="success_login s_new">{this.state.success}</div>
                                                    <div className="error_login e_new">{this.state.error}</div>
                                                </div>
                                                <p className="text-center pt-5 pb-3">
                                                    <span className="b_check_2">
                                                        {/* <span className="b_check_2">
                                                            <input type="checkbox" className="checkbox2" id="c1" name="remember" checked={this.state.remember} value={this.state.remember || ''} onChange={this.handleChange} value="1" />
                                                            <label htmlFor="c1" ><span></span >Remember me</label>
										</span> */}
                                                    </span>
                                                </p>
                                                <h5 className="text-center for_text"><Link to={ROUTER_PATH + 'forgot_password'}>Forgotten your Password ?</Link></h5>
                                            </Form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <footer className="text-center footer_1">
                        <span>&#169;2018-All Right  Reserved <b>ONCALL</b></span>
                    </footer>
                </BlockUi>
            </div>

        );
    }
}

export default login
