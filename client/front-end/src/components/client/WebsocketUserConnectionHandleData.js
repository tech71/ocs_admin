import React, { Component } from 'react';
import { postData, getLoginToken, setPermission, logout} from '../../service/common.js';
import { connect } from 'react-redux'
import { WS_URL} from '../../config.js';
import { getNotification} from './actions/NotificationAction.js';
import Websocket from 'react-websocket';


class WebsocketUserConnectionHandleData extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount() {
      
    }
    
    setWebscoketObject = () => {
        
    }
    
    handleData = (res) => {
        var res = JSON.parse(res);
        console.log(res);
        if(res.type === 'imail_and_notification'){
            this.props.getNotification();
        }
     
    }

    render() {
        const ws_url = WS_URL+'/?req_type=user_connect_to_socket&user_type=participant&chanel=client&token=' + getLoginToken() + '&blk=';
         
        return (
                <div><Websocket reconnect={false} url={ws_url} onMessage={this.handleData} ref={Websocket => { this.setWebscoketObject(Websocket)}} /></div>
        );
    }
}
const mapStateToProps = state => ({
  
})

const mapDispatchtoProps = (dispach) => {
    return {
        getNotification : () => dispach(getNotification())
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(WebsocketUserConnectionHandleData);