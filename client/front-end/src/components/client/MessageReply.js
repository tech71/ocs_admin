import React, { Component } from 'react';
import jQuery from "jquery";
import { Panel, Button, ProgressBar, Modal } from 'react-bootstrap';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import 'react-select-plus/dist/react-select-plus.css';

import { ROUTER_PATH, BASE_URL } from '../../config.js';
import moment from 'moment-timezone';
import { Link, Redirect } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import { postData, getTimeAgoMessage, getFirstname, archiveALL, getPercentage, downloadAttachment } from '../../service/common';
import { connect } from 'react-redux'
import { decreaseNotificationCounter } from './actions/NotificationAction.js';

class MessageAndNotification extends Component {
    constructor(props) {
        super(props);

        this.state = {
            content: [],
            is_block: 1,
        };

    }

    replyMessage = () => {
        var reply_msg = this.state.replyAnser.trim();
        if (undefined !== reply_msg && reply_msg.length > 0) {
            var request = { replyAnser: this.state.replyAnser, messageId: this.props.match.params.id, lastContentId: this.state.lastContentId }
            this.setState({ replyAnser: '' })

            postData('participant/Message_Notification/reply_message', request).then((result) => {
                if (result.status) {
                    this.getSingleMessages();
                }
            });
        }
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    scrollToBottom = () => {
        var msgBox = jQuery('#Noti_scroll_1');
        if (msgBox.length > 0)
            msgBox[0].scrollTop = msgBox[0].scrollHeight;
    }

    onEnterPress = (e) => {
        if (e.keyCode == 13 && e.shiftKey == false) {
            e.preventDefault();
            this.replyMessage();
        }
    }


    addFavourite = (messageId, is_favourite) => {
        is_favourite = (is_favourite == 1) ? 0 : 1
        this.setState({ is_favourite: is_favourite })
        postData('participant/Message_Notification/add_favourite_message', { id: messageId, is_favourite }).then((result) => {

        });
    }

    archiveHandle = (messageId) => {
        archiveALL(messageId, 'external_message_recipient', 'Are you sure want to archive', 'participant/Message_Notification/archiveMessage', '').then((result) => {
            if (result.status) {
                this.setState({ archive: 1 })
            }
        })
    }


    getSingleMessages = () => {
        postData('participant/Message_Notification/get_single_message', this.props.match.params).then((result) => {
            if (result.status) {
                this.setState(result.data);
                var content = result.data.content;
                this.setState({ lastContentId: content[content.length - 1]['id'] })

                if (content.length > 0) {
                    content.map((val, index) => {
                        if (val.is_read == 0) {
                            this.props.decreaseNotificationCounter(val.id);
                        }
                    })
                }
            }
        });
    }

    componentDidMount() {
        this.getSingleMessages();
    }

    render() {

        return (
            <div>
                <Header title="Messages & Notifications" />
                <div className="container">
                    <div className="row mt-3 mb-4">
                        <div className="col-lg-12">
                            <div className="line"></div>
                            <div className="labels mt-2" align="right">

                            </div>
                        </div>
                        <div className="col-md-12 back-arrow"><a onClick={() => this.props.history.goBack()} ><i className="icon icon-back-arrow hand_i"></i></a></div>

                    </div>

                    <div className="row justify-content-center">
                        <div className="col-lg-3 col-md-3 col-sm-6 mb-sm-1 my-1 active" align="center"><Link to={'/inbox/all'} className="index_but">Inbox</Link></div>
                        <div className="col-lg-3 col-md-3 col-sm-6 mb-sm-1 my-1" align="center"><Link to={'/inbox/favourite'} className="drafts_but">favourite</Link></div>
                        <div className="col-lg-3 col-md-3 col-sm-6 mb-sm-1 my-1" align="center"><Link to={'/inbox/archive'} className="archived_but">Archived</Link></div>
                        <div className="col-md-12 mt-3 mb-3"><div className="line"></div></div>
                    </div>


                    <div className="row">
                        <div className="col-md-12">
                            <div className="row">
                                <div className="col-md-6 head_replay">Subject: <span>{this.state.title}</span></div>
                                {this.state.archive == 1 ? "" :
                                    <div className="col-md-6 icons_replay text-right">
                                        <i onClick={() => this.addFavourite(this.props.match.params.id, this.state.is_favourite)} className={'icon icon-like fav_icon hand_i ' + ((this.state.is_favourite == 1) ? ' heart-fill' : 'heart-blank')}></i>
                                        <i onClick={() => this.archiveHandle(this.props.match.params.id)} className="icon icon-email-pending hand_i"></i>
                                    </div>}
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="line"></div>
                        </div>
                    </div>

                    <div className="Noti_scroll_1" id="Noti_scroll_1">
                        {this.state.content.map((val, index) => (
                            (val.sender_type == 1) ?
                                <div className="row my-3" key={index + 1}>
                                    <div className="col-md-8  d-flex">
                                        <div className="chart_name_a"><span>{val.userId}</span></div>
                                        <div className="Box_tx1 msj"><div className="px-3 py-3">{val.content}</div></div>
                                    </div>
                                    <div className="col-md-4 align-self-end text-left txt_Notif_1">{moment(val.created).format('DD/MM/YYYY hh:mm A')} </div>
                                    <div className="col-md-12 my-4">
                                        <div className="mees_pdf_v1 left">
                                            {val.attachments.map((file, index) => (


                                                <a onClick={() => downloadAttachment(file.file_path)} key={index + 1} href={file.file_path} target="_blanck">
                                                    <span className='d_flex2'><i className="icon icon-file-im color"></i>
                                                        <div>{file.filename}</div>
                                                    </span>
                                                </a>


                                            ))}
                                        </div>
                                        
                                    </div>
                                </div>
                                :

                                <div className="row justify-content-end my-3" key={index + 1}>
                                    <div className="col-md-4 text-right align-self-end txt_Notif_1">{moment(val.created).format('DD/MM/YYYY hh:mm A')} </div>
                                    <div className="col-md-8 d-flex">
                                        <div className="Box_tx1 msj-rta"><div className="px-3 py-3">{val.content}</div></div>
                                        <div className="chart_name_u"><span>{getFirstname().charAt(0)}</span></div>
                                    </div>
                                </div>
                        ))}
                        <div id="scroll-container" />
                    </div>


                    {(this.state.archive == 1 || this.state.is_block == 1) ? "" :
                        <div className="row">
                            <div className="col-md-12 mt-4">
                                <div className="line"></div>
                            </div>

                            <div className="col-md-12 mt-3">
                                <textarea name="reply" className="text_Notif" onChange={(e) => this.setState({ replyAnser: e.target.value })} value={this.state.replyAnser || ''} onKeyDown={this.onEnterPress}></textarea>
                            </div>
                        </div>}

                    {(this.state.archive == 1 || this.state.is_block == 1) ? "" :
                        <div className="row justify-content-end">
                            <div className="col-md-3">
                                <button type="button" className="submit_button w-100" onClick={this.replyMessage}>Send</button>
                            </div>
                        </div>}

                </div>

                <Footer />
            </div>
        );
    }
}

const mapStateToProps = state => ({

})

const mapDispatchtoProps = (dispach) => {
    return {
        decreaseNotificationCounter: (contentId) => dispach(decreaseNotificationCounter(contentId))
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(MessageAndNotification);

