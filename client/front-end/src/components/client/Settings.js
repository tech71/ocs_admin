import React, { Component } from 'react';
import jQuery from "jquery";
import { Panel,  Modal, PanelGroup } from 'react-bootstrap';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { Link } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import { toast } from 'react-toastify';
import { postData, logout, postImageData } from '../../service/common';
import '../../service/jquery.validate.js';
import { AccountRemoveReasonListDropdown } from '../../service/ParticipantDropdown';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expanded: false,
            detail: true,
            req: true,
            loading: false,
            success: false,
            error: '',
            passwords: [],
            password: '',
            confirmpassword: '',
            newpassword: '',
            show: false,
            selectedFile: null,
            blocking: false
        };
    }
    setToggel = (key, val) => {
        var state = {}
        state[key] = val
        this.setState(state)
    }
    handleGoalClose = (type) => {
        this.setState({ showcontact: false });
        if (type == true) {
            this.handleThankyouShow();
        }
    }
    handleGoalShow = (val) => { this.setState({ showcontact: true, goal: val, mode: 'update' }); }
    handleThankyouClose = () => { this.setState({ showthankyou: false }); }
    handleThankyouShow = () => { this.setState({ showthankyou: true }); }
    onSubmit = (e) => {
        jQuery('#form_details').validate();
        if (jQuery('#form_details').valid()) {
            postData('participant/dashboard/participant_change_password', this.state).then((result) => {
                if (result.status) {
                    this.setState({ confirmpassword: '', password: '', newpassword: '', error: '' });
                    this.setState({ success: result.success });
                    setTimeout(() => logout(), 2000);
                } else {
                    this.setState({ error: result.error });
                }
            });
        }
        e.preventDefault();
    }
    handleChange = (e) => {
        var state = this.state.passwords;
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }
    componentDidMount() {
        postData('participant/Dashboard/get_participant_profile_image', {}).then((result) => {
            if (result.status) {
                var details = result.data;                
                this.setState({ imagePreviewUrl: details });
            }
        });
    }
    fileChangedHandler = (event) => {        
        let reader = new FileReader();
        let file = event.target.files[0];
        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result,
                selectedFile: file,
                filename: file.name
            });
        }
        reader.readAsDataURL(file)
        this.setState({})
    }
    uploadHandler = (e) => {
        jQuery("#special_agreement_form").validate({ /* */ });
        if (jQuery("#special_agreement_form").valid()) {
            this.setState({ loading: true })
            const formData = new FormData()
            formData.append('myFile', this.state.selectedFile, this.state.selectedFile.name)
            postImageData('participant/auth/profile_pic', formData).then((responseData) => {
                var x = responseData;
                if (responseData.status) {
                    toast.success("Profile picture uploaded successfully.", {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    this.setState({ loading: false })
                } else {
                    toast.error(x.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    this.setState({ loading: false })
                }
            }).catch((error) => {
                //errorMessage("Api Error");
                this.setState({ loading: false })
            });
        }
        e.preventDefault();
    }
    archiveHandle = (messageId) => {
    }
    render() {
        return (
            <div>
                <Header title={'Your Settings'} />
                <div className="container">
                    <div className="row mt-3 mb-4">
                        <div className="col-lg-12">
                            <div className="line"></div>
                            <div className="labels mt-2" align="right">
                            </div>
                        </div>
                        <div className="col-md-12 back-arrow"><Link to="/dashboard"><i className="icon icon-back-arrow"></i></Link></div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12">
                            <PanelGroup
                                accordion
                                id="accordion-controlled-example"
                                activeKey={this.state.activeKey}
                                onSelect={this.handleSelect}
                            >
                                <Panel eventKey="1">
                                    <Panel.Heading>
                                        <Panel.Title toggle>
                                            <span role="button" onClick={() => this.setState({ detail: !this.state.detail })}>
                                                <i className="more-less glyphicon glyphicon-plus"></i>Your Password
                                            </span>
                                        </Panel.Title>
                                    </Panel.Heading>
                                    <Panel.Body collapsible>
                                        <div id="collapseOne" role="tabpanel" aria-labelledby="headingOne">
                                            <form id="form_details" onSubmit={this.onSubmit}>
                                                <div className="row mb-3">
                                                    <div className="col-md-3 pl-0 text-right align-self-center">
                                                        <label >Password :</label>
                                                    </div>
                                                    <div className="col-md-6 pl-0">
                                                        <span className="input_gradient">
                                                            <input className="input_pass" placeholder="Current Password" type="password" name="password" value={this.state.password} onChange={this.handleChange} data-rule-required="true" />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="row mb-3">
                                                    <div className="col-md-3 pl-0 text-right align-self-center">
                                                        <label >New Password :</label>
                                                    </div>
                                                    <div className="col-md-6 pl-0">
                                                        <span className="input_gradient">
                                                            <input className="input_pass" data-rule-minlength="6" placeholder="New Password" type="password" name="newpassword" value={this.state.newpassword} id="newpassword" onChange={this.handleChange} data-rule-required="true" />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="row mb-3">
                                                    <div className="col-md-3 pl-0 text-right align-self-center">
                                                        <label >Confirm Password :</label>
                                                    </div>
                                                    <div className="col-md-6 pl-0">
                                                        <span className="input_gradient">
                                                            <input className="input_pass" data-rule-minlength="6" placeholder="Confirm Password" type="password" name="confirmpassword" data-rule-equalto="#newpassword" onChange={this.handleChange} value={this.state.confirmpassword} data-rule-required="true" />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="row mb-3">
                                                    <div className="col-md-4 mt-9">
                                                    </div>
                                                    <div className="col-md-4 mt-3">
                                                        <button type="submit" className="submit_button w-100">Submit</button>
                                                        <div className="success_login">{this.state.success}</div>
                                                        <div className="error_login">{this.state.error}</div>
                                                    </div>
                                                    <div className="col-md-4 mt-9">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </Panel.Body>
                                </Panel>
                                <Panel eventKey="2">
                                    <Panel.Heading>
                                        <Panel.Title toggle>
                                            <span role="button" onClick={() => this.setState({ detail: !this.state.detail })}>
                                                <i className="more-less glyphicon glyphicon-plus"></i>Your Profile
                                            </span>
                                        </Panel.Title>
                                    </Panel.Heading>
                                    <Panel.Body collapsible>
                                        <form id="special_agreement_form" method="post" autoComplete="off">
                                            <BlockUi tag="div" blocking={this.state.loading}>
                                                <div className="row d-flex justify-content-center">
                                                    <div className="col-lg-9 col-sm-3 pl-3">
                                                        <span className="add_icon">
                                                            <div id="image-preview">
                                                                <img src={this.state.imagePreviewUrl} />
                                                                <label htmlFor="image-upload" id="image-label" ></label>
                                                                <input type="file" name="special_agreement_file" id="image-upload" onChange={this.fileChangedHandler} data-rule-required="true" date-rule-extension="jpg|jpeg|png" />
                                                            </div>
                                                        </span>
                                                    </div>
                                                    <div className="col-lg-3 align-self-end">
                                                        <input type="submit" value={'Save'} name="content" className="remove_account_button w-100" onClick={(e) => this.uploadHandler(e)} />
                                                    </div>
                                                </div>
                                            </BlockUi>
                                        </form>
                                    </Panel.Body>
                                </Panel>
                                <Panel eventKey="3">
                                    <Panel.Heading>
                                        <Panel.Title toggle>
                                            <span role="button" onClick={() => this.setState({ detail: !this.state.detail })}>
                                                <i className="more-less glyphicon glyphicon-plus"></i>Your Account
                                            </span>
                                        </Panel.Title>
                                    </Panel.Heading>
                                    <Panel.Body collapsible>
                                        <div className="row mb-3">
                                            <div className=" col-md-4 offset-md-4 col-sm-6 offset-sm-3 align-self-center">
                                                <button type="submit" onClick={() =>
                                                    this.setState({ showcontact: true })} className="remove_account_button w-100">Remove Your Account</button>
                                            </div>
                                        </div>
                                    </Panel.Body>
                                </Panel>
                            </PanelGroup>
                        </div>
                    </div>
                </div>
                <RemoveAccount goal={this.state.goal} mode={this.state.mode} showcontact={this.state.showcontact} handleGoalClose={this.handleGoalClose} getGoalDetails={this.getGoalDetails} handleDateChangeRaw={this.handleDateChangeRaw} />
                <ThankYou showthankyou={this.state.showthankyou} handleThankyouShow={this.handleThankyouShow} handleThankyouClose={this.handleThankyouClose} />
                <Footer />
            </div>
        );
    }
}
export default Settings;
    
class RemoveAccount extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            RemoveReason: 1,
            iscontact: 1
        }
    }
    componentWillReceiveProps(newProps) {
        var newObj = JSON.parse(JSON.stringify(newProps));       
        this.setState(newObj.goal);
        this.setState({ mode: newObj.mode })
        if (newObj.mode == 'add') {
            this.setState({ title: '', start_date: '', end_date: '' });
        }
    }
    submitgoals = (e) => {
        e.preventDefault();
        this.setState({ loading: true });        
        postData('participant/Dashboard/remove_participant_account', this.state).then((result) => {
            
            if (result.status) {
                this.setState({ iscontact: '', RemoveReason: '' });
                this.props.handleGoalClose(true);
            } else {
                //this.setState({error: result.error});
            }
        });
        this.setState({ loading: false });
    }
    selectChange = (key, value) => {
        var state = {}
        state[key] = value;
        this.setState(state);
    }
    render() {
        return (
            <Modal show={this.props.showcontact} onHide={this.props.handleGoalClose}
                className="modal Modal_A size_add small_modal">
                <Modal.Body>
                    <h4 className="py-3 my-0 text-center heading_modal"> We're Sorry to See You Leave<a className="close_i" onClick={this.props.handleGoalClose}><img src="/assets/images/client/cross_icons.svg" /></a></h4>
                    <div className="my-0 mb-3"><div className="line"></div></div>
                    <form id="form_details" onSubmit={this.submitgoals} >
                        <div className="row">
                            <div className='col-lg-12 col-7 my-3'>
                                <label className="f_w_300 text-center w-100 mb-4">Could you please let us know why you <br /> are removing your account with us.</label>
                                <div className="row justify-content-center mb-4">
                                    <div className="col-md-6">
                                        <Select clearable={false} className="custom_select my-select default_validation " simpleValue={true} searchable={false} name="RemoveReason" value={this.state['RemoveReason']} options={AccountRemoveReasonListDropdown(0)} placeholder="Reason Of Leaving" required={true} onChange={(e) => this.selectChange('RemoveReason', e)} />
                                    </div>
                                </div>
                                <label className="f_w_300 text-center w-100 mb-4">Can we contact you to discuss<br /> your reason for leaving.  </label>
                                <div className="row justify-content-center">
                                    <div className="col-md-3">
                                        <Select clearable={false} searchable={false} value={this.state['iscontact']} simpleValue={true} className="custom_select my-select default_validation" name="iscontact" required={true} options={[{ value: '1', label: 'Yes' }, { value: '0', label: 'No' }]} onChange={(e) => this.selectChange('iscontact', e)} >
                                        </Select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row justify-content-center">
                            <div className="col-md-6 mb-4 mt-3">
                                <button type="submit" disabled={this.state.loading} className="submit_button w-100">Submit</button>
                            </div>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        )
    }
}
// Thank you model
class ThankYou extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    componentWillReceiveProps(newProps) {
        var newObj = JSON.parse(JSON.stringify(newProps));       
        //this.props.handleThankyouShow();
    }
    submitthankyou = (e) => {
        e.preventDefault();
        this.props.handleThankyouClose();
        logout();
    }
    render() {
        return (
            <Modal show={this.props.showthankyou}
                className="modal Modal_A size_add small_modal">
                <Modal.Body>
                    <h4 className="py-3 my-0 text-center heading_modal"> We're Sorry to See You Leave</h4>
                    <div className="my-0 mb-3"><div className="line"></div></div>
                    <form id="form_details" onSubmit={this.submitthankyou} >
                        <div className="row">
                            <div className='col-lg-10 col-7 my-3 offset-lg-1 text-center'>
                                <label className="f_w_300">Your account has been deactivated, please contact us to reinstate your account.</label>
                            </div>
                        </div>
                        <div className="row justify-content-center">
                            <div className="col-md-6 mt-3">
                                <button type="submit" disabled={this.state.loading} className="submit_button w-100">Thank You</button>
                            </div>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        )
    }
}