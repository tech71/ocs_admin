import React, { Component } from 'react';
import jQuery from "jquery";
import { Panel, Button, ProgressBar, Modal } from 'react-bootstrap';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import 'react-select-plus/dist/react-select-plus.css';
import moment from 'moment';
import { ROUTER_PATH, BASE_URL } from '../../config.js';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import { Link, Redirect } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import { postData, archiveALL, getPercentage, handleDateChangeRaw } from '../../service/common';
import { ToastContainer, toast } from 'react-toastify';

const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {
        // request json
        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });
        postData('participant/Message_Notification/get_all_messages', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            setTimeout(() => resolve(res), 0);
        });
    });
};

class MessageAndNotification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: [],
            messageNoticfication: [],
            messageType: this.props.match.params.type,
            columns: []
        };
        this.reactTable = React.createRef();
    }

    componentWillReceiveProps(newProps) {

        this.setState({ filtered: newProps.match.params, messageType: newProps.match.params.type });
    }

    archiveHandle = (messageId) => {
        archiveALL(messageId, 'external_message_recipient', 'Are you sure want to archive', 'participant/Message_Notification/archiveMessage', '').then((result) => {
            if (result.status) {
                this.reFreashTable();
            }
        })
    }
    injectThProps = (state, rowInfo, column) => {
        return {
            style: { display: 'none' }
        }
    }
    addFavourite = (messageId, is_fav) => {
        is_fav = (is_fav == 1) ? 0 : 1
        postData('participant/Message_Notification/add_favourite_message', { id: messageId, is_fav: is_fav }).then((result) => {
            if (result.status) {
                this.reFreashTable();
            }
        });
    }
    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                messageNoticfication: res.rows,
                pages: res.pages,
                loading: false,
                selectAll: 0,
                selected: [],
            });
        });
    }

    reFreashTable = () => {
        var ReactOption = this.reactTable.current.state;
        var state = { pageSize: ReactOption.pageSize, page: ReactOption.page, sorted: ReactOption.sorted, filtered: ReactOption.filtered }
        this.fetchData(state);
    }

    toggleSelectAll = () => {
        let newSelected = {};
        if (this.state.selectAll === 0) {
            this.state.messageNoticfication.forEach(x => {
                if (x.push_to_app != 2) {
                    newSelected[x.id] = true;
                }
            });
        }
        this.setState({
            selected: newSelected,
            selectAll: (this.state.selectAll === 0) ? 1 : 0
        });
    }

    toggleRow = (id) => {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[id] = !this.state.selected[id];
        this.setState({
            selected: newSelected,
            selectAll: 2
        });
    }
    searchBox = (key, value) => {
        var state = {}
        state[key] = value;
        this.setState(state, () => {
            var filter = { search_box: this.state.search_box, start_date: this.state.start_date, end_date: this.state.end_date, type: this.state.messageType }
            this.setState({ filtered: filter });
        });
    }
    CheckSelectAll = () => {
        var checkselect = false;
        Object.keys(this.state.selected).map((type, index) => {
            if (this.state.selected[type]) {
                checkselect = true;
            }
        })
        return checkselect;
    }
    favoriteSelectAll = () => {
        if (this.CheckSelectAll()) {
            postData('participant/Message_Notification/favourite_message_all', this.state.selected).then((result) => {
                if (result.status) {
                    toast.success('All Messages change Favorite successfully', {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    this.reFreashTable();
                } else {
                    this.setState({ error: result.error });
                }
            });
        } else {
            toast.success('Please select atleast one', {
                position: toast.POSITION.TOP_CENTER,
                hideProgressBar: true
            });
        }
    }
    archiveSelectAll = () => {
        if (this.CheckSelectAll()) {
            postData('participant/Message_Notification/archive_message_all', this.state.selected).then((result) => {
                if (result.status) {
                    toast.success('All Messages moves to archived successfully', {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    this.reFreashTable();
                } else {
                    this.setState({ error: result.error });
                }
            });
        } else {
            toast.success('Please select atleast one', {
                position: toast.POSITION.TOP_CENTER,
                hideProgressBar: true
            });
        }
    }
    render() {
        const columns = [this.state.messageType != 'archive' ? {
            id: "checkbox", accessor: "id", width: 100,
            Cell: ({ original }) => {
                return (<span className="new_check_box"><input type='checkbox' className="checkbox1" checked={this.state.selected[original.id] === true}
                    onChange={() => this.toggleRow(original.id)} />
                    <label className="hand_i" onClick={() => this.toggleRow(original.id)}><div className="d_table-cell"> <span></span></div></label>
                    <span onClick={() => this.addFavourite(original.id, original.is_fav)} className="fav_div">
                        <i className={'icon hand_i  fav_icon ' + ((original.is_fav == 1) ? ' icon-like2-ie' : 'icon-like')}></i>
                    </span></span>);
            }, sortable: false,
        } : { style: { display: 'none' } },
        this.state.messageType != 'archive' ? {
            id: "checkbox", accessor: "original", width: 50,
            Cell: ({ original }) => {
                return (<div className="text-center"><i className={'icon icon-circle ' + ((original.is_read == 1) ? 'approved_c' : 'pendding_c')}></i></div>);
            }, sortable: false,
        } : { style: { display: 'none' } },
        {
            Header: 'AdminId', accessor: "userId", width: 120, refs: 'adminId', Cell: ({ original }) => {
                return (<span className="message-noti-admin_1">{original.userId}</span>);
            }, sortable: false
        },
        {
            Header: 'Title', accessor: "title", Cell: ({ original }) => {
                return (<Link to={'/inbox/message/' + original.id}><div className="message-noti-admin_2">{original.title}</div></Link>);
            }, filterable: false, width: 150
        },
        {
            Header: 'Content', accessor: "content", Cell: ({ original }) => {
                return (<Link to={'/inbox/message/' + original.id}><span className="message-noti-admin_3">{original.content}</span></Link>);
            }, filterable: false,
        },
        this.state.messageType != 'archive' ? {
            Header: 'Action', width: 50, accessor: 'id', sortable: false,
            filterable: false,
            Cell: props => <span className="arch_1">
                <button className="archive_btn" onClick={() => this.archiveHandle(props.value)} ><i className="icon icon-email-pending"></i></button>
            </span>
        } : { style: { display: 'none' } },
        ]
        return (
            <div>
                <Header title="Messages & Notifications" />
                <div className="container">
                    <div className="row mt-3 mb-4">
                        <div className="col-lg-12">
                            <div className="line"></div>
                            <div className="labels mt-2" align="right">
                            </div>
                        </div>
                        <div className="col-md-12 back-arrow"><Link to="/dashboard"><i className="icon icon-back-arrow"></i></Link></div>
                    </div>
                    <div className="row my-3">
                        <div className="col-lg-6 mt-2">
                            <div className="input_search">
                                <input onChange={(e) => this.searchBox('search_box', e.target.value)} value={this.state.search_box || ''} type="text" className="form-control" placeholder="Search" />
                                <button type="submit">
                                    <img src="/assets/images/client/search.svg" />
                                </button>
                            </div>
                        </div>
                        <div className="col-lg-2 col-sm-4 col-6 mt-2 filter_date">
                            <label>From:</label>
                            <DatePicker onChangeRaw={handleDateChangeRaw} isClearable={true} name="start_date" maxDate={this.state.end_date} onChange={(e) => this.searchBox('start_date', e)} selected={this.state['start_date'] ? moment(this.state['start_date'], 'DD-MM-YYYY') : null} className="text-center px-0" placeholderText="00/00/0000" />
                        </div>
                        <div className="col-lg-2 col-sm-4 col-6 mt-2 filter_date">
                            <label>To</label>
                            <DatePicker onChangeRaw={handleDateChangeRaw} isClearable={true} name="end_date" minDate={this.state.start_date} onChange={(e) => this.searchBox('end_date', e)} selected={this.state['end_date'] ? moment(this.state['end_date'], 'DD-MM-YYYY') : null} className="text-center px-0" placeholderText="00/00/0000" />
                        </div>
                    </div>
                    <div className="row justify-content-center msg_btnBar">
                        <div className={"col-lg-3 col-md-3 col-sm-6 mb-sm-1 my-2 " + (this.state.messageType == 'all' ? 'active' : '')} align="center"><Link to={'/inbox/all'} className="index_but">Inbox</Link></div>
                        <div className={"col-lg-3 col-md-3 col-sm-6 mb-sm-1 my-2 " + (this.state.messageType == 'favourite' ? 'active' : '')} align="center"><Link to={'/inbox/favourite'} className="drafts_but">favourite</Link></div>
                        <div className={"col-lg-3 col-md-3 col-sm-6 mb-sm-1 my-2 " + (this.state.messageType == 'archive' ? 'active' : '')} align="center"><Link to={'/inbox/archive'} className="archived_but">Archived</Link></div>
                    </div>
                    <div className="row">
                        <div className="col-md-12 mt-3">
                            <div className="line">
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        {this.state.messageType == 'archive' ? "": 
                        <div className="col-lg-12 col-sm-12 mt-12 ">
                            <div className="d-flex  justify-content-between">
                                <div className="fav_and_check">
                                    <span className="new_check_box pt-2 pl-3">
                                    <input type='checkbox' className="checkbox1" checked={this.state.selectAll === 1} ref={input => {
                                            (input) ? input.indeterminate = this.state.selectAll === 2 : ''
                                        }}
                                            onChange={() => this.toggleSelectAll()} />
                                        <label className="hand_i" onClick={() => this.toggleSelectAll()}>
                                            <div className="d_table-cell"><span></span></div>
                                        </label>
                                    </span>
                                 
                                    <span className="fav_icon_15">
                                        <button type="submit" onClick={() => this.favoriteSelectAll()}><i className="icon hand_i icon-like"></i></button>
                                    </span>
                                </div>
                                <span className="arch_icon_15">
                                    <button type="submit" className="archive_btn" onClick={() => this.archiveSelectAll()}><i className="icon icon-email-pending"></i></button>
                                </span>
                            </div>
                            <div className="line"></div>
                        </div>}

                        <div className="Notification_Tab col-md-12">
                            <ReactTable
                                manual
                                columns={columns}
                                ref={this.reactTable}
                                getTheadThProps={this.injectThProps}
                                data={this.state.messageNoticfication}
                                pages={this.state.pages}
                                loading={this.state.loading}
                                onFetchData={this.fetchData}
                                pageSize={this.state.pageSize}
                                defaultFiltered={{ type: this.state.messageType }}
                                filtered={this.state.filtered}
                                defaultPageSize={10}
                                noDataText="Message Empty"
                                className="-striped -highlight"
                                getTrProps={this.onRowClick}
                                minRows={1}
                                previousText={<span className="icon icon-arrow-1-left privious P_A_n"></span>}
                                nextText={<span className="icon icon-arrow-1-right next"></span>} 
                                
                                />
                                
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}
export default MessageAndNotification;