import React, { Component } from 'react';
import jQuery from "jquery";
import { Panel, Button, ProgressBar, Modal } from 'react-bootstrap';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import 'react-select-plus/dist/react-select-plus.css';
import moment from 'moment';
import { ROUTER_PATH, BASE_URL } from '../../config.js';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Timer from 'react-compound-timer';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import { postData, handleShareholderNameChange, getOptionsSuburb, handleAddShareholder, handleRemoveShareholder, getPercentage } from '../../service/common';
import { addressCategory } from '../../service/ParticipantDropdown';
import '../../service/jquery.validate.js';
class FeedbackCaseDetail extends Component {
    constructor(props) {
        super(props);
        this.caseId = this.props.match.params.id;
        this.state = {
            location: [{ address: '', state: '', suburb: '', postal: '' }],
            ShiftMemberOption: [],
            shiftId: '',
            case_detail: [],
            caseReason: [],
			caseId:''
        }
    }
    handleShareholderNameChange = (obj, stateName, index, fieldName, value) => {
        var state = {};
        var tempField = {};
        var List = obj.state[stateName];
        List[index][fieldName] = value
        if (fieldName == 'state') {
            List[index]['suburb'] = {}
            List[index]['postal'] = ''
        }
        if (fieldName == 'suburb' && value) {
            List[index]['postal'] = value.postcode
        }
        state[stateName] = List;
        obj.setState(state);
    }
    selectChange = (key, value) => {
        var state = {}
        if (key == 'shift_date') {
            this.setState({ shiftLoading: true })
            postData('participant/Shift_Roster/get_shift_list_for_feedback', { shift_date: value }).then((result) => {
                if (result.status) {
                    this.setState({ loadedShiftOption: result.data })
                } else {
                    this.setState({ error: result.error });
                }
                this.setState({ shiftLoading: false })
            });
        }
        state[key] = value;
        this.setState(state);
    }
    submitFeedback = () => {
        if (jQuery('#submit_feedback').valid()) {
            postData('participant/Shift_Roster/submit_feedback', this.state).then((result) => {
                if (result.status) {
                    toast.success('Feedback submit successfully', {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    this.setState({ shift_date: '', shiftId: '', shiftMemberId: '', feedback_message: '', location: [{ address: '', state: '', suburb: '', postal: '' }] })
                } else {
                    this.setState({ error: result.error });
                }
            });
        }
    }
    onChangeShiftTime = (e) => {
        this.setState({ shiftId: e }, () => {
            postData('participant/Shift_Roster/get_member_by_shift', this.state).then((result) => {
                if (result.status) {                    
                    this.setState({ ShiftMemberOption: result.data });
                }
            });

        });
    }
    componentDidMount() {
		
        this.get_case_detail(this.props.match.params.id);
    }
    closeCategoryPopUp = (status,caseId) => {
        this.setState({ categoryModalShow: false });
		if(status){
			this.get_case_detail(caseId);
		}
    }
    get_case_detail= (caseId) => {
        var requestData = { case_id:caseId  };
        this.setState({ loading: true });
        postData('participant/Shift_Roster/get_feedback_case_details', requestData).then((result) => {
            if (result.status) {
                this.setState({ case_detail: result.data.case_detail });
                this.setState({ caseReason: result.data.caseReason });
            } else {
                this.setState({ error: result.error });
            }
            this.setState({ loading: false });
        });
    }

    render() {
        return (
            <div>
                
                <section>
                    <div className="container">
                    <Header title={'Case ID - ' + this.state.case_detail.id} />

                    <ul className="user_info P_15_T fdInfo">
                        <li>Initiated by: <span>Yooralla West </span></li>
                        <li>OCSID: <span> {this.state.case_detail.id} </span></li>
                        <li>Initiator Name: <span> {this.state.case_detail.name}</span></li>
                        <li>Site: <span>Fawkner </span></li>
                        <li>Created Date: <span>{moment(this.state.case_detail.created).format('D/M/Y')} </span></li>
                    </ul>

                        <div className="row">
                            <div className="col-lg-12 mt-2"><div className="line"></div></div>
                        </div>
                        <div className="row">
                            <div className="col-md-12 back-arrow"><Link to="../../feedback"><i className="icon icon-back-arrow"></i></Link></div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 mt-2"><div className="line"></div></div>
                        </div>

                        <div>
                            <div className="row">
                                <div className="col-lg-3 col-md-1"></div>

                                <div className="col-lg-3 col-md-2"></div>
                                <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div>


                                <div className="col-lg-1"></div>
                                <div className="col-lg-12 col-md-9 P_25_T">
                                    <div className="panel-group accordion_me accordion_member font_w_1 fms_accor" id="accordion" role="tablist" aria-multiselectable="true">

                                        {(this.state.caseReason.length > 0) ? <span>
                                            {this.state.caseReason.map((value, index) => (
                                                <Panel key={index + 1} eventKey={index + 1}>
                                                    <Panel.Heading>
                                                        <Panel.Title toggle>
                                                            <span>
                                                                <i className="more-less glyphicon glyphicon-plus"></i>
                                                                <ul className='panelHdUl'>
                                                                    <li className="li_fms_1"><span>{moment(value.created).format('D/M/Y')}</span>: {value.title}</li>
                                                                    <li className="li_fms_2"><span>By:</span> {value.created_by}</li>
                                                                </ul>
                                                            </span>
                                                        </Panel.Title>
                                                    </Panel.Heading>

                                                    <Panel.Body collapsible className="px-1 py-0">
                                                        <div className="panel-body px-3 py-2">
                                                            {value.description}
                                                        </div>
                                                        {/* <p className="text-right P_15_LR">
                                                            <a ><i className="icon icon-update update_button" onClick={() => this.setState({ categoryModalShow: true, categoryMode: 'Edit', updateData: value })}></i></a>
                                                            <a ><i className="icon icon-pending-icons history_button"></i></a>
                                                        </p> */}
                                                        <div class="col-md-12 text-right d-flex justify-content-end">
                                                            <div class="">
                                                                <a align="right" class="update_but pb-3 pr-3 mt-1 flx_upd1" >
                                                                    Update 
                                                                    {/* <img 
                                                                        src="/assets/images/client/update.svg" 
                                                                        onClick={
                                                                            () => this.setState({ categoryModalShow: true, categoryMode: 'Edit', updateData: value })
                                                                        } 
                                                                    /> */}
                                                                    <i 
                                                                        class="icon icon-update1-ie updte_ics"
                                                                        onClick={
                                                                            () => this.setState({ categoryModalShow: true, categoryMode: 'Edit', updateData: value })
                                                                        } 
                                                                    ></i>
                                                                </a>
                                                            </div>
                                                            <span>
                                                                <button><i class="icon icon-email-pending archive_button"></i></button>
                                                            </span>
                                                        </div>
                                                    </Panel.Body>
                                                </Panel>
                                            ))}  </span> : 'Currently no reason found for this case.'
                                        }
                                        <div className="P_15_T">
                                            <div className="add_i_icon ">
                                                <a className="P_30_T">
                                                    <button class="add_i_icon " onClick={() => this.setState({ categoryModalShow: true, categoryMode: 'Add', updateData: '' })}><i class="icon icon-add-icons"></i></button>
                                                    {/* <span className="icon icon-add-icons" onClick={() => this.setState({ categoryModalShow: true, categoryMode: 'Add', updateData: '' })}></span> */}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>
                        <AddCategoryNotesPopup isModalShow={this.state.categoryModalShow} mode={this.state.categoryMode} closeCategoryPopUp={this.closeCategoryPopUp} caseId={this.state.case_detail.id} actionTbl="case_notes" get_case_detail={this.get_case_detail} dataAry={this.state.updateData} popName={'Case Reagion'} />


                    </div>
                </section>
                <Footer />
            </div>
        );
    }
}
export default FeedbackCaseDetail;

//category Add/Update
class AddCategoryNotesPopup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            caseId: this.props.caseId,
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ title: '', description: '', mode: '', created: '', created_by: '', created_type: '', id: '', mode: '', popName: '' });
        this.setState({ caseId: this.props.caseId });
        (nextProps.dataAry) ? this.setState(nextProps.dataAry) : '';
        (nextProps.popName) ? this.setState({ popName: nextProps.popName }) : '';
    }

    handleSaveBox = (e) => {
        e.preventDefault();	
		if (jQuery('#categoryBox').valid()) {
            postData('participant/Shift_Roster/submit_feedback_reason', this.state).then((result) => {
                if (result.status) {
                    toast.success('Reagion submitted successfully', {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
					this.props.get_case_detail();
					this.closeClear(true,this.state.caseId);
                } else {
                    this.setState({ error: result.error });
                }
            });
        }		
    }
    handleChangeChkboxInput(Obj, e) {
        var state = {};
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        Obj.setState(state);
    }

    closeClear = (status,caseId) => {
        this.setState({ title: '', description: '', mode: '' });        
        this.props.closeCategoryPopUp(status,caseId);
    }

    render() {
        return (
            <div>
                <Modal className="modal fade Modal_A Modal_B"
                    show={this.props.isModalShow}
                    onHide={this.handleHide}
                    container={this}
                    aria-labelledby="contained-modal-title"
                >
                    <Modal.Body>
                       

                        <h4 class="py-3 my-0 text-center heading_modal pd_r_35"> 
                            {this.props.mode && this.props.mode == 'Edit' ? 'Update ' + this.props.popName : 'Add New ' + this.props.popName}
                            {/* <a class="close_i" onClick={() => this.closeClear(false)}><img src="/assets/images/client/cross_icons.svg" /></a> */}
                        </h4>                            
                        <span onClick={() => this.closeClear(false)} className="icon icon-cross-icons-1 mdl_cross"></span>
                         <div class="my-0 mb-3"><div class="line"></div></div>   
                        <form id="categoryBox" autoComplete="off" method="post">
                            <div className="row P_15_T">
                                <div className="col-md-8">
                                    <label>Please Type a Title Here:</label>                                    
                                    <span className="required input_gradient required-n">
                                        <input type="text" className="input_pass" name="title" value={this.state['title'] || ''} onChange={(e) => this.handleChangeChkboxInput(this, e)} data-rule-required="true" />
                                    </span>
                                </div>
                                <div className="col-md-4">
                                </div>
                            </div>

                            <div className="row P_15_T">
                                <div className="col-md-12">
                                    <label>Short description:</label>
                                    <span className="required gradient_textarea">
                                        <textarea className="col-md-12 min-h-120 text_area cseDt_TxtAr" onChange={(e) => this.handleChangeChkboxInput(this, e)} name="description" value={this.state['description'] || ''} data-rule-required="true" data-rule-maxlength="500" ></textarea>
                                    </span>

                                </div>

                            </div>

                            <div className="row P_15_T">
                                <div class="col-md-6 mb-4 mt-3">                                    
                                    <input className="submit_button w-100" disabled={this.state.loading} type="submit"  value={'Save'} onClick={(e) => this.handleSaveBox(e)} />
                                </div>
                              
                            </div>
                        </form>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}
