import React, { Component } from 'react';
import jQuery from "jquery";
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import 'react-select-plus/dist/react-select-plus.css';
import moment from 'moment';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Timer from 'react-compound-timer';
import { Link } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import { postData, handleShareholderNameChange, getOptionsSuburb, handleAddShareholder, handleRemoveShareholder, getPercentage,handleDateChangeRaw } from '../../service/common';
import { addressCategory } from '../../service/ParticipantDropdown';
import '../../service/jquery.validate.js';
class ProvideFeedback extends Component {
    constructor(props) {
        super(props);
        this.state = {
            location: [{ address: '', state: '', suburb: '', postal: '' }],
            ShiftMemberOption: [],
            shiftId: '',
			categoryid:''
        }
    }
    handleShareholderNameChange = (obj, stateName, index, fieldName, value) => {
        var state = {};
        var tempField = {};
        var List = obj.state[stateName];
        List[index][fieldName] = value
        if (fieldName === 'state') {
            List[index]['suburb'] = {}
            List[index]['postal'] = ''
        }
        if (fieldName === 'suburb' && value) {
            List[index]['postal'] = value.postcode
        }
        state[stateName] = List;
        obj.setState(state);
    }
    selectChange = (key, value) => {
        var state = {}
        if (key === 'shift_date') {
            this.setState({ shiftLoading: true })
            postData('participant/Shift_Roster/get_shift_list_for_feedback', { shift_date: value }).then((result) => {
                if (result.status) {
                    this.setState({ loadedShiftOption: result.data })
                } else {
                    this.setState({ error: result.error });
                }
                this.setState({ shiftLoading: false })
            });
        }
        state[key] = value;
        this.setState(state);
    }
    submitFeedback = () => {       	
        if (jQuery('#submit_feedback').valid()) {
            postData('participant/Shift_Roster/submit_feedback', this.state).then((result) => {
                if (result.status) {
                    toast.success('Feedback submit successfully', {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    this.setState({ shift_date: '', shiftId: '', shiftMemberId: '', feedback_message: '', location: [{ address: '', state: '', suburb: '', postal: '' }] })
                } else {
					toast.error(result.error, {
								 position: toast.POSITION.TOP_CENTER,
								 hideProgressBar: true
							});				
                    this.setState({ error: result.error });
                }
            });
        }
    }
    onChangeShiftTime = (e) => {
        this.setState({ shiftId: e }, () => {
            postData('participant/Shift_Roster/get_member_by_shift', this.state).then((result) => {
                if (result.status) {                    
                    this.setState({ ShiftMemberOption: result.data });
                }
            });

        });
    }
    componentDidMount() {	
		postData('participant/Dashboard/get_state', {}).then((result) => {
            if (result.status) {
                var details = result.data
                this.setState({ stateList: details });
            }
        });	
		postData('participant/Shift_Roster/get_feedback_category_list', {}).then((result) => {
            if (result.status) {
                var details = result.data
                this.setState({ categoryList: details });
            }
        });
    }
    render() {
        return (
            <div>
                <Header title={'Provide Feedback'} />
                <section>					
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 mt-2"><div className="line"></div></div>
                        </div>
                        <div className="row">
                            <div className="col-md-12 back-arrow"><Link to="/dashboard"><i className="icon icon-back-arrow"></i></Link></div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 mt-2"><div className="line"></div></div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 my-3"><h5>Ongoing Feedback Cases:</h5></div>
                            <div className="col-lg-12 mb-3"><div className="line"></div></div>
                        </div>

                        <OngoingFeedbackListing />

                        <div className="row">
                            <div className="col-lg-12 mt-3"><div className="line"></div></div>
                            <div className="col-lg-12 my-3"><h5>Tell us what happened</h5></div>
                            <div className="col-lg-12 mb-3"><div className="line"></div></div>
                        </div>
                        <form id="submit_feedback">
                            <div className="row">
                                <div className="Happy_text my-4">Happy or Sad? We understand and are here to help :)
                                <span>Use the form below, to let us know how you feel your care is going.</span>
                                    Please be as accurate. honest and detailed as possible with any feedback you wish to provide. </div>
                                <div className="col-lg-3 mb-4" align="center">
                                    <label>Select Date</label>
                                    <span className="input_2 required-n dataPicker_w">
                                        <DatePicker required={true} data-rule-email="true" dateFormat="DD/MM/YYYY" maxDate={moment()} placeholderText="00/00/0000" className="text-center input_pass default_validation" selected={this.state.shift_date ? moment(this.state.shift_date) : null} name="shift_date" onChange={(e) => this.selectChange('shift_date', e)} onChangeRaw={handleDateChangeRaw} />
                                    </span>
                                </div>
                                <div className="col-lg-3 col-sm-5 mb-4" align="center">
                                    <label>Select Shift Time</label>
                                    <div className="required-n">
                                        <Select clearable={false} required={true} className="custom_select my-select default_validation" simpleValue={true} searchable={true} value={this.state.shiftId} onChange={(e) => this.onChangeShiftTime(e)}
                                            options={this.state.loadedShiftOption} placeholder="Please Select" disabled={this.state.shift_date ? false : true} isLoading={this.state.shiftLoading} />
                                    </div>
                                </div>
                                <div className="col-lg-3 col-sm-5 mb-4" align="center">
                                    <label>Select Shift Member</label>
                                    <div className="required-n">
                                        <Select clearable={false} required="true" className="custom_select my-select default_validation" simpleValue={true} searchable={true} value={this.state.shiftMemberId}
                                            options={this.state.ShiftMemberOption} placeholder="Please Select" isLoading={this.state.shiftLoading} onChange={(e) => this.selectChange('shiftMemberId', e)} />
                                    </div>
                                </div>

                                <div className="col-lg-3 col-sm-5 mb-4" align="center">
                                    <label>Category</label>
                                    <div className="required-n">
                                        <Select clearable={false} name="category" className="custom_select my-select" simpleValue={true} searchable={false} placeholder="Please Select" isLoading={this.state.shiftLoading} value={this.state.categoryid} options={this.state.categoryList} onChange={(e) => this.selectChange('categoryid', e)}  />
                                    </div>
                                </div>
                            </div>
                            {this.state.location.map((address, index) => (
                                <div className="row P_15_T mb-3" key={index + 1}>
                                    <div className="col-md-5  mb-3">
                                        <label>Location: </label>
                                        <span className="input_2 required-n">
                                            <input 
                                                type="text" 
                                                className="input_pass" 
                                                data-rule-required="true" 
                                                onChange={(e) => this.handleShareholderNameChange(this, 'location', index, 'address', e.target.value)} 
                                                value={address.address} name={'location' + index} 
                                                placeholder="Unit #/Street #, Street Name" />
                                        </span>
                                    </div>
                                    <div className="col-md-2 mb-3">
                                        <label>State: </label>
                                        <div className="required-n">
                                            <Select name="state" clearable={false} className="default_validation my-select" simpleValue={true} required={true} value={address.state} onChange={(e) => this.handleShareholderNameChange(this, 'location', index, 'state', e)} options={this.state.stateList} placeholder="Please Select" />
                                        </div>
                                    </div>
                                    <div className="col-md-3 mb-3 modify_select">
                                        <label>Suburb:</label>
                                        <div className="input_2 required-n">
                                            <Select.Async clearable={false} className="default_validation my-select" required={true}
                                                name={'suburb' + index} value={address.suburb} disabled={(address.state) ? false : true} loadOptions={(val) => getOptionsSuburb(val, address.state)} onChange={(e) => this.handleShareholderNameChange(this, 'location', index, 'suburb', e)} options={this.state.stateList} placeholder="Please Select" />

                                        </div>
                                    </div>
                                    <div className="col-md-2 mb-3">
                                        <label>Postcode: </label>
                                        <span className="input_2 required-n">
                                            <input type="text" className="input_pass text-center" value={address.postal} onChange={(e) => this.handleShareholderNameChange(this, 'location', index, 'postal', e.target.value)} data-rule-number="true" data-rule-required="true" minLength="3" maxLength="5" name={'postcode' + index} placeholder="0000" />
                                        </span>
                                    </div>
                                    <div className="col-md-3 mb-3">
                                        <label>Location Category:</label>
                                        <div className="required-n">
                                            <Select clearable={false} className="default_validation my-select" simpleValue={true} required={true}
                                                name={'locationCategory' + index} options={addressCategory(0)} placeholder="Please Select" onChange={(e) => this.handleShareholderNameChange(this, 'location', index, 'locationCategory', e)} value={address.locationCategory} />
                                        </div>
                                    </div>
                                    <div className="col-md-1 mb-3 align-self-end">
                                        <label></label>
                                        {index > 0 ? <button className="button_unadd" onClick={(e) => handleRemoveShareholder(this, e, index, 'location')}>
                                            <i className="icon icon-decrease-icon icon_cancel_1" ></i>
                                        </button> : (this.state.location.length === 3) ? '' :
                                                <button onClick={(e) => handleAddShareholder(this, e, 'location', address)} className="add_i_icon default_but_remove"><i className="icon icon-add-icons"></i></button>}
                                    </div>
                                </div>
                            ))}


                            <div className='row'>
                                <div className="col-md-5  mb-3">
                                    <label>Title: </label>
                                    <span className="input_2 required-n">
                                        <input type="text" className="input_pass" name="title" data-rule-required="true" placeholder="Title" onChange={(e) => this.selectChange('title', e.target.value)} value={this.state.title}/>
                                    </span>
                                </div>
                            </div>
                            
                            <div className="row">
                                <div className="col-lg-12 mb-3">
                                    <span className="gradient_textarea">
                                        <textarea className="text_area" data-rule-required="true" data-msg-required="Please provide your feedback here" onChange={(e) => this.selectChange('feedback_message', e.target.value)} value={this.state.feedback_message} placeholder="Please provide your feedback here :)"></textarea>
                                    </span>
                                </div>
                            </div>


                            <div className="row my-3">
                                <div className="col-lg-9 col-sm-6">&nbsp;</div>
                                <div className="col-lg-3 col-sm-5">
                                    <a onClick={this.submitFeedback} className="submit_but">Submit</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
                <Footer />
            </div>
        );
    }
}
export default ProvideFeedback;
class OngoingFeedbackListing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ongoingFeedback: []
        }
    }
    componentDidMount() {
        postData('participant/Shift_Roster/get_ongoing_feedback', {}).then((result) => {
            if (result.status) {
                this.setState({ ongoingFeedback: result.data })
            }
        });
    }
    render() {
        return (
            <div>
                {this.state.ongoingFeedback.length > 0 ?
                    this.state.ongoingFeedback.map((val, index) => (
                        <div className="row" key={index + 1}>
                            <div className="col-lg-12">
                                <div className="d-flex time_schedule">
                                    <div className="col-lg-10 offset-lg-1">
                                        <ul>
                                            <li><label>Case ID:</label> {val.id}</li>
                                            <li style={{width:'210px'}}><label>Timer : </label> 											
											<Timer initialTime= {val.created}> 
												{() => (
													<React.Fragment>
														 <Timer.Days />:<Timer.Hours />:<Timer.Minutes />:<Timer.Seconds />
													</React.Fragment>
												)}
											</Timer>
											
											
											</li>
                                            <li><label>Date of Case:</label> {moment(val.event_date).format('DD/MM/YYYY')}</li>
                                        </ul>
                                    </div>
                                    <div className="col-lg-1">
                                        <a href={'feedback/case/'+val.id} >
                                            {/* <img className="py-3" width="35px" src="/assets/images/view.png" alt='view' /> */}
                                            <i className='icon icon-view vw_ic2'></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )) : <div className="row">
                        <div className="col-lg-12">
                            <div className="d-flex time_schedule">
                                <div className="col-lg-10 offset-lg-1">
                                    <ul>
                                        <li>No record found</li>
                                    </ul>
                                </div>
                                <div className="col-lg-1 ">
                                    <a ><i class="icon icon-view vw_ic2"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        );
    }
}