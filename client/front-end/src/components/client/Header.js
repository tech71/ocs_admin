import React, { Component } from 'react';
import BlockUi from 'react-block-ui';

import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import jQuery from "jquery";
import { ROUTER_PATH, BASE_URL, NPM_URL } from '../../config.js';
import { postData, checkItsNotLoggedIn, logout, googleTranslateElementInit, checkLoginWithReturnTrueFalse } from '../../service/common.js';

import { setNotificationData} from './actions/NotificationAction.js';
import { connect } from 'react-redux'


class Header extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();
    }

    logout() {
        logout(ROUTER_PATH);
    }

    render() {
         if(!checkLoginWithReturnTrueFalse()){return <Redirect to="/" />}
                return (
        <header>
    <div className="container">

        <div className="row">
            <div className="col">
                <div className="logo">
                    <img className="img-fluid" src="/assets/images/logo_oncall.svg" />

                </div>
            </div>


            <div className="col text-center">
                <div className="logo">
                    <Link to="/dashboard">
                    {/* <img className="img-fluid" src="/assets/images/house.svg" /> */}
                    <i className='icon icon-house mn_logo_ic'></i>
                    </Link>
                </div>
                <div id="google_translate_element"></div>
            </div>
            <div className="col text-right">
                <div className="logo flx_logo1">
                    {
                /* <img className="img-fluid" src="/assets/images/info.svg" /> */}
                    <i className='icon icon-error-icons ifo_ic_1'></i>
                    <a className="left-i" onClick={
                this.logout.bind(this)} data-placement="right" data-toggle="tooltip" title="Log Out">
                        <i className="icon icon-log-out log_out_icon"></i>
                    </a>
                </div>
            </div>
        </div>

        <div className="row">
            <div className="col-md-12">
                <h1 className="user_name hdng_12" align="center">
                    {
                this.props.title
                    } 
                    {
                (this.props.smiley) ? <i className='icon icon-smail-big smile_ic_hd'></i> : ''
                    }
                </h1>
            </div>
        </div>
    </div>
    
</header>

                );
    }
}

const mapStateToProps = state => ({
  
})

const mapDispatchtoProps = (dispach) => {
    return {
        setNotificationData : (data) => dispach(setNotificationData(data))
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(Header);
