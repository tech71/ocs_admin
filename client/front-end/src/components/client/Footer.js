import React, { Component } from 'react';

class Footer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
                <div>
                <footer className="text-center footer">

	<div className="container">

		<div className="row">

			<div className="col-lg-12">

				<ul className="footer_nav">

					<li><a>About My ONCALL</a></li>

					<li><a>Terms & Conditions</a></li>

					<li className="footer_1 hidden-md-down"><span>&#x24B8;2018-All Right Reserved <b>ONCALL</b></span></li>

					<li><a>Privacy Policy</a></li>

					<li><a>Staff Policies</a></li>

					<li className="footer_1 hidden-lg-up"><span>&#x24B8;2018-All Right Reserved <b>ONCALL</b></span></li>

				</ul>

			</div>

		</div>

	</div>

</footer>
               
                </div>
                );
    }
}

export default Footer
