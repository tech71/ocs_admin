import { BrowserRouter as Router, Switch, Route, Link } from 'react/lib/react-router-dom';
import Login from 'react/components/admin/Login';
import BlockUi from 'react/lib/reactblockui.min';


import Forgot_password from 'react/components/admin/Forgot_password';
import Reset_password from 'react/components/admin/Reset_password';

class AppContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
                <Router>
                    <div>
                        <Switch>
                            <Route exact path={routerPath} render={() => <Login auth={this.props} />}  /> 
                            <Route exact path={routerPath+'forgot-password'}  component={Forgot_password} />
                            <Route exact path={routerPath+'reset-password/:id/:id'} render={() => <Reset_password data={this.props} />} />        
                        </Switch>
                    </div>
                </Router>
                );
    }
}

export default AppContainer