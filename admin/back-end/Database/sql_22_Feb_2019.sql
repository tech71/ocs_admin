ALTER TABLE `tbl_role_permission`
CHANGE `id` `id` int(10) NOT NULL AUTO_INCREMENT FIRST;


ALTER TABLE `tbl_department` ADD `created` datetime NOT NULL;
INSERT INTO `tbl_role` (`companyId`, `name`, `status`, `archive`, `created`)
VALUES ('0', 'Recruitment', '1', '0', now());
INSERT INTO `tbl_permission` (`companyId`, `permission`, `title`)
VALUES ('1', 'access_recruitment', 'Access only Recruitment Portal');

INSERT INTO `tbl_role_permission` (`roleId`, `permission`)
VALUES ('10', '34');
CREATE TABLE `tbl_recruitment_staff` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `adminId` int NOT NULL,
  `departmentId` int NOT NULL
);
ALTER TABLE `tbl_admin_phone`
ADD `id` tinyint NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST,
ADD `archive` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1- Delete';
ALTER TABLE `tbl_admin_email`
ADD `id` tinyint NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST,
ADD `archive` enum('1','0') NOT NULL COMMENT '1- Delete';
ALTER TABLE `tbl_admin`
ADD `created` datetime NOT NULL;
CREATE TABLE `tbl_recruitment_department` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(100) NOT NULL,
 `archive` tinyint(4) NOT NULL COMMENT '0- Not / 1 - Delete',
 `created` datetime NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `tbl_logs`
CHANGE `module` `module` int(11) NULL COMMENT '1- Admin/ 2- Participant / 3 - Member/ 4 - Schedule / 5 - FSM / 6 - House / 7 - Organization / 8 - Imail / 9 - Recruitment' AFTER `userId`;

ALTER TABLE `tbl_participant` CHANGE `created` `created` TIMESTAMP NOT NULL;

CREATE TABLE `tbl_recruitment_staff_department_allocations` (
 `id` tinyint(3) NOT NULL AUTO_INCREMENT,
 `allocated_department` int(11) NOT NULL,
 `status` int(11) NOT NULL COMMENT '1- Active, 0- Inactive',
 `created` datetime NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `tbl_participant` CHANGE `archive` `archive` TINYINT(2) NOT NULL COMMENT '0- not / 1 - yes';

ALTER TABLE `tbl_participant_docs`
ADD `expiry_date` date NOT NULL DEFAULT '0000-00-00',
ADD `category` tinyint NOT NULL DEFAULT '0' AFTER `expiry_date`;
ALTER TABLE `tbl_participant_docs`
CHANGE `expiry_date` `expiry_date` date NULL DEFAULT '0000-00-00' AFTER `archive`,
CHANGE `category` `category` tinyint(4) NULL DEFAULT '0' AFTER `expiry_date`;

ALTER TABLE `tbl_group_message_action`
CHANGE `status` `status` int(11) NOT NULL COMMENT '1 - unread/ 2 - read / 3 - archive' AFTER `senderId`;
ALTER TABLE `tbl_recruitment_staff_department_allocations`
ADD `departmentId` int NOT NULL AFTER `id`;
ALTER TABLE `tbl_recruitment_staff_department_allocations`
CHANGE `departmentId` `adminId` int(11) NOT NULL AFTER `id`;

CREATE TABLE `tbl_admin_pin_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adminId` int(11) NOT NULL,
  `token_id` int(11) NOT NULL COMMENT 'tbl_admin_login primary key',
  `pin` text NOT NULL COMMENT 'generated jwt token ',
  `token_type` tinyint(4) NOT NULL COMMENT '1 for fms, 2 for admin',
  `ip_address` varchar(100) NOT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `tbl_permission`
ADD `pin_type` tinyint NOT NULL DEFAULT '0' COMMENT 'it help for pin token verify check';
UPDATE `tbl_permission` SET `pin_type` = '2' WHERE `id` = '13';
UPDATE `tbl_permission` SET `pin_type` = '2' WHERE `id` = '14';
UPDATE `tbl_permission` SET `pin_type` = '2' WHERE `id` = '15';
UPDATE `tbl_permission` SET `pin_type` = '2' WHERE `id` = '16';
UPDATE `tbl_permission` SET `pin_type` = '1' WHERE `id` = '25';
UPDATE `tbl_permission` SET `pin_type` = '1' WHERE `id` = '26';
UPDATE `tbl_permission` SET `pin_type` = '1' WHERE `id` = '27';
UPDATE `tbl_permission` SET `pin_type` = '1' WHERE `id` = '28';
UPDATE `tbl_permission` SET `pin_type` = '3' WHERE `id` = '29';

ALTER TABLE `tbl_external_message_recipient` ADD `is_notify` TINYINT(1) NOT NULL COMMENT '0- not / 1 - yes' AFTER `is_read`;
ALTER TABLE `tbl_internal_message_recipient` ADD `is_notify` TINYINT(1) NOT NULL COMMENT '0- not / 1 - yes' AFTER `is_read`;

ALTER TABLE `tbl_organisation_site`
ADD `abn` varchar(20) COLLATE 'latin1_swedish_ci' NULL AFTER `postal`;



ALTER TABLE `tbl_admin_email`
CHANGE `archive` `archive` enum('1','0') COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT '0' COMMENT '1- Delete' AFTER `primary_email`;



DROP TABLE IF EXISTS `tbl_external_message_recipient`;
CREATE TABLE `tbl_external_message_recipient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `messageContentId` int(8) NOT NULL,
  `messageId` int(8) NOT NULL,
  `recipinent_type` int(11) NOT NULL COMMENT '1 - admin / 2 - participant / 3 - member / 4 - organisation',
  `recipinentId` int(11) NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1- Yes, 0- No',
  PRIMARY KEY (`id`),
  KEY `recipientId` (`messageContentId`),
  KEY `messageId` (`messageId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `tbl_external_message`;
CREATE TABLE `tbl_external_message` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `companyId` smallint(5) NOT NULL,
  `title` varchar(228) NOT NULL,
  `is_block` int(11) NOT NULL COMMENT '0- not / 1 - yes',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_external_message_action`;
CREATE TABLE `tbl_external_message_action` (
  `messageId` int(11) NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '1 - admin / 2 - participant / 3 - member / 4 - organisation',
  `userId` int(11) NOT NULL,
  `is_fav` int(11) NOT NULL COMMENT '0- not / 1 - yes',
  `is_flage` int(11) NOT NULL COMMENT '0- not / 1 - yes',
  `archive` int(11) NOT NULL COMMENT '0- not / 1 - yes'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_external_message_attachment`;
CREATE TABLE `tbl_external_message_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `messageContentId` int(11) NOT NULL,
  `filename` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_external_message_content`;
CREATE TABLE `tbl_external_message_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `messageId` int(10) NOT NULL,
  `sender_type` tinyint(1) NOT NULL COMMENT '1 - admin / 2 - participant / 3 - member / 4 - organisation',
  `userId` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `content` text NOT NULL,
  `is_priority` int(11) NOT NULL COMMENT '0 - No / 1 - Yes',
  `is_reply` int(11) NOT NULL COMMENT '0- not / 1 - yes',
  `is_draft` int(11) NOT NULL COMMENT '0- not / 1 - yes',
  PRIMARY KEY (`id`),
  KEY `messageId` (`messageId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_external_message_recipient`;
CREATE TABLE `tbl_external_message_recipient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `messageContentId` int(8) NOT NULL,
  `messageId` int(8) NOT NULL,
  `recipinent_type` int(11) NOT NULL COMMENT '1 - admin / 2 - participant / 3 - member / 4 - organisation',
  `recipinentId` int(11) NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1- Yes, 0- No',
  `is_notify` tinyint(1) NOT NULL COMMENT '0- not / 1 - yes',
  PRIMARY KEY (`id`),
  KEY `recipientId` (`messageContentId`),
  KEY `messageId` (`messageId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_internal_message`;
CREATE TABLE `tbl_internal_message` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `companyId` smallint(5) NOT NULL,
  `title` varchar(200) NOT NULL,
  `is_block` int(11) NOT NULL COMMENT '0- not / 1 - yes',
  `created` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `companyId` (`companyId`),
  KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_internal_message_action`;
CREATE TABLE `tbl_internal_message_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `messageId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT '0- not / 1 - yes',
  `is_fav` int(11) NOT NULL COMMENT '0- not / 1 - yes',
  `archive` int(11) NOT NULL COMMENT '0- not / 1 - yes',
  `is_flage` int(11) NOT NULL COMMENT '0- not / 1 - yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_internal_message_attachment`;
CREATE TABLE `tbl_internal_message_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `messageContentId` int(11) NOT NULL,
  `filename` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_internal_message_content`;
CREATE TABLE `tbl_internal_message_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `messageId` int(10) NOT NULL,
  `senderId` int(8) NOT NULL,
  `content` text NOT NULL,
  `is_priority` int(11) NOT NULL COMMENT '0- not / 1 - yes',
  `is_draft` int(11) NOT NULL COMMENT '0- not / 1 - yes',
  `is_reply` int(11) NOT NULL COMMENT '0- not / 1 - yes',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `messageId` (`messageId`),
  KEY `senderId` (`senderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_internal_message_recipient`;
CREATE TABLE `tbl_internal_message_recipient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `messageContentId` int(11) NOT NULL,
  `messageId` int(10) NOT NULL,
  `recipientId` int(8) NOT NULL,
  `cc` int(11) NOT NULL COMMENT '0- not / 1 - yes',
  `is_read` int(11) NOT NULL COMMENT '0- not / 1 - yes',
  `is_notify` tinyint(1) NOT NULL COMMENT '0- not / 1 - yes',
  PRIMARY KEY (`id`),
  KEY `messageId` (`messageId`),
  KEY `recipientId` (`recipientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_role` (`companyId`, `name`, `status`, `archive`, `created`)
VALUES ('1', 'CRM Admin', '1', '0', now());

INSERT INTO `tbl_permission` (`companyId`, `permission`, `title`) VALUES
(1, 'access_crm_admin', 'Access only CRM Admin Portal');

INSERT INTO `tbl_role_permission` (`roleId`, `permission`)
VALUES ('11', '35'); 


 
CREATE TABLE `tbl_crm_department` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `hcmgr_id` varchar(20) NOT NULL,
  `archive` tinyint(4) NOT NULL COMMENT '0- Not / 1 - Delete',
  `start_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 
CREATE TABLE `tbl_crm_participant` (
  `id` int(8) NOT NULL,
  `username` varchar(200) DEFAULT NULL,
  `firstname` varchar(32) NOT NULL,
  `middlename` varchar(32) DEFAULT NULL,
  `lastname` varchar(32) NOT NULL,
  `gender` tinyint(1) DEFAULT NULL COMMENT '1- Male, 2- Female',
  `profile_image` varchar(255) NOT NULL,
  `relation` varchar(50) NOT NULL,
  `preferredname` varchar(32) NOT NULL,
  `prefer_contact` int(11) NOT NULL COMMENT '1-for contact/2-for-email',
  `dob` date NOT NULL,
  `ndis_num` varchar(15) NOT NULL,
  `medicare_num` varchar(15) NOT NULL,
  `behavioural_support_plan` tinyint(1) NOT NULL COMMENT '1- Yes, 0- No',
  `referral` tinyint(2) NOT NULL COMMENT '1- Yes, 0- No',
  `referral_firstname` varchar(200) NOT NULL,
  `referral_lastname` varchar(200) NOT NULL,
  `referral_email` varchar(100) NOT NULL,
  `referral_phone` varchar(20) NOT NULL,
  `living_situation` varchar(200) NOT NULL,
  `aboriginal_tsi` varchar(20) DEFAULT NULL,
  `oc_departments` tinyint(2) DEFAULT NULL,
  `houseId` int(5) DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL COMMENT '1- Active, 0- Inactive',
  `other_relevant_plans` tinyint(1) NOT NULL COMMENT '1- Yes, 0- No',
  `archive` tinyint(1) NOT NULL,
  `assigned_to` varchar(64) NOT NULL,
  `action_status` tinyint(1) NOT NULL COMMENT '1 - Phone Screening, 2- Call ',
  `booking_status` int(11) NOT NULL COMMENT '1 - ''Pending Contact'', 2- ''Unassigned'', 3-''Successful'', 4-''Processing'', 5- ''Rejected''',
  `booking_date` date DEFAULT NULL,
  `referral_org` varchar(30) DEFAULT NULL,
  `referral_relation` varchar(30) DEFAULT NULL,
  `marital_status` tinyint(1) NOT NULL COMMENT '''1-Married, 2-Single, 3-Divorced, 4-Widowed'' '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 
CREATE TABLE `tbl_crm_participant_ability` (
  `id` int(11) NOT NULL,
  `crm_participant_id` int(11) NOT NULL,
  `cognitive_level` varchar(64) DEFAULT NULL,
  `communication` varchar(64) DEFAULT NULL,
  `hearing_interpreter` tinyint(1) DEFAULT NULL COMMENT '1- Yes, 0- No',
  `language_interpreter` tinyint(1) DEFAULT NULL COMMENT '1- Yes, 0- No',
  `languages_spoken` varchar(64) DEFAULT NULL,
  `linguistic_diverse` tinyint(1) DEFAULT NULL COMMENT '1- Yes, 0- No',
  `require_assistance` varchar(64) DEFAULT NULL,
  `require_mobility` varchar(64) DEFAULT NULL,
  `docs` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 CREATE TABLE `tbl_crm_participant_address` (
  `id` int(8) NOT NULL,
  `crm_participant_id` int(8) NOT NULL DEFAULT '0',
  `street` varchar(128) NOT NULL,
  `city` varchar(64) NOT NULL,
  `postal` varchar(10) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `address_type` tinyint(2) NOT NULL COMMENT '1-Own Home,2-Family Home , 3 -Mum''s House, 4- Dad''s House , 5- Relative''s House, 6- Friend''s House, 7 - OnCall House',
  `lat` varchar(200) DEFAULT NULL,
  `long` varchar(200) DEFAULT NULL,
  `site_category` tinyint(1) NOT NULL,
  `primary_address` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary',
  `archive` tinyint(1) NOT NULL COMMENT '0-for-not-deleted/1-for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `tbl_crm_participant_disability` (
  `id` int(11) NOT NULL,
  `crm_participant_id` int(11) DEFAULT NULL,
  `fomal_diagnosis` tinyint(1) DEFAULT NULL COMMENT '1- Primary, 2- Secondary',
  `fomal_diagnosis_desc` varchar(100) NOT NULL,
  `other_relevant_information` varchar(64) DEFAULT NULL,
  `legal_issues` varchar(64) DEFAULT NULL,
  `linked_fms_case_id` int(11) DEFAULT NULL,
  `status` varchar(64) DEFAULT NULL,
  `docs` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_crm_participant_docs` (
  `id` int(8) NOT NULL,
  `crm_participant_id` int(8) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1- Service, 2- SIL Doc',
  `title` varchar(32) NOT NULL,
  `filename` varchar(64) NOT NULL,
  `created` datetime NOT NULL,
  `archive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_crm_participant_email` (
  `id` int(11) NOT NULL,
  `crm_participant_id` int(8) NOT NULL,
  `email` varchar(64) NOT NULL,
  `primary_email` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_crm_participant_kin` (
  `id` int(8) NOT NULL,
  `crm_participant_id` int(8) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `relation` varchar(24) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(64) NOT NULL,
  `primary_kin` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary',
  `archive` tinyint(1) NOT NULL COMMENT '0 - Not / - archive',
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_crm_participant_phone` (
  `id` int(11) NOT NULL,
  `crm_participant_id` int(8) NOT NULL DEFAULT '0',
  `phone` varchar(20) NOT NULL,
  `primary_phone` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_crm_participant_plan` (
  `id` int(8) NOT NULL,
  `crm_participant_id` int(8) NOT NULL,
  `plan_type` tinyint(1) NOT NULL,
  `plan_id` varchar(20) NOT NULL,
  `start_date` varchar(20) NOT NULL,
  `end_date` varchar(20) NOT NULL,
  `total_funding` double(10,2) NOT NULL DEFAULT '0.00',
  `fund_used` double(10,2) NOT NULL DEFAULT '0.00',
  `remaing_fund` double(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_crm_participant_schedule_task` (
  `id` int(11) NOT NULL,
  `crm_participant_id` int(11) NOT NULL,
  `task_name` varchar(30) NOT NULL,
  `category` int(11) NOT NULL,
  `due_date` datetime NOT NULL,
  `assign_to` varchar(30) NOT NULL,
  `relevant_task_note` varchar(100) NOT NULL,
  `parent_id` int(11) NOT NULL COMMENT '0-for parent',
  `note` varchar(100) NOT NULL,
  `task_status` tinyint(2) NOT NULL COMMENT '1-completed,2-selected',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 
CREATE TABLE `tbl_crm_participant_schedule_task_docs` (
  `id` int(11) NOT NULL,
  `crm_task_id` int(11) NOT NULL,
  `documents` varchar(50) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_crm_participant_shifts` (
  `id` int(8) NOT NULL,
  `crm_participant_id` int(8) NOT NULL,
  `title` varchar(228) NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) NOT NULL COMMENT '1- Active / 2 - Archive',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `tbl_crm_participant_stage` (
  `id` int(11) NOT NULL,
  `stage_id` int(11) NOT NULL,
  `crm_participant_id` int(11) NOT NULL,
  `crm_member_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1- Active, 0- Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 
CREATE TABLE `tbl_crm_participant_stage_docs` (
  `id` int(11) DEFAULT NULL,
  `crm_participant_id` int(50) DEFAULT NULL,
  `stage_id` int(11) NOT NULL,
  `file_path` varchar(64) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT '1- Active, 0- Inactive',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 
CREATE TABLE `tbl_crm_participant_stage_notes` (
  `id` int(11) NOT NULL,
  `crm_participant_id` int(50) DEFAULT NULL,
  `stage_id` int(11) DEFAULT NULL,
  `notes` varchar(64) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT '1- Active, 0- Inactive',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 
CREATE TABLE `tbl_crm_staff` (
  `id` int(8) NOT NULL,
  `companyId` smallint(3) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `middlename` varchar(32) NOT NULL,
  `preferredname` varchar(64) NOT NULL,
  `profile_image` varchar(255) NOT NULL,
  `user_type` tinyint(1) NOT NULL COMMENT '0- Support Coordinator, 1- Member',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1- Active, 0- Inactive',
  `prefer_contact` varchar(6) NOT NULL,
  `dob` date NOT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1- Male, 2- Female',
  `department_id` tinyint(2) NOT NULL COMMENT '1- Accommodation & Client Services,2-Casual Staff Services,3-People & Culture, 4-Business Systems, 5-Marketing, 6- Finance',
  `position` varchar(100) NOT NULL,
  `hcmgr_id` varchar(20) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `dwes_confirm` tinyint(1) NOT NULL COMMENT '1- Confirm, 0- not Confirm',
  `account_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-Enable Account, 0-Disable Account',
  `archive` tinyint(1) NOT NULL COMMENT '1- Delete',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `tbl_crm_staff` (`id`, `companyId`, `firstname`, `lastname`, `middlename`, `preferredname`, `profile_image`, `user_type`, `status`, `prefer_contact`, `dob`, `gender`, `department_id`, `position`, `hcmgr_id`, `start_date`, `end_date`, `dwes_confirm`, `account_status`, `archive`, `created`) VALUES
(1, 0, 'Tommy', 'test', 'Peter', '', '', 0, 0, '', '1988-03-08', 1, 1, 'Recruitment Office', '00120812-12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 0, '2018-07-25 16:09:05'),
(2, 0, 'Rose', 'Mary', '', '', '', 0, 1, '', '1989-12-08', 1, 2, 'Recruitment Office', '00120812-12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, '2017-07-25 16:09:05'),
(3, 0, 'Micheal', 'test', 'Peter', '', '', 0, 1, '', '1988-04-09', 1, 3, 'Recruitment Office', '00120812-12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 0, '2018-07-25 16:09:05'),
(4, 0, 'Charlie', 'Kyle', '', '', '', 0, 1, '', '1991-08-07', 2, 4, 'Recruitment Office', '00120812-12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, '2018-07-25 16:09:05'),
(5, 0, 'Amelia', 'Margaret', '', '', '', 0, 0, '', '1992-11-10', 1, 5, 'Recruitment Office', '00120812-12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, '2018-07-25 16:09:05'),
(6, 1, 'Jennilee', 'M\'Quharg', '', 'Eula', '', 1, 1, '1', '2018-09-05', 1, 6, 'Recruitment Office', '00120812-12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, '2018-04-24 00:00:00'),
(7, 1, 'Aundrea', 'Goaley', '', 'Jo', '', 1, 1, '1', '2018-04-27', 2, 1, 'Recruitment Office', '00120812-12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, '2018-09-30 00:00:00'),
(8, 1, 'Junie', 'Whiskin', '', 'Amalia', '', 1, 1, '1', '2017-12-31', 1, 2, 'Recruitment Office', '00120812-12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, '2018-03-13 00:00:00'),
(9, 1, 'Terri', 'Wallentin', '', 'Wenona', '', 1, 1, '1', '2018-02-23', 1, 3, 'Recruitment Office', '00120812-12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, '2018-03-02 00:00:00'),
(10, 1, 'Tobiah', 'Thoresby', '', 'Marya', '', 1, 1, '1', '2017-10-24', 1, 4, 'Recruitment Office', '00120812-12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, '2018-01-04 00:00:00');

CREATE TABLE `tbl_crm_staff_address` (
  `id` int(8) NOT NULL,
  `memberId` int(8) NOT NULL DEFAULT '0',
  `street` varchar(128) NOT NULL,
  `city` varchar(64) NOT NULL,
  `postal` varchar(10) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `primary_address` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary',
  `lat` varchar(200) NOT NULL,
  `long` varchar(200) NOT NULL,
  `archive` tinyint(1) NOT NULL COMMENT '1- Delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_crm_staff_address` (`id`, `memberId`, `street`, `city`, `postal`, `state`, `primary_address`, `lat`, `long`, `archive`) VALUES
(1, 3, 'test streat 28', 'Melbourne', '2826', 0, 0, '', '', 0),
(2, 1, '288 latrobe,street', 'Melbourne', '3000', 7, 0, '-37.810220', '144.960940', 0),
(3, 2, '288 latrobe,street', 'Melbourne', '3000', 0, 1, '', '', 0),
(4, 2, '288 La Trobe Street', 'Melbourne', '3000', 7, 0, '-37.810220', '144.960940', 0),
(5, 3, '288 La Trobe Street', 'Melbourne', '3000', 7, 0, '-37.810220', '144.960940', 0),
(6, 6, 'P.O. Box 931, 3524 Dui, Rd.', 'Grafton', '7733', 2, 1, '47.06098', '-164.47648', 0),
(7, 7, '4615 Aenean Av.', 'Lithgow', '2782', 2, 1, '-85.01041', '-44.35972', 0),
(8, 8, '7005 Facilisis Ave', 'Sale', '2784', 7, 1, '-41.92513', '-0.5557', 0),
(9, 9, 'Ap #242-4710 Sed Rd.', 'Canning', '6091', 8, 1, '-54.68831', '-44.06746', 0),
(10, 10, 'P.O. Box 803, 5213 Nisl. Road', 'Penrith', '2266', 2, 1, '-3.13787', '20.1653', 0);

CREATE TABLE `tbl_crm_staff_disable` (
  `id` int(10) NOT NULL,
  `crm_staff_id` int(10) NOT NULL,
  `disable_account` varchar(30) NOT NULL,
  `account_allocated` varchar(30) NOT NULL,
  `account_allocated_to` int(10) NOT NULL,
  `relevant_note` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_crm_staff_email` (
  `id` int(11) NOT NULL,
  `memberId` int(8) NOT NULL,
  `email` varchar(64) NOT NULL,
  `primary_email` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary',
  `archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1- Delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_crm_staff_phone` (
  `id` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `primary_phone` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1- Primary, 2- Secondary',
  `memberId` int(8) NOT NULL,
  `archive` tinyint(1) NOT NULL COMMENT '1- Delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 
CREATE TABLE `tbl_crm_stage` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL COMMENT '0-for parent',
  `status` int(11) NOT NULL COMMENT '1- Active, 0- Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_crm_stage` (`id`, `name`, `parent_id`, `status`) VALUES
(1, 'Stage 1: NDIS Intake Participant Submission ', 0, 1),
(2, 'Stage 2 - Intake', 0, 1),
(3, 'Stage 3 - Plan Delegation', 0, 1),
(4, 'Stage 2.1 - Participant Assessment', 2, 1),
(5, 'Stage 2.2 - Client Contact', 2, 1),
(6, 'Stage 2.3 - Information Screening', 2, 1),
(7, 'Stage 2.4 - Recieve Documents', 2, 1),
(8, 'Stage 3.1 - Service Agreement Doc', 3, 1),
(9, 'Stage 3.2 - Funding Concent', 3, 1),
(10, 'Stage 3.3 - Locked Funding Confirmation', 3, 1),
(11, 'Stage 3.4 - Send Service Agreement', 3, 1),
(12, 'Stage 3.5 - Engage Services Into HCM', 3, 1),
(13, 'NDIS Intake Participant Submission', 0, 1),
(14, 'NDIS Intake Participant Submission', 0, 1),
(15, 'NDIS Intake Participant Submission', 0, 1),
(16, 'NDIS Intake Participant Submission', 0, 1),
(17, 'NDIS Intake Participant Submission', 0, 1),
(18, 'NDIS Intake Participant Submission', 0, 1),
(19, 'NDIS Intake Participant Submission', 0, 1),
(20, 'NDIS Intake Participant Submission', 0, 1);


CREATE TABLE `tbl_crm_task_category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1-''Active'',0-''InActive'''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_crm_task_category` (`id`, `name`, `code`, `status`) VALUES
(1, 'Category1', 'CA1', 1),
(2, 'Category2', 'CA2', 1);

ALTER TABLE `tbl_crm_department`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tbl_crm_participant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `firstname` (`firstname`),
  ADD KEY `middlename` (`middlename`),
  ADD KEY `lastname` (`lastname`),
  ADD KEY `preferredname` (`preferredname`),
  ADD KEY `status` (`status`),
  ADD KEY `created` (`created`) USING BTREE,
  ADD KEY `houseId` (`houseId`) USING BTREE,
  ADD KEY `id` (`id`);
ALTER TABLE `tbl_crm_participant` ADD FULLTEXT KEY `firstname_2` (`firstname`);
ALTER TABLE `tbl_crm_participant` ADD FULLTEXT KEY `middlename_2` (`middlename`);
ALTER TABLE `tbl_crm_participant` ADD FULLTEXT KEY `lastname_2` (`lastname`);

ALTER TABLE `tbl_crm_participant_ability`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);
 
ALTER TABLE `tbl_crm_participant_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participantId` (`crm_participant_id`);

ALTER TABLE `tbl_crm_participant_disability`
  ADD PRIMARY KEY (`id`);
 
ALTER TABLE `tbl_crm_participant_docs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participantId` (`crm_participant_id`),
  ADD KEY `type` (`type`);
 
ALTER TABLE `tbl_crm_participant_email`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participantId` (`crm_participant_id`);
 
ALTER TABLE `tbl_crm_participant_kin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participantId` (`crm_participant_id`);

 
ALTER TABLE `tbl_crm_participant_phone`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participantId` (`crm_participant_id`);

 
ALTER TABLE `tbl_crm_participant_plan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participantId` (`crm_participant_id`);
 
ALTER TABLE `tbl_crm_participant_schedule_task`
  ADD PRIMARY KEY (`id`);

 
ALTER TABLE `tbl_crm_participant_schedule_task_docs`
  ADD PRIMARY KEY (`id`);
 
ALTER TABLE `tbl_crm_participant_shifts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participantId` (`crm_participant_id`);
 
ALTER TABLE `tbl_crm_participant_stage`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tbl_crm_participant_stage_notes`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tbl_crm_staff`
  ADD PRIMARY KEY (`id`),
  ADD KEY `companyId` (`companyId`),
  ADD KEY `username` (`firstname`),
  ADD KEY `user_type` (`user_type`),
  ADD KEY `status` (`status`),
  ADD KEY `lastname` (`lastname`);
ALTER TABLE `tbl_crm_staff` ADD FULLTEXT KEY `firstname` (`firstname`);
ALTER TABLE `tbl_crm_staff` ADD FULLTEXT KEY `middlename` (`middlename`);
ALTER TABLE `tbl_crm_staff` ADD FULLTEXT KEY `lastname_2` (`lastname`);
 
ALTER TABLE `tbl_crm_staff_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `memberId` (`memberId`);
 
ALTER TABLE `tbl_crm_staff_disable`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tbl_crm_staff_email`
  ADD PRIMARY KEY (`id`);
 
ALTER TABLE `tbl_crm_staff_phone`
  ADD PRIMARY KEY (`id`);
 
ALTER TABLE `tbl_crm_stage`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tbl_crm_task_category`
  ADD PRIMARY KEY (`id`);
 
ALTER TABLE `tbl_crm_department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
 
ALTER TABLE `tbl_crm_participant`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
 
ALTER TABLE `tbl_crm_participant_address`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
 
ALTER TABLE `tbl_crm_participant_disability`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
 
ALTER TABLE `tbl_crm_participant_docs`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
 
ALTER TABLE `tbl_crm_participant_email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
 
ALTER TABLE `tbl_crm_participant_kin`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
 
ALTER TABLE `tbl_crm_participant_phone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
 
ALTER TABLE `tbl_crm_participant_plan`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
 
ALTER TABLE `tbl_crm_participant_schedule_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
 
ALTER TABLE `tbl_crm_participant_schedule_task_docs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
 
ALTER TABLE `tbl_crm_participant_shifts`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
  
ALTER TABLE `tbl_crm_participant_stage_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
 
ALTER TABLE `tbl_crm_staff`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
 
ALTER TABLE `tbl_crm_staff_address`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
  
ALTER TABLE `tbl_crm_stage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
 
ALTER TABLE `tbl_crm_task_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

 


-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2019 at 11:30 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ocs`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_recruitment_additional_questions`
--

CREATE TABLE `tbl_recruitment_additional_questions` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1- Active, 0- Inactive',
  `created` datetime NOT NULL,
  `created_by` int(10) NOT NULL COMMENT 'created by staff id',
  `question_type` tinyint(1) NOT NULL COMMENT '0 Multiple , 1 Single',
  `question_topic` tinyint(3) NOT NULL,
  `training_category` tinyint(1) NOT NULL,
  `updated` datetime NOT NULL,
  `archive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_recruitment_additional_questions_answer`
--

CREATE TABLE `tbl_recruitment_additional_questions_answer` (
  `id` int(11) NOT NULL,
  `question` int(11) NOT NULL,
  `answer` smallint(1) NOT NULL DEFAULT '0',
  `question_option` varchar(500) NOT NULL,
  `serial` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_recruitment_position`
--

CREATE TABLE `tbl_recruitment_position` (
  `id` int(4) NOT NULL,
  `position` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1- Active, 0- Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_recruitment_position`
--

INSERT INTO `tbl_recruitment_position` (`id`, `position`, `status`) VALUES
(1, 'Casual Worker', 1),
(2, 'Casual Worker 2', 1),
(3, 'Casual Worker 3', 1),
(4, 'Casual Worker 4', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_recruitment_question_topic`
--

CREATE TABLE `tbl_recruitment_question_topic` (
  `id` int(11) NOT NULL,
  `topic` varchar(100) NOT NULL,
  `status` smallint(1) NOT NULL COMMENT '1- Active, 0- Inactive',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_recruitment_question_topic`
--

INSERT INTO `tbl_recruitment_question_topic` (`id`, `topic`, `status`, `created`) VALUES
(1, 'Fire Managment', 1, '2019-02-05 01:01:01'),
(2, 'Fire Managment 2', 1, '2019-02-05 01:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_recruitment_additional_questions`
--
ALTER TABLE `tbl_recruitment_additional_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_recruitment_additional_questions_answer`
--
ALTER TABLE `tbl_recruitment_additional_questions_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_recruitment_position`
--
ALTER TABLE `tbl_recruitment_position`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_recruitment_question_topic`
--
ALTER TABLE `tbl_recruitment_question_topic`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_recruitment_additional_questions`
--
ALTER TABLE `tbl_recruitment_additional_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_recruitment_additional_questions_answer`
--
ALTER TABLE `tbl_recruitment_additional_questions_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tbl_recruitment_position`
--
ALTER TABLE `tbl_recruitment_position`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_recruitment_question_topic`
--
ALTER TABLE `tbl_recruitment_question_topic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


 INSERT INTO `tbl_crm_department` (`id`, `name`, `hcmgr_id`, `archive`, `start_date`) VALUES
(1, 'Accommodation & Client Services', '00120812', 0, '2019-02-22 02:11:09'),
(2, 'Casual Staff Services', '00120813', 0, '2019-02-22 03:16:43'),
(3, 'People & Culture', '00120814', 0, '2019-02-22 17:11:34'),
(4, 'Business Systems', '00120815', 0, '2019-02-22 06:38:39'),
(5, 'Marketing', '00120816', 0, '2019-02-21 14:09:14'),
(6, 'Finance', '00120817', 0, '2019-02-21 04:17:12');
