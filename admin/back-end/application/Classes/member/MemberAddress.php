<?php

/*
 * Filename: MemberAddress.php
 * Desc: This file is used to hold information about the resedential details of members like street, city etc.
 * @author YDT <yourdevelopmentteam.com.au>
*/

if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * class: MemberAddress
 * Desc:  The accessbility mode is private for variables, and accessibility mode of members are public.
 * The class holds variables like $memberaddressid, $street, $city and setter and getter methods (getcity, setcity) are used to describe about address of member.
 * Created: 01-08-2018
 */
class MemberAddress
  {
    /**
	 * @var memberaddressid
     * @access private
     * @vartype: int
     */
    private $memberaddressid;

    /**
	 * @var memberid
     * @access private
     * @vartype: int
     */
    private $memberid;
	
    /**
	 * @var street
     * @access private
     * @vartype: int
     */
    private $street;

    /**
	 * @var city
     * @access private
     * @vartype: varchar
     */
    private $city;

    /**
	 * @var postal
     * @access private
     * @vartype: varchar
     */
    private $postal;

    /**
	 * @var state
     * @access private
     * @vartype: varchar
     */
    private $state;

    /**
	 * @var primary_address
     * @access private
     * @vartype: varchar
     */
    private $primary_address;
	
	
    /**
	 * @function getMemberAddressId
	 * @access public
	 * @return $memberaddressid integer
	 * Get $memberaddress Id
	 */
    public function getMemberAddressId() {
        return $this->memberaddressid;
    }

    /**
	 * @function setMemberAddressId
     * @param $memberaddressid integer 
     * @access public
	 * Set Member Address Id
     */
    public function setMemberAddressId($MemberAddressId) {
        $this->memberaddressid = $MemberAddressId;
    }

    /**
	 * @function getMemberid
	 * @access public
	 * @return $memberid integer
	 * Get Member Id
	 */
    public function getMemberid() {
        return $this->memberid;
    }

    /**
	 * @function setMemberid
     * @param $memberid integer 
     * @access public
	 * Set Member Id
     */
    public function setMemberid($memberid) {
        $this->memberid = $memberid;
    }

    /**
	 * @function getStreet
	 * @access public
	 * @return $street varchar
	 * Get Street
	 */
    public function getStreet() {
        return $this->street;
    }

    /**
	 * @function setStreet
     * @param $street varchar 
     * @access public
	 * Set Street
     */
    public function setStreet($street) {
        $this->street = $street;
    }

    /**
	 * @function getCity
	 * @access public
	 * @return $city varchar
	 * Get City
	 */
    public function getCity() {
        return $this->city;
    }

    /**
	 * @function setCity
     * @param $city varchar 
     * @access public
	 * Set City
     */
    public function setCity($city) {
        $this->city = $city;
    }

    /**
	 * @function getPostal
	 * @access public
	 * @return $postal varchar
	 * Get Postal
	 */
    public function getPostal() {
        return $this->postal;
    }

    /**
	 * @function setPostal
     * @param $postal varchar 
     * @access public
	 * Set Postal
     */
    public function setPostal($postal) {
        $this->postal = $postal;
    }

    /**
	 * @function getState
	 * @access public
	 * @return $state varchar
	 * Get State
	 */
    public function getState() {
        return $this->state;
    }

    /**
	 * @function setState
     * @param $state varchar 
     * @access public
	 * Set State
     */
    public function setState($state) {
        $this->state = $state;
    }

    /**
	 * @function getPrimaryAddress
	 * @access public
	 * @return $primary_address varchar
	 * Get Primary Aaddress
	 */
    public function getPrimaryAddress() {
        return $this->primary_address;
    }

    /**
	 * @function setPrimaryAddress
     * @param $primaryAddress varchar 
     * @access public
	 * Set Primary Address
     */
    public function setPrimaryAddress($primaryAddress) {
        $this->primary_address = $primaryAddress;
    }
	
}
