<?php

class Fms_model extends CI_Model {

    public function get_participant_name_d($post_data) {
        $this->db->or_where("(MATCH (firstname) AGAINST ('$post_data *'))", NULL, FALSE);
        $this->db->or_where("(MATCH (middlename) AGAINST ('$post_data *'))", NULL, FALSE);
        $this->db->or_where("(MATCH (lastname) AGAINST ('$post_data *'))", NULL, FALSE);
        $this->db->where('archive =', 0);
        $this->db->where('status=', 1);
        $this->db->select("CONCAT(firstname,' ',middlename,' ',lastname) as participantName");
        $this->db->select('firstname, id');
        $query = $this->db->get(TBL_PREFIX . 'participant');
        $query->result();
        $participant_rows = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $participant_rows[] = array('label' => $val->participantName, 'value' => $val->id);
            }
        }
        return $participant_rows;
    }

    public function get_member_name_d($post_data) {

        $this->db->or_where("(MATCH (firstname) AGAINST ('$post_data *'))", NULL, FALSE);
        $this->db->or_where("(MATCH (middlename) AGAINST ('$post_data *'))", NULL, FALSE);
        $this->db->or_where("(MATCH (lastname) AGAINST ('$post_data *'))", NULL, FALSE);
        $this->db->where('archive =', 0, FALSE);
        $this->db->where('status=', 1, FALSE);
        $this->db->select("CONCAT(firstname,' ',middlename,' ',lastname) as memberName");
        $this->db->select(array('id'));
        $query = $this->db->get(TBL_PREFIX . 'member');
        //last_query();
        $query->result();
        $participant_rows = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $participant_rows[] = array('label' => $val->memberName, 'value' => $val->id);
            }
        }
        return $participant_rows;
    }

    public function get_site_name_d($post_data) {
        $this->db->like('site_name', $post_data, 'both');
        $this->db->where('archive =', 0);
        $this->db->select('site_name, id');
        $query = $this->db->get(TBL_PREFIX . 'organisation_site');
        $query->result();
        $participant_rows = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $participant_rows[] = array('label' => $val->site_name, 'value' => $val->id);
            }
        }
        return $participant_rows;
    }

    public function create_case($reqData) {
        $responseAry = $reqData->data;
        #pr($responseAry); initiator_detail
        $case_ary = array('event_date' => $responseAry->event_date,
            'initiated_by' => isset($responseAry->initiator_detail->value) ? $responseAry->initiator_detail->value : '',
            'initiated_type' => $responseAry->initiatorCategory,
            'created' => DATE_TIME,
            'Initiator_first_name' => $responseAry->initiator_first_name,
            'Initiator_last_name' => $responseAry->initiator_last_name,
            'Initiator_email' => $responseAry->initiator_email,
            'Initiator_phone' => $responseAry->initiator_phone,
            'status' => 0,
        );

        $case_id = $this->Basic_model->insert_records('fms_case', $case_ary, $multiple = FALSE);
        $this->Basic_model->insert_records('fms_case_category', array('categoryId' => $responseAry->CaseCategory, 'caseId' => $case_id), $multiple = FALSE);

        $shift_note = array('title' => $responseAry->notes_title, 'caseId' => $case_id, 'description' => $responseAry->notes, 'created_by' => $reqData->adminId, 'created' => DATE_TIME);
        $shift_caller_id = $this->Basic_model->insert_records('fms_case_notes', $shift_note, $multiple = FALSE);

        $case_reason = array('caseId' => $case_id,
            'title' => $responseAry->title,
            'description' => $responseAry->description,
            'created_by' => $reqData->adminId,
            'created_type' => 2, //admin
            'created' => DATE_TIME,
        );

        $fms_case_reason_id = $this->Basic_model->insert_records('fms_case_reason', $case_reason, $multiple = FALSE);

        $location = $responseAry->completeAddress;
        if (!empty($location)) {
            foreach ($location as $key => $addr) {
                $case_location[] = array('caseId' => $case_id,
                    'address' => $addr->event_location,
                    'suburb' => $addr->suburb->value,
                    'postal' => $addr->suburb->postcode,
                    'state' => $addr->state,
                    'categoryId' => $addr->address_category,
                );
            }
            if (!empty($case_location))
                $this->Basic_model->insert_records('fms_case_location', $case_location, $multiple = TRUE);
        }
        #last_query();
        #pr($responseAry->againstDetail);die;
        if (!empty($responseAry->againstDetail)) {
            $case_againsts = array();
            foreach ($responseAry->againstDetail as $key => $val) {
                if (isset($val->againstCategory) && ($val->againstCategory == 1 || $val->againstCategory == 4)) {
                    $case_againsts[] = array('caseId' => $case_id, 'against_category' => '1', 'against_first_name' => $val->firstName, 'against_last_name' => $val->lastName, 'against_email' => isset($val->againstEmail) ? $val->againstEmail : '', 'against_phone' => isset($val->againstPhone) ? $val->againstPhone : '', 'created' => DATE_TIME, 'against_by' => '0'); //member of public
                } else if (isset($val->againstCategory) && $val->againstCategory == 3) {
                    $case_againsts[] = array('caseId' => $case_id, 'against_category' => $val->againstCategory, 'against_by' => $val->againstOnCallParticipant->value, 'created' => DATE_TIME, 'against_first_name' => null, 'against_last_name' => null, 'against_email' => null, 'against_phone' => null);
                } else if (isset($val->againstCategory) && $val->againstCategory == 2) {
                    $case_againsts[] = array('caseId' => $case_id, 'against_category' => $val->againstCategory, 'against_by' => $val->againstOnCallMember->value, 'created' => DATE_TIME, 'against_first_name' => null, 'against_last_name' => null, 'against_email' => null, 'against_phone' => null);
                } else if (isset($val->againstCategory) && $val->againstCategory == 5) {
                    $case_againsts[] = array('caseId' => $case_id, 'against_category' => $val->againstCategory, 'against_by' => $val->againstOnCallUserAdmin->value, 'created' => DATE_TIME, 'against_first_name' => null, 'against_last_name' => null, 'against_email' => null, 'against_phone' => null);
                }
            }
            #pr($case_againsts);
            if (!empty($case_againsts))
                $shift_requirement_id = $this->Basic_model->insert_records('fms_case_against_detail', $case_againsts, $multiple = TRUE);
        }

        if (!empty($case_id)) {
            return array('status' => true, 'case_id' => $case_id);
        } else {
            return array('status' => false);
        }
    }

    public function get_fms_cases($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';
        $tbl_fms = TBL_PREFIX . 'fms_case';

       

        #Search for a FMS based on: FMSID#, OCSID#, Name-Participant, Name-Member, Name-Org, Name-Sub-Org.
        

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_fms . '.id';
            $direction = 'DESC';
        }

        if (isset($filter->caseType))
            $this->db->where('tbl_fms_case.fms_type =', $filter->caseType);

        if (isset($filter->status))
            $this->db->where_in('tbl_fms_case.status', $filter->status);

        if (!empty($filter->srch_box)) {
            $this->db->group_start();
            $src_columns = array($tbl_fms . ".id as fmsid", $tbl_fms . ".Initiator_first_name", $tbl_fms . ".Initiator_last_name", $tbl_fms . ".initiated_by", "CONCAT(tbl_member.firstname,' ',tbl_member.middlename,' ',tbl_member.lastname)", "CONCAT(tbl_member.firstname,' ',tbl_member.lastname)", "CONCAT(tbl_participant.firstname,' ',tbl_participant.middlename,' ',tbl_participant.lastname)", "CONCAT(tbl_participant.firstname,' ',tbl_participant.lastname)", "tbl_member.id as member_ocs_id", "tbl_participant.id as participant_ocs_id");

            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $this->db->or_like($serch_column[0], $filter->srch_box);
                } else {
                    $this->db->or_like($column_search, $filter->srch_box);
                }
            }
            $this->db->group_end();
        }


        $select_column = array($tbl_fms . ".id", $tbl_fms . ".fms_type", "tbl_fms_case_reason.description", $tbl_fms . ".initiated_type", $tbl_fms . ".Initiator_first_name", $tbl_fms . ".Initiator_last_name", $tbl_fms . ".initiated_by", $tbl_fms . ".created", $tbl_fms . ".status", "tbl_fms_case_category.categoryId", "tbl_fms_case_all_category.name as case_category", "DATE_FORMAT(tbl_fms_case.event_date,'%d/%m/%Y') as event_date");

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_fms);
        $this->db->join('tbl_fms_case_reason', 'tbl_fms_case_reason.caseId = ' . $tbl_fms . '.id', 'left');
        $this->db->join('tbl_fms_case_category', 'tbl_fms_case_category.caseId = ' . $tbl_fms . '.id', 'left');
        $this->db->join('tbl_fms_case_all_category', 'tbl_fms_case_all_category.id = tbl_fms_case_category.categoryId', 'left');
        #LEFT JOIN `tbl_fms_case_all_category` ON `tbl_fms_case_all_category`.`id` = `tbl_fms_case_category`.`categoryId`

        if (!empty($filter->srch_box)) {
            $this->db->join('tbl_participant', 'tbl_participant.id =  tbl_fms_case.initiated_by AND tbl_fms_case.initiated_type = 2', 'left');
            $this->db->join('tbl_member', 'tbl_member.id =  tbl_fms_case.initiated_by AND tbl_fms_case.initiated_type = 1', 'left');
        }

        $this->db->group_by('tbl_fms_case.id');
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
//        last_query(); 
        $dt_filtered_total = $all_count = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                $val->created = $val->created;
                $val->category = $val->case_category;

                if ($val->initiated_type == 5 || $val->initiated_type == 6) {
                    $val->initiated_by = $val->Initiator_first_name . ' ' . $val->Initiator_last_name;
                } else {
                    $val->initiated_by = get_fms_initiated_by_name($val->initiated_type, $val->initiated_by);
                }
                $val->timelapse = time_ago_in_php($val->created);
                $val->against_for = get_fms_against_name($val->id);
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'all_count' => $all_count);
        return $return;
    }

    public function get_case_detail($reqData) {
        $tbl_fms = TBL_PREFIX . 'fms_case';
        $where = '';
        $where = $tbl_fms . ".id = " . $reqData->case_id;
        $sWhere = $where;
        $select_column = array($tbl_fms . ".id", $tbl_fms . ".initiated_type", $tbl_fms . ".Initiator_first_name", $tbl_fms . ".Initiator_last_name", $tbl_fms . ".initiated_by", $tbl_fms . ".status as caseStatus", $tbl_fms . ".created", $tbl_fms . ".fms_type");
        $this->db->select($select_column);
        $this->db->from($tbl_fms);
        $this->db->where($sWhere, null, false);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $val = $query->row();

        if (!empty($val)) {
            if ($val->initiated_type == 5 || $val->initiated_type == 6) {
                $val->initiated_by = $val->Initiator_first_name . ' ' . $val->Initiator_last_name;
            } else {
                $idid = $val->initiated_by;
                $val->initiated_by = get_fms_initiated_by_name($val->initiated_type, $idid);
                $val->ocs_id = $idid;
            }
            $val = isset($val) && !empty($val) ? $val : array();
            $val->categoryId = $this->Basic_model->get_row('fms_case_category', $columns = array('categoryId'), $id_array = array('caseId' => $reqData->case_id));
            $return = array('status' => TRUE, 'data' => $val);
        } else {
            $return = array('status' => FALSE, 'data' => array());
            return $return;
        }
        return $return;
    }

    public function get_link_fms_cases($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        $tbl_fms = TBL_PREFIX . 'fms_case';


        $src_columns = array();

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_fms . '.id';
            $direction = 'DESC';
        }

        $where = '';

        $where = $tbl_fms . ".id = " . $filter->caseId . " AND tbl_fms_case_link.archive = 0";
        #$where = $tbl_fms . ".id = ".$filter->caseId;
        $sWhere = $where;


        if (!empty($filter->search_box)) {
            $where = $where . " AND (";
            $sWhere = " (" . $where;
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $sWhere .= $serch_column[0] . " LIKE '%" . $this->db->escape_like_str($filter->search_box) . "%' OR ";
                } else {
                    $sWhere .= $column_search . " LIKE '%" . $this->db->escape_like_str($filter->search_box) . "%' OR ";
                }
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= '))';
        }

        $select_column = array($tbl_fms . ".id", $tbl_fms . ".event_date", "tbl_fms_case_reason.description", $tbl_fms . ".initiated_type", $tbl_fms . ".Initiator_first_name", $tbl_fms . ".Initiator_last_name", $tbl_fms . ".initiated_by", "tbl_fms_case_link.link_case as linkCaseId", $tbl_fms . ".created", "tbl_fms_case_category.categoryId", "tbl_fms_case_all_category.name as case_category", "DATE_FORMAT(tbl_fms_case.event_date,'%d/%m/%Y') as event_date");

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_fms);
        $this->db->join('tbl_fms_case_reason', 'tbl_fms_case_reason.caseId = ' . $tbl_fms . '.id', 'left');
        $this->db->join('tbl_fms_case_link', 'tbl_fms_case_link.caseId = ' . $tbl_fms . '.id', 'inner');
        $this->db->join('tbl_fms_case_category', 'tbl_fms_case_category.caseId = ' . $tbl_fms . '.id', 'left');
        $this->db->join('tbl_fms_case_all_category', 'tbl_fms_case_all_category.id = tbl_fms_case_category.categoryId', 'left');

        $this->db->group_by('tbl_fms_case_link.id');
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $this->db->where($sWhere, null, false);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #last_query(); die;
        $dt_filtered_total = $all_count = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                if ($val->initiated_type == 5) {
                    $val->initiated_by = $val->Initiator_first_name . ' ' . $val->Initiator_last_name;
                } else if ($val->initiated_type == 1) {
                    $member_id = $val->initiated_by;
                    $tbl = 'tbl_member';
                    $this->db->select("CONCAT($tbl.firstname,' ',$tbl.middlename, ' ', $tbl.lastname) AS name", FALSE);
                    $this->db->where('id', $member_id);
                    $emp_qry = $this->db->get($tbl);
                    $row = $emp_qry->row_array();
                    $val->initiated_by = $row['name'];
                }
                $val->timelapse = time_ago_in_php($val->created);
                $val->against_for = get_fms_against_name($val->linkCaseId);
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'all_count' => $all_count);
        return $return;
    }

    public function get_srch_fms_cases($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';
        $tbl_fms = TBL_PREFIX . 'fms_case';

        $src_columns = array($tbl_fms . ".id", $tbl_fms . ".event_date", "tbl_fms_case_reason.description", $tbl_fms . ".Initiator_first_name", $tbl_fms . ".Initiator_last_name", $tbl_fms . ".initiated_by", "CONCAT(tbl_member.firstname,' ',tbl_member.middlename,' ',tbl_member.lastname)", "CONCAT(tbl_member.firstname,' ',tbl_member.lastname)");

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_fms . '.id';
            $direction = 'DESC';
        }

        $where = '';

        $where = $tbl_fms . ".id != " . $filter->caseId . " AND tbl_fms_case.id NOT IN(select link_case from tbl_fms_case_link where caseId = $filter->caseId)";

        $sWhere = $where;

        if (!empty($filter->srch_box)) {
            $where = $where . " AND (";
            $sWhere = " (" . $where;
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $sWhere .= $serch_column[0] . " LIKE '%" . $this->db->escape_like_str($filter->srch_box) . "%' OR ";
                } else {
                    $sWhere .= $column_search . " LIKE '%" . $this->db->escape_like_str($filter->srch_box) . "%' OR ";
                }
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= '))';
        }

        if (!empty($filter->from_date)) {
            $this->db->where($tbl_fms . ".event_date >= '" . date('Y-m-d', strtotime($filter->from_date)) . "'");
        }
        if (!empty($filter->to_date)) {
            $this->db->where($tbl_fms . ".event_date <= '" . date('Y-m-d', strtotime($filter->to_date)) . "'");
        }

        $select_column = array($tbl_fms . ".id as linkCaseId", $tbl_fms . ".event_date", "tbl_fms_case_reason.description", $tbl_fms . ".initiated_type", $tbl_fms . ".Initiator_first_name", $tbl_fms . ".Initiator_last_name", $tbl_fms . ".initiated_by", $tbl_fms . ".created", "tbl_fms_case_category.categoryId", "tbl_fms_case_all_category.name as case_category", "DATE_FORMAT(tbl_fms_case.event_date,'%d/%m/%Y') as event_date");

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_fms);
        $this->db->join('tbl_fms_case_reason', 'tbl_fms_case_reason.caseId = ' . $tbl_fms . '.id', 'left');
        $this->db->join('tbl_member', 'tbl_member.id = ' . $tbl_fms . '.initiated_by', 'left');
        $this->db->join('tbl_fms_case_category', 'tbl_fms_case_category.caseId = ' . $tbl_fms . '.id', 'left');
        $this->db->join('tbl_fms_case_all_category', 'tbl_fms_case_all_category.id = tbl_fms_case_category.categoryId', 'left');
        $this->db->group_by('tbl_fms_case_reason.caseId');
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));
        $this->db->where($sWhere, null, false);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #last_query(); die;
        $dt_filtered_total = $all_count = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        if (!empty($dataResult)) {
            #pr($dataResult);
            foreach ($dataResult as $val) {
                if ($val->initiated_type == 5) {
                    $val->initiated_by = $val->Initiator_first_name . ' ' . $val->Initiator_last_name;
                } else if ($val->initiated_type == 1) {
                    $member_id = $val->initiated_by;
                    $tbl = 'tbl_member';
                    $this->db->select("CONCAT($tbl.firstname,' ',$tbl.middlename, ' ', $tbl.lastname) AS name", FALSE);
                    $this->db->where('id', $member_id);
                    $emp_qry = $this->db->get($tbl);
                    $row = $emp_qry->row_array();
                    $val->initiated_by = $row['name'];
                }
                $val->timelapse = time_ago_in_php($val->created);
                $val->against_for_srch = get_fms_against_name($val->linkCaseId);
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'all_count' => $all_count);
        return $return;
    }

    public function get_fms_log($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';
        $tbl_fms = TBL_PREFIX . 'fms_case_log';

        $src_columns = array();

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_fms . '.id';
            $direction = 'DESC';
        }

        $where = '';
        $where = $tbl_fms . ".caseId = " . $filter->caseId;
        $sWhere = $where;

        if (!empty($filter->on)) {
            $this->db->where(array('date(tbl_fms_case_log.created)' => date('Y-m-d', strtotime($filter->on))));
        } else {
            if (!empty($filter->from_date)) {
                $this->db->where(" date(tbl_fms_case_log.created) >= '" . date('Y-m-d', strtotime($filter->from_date)) . "'");
            }
            if (!empty($filter->to_date)) {
                $this->db->where(" date(tbl_fms_case_log.created) <= '" . date('Y-m-d', strtotime($filter->to_date)) . "'");
            }
        }

        $select_column = array($tbl_fms . ".title", $tbl_fms . ".created_by", $tbl_fms . ".created");

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_fms);
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        if (!empty($sWhere))
            $this->db->where($sWhere, null, false);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #last_query(); die;
        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                $val->created_by = 'By: ' . $val->created_by;
                $val->created = date('d/m/Y - h:i', strtotime($val->created));
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult);
        return $return;
    }

    public function search_address_book($reqData) {
        $searchbox = $reqData->data->searchbox;
        $except_record = isset($reqData->data->except_record) ? $reqData->data->except_record : '';
        $remove_record = array();
        $remove_member_record = array();
        $remove_participant_record = array();
        if (!empty($except_record)) {
            foreach ($except_record as $key => $value) {
                if ($value->type == 'member')
                    $remove_member_record[] = $value->id;
                else
                    $remove_participant_record[] = $value->id;
            }
        }
        $id_member = implode(',', array_unique($remove_member_record));
        $id_participant = implode(',', array_unique($remove_participant_record));

        if (empty($id_member))
            $id_member = 0;
        if (empty($id_participant))
            $id_participant = 0;

        $query = "select concat(firstname,' ', middlename,' ',lastname) as  label, 'member'  as type,tbl_member_phone.id,tbl_member_phone.phone from tbl_member left join tbl_member_phone on tbl_member.id = tbl_member_phone.memberId AND tbl_member_phone.primary_phone = 1
            WHERE archive = 0 AND tbl_member_phone.id NOT IN($id_member)  AND concat(firstname,' ', middlename,' ',lastname) LIKE '%" . $searchbox . "%'
                union 
            select concat(firstname,' ', middlename,' ',lastname) as  label, 'participant'  as type, tbl_participant.id,tbl_participant_phone.phone from tbl_participant left join tbl_participant_phone on tbl_participant.id = tbl_participant_phone.participantId AND tbl_participant_phone.primary_phone = 1
                WHERE archive = 0 AND tbl_participant_phone.id NOT IN($id_participant) AND concat(firstname,' ', middlename,' ',lastname) LIKE '%" . $searchbox . "%'";

        $query = $this->db->query($query);
        return $query->result();
    }

    public function get_contact_list($reqData) {
        $fms_address_book = TBL_PREFIX . 'fms_address_book';
        $this->db->select(array('id', 'type', 'ocs_id'));
        $this->db->from($fms_address_book);
        $this->db->where(array('caseId' => $reqData->data->case_id), null, false);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        $main_ary = array();
        $temp_ary = array();
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $val) {
                $temp_ary['id'] = $val->id;
                $temp_ary['ocs_id'] = $val->ocs_id;
                if ($val->type == '1') {
                    $sql = "SELECT concat(firstname,' ', middlename,' ',lastname) as name,tbl_member_phone.phone FROM tbl_member LEFT JOIN tbl_member_phone ON tbl_member.id = tbl_member_phone.memberId AND tbl_member_phone.primary_phone = 1 AND tbl_member.id= ? WHERE archive = 0";
                    $fetch_row = $this->db->query($sql, array($val->ocs_id));
                    $row = $fetch_row->row();
                    if (!empty($row)) {
                        $temp_ary['label'] = $row->name;
                        $temp_ary['phone'] = $row->phone;

                        $temp_ary['type'] = 'member';
                    }
                } else {
                    $sql = "SELECT concat(firstname,' ', middlename,' ',lastname) as name,tbl_participant_phone.phone FROM tbl_participant LEFT JOIN tbl_participant_phone on tbl_participant.id = tbl_participant_phone.participantId AND tbl_participant_phone.primary_phone = 1 AND tbl_participant.id= ? WHERE archive = 0 ";
                    $fetch_row = $this->db->query($sql, array($val->ocs_id));
                    $row = $fetch_row->row();
                    if (!empty($row)) {
                        $temp_ary['label'] = $row->name;
                        $temp_ary['phone'] = $row->phone;
                        $temp_ary['type'] = 'participant';
                    }
                }
                $main_ary[] = $temp_ary;
            }
        }

        return $main_ary;
    }

    public function save_contact($reqData) {
        $responseAry = $reqData->data;
        $case_ary = array('caseId' => $responseAry->case_id,
            'type' => isset($responseAry->record->type) && $responseAry->record->type == 'member' ? 1 : 2,
            'ocs_id' => $responseAry->record->id,
            'created' => DATE_TIME,
        );
        return $this->Basic_model->insert_records('fms_address_book', $case_ary);
    }

}
