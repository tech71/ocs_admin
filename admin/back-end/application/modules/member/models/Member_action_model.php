<?php
class Member_action_model extends CI_Model 
{
	 function get_testimonial() {
        $tbl_testimonial = TBL_PREFIX . 'testimonial';
        $tbl_member = TBL_PREFIX . 'member';

        $this->db->select(array($tbl_testimonial . '.title', $tbl_testimonial . '.testimonial', $tbl_testimonial . '.full_name'));
        $this->db->from($tbl_testimonial);
        $this->db->where(array($tbl_testimonial . '.module_type' => 1));
        $this->db->order_by('RAND()');
        $this->db->limit(1);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $x = $query->result();
        return $x;
    }
}


