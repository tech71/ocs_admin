<?php

require_once APPPATH . 'Classes/admin/permission.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_model extends CI_Model {

    public function member_list($reqData) {
        if (!empty($reqData)) {
            $reqData = json_decode($reqData->data->request);

            $limit = $reqData->pageSize;
            $page = $reqData->page;
            $sorted = $reqData->sorted;
            $filter = $reqData->filtered;
            $orderBy = '';
            $direction = '';

            $src_columns = array("tbl_member.id", "tbl_member.firstname", "tbl_member.lastname", "tbl_member_phone.phone", "tbl_member_email.email");

            if (!empty($sorted)) {
                if (!empty($sorted[0]->id)) {
                    $orderBy = $sorted[0]->id == 'id' ? 'tbl_member.id' : $sorted[0]->id;
                    $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
                }
            } else {
                $orderBy = 'tbl_member.id';
                $direction = 'DESC';
            }

            $where = '';
            $where = "tbl_member.archive = 0";
            $sWhere = $where;


            if (!empty($filter)) {
                if ($filter->include_inactive) {
                    $status_con = "0,1";
                    $this->db->where_in('tbl_member.status', $status_con, false);
                } else {
                    $status_con = 1;
                    $this->db->where_in('tbl_member.status', $status_con);
                }

                if (isset($filter->filterByType) && $filter->filterByType == 1) {
                    $this->db->join('tbl_shift_member', 'tbl_shift_member.memberId = tbl_member.id AND tbl_shift_member.status = 3', 'left');
                    $this->db->join('tbl_shift', 'tbl_shift.id = tbl_shift_member.shiftId', 'left');
                    $this->db->where_in('tbl_shift.status', array(1, 2, 3, 7));
                    $this->db->where('tbl_shift.shift_date >=', date("Y-m-d"));
                } else if (isset($filter->filterByType) && $filter->filterByType == 2) {
                    $this->db->where_not_in('tbl_member.id', "SELECT  tbl_member.id FROM tbl_member
						LEFT JOIN tbl_shift_member ON tbl_shift_member.memberId = tbl_member.id AND tbl_shift_member.status = 1
						LEFT JOIN tbl_shift ON tbl_shift.id = tbl_shift_member.shiftId AND tbl_shift.status IN(1, 2, 3, 7)
						WHERE tbl_member.status = 1 AND tbl_shift.shift_date = '" . date('Y-m-d') . "' AND tbl_member.archive = 0 GROUP BY tbl_member.id");
                } else if (isset($filter->filterByType) && $filter->filterByType == 3) {
                    $this->db->join('tbl_fms_case', 'tbl_fms_case.initiated_by = tbl_member.id AND tbl_fms_case.initiated_type = 1', 'right');
                    $this->db->where_in('tbl_fms_case.status', '0,1');
                } else if (isset($filter->filterByType) && $filter->filterByType == 4) {
                    $this->db->join('tbl_member_special_agreement', 'tbl_member_special_agreement.memberId = tbl_member.id AND tbl_member_special_agreement.archive = 0', 'right');
                } else if (isset($filter->filterByType) && $filter->filterByType == 5) {
                    $this->db->join('tbl_member_availability', "tbl_member_availability.memberId = tbl_member.id AND tbl_member_availability.archive = 0", 'right');
                } else if (isset($filter->filterByType) && $filter->filterByType == 6) {
                    $this->db->join('tbl_member_qualification', "tbl_member_qualification.memberId = tbl_member.id AND tbl_member_qualification.archive = 0 AND DATEDIFF(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(expiry)), '%Y-%m-%d'),CURDATE()) < 30 AND DATEDIFF(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(expiry)), '%Y-%m-%d'),CURDATE()) >=0", 'right');
                }

                $search_value = isset($filter->srch_box) ? $filter->srch_box : '';
                if (!empty($search_value)) {
                    $where = $where . " AND (";
                    $sWhere = " (" . $where;
                    for ($i = 0; $i < count($src_columns); $i++) {
                        $column_search = $src_columns[$i];
                        if (strstr($column_search, "as") !== false) {
                            $serch_column = explode(" as ", $column_search);
                            $sWhere .= $serch_column[0] . " LIKE '%" . $this->db->escape_like_str($search_value) . "%' OR ";
                        } else {
                            $sWhere .= $column_search . " LIKE '%" . $this->db->escape_like_str($search_value) . "%' OR ";
                        }
                    }
                    $sWhere = substr_replace($sWhere, "", -3);
                    $sWhere .= '))';
                }
            } else {
                $status_con = 1;
                $this->db->where_in('tbl_member.status', $status_con);
            }

            $select_column = array('tbl_member.id as OCS_id', 'tbl_member.firstname', 'tbl_member.lastname', 'tbl_member_address.street', 'tbl_member_address.city', 'tbl_member_address.postal', 'tbl_member_address.state', 'tbl_member_phone.phone', 'tbl_member.gender', 'tbl_state.name as state_name');

            $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
            $this->db->from(TBL_PREFIX . 'member');

            $this->db->join('tbl_member_address', 'tbl_member_address.memberId = tbl_member.id AND tbl_member_address.primary_address = 1', 'left');
            $this->db->join('tbl_member_phone', 'tbl_member_phone.memberId = tbl_member.id AND tbl_member_phone.primary_phone = 1', 'left');
            $this->db->join('tbl_member_email', 'tbl_member_email.memberId = tbl_member.id AND tbl_member_email.primary_email = 1', 'left');
            $this->db->join('tbl_state', 'tbl_state.id = tbl_member_address.state', 'left');

            $this->db->group_by('tbl_member.id');
            $this->db->order_by($orderBy, $direction);
            $this->db->limit($limit, ($page * $limit));

            $this->db->where($sWhere, null, false);
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            #last_query();die;
            $total_count = $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

            if ($dt_filtered_total % $limit == 0) {
                $dt_filtered_total = ($dt_filtered_total / $limit);
            } else {
                $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
            }

            $dataResult = array();
            if (!empty($query->result())) {
                foreach ($query->result() as $val) {
                    $row = array();
                    $row['ocs_id'] = $val->OCS_id;
                    $row['firstname'] = $val->firstname . ' ' . $val->lastname;

                    $street = isset($val->street) ? $val->street : '';
                    $city = isset($val->city) ? $val->city : '';
                    $postal = isset($val->postal) ? $val->postal : '';
                    $state = isset($val->state_name) ? $val->state_name : '';

                    $row['street'] = $street;
                    $row['city'] = $city;
                    $row['postal'] = $postal;
                    $row['state'] = $state;
                    $row['completeAddress'] = $street . ' ' . $city . ' ' . $postal . ' ' . $state;

                    $row['department'] = 'Department';
                    $row['phone'] = $val->phone;
                    $row['gender'] = isset($val->gender) && $val->gender == 1 ? 'Male' : 'Female';
                    ;
                    $dataResult[] = $row;
                }
            }

            $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'status' => true, 'total_count' => $total_count);
            return $return;
        }
    }

    public function get_member_about($objMember) {
        $memberId = $objMember->getMemberid();

        if (!empty(request_handler())) {
            //$request = request_handler();
            //$memberId = $request->data->member_id;

            $tbl_1 = TBL_PREFIX . 'member';
            $tbl_2 = TBL_PREFIX . 'member_address';
            $tbl_3 = TBL_PREFIX . 'member_phone';
            $tbl_4 = TBL_PREFIX . 'member_email';
            $tbl_5 = TBL_PREFIX . 'member_kin';
            $tbl_6 = TBL_PREFIX . 'member_phone';

            $this->db->select("CONCAT($tbl_1.firstname,' ',$tbl_1.middlename, ' ', $tbl_1.lastname) AS user_name", FALSE);

            $dt_query = $this->db->select(array($tbl_1 . '.enable_app_access', $tbl_1 . '.id as ocs_id', $tbl_1 . '.firstname', $tbl_1 . '.lastname', $tbl_1 . '.companyId', $tbl_1 . '.middlename', $tbl_1 . '.profile_image', $tbl_1 . '.prefer_contact', $tbl_1 . '.dob', $tbl_1 . '.gender', $tbl_1 . '.status'));

            $this->db->from($tbl_1);
            #$this->db->join($tbl_2, 'tbl_member_address.memberId = tbl_member.id AND tbl_member_address.primary_address = 1', 'left');
            #$this->db->join($tbl_4, 'tbl_member_email.memberId = tbl_member.id AND tbl_member_email.primary_email = 1', 'left');
            #$this->db->join('tbl_state', 'tbl_state.id = tbl_member_address.state', 'left');
            $sWhere = array($tbl_1 . '.id' => $memberId);
            $this->db->where($sWhere, null, false);
            $query = $this->db->get();
            $x = $query->row_array();

            $user_ary = array();
            $user_ary['basic_detail']['full_name'] = $x['user_name'];
            $user_ary['basic_detail']['ocs_id'] = $x['ocs_id'];
            $user_ary['basic_detail']['first_name'] = $x['firstname'];
            $user_ary['basic_detail']['companyId'] = $x['companyId'];
            $user_ary['basic_detail']['prefer_contact'] = !empty($x['prefer_contact']) ? $x['prefer_contact'] : '';
            $user_ary['basic_detail']['profile_image'] = $x['profile_image'];
            #$user_ary['basic_detail']['email'] = $x['email'];
            $user_ary['basic_detail']['enable_app_access'] = $x['enable_app_access'];
            $user_ary['basic_detail']['gender'] = !empty($x['gender']) && $x['gender'] == 1 ? 'Male' : 'Female';
            $user_ary['basic_detail']['status'] = $x['status'];
            $user_ary['basic_detail']['dob'] = !empty($x['dob']) && $x['dob'] != '0000-00-00' ? date('d/m/Y', strtotime($x['dob'])) : '';

            $query_em = $this->db->select(array($tbl_4 . ".email", $tbl_4 . ".primary_email", $tbl_4 . ".id"));
            $this->db->from($tbl_4);
            $this->db->where(array('memberId' => $memberId, 'archive' => 0));
            $query_mail = $this->db->get();
            $user_ary['basic_detail']['email_ary'] = $query_mail->result();

            /* Address ary */
            $dt_query = $this->db->select(array($tbl_2 . '.street', $tbl_2 . '.city', $tbl_2 . '.postal', $tbl_2 . '.state', $tbl_2 . '.primary_address', 'tbl_state.name as statename'));
            $this->db->join('tbl_state', 'tbl_state.id = tbl_member_address.state', 'left');
            $this->db->from($tbl_2);
            $sWhere = array($tbl_2 . '.memberId' => $memberId, $tbl_2 . '.archive' => 0);
            $this->db->where($sWhere);
            $this->db->order_by($tbl_2 . '.primary_address', 'ASC');
            $query = $this->db->get();
            $addr_rows = $query->result_array();
            if (!empty($addr_rows)) {
                $user_ary['basic_detail']['address_ary'] = $addr_rows;
            }

            /* Get keen */
            $dt_query = $this->db->select(array($tbl_5 . '.relation', $tbl_5 . '.phone', $tbl_5 . '.email', $tbl_5 . '.primary_kin'));
            $this->db->select("CONCAT($tbl_5.firstname, ' ', $tbl_5.lastname) AS name", FALSE);

            $this->db->from($tbl_5);
            $sWhere = array($tbl_5 . '.memberId' => $memberId, $tbl_5 . '.archive' => 0);
            $this->db->where($sWhere, null, false);
            $query = $this->db->get();
            $y = $query->result_array();
            if (!empty($y)) {
                $user_ary['basic_detail']['kin_ary'] = $y;
            }

            /* Get phone no */
            $dt_query = $this->db->select(array($tbl_6 . '.phone', $tbl_6 . '.primary_phone'));
            $this->db->from($tbl_6);
            $sWhere = array($tbl_6 . '.memberId' => $memberId, $tbl_6 . '.archive' => 0);
            $this->db->where($sWhere, null, false);
            $query = $this->db->get();
            $z = $query->result_array();
            if (!empty($z)) {
                $user_ary['basic_detail']['phone_no_ary'] = $z;
            }
            return $user_ary;
        }
    }

    public function get_member_profile($objMember) {
        $memberId = $objMember->getMemberid();
        if (!empty(request_handler())) {
            $user_ary = array();

            $tbl_1 = TBL_PREFIX . 'member';
            $tbl_2 = TBL_PREFIX . 'member_phone';
            $tbl_3 = TBL_PREFIX . 'member_email';
            $tbl_4 = TBL_PREFIX . 'member_address';
            $tbl_5 = TBL_PREFIX . 'member_kin';

            $this->db->select("CONCAT($tbl_1.firstname,' ',$tbl_1.middlename, ' ', $tbl_1.lastname) AS user_name", FALSE);
            $dt_query = $this->db->select(array($tbl_1 . '.enable_app_access', $tbl_1 . '.id as ocs_id', $tbl_1 . '.firstname', $tbl_1 . '.lastname', $tbl_1 . '.companyId', $tbl_1 . '.middlename', $tbl_1 . '.profile_image', $tbl_1 . '.prefer_contact', $tbl_1 . '.dob', $tbl_1 . '.gender', $tbl_1 . '.status', $tbl_1 . '.dwes_confirm', $tbl_2 . '.phone'));

            $this->db->from($tbl_1);
            $this->db->join($tbl_2, 'tbl_member.id = tbl_member_phone.memberId', 'left');
            $sWhere = array($tbl_1 . '.id' => $memberId, $tbl_1 . '.archive' => 0);
            $this->db->where($sWhere, null, false);
            $query = $this->db->get();
            #last_query();
            $x = $query->row_array();

            if (!empty($x)) {
                $query_ph = $this->db->select(array($tbl_2 . ".phone", $tbl_2 . ".primary_phone", $tbl_2 . ".id"));
                $this->db->from($tbl_2);
                $this->db->where(array('memberId' => $memberId, 'archive' => 0));
                $query = $this->db->get();
                #last_query();	
                $user_ary['basic_detail']['phone_ary'] = $query->result();

                $query_em = $this->db->select(array($tbl_3 . ".email", $tbl_3 . ".primary_email", $tbl_3 . ".id"));
                $this->db->from($tbl_3);
                $this->db->where(array('memberId' => $memberId, 'archive' => 0));
                $query = $this->db->get();
                $user_ary['basic_detail']['email_ary'] = $query->result();

                $query_adr = $this->db->select(array($tbl_4 . ".street", $tbl_4 . ".city", $tbl_4 . ".id", $tbl_4 . ".postal", $tbl_4 . ".state", $tbl_4 . ".primary_address", 'tbl_state.name as state_name'));
                $this->db->from($tbl_4);
                $this->db->join('tbl_state', 'tbl_state.id = tbl_member_address.state', 'left');
                $this->db->where(array('memberId' => $memberId, $tbl_4 . '.archive' => 0));
                $query = $this->db->get();
                $addr = $query->result();
                $completeAddress = array();
                if (!empty($addr)) {
                    foreach ($addr as $key => $value) {
                        $temp_ary['street'] = $value->street;
                        $temp_ary['id'] = $value->id;
                        #$temp_ary['city'] = $value->city;
                        $temp_ary['postal'] = $value->postal;
                        $temp_ary['statename'] = $value->state_name;
                        $temp_ary['primary_address'] = $value->primary_address;
                        $temp_ary['state'] = $value->state;
                        $temp_ary['city'] = array('value' => $value->city, 'label' => $value->city);
                        $completeAddress[] = $temp_ary;
                    }
                }

                $query_kin = $this->db->select(array($tbl_5 . ".firstname", $tbl_5 . ".lastname", $tbl_5 . ".id", $tbl_5 . ".relation", $tbl_5 . ".phone", $tbl_5 . ".email", $tbl_5 . ".primary_kin"));
                $this->db->select("CONCAT($tbl_5.firstname, ' ', $tbl_5.lastname) AS name", FALSE);
                $this->db->from($tbl_5);
                $this->db->where(array('memberId' => $memberId, 'archive' => 0));
                $kin_query = $this->db->get();

                $user_ary['basic_detail']['kin_ary'] = $kin_query->result();
                $user_ary['basic_detail']['completeAddress'] = $completeAddress;
                $user_ary['basic_detail']['full_name'] = $x['user_name'];
                $user_ary['basic_detail']['prefer_contact'] = $x['prefer_contact'];
                $user_ary['basic_detail']['ocs_id'] = $x['ocs_id'];
                $user_ary['basic_detail']['first_name'] = $x['firstname'];
                $user_ary['basic_detail']['companyId'] = $x['companyId'];
                $user_ary['basic_detail']['profile_image'] = $x['profile_image'];
                $user_ary['basic_detail']['enable_app_access'] = $x['enable_app_access'];
                $user_ary['basic_detail']['gender'] = $x['gender'];
                $user_ary['basic_detail']['status'] = $x['status'];
                $user_ary['basic_detail']['dwes_confirm'] = $x['dwes_confirm'];
                $user_ary['basic_detail']['dob'] = !empty($x['dob']) && $x['dob'] != '0000-00-00' ? date('d/m/Y', strtotime($x['dob'])) : '';
                return array('status' => true, 'data' => $user_ary);
            } else {
                return array('status' => false);
            }
        }
    }

    public function member_qualification($objMember) {
        if (!empty(request_handler())) {

            $memberId = $objMember->getMemberid();
            $view_by = $objMember->getArchive();

            $start_date = $objMember->getStart_date();
            $end_date = $objMember->getEnd_date();

            if (!empty($start_date)) {
                $this->db->where('mq.expiry >=', DateFormate($start_date, 'Y-m-d'));
            }if (!empty($end_date)) {
                $this->db->where('mq.expiry <=', DateFormate($end_date, 'Y-m-d'));
            }


            $this->db->where('archive', $view_by);



            $dt_query = $this->db->select(array('mq.id', 'mq.expiry', 'mq.title', 'mq.created', 'mq.can_delete', 'mq.filename', "DATEDIFF(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(expiry)), '%Y-%m-%d'),CURDATE()) AS days_left", 'mq.type',));
            $sWhere = array('mq.memberId' => $memberId);
            $this->db->from('tbl_member_qualification as mq');


            $this->db->where($sWhere, null, false);
            $query = $this->db->get();

            $x = $query->result_array();
            $data_ary = array();
            if (!empty($x)) {
                foreach ($x as $key => $value) {
                    $temp_ary['id'] = $value['id'];
                    $temp_ary['type'] = $value['type'];
                    $temp_ary['days_left'] = $value['days_left'];
                    $temp_ary['expiry'] = date('d-m-Y', strtotime($value['expiry']));
                    $temp_ary['title'] = $value['title'];
                    $temp_ary['created'] = $value['created'];
                    $temp_ary['can_delete'] = $value['can_delete'];
                    $temp_ary['filename'] = $value['filename'];
                    if ($value['days_left'] < 0 || $value['days_left'] < 5)
                        $class_name = 'bg_dark_Pink';
                    else if ($value['days_left'] > 0 && $value['days_left'] < 30)
                        $class_name = 'bg_dark_yello';
                    else
                        $class_name = '';

                    $temp_ary['class_name'] = $class_name;
                    $data_ary[] = $temp_ary;
                }
            }

            if (!empty($data_ary))
                return $data_ary;
            else
                return array();
        }
    }

    public function get_member_preference($objMember) {
        $memberId = $objMember->getMemberid();
        $tbl_12 = TBL_PREFIX . 'place';
        $tbl_11 = TBL_PREFIX . 'member_place';

        $tbl_10 = TBL_PREFIX . 'member_activity';
        $tbl_13 = TBL_PREFIX . 'activity';

        $arr = array('placeFav' => '', 'placeLeastFav' => '', 'activityFav' => '', 'activityLeastFav' => '');
        // get places participant
        $dt_query = $this->db->select(array($tbl_12 . ".name as label", $tbl_12 . ".id", $tbl_11 . ".type"));
        $this->db->from($tbl_12);
        $this->db->join($tbl_11, $tbl_12 . '.id = ' . $tbl_11 . '.placeId AND memberId = ' . $memberId, 'left');
        $query = $this->db->get();
        $arr['places'] = $query->result();

        if (!empty($arr['places'])) {
            $fav = $leastFav = [];
            foreach ($arr['places'] as $val) {
                if ($val->type == 1) {
                    $fav[] = $val->label;
                } elseif ($val->type == 2) {
                    $leastFav[] = $val->label;
                }
                $val->type = ($val->type) ? $val->type : 'none';
            }

            $arr['placeFav'] = implode(', ', $fav);
            $arr['placeLeastFav'] = implode(', ', $leastFav);
        }

        // get activity participant
        $dt_query = $this->db->select(array($tbl_13 . ".name as label", $tbl_13 . ".id", $tbl_10 . ".type"));
        $this->db->from($tbl_13);
        $this->db->join($tbl_10, $tbl_13 . '.id = ' . $tbl_10 . '.activityId AND memberId = ' . $memberId, 'left');
        $query = $this->db->get();
        $arr['activity'] = $query->result();

        if (!empty($arr['activity'])) {
            $fav = $leastFav = [];
            foreach ($arr['activity'] as $val) {
                if ($val->type == 1) {
                    $fav[] = $val->label;
                } elseif ($val->type == 2) {
                    $leastFav[] = $val->label;
                }
                $val->type = ($val->type) ? $val->type : 'none';
            }
            $arr['activityFav'] = implode(', ', $fav);
            $arr['activityLeastFav'] = implode(', ', $leastFav);
        }
        return $arr;
    }

    public function member_shift_count($reqData) {
        $member_id = $reqData->data->member_id;

        $tbl_1 = TBL_PREFIX . 'shift';
        $tbl_2 = TBL_PREFIX . 'shift_member';

        if (!empty($reqData->data->from_date))
            $this->db->where($tbl_1 . ".shift_date >= '" . date('Y-m-d', strtotime($reqData->data->from_date)) . "'");

        if (!empty($reqData->data->to_date))
            $this->db->where($tbl_1 . ".shift_date <= '" . date('Y-m-d', strtotime($reqData->data->to_date)) . "'");


        /* Rejected Quator */
        $this->db->select("count($tbl_1.id) as rejected_count_Q");
        $this->db->from($tbl_1);
        $this->db->join($tbl_2, 'tbl_shift.id = tbl_shift_member.shiftId', 'left');
        $this->db->where('tbl_shift.created > DATE_SUB(NOW(), INTERVAL 90 DAY)');
        $sWhere = array($tbl_2 . '.memberId' => $member_id, $tbl_2 . '.status' => 2);
        if (!empty($reqData->data->from_date))
            $this->db->where($tbl_1 . ".shift_date >= '" . date('Y-m-d', strtotime($reqData->data->from_date)) . "'");

        if (!empty($reqData->data->to_date))
            $this->db->where($tbl_1 . ".shift_date <= '" . date('Y-m-d', strtotime($reqData->data->to_date)) . "'");

        $this->db->where($sWhere, null, false);
        $query = $this->db->get();

        $count = $query->row();
        $rejected_count_Q = $count->rejected_count_Q;

        /* Cancelled Quator */
        $this->db->select("count($tbl_1.id) as cancelled_count_Q");
        $this->db->from($tbl_1);
        $this->db->join($tbl_2, 'tbl_shift.id = tbl_shift_member.shiftId', 'left');
        $this->db->where('tbl_shift.created > DATE_SUB(NOW(), INTERVAL 90 DAY)');
        $sWhere = array($tbl_2 . '.memberId' => $member_id, $tbl_2 . '.status' => 4);
        $this->db->where($sWhere, null, false);
        if (!empty($reqData->data->from_date))
            $this->db->where($tbl_1 . ".shift_date >= '" . date('Y-m-d', strtotime($reqData->data->from_date)) . "'");

        if (!empty($reqData->data->to_date))
            $this->db->where($tbl_1 . ".shift_date <= '" . date('Y-m-d', strtotime($reqData->data->to_date)) . "'");
        $query = $this->db->get();

        $count = $query->row();
        $cancelled_count_Q = $count->cancelled_count_Q;

        /* Filled Quator */
        $this->db->select("count($tbl_1.id) as filled_count_Q");
        $this->db->from($tbl_1);
        $this->db->join($tbl_2, 'tbl_shift.id = tbl_shift_member.shiftId', 'left');
        $this->db->where('tbl_shift.created > DATE_SUB(NOW(), INTERVAL 90 DAY)');
        $sWhere = array($tbl_2 . '.memberId' => $member_id, $tbl_1 . '.status' => 7, $tbl_2 . '.status' => 3);
        $this->db->where($sWhere, null, false);
        if (!empty($reqData->data->from_date))
            $this->db->where($tbl_1 . ".shift_date >= '" . date('Y-m-d', strtotime($reqData->data->from_date)) . "'");

        if (!empty($reqData->data->to_date))
            $this->db->where($tbl_1 . ".shift_date <= '" . date('Y-m-d', strtotime($reqData->data->to_date)) . "'");
        $query = $this->db->get();
        $count = $query->row();
        $filled_count_Q = $count->filled_count_Q;

        /* Rejected Week */
        $this->db->select("count($tbl_1.id) as rejected_count_W");
        $this->db->from($tbl_1);
        $this->db->join($tbl_2, 'tbl_shift.id = tbl_shift_member.shiftId', 'left');
        $this->db->where('tbl_shift.created > DATE_SUB(NOW(), INTERVAL 7 DAY)');
        $sWhere = array($tbl_2 . '.memberId' => $member_id, $tbl_2 . '.status' => 2);
        $this->db->where($sWhere, null, false);
        if (!empty($reqData->data->from_date))
            $this->db->where($tbl_1 . ".shift_date >= '" . date('Y-m-d', strtotime($reqData->data->from_date)) . "'");

        if (!empty($reqData->data->to_date))
            $this->db->where($tbl_1 . ".shift_date <= '" . date('Y-m-d', strtotime($reqData->data->to_date)) . "'");
        $query = $this->db->get();
        $count = $query->row();
        $rejected_count_W = $count->rejected_count_W;

        /* Cancelled Week */
        $this->db->select("count($tbl_1.id) as cancelled_count_W");
        $this->db->from($tbl_1);
        $this->db->join($tbl_2, 'tbl_shift.id = tbl_shift_member.shiftId', 'left');
        $this->db->where('tbl_shift.created > DATE_SUB(NOW(), INTERVAL 7 DAY)');
        $sWhere = array($tbl_2 . '.memberId' => $member_id, $tbl_2 . '.status' => 4);
        $this->db->where($sWhere, null, false);
        if (!empty($reqData->data->from_date))
            $this->db->where($tbl_1 . ".shift_date >= '" . date('Y-m-d', strtotime($reqData->data->from_date)) . "'");

        if (!empty($reqData->data->to_date))
            $this->db->where($tbl_1 . ".shift_date <= '" . date('Y-m-d', strtotime($reqData->data->to_date)) . "'");
        $query = $this->db->get();
        $count = $query->row();
        $cancelled_count_W = $count->cancelled_count_W;

        /* Filled Week */
        $this->db->select("count($tbl_1.id) as filled_count_W");
        $this->db->from($tbl_1);
        $this->db->join($tbl_2, 'tbl_shift.id = tbl_shift_member.shiftId', 'left');
        $this->db->where('tbl_shift.created > DATE_SUB(NOW(), INTERVAL 7 DAY)');
        $sWhere = array($tbl_2 . '.memberId' => $member_id, $tbl_1 . '.status' => 7, $tbl_2 . '.status' => 3);
        $this->db->where($sWhere, null, false);
        if (!empty($reqData->data->from_date))
            $this->db->where($tbl_1 . ".shift_date >= '" . date('Y-m-d', strtotime($reqData->data->from_date)) . "'");

        if (!empty($reqData->data->to_date))
            $this->db->where($tbl_1 . ".shift_date <= '" . date('Y-m-d', strtotime($reqData->data->to_date)) . "'");
        $query = $this->db->get();
        $count = $query->row();
        $filled_count_W = $count->filled_count_W;

        /* Rejected Year */
        $this->db->select("count($tbl_1.id) as rejected_count_Y");
        $this->db->from($tbl_1);
        $this->db->join($tbl_2, 'tbl_shift.id = tbl_shift_member.shiftId', 'left');
        $this->db->where('YEAR(tbl_shift.created)=YEAR(CURDATE())');
        $sWhere = array($tbl_2 . '.memberId' => $member_id, $tbl_2 . '.status' => 2);
        $this->db->where($sWhere, null, false);
        if (!empty($reqData->data->from_date))
            $this->db->where($tbl_1 . ".shift_date >= '" . date('Y-m-d', strtotime($reqData->data->from_date)) . "'");

        if (!empty($reqData->data->to_date))
            $this->db->where($tbl_1 . ".shift_date <= '" . date('Y-m-d', strtotime($reqData->data->to_date)) . "'");
        $query = $this->db->get();
        $count = $query->row();
        $rejected_count_Y = $count->rejected_count_Y;

        /* Cancelled Year */
        $this->db->select("count($tbl_1.id) as cancelled_count_Y");
        $this->db->from($tbl_1);
        $this->db->join($tbl_2, 'tbl_shift.id = tbl_shift_member.shiftId', 'left');
        $this->db->where('YEAR(tbl_shift.created)=YEAR(CURDATE())');
        $this->db->where('tbl_shift.created > DATE_SUB(NOW(), INTERVAL 90 DAY)');
        $sWhere = array($tbl_2 . '.memberId' => $member_id, $tbl_2 . '.status' => 4);
        $this->db->where($sWhere, null, false);
        if (!empty($reqData->data->from_date))
            $this->db->where($tbl_1 . ".shift_date >= '" . date('Y-m-d', strtotime($reqData->data->from_date)) . "'");

        if (!empty($reqData->data->to_date))
            $this->db->where($tbl_1 . ".shift_date <= '" . date('Y-m-d', strtotime($reqData->data->to_date)) . "'");
        $query = $this->db->get();
        $count = $query->row();
        $cancelled_count_Y = $count->cancelled_count_Y;

        /* Filled Year */
        $this->db->select("count($tbl_1.id) as filled_count_Y");
        $this->db->from($tbl_1);
        $this->db->join($tbl_2, 'tbl_shift.id = tbl_shift_member.shiftId', 'left');
        $this->db->where('YEAR(tbl_shift.created)=YEAR(CURDATE())');
        $this->db->where('tbl_shift.created > DATE_SUB(NOW(), INTERVAL 90 DAY)');
        $sWhere = array($tbl_2 . '.memberId' => $member_id, $tbl_1 . '.status' => 7, $tbl_2 . '.status' => 3);
        $this->db->where($sWhere, null, false);
        if (!empty($reqData->data->from_date))
            $this->db->where($tbl_1 . ".shift_date >= '" . date('Y-m-d', strtotime($reqData->data->from_date)) . "'");

        if (!empty($reqData->data->to_date))
            $this->db->where($tbl_1 . ".shift_date <= '" . date('Y-m-d', strtotime($reqData->data->to_date)) . "'");
        $query = $this->db->get();

        $count = $query->row();
        $filled_count_Y = $count->filled_count_Y;

        echo json_encode(array(
            'filled_count_Q' => $filled_count_Q,
            'cancelled_count_Q' => $cancelled_count_Q,
            'rejected_count_Q' => $rejected_count_Q,
            'filled_count_W' => $filled_count_W,
            'cancelled_count_W' => $cancelled_count_W,
            'rejected_count_W' => $rejected_count_W,
            'filled_count_Y' => $filled_count_Y,
            'cancelled_count_Y' => $cancelled_count_Y,
            'rejected_count_Y' => $rejected_count_Y,
        ));
        /* echo json_encode(array(
          'filled_count_Q'=>54354,
          'cancelled_count_Q'=>55434,
          'rejected_count_Q'=>55654,

          'filled_count_W'=>012,
          'cancelled_count_W'=>2,
          'rejected_count_W'=>1,

          'filled_count_Y'=>1,
          'cancelled_count_Y'=>0,
          'rejected_count_Y'=>111,
          )); */
    }

    public function get_member_shifts($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $member_id = isset($reqData->member_id) ? $reqData->member_id : '';

        $orderBy = '';
        $direction = '';

        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_member = TBL_PREFIX . 'shift_member';
        $tbl_member = TBL_PREFIX . 'member';

        $src_columns = array($tbl_shift . ".id", $tbl_member . '.firstname', $tbl_member . '.middlename', $tbl_member . '.lastname', "CONCAT(" . $tbl_member . ".firstname,' '," . $tbl_member . ".middlename,' '," . $tbl_member . ".lastname)", "CONCAT(" . $tbl_member . ".firstname,' '," . $tbl_member . ".lastname)");

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_shift . '.id';
            $direction = 'ASC';
        }

        $shift_status = $filter->shift_status;
        if ($shift_status == 6) {
            $member_shift_status = 3;
        } else if ($shift_status == 5) {
            $member_shift_status = 4;
        }

        $where = '';
        $where = $tbl_shift . ".status = " . $shift_status . "  AND " . $tbl_shift_member . ".memberId = " . $member_id;
        $sWhere = $where;


        if (!empty($filter->search_box)) {
            $where = $where . " AND (";
            $sWhere = " (" . $where;
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $sWhere .= $serch_column[0] . " LIKE '%" . $this->db->escape_like_str($filter->search_box) . "%' OR ";
                } else {
                    $sWhere .= $column_search . " LIKE '%" . $this->db->escape_like_str($filter->search_box) . "%' OR ";
                }
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= '))';
        }

        if (!empty($filter->start_date)) {
            $this->db->where($tbl_shift . ".shift_date >= '" . date('Y-m-d', strtotime($filter->start_date)) . "'");
        }
        if (!empty($filter->end_date)) {
            $this->db->where($tbl_shift . ".shift_date <= '" . date('Y-m-d', strtotime($filter->end_date)) . "'");
        }

        $select_column = array($tbl_shift . ".id", $tbl_shift . ".shift_date", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_shift . ".expenses", $tbl_shift . ".status");

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_shift);

        $this->db->select("CONCAT(" . $tbl_member . ".firstname,' '," . $tbl_member . ".middlename,' '," . $tbl_member . ".lastname ) as memberName");
        $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");

        $this->db->join($tbl_shift_member, $tbl_shift_member . '.shiftId = ' . $tbl_shift . '.id AND ' . $tbl_shift_member . '.status = ' . $member_shift_status, 'left');
        $this->db->join($tbl_member, $tbl_member . '.id = ' . $tbl_shift_member . '.memberId', 'LEFT');

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $this->db->where($sWhere, null, false);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #last_query();
        $dt_filtered_total = $all_count = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();
        $total_duration = 0.00;
        if (!empty($dataResult)) {
            $time = array();
            foreach ($dataResult as $val) {
                //$val->memberNames = $this->get_preferred_member($val->id);
                //$val->address = $this->get_shift_location($val->id);

                $val->diff = (strtotime(date('Y-m-d h:i:sa', strtotime($val->start_time))) - strtotime(date('Y-m-d h:i:sa'))) * 1000;

                $time[] = str_replace('hrs', '', $val->duration);
                $val->shift_date = date('d/m/Y', strtotime($val->shift_date));
                $val->start_time = date('h:ia', strtotime($val->start_time));
                $val->end_time = date('h:ia', strtotime($val->end_time));
                $val->duration = $val->duration;
                $val->download_status = FALSE;
                $val->status = isset($val->status) && $val->status == 6 ? 'Complete' : ($val->status == 5 ? 'Cancelled' : 'Upcoming Shifts');
            }
            $total_duration = add_hour_minute($time);
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'total_duration' => $total_duration, 'all_count' => $all_count);
        return $return;
    }

    public function get_member_upcoming_shifts($reqData) {
        $data = json_decode($reqData, true);
        $member_id = $data['member_id'];
        $default_date = $data['default_date'];
        $month = date('m', strtotime($default_date));
        $year = date('Y', strtotime($default_date));

        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_member = TBL_PREFIX . 'shift_member';

        $where = '';
        $where = $tbl_shift . ".status IN (2,7) AND " . $tbl_shift_member . ".memberId = " . $member_id . " AND MONTH($tbl_shift.shift_date) = " . $month . " AND YEAR(shift_date) = " . $year;
        //$where = $tbl_shift . ".status IN (2,7) AND MONTH($tbl_shift.shift_date) = ".$month." AND YEAR(shift_date) = ".$year;
        $sWhere = $where;

        $select_column = array($tbl_shift . ".id", $tbl_shift . ".shift_date", $tbl_shift . ".status");
        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_shift);
        $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");

        #$this->db->join($tbl_shift_member, $tbl_shift_member . '.shiftId = ' . $tbl_shift . '.id AND ' . $tbl_shift_member . '.status = 1', 'left');
        $this->db->join($tbl_shift_member, $tbl_shift_member . '.shiftId = ' . $tbl_shift . '.id AND ' . $tbl_shift_member . '.status IN (1,3)', 'left');


        $this->db->where($sWhere, null, false);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #last_query();
        $dataResult = $query->result();
        $total_duration = 0.00;
        if (!empty($dataResult)) {
            $time = array();
            foreach ($dataResult as $val) {
                $time[] = str_replace('hrs', '', $val->duration);
                $val->title = '';
                $val->description = '';
                $val->start = $val->shift_date;
                $val->end = $val->shift_date;
                $val->status = $val->status;
                //$val->start_time = date('h:ia', strtotime($val->start_time));
                //$val->end_time = date('h:ia', strtotime($val->end_time));                
            }
            $total_duration = add_hour_minute($time);
        }
        $return = array('data' => $dataResult, 'status' => true, 'total_duration' => $total_duration);
        return $return;
    }

    public function get_previous_availiability($reqData) {
        $day_difference = 28;
        $member_id = $reqData->member_id;
        $start_date = date('Y-m-d', strtotime($reqData->start_date));

        if (isset($reqData->end_date)) {
            $end_date = date('Y-m-d', strtotime($reqData->end_date));
        } else {
            $end_date = date('Y-m-d', strtotime($start_date . " +$day_difference days"));
        }

        $tbl = TBL_PREFIX . 'member_availability_list';
        $this->db->from($tbl);
        $this->db->select("availability_type,availability_date,id");
        $where_ary = array('memberId' => $member_id);
        $this->db->where('availability_date >= ', $start_date);
        $this->db->where('availability_date <= ', $end_date);
        $this->db->where($where_ary, null, false);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        //echo $this->db->last_query();
        return $dataResult = $query->result_array();
    }

    public function check_availibility_already_exist($rosObj) {
        $start_date = $rosObj->start_date;
        $end_date = $rosObj->end_date;
        if (!empty($end_date)) {
            $this->db->select(array("tbl_member_availability.id"));
            $this->db->from('tbl_member_availability');

            $where = "is_default = 2 AND memberId = " . $rosObj->member_id . " AND 
			(('" . $start_date . "' BETWEEN date(start_date) AND date(end_date))  or ('" . $end_date . "' BETWEEN date(start_date) AND date(end_date)) OR
			(date(start_date) BETWEEN '" . $start_date . "' AND '" . $end_date . "')  or (date(end_date) BETWEEN '" . $start_date . "' AND '" . $end_date . "')) ";
            $this->db->where($where);
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            $x = $query->row();
            return (!empty($x)) ? $x : array();
        } else {
            return array();
        }
    }

    public function get_shift_for_availibility($rosObj) {
        $member_id = $rosObj['member_id'];
        $shift_date = $rosObj['shift_date'];

        $this->db->select(array("tbl_shift.id"));
        $this->db->from('tbl_shift');
        $this->db->join('tbl_shift_member', 'tbl_shift_member.shiftId = tbl_shift.id', 'left');
        $where = "tbl_shift.shift_date = '" . $shift_date . "' AND tbl_shift_member.memberId = " . $member_id . " AND tbl_shift_member.status = '3' AND tbl_shift.status = '7'";
        $this->db->where($where);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #last_query();
        return $query->row();
    }

    public function get_member_fms($reqData, $adminId) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';
        $tbl_fms = TBL_PREFIX . 'fms_case';

        $obj_permission = new classPermission\Permission();
        $incident_fms = $obj_permission->check_permission($adminId, 'incident_fms');

        if (!$incident_fms) {
            $this->db->where('fms_type', '0');
        }

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_fms . '.id';
            $direction = 'DESC';
        }

        $where = '';

        $where = '((' . $tbl_fms . ".initiated_by = " . $reqData->memberId . " AND " . $tbl_fms . ".initiated_type = 1) OR (tbl_fms_case_against_detail.against_by = " . $reqData->memberId . ")) ";
        $sWhere = $where;

        if (!empty($filter->srch_box)) {

            $src_columns = array($tbl_fms . ".id as fmsid", $tbl_fms . ".Initiator_first_name", $tbl_fms . ".Initiator_last_name", $tbl_fms . ".initiated_by", "CONCAT(tbl_member.firstname,' ',tbl_member.middlename,' ',tbl_member.lastname)", "CONCAT(tbl_member.firstname,' ',tbl_member.lastname)", "CONCAT(tbl_participant.firstname,' ',tbl_participant.middlename,' ',tbl_participant.lastname)", "CONCAT(tbl_participant.firstname,' ',tbl_participant.lastname)", "tbl_member.id as member_ocs_id", "tbl_participant.id as participant_ocs_id");

            $where = $where . " AND (";
            $sWhere = " (" . $where;
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $sWhere .= $serch_column[0] . " LIKE '%" . $this->db->escape_like_str($filter->srch_box) . "%' OR ";
                } else {
                    $sWhere .= $column_search . " LIKE '%" . $this->db->escape_like_str($filter->srch_box) . "%' OR ";
                }
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= '))';
        }

        $select_column = array($tbl_fms . ".id", $tbl_fms . ".event_date", "tbl_fms_case_reason.description", $tbl_fms . ".initiated_type", $tbl_fms . ".Initiator_first_name", $tbl_fms . ".Initiator_last_name", $tbl_fms . ".initiated_by", $tbl_fms . ".created", "tbl_fms_case_category.categoryId", "tbl_fms_case_all_category.name as case_category");

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);

        if (!empty($filter->status)) {
            $filter->status = isset($filter->status) && ($filter->status == 2 || $filter->status == -1) ? 0 : $filter->status;
            $this->db->where_in($tbl_fms . ".status", $filter->status);
        }

        $this->db->from($tbl_fms);
        $this->db->join('tbl_fms_case_reason', 'tbl_fms_case_reason.caseId = ' . $tbl_fms . '.id', 'left');
        $this->db->join('tbl_fms_case_against_detail', 'tbl_fms_case_against_detail.caseId = ' . $tbl_fms . '.id AND tbl_fms_case_against_detail.against_category = 2', 'left');
        $this->db->join('tbl_fms_case_category', 'tbl_fms_case_category.caseId = ' . $tbl_fms . '.id', 'left');
        $this->db->join('tbl_fms_case_all_category', 'tbl_fms_case_all_category.id = tbl_fms_case_category.categoryId', 'left');

        if (!empty($filter->srch_box)) {
            $this->db->join('tbl_participant', 'tbl_participant.id =  tbl_fms_case.initiated_by AND tbl_fms_case.initiated_type = 2', 'left');
            $this->db->join('tbl_member', 'tbl_member.id =  tbl_fms_case.initiated_by AND tbl_fms_case.initiated_type = 1', 'left');
        }

        $this->db->group_by('tbl_fms_case.id');
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $this->db->where($sWhere, null, false);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #last_query();
        $dt_filtered_total = $all_count = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                $val->category = $val->case_category;
                if ($val->initiated_type == 5 || $val->initiated_type == 6) {
                    $val->initiated_by = $val->Initiator_first_name . ' ' . $val->Initiator_last_name;
                } else {
                    $val->initiated_by = get_fms_initiated_by_name($val->initiated_type, $val->initiated_by);
                }
                $val->timelapse = time_ago_in_php($val->created);
                $val->against_for = get_fms_against_name($val->id);
                $val->description = $val->description;
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'all_count' => $all_count);
        return $return;
    }

    public function get_imail_call_list($reqData) {
        $memberId = $reqData->data->memberId;
        $view_by = $reqData->data->view_by;
        $msg_ary = array();
        $call_ary = array();
        $main_array = array();
        $csv_array = array();

        if ($view_by == 'iMail' || $view_by == '') {
            $tbl_external_message = TBL_PREFIX . 'external_message';
            $tbl_external_message_content = TBL_PREFIX . 'external_message_content';

            if (!empty($reqData->data->start_date)) {
                $this->db->where($tbl_external_message_content . ".created >= '" . date('Y-m-d', strtotime($reqData->data->start_date)) . "'");
            }
            if (!empty($reqData->data->end_date)) {
                $this->db->where($tbl_external_message_content . ".created <= '" . date('Y-m-d', strtotime($reqData->data->end_date)) . "'");
            }

            $sWhere = array('tbl_external_message.recipient_type' => '1', 'tbl_external_message_content.userId' => $memberId);
            $this->db->select(array($tbl_external_message . '.id', $tbl_external_message . '.title', "DATE_FORMAT(tbl_external_message_content.created,'%d/%m/%Y') as created_date", "'Imail'  as contact_type", $tbl_external_message . '.title as csv_title'));
            $this->db->from($tbl_external_message);
            $this->db->join($tbl_external_message_content, $tbl_external_message . '.id = ' . $tbl_external_message_content . '.messageId', 'left');
            $this->db->where($sWhere);
            $this->db->order_by($tbl_external_message . '.id', 'desc');
            $this->db->group_by($tbl_external_message . '.id');
            $query = $this->db->get();
            $obj_1 = $query->result();
            $main_array['msg_data'] = $obj_1;
        }

        if ($view_by == 'phone_call' || $view_by == '') {
            $tbl_member_phone = TBL_PREFIX . 'member_phone';
            $this->db->select(array($tbl_member_phone . '.phone'));
            $this->db->from($tbl_member_phone);
            $this->db->where($tbl_member_phone . '.memberId', $memberId);

            $get_ph_query = $this->db->get();
            $member_ph_data = $get_ph_query->result_array();
            $number = array();
            if (!empty($member_ph_data)) {
                foreach ($member_ph_data as $key => $value) {
                    $number[] = $value['phone'];
                }
            }
            $all_number = implode(',', $number);
            if ($all_number != '') {
                $tbl_call_log = TBL_PREFIX . 'call_log';
                $this->db->select(array($tbl_call_log . '.id', $tbl_call_log . '.audio_url', "DATE_FORMAT(tbl_call_log.created,'%d/%m/%Y') as created_date", "'Call'  as contact_type", $tbl_call_log . '.audio_url as csv_title'));
                $this->db->from($tbl_call_log);
                $this->db->group_start();
                $this->db->or_where_in($tbl_call_log . '.caller_number', $all_number, false);
                $this->db->or_where_in($tbl_call_log . '.receiver_number', $all_number, false);
                $this->db->group_end();
                if (!empty($reqData->data->start_date)) {
                    $this->db->where("Date(tbl_call_log.created) >= '" . date('Y-m-d', strtotime($reqData->data->start_date)) . "'");
                }
                if (!empty($reqData->data->end_date)) {
                    $this->db->where("Date(tbl_call_log.created) <= '" . date('Y-m-d', strtotime($reqData->data->end_date)) . "'");
                }
                $this->db->order_by($tbl_call_log . '.id', 'desc');
                $query = $this->db->get();
                $obj_2 = $query->result_array();
                $main_array['call_data'] = $obj_2;
            }
        }

        $main_array['call_data'] = isset($main_array['call_data']) && !empty($main_array['call_data']) ? $main_array['call_data'] : array();
        $main_array['msg_data'] = isset($main_array['msg_data']) && !empty($main_array['msg_data']) ? $main_array['msg_data'] : array();
        $main_array['csv_array'] = array_merge($main_array['call_data'], $main_array['msg_data']);
        return $main_array;
    }

    public function update_Member_profile($post_data) {
        error_reporting(0);
        #pr($post_data);
        if (!empty($post_data)) {
            $member_id = $post_data['ocs_id'];

            if (!empty($post_data['completeAddress'])) {
                $member_adr = array();
                $dob = date('Y-m-d', strtotime($post_data['dob']));
                $prefer_contact = $post_data['prefer_contact'];
                $this->Basic_model->update_records('member', array('dob' => $dob, 'prefer_contact' => $prefer_contact), array('id' => $member_id));

                /* First delete old record and then insert new record */
                $this->Basic_model->update_records('member_address', array('archive' => '1'), array('memberId' => $member_id));

                foreach ($post_data['completeAddress'] as $key => $value) {
                    $member_adr = array(
                        'street' => $value->street,
                        'postal' => $value->postal,
                        'state' => $value->state,
                        'memberId' => $member_id,
                        'archive' => '0'
                    );

                    $address = $value->street . ' ' . $value->statename . ' ' . $value->postal . ' ' . 'Australia';
                    $lat_long = getLatLong($address);

                    if (isset($value->id) && $value->id != '') {
                        $member_adr['city'] = isset($value->city->value) ? $value->city->value : '';
                        $member_adr['lat'] = isset($lat_long['lat']) ? $lat_long['lat'] : '';
                        $member_adr['long'] = isset($lat_long['long']) ? $lat_long['long'] : '';
                        $this->basic_model->update_records('member_address', $member_adr, array('id' => $value->id));
                    } else {
                        $member_adr['primary_address'] = 2;
                        $member_adr['city'] = isset($value->city->value) ? $value->city->value : '';
                        $member_adr['lat'] = isset($lat_long['lat']) ? $lat_long['lat'] : '';
                        $member_adr['long'] = isset($lat_long['long']) ? $lat_long['long'] : '';
                        $this->Basic_model->insert_records('member_address', $member_adr);
                    }
                }
            }

            if (!empty($post_data['phone_ary'])) {
                /* First delete old record and then insert new record */
                $this->Basic_model->update_records('member_phone', array('archive' => '1'), array('memberId' => $member_id));
                $member_ph = array();
                foreach ($post_data['phone_ary'] as $kk => $ph) {
                    $member_ph = array('memberId' => $member_id,
                        'phone' => $ph->phone,
                        'archive' => '0'
                    );

                    if (isset($ph->id) && $ph->id != '') {
                        $this->basic_model->update_records('member_phone', $member_ph, array('id' => $ph->id));
                    } else {
                        $member_ph['primary_phone'] = 2;
                        $this->Basic_model->insert_records('member_phone', $member_ph);
                    }
                }
            }

            if (!empty($post_data['email_ary'])) {
                /* First delete old record and then insert new record */
                $this->Basic_model->update_records('member_email', array('archive' => '1'), array('memberId' => $member_id));
                $member_mail = array();
                foreach ($post_data['email_ary'] as $ke => $email) {
                    $member_mail = array('memberId' => $member_id,
                        'email' => $email->email,
                        //'primary_email'=>isset($ke) && $ke == 0?'1':'2',
                        'archive' => '0'
                    );
                    if (isset($email->id) && $email->id != '') {
                        $this->basic_model->update_records('member_email', $member_mail, array('id' => $email->id));
                    } else {
                        $member_mail['primary_email'] = 2;
                        $this->Basic_model->insert_records('member_email', $member_mail);
                    }
                }
            }

            if (!empty($post_data['kin_ary'])) {
                $member_kin = array();
                /* First delete old record and then insert new record */
                $this->Basic_model->update_records('member_kin', array('archive' => '1'), array('memberId' => $member_id));

                foreach ($post_data['kin_ary'] as $key => $value) {
                    $member_kin = array(
                        'firstname' => $value->firstname,
                        'lastname' => isset($value->lastname) ? $value->lastname : '',
                        'relation' => $value->relation,
                        'phone' => $value->phone,
                        'email' => $value->email,
                        'memberId' => $member_id,
                        'archive' => '0'
                    );

                    if (isset($value->id) && $value->id != '') {
                        $this->basic_model->update_records('member_kin', $member_kin, array('id' => $value->id));
                    } else {
                        $member_kin['primary_kin'] = $key == 0 ? 1 : 2;
                        $this->Basic_model->insert_records('member_kin', $member_kin);
                    }
                }
            }
            return array('id' => $member_id);
        }
    }

    function get_member_position_award($responseAry) {
        $where_array = array('memberId' => $responseAry->member_id);
        $status = $responseAry->status;

        $where_array['archive'] = ($status == 'Archive') ? '1' : '0';

        $columns = array('mps.position', 'cp.point_name', 'cp.id as award', 'mps.archive', 'mps.id', 'cl.level_name', 'cl.id as level');
        $this->db->select($columns);
        $this->db->where($where_array);
        $this->db->from('tbl_member_position_award as mps');
        $this->db->join('tbl_classification_level as cl', 'cl.id = mps.level', 'inner');
        $this->db->join('tbl_classification_point as cp', 'cp.id = mps.award', 'inner');
        $res = $this->db->get();

        return $res->result_array();
    }

}
