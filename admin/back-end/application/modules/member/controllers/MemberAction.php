<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MemberAction extends MX_Controller 
{
    function __construct() {
        parent::__construct();
        $this->load->model('Member_action_model');
        $this->load->model('Basic_model');
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->loges->setModule(3);
        /*if (!$this->session->userdata('username')) 
        {

        }*/
    }

    public function index() 
    {
        request_handler('access_member');
        die('Invalid request');
    }

    public function get_member_testimonial() {
        request_handler('access_member');
        $result = $this->Member_action_model->get_testimonial();
        echo json_encode(array('status' => true, 'data' => $result));
    }

    public function contact_history_export()
    {
        $request = request_handler('access_member'); 
        $csv_data = $request->data->userSelectedList;
        #pr($csv_data);
        $this->load->library("excel");  
        $object = new PHPExcel();
        if(!empty($csv_data))
        {
            
            $object->setActiveSheetIndex(0);
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'Contact Type');
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'Title/URL');
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'Date');

            $var_row = 2;
            foreach ($csv_data as $one_row) 
            {                                
                $object->getActiveSheet()->SetCellValue('A'.$var_row, $one_row->contact_type);  
                $object->getActiveSheet()->SetCellValue('B'.$var_row, $one_row->csv_title);  
                $object->getActiveSheet()->SetCellValue('C'.$var_row, $one_row->created_date);
                $var_row++;
            }
            
            $object->setActiveSheetIndex()
            ->getStyle('A1:D1')
            ->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'C0C0C0:')
                    )
                )
            );

            $object->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true);
            #$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');            
            $object_writer = PHPExcel_IOFactory::createWriter($object, 'CSV');            
            $filename = time().'_contact_history'.'.csv';
            /*header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$filename);
            $object_writer->save('php://output'); */   
            $object_writer->save(ARCHIEVE_DIR.'/'.$filename);                       
            echo json_encode(array('status'=>true,'csv_url'=>$filename));exit();
        }
        else
        {
            echo json_encode(array('status'=>false,'error'=>'No record to export'));exit();
        }
    }

    public function member_shift_export()
    {
        $request = request_handler('access_member'); 
        $csv_data = $request->data->userSelectedList;
        $active_tab = $request->data->active_tab;
        #pr($csv_data);
        $this->load->library("excel");  
        $object = new PHPExcel();
        if(!empty($csv_data))
        {
            
            $object->setActiveSheetIndex(0);
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'Shift Date');
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'Member name');
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'Start');
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'End');
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'Duration');
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'Status');

            $var_row = 2;
            foreach ($csv_data as $one_row) 
            {                                
                $object->getActiveSheet()->SetCellValue('A'.$var_row, $one_row->shift_date);  
                $object->getActiveSheet()->SetCellValue('B'.$var_row, ucfirst($one_row->memberName));  
                $object->getActiveSheet()->SetCellValue('C'.$var_row, $one_row->start_time);
                $object->getActiveSheet()->SetCellValue('D'.$var_row, $one_row->end_time);
                $object->getActiveSheet()->SetCellValue('E'.$var_row, $one_row->duration);
                $object->getActiveSheet()->SetCellValue('F'.$var_row, $one_row->status);
                $var_row++;
            }
            
            $object->setActiveSheetIndex()
            ->getStyle('A1:D1')
            ->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'C0C0C0:')
                    )
                )
            );

            $object->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true);
            $object_writer = PHPExcel_IOFactory::createWriter($object, 'CSV');            
            if($active_tab == 6)
            $filename = time().'_completed_shift'.'.csv';
            else
            $filename = time().'_cancel_shift'.'.csv';
            $object_writer->save(ARCHIEVE_DIR.'/'.$filename);                       
            echo json_encode(array('status'=>true,'csv_url'=>$filename));exit();
        }
        else
        {
            echo json_encode(array('status'=>false,'error'=>'No record to export'));exit();
        }
    }
}
