<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MemberDashboard extends MX_Controller {

    use formCustomValidation;

    function __construct() {
        parent::__construct();
        $this->load->model('Member_model');
        $this->load->model('Basic_model');
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->loges->setModule(3);
        if (!$this->session->userdata('username')) {

        }
    }

    public function index() {
        request_handler('access_member');
        $data['member_count'] = base_url('member_count');
        $data['form_action_member_srch'] = base_url('get_members_list');
    }

    public function member_count() {
        $reqData = request_handler('access_member');
        if (!empty($reqData->data)) {
            $post_data = $reqData->data;
            if ($post_data->view_type == 'year') {
                $where['YEAR(created)'] = date('Y');
                $where['archive'] = '0';
            } else if ($post_data->view_type == 'week') {
                $where = '';
                $where1 = "created > DATE_SUB(NOW(), INTERVAL 1 WEEK) AND archive = '0'";
                $this->db->where($where1);
            } else {
                $where['MONTH(created)'] = date('m');
                $where['YEAR(created)'] = date('Y');
                $where['archive'] = '0';
            }

            $rows = $this->Basic_model->get_result('member', $where, array('id'));
            $all_rows = $this->Basic_model->get_result('member', array('archive' => '0'), array('id'));
            if (!empty($all_rows))
                $all_member_count = count($all_rows);
            else
                $all_member_count = 0;

            if (!empty($rows))
                $count = count($rows);
            else
                $count = 0;

            echo json_encode(array('member_count' => $count, 'status' => TRUE, 'all_member_count' => $all_member_count));
            exit();
        }
    }

    public function load_member_list() {
        require_once APPPATH . 'Classes/member/Member.php';
        $reqData = request_handler('access_member');
        $objMember = new MemberClass\Member();
        $rows = $objMember->load_member_list($reqData);
        echo json_encode($rows);
        exit();
    }

    public function get_member_profile() {
        require_once APPPATH . 'Classes/member/Member.php';
        $objMember = new MemberClass\Member();
        $request = request_handler('access_member');
        $memberId = $request->data->member_id;
        $objMember->setMemberid($memberId);
        $row = $objMember->get_member_profile();
        echo json_encode($row);
        exit();
    }

    public function get_member_about() {
        require_once APPPATH . 'Classes/member/Member.php';
        $objMember = new MemberClass\Member();
        $request = request_handler('access_member');
        $memberId = $request->data->member_id;
        $objMember->setMemberid($memberId);
        $row = $objMember->get_member_about();
        echo json_encode(array('status' => true, 'data' => $row));
        exit();
    }

    public function member_app_access() {
        if (!empty(request_handler('access_member'))) {
            $request = request_handler('access_member');

            $status = $request->data->status;
            $member_id = $request->data->member_id;

            $data = array('enable_app_access' => $status);
            $where = array('id' => $member_id);

            $this->Basic_model->update_records('member', $data, $where);
            /* logs */
            $this->loges->setCreatedBy($request->adminId);
            $this->loges->setUserId($member_id);
            $this->loges->setDescription(json_encode($request));
            $this->loges->setTitle('Update portal access for MemberId : ' . $member_id);
            $this->loges->createLog();

            if ($status == 0)
                $this->Basic_model->delete_records('member_login', array('memberId' => $member_id));

            echo json_encode(array('status' => true));
            exit();
        }
    }

    public function change_status() {
        if (!empty(request_handler('update_member'))) {
            $request = request_handler('update_member');
            $status = $request->data->status;
            $member_id = $request->data->member_id;
            $data = array('status' => $status);
            $where = array('id' => $member_id);
            $this->Basic_model->update_records('member', $data, $where);

            /* logs */
            $this->loges->setCreatedBy($request->adminId);
            $this->loges->setUserId($member_id);
            $this->loges->setDescription(json_encode($request));
            $this->loges->setTitle('Change status for MemberId : ' . $member_id);
            $this->loges->createLog();

            if ($status == 0)
                $this->Basic_model->delete_records('member_login', array('memberId' => $member_id));
            echo json_encode(array('status' => true));
            exit();
        }
    }

    public function member_qualification() {
        require_once APPPATH . 'Classes/member/MemberQualification.php';
        $objMember = new MemberQualification();
        $request = request_handler('access_member');

        if (!empty($request->data)) {
            $req = $request->data;

            $memberId = $req->member_id;
            $view_by = $req->view_by;

            if ($view_by == 'Archive')
                $archive = 1;
            else
                $archive = 0;

            $objMember->setMemberid($memberId);
            $objMember->setArchive($archive);
            $objMember->setStart_date($req->start_date);
            $objMember->setEnd_date($req->end_date);

            $row = $objMember->get_member_qualification();

            if (!empty($row)) {
                echo json_encode(array('status' => true, 'data' => $row));
                exit();
            } else {
                echo json_encode(array('status' => false, 'data' => array()));
                exit();
            }
        }
    }

    public function upload_member_qualification() {
        $request = request_handlerFile('update_member');
        $member_data = (array) $request;
//        pr($request);
        if (!empty($member_data)) {
            $validation_rules = array(
                // array('field' => 'expiry', 'label' => 'Qualification Expiry Date', 'rules' => 'required'),
                array('field' => 'docsTitle', 'label' => 'Title', 'rules' => 'required'),
                array('field' => 'docsCategory', 'label' => 'Qualification Category', 'rules' => 'required'),
            );

            $this->form_validation->set_data($member_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                #$request_body = file_get_contents('php://input',true);
                #$request_body = json_decode($request_body);
                if (!empty($_FILES) && $_FILES['myFile']['error'] == 0) {
                    $config['upload_path'] = MEMBER_UPLOAD_PATH;
                    $config['input_name'] = 'myFile';
                    $config['directory_name'] = $request->memberId;
                    $config['allowed_types'] = 'jpg|jpeg|png|xlx|xls|doc|docx|pdf';

                    $is_upload = do_upload($config);

                    if (isset($is_upload['error'])) {
                        echo json_encode(array('status' => false, 'error' => strip_tags($is_upload['error'])));
                        exit();
                    } else {
                        $insert_ary = array('filename' => $is_upload['upload_data']['file_name'],
                            'memberId' => $request->memberId,
                            'title' => $request->docsTitle,
                            'type' => $request->docsCategory,
                            'expiry' => !empty($request->expiry) ? date('Y-m-d', strtotime($request->expiry)) : null,
                            'created' => DATE_TIME,
                            'archive' => 0,
                        );
                        
                        $rows = $this->Basic_model->insert_records('member_qualification', $insert_ary, $multiple = FALSE);

                        /* logs */
                        $this->loges->setCreatedBy($request->adminId);
                        $this->loges->setUserId($this->input->post('memberId'));
                        $this->loges->setDescription(json_encode($request));
                        $this->loges->setTitle('Added New document : ' . $request->memberName);
                        $this->loges->createLog();

                        if (!empty($rows)) {
                            echo json_encode(array('status' => true));
                            exit();
                        } else {
                            echo json_encode(array('status' => false));
                            exit();
                        }
                    }
                } else {
                    echo json_encode(array('status' => false, 'error' => 'Error in uploading file.'));
                    exit();
                }
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode("\n", $errors));
            }
            echo json_encode($return);
        }
    }

    public function get_member_preference() {
        require_once APPPATH . 'Classes/member/Member.php';
        $objMember = new MemberClass\Member();
        $request = request_handler('access_member');
        $memberId = $request->data->member_id;
        $objMember->setMemberid($memberId);

        $row = $objMember->get_member_preference();
        if (!empty($row)) {
            echo json_encode(array('status' => true, 'data' => $row));
            exit();
        } else {
            echo json_encode(array('status' => false));
            exit();
        }
    }

    public function save_member_availability() {
        $request = request_handler('create_member');
        $responseAry = $request->data;
        $member_data = (array) $request->data->data;
        if (!empty($member_data)) {
            $validation_rules = array(
                array('field' => 'start_date', 'label' => 'Start Date', 'rules' => 'required'),
                array('field' => 'title', 'label' => 'Title', 'rules' => 'required'),
                    // array('field' => 'end_date', 'label' => 'End Date', 'rules' => 'required|callback_check_enddate['.isset($member_data->data['end_date'])?$member_data->data['end_date']:''.','.$member_data->data['is_default'].']'),
                    //array('field' => 'first_week[]', 'label' => 'Please select atleast one day availability to Continue', 'rules' => 'callback_check_avail['.$member_data->data['first_week'].']'),
            );

            $this->form_validation->set_data($member_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $is_default = $responseAry->data->is_default;
                $memberId = $responseAry->memberId;
                $insert_ary = array('title' => $responseAry->data->title,
                    'memberId' => $memberId,
                    'start_date' => DateFormate($responseAry->data->start_date, 'Y-m-d'),
                    'end_date' => isset($responseAry->data->end_date) && !empty($responseAry->data->end_date) ? DateFormate($responseAry->data->end_date, 'Y-m-d') : '',
                    'first_week' => isset($responseAry->data->first_week) ? json_encode($responseAry->data->first_week) : '',
                    'second_week' => isset($responseAry->data->second_week) ? json_encode($responseAry->data->second_week) : '',
                    'flexible_availability' => isset($responseAry->data->flexible_availability) ? $responseAry->data->flexible_availability : '',
                    'flexible_km' => isset($responseAry->data->flexible_km) ? $responseAry->data->flexible_km : '',
                    'travel_km' => isset($responseAry->data->travel_km) ? $responseAry->data->travel_km : '',
                    'status' => 1,
                    'is_default' => $is_default,
                    'travel_km' => isset($responseAry->data->travel_km) ? $responseAry->data->travel_km : 10,
                    'updated' => DATE_TIME
                );

                if ($is_default == 1) {
                    $delete_data = array('memberId' => $responseAry->memberId);
                    $this->delete_default_availability($delete_data);
                }

                if ($responseAry->is_update == 0) {
                    $availability_id = $row = $this->Basic_model->insert_records('member_availability', $insert_ary, $multiple = FALSE);
                    $last_query = $this->db->last_query();
                    //@file_put_contents($_SERVER['DOCUMENT_ROOT'].'/application/modules/member/controllers/response.txt', $last_query.PHP_EOL , FILE_APPEND | LOCK_EX);
                    $this->generate_member_shift($row);
                    $msg = "Availabitity created successfully.";
                    $log_msg = 'Added new availability : ' . $availability_id;
                } else {
                    $availability_id = $responseAry->data->id;
                    $where = array('id' => $responseAry->data->id);
                    $row = $this->Basic_model->update_records('member_availability', $insert_ary, $where);
                    $msg = "Availabitity updated successfully.";
                    $log_msg = 'Updated availability : ' . $responseAry->full_name;
                }
                #if($responseAry->avail_list)
                {
                    #$this->merge_avail_list($responseAry,$memberId,$availability_id);
                }

                //logs
                $this->loges->setCreatedBy($request->adminId);
                $this->loges->setUserId($responseAry->memberId);
                $this->loges->setDescription(json_encode($request));
                $this->loges->setTitle($log_msg);
                $this->loges->createLog();
                $return = array('status' => true, 'response_msg' => $msg);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($return);
            exit();
        }
    }

    public function delete_default_availability($delete_data) {
        $where_avail = array('memberId' => $delete_data['memberId'], 'is_default' => 1);
        $data = array('archive' => 1);
        $this->Basic_model->update_records('member_availability', $data, $where_avail);
        $last_query = $this->db->last_query();
        @file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/application/modules/member/controllers/response.txt', $last_query . PHP_EOL, FILE_APPEND | LOCK_EX);
        $this->Basic_model->update_records('member_availability_list', $data, $where_avail);
        $last_query = $this->db->last_query();
        @file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/application/modules/member/controllers/response.txt', $last_query . PHP_EOL, FILE_APPEND | LOCK_EX);
    }

    public function merge_avail_list($responseAry, $memberId, $availability_id) {
        //pr($responseAry->avail_list);
        //1 = old save krna hai (nothing to do)
        //2 = old ko delete and new save
        $new_list = $responseAry->avail_list;
        if (!empty($new_list)) {
            $is_run = TRUE;
            foreach ($new_list as $key => $value) {
                if ($value->is_collapse == 'true') {
                    if ($value->status == 2) {
                        $where_avail = array('memberId' => $memberId, 'availability_type' => $value->old->availability_type, 'availability_date' => $value->old->availability_date);
                        $data = array('archive' => 1);
                        $this->Basic_model->update_records('member_availability_list', $data, $where_avail);
                        $is_run = FALSE;
                    }
                } else {
                    $is_run = FALSE;
                }

                if ($is_run == FALSE) {
                    $new_avail_list[] = array('member_availability_id' => $availability_id,
                        'memberId' => $memberId,
                        'is_default' => $value->new->is_default,
                        'availability_type' => $value->new->availability_type,
                        'availability_date' => DateFormate($value->new->availability_date, 'Y-m-d'),
                        'flexible_availability' => isset($responseAry->data->flexible_availability) ? $responseAry->data->flexible_availability : '',
                        'flexible_km' => isset($responseAry->data->flexible_km) ? $responseAry->data->flexible_km : '',
                        'travel_km' => isset($responseAry->data->travel_km) ? $responseAry->data->travel_km : 10,
                    );
                }
            }
            $this->Basic_model->insert_records('member_availability_list', $new_avail_list, $multiple = TRUE);
        }
    }

    public function get_member_availability() {
        require_once APPPATH . 'Classes/member/Member.php';
        $objMember = new MemberClass\Member();

        $request = request_handler('access_member');
        $memberId = $request->data->member_id;
        $status = $request->data->status;

        if ($status == 'Active')
            $Db_status = 1;
        else
            $Db_status = 0;

        $objMember->setMemberid($memberId);
        $objMember->setStatus($Db_status);
        $row = $objMember->get_member_availability();

        if (!empty($row)) {
            echo json_encode(array('status' => true, 'data' => $row));
            exit();
        } else {
            echo json_encode(array('status' => false, 'data' => array()));
            exit();
        }
    }

    public function save_member_work_area() {
        $request = request_handler('create_member');
        $responseAry = $request->data;
        $responseData = (array) $request->data->data;
        #pr($responseData);
        if (!empty($responseData)) {
            $validation_rules = array(
                array('field' => 'memberWorkArea', 'label' => 'Work Area', 'rules' => 'required'),
                array('field' => 'memberWorkAreaStatus', 'label' => 'Status for Work Area', 'rules' => 'required'),
            );

            $this->form_validation->set_data($responseData);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $row = $this->Basic_model->get_row('member_work_area', array('work_area'), array('memberId' => $responseAry->memberId, 'archive' => 0, 'work_area' => $responseAry->data->memberWorkArea));
                if ($row) {
                    echo json_encode(array('status' => false, 'error' => 'This Work area is already exist.'));
                    exit();
                } else {
                    $insert_ary = array('memberId' => $responseAry->memberId,
                        'work_area' => isset($responseAry->data->memberWorkArea) ? $responseAry->data->memberWorkArea : '',
                        'work_status' => isset($responseAry->data->memberWorkAreaStatus) ? $responseAry->data->memberWorkAreaStatus : '',
                        'created' => DATE_TIME,
                    );

                    $rows = $this->Basic_model->insert_records('member_work_area', $insert_ary, $multiple = FALSE);

                    //logs
                    $this->loges->setCreatedBy($request->adminId);
                    $this->loges->setUserId($responseAry->memberId);
                    $this->loges->setDescription(json_encode($request));
                    $this->loges->setTitle('Save workarea : ' . $responseAry->memberId);
                    $this->loges->createLog();

                    if (!empty($rows)) {
                        $return = array('status' => TRUE);
                        echo json_encode($return);
                        exit();
                    } else {
                        $return = array('status' => false);
                        echo json_encode($return);
                        exit();
                    }
                }
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($return);
        }
    }

    public function get_member_work_area() {
        $request = request_handler('access_member');
        $responseAry = $request->data;
        $where_array = array('memberId' => $responseAry->member_id);
        $status = $responseAry->status;

        if ($status == 'Archive')
            $where_array['archive'] = 1;
        else
            $where_array['archive'] = 0;

        $columns = array('work_area,work_status,archive,id');
        $row = $this->Basic_model->get_result('member_work_area', $where_array, $columns);
        echo json_encode(array('status' => true, 'data' => $row));
        exit();
    }

    public function upload_member_special_agreement() {
        $request = request_handlerFile('update_member');
        $member_data = (array) $request;
        #pr($request);
        if (!empty($member_data)) {
            $validation_rules = array(
                array('field' => 'expiry', 'label' => 'Expiry date', 'rules' => 'required'),
                array('field' => 'docsTitle', 'label' => 'Title', 'rules' => 'required'),
            );

            $this->form_validation->set_data($member_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                #$request_body = file_get_contents('php://input',true);
                #$request_body = json_decode($request_body);
                if (!empty($_FILES) && $_FILES['myFile']['error'] == 0) {
                    $config['upload_path'] = MEMBER_UPLOAD_PATH;
                    $config['input_name'] = 'myFile';
                    $config['directory_name'] = $this->input->post('memberId');
                    $config['allowed_types'] = 'jpg|jpeg|png|xlx|xls|doc|docx|pdf';

                    $is_upload = do_upload($config);

                    if (isset($is_upload['error'])) {
                        echo json_encode(array('status' => false, 'error' => strip_tags($is_upload['error'])));
                        exit();
                    } else {
                        $insert_ary = array('filename' => $is_upload['upload_data']['file_name'],
                            'memberId' => $this->input->post('memberId'),
                            'title' => $this->input->post('docsTitle'),
                            'expiry' => date('Y-m-d', strtotime($this->input->post('expiry'))),
                            'created' => DATE_TIME,
                            'archive' => 0,
                        );
                        $rows = $this->Basic_model->insert_records('member_special_agreement', $insert_ary, $multiple = FALSE);
                        /* logs */
                        $this->loges->setCreatedBy($request->adminId);
                        $this->loges->setUserId($this->input->post('memberId'));
                        $this->loges->setDescription(json_encode($request));
                        $this->loges->setTitle('Added special agreement : ' . $this->input->post('memberName'));
                        $this->loges->createLog();
                        if (!empty($rows)) {
                            echo json_encode(array('status' => true));
                            exit();
                        } else {
                            echo json_encode(array('status' => false));
                            exit();
                        }
                    }
                } else {
                    echo json_encode(array('status' => false, 'error' => 'Error in uploading file.'));
                    exit();
                }

                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode("\n", $errors));
            }
            echo json_encode($return);
        }
    }

    public function get_member_special_agreement() {
        $request = request_handler('access_member');
        $status = $request->data->status;
        $where_array = array('memberId' => $request->data->member_id);

        if ($status == 'Expire')
            $where_array['DATE(expiry) < '] = date('Y-m-d');
        else
            $where_array['DATE(expiry) >='] = date('Y-m-d');

        $columns = array('expiry,filename,id,title');
        $row = $this->Basic_model->get_result('member_special_agreement', $where_array, $columns);

        if (!empty($row)) {
            echo json_encode(array('status' => true, 'data' => $row));
            exit();
        } else {
            echo json_encode(array('status' => false, 'data' => array()));
            exit();
        }
    }

    public function download_selected_file() {
        $request = request_handler('access_member');
        $responseAry = $request->data;
        $this->load->library('zip');
        $member_id = $responseAry->member_id;
        $download_data = $responseAry->downloadData;
        $this->zip->clear_data();
        $x = '';
        $file_count = 0;
        if (!empty($download_data)) {
            $zip_name = time() . '_' . $member_id . '.zip';
            foreach ($download_data as $file) {
                if (isset($file->is_active) && $file->is_active) {
                    $file_path = MEMBER_UPLOAD_PATH . $member_id . '/' . $file->filename;
                    $this->zip->read_file($file_path, FALSE);
                    $file_count = 1;
                }
            }
            $x = $this->zip->archive('archieve/' . $zip_name);
        }
        if ($x && $file_count == 1) {
            echo json_encode(array('status' => true, 'zip_name' => $zip_name));
            exit();
        } else {
            echo json_encode(array('status' => false, 'error' => 'Please select atleast one file to continue.'));
            exit();
        }
    }

    public function update_member_preference() {
        $reqData = request_handler('update_member');
        if (!empty($reqData->data)) {
            $data = $reqData->data;
            $prefers = array();
            $key = ($data->type == 'places') ? 'placeId' : 'activityId';
            $table = ($data->type == 'places') ? 'member_place' : 'member_activity';

            $this->basic_model->delete_records($table, $where = array('memberId' => $data->memberId));
            if (!empty($data->preference)) {
                foreach ($data->preference as $val) {
                    if ($val->type == 1 || $val->type == 2) {
                        $prefers[] = array('memberId' => $data->memberId, 'type' => $val->type, $key => $val->id);
                    }
                }

                if (!empty($prefers)) {
                    $this->basic_model->insert_records($table, $prefers, $multiple = true);
                    //logs
                    $this->loges->setCreatedBy($reqData->adminId);
                    $this->loges->setUserId($data->memberId);
                    $this->loges->setDescription(json_encode($reqData));
                    $this->loges->setTitle('Updated Preference ' . $data->type . ': ' . $data->memberId);
                    $this->loges->createLog();
                }
            }

            echo json_encode(array('status' => true));
        }
    }

    public function member_shift_count() {
        $reqData = request_handler();
        $this->Member_model->member_shift_count($reqData);
    }

    public function get_member_shifts() {
        $reqData = request_handler('access_member');
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $result = $this->Member_model->get_member_shifts($reqData);
            echo json_encode($result);
        }
    }

    public function get_member_upcoming_shifts() {
        $reqData = request_handler('access_member');
        if (!empty($reqData->data)) {
            $result = $this->Member_model->get_member_upcoming_shifts($reqData->data);
            echo json_encode($result);
        }
    }

    public function generate_member_shift($member_availability_id) {
        /*         * this function is call inside another fun.
        So server side validation is not implemented* */
        $where = array('id' => $member_availability_id, 'archive' => 0);
        $rows = $this->Basic_model->get_row('member_availability', array('*'), $where);

        $x = $this->generate_shift_data($rows, $member_availability_id);
        if (!empty($x))
            $this->Basic_model->insert_records('member_availability_list', $x, $multiple = TRUE);

        $last_query = $this->db->last_query();
        //@file_put_contents($_SERVER['DOCUMENT_ROOT'].'/application/modules/member/controllers/response.txt', $last_query.PHP_EOL , FILE_APPEND | LOCK_EX);
    }

    public function generate_shift_data($rows, $member_availability_id) {
        /*         * this function is call inside another fun.
        So server side validation is not implemented* */

        $main_ary = array();
        $is_create_shift = TRUE;
        $break_loop = FALSE;

        if (is_json($rows->first_week))
            $week = $rows->first_week;
        else if (is_json($rows->second_week))
            $week = $rows->second_week;
        else
            $is_create_shift = FALSE;

        $day_before = '';
        if (is_json($week)) {
            $start_date = date('Y-m-d', strtotime($rows->start_date));
            // if end date is 00 and default is 1 means create current date shift
            if ($rows->is_default == 1 && $rows->end_date == '0000-00-00 00:00:00') {
                //echo '<br/>';
                //shift is cr
                $day_before = dayDifferenceBetweenDate(date('Y-m-d'), $start_date);
                //echo '<br/>';
                //shift is created if difference between current date and start date is less than 28
                if ($day_before <= 28) {
                    $day_difference = 28 - $day_before;
                    $end_date = date('Y-m-d', strtotime($start_date . " +$day_difference days"));
                } else {
                    $is_create_shift = FALSE;
                }
            } else {
                $end_date = DateFormate($rows->end_date, 'Y-m-d');
            }

//pr([$rows->start_date, $end_date]);
            if (strtotime($rows->start_date) >= strtotime(date('Y-m-d')) && $is_create_shift) {
                $dates = dateRangeBetweenDate($start_date, $end_date);
                $avail_ary = json_decode($week, true);
                #pr($dates); 
                foreach ($dates as $dateKey => $date) {
                    $myWweek = '';
                    if ($break_loop) {
                        $myWweek = 'second_week';
                        $break_loop = FALSE;
                        $avail_ary = json_decode($rows->second_week, true);
                    }

                    if (!empty($avail_ary)) {
                        foreach ($avail_ary as $key => $value) {
                            if ($break_loop)
                                break;
                            foreach ($value as $val) {
                                if ($break_loop)
                                    break;
                                foreach ($val as $kk => $day) {
                                    if ($day && ($date == $kk)) {

                                        $shift_row = $this->Member_model->get_shift_for_availibility(array('member_id' => $rows->memberId, 'shift_date' => $dateKey));
                                        /* if shift is created for any availiibility date then that date will not overide */
                                        if (empty($shift_row)) {
                                            $temp_ary['memberId'] = $rows->memberId;
                                            $temp_ary['flexible_availability'] = $rows->flexible_availability;
                                            $temp_ary['flexible_km'] = $rows->flexible_km;
                                            $temp_ary['is_default'] = $rows->is_default;
                                            $temp_ary['travel_km'] = $rows->travel_km;
                                            $temp_ary['run_mode'] = 'OCS';
                                            $temp_ary['availability_type'] = $key;
                                            $temp_ary['availability_date'] = $dateKey;
                                            $temp_ary['created'] = DATE_TIME;
                                            $temp_ary['member_availability_id'] = $member_availability_id;
                                            $main_ary[] = $temp_ary;
                                        }
                                        // if condition is bocoz, In first/second week we have to create shift till sunday i.e., for a week only
                                        /*  if ($kk == 'Sun' && $myWweek != 'second_week') {
                                          $break_loop = true;
                                          break;
                                      } */
                                  }
                              }
                          }
                      }
                  }
              }
              return $main_ary;
          }
      }
  }

  public function dwes_checked() {
    if (!empty(request_handler('update_member'))) {
        $request = request_handler('update_member');
        $status = $request->data->status;
        $member_id = $request->data->member_id;
        $data = array('dwes_confirm' => $status);
        $where = array('id' => $member_id);
        $this->Basic_model->update_records('member', $data, $where);
        $return_status = isset($status) && $status == 1 ? 0 : 1;
        echo json_encode(array('status' => true, 'return_status' => $return_status));
        exit();
    }
}

public function get_previous_availiability() {
    $reqData = request_handler('access_member');
    $data = $reqData->data;
    $result = $this->Member_model->get_previous_availiability($data);

    $day_difference = 28;
    if (isset($reqData->end_date)) {
        $end_date = date('Y-m-d', strtotime($reqData->end_date));
    } else {
        $start_date = date('Y-m-d', strtotime($data->start_date));
        $end_date = date('Y-m-d', strtotime($start_date . " +$day_difference days"));
    }

        //
    $first_week = isset($data->first_week) ? json_encode($data->first_week) : '';
    $second_week = isset($data->second_week) ? json_encode($data->second_week) : '';

    $row = (object) array('first_week' => $first_week,
        'second_week' => $second_week,
        'is_default' => $data->is_default,
        'end_date' => $end_date,
        'start_date' => date('Y-m-d', strtotime($data->start_date)),
        'memberId' => $data->member_id,
        'flexible_availability' => $data->flexible_availability,
                //'travel_km'=>$data->travel_km,
                //'flexible_km'=>$data->flexible_km,
    );
    $x = $this->generate_shift_data($row, 0);

    $old_record = $result;
    $new_record = $x;

    $colapse_count = 0;
    $main_ary = array();

    if (!empty($new_record)) {
        foreach ($new_record as $key => $value) {
            $main_ary[$key]['is_collapse'] = FALSE;
            $new_s_key = array_search($value['availability_date'], array_column($old_record, 'availability_date'));
            if ($new_s_key !== FALSE) {
                if ($old_record[$new_s_key]['availability_type'] == $value['availability_type']) {
                    $main_ary[$key]['old'] = $old_record[$new_s_key];
                    $main_ary[$key]['is_collapse'] = true;
                    $main_ary[$key]['status'] = 1;
                    $colapse_count = $colapse_count + 1;
                }
            }
            $main_ary[$key]['new'] = $value;
        }
    }
    echo json_encode(array('data_ary' => $main_ary, 'colapse_count' => $colapse_count, 'status' => true));
    exit();
}

public function check_availibility_already_exist() {
    $reqData = request_handler('access_member');
    $data = $reqData->data;
    $result = $this->Member_model->check_availibility_already_exist($data);
    echo json_encode(array('rows_count' => count((array) $result), 'status' => true));
    exit();
}

public function get_member_fms() {
    $reqestData = request_handler('access_fms', 1, 1);
    if (!empty($reqestData->data)) {
        $reqData = json_decode($reqestData->data);
        $result = $this->Member_model->get_member_fms($reqData, $reqestData->adminId);
        echo json_encode($result);
    }
}

public function get_member_position_award() {
    $request = request_handler('access_member');
    $responseAry = $request->data;

    $row = $this->Member_model->get_member_position_award($responseAry);

    echo json_encode(array('status' => true, 'data' => $row));
    exit();
}

public function get_posistion_and_level() {
    request_handler('access_member');

    $row = [];
    $row['levels'] = $this->Basic_model->get_record_where('classification_level', ['level_name as label', 'id as value'], '');
    $row['positions'] = $this->Basic_model->get_record_where('classification_point', ['point_name as label', 'id as value'], '');

    echo json_encode(array('status' => true, 'data' => $row));
    exit();
}

public function save_member_position_award() {
    $request = request_handler('create_member');
    $responseAry = $request->data;

    $mode = $responseAry->mode;

    if ($mode === 'add') {
        $res = $this->Basic_model->get_row('member_position_award', ['position'], ['memberId' => $responseAry->memberId, 'archive' => 0]);
        if (!empty($res)) {
            echo json_encode(array('status' => false, 'msg' => 'Position details already exist.'));
            exit();
        } else {
            $success_msg = 'Position created successfully';
            $this->loges->setTitle('Save Position/Award : ' . $responseAry->memberId);
            $insert_ary = array('memberId' => $responseAry->memberId,
                'position' => isset($responseAry->data->position) ? $responseAry->data->position : '',
                'award' => isset($responseAry->data->award) ? $responseAry->data->award : '',
                'level' => isset($responseAry->data->level) ? $responseAry->data->level : '',
                'created' => DATE_TIME,
                'updated' => DATE_TIME,
            );
            $rows = $this->Basic_model->insert_records('member_position_award', $insert_ary);
        }
    } else {
        $success_msg = 'Position updated successfully';
        $this->loges->setTitle('Update Position/Award : ' . $responseAry->memberId);
        $update_ary = array('memberId' => $responseAry->memberId,
            'position' => isset($responseAry->data->position) ? $responseAry->data->position : '',
            'award' => isset($responseAry->data->award) ? $responseAry->data->award : '',
            'level' => isset($responseAry->data->level) ? $responseAry->data->level : '',
        );
        $rows = $this->Basic_model->update_records('member_position_award', $update_ary, array('id' => $responseAry->id));
        $rows = TRUE;
    }

        //logs
    $this->loges->setCreatedBy($request->adminId);
    $this->loges->setUserId($responseAry->memberId);
    $this->loges->setDescription(json_encode($request));

    $this->loges->createLog();

    if (!empty($rows)) {
        echo json_encode(array('status' => true, 'msg' => $success_msg));
        exit();
    } else {
        echo json_encode(array('status' => false, 'msg' => 'Error,Please try again'));
        exit();
    }
        //}    
}

public function get_imail_call_list() {
    $request = request_handler('access_member');
    $result = $this->Member_model->get_imail_call_list($request);
    if (!empty($result)) {
        echo json_encode(array('status' => true, 'data' => $result));
        exit();
    } else {
        echo json_encode(array('status' => false));
        exit();
    }
}

public function update_Member_profile() {
    $request = request_handler('update_member');
    $member_data = (array) $request->data->member_data;

        //pr($member_data);
    if (!empty($member_data)) {
        $validation_rules = array(
            array('field' => 'prefer_contact', 'label' => 'Prefered Contact', 'rules' => 'required'),
            array('field' => 'dob', 'label' => 'Date of Birth', 'rules' => 'required|callback_date_check[dob,current,%s cannot enter a date in the future.]'),
                //array('field' => 'phone_ary[]', 'label' => 'Member Phone', 'rules' => 'callback_check_phone_number'),
                //array('field' => 'email_ary[]', 'label' => 'Member Email', 'rules' => 'callback_check_email_address'),
            array('field' => 'completeAddress[]', 'label' => 'Address', 'rules' => 'callback_check_address|callback_postal_code_check[postal]'),
            array('field' => 'kin_ary[]', 'label' => 'Kin Address', 'rules' => 'callback_kin_details_check[Next of kin ]|callback_phone_number_check[phone,required,Kin contact should be enter valid phone number.]'),
            array('field' => 'phone_ary[]', 'label' => 'Member Phone', 'rules' => 'callback_phone_number_check[phone,required,Member Contact should be enter valid phone number.]'),
        );

        $this->form_validation->set_data($member_data);
        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run()) {
            $ocs_row = $this->Member_model->update_Member_profile($member_data);
            /* logs */
            $this->loges->setTitle("Update Member: " . $ocs_row['id']);
            $this->loges->setUserId($request->adminId);
            $this->loges->setDescription(json_encode($member_data));
            $this->loges->setCreatedBy($request->adminId);
            $this->loges->createLog();
            $return = array('status' => true, 'msg' => 'Member updated successfully.', 'memberId' => $ocs_row['id']);
        } else {
            $errors = $this->form_validation->error_array();
            $return = array('status' => false, 'error' => implode(', ', $errors));
        }
        echo json_encode($return);
    }
}

public function check_phone_number($phone_numbers) {
    if (!empty($phone_numbers)) {
        foreach ($phone_numbers as $key => $val) {
            if ($key == 'primary_phone')
                continue;

            if (empty($val)) {
                $this->form_validation->set_message('check_phone_number', 'Phone number can not be empty');
                return false;
            }
        }
    } else {
        $this->form_validation->set_message('check_participant_number', 'phone number can not be empty');
        return false;
    }
    return true;
}

public function check_email_address($email_address) {
        //pr($email_address);
    if (!empty($email_address)) {
        foreach ($email_address as $key => $val) {
            if ($key == 'primary_email')
                continue;

            if (empty($val)) {
                $this->form_validation->set_message('check_email_address', 'Email can not empty');
                return false;
            } elseif (!filter_var($val, FILTER_VALIDATE_EMAIL)) {
                $this->form_validation->set_message('check_email_address', 'Email is not valid');
                return false;
            }
        }
    } else {
        $this->form_validation->set_message('check_participant_email_address', 'participant email can not empty');
        return false;
    }
    return true;
}

public function check_address($address) {

    if (!empty($address)) {
        if (empty($address->street)) {
            $this->form_validation->set_message('check_address', 'Address can not be empty');
            return false;
        } elseif (empty($address->state)) {
            $this->form_validation->set_message('check_address', 'State can not be empty');
            return false;
        } elseif (empty($address->postal)) {
            $this->form_validation->set_message('check_address', 'Pincode can not be empty');
            return false;
        } elseif ((isset($address->city) && empty($address->city)) || (!isset($address->city->value) || (isset($address->city->value) && empty($address->city->value)) )) {
            $this->form_validation->set_message('check_address', 'Suburb can not be empty');
            return false;
        }
    } else {
        $this->form_validation->set_message('check_address', 'Address can not empty');
        return false;
    }
    return true;
}

public function archive_member_qualification() {
    require_once APPPATH . 'Classes/member/MemberQualification.php';
    $objMember = new MemberQualification();
    $request = request_handler('access_member');

    if (!empty($request->data)) {
        $req = $request->data;
        $memberId = $req->member_id;
        $id = $req->id;

        $objMember->setMemberid($memberId);
        $objMember->setMemberqualificationid($id);
        $row = $objMember->archive_member_qualification();

        if (!empty($row)) {
            echo json_encode(array('status' => true));
            exit();
        } else {
            echo json_encode(array('status' => false));
            exit();
        }
    }
}

public function archive_member_workarea_awards() {
    $request = request_handler('access_member');

    if (!empty($request->data)) {
        $req = $request->data;
        $memberId = $req->member_id;
        $id = $req->id;
        $type = $req->type;

        $arr = array("id" => $id, "memberId" => $memberId);

        if($type == 2)    
            $row =  $this->Basic_model->update_records('member_position_award', array('archive'=>1),$arr);
        else
         $row =  $this->Basic_model->update_records('member_work_area', array('archive'=>1),$arr);

     if (!empty($row)) {
        echo json_encode(array('status' => true));
        exit();
    } else {
        echo json_encode(array('status' => false));
        exit();
    }
}
}

}
