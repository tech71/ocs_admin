<?php

class Org_model extends CI_Model {

    public function get_org($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';
        $tbl_org = TBL_PREFIX . 'organisation';
        $src_columns = array();
        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_org . '.id';
            $direction = 'DESC';
        }

        $where = '';

        $where = $tbl_org . ".parent_org = 0";

        $sWhere = $where;

        if (!empty($filter->srch_box)) {
            $src_columns = array($tbl_org . ".id", $tbl_org . ".name", $tbl_org . ".status", "tbl_organisation_phone.phone", "tbl_organisation_address.street", "tbl_organisation_address.city", "tbl_organisation_address.postal", "tbl_suburb_state.state");

            $where = $where . " AND (";
            $sWhere = " (" . $where;
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $sWhere .= $serch_column[0] . " LIKE '%" . $this->db->escape_like_str($filter->srch_box) . "%' OR ";
                } else {
                    $sWhere .= $column_search . " LIKE '%" . $this->db->escape_like_str($filter->srch_box) . "%' OR ";
                }
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= '))';
        }

        if (isset($filter->is_active) && $filter->is_active && $filter->is_active == TRUE)
            $this->db->where('status=', 0);
        else
            $this->db->where('status=', 1);

        $select_column = array($tbl_org . ".id", $tbl_org . ".name", $tbl_org . ".status", "tbl_organisation_phone.phone", "CONCAT(tbl_organisation_address.street,' ',tbl_organisation_address.city, ' ', tbl_organisation_address.postal) AS address", "tbl_suburb_state.state");

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_org);

        $this->db->join('tbl_organisation_address', 'tbl_organisation_address.organisationId =  tbl_organisation.id AND tbl_organisation_address.primary_address = 1', 'left');
        $this->db->join('tbl_organisation_phone', 'tbl_organisation_phone.organisationId =  tbl_organisation.id AND tbl_organisation_phone.primary_phone = 1', 'left');
        $this->db->join('tbl_suburb_state', 'tbl_suburb_state.stateId =  tbl_organisation_address.state AND tbl_organisation_phone.primary_phone = 1', 'left');
        $this->db->group_by($orderBy);
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        if (!empty($sWhere))
            $this->db->where($sWhere, null, false);

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                $this->db->select('id');
                $this->db->where('id!=', $val->id);
                $this->db->where('parent_org=', $val->id);
                $this->db->where('archive', '0');
                $query_sub_org = $this->db->get('tbl_organisation');
                $sub_org_count = $query_sub_org->num_rows();
                $sub_org_count = isset($sub_org_count) && $sub_org_count > 0 ? $sub_org_count . ' Sub-Orgs' : '0 Sub-Orgs';
                $val->sub_org_count = $sub_org_count;

                $this->db->select('id');
                $this->db->where('organisationId', $val->id);
                $query = $this->db->get('tbl_organisation_site');
                $site_count = $query->num_rows();
                $site_count = isset($site_count) && $site_count > 0 ? $site_count . ' Sites' : '0 Sites';
                $val->site_count = $site_count;
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult);
        return $return;
    }

    public function get_organisation_profile($objOrg) {
        $orgId = $objOrg->getId();
        $orgParentId = $objOrg->getParent_org();
        $org_ary = array();

        if (!empty(request_handler())) {
            $tbl_1 = TBL_PREFIX . 'organisation';
            $tbl_2 = TBL_PREFIX . 'organisation_address';


            $dt_query = $this->db->select(array($tbl_1 . '.id', $tbl_1 . '.name', $tbl_1 . '.website', $tbl_1 . '.companyId', $tbl_1 . '.enable_portal_access', $tbl_1 . '.id as ocs_id', $tbl_1 . '.logo_file', $tbl_1 . '.payroll_tax', $tbl_1 . '.gst', $tbl_1 . '.abn', $tbl_1 . '.status', $tbl_2 . '.street', $tbl_2 . '.city', $tbl_2 . '.postal', $tbl_2 . '.state', $tbl_2 . '.primary_address', 'tbl_state.name as statename', $tbl_1 . '.booking_status', $tbl_1 . '.booking_date'));

            $this->db->from($tbl_1);
            #$this->db->join($tbl_2, 'tbl_organisation_address.organisationId = tbl_organisation.id AND tbl_organisation_address.primary_address = 1', 'left');
            $this->db->join($tbl_2, 'tbl_organisation_address.organisationId = tbl_organisation.id', 'left');
            #$this->db->join($tbl_3, 'tbl_organisation_email.organisationId = tbl_organisation.id AND tbl_organisation_email.primary_email = 1', 'left');
            #$this->db->join($tbl_4, 'tbl_organisation_phone.organisationId = tbl_organisation.id AND tbl_organisation_phone.primary_phone = 1', 'left');
            $this->db->join('tbl_state', 'tbl_state.id = tbl_organisation_address.state', 'left');

            if (!empty($orgParentId)) {
                $sWhere = array($tbl_1 . '.id' => $orgParentId, $tbl_1 . '.parent_org' => $orgId);
            } else {
                $sWhere = array($tbl_1 . '.id' => $orgId);
            }

            $sWhere = array($tbl_1 . '.id' => $orgId);
            $this->db->where($sWhere, null, false);
            $query = $this->db->get();

            $x = $query->row_array();

            if (!empty($x)) {
                if (!empty($orgParentId)) {
                    $this->db->select('name');
                    $this->db->where('id=', $orgParentId);
                    $query_sub_org = $this->db->get('tbl_organisation');
                    $dataResult = $query_sub_org->row_array();

                    $org_ary['basic_detail']['parent_org'] = $dataResult['name'];
                    $org_ary['basic_detail']['type'] = 'sub_org';
                    $org_ary['basic_detail']['parent_id'] = $orgParentId;
                    $org_ary['basic_detail']['page_title'] = $dataResult['name'] . ' - ' . $x['name'];

                    $this->db->select('id');
                    #$this->db->where('organisationId',$orgParentId);
                    $this->db->where('organisationId', $orgId);
                    $query = $this->db->get('tbl_organisation_site');
                    //last_query(0);
                    $site_count = $query->num_rows();
                    $org_ary['basic_detail']['site_count'] = $site_count;
                } else {
                    $this->db->select('id');
                    $this->db->where('id!=', $orgId);
                    $this->db->where('archive', '0');
                    $this->db->where('parent_org=', $orgId);
                    $query_sub_org = $this->db->get('tbl_organisation');
                    $sub_org_count = $query_sub_org->num_rows();

                    $this->db->select('id');
                    $this->db->where('organisationId', $orgId);
                    $query = $this->db->get('tbl_organisation_site');
                    //last_query(0);
                    $site_count = $query->num_rows();

                    $org_ary['basic_detail']['site_count'] = $site_count;
                    $org_ary['basic_detail']['page_title'] = $x['name'];
                    $org_ary['basic_detail']['sub_org_count'] = $sub_org_count;
                    $org_ary['basic_detail']['type'] = 'org';
                }

                //if(isset($x['primary_address']))
                {
                    $org_ary['basic_detail']['primary_address'] = $x['street'] . ', ' . $x['city'] . ', ' . $x['postal'] . ' ' . $x['statename'];
                }

                /* if(isset($x['primary_phone']))
                  {
                  $org_ary['basic_detail']['primary_phone'] = $x['phone'];
                  }

                  if(isset($x['primary_email']))
                  {
                  $org_ary['basic_detail']['primary_email'] = $x['email'];
                  } */

                $xx = $this->basic_model->get_result('organisation_email', array('organisationId' => $orgId, 'archive' => '0'), array('email', 'primary_email'), array('primary_email', 'ASC'));
                $org_ary['basic_detail']['OrganisationEmail'] = isset($xx) && !empty($xx) ? $xx : array();
                $yy = $this->basic_model->get_result('organisation_phone', array('organisationId' => $orgId, 'archive' => '0'), array('phone', 'primary_phone'), array('primary_phone', 'ASC'));
                $org_ary['basic_detail']['OrganisationPh'] = isset($yy) && !empty($yy) ? $yy : array();

                $ocsid = $x['ocs_id'];
                $org_ary['basic_detail']['companyId'] = $x['companyId'];
                $org_ary['basic_detail']['abn'] = $x['abn'];
                $org_ary['basic_detail']['name'] = $x['name'];
                $org_ary['basic_detail']['ocs_id'] = $x['ocs_id'];
                if (file_exists(ORG_UPLOAD_PATH . $ocsid . '/' . $x['logo_file']) && $x['logo_file'] != '')
                    $profile_img = base_url() . ORG_UPLOAD_PATH . $ocsid . '/' . $x['logo_file'];
                else
                    $profile_img = '/assets/images/Members_icons/boy.svg';

                $org_ary['basic_detail']['logo_file'] = $profile_img;
                $org_ary['basic_detail']['enable_portal_access'] = $x['enable_portal_access'];
                $org_ary['basic_detail']['status'] = $x['status'];
                $org_ary['basic_detail']['booking_status'] = isset($x['booking_status']) && $x['booking_status'] == '1' ? TRUE : FALSE;
                $org_ary['basic_detail']['booking_date'] = isset($x['booking_date']) && $x['booking_date'] != '0000-00-00' ? $x['booking_date'] : '';

                $org_ary['basic_detail']['street'] = $x['street'];
                $org_ary['basic_detail']['city'] = $x['city'];
                $org_ary['basic_detail']['postal'] = $x['postal'];
                $org_ary['basic_detail']['statename'] = $x['statename'];
                $org_ary['basic_detail']['state'] = $x['state'];
                $org_ary['basic_detail']['website'] = $x['website'];
                $org_ary['basic_detail']['payroll_tax'] = $x['payroll_tax'];
                $org_ary['basic_detail']['gst'] = $x['gst'];
                $org_ary['basic_detail']['city'] = array('value' => $x['city'], 'label' => $x['city']);
                return array('status' => true, 'data' => $org_ary);
            }
            else {
                return array('status' => false);
            }
        }
    }

    public function get_organisation_about($objOrg) {
        $orgId = $objOrg->getId();
        if (!empty(request_handler())) {
            $tbl_1 = TBL_PREFIX . 'organisation';
            $tbl_2 = TBL_PREFIX . 'organisation_address';
            $tbl_3 = TBL_PREFIX . 'organisation_email';
            $tbl_4 = TBL_PREFIX . 'organisation_phone';

            $dt_query = $this->db->select(array($tbl_1 . '.name', $tbl_1 . '.companyId', $tbl_1 . '.enable_portal_access', $tbl_1 . '.id as ocs_id', $tbl_1 . '.logo_file', $tbl_1 . '.payroll_tax', $tbl_1 . '.gst', $tbl_1 . '.abn', $tbl_1 . '.status', $tbl_2 . '.street', $tbl_2 . '.city', $tbl_2 . '.postal', $tbl_2 . '.state', $tbl_2 . '.primary_address', 'tbl_state.name as statename', $tbl_3 . '.email', $tbl_4 . '.primary_phone', $tbl_4 . '.phone', $tbl_3 . '.primary_email'));

            $this->db->from($tbl_1);
            $this->db->join($tbl_2, 'tbl_organisation_address.organisationId = tbl_organisation.id AND tbl_organisation_address.primary_address = 1', 'left');
            $this->db->join($tbl_3, 'tbl_organisation_email.organisationId = tbl_organisation.id AND tbl_organisation_email.primary_email = 1', 'left');
            $this->db->join($tbl_4, 'tbl_organisation_phone.organisationId = tbl_organisation.id AND tbl_organisation_phone.primary_phone = 1', 'left');
            $this->db->join('tbl_state', 'tbl_state.id = tbl_organisation_address.state', 'left');
            $sWhere = array($tbl_1 . '.id' => $orgId);
            $this->db->where($sWhere, null, false);
            $query = $this->db->get();
            $x = $query->row_array();

            $user_ary = array();
            $user_ary['basic_detail']['name'] = $x['name'];
            $user_ary['basic_detail']['payroll_tax'] = $x['payroll_tax'];
            $user_ary['basic_detail']['ocs_id'] = $x['ocs_id'];
            $user_ary['basic_detail']['enable_portal_access'] = $x['enable_portal_access'];
            $user_ary['basic_detail']['companyId'] = $x['companyId'];
            $user_ary['basic_detail']['logo_file'] = $x['logo_file'];
            $user_ary['basic_detail']['gst'] = $x['gst'];
            $user_ary['basic_detail']['abn'] = $x['abn'];

            if (isset($x['primary_address'])) {
                $user_ary['basic_detail']['primary_address'] = $x['street'] . ', ' . $x['city'] . ', ' . $x['postal'] . ' ' . $x['statename'];
            }

            if (isset($x['primary_phone'])) {
                $user_ary['basic_detail']['primary_phone'] = $x['phone'];
            }

            if (isset($x['primary_email'])) {
                $user_ary['basic_detail']['primary_email'] = $x['email'];
            }

            return $user_ary;
        }
    }

    public function get_sub_org($objOrg) {

        $orgId = $objOrg->getId();
        $orgStatus = $objOrg->getStatus();
        $orgText_search = $objOrg->gettext_search();
        $limit = $objOrg->getLimit();
        $page = $objOrg->getPage();
        $tbl_phone = 'organisation_phone';
        $tbl_email = 'organisation_email';
        $tbl_phone_prefix = TBL_PREFIX . $tbl_phone;
        $tbl_email_prefix = TBL_PREFIX . $tbl_email;
        if (!empty(request_handler())) {
            $tbl_1 = TBL_PREFIX . 'organisation';
            $tbl_2 = TBL_PREFIX . 'organisation_address';
            $tbl_3 = TBL_PREFIX . 'organisation_email';
            $tbl_4 = TBL_PREFIX . 'organisation_phone';
            $tbl_5 = TBL_PREFIX . 'organisation_all_contact';
            $select_column = array($tbl_1 . '.name', $tbl_1 . '.id as ocs_id', $tbl_1 . '.status', $tbl_2 . '.id as address_id', $tbl_2 . '.street', $tbl_2 . '.city', $tbl_2 . '.postal', $tbl_2 . '.state', $tbl_2 . '.primary_address', 'tbl_state.name as statename', $tbl_3 . '.email', $tbl_4 . '.primary_phone', $tbl_4 . '.phone', $tbl_3 . '.primary_email', $tbl_5 . '.name as key_contact', $tbl_5 . '.position', $tbl_5 . '.id as all_contact_id', $tbl_3 . '.id as email_id', $tbl_4 . '.id as ph_id');
            $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
            $this->db->from($tbl_1);
            $this->db->join($tbl_2, "tbl_organisation_address.organisationId = tbl_organisation.id AND tbl_organisation_address.primary_address = 1", "left");
            $this->db->join($tbl_3, "tbl_organisation_email.organisationId = tbl_organisation.id AND tbl_organisation_email.primary_email = 1 and tbl_organisation_email.archive='0'", "left");
            $this->db->join($tbl_4, "tbl_organisation_phone.organisationId = tbl_organisation.id AND tbl_organisation_phone.primary_phone = 1 and tbl_organisation_phone.archive='0'", "left");
            $this->db->join($tbl_5, "tbl_organisation_all_contact.organisationId = tbl_organisation.id AND tbl_organisation_all_contact.type = 3 AND tbl_organisation_all_contact.archive = '0'", "left", TRUE);
            $this->db->join("tbl_state", "tbl_state.id = tbl_organisation_address.state", "left");

            $this->db->where($tbl_1 . '.archive=', '0');
            $this->db->where($tbl_1 . '.status=', $orgStatus);
            $this->db->where($tbl_1 . '.parent_org=', $orgId);
            $this->db->where($tbl_1 . '.id!=', $orgId);

            if (!empty($orgText_search)) {
                #OCPID,Org Name,Primary Contact Name,Sub Org Name,  Head Office Address
                $this->db->group_start();
                $src_columns = array($tbl_1 . ".id", $tbl_1 . ".name", "CONCAT(tbl_organisation_all_contact.name,' ',tbl_organisation_all_contact.lastname)", $tbl_2 . ".street", $tbl_2 . ".city", $tbl_2 . ".postal", $tbl_2 . ".state",);

                for ($i = 0; $i < count($src_columns); $i++) {
                    $column_search = $src_columns[$i];
                    if (strstr($column_search, "as") !== false) {
                        $serch_column = explode(" as ", $column_search);
                        $this->db->or_like($serch_column[0], $orgText_search);
                    } else {
                        $this->db->or_like($column_search, $orgText_search);
                    }
                }
                $this->db->group_end();
            }

            $this->db->group_by($tbl_1 . '.id');
            $this->db->limit($limit, ($page * $limit));
            $query = $this->db->get();
            $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
            if ($dt_filtered_total % $limit == 0) {
                $dt_filtered_total = ($dt_filtered_total / $limit);
            } else {
                $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
            }
            $x = $query->result_array();
            $org_ary = array();
            if (!empty($x)) {
                foreach ($x as $key => $value) {
                    $temp['name'] = $value['name'];
                    $temp['status'] = $value['status'];
                    $temp['ocs_id'] = $value['ocs_id'];
                    $temp['key_contact'] = $value['key_contact'] . '-' . $value['position'];
                    $temp['key_contact_label'] = $value['key_contact'];
                    $temp['position'] = $value['position'];

                    $temp['address_id'] = $value['address_id'];
                    $temp['all_contact_id'] = $value['all_contact_id'];
                    $temp['email_id'] = $value['email_id'];
                    $temp['ph_id'] = $value['ph_id'];

                    if (isset($value['primary_address'])) {
                        $temp['primary_address'] = $value['street'] . ', ' . $value['city'] . ', ' . $value['postal'] . ' ' . $value['statename'];
                        $temp['street'] = $value['street'];
                        $temp['city'] = array('value' => $value['city'], 'label' => $value['city']);
                        $temp['postal'] = $value['postal'];
                        $temp['state'] = $value['state'];
                    }

                    if (isset($value['primary_phone'])) {
                        $temp['primary_phone'] = $value['phone'];
                    }

                    if (isset($value['primary_email'])) {
                        $temp['primary_email'] = $value['email'];
                    }
                    /* Get organisatin phone no */
                    $zPhone = $this->Basic_model->get_result($tbl_phone, array($tbl_phone_prefix . '.organisationId' => $value['ocs_id'], $tbl_phone_prefix . '.archive' => '0'), $columns = array($tbl_phone_prefix . '.phone', $tbl_phone_prefix . '.primary_phone', $tbl_phone_prefix . '.id'));
                    if (!empty($zPhone)) {
                        $ph_temp = array();
                        foreach ($zPhone as $key => $valPh) {
                            $ph_temp[] = array('phone' => $valPh->phone, 'id' => $valPh->id, 'primary_phone' => $valPh->primary_phone);
                        }
                        $temp['OrganisationPh'] = $ph_temp;
                    }

                    /* Get organisatin email no */
                    $zMail = $this->Basic_model->get_result($tbl_email, array($tbl_email_prefix . '.organisationId' => $value['ocs_id'], $tbl_email_prefix . '.archive' => '0'), $columns = array($tbl_email_prefix . '.email', $tbl_email_prefix . '.primary_email', $tbl_email_prefix . '.id'));
                    if (!empty($zMail)) {
                        $email_temp = array();
                        foreach ($zMail as $key => $valmail) {
                            $email_temp[] = array('email' => $valmail->email, 'id' => $valmail->id, 'primary_email' => $valmail->primary_email);
                        }
                        $temp['OrganisationEmail'] = $email_temp;
                    }
                    $org_ary[] = $temp;
                }
            }
            return ['count' => $dt_filtered_total, 'data' => $org_ary];
        }
    }

    public function get_org_sites_old($objOrg) {
        $orgId = $objOrg->getId();
        $orgArchive = $objOrg->getArchive();
        $orgText_search = $objOrg->gettext_search();
        if (!empty(request_handler())) {
            $tbl_1 = TBL_PREFIX . 'organisation_site';
            $tbl_3 = TBL_PREFIX . 'organisation_site_key_contact';

            /* $tbl_4 = TBL_PREFIX . 'organisation_phone'; */

            $dt_query = $this->db->select(array($tbl_1 . '.site_name', $tbl_1 . '.id as ocs_id', $tbl_1 . '.street', $tbl_1 . '.city', $tbl_1 . '.postal', $tbl_1 . '.state', 'tbl_state.name as statename', $tbl_3 . '.position', "CONCAT(tbl_organisation_site_key_contact.firstname,' ',tbl_organisation_site_key_contact.lastname) AS key_contact"));

            $this->db->from($tbl_1);
            $this->db->join($tbl_3, 'tbl_organisation_site.id = tbl_organisation_site_key_contact.siteId', 'left');
            #$this->db->join($tbl_4, 'tbl_organisation_phone.organisationId = tbl_organisation_site.organisationId AND tbl_organisation_phone.primary_phone = 1', 'left');
            $this->db->join('tbl_state', 'tbl_state.id = tbl_organisation_site.state', 'left');
            $this->db->where($tbl_1 . '.archive=', "$orgArchive");
            $this->db->where($tbl_1 . '.organisationId=', $orgId);


            if (!empty($orgText_search)) {
                #Search by all site table 
                $this->db->group_start();
                $src_columns = array($tbl_1 . '.site_name', $tbl_1 . '.id as ocs_id', $tbl_1 . '.street', $tbl_1 . '.city', $tbl_1 . '.postal', $tbl_1 . '.state', 'tbl_state.name as statename');

                for ($i = 0; $i < count($src_columns); $i++) {
                    $column_search = $src_columns[$i];
                    if (strstr($column_search, "as") !== false) {
                        $serch_column = explode(" as ", $column_search);
                        $this->db->or_like($serch_column[0], $orgText_search);
                    } else {
                        $this->db->or_like($column_search, $orgText_search);
                    }
                }
                $this->db->group_end();
            }

            $query = $this->db->get();
            #last_query();
            $x = $query->result_array();

            $site_ary = array();
            foreach ($x as $key => $value) {
                $temp['site_name'] = $value['site_name'];
                $temp['title'] = $value['site_name'];
                $temp['ocs_id'] = $value['ocs_id'];
                $temp['site_id'] = $value['ocs_id'];
                $temp['site_address'] = $value['street'];
                $temp['state'] = $value['state'];
                $temp['postal'] = $value['postal'];
                $temp['address'] = $value['street'] . ', ' . $value['city'] . ', ' . $value['postal'] . ' ' . $value['statename'];
                $temp['key_contact'] = $value['key_contact'] . ' - ' . $value['position'];


                $temp['key_contact_label'] = $value['key_contact'];
                $temp['position'] = $value['position'];
                $temp['street'] = $value['street'];
                $temp['city'] = array('value' => $value['city'], 'label' => $value['city']);

                #$temp['email_id'] = $value['email_id'];
                #$temp['ph_id'] = $value['ph_id'];
                #if(isset($value['primary_phone']))
                {
                    #$temp['primary_phone'] = $value['phone'];
                }

                #if(isset($value['primary_email']))
                {
                    #$temp['primary_email'] = $value['email'];
                }
                $site_ary[] = $temp;
            }
            return $site_ary;
        }
    }

    public function get_org_sites($objOrg) {
        $orgId = $objOrg->getId();
        $orgArchive = $objOrg->getArchive();
        $orgText_search = $objOrg->gettext_search();
        $limit = $objOrg->getLimit();
        $page = $objOrg->getPage();
        if (!empty(request_handler())) {
            $tbl_1 = TBL_PREFIX . 'organisation_site';
            $select_column = array($tbl_1 . '.site_name', $tbl_1 . '.id as ocs_id', $tbl_1 . '.street', $tbl_1 . '.city', $tbl_1 . '.postal', $tbl_1 . '.state', 'tbl_state.name as statename', $tbl_1 . '.abn');
            $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
            $this->db->from($tbl_1);
            $this->db->join('tbl_state', 'tbl_state.id = tbl_organisation_site.state', 'left');
            $this->db->where($tbl_1 . '.archive=', "$orgArchive");
            $this->db->where($tbl_1 . '.organisationId=', $orgId);

            if (!empty($orgText_search)) {
                #Search by all site table 
                $this->db->group_start();
                $src_columns = array($tbl_1 . '.site_name', $tbl_1 . '.id as ocs_id', $tbl_1 . '.street', $tbl_1 . '.city', $tbl_1 . '.postal', $tbl_1 . '.state', 'tbl_state.name as statename');

                for ($i = 0; $i < count($src_columns); $i++) {
                    $column_search = $src_columns[$i];
                    if (strstr($column_search, "as") !== false) {
                        $serch_column = explode(" as ", $column_search);
                        $this->db->or_like($serch_column[0], $orgText_search);
                    } else {
                        $this->db->or_like($column_search, $orgText_search);
                    }
                }
                $this->db->group_end();
            }
            $this->db->limit($limit, ($page * $limit));
            $query = $this->db->get();
            $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
            if ($dt_filtered_total % $limit == 0) {
                $dt_filtered_total = ($dt_filtered_total / $limit);
            } else {
                $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
            }
            $x = $query->result_array();

            $site_ary = array();
            if (!empty($x)) {
                foreach ($x as $key => $value) {
                    $site_id = $value['ocs_id'];
                    $temp['mode'] = 'edit';
                    $temp['orgId'] = $orgId;
                    $temp['title'] = $value['site_name'];
                    $temp['site_type'] = 1;
                    $temp['ocs_id'] = $site_id;
                    $temp['site_id'] = $site_id;
                    $temp['site_address'] = $value['street'];
                    $temp['state'] = $value['state'];
                    $temp['postal'] = $value['postal'];
                    $temp['abn'] = $value['abn'];
                    $temp['address'] = $value['street'] . ', ' . $value['city'] . ', ' . $value['postal'] . ' ' . $value['statename'];
                    $temp['city'] = array('value' => $value['city'], 'label' => $value['city'], 'postcode' => $value['postal']);

                    /* Get organisatin phone no */
                    $tbl_6 = 'tbl_organisation_site_phone';
                    $z = $this->Basic_model->get_result('organisation_site_phone', array($tbl_6 . '.siteId' => $site_id, $tbl_6 . '.archive' => '0'), $columns = array($tbl_6 . '.phone', $tbl_6 . '.primary_phone', $tbl_6 . '.id'));

                    if (!empty($z)) {
                        $ph_temp = array();
                        foreach ($z as $key => $valPh) {
                            $ph_temp[] = array('phone' => $valPh->phone, 'id' => $valPh->id, 'primary_phone' => $valPh->primary_phone);
                        }
                        $temp['SitePhone'] = $ph_temp;
                    }
                    /* Get organisatin email no */
                    $tbl_7 = 'tbl_organisation_site_email';
                    $zMail = $this->Basic_model->get_result('organisation_site_email', array($tbl_7 . '.siteId' => $site_id, $tbl_7 . '.archive' => '0'), $columns = array($tbl_7 . '.email', $tbl_7 . '.primary_email', $tbl_7 . '.id'));
                    if (!empty($zMail)) {
                        $email_temp = array();
                        foreach ($zMail as $key => $valmail) {
                            $email_temp[] = array('email' => $valmail->email, 'id' => $valmail->id, 'primary_email' => $valmail->primary_email);
                        }
                        $temp['SiteEmail'] = $email_temp;
                    }
                    /* Site multiple key contact */
                    /* $tbl_8 = 'tbl_organisation_site_key_contact';
                      $keycontacts_rows = $this->Basic_model->get_result('organisation_site_key_contact',array($tbl_8.'.siteId'=>$site_id,$tbl_8.'.archive'=>'0'),$columns=array($tbl_8.'.position',$tbl_8.'.department',$tbl_8.'.id',$tbl_8.'.street',$tbl_8.'.city',$tbl_8.'.postal',$tbl_8.'.lastname',$tbl_8.'.firstname',$tbl_8.'.state',"CONCAT(tbl_organisation_site_key_contact.firstname,' ',tbl_organisation_site_key_contact.lastname) AS key_contact"));
                      #last_query();
                      $kEmail_temp = array();
                      $kPhone_temp = array();
                      if(!empty($keycontacts_rows))
                      {
                      $kContacts_temp = array();
                      foreach ($keycontacts_rows as $key => $valcontact)
                      {
                      $kContEmail = $this->Basic_model->get_result('organisation_site_key_contact_email',array('contactId'=>$valcontact->id,'archive'=>'0'),$columns=array('id','email','primary_email'));
                      if(!empty($kContEmail))
                      {
                      foreach ($kContEmail as $key => $ContEmail)
                      {
                      $kEmail_temp[] = array('email'=>$ContEmail->email,'id'=>$ContEmail->id,'primary_email'=>$ContEmail->primary_email);
                      }
                      }

                      $kContPhone = $this->Basic_model->get_result('organisation_site_key_contact_phone',array('contactId'=>$valcontact->id,'archive'=>'0'),$columns=array('id','phone','primary_phone'));
                      if(!empty($kContPhone))
                      {
                      foreach ($kContPhone as $key => $ContPhone)
                      {
                      $kPhone_temp[] = array('phone'=>$ContPhone->phone,'id'=>$ContPhone->id,'primary_phone'=>$ContPhone->primary_phone);
                      }
                      }

                      $kContacts_temp[] = array('key_contact_label'=>$valcontact->key_contact,'key_contact'=> $valcontact->key_contact.' - '.$valcontact->position,'lastname'=>$valcontact->lastname,'id'=>$valcontact->id,'firstname'=>$valcontact->firstname,'position'=>$valcontact->position,'department'=>$valcontact->department,'sitePh'=>$kPhone_temp,'siteMail'=>$kEmail_temp);
                      }
                      $temp['siteDetails'] = $kContacts_temp;
                      }
                      else
                      {
                      $temp['siteDetails'] = array(array('key_contact'=>'','sitePh'=>array(),'siteMail'=>array()));
                      } */
                    $temp['siteDetails'] = $this->get_site_cotact_multiple($site_id);
                    $site_ary[] = $temp;
                }
            }
            return ['data' => $site_ary, 'count' => $dt_filtered_total];
        }
    }

    public function update_sub_org($reqData) {
        if (!empty($reqData)) {
            /* organization name not be update */
            $suburb = $reqData['city']->value;
            $org_adr = array('street' => $reqData['street'], 'city' => $suburb, 'postal' => $reqData['postal'], 'state' => $reqData['state']);

            if (isset($reqData['address_id'])) {
                $this->basic_model->update_records('organisation_address', $org_adr, array('id' => $reqData['address_id']));
            } else {
                $org_adr['primary_address'] = 1;
                $org_adr['organisationId'] = $reqData['ocs_id'];
                $this->Basic_model->insert_records('organisation_address', $org_adr);
            }

            /*  $org_position = array('name'=>$reqData['key_contact_label'],'position'=>$reqData['position']);

              if(isset($reqData['all_contact_id'])){
              $this->basic_model->update_records('organisation_all_contact', $org_position, array('id'=>$reqData['all_contact_id']));
              }
              else{
              $org_position['type  '] = 3;
              $org_position['organisationId'] = $reqData['ocs_id'];
              $this->Basic_model->insert_records('organisation_all_contact', $org_position);
              } */

            if (isset($reqData['OrganisationPh'])) {
                $organisation_ph = array();
                if (!empty($reqData['OrganisationPh'])) {
                    /* First delete old record and then insert new record */
                    $this->Basic_model->update_records('organisation_phone', array('archive' => '1'), array('organisationId' => $reqData['ocs_id']));
                    foreach ($reqData['OrganisationPh'] as $kp => $val) {
                        $val = is_object($val) ? (array) $val : $val;
                        $organisation_ph = array('phone' => $val['phone']);
                        if (isset($val['id']) && !empty($val['id'])) {
                            $this->Basic_model->update_records('organisation_phone', array_merge(array('archive' => '0'), $organisation_ph), array('id' => $val['id']));
                        } else {
                            $organisation_ph['archive'] = '0';
                            $organisation_ph['primary_phone'] = $kp == 0 ? '1' : '2';
                            $organisation_ph['organisationId'] = $reqData['ocs_id'];
                            $this->Basic_model->insert_records('organisation_phone', $organisation_ph);
                        }
                    }
                }
            }

            if (isset($reqData['OrganisationEmail'])) {
                $organisation_email = array();
                if (!empty($reqData['OrganisationEmail'])) {
                    /* First delete old record and then insert new record */
                    $this->Basic_model->update_records('organisation_email', array('archive' => '1'), array('organisationId' => $reqData['ocs_id']));
                    foreach ($reqData['OrganisationEmail'] as $ke => $val) {
                        $val = is_object($val) ? (array) $val : $val;
                        $organisation_email = array('email' => $val['email']);
                        if (isset($val['id']) && !empty($val['id'])) {
                            $this->Basic_model->update_records('organisation_email', array_merge(array('archive' => '0'), $organisation_email), array('id' => $val['id']));
                        } else {
                            $organisation_email['archive'] = '0';
                            $organisation_email['primary_email'] = $ke == 0 ? '1' : '2';
                            $organisation_email['organisationId'] = $reqData['ocs_id'];
                            $this->Basic_model->insert_records('organisation_email', $organisation_email);
                        }
                    }
                }
            }


            /* $this->basic_model->update_records('organisation_email', $org_email_data, array('id'=>$reqData['email_id']));
              $this->basic_model->update_records('organisation_phone', $org_ph_data, array('id'=>$reqData['ph_id'])); */
        }
    }

    public function get_org_contact($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orgId = $reqData->orgId;

        $orderBy = '';
        $direction = '';
        $tbl_org_contact = TBL_PREFIX . 'organisation_all_contact';
        $src_columns = array();

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_org_contact . '.id';
            $direction = 'DESC';
        }

        if (isset($filter) && !empty($filter))
            $is_archive = $filter->archive;
        else
            $is_archive = 0;

        $where = '';
        $where = $tbl_org_contact . ".archive = '$is_archive'  AND " . $tbl_org_contact . ".organisationId = $orgId ";
        $sWhere = $where;

        $select_column = array($tbl_org_contact . ".name as firstname", $tbl_org_contact . ".lastname", $tbl_org_contact . ".id", $tbl_org_contact . ".position", $tbl_org_contact . ".department", "tbl_organisation_all_contact_phone.phone", "tbl_organisation_all_contact_phone.phone", "tbl_organisation_all_contact_email.email", "tbl_organisation_all_contact_email.primary_email", "tbl_organisation_all_contact_phone.primary_phone", "CONCAT(tbl_organisation_all_contact.name,' ',tbl_organisation_all_contact.lastname) AS name", $tbl_org_contact . ".type", $tbl_org_contact . ".type as contact_type");

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_org_contact);

        $this->db->join('tbl_organisation_all_contact_email', "tbl_organisation_all_contact_email.contactId =  tbl_organisation_all_contact.id AND tbl_organisation_all_contact_email.primary_email = 1 AND tbl_organisation_all_contact_email.archive = '0'", 'left');

        $this->db->join('tbl_organisation_all_contact_phone', "tbl_organisation_all_contact_phone.contactId =  tbl_organisation_all_contact.id AND tbl_organisation_all_contact_phone.primary_phone = 1 AND tbl_organisation_all_contact_phone.archive = '0'", 'left');

        $this->db->group_by($orderBy);
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        if (!empty($sWhere))
            $this->db->where($sWhere, null, false);

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        //last_query();
        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                $val->key_contact = $val->position . ' - ' . $val->name;
                $val->firstname = $val->firstname;
                $val->lastname = $val->lastname;
                $val->key_contact_label = $val->key_contact;
                $val->position = $val->position;

                $xx = $this->basic_model->get_result('organisation_all_contact_email', array('contactId' => $val->id, 'archive' => '0'), array('email', 'primary_email'), array('primary_email', 'ASC'));
                $val->OrganisationEmail = isset($xx) && !empty($xx) ? $xx : array();
                $yy = $this->basic_model->get_result('organisation_all_contact_phone', array('contactId' => $val->id, 'archive' => '0'), array('phone', 'primary_phone'), array('primary_phone', 'ASC'));
                $val->OrganisationPh = isset($yy) && !empty($yy) ? $yy : array();
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult);
        return $return;
    }

    public function get_org_fms($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orgId = $reqData->orgId;
        $orderBy = '';
        $direction = '';
        $tbl_fms = TBL_PREFIX . 'fms_case';

        $src_columns = array();

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_fms . '.id';
            $direction = 'DESC';
        }


        if (isset($filter)) {
            if ($filter->archive == 1) {
                $this->db->where('tbl_organisation.parent_org=', $orgId); //sub org
            } else {
                $this->db->where('tbl_fms_case.initiated_by=', $orgId); //for organisation
            }
        }

        $where = '';
        $where = $tbl_fms . ".initiated_type = 3";
        $sWhere = $where;

        /* if (!empty($filter->srch_box)) {
          $src_columns = array($tbl_fms . ".id as fmsid", $tbl_fms . ".Initiator_first_name",$tbl_fms . ".Initiator_last_name",$tbl_fms.".initiated_by","CONCAT(tbl_member.firstname,' ',tbl_member.middlename,' ',tbl_member.lastname)", "CONCAT(tbl_member.firstname,' ',tbl_member.lastname)","CONCAT(tbl_participant.firstname,' ',tbl_participant.middlename,' ',tbl_participant.lastname)","CONCAT(tbl_participant.firstname,' ',tbl_participant.lastname)","tbl_member.id as member_ocs_id","tbl_participant.id as participant_ocs_id");

          $where = $where . " AND (";
          $sWhere = " (" . $where;
          for ($i = 0; $i < count($src_columns); $i++) {
          $column_search = $src_columns[$i];
          if (strstr($column_search, "as") !== false) {
          $serch_column = explode(" as ", $column_search);
          $sWhere .= $serch_column[0] . " LIKE '%" . $this->db->escape_like_str($filter->srch_box) . "%' OR ";
          } else {
          $sWhere .= $column_search . " LIKE '%" . $this->db->escape_like_str($filter->srch_box) . "%' OR ";
          }
          }
          $sWhere = substr_replace($sWhere, "", -3);
          $sWhere .= '))';
          } */

        $select_column = array($tbl_fms . ".id", $tbl_fms . ".event_date", "tbl_fms_case_reason.description", $tbl_fms . ".initiated_type", $tbl_fms . ".Initiator_first_name", $tbl_fms . ".Initiator_last_name", $tbl_fms . ".initiated_by", $tbl_fms . ".created", "tbl_fms_case_category.categoryId", "tbl_fms_case_all_category.name as case_category");

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_fms);
        $this->db->join('tbl_fms_case_reason', 'tbl_fms_case_reason.caseId = ' . $tbl_fms . '.id', 'left');
        $this->db->join('tbl_organisation', 'tbl_organisation.id = ' . $tbl_fms . '.initiated_by', 'left');

        $this->db->join('tbl_fms_case_category', 'tbl_fms_case_category.caseId = ' . $tbl_fms . '.id', 'left');
        $this->db->join('tbl_fms_case_all_category', 'tbl_fms_case_all_category.id = tbl_fms_case_category.categoryId', 'left');

        $this->db->group_by('tbl_fms_case.id');
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $this->db->where($sWhere, null, false);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        //last_query(); die;
        $dt_filtered_total = $all_count = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();
        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                $val->category = $val->case_category;
                $init_by_id = $val->initiated_by;
                $tbl = 'tbl_organisation';
                $this->db->select("name");
                $this->db->where('id', $init_by_id);
                $emp_qry = $this->db->get($tbl);
                $row = $emp_qry->row_array();
                $val->initiated_by = $row['name'];
                $val->timelapse = time_ago_in_php($val->created);
                $val->against_for = get_fms_against_name($val->id);
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'all_count' => $all_count);
        return $return;
    }

    public function add_site_contact($reqData, $contact_id) {
        if (!empty($reqData)) {
            /* First delete old record and then insert new record */
            $this->Basic_model->update_records('organisation_site_key_contact_phone', array('archive' => '1'), array('contactId' => $contact_id));
            $this->Basic_model->update_records('organisation_site_key_contact_email', array('archive' => '1'), array('contactId' => $contact_id));


            if (!empty($reqData['OrganisationPh'])) {
                foreach ($reqData['OrganisationPh'] as $key => $value) {
                    if ($key == 0) {
                        //primary details
                        $data_ph = array('phone' => $value->phone, 'primary_phone' => '1', 'contactId' => $contact_id);
                    } else {
                        $data_ph['phone'] = $value->phone;
                        $data_ph['primary_phone'] = '2';
                        $data_ph['contactId'] = $contact_id;
                    }
                    $main_ph[] = $data_ph;
                }
            }

            if (!empty($reqData['OrganisationEmail'])) {
                foreach ($reqData['OrganisationEmail'] as $key => $value) {
                    if ($key == 0) {
                        //primary details
                        $data_email = array('email' => $value->email, 'primary_email' => '1', 'contactId' => $contact_id);
                    } else {
                        $data_email['email'] = $value->email;
                        $data_email['primary_email'] = '2';
                        $data_email['contactId'] = $contact_id;
                    }
                    $main_email[] = $data_email;
                }
            }


            if (!empty($main_ph)) {
                $this->Basic_model->insert_records('organisation_site_key_contact_phone', $main_ph, TRUE);
            }
            if (!empty($main_email)) {
                $this->Basic_model->insert_records('organisation_site_key_contact_email', $main_email, TRUE);
            }
        }
    }

    public function get_contact_detail($contact_id) {
        error_reporting(0);
        $mail = $this->basic_model->get_result('organisation_all_contact_email', array('contactId' => $contact_id, 'archive' => '0'), array('email', 'primary_email'), array('primary_email', 'ASC'));
        $ph = $this->basic_model->get_result('organisation_all_contact_phone', array('contactId' => $contact_id, 'archive' => '0'), array('phone', 'primary_phone'), array('primary_phone', 'ASC'));
        $temp = array();
        $main_ary = array();
        if (!empty($mail)) {
            foreach ($mail as $key => $value) {
                $temp['email'] = $value->email;
                $temp['phone'] = isset($key) ? $ph[$key]->phone : '';
                $main_ary[] = $temp;
            }
        }
        return $main_ary;
    }

    public function get_org_docs($objMember) {
        if (!empty(request_handler('access_organization'))) {
            $request = request_handler();
            $request_data = $request->data;
            $org_id = $request_data->org_id;
            $view_by = $request_data->view_by;

            if ($view_by == '1')
                $this->db->where('archive', '0');
            else
                $this->db->where('archive', '1');

            $tbl_1 = TBL_PREFIX . 'organisation_docs';
            $dt_query = $this->db->select(array($tbl_1 . '.id', $tbl_1 . '.expiry', $tbl_1 . '.title', $tbl_1 . '.created', $tbl_1 . '.filename', "DATEDIFF(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(expiry)), '%Y-%m-%d'),CURDATE()) AS days_left"));
            $sWhere = array($tbl_1 . '.organisationId' => $org_id);
            $this->db->from($tbl_1);
            $this->db->where($sWhere, null, false);
            $query = $this->db->get();
            #last_query();
            $x = $query->result_array();
            $data_ary = array();
            if (!empty($x)) {
                foreach ($x as $key => $value) {
                    $temp_ary['id'] = $value['id'];
                    $temp_ary['days_left'] = $value['days_left'];
                    $temp_ary['expiry'] = date('d-m-Y', strtotime($value['expiry']));
                    $temp_ary['title'] = $value['title'];
                    $temp_ary['created'] = $value['created'];
                    $temp_ary['filename'] = $value['filename'];
                    if ($value['days_left'] < 0 || $value['days_left'] < 5)
                        $class_name = 'text_dark_Pink';
                    else if ($value['days_left'] > 0 && $value['days_left'] < 30)
                        $class_name = 'text_dark_yello';
                    else
                        $class_name = '';

                    $temp_ary['class_name'] = $class_name;
                    $data_ary[] = $temp_ary;
                }
            }

            if (!empty($data_ary))
                return $data_ary;
            else
                return array();
        }
    }

    public function get_house_profile($objOrg) {
        $houseId = $objOrg->getId();
        $orgId = $objOrg->getOrganisationId();
        $org_ary = array();

        if (!empty(request_handler())) {
            $tbl_1 = TBL_PREFIX . 'organisation_site';

            $tbl_3 = TBL_PREFIX . 'organisation_site_email';
            $tbl_4 = TBL_PREFIX . 'organisation_site_phone';



            $this->db->select(array($tbl_1 . '.organisationId', $tbl_1 . '.site_name', $tbl_1 . '.enable_portal_access', $tbl_1 . '.id as ocs_id', $tbl_1 . '.logo_file', $tbl_1 . '.status', $tbl_1 . '.street', $tbl_1 . '.city', $tbl_1 . '.postal', $tbl_1 . '.state', 'tbl_state.name as statename', $tbl_3 . '.email', $tbl_4 . '.phone', $tbl_1 . '.abn'));

            $this->db->from($tbl_1);

            $this->db->join($tbl_3, 'tbl_organisation_site_email.siteId = tbl_organisation_site.id AND tbl_organisation_site_email.primary_email = 1', 'left');
            $this->db->join($tbl_4, 'tbl_organisation_site_phone.siteId = tbl_organisation_site.id AND tbl_organisation_site_phone.primary_phone = 1', 'left');
            $this->db->join('tbl_state', 'tbl_state.id = tbl_organisation_site.state', 'left');
            $sWhere = array($tbl_1 . '.id' => $houseId, $tbl_1 . '.organisationId' => $orgId);
            $this->db->where($sWhere, null, false);
            $query = $this->db->get();

            $x = $query->row_array();

            $title = '';
            $subOrgName = '';
            $parentOrgName = '';
            if (!empty($x['organisationId'])) {
                $this->db->select("name,id,parent_org");
                $this->db->where('id', $orgId);
                $emp_qry = $this->db->get('tbl_organisation');

                $row = $emp_qry->row_array();
                $title_temp = $row['name'];

                if (isset($row['parent_org']) && $row['parent_org'] > 0) {
                    $id = $row['parent_org'];
                    $this->db->select("name");
                    $this->db->where('id', $id);
                    $emp_qr = $this->db->get('tbl_organisation');
                    $rw = $emp_qr->row_array();
                    $name = $rw['name'];
                    $subOrgName = $title_temp;
                    $parentOrgName = $name;

                    $title = $name . ' - ' . $title_temp;
                    $org_ary['basic_detail']['anySubOrg'] = true;
                } else {
                    $parentOrgName = $row['name'];
                    $org_ary['basic_detail']['anySubOrg'] = false;
                    $title = $title_temp;
                }
            } else {
                return ['status' => false, 'data' => []];
            }

            $ocsid = $x['ocs_id'];
            if (file_exists(ORG_UPLOAD_PATH . $ocsid . '/' . $x['logo_file']) && $x['logo_file'] != '')
                $profile_img = base_url() . ORG_UPLOAD_PATH . $ocsid . '/' . $x['logo_file'];
            else
                $profile_img = '/assets/images/Members_icons/boy.svg';

            $org_ary['basic_detail']['page_title'] = $title . ' - ' . $x['site_name'] . ' (House)';
            $org_ary['basic_detail']['parent_org'] = isset($parentOrgName) ? $parentOrgName : '';
            $org_ary['basic_detail']['type'] = 'house';
            $org_ary['basic_detail']['site_type'] = 1;
            $org_ary['basic_detail']['orgId'] = $orgId;
            $org_ary['basic_detail']['linked_to'] = isset($subOrgName) ? $subOrgName : '';

            $org_ary['basic_detail']['phone_no_ary'] = get_multiple_records('tbl_organisation_site_phone', $x['ocs_id']);
            $org_ary['basic_detail']['email_ary'] = get_multiple_records('tbl_organisation_site_email', $x['ocs_id']);
            $org_ary['basic_detail']['siteDetails'] = $this->get_site_cotact_multiple($x['ocs_id']);

            #$org_ary['basic_detail']['primary_phone'] = $x['phone'];
            #$org_ary['basic_detail']['primary_email'] = $x['email'];
            $org_ary['basic_detail']['name'] = $x['site_name'];
            $org_ary['basic_detail']['ocs_id'] = $x['ocs_id'];
            $org_ary['basic_detail']['logo_file'] = $x['logo_file'];
            $org_ary['basic_detail']['enable_portal_access'] = $x['enable_portal_access'];
            $org_ary['basic_detail']['status'] = $x['status'];
            $org_ary['basic_detail']['primary_address'] = $x['street'] . ', ' . $x['city'] . ', ' . $x['postal'] . ' ' . $x['statename'];
            $org_ary['basic_detail']['street'] = $x['street'];
            $org_ary['basic_detail']['city'] = $x['city'];
            $org_ary['basic_detail']['postal'] = $x['postal'];
            $org_ary['basic_detail']['abn'] = $x['abn'];
            $org_ary['basic_detail']['city'] = array('value' => $x['city'], 'label' => $x['city'], 'postcode' => $x['postal']);
            $org_ary['basic_detail']['state'] = $x['state'];
            $org_ary['basic_detail']['logo_file'] = $profile_img;
            return ['status' => true, 'data' => $org_ary];
        }
    }

    public function get_house_contact($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $siteId = $reqData->houseId;
        $type = isset($reqData->type) ? $reqData->type : 3;

        $orderBy = '';
        $direction = '';
        $tbl_org_contact = TBL_PREFIX . 'organisation_site_key_contact';
        $tbl_phone = 'organisation_site_key_contact_phone';
        $tbl_email = 'organisation_site_key_contact_email';
        $tbl_phone_prefix = TBL_PREFIX . $tbl_phone;
        $tbl_email_prefix = TBL_PREFIX . $tbl_email;
        $src_columns = array();

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_org_contact . '.id';
            $direction = 'DESC';
        }

        if (isset($filter) && !empty($filter))
            $is_archive = $filter->archive;
        else
            $is_archive = 0;

        $where = '';
        $where = $tbl_org_contact . ".archive = '$is_archive'  AND " . $tbl_org_contact . ".siteId = '$siteId' AND " . $tbl_org_contact . ".type=$type";
        $sWhere = $where;

        $select_column = array($tbl_org_contact . ".firstname as firstname", $tbl_org_contact . ".lastname", $tbl_org_contact . ".id", $tbl_org_contact . ".position", $tbl_org_contact . ".department", $tbl_org_contact . ".type", "tbl_organisation_site_key_contact_phone.phone", "tbl_organisation_site_key_contact_phone.phone", "tbl_organisation_site_key_contact_email.email", "tbl_organisation_site_key_contact_email.primary_email", "tbl_organisation_site_key_contact_phone.primary_phone", "CONCAT(tbl_organisation_site_key_contact.firstname,' ',tbl_organisation_site_key_contact.lastname) AS name");

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_org_contact);

        $this->db->join('tbl_organisation_site_key_contact_email', "tbl_organisation_site_key_contact_email.contactId =  tbl_organisation_site_key_contact.id AND tbl_organisation_site_key_contact_email.primary_email = 1 AND tbl_organisation_site_key_contact_email.archive = '0'", 'left');

        $this->db->join('tbl_organisation_site_key_contact_phone', "tbl_organisation_site_key_contact_phone.contactId =  tbl_organisation_site_key_contact.id AND tbl_organisation_site_key_contact_phone.primary_phone = 1 AND tbl_organisation_site_key_contact_phone.archive = '0'", 'left');

        $this->db->group_by($orderBy);
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        if (!empty($sWhere))
            $this->db->where($sWhere, null, false);

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                $val->key_contact = $val->position . ' - ' . $val->name;
                $val->firstname = $val->firstname;
                $val->lastname = $val->lastname;
                $val->key_contact_label = $val->key_contact;
                $val->position = $val->position;
                $val->contact_type = $val->type;

                if (isset($val->primary_phone)) {
                    $val->primary_phone = $val->phone;
                }

                if (isset($val->primary_email)) {
                    $val->primary_email = $val->email;
                }

                $zPhone = $this->Basic_model->get_result($tbl_phone, array($tbl_phone_prefix . '.contactId' => $val->id, $tbl_phone_prefix . '.archive' => '0'), $columns = array($tbl_phone_prefix . '.phone', $tbl_phone_prefix . '.primary_phone', $tbl_phone_prefix . '.id'));
                if (!empty($zPhone)) {
                    $ph_temp = array();
                    foreach ($zPhone as $key => $valPh) {
                        $ph_temp[] = array('phone' => $valPh->phone, 'id' => $valPh->id, 'primary_phone' => $valPh->primary_phone);
                    }
                    $val->OrganisationPh = $ph_temp;
                } else {
                    $val->OrganisationPh = [['phone' => '']];
                }

                /* Get organisatin email no */
                $zMail = $this->Basic_model->get_result($tbl_email, array($tbl_email_prefix . '.contactId' => $val->id, $tbl_email_prefix . '.archive' => '0'), $columns = array($tbl_email_prefix . '.email', $tbl_email_prefix . '.primary_email', $tbl_email_prefix . '.id'));
                if (!empty($zMail)) {
                    $email_temp = array();
                    foreach ($zMail as $key => $valmail) {
                        $email_temp[] = array('email' => $valmail->email, 'id' => $valmail->id, 'primary_email' => $valmail->primary_email);
                    }
                    $val->OrganisationEmail = $email_temp;
                } else {
                    $val->OrganisationEmail = [['email' => '']];
                }
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult);
        return $return;
    }

    public function get_house_fms($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $houseId = $reqData->houseId;
        $orderBy = '';
        $direction = '';
        $tbl_fms = TBL_PREFIX . 'fms_case';
        $src_columns = array();

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_fms . '.id';
            $direction = 'DESC';
        }

        /* if (isset($filter)) 
          {
          if($filter->archive == 1)
          {
          $this->db->where('tbl_organisation.parent_org=',$houseId);
          }
          else
          {
          $this->db->where('tbl_fms_case.initiated_by=',$houseId);
          }
          } */
        $this->db->where('tbl_fms_case.initiated_by=', $houseId);

        $where = '';
        $where = $tbl_fms . ".initiated_type = 4";
        $sWhere = $where;

        $select_column = array($tbl_fms . ".id", $tbl_fms . ".event_date", "tbl_fms_case_reason.description", $tbl_fms . ".initiated_type", $tbl_fms . ".Initiator_first_name", $tbl_fms . ".Initiator_last_name", $tbl_fms . ".initiated_by", $tbl_fms . ".created");

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_fms);
        $this->db->join('tbl_fms_case_reason', 'tbl_fms_case_reason.caseId = ' . $tbl_fms . '.id', 'left');
        $this->db->join('tbl_organisation', 'tbl_organisation.id = ' . $tbl_fms . '.initiated_by', 'left');

        $this->db->group_by('tbl_fms_case_reason.caseId');
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $this->db->where($sWhere, null, false);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        //last_query(); die;
        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();
        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                $init_by_id = $val->initiated_by;
                $tbl = 'tbl_organisation';
                $this->db->select("name");
                $this->db->where('id', $init_by_id);
                $emp_qry = $this->db->get($tbl);
                $row = $emp_qry->row_array();
                $val->initiated_by = $row['name'];
                $val->timelapse = time_ago_in_php($val->created);
                $val->against_for = get_fms_against_name($val->id);
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult);
        return $return;
    }

    public function get_house_docs($objMember) {
        if (!empty(request_handler('access_organization'))) {
            $request = request_handler();
            $request_data = $request->data;
            $houseId = $request_data->org_id;
            $view_by = $request_data->view_by;

            if ($view_by == '1')
                $this->db->where('archive', '0');
            else
                $this->db->where('archive', '1');

            $tbl_1 = TBL_PREFIX . 'house_docs';
            $dt_query = $this->db->select(array($tbl_1 . '.id', $tbl_1 . '.expiry', $tbl_1 . '.title', $tbl_1 . '.created', $tbl_1 . '.filename', "DATEDIFF(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(expiry)), '%Y-%m-%d'),CURDATE()) AS days_left"));
            $sWhere = array($tbl_1 . '.houseId' => $houseId);
            $this->db->from($tbl_1);
            $this->db->where($sWhere, null, false);
            $query = $this->db->get();
            #last_query();
            $x = $query->result_array();
            $data_ary = array();
            if (!empty($x)) {
                foreach ($x as $key => $value) {
                    $temp_ary['id'] = $value['id'];
                    $temp_ary['days_left'] = $value['days_left'];
                    $temp_ary['expiry'] = date('d-m-Y', strtotime($value['expiry']));
                    $temp_ary['title'] = $value['title'];
                    $temp_ary['created'] = $value['created'];
                    $temp_ary['filename'] = $value['filename'];
                    if ($value['days_left'] < 0 || $value['days_left'] < 5)
                        $class_name = 'text_dark_Pink';
                    else if ($value['days_left'] > 0 && $value['days_left'] < 30)
                        $class_name = 'text_dark_yello';
                    else
                        $class_name = '';

                    $temp_ary['class_name'] = $class_name;
                    $data_ary[] = $temp_ary;
                }
            }

            if (!empty($data_ary))
                return $data_ary;
            else
                return array();
        }
    }

    public function get_parent_org_name($post_data) {
        $this->db->or_like('name', $post_data);
        $this->db->where('archive =', '0');
        $this->db->where('status=', 1, FALSE);
        $this->db->where('parent_org=', 0, FALSE);
        $this->db->select(array('id', 'name'));
        $query = $this->db->get(TBL_PREFIX . 'organisation');
        //last_query();
        $query->result();
        $rows = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $rows[] = array('label' => $val->name, 'value' => $val->id);
            }
        }
        return $rows;
    }

    public function create_org($post_data) {
        if (!empty($post_data)) {
            $parent_org_id = isset($post_data['parent_organisation']->value) ? $post_data['parent_organisation']->value : '0';
            $organisation_data = array('name' => $post_data['organisation_name']->value,
                'abn' => $post_data['organisation_abn'],
                'logo_file' => '',
                'parent_org' => $parent_org_id,
                'website' => $post_data['organisation_website'],
                'payroll_tax' => isset($post_data['organisation_payroll_tax']) ? $post_data['organisation_payroll_tax'] : '',
                'gst' => isset($post_data['organisation_gst']) ? $post_data['organisation_gst'] : '0',
                'enable_portal_access' => '1',
            );
            $org_id = $this->Basic_model->insert_records('organisation', $organisation_data);
            /* site added code start */
            if (isset($post_data['site_org']) && !empty($post_data['site_org'])) {
                $this->attach_site_to_org($post_data['site_org'], $org_id);
            }
            /* site added code end */
            /* Image upload */
            $config['upload_path'] = ORG_UPLOAD_PATH;
            $config['input_name'] = 'myFile';
            $config['directory_name'] = $org_id;
            $config['allowed_types'] = 'jpg|jpeg|png';

            $is_upload = do_upload($config);
            if (isset($is_upload['error'])) {
                $upload_error = $is_upload['error'];
            } else {
                $upload_error = '';
                $org_data = array('logo_file' => $is_upload['upload_data']['file_name']);
                $this->basic_model->update_records('organisation', $org_data, array('id' => $org_id));
            }
            /**/

            if (!empty($post_data['completeAddress'])) {
                $organisation_adr = array();
                foreach ($post_data['completeAddress'] as $key => $value) {
                    $organisation_adr[] = array('organisationId' => $org_id,
                        'street' => $value->location,
                        'city' => $value->suburb->label,
                        'postal' => $value->postal,
                        'state' => $value->state,
                        'category' => $value->address_category,
                        'primary_address' => isset($key) && $key == 0 ? '1' : '2'
                    );
                }
                if (!empty($organisation_adr))
                    $this->Basic_model->insert_records('organisation_address', $organisation_adr, TRUE);
            }

            if (!empty($post_data['OrgRequirement'])) {
                $organisation_req = array();
                foreach ($post_data['OrgRequirement'] as $key => $val) {
                    if ($val->checked == 1) {
                        $organisation_req[] = array('organisationId' => $org_id,
                            'requirementId' => $val->value,
                        );
                    }
                }
                if (!empty($organisation_req))
                    $this->Basic_model->insert_records('organisation_requirements', $organisation_req, TRUE);
            }

            if (!empty($post_data['OrganisationPh'])) {
                $organisation_ph = array();
                foreach ($post_data['OrganisationPh'] as $kk => $ph) {
                    $organisation_ph[] = array('organisationId' => $org_id,
                        'phone' => $ph->phone,
                        'primary_phone' => isset($kk) && $kk == 0 ? '1' : '2',
                        'archive' => '0'
                    );
                }
                if (!empty($organisation_ph))
                    $this->Basic_model->insert_records('organisation_phone', $organisation_ph, TRUE);
            }

            if (!empty($post_data['OrganisationEmail'])) {
                $organisation_mail = array();
                foreach ($post_data['OrganisationEmail'] as $ke => $email) {
                    $organisation_mail[] = array('organisationId' => $org_id,
                        'email' => $email->email,
                        'primary_email' => isset($ke) && $ke == 0 ? '1' : '2',
                        'archive' => '0'
                    );
                }
                if (!empty($organisation_mail))
                    $this->Basic_model->insert_records('organisation_email', $organisation_mail, TRUE);
            }

            $org_key_contact_data = array('organisationId' => $org_id,
                'name' => isset($post_data['key_contact_fname']) ? $post_data['key_contact_fname'] : '',
                'lastname' => isset($post_data['key_contact_lname']) ? $post_data['key_contact_lname'] : '',
                'position' => isset($post_data['key_contact_position']) ? $post_data['key_contact_position'] : '',
                'department' => isset($post_data['key_contact_department']) ? $post_data['key_contact_department'] : '',
                'type' => 3,
                'archive' => '0'
            );

            $key_contact_id = $this->Basic_model->insert_records('organisation_all_contact', $org_key_contact_data);

            if (!empty($post_data['OrganisationKeyContactEmail'])) {
                $organisation_keycontact_mail = array();
                foreach ($post_data['OrganisationKeyContactEmail'] as $ke => $emailid) {
                    $organisation_keycontact_mail[] = array('contactId' => $key_contact_id,
                        'email' => $emailid->email,
                        'primary_email' => isset($ke) && $ke == 0 ? '1' : '2',
                        'archive' => '0'
                    );
                }
                if (!empty($organisation_keycontact_mail))
                    $this->Basic_model->insert_records('organisation_all_contact_email', $organisation_keycontact_mail, TRUE);
            }

            if (!empty($post_data['OrganisationKeyContactPh'])) {
                $organisation_keycontact_ph = array();
                foreach ($post_data['OrganisationKeyContactPh'] as $ke => $pho) {
                    $organisation_keycontact_ph[] = array('contactId' => $key_contact_id,
                        'phone' => $pho->phone,
                        'primary_phone' => isset($ke) && $ke == 0 ? '1' : '2',
                        'archive' => '0'
                    );
                }
                if (!empty($organisation_keycontact_ph))
                    $this->Basic_model->insert_records('organisation_all_contact_phone', $organisation_keycontact_ph, TRUE);
            }

            $org_bill_contact_data = array('organisationId' => $org_id,
                'name' => isset($post_data['bill_contact_fname']) ? $post_data['bill_contact_fname'] : '',
                'lastname' => isset($post_data['bill_contact_lname']) ? $post_data['bill_contact_lname'] : '',
                'position' => isset($post_data['bill_contact_position']) ? $post_data['bill_contact_position'] : '',
                'department' => isset($post_data['bill_contact_department']) ? $post_data['bill_contact_department'] : '',
                'type' => 4,
                'archive' => '0'
            );

            $bill_contact_id = $this->Basic_model->insert_records('organisation_all_contact', $org_bill_contact_data);
            if (!empty($post_data['OrganisationBillingContactEmail'])) {
                $organisation_billcontact_mail = array();
                foreach ($post_data['OrganisationBillingContactEmail'] as $ke => $emaild) {
                    $organisation_billcontact_mail[] = array('contactId' => $bill_contact_id,
                        'email' => $emaild->email,
                        'primary_email' => isset($ke) && $ke == 0 ? '1' : '2',
                        'archive' => '0'
                    );
                }
                if (!empty($organisation_billcontact_mail))
                    $this->Basic_model->insert_records('organisation_all_contact_email', $organisation_billcontact_mail, TRUE);
            }

            if (!empty($post_data['OrganisationBillingContactPh'])) {
                $organisation_billcontact_ph = array();
                foreach ($post_data['OrganisationBillingContactPh'] as $ke => $pho) {
                    $organisation_billcontact_ph[] = array('contactId' => $bill_contact_id,
                        'phone' => $pho->phone,
                        'primary_phone' => isset($ke) && $ke == 0 ? '1' : '2',
                        'archive' => '0'
                    );
                }
                if (!empty($organisation_billcontact_ph))
                    $this->Basic_model->insert_records('organisation_all_contact_phone', $organisation_billcontact_ph, TRUE);
            }
            return array('org_id' => $org_id, 'image_status' => $upload_error);
        }
    }

    public function update_org($post_data) {
        if (!empty($post_data)) {
            # $parent_org_id = isset($post_data['parent_organisation']->value)?$post_data['parent_organisation']->value:'0';
            $org_id = $post_data['ocs_id'];
            $organisation_data = array(
                'website' => $post_data['website'],
                'payroll_tax' => isset($post_data['payroll_tax']) ? $post_data['payroll_tax'] : '',
                'gst' => isset($post_data['gst']) ? $post_data['gst'] : '0',
            );

            $this->basic_model->update_records('organisation', $organisation_data, array('id' => $org_id));
            //last_query();
            /* Image upload */
            $upload_error = '';
            if ($post_data['is_upload_img']) {
                $config['upload_path'] = ORG_UPLOAD_PATH;
                $config['input_name'] = 'myFile';
                $config['directory_name'] = $org_id;
                $config['allowed_types'] = 'jpg|jpeg|png';

                $is_upload = do_upload($config);

                if (isset($is_upload['error'])) {
                    $upload_error = strip_tags($is_upload['error']);
                } else {
                    $org_data = array('logo_file' => $is_upload['upload_data']['file_name']);
                    $this->basic_model->update_records('organisation', $org_data, array('id' => $org_id));
                }
            }
            /**/


            $organisation_adr = array();

            $organisation_adr = array('organisationId' => $org_id,
                'street' => $post_data['street'],
                'city' => $post_data['city']->value,
                'postal' => $post_data['postal'],
                'state' => $post_data['state']
            );

            if (!empty($organisation_adr)) {
                $this->Basic_model->update_records('organisation_address', $organisation_adr, array('organisationId' => $org_id));
            }



            if (!empty($post_data['OrganisationPh'])) {
                /* First delete old record and then insert new record */
                $this->Basic_model->update_records('organisation_phone', array('archive' => '1'), array('organisationId' => $org_id));

                $organisation_ph = array();
                foreach ($post_data['OrganisationPh'] as $kk => $ph) {
                    $organisation_ph[] = array('organisationId' => $org_id,
                        'phone' => $ph->phone,
                        'primary_phone' => isset($kk) && $kk == 0 ? '1' : '2',
                        'archive' => '0'
                    );
                }
                if (!empty($organisation_ph))
                    $this->Basic_model->insert_records('organisation_phone', $organisation_ph, TRUE);
            }

            if (!empty($post_data['OrganisationEmail'])) {
                /* First delete old record and then insert new record */
                $this->Basic_model->update_records('organisation_email', array('archive' => '1'), array('organisationId' => $org_id));
                $organisation_mail = array();
                foreach ($post_data['OrganisationEmail'] as $ke => $email) {
                    $organisation_mail[] = array('organisationId' => $org_id,
                        'email' => $email->email,
                        'primary_email' => isset($ke) && $ke == 0 ? '1' : '2',
                        'archive' => '0'
                    );
                }
                if (!empty($organisation_mail))
                    $this->Basic_model->insert_records('organisation_email', $organisation_mail, TRUE);
            }
            return array('org_id' => $org_id, 'image_status' => $upload_error);
        }
    }

    public function save_site($post_data) {
        #pr($post_data);
        if (!empty($post_data)) {
            $org_id = isset($post_data['organisation_name']['value']) ? $post_data['organisation_name']['value'] : (isset($post_data['orgId']) ? $post_data['orgId'] : '0');

            $organisation_data = array('site_name' => $post_data['title'],
                'logo_file' => '',
                'organisationId' => $org_id,
                'street' => $post_data['site_address'],
                'city' => $post_data['city']['value'],
                'postal' => isset($post_data['postal']) ? $post_data['postal'] : '',
                'state' => $post_data['state'],
                'created' => DATE_TIME
            );
            if (isset($post_data['abn'])) {
                $organisation_data['abn'] = $post_data['abn'];
            }
            $site_id = $this->Basic_model->insert_records('organisation_site', $organisation_data);

            if (!empty($post_data['SitePhone'])) {
                $site_ph = array();
                foreach ($post_data['SitePhone'] as $kk => $ph) {
                    $site_ph[] = array('siteId' => $site_id,
                        'phone' => $ph['phone'],
                        'primary_phone' => isset($kk) && $kk == 0 ? '1' : '2',
                        'archive' => '0'
                    );
                }
                if (!empty($site_ph))
                    $this->Basic_model->insert_records('organisation_site_phone', $site_ph, TRUE);
            }

            if (!empty($post_data['SiteEmail'])) {
                $site_mail = array();
                foreach ($post_data['SiteEmail'] as $ke => $email) {
                    $site_mail[] = array('siteId' => $site_id,
                        'email' => $email['email'],
                        'primary_email' => isset($ke) && $ke == 0 ? '1' : '2',
                        'archive' => '0'
                    );
                }
                if (!empty($site_mail))
                    $this->Basic_model->insert_records('organisation_site_email', $site_mail, TRUE);
            }

            if (!empty($post_data['siteDetails'])) {
                foreach ($post_data['siteDetails'] as $key => $value) {
                    $site_key_contact_data = array('siteId' => $site_id,
                        'firstname' => isset($value['firstname']) ? $value['firstname'] : '',
                        'lastname' => isset($value['lastname']) ? $value['lastname'] : '',
                        'position' => isset($value['position']) ? $value['position'] : '',
                        'department' => isset($value['department']) ? $value['department'] : '',
                        'type' => 3,
                        'created' => DATE_TIME,
                        'archive' => '0'
                    );
                    $key_contact_id = $this->Basic_model->insert_records('organisation_site_key_contact', $site_key_contact_data);

                    if (isset($value['sitePh'])) {
                        $organisation_keycontact_ph = array();
                        if (!empty($value['sitePh'])) {
                            foreach ($value['sitePh'] as $key => $val) {
                                $site_ph = array('phone' => $val['phone']);
                                if (isset($val['id']) && !empty($val['id'])) {
                                    $this->Basic_model->update_records('organisation_site_key_contact_phone', array_merge(array('archive' => '0'), $site_ph), array('id' => $val['id']));
                                } else {
                                    $site_ph['primary_phone'] = $key == 0 ? '1' : '2';
                                    $site_ph['archive'] = '0';
                                    $site_ph['contactId'] = $key_contact_id;
                                    $organisation_keycontact_ph[] = $site_ph;
                                }
                            }
                        }
                        if (!empty($organisation_keycontact_ph))
                            $this->Basic_model->insert_records('organisation_site_key_contact_phone', $organisation_keycontact_ph, TRUE);
                    }

                    if (isset($value['siteMail'])) {
                        $organisation_keycontact_mail = array();
                        if (!empty($value['siteMail'])) {
                            foreach ($value['siteMail'] as $ke => $val) {

                                $site_email = array('email' => $val['email']);
                                if (isset($val['id']) && !empty($val['id'])) {
                                    $this->Basic_model->update_records('organisation_site_key_contact_email', array_merge(array('archive' => '0'), $site_email), array('id' => $val['id']));
                                } else {
                                    $site_email['primary_email'] = $ke == 0 ? '1' : '2';
                                    $site_email['archive'] = '0';
                                    $site_email['contactId'] = $key_contact_id;
                                    $organisation_keycontact_mail[] = $site_email;
                                }
                            }
                        }
                        if (!empty($organisation_keycontact_mail))
                            $this->Basic_model->insert_records('organisation_site_key_contact_email', $organisation_keycontact_mail, TRUE);
                    }
                }
            }
            return array('site_id' => $site_id);
        }
    }

    public function update_site($post_data) {
        #pr($post_data);
        if (!empty($post_data)) {
            $org_id = isset($post_data['organisation_name']['value']) ? $post_data['organisation_name']['value'] : (isset($post_data['orgId']) ? $post_data['orgId'] : '0');

            $site_id = $post_data['site_id'];
            $organisation_data = array('site_name' => $post_data['title'],
                'street' => $post_data['site_address'],
                'city' => $post_data['city']['value'],
                'postal' => $post_data['postal'],
                'state' => $post_data['state']
            );
            if (isset($post_data['abn'])) {
                $organisation_data['abn'] = $post_data['abn'];
            }
            $this->Basic_model->update_records('organisation_site', $organisation_data, array('id' => $site_id));

            if (!empty($post_data['SitePhone'])) {
                /* First delete old record and then insert new record */
                $this->Basic_model->update_records('organisation_site_phone', array('archive' => '1'), array('siteId' => $site_id));
                foreach ($post_data['SitePhone'] as $kk => $ph) {
                    $site_ph = array('phone' => $ph['phone']);
                    if (isset($ph['id']) && !empty($ph['id'])) {
                        $this->Basic_model->update_records('organisation_site_phone', array_merge(array('archive' => '0'), $site_ph), array('id' => $ph['id']));
                    } else {
                        $site_ph['primary_phone'] = $kk == 0 ? '1' : '2';
                        $site_ph['archive'] = '0';
                        $site_ph['siteId'] = $site_id;
                        $this->Basic_model->insert_records('organisation_site_phone', $site_ph);
                    }
                }
            }

            if (!empty($post_data['SiteEmail'])) {
                /* First delete old record and then insert new record */
                $this->Basic_model->update_records('organisation_site_email', array('archive' => '1'), array('siteId' => $site_id));
                foreach ($post_data['SiteEmail'] as $ke => $email) {
                    $site_mail = array('email' => $email['email']);
                    if (isset($email['id']) && !empty($email['id'])) {
                        $this->Basic_model->update_records('organisation_site_email', array_merge(array('archive' => '0'), $site_mail), array('id' => $email['id']));
                    } else {
                        $site_mail['primary_email'] = $ke == 0 ? '1' : '2';
                        $site_mail['archive'] = '0';
                        $site_mail['siteId'] = $site_id;
                        $this->Basic_model->insert_records('organisation_site_email', $site_mail);
                    }
                }
            }

            if (!empty($post_data['siteDetails'])) {
                /* First delete old record and then insert new record */
                $this->Basic_model->update_records('organisation_site_key_contact', array('archive' => '1'), array('siteId' => $site_id));

                foreach ($post_data['siteDetails'] as $key => $value) {
                    $site_key_contact_data = array('firstname' => isset($value['firstname']) ? $value['firstname'] : '',
                        'lastname' => isset($value['lastname']) ? $value['lastname'] : '',
                        'position' => isset($value['position']) ? $value['position'] : '',
                        'department' => isset($value['department']) ? $value['department'] : '',
                    );
                    if (isset($value['id']) && !empty($value['id'])) {
                        $this->Basic_model->update_records('organisation_site_key_contact', array_merge(array('archive' => '0'), $site_key_contact_data), array('id' => $value['id']));
                        $key_contact_id = $value['id'];
                    } else {
                        $site_key_contact_data['type'] = '3';
                        $site_key_contact_data['archive'] = '0';
                        $site_key_contact_data['siteId'] = $site_id;
                        $key_contact_id = $this->Basic_model->insert_records('organisation_site_key_contact', $site_key_contact_data);
                    }

                    if (isset($value['sitePh'])) {
                        $organisation_keycontact_ph = array();
                        if (!empty($value['sitePh'])) {
                            /* First delete old record and then insert new record */
                            $this->Basic_model->update_records('organisation_site_key_contact_phone', array('archive' => '1'), array('contactId' => $key_contact_id));

                            foreach ($value['sitePh'] as $key => $val) {
                                $organisation_keycontact_ph = array('phone' => $val['phone']);
                                if (isset($val['id']) && !empty($val['id'])) {
                                    $this->Basic_model->update_records('organisation_site_key_contact_phone', array_merge(array('archive' => '0'), $organisation_keycontact_ph), array('id' => $val['id']));
                                } else {
                                    $organisation_keycontact_ph['archive'] = '0';
                                    $organisation_keycontact_ph['primary_phone'] = $key == 0 ? '1' : '2';
                                    $organisation_keycontact_ph['contactId'] = $key_contact_id;
                                    $this->Basic_model->insert_records('organisation_site_key_contact_phone', $organisation_keycontact_ph);
                                }
                            }
                        }
                    }

                    if (isset($value['siteMail'])) {
                        $organisation_keycontact_mail = array();
                        if (!empty($value['siteMail'])) {
                            /* First delete old record and then insert new record */
                            $this->Basic_model->update_records('organisation_site_key_contact_email', array('archive' => '1'), array('contactId' => $key_contact_id));
                            foreach ($value['siteMail'] as $ke => $emailid) {
                                $organisation_keycontact_mail = array('email' => $emailid['email']);
                                if (isset($emailid['id']) && !empty($emailid['id'])) {
                                    $this->Basic_model->update_records('organisation_site_key_contact_email', array_merge(array('archive' => '0'), $organisation_keycontact_mail), array('id' => $emailid['id']));
                                } else {
                                    $organisation_keycontact_mail['archive'] = '0';
                                    $organisation_keycontact_mail['primary_email'] = $ke == 0 ? '1' : '2';
                                    $organisation_keycontact_mail['contactId'] = $key_contact_id;
                                    $this->Basic_model->insert_records('organisation_site_key_contact_email', $organisation_keycontact_mail);
                                }
                            }
                        }
                    }
                }
            }
            return array('site_id' => $site_id);
        }
    }

    public function add_org_site($post_data) {
        
    }

    public function add_org_contact($reqData, $contact_id) {
        $tablePhone = 'organisation_all_contact_phone';
        $tableEmail = 'organisation_all_contact_email';
        if (!empty($reqData)) {
            /* First delete old record and then insert new record */
            $this->Basic_model->update_records($tablePhone, array('archive' => '1'), array('contactId' => $contact_id));
            $this->Basic_model->update_records($tableEmail, array('archive' => '1'), array('contactId' => $contact_id));


            if (!empty($reqData['OrganisationPh'])) {
                $phone = json_decode(json_encode($reqData['OrganisationPh']), TRUE);
                $key = 0;
                $main_ph = array_map(function($val) use ($key, $contact_id) {
                    global $key;
                    $val['primary_phone'] = $key == 0 ? 1 : 2;
                    $val['contactId'] = $contact_id;
                    $key++;
                    return $val;
                }, $phone);
            }

            if (!empty($reqData['OrganisationEmail'])) {
                $email = json_decode(json_encode($reqData['OrganisationEmail']), TRUE);
                $keyEmail = 0;
                $main_email = array_map(function($val) use ($keyEmail, $contact_id) {
                    global $keyEmail;
                    $val['primary_email'] = $keyEmail == 0 ? 1 : 2;
                    $val['contactId'] = $contact_id;
                    $keyEmail++;
                    return $val;
                }, $email);
            }


            if (!empty($main_ph)) {
                $this->Basic_model->insert_records($tablePhone, $main_ph, TRUE);
            }
            if (!empty($main_email)) {
                $this->Basic_model->insert_records($tableEmail, $main_email, TRUE);
            }
        }
    }

    public function get_site_cotact_multiple($site_id = 0) {
        $tbl_8 = 'tbl_organisation_site_key_contact';
        $keycontacts_rows = $this->Basic_model->get_result('organisation_site_key_contact', array($tbl_8 . '.siteId' => $site_id, $tbl_8 . '.archive' => '0'), $columns = array($tbl_8 . '.position', $tbl_8 . '.department', $tbl_8 . '.id', $tbl_8 . '.street', $tbl_8 . '.city', $tbl_8 . '.postal', $tbl_8 . '.lastname', $tbl_8 . '.firstname', $tbl_8 . '.state', "CONCAT(tbl_organisation_site_key_contact.firstname,' ',tbl_organisation_site_key_contact.lastname) AS key_contact"));
        #last_query();

        if (!empty($keycontacts_rows)) {
            $kContacts_temp = array();
            foreach ($keycontacts_rows as $key => $valcontact) {
                $kEmail_temp = array();
                $kPhone_temp = array();
                $kContEmail = $this->Basic_model->get_result('organisation_site_key_contact_email', array('contactId' => $valcontact->id, 'archive' => '0'), $columns = array('id', 'email', 'primary_email'));
                if (!empty($kContEmail)) {
                    foreach ($kContEmail as $key => $ContEmail) {
                        $kEmail_temp[] = array('email' => $ContEmail->email, 'id' => $ContEmail->id, 'primary_email' => $ContEmail->primary_email);
                    }
                } else {
                    $kPhone_temp[] = ['email' => ''];
                }

                $kContPhone = $this->Basic_model->get_result('organisation_site_key_contact_phone', array('contactId' => $valcontact->id, 'archive' => '0'), $columns = array('id', 'phone', 'primary_phone'));
                if (!empty($kContPhone)) {
                    foreach ($kContPhone as $key => $ContPhone) {
                        $kPhone_temp[] = array('phone' => $ContPhone->phone, 'id' => $ContPhone->id, 'primary_phone' => $ContPhone->primary_phone);
                    }
                } else {
                    $kPhone_temp[] = ['phone' => ''];
                }

                $kContacts_temp[] = array('key_contact_label' => $valcontact->key_contact, 'key_contact' => $valcontact->key_contact . ' - ' . $valcontact->position, 'lastname' => $valcontact->lastname, 'id' => $valcontact->id, 'firstname' => $valcontact->firstname, 'position' => $valcontact->position, 'department' => $valcontact->department, 'sitePh' => $kPhone_temp, 'siteMail' => $kEmail_temp);
            }
            return $kContacts_temp;
        } else {
            return array(
                array(
                    'key_contact' => '',
                    'sitePh' => [['phone' => '']],
                    'siteMail' => [['email' => '']],
                    'firstname' => '',
                    'lastname' => '',
                    'position' => '',
                    'department' => ''
                )
            );
        }
    }

    public function add_house_contact($reqData, $contact_id) {
        $tablePhone = 'organisation_site_key_contact_phone';
        $tableEmail = 'organisation_site_key_contact_email';
        if (!empty($reqData)) {
            /* First delete old record and then insert new record */
            $this->Basic_model->update_records($tablePhone, array('archive' => '1'), array('contactId' => $contact_id));
            $this->Basic_model->update_records($tableEmail, array('archive' => '1'), array('contactId' => $contact_id));


            if (!empty($reqData['OrganisationPh'])) {
                $phone = json_decode(json_encode($reqData['OrganisationPh']), TRUE);
                $keyphone = 0;
                $main_ph = array_map(function($val) use ($keyphone, $contact_id, $tablePhone) {
                    global $keyphone;
                    if (isset($val['id']) && !empty($val['id'])) {
                        $this->Basic_model->update_records($tablePhone, array('archive' => '0', 'phone' => $val['phone']), array('id' => $val['id']));
                        $keyphone++;
                        return false;
                    }
                    if (isset($val['id'])) {
                        unset($val['id']);
                    }
                    $val['primary_phone'] = $keyphone == 0 ? 1 : 2;
                    $val['contactId'] = $contact_id;
                    $val['archive'] = '0';
                    $keyphone++;
                    return $val;
                }, $phone);
                $main_ph = array_filter($main_ph);
            }

            if (!empty($reqData['OrganisationEmail'])) {
                $email = json_decode(json_encode($reqData['OrganisationEmail']), TRUE);
                $keyemail = 0;
                $main_email = array_map(function($val) use ($keyemail, $contact_id, $tableEmail) {
                    global $keyemail;
                    if (isset($val['id']) && !empty($val['id'])) {
                        $this->Basic_model->update_records($tableEmail, array('archive' => '0', 'email' => $val['email']), array('id' => $val['id']));
                        $keyemail++;
                        return false;
                    }
                    if (isset($val['id'])) {
                        unset($val['id']);
                    }
                    $val['primary_email'] = $keyemail == 0 ? 1 : 2;
                    $val['contactId'] = $contact_id;
                    $val['archive'] = '0';
                    $keyemail++;
                    return $val;
                }, $email);
                $main_email = array_filter($main_email);
            }


            if (!empty($main_ph)) {
                $this->Basic_model->insert_records($tablePhone, $main_ph, TRUE);
            }
            if (!empty($main_email)) {
                $this->Basic_model->insert_records($tableEmail, $main_email, TRUE);
            }
        }
    }

    public function get_site_org_unassign($post_data) {
        $resultData = [];
        if (!empty($post_data) && gettype($post_data) == 'string') {
            $tbl_1 = TBL_PREFIX . 'organisation_site';
            $this->db->like($tbl_1 . '.site_name', $post_data);
            $this->db->where($tbl_1 . '.archive', '0');
            $this->db->where($tbl_1 . '.status', 1);
            $this->db->where($tbl_1 . '.organisationId', 0);
            $this->db->select(array($tbl_1 . '.id as value', $tbl_1 . '.site_name as label'));
            $query = $this->db->get($tbl_1);
            $resultData = $query->num_rows() > 0 ? $query->result_array() : [];
        }
        return $resultData;
    }

    public function attach_site_to_org($siteOrgData = [], $orgId = 0) {
        $res = false;
        if (count($siteOrgData) > 0 && (int) $orgId > 0) {
            foreach ($siteOrgData as $val) {
                $val = !empty($val) && is_object($val) ? (array) $val : $val;
                if (isset($val['value']) && !empty($val['value']) && is_numeric($val['value'])) {
                    $this->Basic_model->update_records('organisation_site', ['organisationId' => $orgId], ['archive' => '0', 'organisationId' => '0', 'id' => $val['value']]);
                    $res = true;
                }
            }
        }
        return $res;
    }

}
