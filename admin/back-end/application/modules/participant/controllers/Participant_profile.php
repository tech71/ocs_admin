<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class Participant_profile extends MX_Controller {

    // add here our custom form validation of server side
    use formCustomValidation;

    // defualt contructor function call every time when any function called on this class
    function __construct() {

        parent::__construct();
        $this->load->library('form_validation'); // load form validation library
        $this->form_validation->CI = & $this;
        $this->load->model('Participant_profile_model'); // load participant profile model

        $this->loges->setModule(2);
    }

    /*
     * function participant_app_access
     * update participant portal access
     * return true and false with error 
     * return type json
     */

    public function participant_app_access() {
        // handle request and check permission to update participant access
        $reqData = request_handler('update_participant');
        $this->loges->setCreatedBy($reqData->adminId); // set created by in log
        // check request data not empty
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            // set form validation data
            $this->form_validation->set_data((array) $reqData);

            // make form validation rule data
            $validation_rules = array(
                array('field' => 'status', 'label' => 'status', 'rules' => 'required'),
                array('field' => 'participant_id', 'label' => 'participant id', 'rules' => 'required'),
            );

            // set validation rule
            $this->form_validation->set_rules($validation_rules);

            // check participant data valid
            if ($this->form_validation->run()) {

                // check status if its zero mean app access is disable
                if ($reqData->status == 0) {
                    // logout participant if its login in portal
                    $this->basic_model->delete_records('participant_login', $where = array('participantId' => $reqData->participant_id));
                }

                // update app access
                $this->basic_model->update_records('participant', $data = array('portal_access' => $reqData->status), $where = array('id' => $reqData->participant_id));

                $this->loges->setTitle((($reqData->status == 0) ? 'Disable' : 'Enable') . ' app access :' . trim($reqData->fullName));
                $this->loges->setUserId($reqData->participant_id);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->createLog(); // create log

                $response = array('status' => true);
            } else {
                // return form validation
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($response);
        }
    }

    /*
     * function change_active_deactive
     * update participant status
     * return status true and false 
     * return type json
     */

    public function change_active_deactive() {
        // handle request and check permission to update participant access
        $reqData = request_handler('update_participant');
        $this->loges->setCreatedBy($reqData->adminId); // set created by in log

        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $this->loges->setTitle((($reqData->status == 0) ? 'Inactive' : 'Active') . ' user : ' . trim($reqData->fullName));
            $this->loges->setUserId($reqData->participant_id);
            $this->loges->setDescription(json_encode($reqData));
            $this->loges->createLog();

            // update participant status
            $this->basic_model->update_records('participant', $data = array('status' => $reqData->status), $where = array('id' => $reqData->participant_id));

            // check status if its zero
            if ($reqData->status == 0) {
                // logout participant if its login in portal
                $this->basic_model->delete_records('participant_login', $where = array('participantId' => $reqData->participant_id));
            }

            echo json_encode(array('status' => true));
        }
    }

    /*
     * function get_participant_profile
     * return participant profile details 
     * return type json
     */

    public function get_participant_profile() {
        // handle request and check permission to access participant
        $reqData = request_handler('access_participant');
        if (!empty($reqData->data)) {
            $participantID = $reqData->data->id;

            // get participant detials
            $result = $this->Participant_profile_model->participant_profile($participantID);

            if (!empty($result)) {

                $result->booking_date = ($result->booking_date == '0000-00-00') ? '' : $result->booking_date;

                // get participant prpfile image
                $result->profile_image = get_participant_img($participantID, $result->profile_image, $result->gender);

                // get participant profile percentage, return in percentage how much profile completed
                $result->profile_complete = get_profile_complete('PARTICIPANT', $participantID);

                echo json_encode(array('status' => true, 'data' => $result));
            } else {
                echo json_encode(array('status' => false, 'data' => 'Invalid participant Id'));
            }
        }
    }

    /*
     * function get_participant_about
     * return participant about details like details, addresss, kin details, care not to book, phone and email address
     * return type json
     */

    public function get_participant_about() {
        // handle request and check permission to access participant
        $reqData = request_handler('access_participant');
        if (!empty($reqData->data)) {
            $participantId = $reqData->data->id;
            $where = array('participantId' => $participantId);

            // get about participant details
            $result = $this->Participant_profile_model->participant_about($participantId);

            if (!empty($result)) {
                // get participant site
                $result['address'] = $this->Participant_profile_model->participant_sites($participantId);

                // get kin details
                $result['kin_detials'] = $this->Participant_profile_model->get_participant_kin($participantId);

                // cate not to book
                $xx = $this->basic_model->get_record_where('participant_care_not_tobook', '', $where);
                if(!empty($xx))
                {
                    $Ethnicity_female_option = array();
                    $Religious_female_option = array();
                    $Ethnicity_male_option = array();
                    $Religious_male_option = array();

                    foreach ($xx as $key => $value)
                    {
                        if($value->gender == 2)
                        {
                            if($value->ethnicity!='')
                                $Ethnicity_female_option[] = $value->ethnicity;

                            if($value->religious!='')
                                $Religious_female_option[] = $value->religious;
                        }                        

                        if($value->gender == 1)
                        {
                            if($value->ethnicity!='')
                                $Ethnicity_male_option[] = $value->ethnicity;
                        
                            if($value->religious!='')
                                $Religious_male_option[] = $value->religious;
                        }
                    }
                }
                $result['care_not_tobook'] = $xx;
                $result['care_not_tobook_n'] = array('Ethnicity_female_option'=>$Ethnicity_female_option,'Religious_female_option'=>$Religious_female_option,'Ethnicity_male_option'=>$Ethnicity_male_option,'Religious_male_option'=>$Religious_male_option);

                // get particpant Phones
                $result['phones'] = $this->basic_model->get_record_where('participant_phone', array('phone', 'primary_phone'), $where);

                // get particpant email
                $result['emails'] = $this->basic_model->get_record_where('participant_email', array('email', 'primary_email'), $where);


                echo json_encode(array('status' => true, 'data' => $result));
            } else {
                echo json_encode(array('status' => false, 'error' => 'Sorry no data found'));
            }
        }
    }

    public function get_participant_plans() {
        echo json_encode(array('status' => true, 'data' => ''));
    }

    /*
     * function get_participant_docs
     * return participant docs details like SIL docs and service docs
     * return type json
     */

    public function get_participant_docs() {
        // handle request and check permission to access participant
        $reqData = request_handler('access_participant');
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            //get participant docs, according filter
            $result = $this->Participant_profile_model->get_participant_docs($reqData);
            echo json_encode(array('status' => true, 'data' => $result));
        }
    }

    /*
     * function get_participant_sites
     * return participant address according filter request
     * return type json 
     */

    public function get_participant_sites() {
        // handle request and check permission to access participant
        $reqData = request_handler('access_participant');
        if (!empty($reqData->data)) {
            $participantID = $reqData->data->id;
            $viewBy = $reqData->data->view_by;

            // here 1 mean archive and 0 mean none archived
            $viewBy = ($viewBy == 1) ? 1 : 0;

            // get participant address (sites)
            $result = $this->Participant_profile_model->participant_sites($participantID, $viewBy);

            echo json_encode(array('status' => true, 'data' => $result));
        }
    }

    /*
     * function get_participant_strategies_care_notes
     * return participant care notes and strategies notes and filter notes according client side request
     * retunr type json
     */

    public function get_participant_strategies_care_notes() {
        // handle request and check permission to access participant
        $reqData = request_handler('access_participant');
        if (!empty($reqData->data)) {
            $participantID = $reqData->data->id;
            $type = $reqData->data->type;
            $categories = array('care_notes', 'strategies');

            // get participant notes
            $result = $this->Participant_profile_model->get_care_abouts($participantID, $type, $categories);

            echo json_encode(array('status' => true, 'data' => $result));
        }
    }

    /*
     * funciton get_participant_health_and_misc
     * return participant health notes and misc details like (medicare number and CRN number)
     * return type json
     */

    public function get_participant_health_and_misc() {
        // handle request and check permission to access participant
        $reqestData = request_handler('access_participant');
        $reqData = $reqestData->data;

        if (!empty($reqData->id)) {
            $participantID = $reqData->id;
            $type = $reqData->type;

            $categories = array('health_notes', 'diagnosis');

            // get participant health care notes
            $result = $this->Participant_profile_model->get_care_abouts($participantID, $type, $categories);

            // get participant medicare number and CRN number
            $misc = $this->basic_model->get_row('participant', $column = array('medicare_num', 'crn_num'), $where = array('id' => $participantID));
            $result['misc'] = $misc;

            echo json_encode(array('status' => true, 'data' => $result));
        } else {
            echo json_encode(array('status' => false, 'error' => 'Invliad participant id'));
        }
    }

    /*
     * function create_cares_notes
     * update and create participant health, care notes and strategies notes
     * return type json with true false
     */

    public function create_cares_notes() {
        // handle request
        $reqData = request_handler();
        $this->loges->setCreatedBy($reqData->adminId); // set created by in log

        if (!empty($reqData->data)) {
            $data = $reqData->data;

            // set notes data
            $this->form_validation->set_data((array) $data);

            // make validation rule
            $validation_rules = array(
                array('field' => 'content', 'label' => 'content', 'rules' => 'required|max_length[500]', 'errors' => ['max_length' => "Content field cannot exceed 500 characters."]),
                array('field' => 'categories', 'label' => 'categories', 'rules' => 'required'),
                array('field' => 'participantID', 'label' => 'categories', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            // check data is validate or not return type true and false  
            if ($this->form_validation->run()) {

                $update_data = array('title' => $data->title, 'content' => $data->content, 'categories' => $data->categories, 'updated' => DATE_TIME, 'participantID' => $data->participantID, 'primary_key' => (!empty($data->primary_key)) ? $data->primary_key : 0);
                if (!empty($data->notesID) && $data->notesID > 0) {
                    // check permission of update participant notes
                    check_permission($reqData->adminId, 'update_participant');

                    $this->loges->setTitle('Update ' . (str_replace("_notes", "", $data->categories)) . ' note: ' . $data->fullName);
                    $this->basic_model->update_records('participant_about_care', $update_data, $where = array('id' => $data->notesID));
                } else {
                    // check permission of create participant notes
                    check_permission($reqData->adminId, 'create_participant');

                    $this->loges->setTitle('Added new ' . (str_replace("_notes", "", $data->categories)) . ' note: ' . $data->fullName);
                    $update_data['created'] = DATE_TIME;
                    $this->basic_model->insert_records('participant_about_care', $update_data, $multiple = FALSE);
                }

                $this->loges->setUserId($data->participantID);
                $this->loges->setDescription(json_encode($reqData->data));
                $this->loges->createLog(); //create log

                $response = array('status' => true);
            } else {
                // return error in validate data
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    /*
     * function get_participant_preference
     * 1. return participant preference like activity and places 
     * 2. return data array of activiy and places
     * return type json
     */

    public function get_participant_preference() {
        // handle request and check permission to access participant
        $reqData = request_handler('access_participant');

        if (!empty($reqData->data)) {
            $data = $reqData->data;
            if (!empty($data->id)) {
                $participantID = $data->id;

                // get participant preference
                $result = $this->Participant_profile_model->pfeference_participant($participantID);
                echo json_encode(array('status' => true, 'data' => $result));
            } else {
                echo json_encode(array('status' => false, 'error' => 'Invalid participant Id'));
            }
        }
    }

    /*
     * function update_participant_preference
     * update participant preference like place and activity
     * return type json true or false
     */

    public function update_participant_preference() {
        // handle request and check permission to update participant prefereces
        $reqData = request_handler('update_participant');
        $this->loges->setCreatedBy($reqData->adminId);

        if (!empty($reqData->data)) {
            $data = $reqData->data;

            // set requested data for validate
            $this->form_validation->set_data((array) $data);

            // make validation rule
            $validation_rules = array(
                array('field' => 'type', 'label' => 'type', 'rules' => 'required'),
                array('field' => 'participantId', 'label' => 'participant id', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            // validate requested data return true and false
            if ($this->form_validation->run()) {
                // check its place or activity according to make key
                $key = ($data->type == 'places') ? 'placeId' : 'activityId';

                // check its place or activity according to select table
                $table = ($data->type == 'places') ? 'participant_place' : 'participant_activity';

                // delete previous preferences (place or activity)
                $this->basic_model->delete_records($table, $where = array('participantId' => $data->participantId));


                if (!empty($data->preference)) {
                    foreach ($data->preference as $val) {
                        // we are only save data of "favioute" and "least favioute"
                        // we are not save data of "none of these"
                        if ($val->type == 1 || $val->type == 2) {
                            $prefers[] = array('participantId' => $data->participantId, 'type' => $val->type, $key => $val->id);
                        }
                    }

                    // check it not empty
                    if (!empty($prefers)) {

                        // insert updated preferces
                        $this->basic_model->insert_records($table, $prefers, $multiple = true);
                    }
                }

                $this->loges->setUserId($data->participantId);
                $this->loges->setTitle("Update prefer " . $data->type . ": " . trim($data->fullName));
                $this->loges->setDescription(json_encode($data));
                $this->loges->createLog(); // create log

                $response = array('status' => true);
            } else {
                // return error
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    /*
     * function get_participant_booking_list
     * return participant bookers 
     * return type json data dictionary array
     */

    public function get_participant_booking_list() {
        // handle request and check permission to access participant
        $reqData = request_handler('access_participant');

        if (!empty($reqData->data)) {
            $data = $reqData->data;
            $participantID = $data->filtered->participantId;
            $view_by = $data->filtered->view_by;

            $archive_status = ($view_by === 'current') ? '0' : '1';
            $colown = array('id', 'firstname', 'lastname', 'relation', 'phone', 'email', 'created');
            $where = array('participantId' => $participantID, 'archive' => $archive_status);

            // get participant booker list
            $result = $this->basic_model->get_record_where('participant_booking_list', $colown, $where);

            echo json_encode(array('status' => true, 'data' => $result));
        }
    }

    /*
     * function update_participant_booking_list
     * create or update participant bookers
     * return type json true or false
     */

    public function update_participant_booking_list() {
        // handle request and check permission to update participant booker list
        $reqData = request_handler('update_participant');
        $this->loges->setCreatedBy($reqData->adminId);

        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            // set array of requested data
            $this->form_validation->set_data((array) $reqData);

            // make validation rule 
            $validation_rules = array(
                array('field' => 'firstname', 'label' => 'firstname', 'rules' => 'required'),
                array('field' => 'lastname', 'label' => 'lastname', 'rules' => 'required'),
                array('field' => 'email', 'label' => 'email', 'rules' => 'required'),
                array('field' => 'phone', 'label' => 'phone', 'rules' => 'required|callback_phone_number_check[phone,reuired,Booking List contact should be enter valid phone number.]'),
                array('field' => 'relation', 'label' => 'relation', 'rules' => 'required'),
                array('field' => 'participantId', 'label' => 'participant id', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            // check requested data return true or false
            if ($this->form_validation->run()) {

                // make array of booker
                $data = array('firstname' => $reqData->firstname, 'lastname' => $reqData->lastname, 'email' => $reqData->email, 'phone' => $reqData->phone, 'relation' => $reqData->relation, 'participantId' => $reqData->participantId);

                // if booker id come then update participant booker else create new booker
                if (!empty($reqData->id)) {
                    $data['created'] = DATE_TIME;

                    // update booker
                    $this->basic_model->update_records('participant_booking_list', $data, $where = array('id' => $reqData->id));
                    $this->loges->setTitle("Update booker : " . trim($reqData->fullName));
                } else {

                    // create booker
                    $this->basic_model->insert_records('participant_booking_list', $data, $multiple = false);
                    $this->loges->setTitle("Added new booker :" . trim($reqData->fullName));
                }

                $this->loges->setUserId($reqData->participantId);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->createLog(); // create log

                $response = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    /*
     * function get_participant_upcoming_shift
     * return participant upcoming shift like unfiller, unconfirmed, confirmed and cancelled shift
     * return type json data dictionary
     */

    public function get_participant_upcoming_shift() {
        // handle request and check permission to access participant
        $reqData = request_handler('access_participant');
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            // get participant upcoming shift
            $result = $this->Participant_profile_model->participant_upcoming_shifts($reqData);

            echo json_encode(array('status' => true, 'data' => $result));
        }
    }

    /*
     * function archive_participant_docs
     * archive participant docs like SIL and service docs
     * return type json true and false
     */

    public function archive_participant_docs() {
        // handle request and check permission to archive participant docs
        $reqData = request_handler('update_participant');
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            if (!empty($reqData->docs)) {
                foreach ($reqData->docs as $val) {
                    if (!empty($val->is_active)) {

                        $updateData = array('archive' => 1);
                        // update doc status 1 
                        $this->basic_model->update_records('participant_docs', $updateData, $where = array('id' => $val->id));
                    }
                }
            }

            echo json_encode(array('status' => true));
        }
    }

    /*
     * function upload_participant_docs
     * create new participant docs like SIL and Service docs
     * return type json 
     */

    public function upload_participant_docs() {
        // handle request and check permission to upload participant docs
        $data = request_handlerFile('create_participant');

        // include participant docs class
        require_once APPPATH . 'Classes/participant/ParticipantDocs.php';
        $docObj = new ClassParticipantDocs\ParticipantDocs();

        // set requested data for validate
        $this->form_validation->set_data((array) $data);

        // make validation rule
        $validation_rules = array(
            array('field' => 'docsTitle', 'label' => 'title', 'rules' => 'required'),
            array('field' => 'participantId', 'label' => 'participant id', 'rules' => 'required'),
            array('field' => 'doc_type', 'label' => 'doc type', 'rules' => 'required'),
            array('field' => 'docsCategory', 'label' => 'Docs Category', 'rules' => 'required'),
            array('field' => 'expiry', 'label' => 'Docs Expiry Date', 'rules' => 'required'),
        );

        // set rules form validation
        $this->form_validation->set_rules($validation_rules);

        // check requested data return true or false
        if ($this->form_validation->run()) {
            if (!empty($_FILES) && $_FILES['myFile']['error'] == 0) {

                $docObj->setFilename($_FILES['myFile']['name']);
                $docObj->setParticipantid($data->participantId);
                $docObj->setTitle($data->docsTitle);
                $docObj->setCreated(DATE_TIME);

                $docObj->setType(($data->doc_type == 'service_docs') ? 1 : 2); //check type of doc service or SIL docs
                $docObj->setArchive(0);
                $docObj->setExpiryDate($data->expiry);
                $docObj->setCategoryType($data->docsCategory);

                $dub_result = $docObj->checkDublicateDocs(); // check its duplicate

                $config['upload_path'] = PARTICIPANT_UPLOAD_PATH; // user here constact for specific path
                $config['input_name'] = 'myFile';
                $config['directory_name'] = $data->participantId;
                $config['allowed_types'] = 'jpg|jpeg|png|xlx|xls|doc|docx|pdf|pages';

                $is_upload = do_upload($config); // upload file
                // check here file is uploaded or not return key error true
                if (isset($is_upload['error'])) {
                    // return error comes in file uploading
                    echo json_encode(array('status' => false, 'error' => strip_tags($is_upload['error'])));
                    exit();
                } else {
                    $docObj->setFilename($is_upload['upload_data']['file_name']);
                    $docObj->createFileData(); // insert data related doc

                    if (!$dub_result['status']) {
                        $return = array('status' => true, 'warn' => $dub_result['warn']);
                    } else {
                        $return = array('status' => true);
                    }
                }
            } else {
                $return = array('status' => false, 'error' => 'Error in uploading file.');
            }
        } else {
            // return error else data data not valid
            $errors = $this->form_validation->error_array();
            $return = array('status' => false, 'error' => implode(', ', $errors));
        }

        echo json_encode($return);
    }

    /*
     * function get_participant_roster
     * 
     */

    public function get_participant_roster() {
        $reqData = request_handler('access_schedule');
        require_once APPPATH . 'Classes/shift/Shift.php';
        $objShift = new ShiftClass\Shift();

        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $request = json_decode($reqData->Request);
            $participantId = $reqData->participantId;

            $result = $objShift->get_roster_listing($request, $participantId);
            echo json_encode($result);
        }
    }

    public function get_participant_goals() {
        $reqData = request_handler('access_participant');
        if (!empty($reqData->data)) {
            $data = $reqData->data;
            $participantID = $data->id;
            $view_by = $data->view_by;


            $result = $this->Participant_profile_model->get_participant_goals($participantID, $view_by);
            echo json_encode(array('status' => true, 'data' => $result));
        }
    }

    public function get_participant_goal_history() {
        $reqData = request_handler('access_participant');
        if (!empty($reqData->data)) {
            $data = $reqData->data;
            $participantID = $data->id;

            $result = $this->Participant_profile_model->get_participant_goal_history($participantID);
            echo json_encode(array('status' => true, 'data' => $result));
        }
    }

    function get_participant_fms() {
        $reqestData = request_handler('access_fms', 1, 1);
        if (!empty($reqestData->data)) {
            $reqData = json_decode($reqestData->data);
            $result = $this->Participant_profile_model->get_participant_fms($reqData, $reqestData->adminId);
            echo json_encode($result);
        }
    }

    public function download_selected_file() {
        $request = request_handler('access_participant');
        $responseAry = $request->data;
        if (!empty($responseAry)) {
            $this->load->library('zip');

            $participantId = $responseAry->participantId;
            $download_data = $responseAry->downloadData;
            $this->zip->clear_data();

            $x = '';
            $file_count = 0;
            if (!empty($download_data)) {
                $zip_name = time() . '_' . $participantId . '.zip';
                foreach ($download_data as $file) {
                    if (isset($file->is_active) && $file->is_active) {
                        $file_path = PARTICIPANT_UPLOAD_PATH . $participantId . '/' . $file->filename;
                        $this->zip->read_file($file_path, FALSE);
                        $file_count = 1;
                    }
                }
                $x = $this->zip->archive('archieve/' . $zip_name);
            }


            if ($x && $file_count == 1) {
                echo json_encode(array('status' => true, 'zip_name' => $zip_name));
                exit();
            } else {
                echo json_encode(array('status' => false, 'error' => 'Please select atleast one file to continue.'));
                exit();
            }
        }
    }

    public function archive_booker() {
        $requestData = request_handler('access_participant');
        $reqData = $requestData->data;

        if (!empty($reqData)) {
            $updateData = array('archive' => '1');
            $this->basic_model->update_records('participant_booking_list', $updateData, $where = array('id' => $reqData->bookerId, 'participantId' => $reqData->participantId));

            echo json_encode(array('status' => true));
        }
    }

    function export_goal_report() {
        $requestData = request_handler('access_participant');
        $reqData = $requestData->data;

        if (!empty($reqData)) {
            $participantId = $reqData->id;

            $response = $this->Participant_profile_model->get_participant_goal_history_report($participantId);

            $this->load->library('m_pdf');
            $pdf = $this->m_pdf->load();
            $html = '<div style="text-align:left; border-bottom:1px solid #000; padding-bottom:10px; margin-bottom:15px;"><img src="./uploads/assets/ocs_logo.svg"></div>';

            if (!empty($response['data'])) {
                foreach ($response['data'] as $key => $value) {

                    $html .= '<div style="border-bottom:1px solid #000; font-size:19px; margin-top:25px;"><b>Title:</b><span style="font-weight:300;">' . $value->title . '</span></div><table width="100%" style="font-family: sans-serif; margin-top:15px; margin-bottom:5px;" ><tr><td><p style="font-size:15px;"><b>Start Date</b> ' . date("d/m/Y", strtotime($value->start_date)) . '</p></td><td align="right"><p style="font-size:15px;"><b>End Date</b> ' . date("d/m/Y", strtotime($value->end_date)) . '</p></td></tr></table>';

                    if (!empty($value->rating)) {
                        $html .= '<table border="1" cellpadding="5px"  width="100%" style="border-collapse: collapse; border: 1px solid #880000; font-family: Mono; font-size: 12px; font-family: sans-serif; font "><thead><tr> <th>Date</th> <th>Rating</th></tr></thead><tbody>';
                        foreach ($value->rating as $rating) {
                            $html .= '<tr><td>' . date("d/m/Y", strtotime($rating->created)) . '</td> <td>' . $rating->name . '</td></tr>';
                        }
                        $html .= '</tbody></table>';
                    }
                }
                $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . date('d-m-Y H:i:s')); // Add a footer
                $pdf->WriteHTML($html); // write the HTML into the PDF
                $rand = rand(100000, 5000000);
                $filename = $rand . '.pdf';
                $pdfFilePath = './archieve/' . $filename;
                $pdf->Output($pdfFilePath, 'F');
                echo json_encode(array('status' => true, 'parthName' => $pdfFilePath));
            } else {
                echo json_encode(array('status' => false));
            }
        }
    }

    function update_booking_data() {
        $requestData = request_handler('update_participant');
        $this->loges->setCreatedBy($requestData->adminId);

        $reqData = $requestData->data;

        if (!empty($reqData)) {
            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'participantId', 'label' => 'participant id', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $data = array('booking_status' => $reqData->booking_status, 'booking_date' => DateFormate("Y-m-d", $reqData->booking_date));
                $where = array('id' => $reqData->participantId);

                $this->basic_model->update_records('participant', $data, $where);

                $this->loges->setTitle("Update booking status :" . trim($reqData->fullName));
                $this->loges->setUserId($reqData->participantId);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->createLog();

                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($return);
        }
    }

    function get_contact_log() {
        $this->load->model('Participant_dashboard_model');
        $requestData = request_handler('access_participant');

        $reqData = $requestData->data;
        if (!empty($reqData)) {
            $result = $this->Participant_dashboard_model->get_imail_call_list($reqData);
            echo json_encode($result);
        }
    }

    function archive_participant_address() {
        $requestData = request_handler('delete_participant');
        $reqData = $requestData->data;

        if (!empty($reqData)) {
            $result = $this->basic_model->update_records('participant_address', ['archive' => 1], ['id' => $reqData->id]);
            echo json_encode(['status' => true]);
        }
    }

    function archive_participant_about_care() {
        $requestData = request_handler('delete_participant');
        $reqData = $requestData->data;

        if (!empty($reqData)) {
            $where = ['id' => $reqData->id, 'primary_key' => 1, 'participantId' => $reqData->participantId];
            $result = $this->basic_model->get_row('participant_about_care', ['categories'], $where);
           
            if (!empty($result)) {
                $where_ab = ['categories' => $result->categories, 'primary_key' => 2, 'participantId' => $reqData->participantId, 'archive' => 0];
                $seconday_about = $this->basic_model->get_row('participant_about_care', ['id', 'categories'], $where_ab);
                
                
                if (!empty($seconday_about)) {
                    $this->basic_model->update_records('participant_about_care', ['primary_key' => 1], ['id' => $seconday_about->id]);
                } else {
                    echo json_encode(['status' => false, 'error' => 'Secondary Diagnosis note no found for move secondary to primary']);
                    exit();
                }
            }

            $this->basic_model->update_records('participant_about_care', ['archive' => 1], ['id' => $reqData->id]);
            echo json_encode(['status' => true]);
            exit();
        }
    }

}
