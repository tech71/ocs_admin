<?php


defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class CrmSchedule extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->load->model('CrmSchedule_model');
        $this->load->model('Dashboard_model');
        $this->loges->setModule(10);
    }


    public function schedules_calendar_data(){
      $this->load->model('CrmSchedule_model');
      $reqData = request_handler();
      if (!empty($reqData->data)) {
          // $reqData = json_decode($reqData->data);
          $response = $this->CrmSchedule_model->schedules_calendar_data($reqData);
          echo json_encode($response);
      }
    }


}
