<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CrmStaff_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    public function user_management_list($reqData) {
      $limit = $reqData->pageSize;
      $page = $reqData->page;
      $sorted = $reqData->sorted;
      $filter = $reqData->filtered;
      $orderBy = array();
      $direction = '';

      if(!empty(request_handler()))
      {
          $tbl_1 = TBL_PREFIX . 'admin';
          $tbl_2 = TBL_PREFIX . 'crm_department';
          $dt_query = $this->db->select(array($tbl_1.'.username',$tbl_1.'.status',$tbl_1.'.id as ocs_id',$tbl_1.'.position',$tbl_1.'.status',"CONCAT(tbl_admin.firstname,' ',tbl_admin.lastname) AS name",$tbl_1.'.firstname',$tbl_1.'.lastname',$tbl_1.'.created',$tbl_2.'.name as department'));

          $this->db->from($tbl_1);
          $this->db->join('tbl_crm_staff', 'tbl_crm_staff.admin_id = tbl_admin.id', 'inner');
          $this->db->join('tbl_crm_department', 'tbl_crm_department.id = tbl_admin.department', 'left');
          $this->db->where($tbl_1.'.archive=',"0");

          //pr($sorted);


          if (!empty($sorted)) {
              if (!empty($sorted[0]->id)) {
                  if ($sorted[0]->id) {
                      $orderBy = $sorted[0]->id;
                  } else {
                      $orderBy = 'tbl_admin.' . $sorted[0]->id;
                  }
                  $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
              }
          } else {
              $orderBy = 'tbl_admin.id';
              $direction = 'desc';
          }
          if(!empty($filter)){
            if ($filter->inactive) {
                  $status_con = array(0, 1);
              }

            //if (!empty($filter->filterVal)) {
                  if($filter->filterVal=='All'){
                     $this->db->where($tbl_1.'.status=',1);
                     $this->db->or_where($tbl_1.'.status=',0);
                  } else {
                    $this->db->where($tbl_1.'.status=',$filter->filterVal);
                  }
              //}


            if (!empty($filter->search))
            {

                $this->db->group_start();
                $src_columns = array($tbl_1.'.username',$tbl_1.'.id as ocs_id',$tbl_1.'.position',$tbl_1.'.firstname',$tbl_1.'.lastname',"CONCAT(tbl_admin.firstname,' ',tbl_admin.lastname) as name",$tbl_2.'.name');

                for ($i = 0; $i < count($src_columns); $i++)
                {
                    $column_search = $src_columns[$i];
                    if (strstr($column_search, "as") !== false) {
                        $serch_column = explode(" as ", $column_search);
                        $this->db->or_like($serch_column[0],$filter->search);
                    } else {
                        $this->db->or_like($column_search,$filter->search);
                    }
                }
                $this->db->group_end();
            }

          }
          $this->db->order_by($orderBy, $direction);
          $this->db->limit($limit, ($page * $limit));
          $query = $this->db->get();
        //  echo $this->db->last_query();
          //last_query();

          $dt_filtered_total = $all_count =  $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
          if ($dt_filtered_total % $limit == 0) {
              $dt_filtered_total = ($dt_filtered_total / $limit);
          } else {
              $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
          }

          $x = $query->result_array();

          $site_ary = array();
          foreach ($x as $key => $value)
          {
              $staff_id =  $value['ocs_id'];
              $temp['ocs_id'] = $staff_id;
              $temp['name'] = $value['name'];
              $temp['username'] = $value['username'];
              $temp['firstname'] = $value['firstname'];
              $temp['lastname'] = $value['lastname'];
              $temp['position'] = $value['position'];
              $temp['department'] = $value['department'];
              $temp['service_area'] = 'NDIS';
              $temp['user_status'] =$value['status'];
              $temp['created'] = $value['created']!='0000-00-00 00:00:00'?date('d/m/Y',strtotime($value['created'])):'';
              $temp['updated'] = $value['created']!='0000-00-00 00:00:00'?date('d/m/Y',strtotime($value['created'])):'';

              /*Get Staff phone no*/
              $z = $this->Basic_model->get_result('admin_phone',array('tbl_admin_phone.adminId'=>$staff_id,'tbl_admin_phone.archive'=>'0'),$columns=array('tbl_admin_phone.phone','tbl_admin_phone.primary_phone','tbl_admin_phone.id'));
              $ph_temp = array();
              if(!empty($z))
              {
                  foreach ($z as $key => $valPh)
                  {
                      $ph_temp[] = array('name'=>$valPh->phone,'id'=>$valPh->id,'primary_phone'=>$valPh->primary_phone);
                  }
              }
              else{
                $ph_temp[] = array('name'=>'');
              }
              $temp['PhoneInput'] = $ph_temp;
              /*Get staff email*/
              $zMail = $this->Basic_model->get_result('admin_email',array('tbl_admin_email.adminId'=>$staff_id,'tbl_admin_email.archive'=>'0'),$columns=array('tbl_admin_email.email','tbl_admin_email.primary_email','tbl_admin_email.id'));
              $email_temp = array();
              if(!empty($zMail))
              {
                  foreach ($zMail as $key => $valmail)
                  {
                      $email_temp[] = array('name'=>$valmail->email,'id'=>$valmail->id,'primary_email'=>$valmail->primary_email);
                  }
              }
              else{
                $email_temp[] = array('name'=>'');
              }
              $temp['EmailInput'] = $email_temp;

              /*Get alloted department*/
              $zt_query = $this->db->select(array('tbl_crm_staff_department_allocations.admin_id','tbl_crm_staff_department_allocations.allocated_department','tbl_crm_staff_department_allocations.id','tbl_crm_staff_department_allocations.status','tbl_crm_department.name','tbl_crm_department.id as recruit_id'));
              $this->db->from('tbl_crm_staff_department_allocations');
              $this->db->join('tbl_crm_department', 'tbl_crm_staff_department_allocations.allocated_department = tbl_crm_department.id', 'left');
              $this->db->where('tbl_crm_staff_department_allocations.status=',"1");
              $this->db->where('tbl_crm_staff_department_allocations.admin_id=',$staff_id);
              $zt_query_query = $this->db->get();
              //last_query();
              $z_dept = $zt_query_query->result_array();
              $department = array();
              if(!empty($z_dept))
              {
                  foreach ($z_dept as $key => $valDept)
                  {
                      #$department[] = array('value'=>$valDept['recruit_id'],'id'=>$valDept['recruit_id'],'text'=>$valDept['name'],'name'=>$valDept['name']);
                      #$department[] = array($valDept['recruit_id']);
                      $department[] = $valDept['recruit_id'];
                  }
              }
              $department = array_filter($department);
              $temp['alloted_dept'] = !empty($department)? $department : '';

              $site_ary[] = $temp;
          }
          $return = array('data' => $site_ary,'count' => $dt_filtered_total);
          return $return;
        }
      }

      public function get_staff_name($post_data) {

        $this->db->select("CONCAT(tbl_admin.firstname,' ',tbl_admin.lastname) as staffName, tbl_crm_staff.admin_id as id");
        $this->db->from('tbl_crm_staff');
        $this->db->join('tbl_admin', 'tbl_admin.id = tbl_crm_staff.admin_id', 'left');
        $this->db->or_like('tbl_admin.firstname', $post_data);
        $this->db->or_like('tbl_admin.lastname', $post_data);

        $this->db->where('archive =', 0);
        $this->db->where('status=', 1);
        $query = $this->db->get();
        $staff_rows = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $staff_rows[] = array('label' => $val->staffName, 'value' => $val->id);
            }
        }
        return $staff_rows;
    }

    public function get_all_ocs_users($post_data) {
      $this->db->select("CONCAT(tbl_admin.firstname,' ',tbl_admin.lastname) as staffName, tbl_admin.id as id, tbl_admin.id as hcm_id,
      tbl_admin.position as position,tbl_admin.username as username, tbl_admin_email.email as email , tbl_admin_phone.phone as phone ");
      $this->db->from('tbl_admin');
      $this->db->join('tbl_admin_email', 'tbl_admin_email.id = tbl_admin.id', 'left');
      $this->db->join('tbl_admin_phone', 'tbl_admin_phone.id = tbl_admin.id', 'left');
      $this->db->or_like('tbl_admin.firstname', $post_data);
      $this->db->or_like('tbl_admin.lastname', $post_data);

      $this->db->where('tbl_admin.archive =', 0);
      $this->db->where('tbl_admin.status=', 1);
      $query = $this->db->get();
      $staff_rows = array();
      if (!empty($query->result())) {
          foreach ($query->result() as $val) {
              $staff_rows[] = array('label' => $val->staffName, 'value' => $val->id, 'data'=>$val);

          }
      }
      return $staff_rows;
    }

    public function staff_details($staffId){
      $select_column = array(
          'tbl_admin.id',
          'tbl_admin.id as ocs_id',
          'tbl_admin.position',
          'tbl_admin.status',
          'tbl_admin.profile',
          'concat(tbl_admin.firstname," ",tbl_admin.lastname) as FullName',
          'tbl_crm_department.name as DepartmentName',
          'tbl_admin.created as start_date',
          'tbl_admin.created as end_date',
          'tbl_admin_phone.phone',
          'tbl_admin_email.email',

        );
        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from(TBL_PREFIX . 'admin');
        $this->db->join('tbl_admin_email', 'tbl_admin_email.adminId = tbl_admin.id AND tbl_admin_email.primary_email = 1', 'left');
        $this->db->join('tbl_admin_phone', 'tbl_admin_phone.adminId = tbl_admin.id AND tbl_admin_phone.primary_phone = 1', 'left');

        $this->db->join('tbl_crm_department', 'tbl_crm_department.id = tbl_admin.department', 'left');
        $sWhere = array(TBL_PREFIX . 'admin.id' => $staffId);
        $this->db->where($sWhere);

        $this->db->group_by('tbl_admin.id');

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $dept_list = $this->db->query('select name as deptList from tbl_crm_department')->result_array();
        $select_assign_participant = array(
          'concat(tbl_crm_participant.firstname," ",tbl_crm_participant.lastname) as FullName',
          'tbl_crm_participant_stage.stage_id',
        );
        $this->db->select($select_assign_participant);
        $this->db->from('tbl_crm_participant');
        $this->db->join('tbl_admin','tbl_admin.id = tbl_crm_participant.assigned_to', 'left');
        $this->db->join('tbl_crm_participant_stage','tbl_crm_participant_stage.crm_participant_id = tbl_crm_participant.id', 'left');
        $sWhere = array(TBL_PREFIX . 'admin.id' => $staffId);
        $this->db->where($sWhere);
        $this->db->where("`tbl_crm_participant_stage`.`stage_id` = (SELECT MAX(`tbl_crm_participant_stage`.`stage_id`)
               FROM `tbl_crm_participant_stage`
               WHERE `tbl_crm_participant_stage`.`crm_participant_id` = `tbl_crm_participant`.`id`)");
        $assigned_participant = $this->db->get();
        $assigned = array();
        $assign = array();
        $user_name = $query->row_array();
        if (!empty($assigned_participant->result())) {
            foreach ($assigned_participant->result() as $val) {
              $assigned['FullName'] = $val->FullName;
              $assigned['stage_id'] = $val->stage_id;
              $assigned['user'] = $user_name['FullName'];
              $assign[] = $assigned;
            }
          }
        $select_task = array(
          'concat(tbl_crm_participant.firstname," ",tbl_crm_participant.lastname) as FullName',
          'tbl_admin.id as hcmgr_id',
          'tbl_admin.status',
          'tbl_crm_participant_stage.stage_id',
          'tbl_crm_participant_schedule_task.relevant_task_note as note',
          'DATE(tbl_crm_participant_schedule_task.created_at) as date',
          'tbl_crm_participant_schedule_task.task_name',
          'tbl_crm_participant_schedule_task.id as task_id',
          'tbl_crm_participant_schedule_task.priority',
          'tbl_crm_participant.prefer_contact'
        );
        $this->db->select($select_task);
        $this->db->from('tbl_crm_participant_schedule_task');
        $this->db->join('tbl_crm_participant','tbl_crm_participant.id = tbl_crm_participant_schedule_task.crm_participant_id', 'left');
        $this->db->join('tbl_admin','tbl_admin.id = tbl_crm_participant_schedule_task.assign_to', 'left');
        $this->db->join('tbl_crm_participant_stage','tbl_crm_participant_stage.crm_member_id = tbl_admin.id', 'left');
        $sWhere = array(TBL_PREFIX . 'admin.id' => $staffId);
        $this->db->where($sWhere);
        $task = $this->db->get();
        $task_list = $task->result_array();
        $row = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {

                $row['id'] = $val->id;
                $row['hcmgr_id'] = $val->ocs_id   ;
                $row['status'] = $val->status;
                $row['FullName'] = $val->FullName;
                $row['DepartmentName'] = $val->DepartmentName;
                $row['start_date'] = $val->start_date;
                $row['email'] = $val->email;
                $row['phone'] = $val->phone;
                $row['position'] = $val->position;
                $row['profile_image'] = $val->profile;
                $row['end_date'] = $val->end_date;
                //$row['DepartmentList'] = $dept_list;
                $row['task_list'] = $task_list;
                $row['assigned_participant'] = $assign;
                $row['action'] = isset($val->action_status) && $val->action_status == 1 ? 'Phone Screening' : 'Call';

              }
          }

          return $row;
    }




      public function get_all_staffs($reqData) {
        $orderBy = 'tbl_crm_staff.id';
        $direction = 'DESC';
        $status_con = array(1);
        $src_columns = array(
            'tbl_crm_staff.id',
            'tbl_crm_staff.hcmgr_id',
            'concat(tbl_crm_staff.preferredname," ",tbl_crm_staff.firstname," ",tbl_crm_staff.lastname) as FullName',
            'tbl_crm_department.name',
            'tbl_crm_staff.start_date',
            'tbl_crm_staff.end_date'
        );


        $where = '';
        $where = "tbl_crm_staff.archive = 0";

        $sWhere = $where;


          $select_column = array(
              'tbl_crm_staff.id',
              'concat(tbl_crm_staff.preferredname," ",tbl_crm_staff.firstname," ",tbl_crm_staff.lastname) as FullName',
            );
            $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
            $this->db->from(TBL_PREFIX . 'crm_staff');
            $this->db->where_in('tbl_crm_staff.account_status', $status_con);
            $this->db->order_by($orderBy, $direction);
            $this->db->group_by('tbl_crm_staff.id');
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

            $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
           $dataResult = array();
            if (!empty($query->result())) {
                foreach ($query->result() as $val) {
                    $row = array();
                    $row['value'] = $val->id;
                    $row['label'] = $val->FullName;
                    $dataResult[] = $row;
                  }
              }
              $return = array('count' => $dt_filtered_total, 'data' => $dataResult);
              return $return;

      }


      public function get_staff_disable($reqData) {
        $orderBy = 'tbl_crm_staff.id';
        $direction = 'DESC';
        $status_con = array(1);
        $src_columns = array(
            'tbl_crm_staff.id',
            'tbl_crm_staff.hcmgr_id',
            'concat(tbl_crm_staff.preferredname," ",tbl_crm_staff.firstname," ",tbl_crm_staff.lastname) as FullName',
            'tbl_crm_department.name',
            'tbl_crm_staff.start_date',
            'tbl_crm_staff.end_date',

        );


        $where = '';
        $where = "tbl_crm_staff.archive = 0";
        $where = "tbl_crm_staff_disable.crm_staff_id = 10";
        $sWhere = $where;


          $select_column = array(
              'tbl_crm_staff.id',
              'concat(tbl_crm_staff.preferredname," ",tbl_crm_staff.firstname," ",tbl_crm_staff.lastname) as FullName',
              'tbl_crm_staff_disable.disable_account',
              'tbl_crm_staff_disable.account_allocated_to',
              'tbl_crm_staff_disable.account_allocated',
            );
            $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
            $this->db->from(TBL_PREFIX . 'crm_staff');
            $this->db->join('tbl_crm_staff_disable','tbl_crm_staff.id = tbl_crm_staff_disable.crm_staff_id', 'left');
          //  $this->db->where_in('tbl_crm_staff.account_status', $status_con);
            $this->db->where($sWhere);
            $this->db->order_by($orderBy, $direction);
            $this->db->group_by('tbl_crm_staff.id');
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

            $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
            $dataResult = array();
            $val=$query->row();
            if (!empty($val)) {
                   $row = array();
                    $row['staff_id'] = $val->id;
                    $row['staff_name'] = $val->FullName;
                    $row['disableAccount'] = $val->disable_account;
                    $row['account_allocated_staff_to'] = $val->account_allocated_to;
                    $row['allocatedAccount'] = $val->account_allocated;
                    $dataResult[] = $row;
              }
              $return =  $dataResult;
              return $return;
      }

      public function assign_staff_member($reqData){
        $details = (array) $reqData->data;
        $staff_details = array('department_id'=>$details['department_id']);
        $this->db->where($where = array('id' => $details['id']));
        $this->db->update(TBL_PREFIX . 'crm_staff', $staff_details);

        return true;
      }
      function enable_crm_user($reqData){
        $where=array('crm_staff_id'=>$reqData);
        $crm_user_id = $this->Basic_model->get_record_where('crm_staff_disable', 'id,crm_staff_id',$where);
        $swhere = array('id'=>$reqData);
        $table='crm_staff_disable';

        if($crm_user_id){

            $result = $this->Basic_model->delete_records($table,$where);

        }
        $this->Basic_model->update_records('admin',array('status'=>'1'),array('id'=>$reqData));
        return true;
      }
      public function update_alloted_department($reqData)
     {
         if(!empty($reqData))
         {
             $this->basic_model->update_records('crm_staff_department_allocations', array('status'=>'0'), array('admin_id'=>$reqData->staffId));

             #pr($reqData->selectedData->options);
             foreach ($reqData->selectedData->options as $key => $myVal)
             {
                 if($myVal->value!=null)
                 {
                     $tbl = 'tbl_crm_staff_department_allocations';
                     $dt_query = $this->db->select(array($tbl.'.id'));
                     $this->db->from($tbl);
                     $sWhere = array($tbl.'.admin_id'=>$reqData->staffId,$tbl.'.allocated_department'=>$myVal->value,$tbl.'.status'=>'0');
                     $this->db->where($sWhere, null, false);
                     $query = $this->db->get();
                     #echo $this->db->last_query();
                     $tbl = 'crm_staff_department_allocations';
                     $row = $query->row_array();
                     if($row)
                     {
                         $this->Basic_model->update_records($tbl, array('status'=>'1'), array('tbl_crm_staff_department_allocations.id'=>$row['id']));
                         #echo $this->db->last_query();
                     }
                     else
                     {
                         $this->Basic_model->insert_records($tbl, array('admin_id'=>$reqData->staffId,'allocated_department'=>$myVal->value,'status'=>'1','created'=>DATE_TIME));
                         #echo $this->db->last_query();
                     }
                 }
             }

             $tbl = 'tbl_crm_staff_department_allocations';
             $dt_query = $this->db->select(array($tbl.'.allocated_department'));
             $this->db->from($tbl);
             $sWhere = array($tbl.'.admin_id'=>$reqData->staffId,$tbl.'.status'=>'1');
             $this->db->where($sWhere, null, false);
             $query = $this->db->get();

             $z_dept = $query->result_array();
             $department = array();
             if(!empty($z_dept))
             {
                 foreach ($z_dept as $key => $valDept)
                 {
                     $department[] = $valDept['allocated_department'];
                 }
             }
             $department = array_filter($department);
             return $department;
         }
     }
    }
