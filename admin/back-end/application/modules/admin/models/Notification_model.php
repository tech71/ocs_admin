<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_all_notification($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';
        $tbl_notification = TBL_PREFIX . 'notification';
        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'id';
            $direction = 'desc';
        }

        $notification_columns = array($tbl_notification . ".id", $tbl_notification . ".created", $tbl_notification . ".title", $tbl_notification . ".shortdescription");
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $notification_columns)), false);

        $this->db->select("(case when(user_type = 1) 
        THEN 		
        (select concat(firstname,' ',lastname)  from tbl_member where id= userId)
        ELSE
          (select concat(firstname,' ',lastname) from tbl_participant where id= userId)  
        END) as username");

        $this->db->select("(case when(user_type = 1) 
        THEN 		
        'Member'
        ELSE
          'Participant'
        END) as user_type");

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));
        $this->db->from($tbl_notification);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $return = array('count' => $dt_filtered_total, 'data' => $query->result());
        return $return;
    }

    function get_notification_alert() {
//        $limit = 10;
//        $page = 0;
        $tbl_notification = TBL_PREFIX . 'notification';

        $notification_columns = array($tbl_notification . ".id", $tbl_notification . ".created", $tbl_notification . ".title", $tbl_notification . ".shortdescription");
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $notification_columns)), false);

        $this->db->select("(case when(user_type = 1) 
        THEN 		
        (select concat(firstname)  from tbl_member where id= userId)
        ELSE
          (select concat(firstname) from tbl_participant where id= userId)  
        END) as username");

        $this->db->select("(case when(user_type = 1) 
        THEN 'Member'
        ELSE
          'Participant'
        END) as user_type");

//        $this->db->limit($limit);
        $this->db->from($tbl_notification);
        $this->db->where('sender_type', '1');
        $this->db->where('status', '0');
        $this->db->order_by('created', 'DESC');
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        $return = array('count' => $dt_filtered_total, 'data' => $query->result());
        return $return;
    }

    function clear_all_notification() {
        $tbl_notification = TBL_PREFIX . 'notification';
        $this->db->where('sender_type', '1');
        $this->db->update($tbl_notification, array("status" => 1));
        return array('status' => true);
    }

    public function get_external_imail_notification($currentAdminId) {


        $select_colown = array('tbl_external_message.id', 'tbl_external_message.title', 'tbl_external_message_content.content', 'tbl_external_message_content.created', 'tbl_external_message_content.sender_type', 'tbl_external_message_content.id as contentId');

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_colown)), false);


        $this->db->select("CASE tbl_external_message_content.sender_type
            WHEN 1 THEN (select concat(firstname,' ',lastname,'||',profile,'||',gender,'||',id) from tbl_admin where id = tbl_external_message_content.userId)
            WHEN 2 THEN (select concat(firstname,' ', middlename,' ',lastname,'||', profile_image,'||',gender,'||',id) from tbl_participant where id = tbl_external_message_content.userId)
            WHEN 3 THEN (select concat(firstname,' ', middlename,' ',lastname,'||', profile_image,'||',gender,'||',id)  from tbl_member where id = tbl_external_message_content.userId)
            WHEN 4 THEN (select concat(name,'||', logo_file,'||',id)  from tbl_organisation where id = tbl_external_message_content.userId)
            ELSE NULL
            END as user_data");


        $this->db->from('tbl_external_message');
        $this->db->join('tbl_external_message_content', 'tbl_external_message.id = tbl_external_message_content.messageId', 'left');
        $this->db->join('tbl_external_message_recipient', 'tbl_external_message_recipient.messageContentId = tbl_external_message_content.id AND tbl_external_message_recipient.recipinent_type = 1', 'left');


        $this->db->where('tbl_external_message_recipient.is_read', 0);
        $this->db->where('tbl_external_message_recipient.is_notify', 0);
        $this->db->where('tbl_external_message_recipient.recipinentId', $currentAdminId);
        $this->db->where('tbl_external_message_content.is_draft', 0);

        $this->db->order_by('tbl_external_message_content.created', 'desc');

        $query = $this->db->get();
//        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        $result = $query->result();

//        last_query();
        $ext_msg = array();
        if (!empty($result)) {
            foreach ($result as $key => $val) {
                $x['id'] = $val->id;
                $x['title'] = $val->title;
                $x['mail_date'] = $val->created;
                $x['contentId'] = $val->contentId;
                $x['content'] = setting_length($val->content, 100);
                $x['redirect_uri'] = '/admin/imail/external/inbox/' . $val->id;
                $sender_type = $val->sender_type;
                $x['type'] = 'external';
                $user_data = explode('||', $val->user_data);

                if (count($user_data) > 1) {
                    // here 0 = user name 
                    $x['user_name'] = $user_data[0];

                    if ($sender_type == 1) {

                        // here 3 = user id // 1 = profile img name // 2 = gender
                        $x['user_img'] = get_admin_img($user_data[3], $user_data[1], $user_data[2]);
                    } else if ($sender_type == 2) {

                        // here 3 = user id  // 1 = profile img name  // 2 = gender
                        $x['user_img'] = get_participant_img($user_data[3], $user_data[1], $user_data[2]);
                    } else if ($sender_type == 3) {

                        // here 3 = user id  // 1 = profile img name  // 2 = gender
                        $x['user_img'] = get_admin_img($user_data[3], $user_data[1], $user_data[2]);
                    } else if ($sender_type == 4) {

                        // here 2 = user id  // 1 = profile img name
                        $x['user_img'] = get_org_img($user_data[2], $user_data[1]);
                    }
                }

                $ext_msg[] = $x;
            }
        }

        return $ext_msg;
    }

    function get_internal_imail_notification($currentAdminId) {

        $this->db->select(array('tbl_internal_message.id', 'tbl_internal_message.title', 'concat(firstname," ",lastname) as user_name', 'tbl_admin.gender', 'tbl_admin.profile', 'tbl_internal_message_content.senderId', 'tbl_internal_message_content.content', 'tbl_internal_message_content.created as mail_date', 'tbl_internal_message_content.id as contentId'));


        $this->db->from('tbl_internal_message');
        $this->db->join('(tbl_internal_message_content)', 'tbl_internal_message.id = tbl_internal_message_content.messageId', 'left');
        $this->db->join('tbl_internal_message_action', 'tbl_internal_message_action.messageId = tbl_internal_message.id AND tbl_internal_message_action.userId = ' . $currentAdminId, 'left');
        $this->db->join('tbl_internal_message_recipient', 'tbl_internal_message_recipient.messageContentId = tbl_internal_message_content.id', 'left');
        $this->db->join('tbl_admin', 'tbl_internal_message_content.senderId = tbl_admin.id', 'left');
        $this->db->where('tbl_internal_message_recipient.recipientId', $currentAdminId);
        $this->db->where('tbl_internal_message_recipient.is_read', 0);
        $this->db->where('tbl_internal_message_recipient.is_notify', 0);
        $this->db->where('tbl_internal_message_content.is_draft', 0);
        $this->db->order_by('mail_date', 'desc');


        $query = $this->db->get();

//        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        $result = $query->result();
//        print_r($result);
//        last_query();

        $ext_msg = array();
        if (!empty($result)) {
            foreach ($result as $key => $val) {
                $x['id'] = $val->id;
                $x['title'] = $val->title;
                $x['mail_date'] = $val->mail_date;
                $x['contentId'] = $val->contentId;
                $x['content'] = setting_length($val->content, 100);
                $x['user_name'] = $val->user_name;
                $x['user_img'] = get_admin_img($val->senderId, $val->profile, $val->gender);
                $x['redirect_uri'] = '/admin/imail/internal/inbox/' . $val->id;
                $x['type'] = 'internal';
                $ext_msg[] = $x;
            }
        }

        return $ext_msg;
    }

    function clear_imail_notification($reqData, $currentAdminId) {
        $data = array('is_notify' => 1);

        $contentId = $reqData->contentId;

        if ($contentId === 'ALL') {

            $where = array('recipientId' => $currentAdminId);

            // clear notification from internal mail
            $this->basic_model->update_records('internal_message_recipient', $data, $where);

            // clear notification form external mail
            $where = array('recipinentId' => $currentAdminId);
            $this->basic_model->update_records('external_message_recipient', $data, $where);
        } elseif ($contentId) {
            $where = array('recipientId' => $currentAdminId, 'messageContentId' => $contentId);

            if ($reqData->type === 'internal') {

                // clear notification from internal mail
                $this->basic_model->update_records('internal_message_recipient', $data, $where);
            } elseif ($reqData->type === 'external') {

                // clear notification form external mail
                $where = array('recipinentId' => $currentAdminId);
                $this->basic_model->update_records('external_message_recipient', $data, $where);
            }
        }
    }

    function get_single_notification($notificationId) {
        $tbl_notification = TBL_PREFIX . 'notification';

        $notification_columns = array($tbl_notification . ".id", $tbl_notification . ".created", $tbl_notification . ".title", $tbl_notification . ".shortdescription");
        $this->db->select($notification_columns);

        $this->db->select("(case when(user_type = 1) 
        THEN 		
        (select concat(firstname)  from tbl_member where id= userId)
        ELSE
          (select concat(firstname) from tbl_participant where id= userId)  
        END) as username");

        $this->db->select("(case when(user_type = 1) 
        THEN 'Member'
        ELSE
          'Participant'
        END) as user_type");


        $this->db->from($tbl_notification);
        $this->db->where('sender_type', '1');
        $this->db->where('status', '0');
        $this->db->order_by('created', 'DESC');
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());


        $return = $query->row();
        return $return;
    }

}
