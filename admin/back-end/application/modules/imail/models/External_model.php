<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'Classes/websocket/Websocket.php';

class External_model extends CI_Model {

    function __construct() {

        parent::__construct();
    }

    public function get_external_messages($filter, $currentAdminId) {
        $type = $filter->type;

        $src_columns = array('em.title', 'emc.content', "concat(admin.firstname,' ',admin.lastname)", "concat(part.firstname,' ',part.middlename,' ',part.lastname)", "concat(mem.firstname,' ',mem.lastname)");


        if (isset($filter->search_box) && $filter->search_box != "") {
            $this->db->group_start();
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    if ($serch_column[0] != 'null')
                        $this->db->or_like($serch_column[0], $filter->search_box);
                }
                else if ($column_search != 'null') {
                    $this->db->or_like($column_search, $filter->search_box);
                }
            }

            $this->db->group_end();
        }

        if ($type == 'inbox') {
            $this->db->where("((emc.sender_type = 1 AND emc.userId = " . $currentAdminId . ") Or (emr.recipinent_type = 1 AND emr.recipinentId = " . $currentAdminId . "))");

            $this->db->where('emc.is_draft', 0);
            $this->db->where('ema.archive', 0);
        } elseif ($type == 'draft') {
            $this->db->where("emc.sender_type = 1 AND emc.userId = " . $currentAdminId);

            $this->db->where('ema.archive', 0);
            $this->db->where('emc.is_draft', 1);
        } elseif ($type == 'archive') {
            $this->db->where("((emc.sender_type = 1 AND emc.userId = " . $currentAdminId . ") Or (emr.recipinent_type = 1 AND emr.recipinentId = " . $currentAdminId . "))");

            $this->db->where('ema.archive', 1);
        }

        if (!empty($filter->select)) {
            if ($filter->select == 'unread') {
                $this->db->where('emr.is_read', 0);
                $this->db->where('emr.recipinent_type', 1);
                $this->db->where('emr.recipinentId', $currentAdminId);
            } elseif ($filter->select == 'flagged') {
                $this->db->where('ema.is_flage', 1);
            } elseif ($filter->select == 'favourite') {
                $this->db->where('ema.is_fav', 1);
            } elseif ($filter->select == 'priority') {
                $this->db->where('emc.is_priority', 1);
            } elseif ($filter->select == 'attachment') {
                $this->db->where('emattach.filename !=', '');
            }
        }


        $this->db->select(array('em.id', 'em.title', 'em.is_block', 'ema.is_fav', 'ema.is_flage', 'ema.archive', 'emc.content', 'emc.created', 'emc.is_priority', 'emc.id as contentId', 'emc.sender_type'));


        $this->db->select("CASE emc.sender_type
            WHEN 1 THEN (select concat(firstname,' ',lastname) from tbl_admin where id = emc.userId)
            WHEN 2 THEN (select concat(firstname,' ', middlename,' ',lastname) from tbl_participant where id = emc.userId)
            WHEN 3 THEN (select concat(firstname,' ', middlename,' ',lastname)  from tbl_member where id = emc.userId)
            WHEN 4 THEN (select concat(name)  from tbl_organisation where id = emc.userId)
            ELSE NULL
            END as user_name");

        $this->db->from('tbl_external_message as em');
        $this->db->join('tbl_external_message_content as emc', 'em.id = emc.messageId', 'left');
        $this->db->join('tbl_external_message_action as ema', 'ema.messageId = em.id AND ema.userId = ' . $currentAdminId . ' AND ema.user_type = 1', 'left');
        $this->db->join('tbl_external_message_recipient as emr', 'emr.messageContentId = emc.id', 'left');
        $this->db->join('tbl_external_message_attachment as emattach', 'emattach.messageContentId = emc.id', 'left');

        // add when join user search something
        if (isset($filter->search_box) && $filter->search_box != "") {
            $this->db->join('tbl_admin as admin', 'emr.recipinentId = admin.id AND emr.recipinent_type = 1', 'left');
            $this->db->join('tbl_participant as part', 'emr.recipinentId = part.id AND emr.recipinent_type = 2', 'left');
            $this->db->join('tbl_member as mem', 'emr.recipinentId = mem.id AND emr.recipinent_type = 3', 'left');
            $this->db->join('tbl_organisation as org', 'emr.recipinentId = org.id AND emr.recipinent_type = 4', 'left');
        }

        $this->db->where('emc.id IN (SELECT MAX(emc.id) FROM tbl_external_message_content as emc left join tbl_external_message_recipient as emr ON emr.messageContentId = emc.id where ((emc.sender_type = 1 AND emc.userId = ' . $currentAdminId . ') Or (emr.recipinent_type = 1 AND emr.recipinentId = ' . $currentAdminId . ')) GROUP BY emc.messageId )');
        $this->db->group_by('emc.messageId');
        $this->db->order_by('emc.created', 'desc');

        $query = $this->db->get();
        $result = $query->result();

//        last_query();

        $ext_msg = array();
        if (!empty($result)) {
            foreach ($result as $val) {
                $x['id'] = $val->id;
                $x['title'] = $val->title;
                $x['mail_date'] = $val->created;

                $x['is_flage'] = $val->is_flage;
                $x['is_block'] = $val->is_block;
                $x['is_fav'] = $val->is_fav;
                $x['user_name'] = $val->user_name;
                $x['is_priority'] = $val->is_priority;
                $x['content'] = setting_length($val->content, 100);
                $x['have_attachment'] = $this->check_attachment($val->contentId);

                $sender_type = $val->sender_type;

                if ($sender_type == 1) {
                    $x['sender_type'] = "a";
                } else if ($sender_type == 2) {
                    $x['sender_type'] = "p";
                } else if ($sender_type == 3) {
                    $x['sender_type'] = "m";
                } else if ($sender_type == 4) {
                    $x['sender_type'] = "o";
                }

                $ext_msg[] = $x;
            }
        }

        return $ext_msg;
    }

    public function get_single_chat($reqData, $currentAdminId) {
        $messageId = $reqData->messageId;

        if ($reqData->type == 'inbox') {
            $this->db->where('ema.archive', 0);
        } elseif ($reqData->type == 'draft') {
            $this->db->where('ema.archive', 0);
        } elseif ($reqData->type == 'archive') {
            $this->db->where('ema.archive', 1);
        }

        $this->db->select(array('em.id', 'em.title', 'em.is_block', 'ema.is_fav', 'ema.is_flage', 'ema.is_flage'));
        $this->db->from('tbl_external_message as em');

        $this->db->join('tbl_external_message_action as ema', 'ema.messageId = em.id AND ema.userId = ' . $currentAdminId . ' AND ema.user_type = 1', 'left');
        $this->db->where('ema.messageId', $messageId);


        $query = $this->db->get();
        $messageData = $query->row();



        if (!empty($messageData)) {
            $messageData->is_read = 1;

            // get all content of message
            $content_details = $this->get_external_mail_all_content($messageId, $reqData->type, $currentAdminId);

            // mark as read message
            $this->mark_read_unread($messageId, $currentAdminId, 1);

            // if its comes blank mean this user not have permission to mail
            if (!empty($content_details)) {
                $messageData->content = $content_details;

                $return = array('status' => true, 'data' => $messageData);
            } else {
                $return = array('status' => false, 'error' => 'No mail found');
            }
        } else {
            $return = array('status' => false, 'error' => 'No mail found');
        }

        return $return;
    }

    function get_mail_sender_person($messageContentId) {
        $this->db->select("CASE sender_type
            WHEN 1 THEN (select concat(firstname,' ',lastname) from tbl_admin where id = userId)
            WHEN 2 THEN (select concat(firstname,' ', middlename,' ',lastname) from tbl_participant where id = userId)
            WHEN 3 THEN (select concat(firstname,' ', middlename,' ',lastname)  from tbl_member where id = userId)
            WHEN 4 THEN (select concat(name)  from tbl_organisation where id = userId)
            ELSE NULL
            END as label");

        $this->db->select(array('userId as value', 'sender_type as type'));
        $this->db->from('tbl_external_message_content');
        $this->db->where('tbl_external_message_content.id', $messageContentId);

        $query = $this->db->get();
        return $query->row();
    }

    function get_reply_to_person($messageContentId, $currentAdminId, $action_type) {

        $users = array('cc_user' => [], 'to_user' => []);

        $reply_person = $this->get_mail_sender_person($messageContentId);

        if ($reply_person->value != $currentAdminId || ($action_type == 'reply')) {
            $users['to_user'][] = $reply_person;
        }

        if ($action_type == 'reply_all') {

            $this->db->select("CASE tbl_external_message_recipient.recipinent_type
            WHEN 1 THEN (select concat(firstname,' ',lastname) from tbl_admin where id = tbl_external_message_recipient.recipinentId)
            WHEN 2 THEN (select concat(firstname,' ', middlename,' ',lastname) from tbl_participant where id = tbl_external_message_recipient.recipinentId)
            WHEN 3 THEN (select concat(firstname,' ', middlename,' ',lastname)  from tbl_member where id = tbl_external_message_recipient.recipinentId)
            WHEN 4 THEN (select concat(name)  from tbl_organisation where id = tbl_external_message_recipient.recipinentId)
            ELSE NULL
            END as label");

            $this->db->select(array('tbl_external_message_recipient.recipinentId as value', 'tbl_external_message_recipient.recipinent_type as type'));
            $this->db->from('tbl_external_message_recipient');
            $this->db->where('tbl_external_message_recipient.messageContentId', $messageContentId);

            $this->db->group_by('tbl_external_message_recipient.recipinentId');
            $this->db->group_by('tbl_external_message_recipient.recipinent_type');

            $query = $this->db->get();
            $result = $query->result();


            if (!empty($result)) {
                foreach ($result as $val) {
                    if ($val->type == 1) {
                        if ($val->value != $currentAdminId) {
                            $users['cc_user'][] = $val;
                        }
                    } else {
                        $users['to_user'][] = $val;
                    }
                }
            }
        }

        return $users;
    }

    function get_external_mail_all_content($messageId, $type, $currentAdminId) {
        $this->db->select("CASE emc.sender_type
            WHEN 1 THEN (select concat(firstname,' ',lastname,'||',profile,'||',gender,'||',id) from tbl_admin where id = emc.userId)
            WHEN 2 THEN (select concat(firstname,' ', middlename,' ',lastname,'||', profile_image,'||',gender,'||',id) from tbl_participant where id = emc.userId)
            WHEN 3 THEN (select concat(firstname,' ', middlename,' ',lastname,'||', profile_image,'||',gender,'||',id)  from tbl_member where id = emc.userId)
            WHEN 4 THEN (select concat(name,'||', logo_file,'||',id)  from tbl_organisation where id = emc.userId)
            ELSE NULL
            END as user_data");

        if ($type == 'inbox') {
            $this->db->where("((emc.sender_type = 1 AND emc.userId = " . $currentAdminId . ") Or (emr.recipinent_type = 1 AND emr.recipinentId = " . $currentAdminId . "))");
            $this->db->where('emc.is_draft', 0);
        } elseif ($type == 'draft') {
            $this->db->where("emc.sender_type = 1 AND emc.userId = " . $currentAdminId);
            $this->db->where('emc.is_draft', 1);
        } elseif ($type == 'archive') {
            $this->db->where("((emc.sender_type = 1 AND emc.userId = " . $currentAdminId . ") Or (emr.recipinent_type = 1 AND emr.recipinentId = " . $currentAdminId . "))");
        } else {
            return false;
        }

        $this->db->select(array('emc.id', 'emc.created', 'emc.sender_type', 'is_priority', 'content', 'emr.is_read', 'emc.is_draft', 'is_reply', 'emr.is_notify'));

        $this->db->from('tbl_external_message_content as emc');
        $this->db->join('tbl_external_message_recipient as emr', 'emr.messageContentId = emc.id AND emr.recipinent_type = 1', 'left');
        $this->db->order_by('emc.created', 'asc');
        $this->db->group_by('emc.id');
        $this->db->where('emc.messageId', $messageId);

        $query = $this->db->get();
        $result = $query->result();

        $ext_msg = array();
        if (!empty($result)) {
            foreach ($result as $key => $val) {
                $x['id'] = $val->id;
                $x['mail_date'] = $val->created;
                $x['content'] = $val->content;
                $x['is_priority'] = $val->is_priority;
                $x['is_read'] = $val->is_read;
                $x['is_notify'] = $val->is_notify;
                $x['is_draft'] = $val->is_draft;
                $x['is_reply'] = $val->is_reply;

                $user_data = explode('||', $val->user_data);

                $sender_type = $val->sender_type;

                if (count($user_data) > 1) {
                    // here 0 = user name 
                    $x['user_name'] = $user_data[0];

                    if ($sender_type == 1) {

                        // here 3 = user id // 1 = profile img name // 2 = gender
                        $x['user_img'] = get_admin_img($user_data[3], $user_data[1], $user_data[2]);
                    } else if ($sender_type == 2) {

                        // here 3 = user id  // 1 = profile img name  // 2 = gender
                        $x['user_img'] = get_participant_img($user_data[3], $user_data[1], $user_data[2]);
                    } else if ($sender_type == 3) {

                        // here 3 = user id  // 1 = profile img name  // 2 = gender
                        $x['user_img'] = get_admin_img($user_data[3], $user_data[1], $user_data[2]);
                    } else if ($sender_type == 4) {

                        // here 2 = user id  // 1 = profile img name
                        $x['user_img'] = get_org_img($user_data[2], $user_data[1]);
                    }
                }

                // get all attachments
                $x['attachments'] = $this->get_mail_attachment($val->id);

                $ext_msg[] = $x;
            }
        }

        return $ext_msg;
    }

    function check_attachment($messageContentId) {
        $this->db->select(array('id'));

        $this->db->from('tbl_external_message_attachment');
        $this->db->where('messageContentId', $messageContentId);

        $query = $this->db->get();
        $result = $query->num_rows();

        if ($result > 0) {
            return true;
        }
        return false;
    }

    function get_mail_attachment($messageContentId) {
        $this->db->select(array('id', 'filename'));

        $this->db->from('tbl_external_message_attachment');
        $this->db->where('tbl_external_message_attachment.messageContentId', $messageContentId);

        $query = $this->db->get();
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $val) {
                $val->file_path = base_url() . EXTERNAL_IMAIL_PATH . '/' . $messageContentId . '/' . $val->filename;
                $val->contentId = $messageContentId;
            }
        }

        return $result;
    }

    function clear_all_imail_notification() {
        $tbl_external_message_content = TBL_PREFIX . 'external_message_content';
        $this->db->where('sender_type', '2');
        $this->db->update($tbl_external_message_content, array("is_read" => 1));
        return array('status' => true);
    }

    function save_title_data($reqData, $mode) {
        $messageData = array(
            'companyId' => 1,
            'title' => $reqData->title,
            'created' => DATE_TIME,
            'is_block' => 0,
        );


        // create message
        if ($mode === 'create') {
            return $messageId = $this->basic_model->insert_records('external_message', $messageData, FALSE);
        } else {

            // update titie here
            $where_ttl = array('id' => $reqData->messageId);
            $this->basic_model->update_records('external_message', $messageData, $where_ttl);
            return $reqData->messageId;
        }
    }

    function compose_new_mail($reqData, $current_admin) {

        // set default it is intial message
        $reqData->is_reply = 0;

        // save title data
        $messageId = $this->save_title_data($reqData, 'create');

        // create message content (message body)
        $contentId = $this->set_content_mail($reqData, $messageId, $current_admin, 'create');

        // upload attachment if have any attachement
        $this->upload_content_attachement($messageId, $contentId);

        // if forword its forword mail then copy its attachment if have any attachments
        $this->copy_forword_attachment($reqData, $messageId, $contentId);

        // set recipent data of user
        $this->set_reciepent_data($reqData, $messageId, $contentId);

        // set message action
        $this->set_message_action($reqData, $messageId, $current_admin);
    }

    function set_content_mail($reqData, $messageId, $current_admin, $mode) {

        $messageContent = array(
            'messageId' => $messageId,
            'userId' => $current_admin,
            'sender_type' => 1,
            'is_priority' => ($reqData->is_priority == 1) ? 1 : 0,
            'created' => DATE_TIME,
            'content' => $reqData->content,
            'is_reply' => $reqData->is_reply,
            'is_draft' => ($reqData->submit_type == 'is_draft') ? 1 : 0,
        );

        if ($mode === 'create') {

            return $this->basic_model->insert_records('external_message_content', $messageContent, false);
        } else {
            // update content here
            $where_ttl = array('id' => $reqData->contentId);
            $this->basic_model->update_records('external_message_content', $messageContent, $where_ttl);
            return $reqData->contentId;
        }
    }

    function upload_content_attachement($messageId, $contentId) {
        $error = array();

        if (!empty($_FILES)) {
            $config['upload_path'] = EXTERNAL_IMAIL_PATH;
            $config['input_name'] = 'attachments';
            $config['directory_name'] = $contentId;
            $config['allowed_types'] = 'jpg|jpeg|png|xlx|xls|doc|docx|pdf|csv|odt|rtf';

            $response = do_muliple_upload($config);

            $attachments = array();

            if (!empty($response)) {
                foreach ($response as $key => $val) {
                    if (isset($val['error'])) {
                        $error[]['file_error'] = $val['error'];
                    } else {
                        $attachments[$key]['filename'] = $val['upload_data']['file_name'];
                        $attachments[$key]['created'] = DATE_TIME;
                        $attachments[$key]['messageContentId'] = $contentId;
                    }
                }

                if (!empty($attachments)) {
                    $this->basic_model->insert_records('external_message_attachment', $attachments, true);
                }
            }
        }

        return $error;
    }

    function copy_forword_attachment($reqData, $messageId, $contentId) {
        if (!empty($reqData->forword_attachments)) {

            $attachments = json_decode($reqData->forword_attachments);

            if (!empty($attachments)) {
                $insert_att = array();

                foreach ($attachments as $val) {
                    $directoryName = EXTERNAL_IMAIL_PATH . '/' . $contentId;

                    create_directory($directoryName);

                    $from = EXTERNAL_IMAIL_PATH . '/' . $val->contentId . '/' . $val->filename;
                    $to = $directoryName . '/' . $val->filename;
                    copy($from, $to);

                    $insert_att[] = array('filename' => $val->filename, 'messageContentId' => $contentId, 'created' => DATE_TIME);
                }

                if (!empty($insert_att)) {
                    $this->basic_model->insert_records('external_message_attachment', $insert_att, true);
                }
            }
        }
    }

    function set_reciepent_data($reqData, $messageId, $contentId) {
        $wbObj = new Websocket();

        // first delete all recipent data then inster new data
        $where_dlt = ['messageId' => $messageId, 'messageContentId' => $contentId];
        $this->basic_model->delete_records('external_message_recipient', $where_dlt);

        $to_users = json_decode($reqData->to_user);
        $cc_user = json_decode($reqData->cc_user);

        $userIds = array();

        foreach ($to_users as $val) {
            $recipient_data[] = array(
                'messageContentId' => $contentId,
                'messageId' => $messageId,
                'recipinent_type' => $val->type,
                'recipinentId' => $val->value,
                'is_read' => 0,
            );

            $userIds[$val->type][] = $val->value;

            // remove from archive
            $this->remove_mail_from_archive($messageId, $val->value, $val->type);
        }

        foreach ($cc_user as $val) {
            $recipient_data[] = array(
                'messageContentId' => $contentId,
                'messageId' => $messageId,
                'recipinent_type' => 1,
                'recipinentId' => $val->value,
                'is_read' => 0,
            );

            $userIds[1][] = $val->value;

            // remove from archive
            $this->remove_mail_from_archive($messageId, $val->value, 1);
        }

        if (!empty($recipient_data)) {
            $this->basic_model->insert_records('external_message_recipient', $recipient_data, true);

            // check websoket here send and alert
            if ($wbObj->check_webscoket_on() && $reqData->submit_type !== 'is_draft') {
                $data = array('chanel' => 'server', 'req_type' => 'user_external_imail_notification', 'token' => $wbObj->get_token(), 'data' => $userIds);
                $wbObj->send_data_on_socket($data);
            }
        }
    }

    function remove_mail_from_archive($messageId, $userId, $user_type) {
        $where = array('user_type' => $user_type, 'userId' => $userId, 'messageId' => $messageId);
        $data = array('archive' => 0);
        $this->basic_model->update_records('external_message_action', $data, $where);
    }

    function set_message_action($reqData, $messageId, $current_admin) {

        $cc_user = json_decode($reqData->cc_user, true);
        $to_user = json_decode($reqData->to_user, true);

        $cc_user[]['value'] = $current_admin;

        $action_data = array();
        if (!empty($cc_user)) {
            foreach ($cc_user as $val) {
                $where_ck = ['messageId' => $messageId, 'user_type' => 1, 'userId' => $val['value']];
                $ch_res = $this->basic_model->get_row('external_message_action', ['userId'], $where_ck);

                if (empty($ch_res)) {
                    $action_data[] = array(
                        'messageId' => $messageId,
                        'user_type' => 1,
                        'userId' => $val['value'],
                        'is_fav' => 0,
                        'is_flage' => 0,
                        'archive' => 0
                    );
                }
            }
        }

        if (!empty($to_user)) {
            foreach ($to_user as $val) {
                $where_ck = ['messageId' => $messageId, 'user_type' => $val['type'], 'userId' => $val['value']];
                $ch_res = $this->basic_model->get_row('external_message_action', ['userId'], $where_ck);
                if (empty($ch_res)) {
                    $action_data[] = array(
                        'messageId' => $messageId,
                        'user_type' => $val['type'],
                        'userId' => $val['value'],
                        'is_fav' => 0,
                        'is_flage' => 0,
                        'archive' => 0
                    );
                }
            }
        }

        if (!empty($action_data)) {
            $this->basic_model->insert_records('external_message_action', $action_data, true);
        }
    }

    function reply_mail($reqData, $current_admin) {
        $messageId = $reqData->messageId;

        // set default it is reply message
        $reqData->is_reply = 1;

        // create message content (message body)
        $contentId = $this->set_content_mail($reqData, $messageId, $current_admin, 'create');

        // upload attachment if have any attachement
        $this->upload_content_attachement($messageId, $contentId);

        // set recipent data of user
        $this->set_reciepent_data($reqData, $messageId, $contentId);

        // set message action
        $this->set_message_action($reqData, $messageId, $current_admin);
    }

    function mark_read_unread($messageId, $userId, $status) {
        if ($status == 0 || $status == 1) {

            $where = array('messageId' => $messageId, 'recipinent_type' => 1, 'recipinentId' => $userId);
            $data = array('is_read' => $status);

            if ($status == 1) {
                $data['is_notify'] = 1;
            }
            $this->basic_model->update_records('external_message_recipient', $data, $where);
        }
    }

    function save_or_send_draft_mail($reqData, $current_admin) {
        // check its first reply or not
        $where_ck_ttl = ['id' => $reqData->contentId, 'messageId' => $reqData->messageId, 'is_draft' => 0];
        $message_titie = $this->basic_model->get_row('external_message_content', array('content'), $where_ck_ttl);

        if (empty($message_titie)) {
            // save title here
            $this->save_title_data($reqData, 'update');

            // set default it is intial message
            $reqData->is_reply = 0;
        } else {
            // set default it is second message
            $reqData->is_reply = 1;
        }


        // update message content (message body)
        $contentId = $this->set_content_mail($reqData, $reqData->messageId, $current_admin, 'update');

        // first check any removal attachment and if its remove by user then also remove from database 
        if (!empty($reqData->forword_attachments)) {
            $attachments = json_decode($reqData->forword_attachments);

            if (!empty($attachments)) {
                foreach ($attachments as $val) {
                    if (!empty($val->removed) && $val->removed == true) {

                        // delete attachment here
                        $where_dl_att = array('id' => $val->id);
                        $this->basic_model->delete_records('internal_message_attachment', $where_dl_att);
                    }
                }
            }
        }

        // upload attachment if have any new attachement
        $this->upload_content_attachement($reqData->messageId, $contentId);

        // update recipent data of user
        $this->set_reciepent_data($reqData, $reqData->messageId, $contentId);

        // update message action
        $this->set_message_action($reqData, $reqData->messageId, $current_admin);
    }

    function get_mail_title($messageId) {
        $where = ['id' => $messageId];
        $mail_data = $this->basic_model->get_row('external_message', ['title'], $where);
        return $mail_data;
    }

    function get_mail_content($contentId) {
        $where = ['id' => $contentId];
        $mail_data = $this->basic_model->get_row('external_message_content', ['content', 'is_priority'], $where);
        return $mail_data;
    }

    function get_mail_pre_filled_data($reqData, $currentAdminId) {
        $return_data = [
            'cc_user' => [],
            'to_user' => [],
            'title' => '',
            'content' => '',
            'forword_attachments' => [],
            'is_priority' => false,
            'titleFixed' => false,
        ];

        if (!empty($reqData->action_type)) {


            if ($reqData->action_type === 'reply') {
                $message_data = $this->get_mail_title($reqData->messageId);
                $return_data['title'] = 'RE: ' . $message_data->title;

                $x = $this->get_reply_to_person($reqData->contentId, $currentAdminId, $reqData->action_type);
                $return_data['to_user'] = $x['to_user'];

                $return_data['titleFixed'] = true;
            } elseif ($reqData->action_type === 'reply_all') {


                $message_data = $this->get_mail_title($reqData->messageId);
                $return_data['title'] = 'RE: ' . $message_data->title;

                $x = $this->get_reply_to_person($reqData->contentId, $currentAdminId, $reqData->action_type);

                $return_data['to_user'] = $x['to_user'];
                $return_data['cc_user'] = $x['cc_user'];

                $return_data['titleFixed'] = true;
            } elseif ($reqData->action_type === 'forword_mail') {

                $mail_data = $this->get_mail_content($reqData->contentId);

                $return_data['is_priority'] = $mail_data->is_priority;
                $return_data['content'] = $mail_data->content;

                $return_data['forword_attachments'] = $this->get_mail_attachment($reqData->contentId);

                $message_data = $this->get_mail_title($reqData->messageId);
                $return_data['title'] = $message_data->title;
            } elseif ($reqData->action_type === 'open_draft') {
                $x = $this->get_reply_to_person($reqData->contentId, $currentAdminId, 'reply_all');

                $return_data['to_user'] = $x['to_user'];
                $return_data['cc_user'] = $x['cc_user'];

                $mail_data = $this->get_mail_content($reqData->contentId);
                $return_data['is_priority'] = $mail_data->is_priority;
                $return_data['content'] = $mail_data->content;

                $return_data['forword_attachments'] = $this->get_mail_attachment($reqData->contentId);

                $message_data = $this->get_mail_title($reqData->messageId);
                $return_data['title'] = $message_data->title;
            }
        }

        return $return_data;
    }

}
