<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recruitment_question_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function create_Questions($objQuestion) {
        $tbl_question= TBL_PREFIX.'recruitment_additional_questions';
        					
		$arrQuestions = array();
        $arrQuestions['question'] = $objQuestion->getQuestion();
        $arrQuestions['status'] = $objQuestion->getStatus();        
        $arrQuestions['created_by'] = $objQuestion->getCreated_by();
		$arrQuestions['question_topic'] = $objQuestion->getQuestionTopic();      
		$arrQuestions['question_type'] = $objQuestion->getQuestion_Type();		
		$arrQuestions['training_category'] = $objQuestion->getTrainingCategory();
		$arrQuestions['updated'] = $objQuestion->getCreated();		
		if($objQuestion->getId()>0){			
			$this->db->where('id', $objQuestion->getId());
			$insert_query =$this->db->update($tbl_question,$arrQuestions);
			//echo $this->db->last_query();
			$this->update_answer($objQuestion->getId(),$objQuestion->getAnswer());
		}else{
			$arrQuestions['created'] = $objQuestion->getCreated();
			$insert_query = $this->db->insert($tbl_question, $arrQuestions);
			$questionid=$this->db->insert_id();		
			$this->insert_answer($questionid,$objQuestion->getAnswer());
		}
        return $insert_query;		
    }
	private function insert_answer($question,$arrAnswer){
		$tbl_question_answer= TBL_PREFIX.'recruitment_additional_questions_answer';
		foreach($arrAnswer as $key=>$value){
			$arAnswer = array();				
			$arAnswer['question'] = $question;
			$arAnswer['question_option'] = $value->value;
			$arAnswer['serial'] = $value->lebel;			
			$arAnswer['answer'] = $value->checked;
			$this->db->insert($tbl_question_answer, $arAnswer);
		}		
	}
	private function update_answer($question,$arrAnswer){
		$tbl_question_answer= TBL_PREFIX.'recruitment_additional_questions_answer';
		foreach($arrAnswer as $key=>$value){
			$arAnswer = array();				
			$arAnswer['question'] = $question;
			$arAnswer['question_option'] = $value->value;
			$arAnswer['serial'] = $value->lebel;			
			$arAnswer['answer'] = $value->checked;			
			$this->db->where('id', $value->answer_id);
			$this->db->update($tbl_question_answer,$arAnswer);	
		}
	}
		
	public function delete_Questions($objQuestion){
		 $tbl_question= TBL_PREFIX.'recruitment_additional_questions'; 
		$this->db->where('id', $objQuestion->getId());
		$this->db->update($tbl_question,array('archive'=>1));
		return $this->db->affected_rows();
	}
	private function remove_answer(){
		
		
	}
	public function question_list(){
		$tbl_question= TBL_PREFIX.'recruitment_additional_questions';
		$tbl_question_topic= TBL_PREFIX.'recruitment_question_topic';
		$tbl_question_answer= TBL_PREFIX.'recruitment_additional_questions_answer';
		
        $select_column = array($tbl_question . ".id", $tbl_question . ".question", $tbl_question . ".status", $tbl_question . '.created', $tbl_question . '.training_category',$tbl_question . '.question_type',$tbl_question . '.question_topic',$tbl_question . ".created_by" ,$tbl_question . ".updated" ,$tbl_question_topic . ".topic");
		
		$dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
		$this->db->from($tbl_question);
		$this->db->where('archive', '0');
		$this->db->join($tbl_question_topic,$tbl_question.'.question_topic =' . $tbl_question_topic . '.id', 'inner');		
		$query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
		$dataResult = $query->result();
		$dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
		
		foreach($dataResult as $data){				
				
			$select_answer_column = array($tbl_question_answer . ".id as answer_id",$tbl_question_answer . ".answer as checked",$tbl_question_answer . ".question_option as value", $tbl_question_answer . ".serial as lebel");	
			$this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_answer_column)), false);
			$this->db->from($tbl_question_answer);
			$this->db->where('question',$data->id);		
			$query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
			$dataAnsResult = $query->result_array();
			$data->answers=$dataAnsResult;				
		}				
		$return = array('count' => $dt_filtered_total, 'data' => $dataResult);
        return $return;		
	}
}