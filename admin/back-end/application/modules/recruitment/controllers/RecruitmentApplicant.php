<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class RecruitmentApplicant extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Recruitment_applicant_model');

        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->loges->setModule(9);
    }

    function get_requirement_applicants() {
        $reqData = request_handler('access_recruitment');

        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $response = $this->Recruitment_applicant_model->get_requirement_applicants($reqData);

            echo json_encode($response);
        }
    }

}
