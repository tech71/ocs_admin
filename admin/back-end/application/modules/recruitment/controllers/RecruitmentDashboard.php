<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class RecruitmentDashboard extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Recruitment_model');
        $this->load->model('Basic_model');
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->loges->setModule(9);
    }

    public function get_department() {
        $reqData = $reqData1 = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $result = $this->Recruitment_model->get_department($reqData);
            echo json_encode($result);
        }
    }

    public function create_user() {

        $reqData = request_handler();
        $this->loges->setCreatedBy($reqData->adminId);
        #pr($reqData);
        require_once APPPATH . 'Classes/recruitment/admin.php';
        $objAdmin = new AdminClass\Admin();

        if (!empty($reqData->data)) {
            $data = (array) $reqData->data;
            $data['ocs_id'] = (!empty($data['ocs_id'])) ? $data['ocs_id'] : false;

            $this->form_validation->set_data($data);
            $validation_rules = array(
                array('field' => 'username', 'label' => 'UserName', 'rules' => 'callback_check_username_already_exist[' . $data['ocs_id'] . ']'),
                array('field' => 'firstname', 'label' => 'First Name', 'rules' => 'required'),
                array('field' => 'lastname', 'label' => 'Last Name', 'rules' => 'required'),
                array('field' => 'position', 'label' => 'Date Of Birth', 'rules' => 'required'),
                array('field' => 'EmailInput[]', 'label' => 'Email address', 'rules' => 'callback_check_user_emailaddress_already_exist[' . $data['ocs_id'] . ']'),
                array('field' => 'PhoneInput[]', 'label' => 'phone number', 'rules' => 'callback_check_phone_number_validation'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {

                $objAdmin->setUsername($data['username']);
                isset($data['password']) ? $objAdmin->setPassword($data['password']) : '';
                $objAdmin->setFirstname($data['firstname']);
                $objAdmin->setLastname($data['lastname']);
                #$objAdmin->setDepartment($data['department']);
                $objAdmin->setPosition($data['position']);
                $objAdmin->setSecondaryEmails($data['EmailInput']);
                $objAdmin->setPrimaryEmail($data['EmailInput'][0]->name);
                $objAdmin->setSecondaryPhone($data['PhoneInput']);
                $objAdmin->setRoles(array('id' => '10', 'access' => true));
                // give permission of recruitment portal only

                if ($data['ocs_id'] > 0) {
                    // update user
                    $objAdmin->setAdminid($data['ocs_id']);
                    $objAdmin->updateUser();
                    $this->loges->setTitle('User staff update: ' . $objAdmin->getFirstname() . ' ' . $objAdmin->getLastname());
                } else {
                    // create user
                    $objAdmin->createUser();
                    $objAdmin->insertRoleToAdmin();
                    $objAdmin->insertStaffDetail();
                    // send welcome to admin
                    if (ENABLE_MAIL)
                        $objAdmin->send_welcome_mail();

                    $this->loges->setTitle('New User staff: ' . $objAdmin->getFirstname() . ' ' . $objAdmin->getLastname());
                }
                $this->loges->setDescription(json_encode($reqData->data));
                $this->loges->setCreatedBy($reqData->adminId);
                $this->loges->setUserId($reqData->adminId);
                $this->loges->createLog();
                // insert secondary email
                $objAdmin->insertEmail();
                // insert secondary email
                $objAdmin->insertPhone();
                $response = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($response);
        }
    }

    // this method use for check user email already exist
    public function check_user_emailaddress_already_exist($sever_emailAddress = array(), $adminId_NEW = false) {
        $this->load->model('admin/admin_model');
        $adminId = false;

        if ($this->input->get() || $sever_emailAddress) {
            $emails = ($this->input->get()) ? $this->input->get() : (array) $sever_emailAddress;

            if ($this->input->get('adminId') || $adminId_NEW) {
                $adminId = ($this->input->get('adminId')) ? $this->input->get('adminId') : $adminId_NEW;
                if (array_key_exists('adminId', $emails)) {
                    unset($emails['adminId']);
                };
            }


            foreach ($emails as $val) {
                $result = $this->admin_model->check_dublicate_email($val, $adminId);
                if ($sever_emailAddress) {
                    if (!empty($result)) {
                        $this->form_validation->set_message('check_user_emailaddress_already_exist', 'this ' . $result[0]->email . ' Email address Allready Exist');
                        return false;
                    }
                }

                if ($this->input->get()) {
                    if (!empty($result)) {
                        echo 'false';
                    } else {
                        echo 'true';
                    }
                }

                return true;
            }
        }
    }

    public function check_phone_number_validation($phone_numbers) {
        foreach ($phone_numbers as $val) {
            if (empty($val)) {
                $this->form_validation->set_message('phone number can not empty', 'phone number can not be empty');
                return false;
            }
        }
        return true;
    }

    // this method use for check username already exist
    public function check_username_already_exist($sever_end_username = false, $adminId = false) {
        if ($this->input->get('username') || $sever_end_username) {
            $username = ($this->input->get('username')) ? $this->input->get('username') : $sever_end_username;
            $where = array('username' => $username);


            if ($this->input->get('adminId') || $adminId) {
                $where['id !='] = ($this->input->get('adminId')) ? $this->input->get('adminId') : $adminId;
            }

            $result = $this->basic_model->get_row('admin', array('username'), $where);

            if ($sever_end_username) {
                if (!empty($result)) {
                    $this->form_validation->set_message('check_username_already_exist', 'User Name Allready Exist');
                    return false;
                }
                return true;
            }

            if (!empty($result)) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
    }

    // using this method get recuritment staff list
    public function get_staff_members() {
        $reqData = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;
            $reqData = json_decode($reqData);
            $response = $this->Recruitment_model->get_staff_members($reqData);
            echo json_encode($response);
        }
    }

    public function create_department() {
        $reqData = request_handler('access_recruitment');
        $this->loges->setCreatedBy($reqData->adminId);
        #pr($reqData);
        require_once APPPATH . 'Classes/recruitment/recruitment_department.php';
        $objAdmin = new classRecuritmentDepartment\Recruitment_department();

        if (!empty($reqData->data)) {
            $data = (array) $reqData->data;
            $data['id'] = (!empty($data['id'])) ? $data['id'] : false;

            $this->form_validation->set_data($data);

            $validation_rules = array(
                array('field' => 'name', 'label' => 'Department Name', 'rules' => 'required'),
            );
            // set rules form validation
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {

                $objAdmin->setName($data['name']);

                if ($data['id'] > 0) {
                    // update department
                    $objAdmin->setId($data['id']);
                    $objAdmin->updateRecuritmentDepartment();
                    $this->loges->setTitle('Department update: ' . $objAdmin->getName());
                } else {
                    // create department
                    $dep_id = $objAdmin->creatRecuritmentDepartment();
                    $this->loges->setTitle('New Department created: ' . $objAdmin->getName());
                }

                $this->loges->setCreatedBy($reqData->adminId);
                $this->loges->setUserId($reqData->adminId);
                $this->loges->setDescription(json_encode($reqData->data));
                $this->loges->createLog();
                $response = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($response);
        }
    }

    public function update_alloted_department() {
        $reqData = request_handler();
        $allotedData = $reqData->data;
        if (!empty($allotedData)) {
            #pr($reqData);
            $update_dept = $this->Recruitment_model->update_alloted_department($allotedData);
            $response = array('status' => true, 'update_dept' => $update_dept);
            echo json_encode($response);
        }
    }

    public function get_applicant_list() {
        $reqData = $reqData1 = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $result = $this->Recruitment_model->get_applicant_list($reqData);
            echo json_encode($result);
        }
    }

    public function create_action() {
        $reqData = request_handler('create_recruitment');
        #pr($reqData);
        require_once APPPATH . 'Classes/recruitment/action.php';
        $objAction = new classAction\Action();

        require_once APPPATH . 'Classes/recruitment/applicant.php';
        $objApplicant = new classApplicant\Applicant();

        require_once APPPATH . 'Classes/recruitment/sub_task.php';
        $objSubTask = new classSubTask\Sub_task();

        $data = (array) $reqData->data;
        #pr($req_data);die;
        if (!empty($data)) {
            $validation_rules = array(
                array('field' => 'action_name', 'label' => 'Action name', 'rules' => 'required'),
                array('field' => 'action_type', 'label' => 'Action type', 'rules' => 'required'),
                array('field' => 'action_status', 'label' => 'Action status', 'rules' => 'required'),
                array('field' => 'time_duration', 'label' => 'Time duration', 'rules' => 'required'),
                array('field' => 'action_location', 'label' => 'Action location', 'rules' => 'required'),
            );

            $this->form_validation->set_data($data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $assign_user = '';
                if (!empty($data['assigned_user'])) {
                    $assign_user = $data['assigned_user']->value;
                }

                $objAction->setAction_name($data['action_name']);
                $objAction->setUser($assign_user);
                $objAction->setAction_type($data['action_type']); //
                $objAction->setStart_datetime($data['time_duration']);
                $objAction->setEnd_datetime($data['time_duration']);
                $objAction->setTraining_location($data['action_location']);
                $objAction->setStatus($data['action_status']); //
                $objAction->setMail_status('0');
                $rowAction = $objAction->CreateAction($reqData);

                if (!empty($data['attachedApplicant'])) {
                    foreach ($data['attachedApplicant'] as $key => $value) {
                        $objApplicant->setAction_id($rowAction);
                        $objApplicant->setApplicant_id($value);
                        $objApplicant->setEmail_status('0');
                        $objApplicant->setStatus('0');
                        $rowAttachedApplicant = $objApplicant->attachApplicant($reqData);
                    }
                }

                if (!empty($data['subsTasks_ul'])) {
                    foreach ($data['subsTasks_ul'] as $key => $val) {
                        if (isset($val->new)) {
                            $objSubTask->setAction_id($rowAction);
                            $objSubTask->setSubtask($val->name);
                            $objSubTask->setSubtask_detail($val->decription);
                            $objSubTask->setAssigned_to($val->task_assigned_id);
                            $objSubTask->setDue_date($val->due_date);
                            #$objSubTask->setAttachment($data['action_name']);
                            $objSubTask->setTask_completed('0');
                            $rowAttachedApplicant = $objSubTask->CreateSubTask($reqData);
                        }
                    }
                }
                //logs
                $this->loges->setCreatedBy($reqData->adminId);
                $this->loges->setUserId($reqData->adminId);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->setTitle('New action created : Action Id ' . $rowAction);
                $this->loges->createLog();
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($return);
            exit();
        }
    }

    public function archive_department() {
        $reqData = request_handler('delete_recruitment');
        $reqData = $reqData->data;

        if (!empty($reqData->id)) {
            $this->basic_model->update_records('recruitment_department', ['archived' => 1], ['id' => $reqData->id]);
            
            echo json_encode(['status' => true]);
        }
    }

}
