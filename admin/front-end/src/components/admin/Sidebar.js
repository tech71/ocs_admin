import React, { Component } from 'react';
import { NavLink,Link } from 'react-router-dom';
import PinModal from './PinModal';
import {checkLoginModule, pinHtml} from 'service/common.js';

class Sidebar extends Component {


    constructor(props) {
        super(props);
        this.state = {
            drop1: false,
            drop2: false
        }
    }
makeNavLinkTag =(menu,typeDatatag) =>{
    let classData={};
            let path = menu.hasOwnProperty('path') && menu.path != '' ? menu.path:'#';
            let linkOnlyHide = menu.hasOwnProperty('linkOnlyHide') && menu.linkOnlyHide ? true:false;
            let pathstructure = menu.hasOwnProperty('pathstructure') && menu.pathstructure != '' ? menu.pathstructure:'';
            let type = menu.hasOwnProperty('type') && menu.type != '' ? menu.type:0;
            if(type==3){
                let menuName = menu.hasOwnProperty('name') && menu.name != '' ? menu.name:'';
                let moduelName = menu.hasOwnProperty('moduelname') && menu.moduelname != '' ? menu.moduelname:'fms';
                var re = new RegExp(Object.keys(this.props.replacePropsData).join("|"),"gi");
                pathstructure = pathstructure.replace(re, (matched)=>{
                return this.props.replacePropsData[matched];
                });
                path = pathstructure;
                return pinHtml(this,moduelName,'menu',pathstructure,menuName);
            }
            if(pathstructure!='' && type==1){
                var re = new RegExp(Object.keys(this.props.replacePropsData).join("|"),"gi");
                pathstructure = pathstructure.replace(re, (matched)=>{
                return this.props.replacePropsData[matched];
                });
                path = pathstructure;
            }
            if(menu.hasOwnProperty('className')){
                classData ={className:menu.className}
            }

            if(linkOnlyHide){
                return <React.Fragment />;
            }
            if(typeDatatag=='menu'){
                return <NavLink exact to={path} {...classData}><span><strong>{menu.name}</strong></span></NavLink>
            }else{
                return <NavLink exact to={path} {...classData}>{menu.name}</NavLink>
            }
}

    closeModal=()=>{
        this.setState({pinModalOpen:false})
    }
    
    hideShowNavDropDown = (actionName, sts) => {
        this.setState({ [actionName]: !sts })
    }
    
    render() {



        return (

            <aside className={'main_sidebar__'}>
                <div className='sideHead1__'>
                    {this.props.heading}
                </div>
                <ul className='sideNavies__'>

                    {this.props.menus.map((menu, i) => {

                        let list = '';
                        if(menu.hasOwnProperty('linkShow') && !menu.linkShow){
                            return list;
                        }

                        if ((menu.hasOwnProperty('submenus') && menu.submenus.length == 0) || !this.props.subMenuShowStatus || !menu.hasOwnProperty('submenus')) {
                            list = <li key={i}>{this.makeNavLinkTag(menu,'menu')}</li>
                        }
                        else {

                            let sts = this.state[menu.name + '_action'] == undefined ? true : this.state[menu.name + '_action'];
                            let actionName = menu.name + '_action';
                            let classNameData = menu.hasOwnProperty('className') ? 'dropdownMenu '+menu.className : 'dropdownMenu';

                            list = <li key={'menu_'+i} className={classNameData} >
                                <div
                                    className={'drpHdng' + ' ' + (sts ? 'open' : '')}
                                    onClick={() => this.hideShowNavDropDown(actionName, sts)}
                                >
                                        {this.makeNavLinkTag(menu,'menu')}
                                    <i className='icon icon-arrow-right'></i>
                                </div>
                                <ul>
                                    {!menu.closeMenus?
                                     menu.submenus.map((submenu, i) => {
                                        return (
                                            <li key={'submenu_'+i}>{this.makeNavLinkTag(submenu, 'submenu')}
                                                
                                                {
                                                    (submenu.subSubMenu && !submenu.closeMenus) ? <ul className='Sub_in_sub'>
                                                        
                                                        {
                                                            submenu.subSubMenu.map((subSubmenu, i) => {
                                                                return (
                                                                    <li key={'subsubmenu_'+i}>{this.makeNavLinkTag(subSubmenu,'subsubmenu')}</li>
                                                                )
                                                            })
                                                        }
                                                    </ul> : null
                                                }
                                            </li>
                                        )
                                    }): ''}

                                </ul>
                            </li>
                        }
                        return (list)
                    })}

                </ul>
                <PinModal
                 color={this.state.color}  
                 moduleHed={this.state.moduleHed} 
                 pinType={this.state.pinType} 
                 modal_show={this.state.pinModalOpen} 
                 returnUrl={this.state.returnUrl}
                 closeModal={this.closeModal}
                 />
            </aside>
        );
    }
}
Sidebar.defaultProps = {
    replaceData:{':id':0}
}
export default Sidebar;
