import React, { Component } from 'react';

import {Link} from 'react-router-dom';
import PinModal from './PinModal';
import { checkItsNotLoggedIn, getPermission,checkLoginModule,pinHtml} from '../../service/common.js';
import { connect } from 'react-redux'

class AdminDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
           pinModalOpen:false,
		   permissions : (getPermission() == undefined)? [] : JSON.parse(getPermission()),

        }
        checkItsNotLoggedIn();
    }

    closeModal =()=> {
    	this.setState({pinModalOpen: false})
	}


    render() {
        return (
                <div>


        <section className="manage_top w-100">
            <div className="container-fluid Red">
				<div className="row">
				<div className="col-lg-2 col-sm-2"></div>
					<ul className="nav nav-tabs Category_tap col-lg-8 col-md-8 P_20_TB bb-0 mt-5" role="tablist">
						<li role="presentation" className="col-lg-6 col-sm-6 col-lg-offset-3 col-sm-offset-3 active px-0">
							<a  href="#Barry_details" aria-controls="Barry_details" role="tab" data-toggle="tab">Your Apps</a>
						</li>
					</ul>
					<div className="col-lg-2 col-sm-2"></div>
				</div>

				<div className="row">
					<div className="tab-content folder_tab">

							<div role="tabpanel" className="tab-pane active" id="Barry_details">
							<div className="row">
								<div className="col-lg-10 col-md-10 col-md-offset-1 col-lg-offset-1">
									<div className="row">
									<ul className="but_around_second text-center">
											{this.state.permissions.access_participant? <li><Link to={'/admin/participant/dashboard'}><span className="add_access p-colr">P</span><p>Participants</p></Link></li>:''}
											{this.state.permissions.access_organization? <li><Link to={'/admin/organisation/dashboard'}><span className="add_access o-colr">O</span><p>Organisation</p></Link></li>:''}
											{this.state.permissions.access_fms? <li>{pinHtml(this,'fms','dashboard')}</li> :''}
											{this.state.permissions.access_imail? <li><Link to={'/admin/imail/dashboard'}><span className="add_access i-colr">I</span><p>Imail</p></Link></li>:''}
											{this.state.permissions.access_member? <li><Link to={'/admin/member/dashboard'}><span className="add_access m-colr">M</span><p>Members</p></Link></li>:''}
											{this.state.permissions.access_schedule? <li><Link to={'/admin/schedule/unfilled/unfilled'}><span className="add_access s-colr">S</span><p>Schedule</p></Link></li>:''}
											{this.state.permissions.access_admin? <li>{pinHtml(this,'admin','dashboard')}</li> :''}
											{this.state.permissions.access_crm_admin? <li><Link to={'/admin/crm/participantadmin'}><span className="add_access c-colr">C</span><p>CRM Admin</p></Link></li>:(this.state.permissions.access_crm)?<li><Link to={'/admin/crm/participantuser'}><span className="add_access c-colr">C</span><p>CRM User</p></Link></li>:''}
                                            {this.state.permissions.access_recruitment? <li><Link to={'/admin/recruitment/dashboard'}><span className="add_access r-colr">R</span><p>Recruitment</p></Link></li>:''}
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<PinModal
					color={this.state.color}
					pinType={this.state.pinType}
					moduleHed={this.state.moduleHed}
					modal_show={this.state.pinModalOpen}
					returnUrl={this.state.returnUrl}
					closeModal={this.closeModal}
				/>
			</div>
		</section>

                </div>
                );
    }
}

const mapStateToProps = state => ({
    permissions : state.Permission.AllPermission,
})

export default connect(mapStateToProps)(AdminDashboard)
