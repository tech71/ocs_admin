import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import { postData } from '../../../service/common.js';
import { Doughnut } from 'react-chartjs-2';
import { connect } from 'react-redux';
import { CounterShowOnBox } from 'service/CounterShowOnBox.js';


class ScheduleDashboard extends Component {
    constructor(props) {
        super(props);
        this.active = this.props.active;
        this.state = {
            view_type_by_admin: 'month',
            view_type_by_app: 'month',
            view_type_graph: 'day',
            count_fill_by_admin: 0,
            count_fill_by_app: 0,
            sub_count: 0,
            all_shift: 0
        }
    }

    handleChange = (key, parameter) => {
        var state = {}
        state[key] = parameter;
        this.setState(state, () => {
            this.getShiftCounter();
        })
    }

    getShiftCounter = () => {
        postData('schedule/ScheduleDashboard/get_shift_dashboard_count', this.state).then((result) => {
            if (result.status) {
                this.setState(result.data);
            }
        });
    }

    componentDidMount() {
        this.getShiftCounter();
    }

    render() {
        const data = {
            labels: ['Other Shift', 'New Shift'],
            datasets: [{
                data: [(this.state.all_shift - this.state.sub_count), this.state.sub_count],
                backgroundColor: ['#d04170', '#d04170'], 
                hoverBackgroundColor: ['#d04170', '#d04170'],
                borderWidth: 0,
            }],
        };
        return (
            <div>
                <section className="manage_top">
                    <div className="container-fluid">

                        <div className="row  _Common_back_a">
                            <div className="col-lg-10 col-lg-offset-1 col-md-12"><Link className="d-inline-flex" to={'/admin/dashboard'}><div className="icon icon-back-arrow back_arrow"></div></Link></div>
                        </div>
                        <div className="row"><div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div></div>


                        <div className="row _Common_He_a">
                        <div className="col-lg-8 col-lg-offset-1 col-md-9 col-sm-8">
                            <h1 className="my-0 color">{this.props.showPageTitle}</h1>
                        </div>
                        <div className="col-lg-2 col-md-3 col-sm-4">
                            {this.props.roster? 
                            <Link className="Plus_button" to={'/admin/schedule/create_roster'}><i className="icon icon-add-icons create_add_but"></i><span>Create New Roster</span></Link>:
                            <Link className="Plus_button" to={'/admin/schedule/create_shift'}><i className="icon icon-add-icons create_add_but"></i><span>Create New Shift</span></Link>        
                            }
                        </div>
                    </div>
                        <div className="row"><div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div></div>

                        <div className="row">
                            <div className="col-lg-1"></div>
                            <div className="col-lg-10 col-md-12">
                                <ul className="landing_graph my-5">
                                    <li className="radi_2">
                                        <h2 className="text-center P_15_b cl_black">Shifts Filled  This Month:</h2>
                                        <div className="row pb-3 pt-4 w-100 mx-auto Graph_flex">

                                        <div className="col-md-8 col-xs-12 d-inline-flex align-self-center justify-content-center mb-3 ">
                                         <CounterShowOnBox minNumber={2} counterTitle={this.state.count_fill_by_admin}/>
                                     </div>

                                            {/* <div className="col-md-8 col-xs-12 d-inline-flex align-self-center"><span id="odometer_four" className="odometer w-100 text-center">{this.state.count_fill_by_admin}</span></div> */}
                                            <div className="W_M_Y_box col-md-4 col-xs-12 d-inline-flex align-self-center">
                                                <div className="vw_bx12 mx-auto">
                                                    <h4>View By</h4>
                                                    <span onClick={() => this.handleChange('view_type_by_admin', 'week')} className={(this.state.view_type_by_admin == 'week') ? 'color' : ''}>Week</span><br />
                                                    <span onClick={() => this.handleChange('view_type_by_admin', 'month')} className={(this.state.view_type_by_admin == 'month') ? 'color' : ''}>Month </span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="radi_2">
                                        <h2 className="text-center P_15_b cl_black">Todays Total:</h2>

                                        <div className="row pb-3 w-100 mx-auto Graph_flex">
                                            <div className="text-center align-self-center col-md-5 col-xs-12">
                                                <div className="myChart12 mx-auto" >
                                                    <Doughnut data={data} height={210} className="myDoughnut" legend={{ display: false }} />
                                                </div>
                                            </div>



                                            <div className="col-md-4 col-xs-12 text-center d-inline-flex align-self-center">
                                                <div className="myLegend mx-auto">
                                                    <h4 className="clr1">New = {this.state.sub_count}</h4>
                                                    <h4 className="clr2">Total = {this.state.all_shift}</h4>

                                                </div>
                                            </div>



                                            <div className="W_M_Y_box P_15_T col-md-3 col-xs-12 pb-3 d-inline-flex align-self-center">
                                                <div className="vw_bx12 mx-auto">
                                                    <h5 className=""><b>View by</b></h5>
                                                    <span onClick={() => this.handleChange('view_type_graph', 'week')} className={this.state.view_type_graph == 'week' ? 'color' : ''}>Week</span><br />
                                                    <span onClick={() => this.handleChange('view_type_graph', 'month')} className={this.state.view_type_graph == 'month' ? 'color' : ''}>Month </span><br />
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="radi_2">
                                        <h2 className="text-center P_15_b cl_black">Shifts Filled via App:</h2>
                                        <div className="row pb-3 pt-4 w-100 mx-auto Graph_flex">
                                        <div className="col-md-8 col-xs-12 d-inline-flex align-self-center justify-content-center mb-3 ">
                                         <CounterShowOnBox minNumber={2} counterTitle={this.state.count_fill_by_app}/>
                                     </div>
                                            {/* <div className="col-md-8 col-xs-12 d-inline-flex align-self-center"><span id="odometer" className="odometer w-100 text-center">{this.state.count_fill_by_app}</span></div> */}
                                            <div className="W_M_Y_box col-md-4 col-xs-12 d-inline-flex align-self-center">
                                                <div className="vw_bx12 mx-auto">
                                                    <h4>View By</h4>
                                                    <span onClick={() => this.handleChange('view_type_by_app', 'week')} className={(this.state.view_type_by_app == 'week') ? 'color' : ''}>Week</span><br />
                                                    <span onClick={() => this.handleChange('view_type_by_app', 'month')} className={(this.state.view_type_by_app == 'month') ? 'color' : ''}>Month </span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-md-1"></div>
                        </div>

                    </div>
                </section>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    showPageTitle: state.ScheduleDetailsData.activePage.pageTitle
})

export default connect(mapStateToProps)(ScheduleDashboard)
