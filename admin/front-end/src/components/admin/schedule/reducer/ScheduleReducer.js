const initialState = {
  shfit_details: {
   id:''
},
roster_details: {
  id:''
},
activePage:{pageTitle:'',pageType:''}
}


const ScheduleDetailsData = (state = initialState, action) => {
  switch (action.type) {
    case 'set_schedule_shfits_details_data':
    return { ...state, shfit_details: action.detailsData };

    case 'set_schedule_roster_details_data':
    return { ...state, roster_details: action.detailsData };

    case 'set_active_page_schedule':
    return {...state, activePage: action.value};

    default:
            return state
        }
    }
export default ScheduleDetailsData