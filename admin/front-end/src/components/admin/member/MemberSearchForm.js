import React from 'react';
import {Redirect } from 'react-router-dom';
import { handleChangeSelectDatepicker } from '../../../service/common.js';
import {dashboardFilterByType} from '../../../dropdown/memberdropdown.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';

class MemberSearchForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            search_value: '',
            include_inactive: '',
            redirect: false,
            searching: false,
            inactive: false
        };
        this.onChange = this.onChange.bind(this);
        this.updateValue = this.updateValue.bind(this);

    }

    updateValue(val) {
        this.setState = {srch_box: val};
    }

    onChange(e) {
        var state = {};
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }

    searchData(e) {
        e.preventDefault();
        this.setState({url: '/admin/member/list'});
        this.setState({searching: true});
        
        if(this.props.search_function){
           var  search = (this.state.search_value == 'undefined')? '': this.state.search_value;
            this.props.search_function(search,this.state.include_inactive );
        }
    }

    render() {
        
        return (
                <div>
                    {(this.state.searching) ? <Redirect to={{pathname: this.state.url, search: '?search=' + this.state.search_value + '&inactive=' + this.state.include_inactive, state: {inactive: this.state.include_inactive, search: this.state.search_value }}}  /> : ''}
                   
                   <div className="w-100">
                        <form method='get' onSubmit={this.searchData.bind(this)}   autoComplete="off">
                        <div className="row _Common_Search_a">
                            <div className="col-lg-6 col-lg-offset-1 col-sm-6">
                            <div className="search_bar">
                                <div className="input_search">
                                    <input type="text" className="form-control" placeholder="Search" name='search_value' value={this.state.search_value}  onChange={this.onChange} />
                                    <button type="submit"><span className="icon icon-search"></span></button>
                                    </div>
                                </div>
                            </div>
                
                            <div className="col-lg-2 col-sm-3">
                                <div className="row">
                                    <span className="col-md-6 include_box">
                                        <input type="checkbox" className="checkbox_big" id="Include" name="include_inactive" checked={this.state['include_inactive']} value={this.state.include_inactive || ''} onChange={(e) => this.setState({include_inactive: e.target.checked})}/>
                                        <label htmlFor="Include"><span></span><small className="pl-1">Include Inactive</small></label>
                                    </span>                
                                </div>
                            </div>
                
                            <div className="col-lg-2 col-sm-3">
                                <div className="box">
                                    <Select name="filterByType" required={true} simpleValue={true} searchable={false} Clearable={false} value={this.state.filterByType} onChange={(e) => handleChangeSelectDatepicker(this,e, 'filterByType')} 
                                        options={dashboardFilterByType(0)} placeholder="Filter by type" />               
                                </div>
                            </div>
                            </div>
                            <div className="row"><div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div></div>
                        </form>
                        </div>
                    
                </div>
                );
    }
}
export default MemberSearchForm;


