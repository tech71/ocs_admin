import {memberActiveTitle} from 'menujson/member_menu_json';
export const setMemberProfileData = (profileData) => ({
        type: 'set_member_profile_data',
        profileData
    })

export const updateMemberProfileData = (profileData) => ({
        type: 'member_update_profile_record',
        profileData
    })

export const setMemberActiveClass = (value) => ({
        type: 'set_active_class_member',
        value
    })

export const setActiveSelectPageData= (value) => {
    return {
        type: 'set_active_page_member',
        value
}}


export function setActiveSelectPage(request) {
    return (dispatch, getState) => {
        let pageData =memberActiveTitle;
        let pageType = pageData.hasOwnProperty(request) ? request: 'details';
        let pageTypeTitle = pageData[pageType];
        return dispatch(setActiveSelectPageData({pageType:pageType,pageTitle:pageTypeTitle}))
    }
}