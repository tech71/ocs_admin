import React from 'react';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from '../../../config.js';
import Header from '../../admin/Header';
import Footer from '../../admin/Footer';
import MembersList from './MembersList';
import { checkItsNotLoggedIn, postData } from '../../../service/common.js';
import { dashboardFilterByType } from '../../../dropdown/memberdropdown.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { Doughnut } from 'react-chartjs-2';
import { connect } from 'react-redux';
import { setSubmenuShow } from 'components/admin/actions/SidebarAction';
import { CounterShowOnBox } from 'service/CounterShowOnBox.js';

class MemberDashboard extends React.Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);

        this.state = {
            srch_box: '',
            search_value: '',
            include_inactive: false,
            filtered: '',
            testimonial: []
        };
        this.onChange = this.onChange.bind(this);
        this.updateValue = this.updateValue.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    updateValue(val) {
        this.setState = { srch_box: val };
    }

    searchData(e) {
        e.preventDefault();
        var srch_ary = {
            'srch_box': this.state.search_value,
            'include_inactive': this.state.include_inactive,
            'search_by': this.state.filterByType,
        }
        this.setState({ filtered: srch_ary }, function () { });
    }

    onChange(e) {
        var state = {};
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }

    componentDidMount() {
        this.props.setSubmenuShow(0);
        this.getMemberTestimonial();
    }

    getMemberTestimonial = () => {
        postData('member/MemberAction/get_member_testimonial').then((result) => {
            if (result.status) {
                this.setState({ testimonial: result.data }, () => { });

            }
        });
    }

    render() {
        return (
            <div>
                <script src={'assets/js/site_js/counter.js'} />

                <div className="row  _Common_back_a">
                    <div className="col-lg-10 col-lg-offset-1 col-md-12">
                        <Link className="d-inline-flex" to={'/admin/dashboard'}><span className="icon icon-back-arrow back_arrow"></span></Link>
                    </div>
                </div>
                <div className="row"><div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div></div>

                <div className="row _Common_He_a">
                    <div className="col-lg-8 col-lg-offset-1  col-md-12">
                        <h1 className="color">Members</h1>
                    </div>
                    <div className="col-lg-2">
                    </div>
                </div>

                <div className="row"><div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div></div>


                <div className="row">
                    <div className="col-lg-1"></div>
                    <div className="col-lg-10 col-md-12">

                        {this.state.testimonial.map((val, index) => (
                            <ul className="landing_graph mt-5">
                                <li className="radi_2 w-100 text-center">
                                    <h3 className="text-center  d-inline-block">{val.title}:</h3>
                                    <h3 className="d-inline-block color">"{val.testimonial}"</h3>
                                    <span className="name d-inline-block"><small>- {val.full_name}</small></span>
                                </li>
                            </ul>
                        ))}

                    </div>
                </div>


                <div className="row">

                    <div className="col-lg-10 col-lg-offset-1 col-md-12">
                        <ul className="landing_graph landing_graph_item_2 mt-5">
                            <MemberGraphCount />
                            <MemberCount />
                        </ul>
                    </div>

                </div>

                <div className="mt-5">
                    <div className="row"><div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div></div>
                    <form method='get' onSubmit={this.searchData.bind(this)} autoComplete="off">
                        <div className="row _Common_Search_a">
                            <div className="col-lg-6 col-lg-offset-1 col-xs-6">
                                <div className="search_bar">
                                    <div className="input_search">
                                        <input type="text" className="form-control" placeholder="Search" name='search_value' value={this.state.search_value} onChange={this.onChange} />
                                        <button type="submit"><span className="icon icon-search"></span></button>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-2 col-xs-3">
                                <div className="row">
                                    <span className="col-md-6 include_box">
                                        <input type="checkbox" className="checkbox_big" id="Include" name="include_inactive" checked={this.state['include_inactive']} value={this.state.include_inactive || ''} onChange={(e) => this.setState({ include_inactive: e.target.checked, filtered: { include_inactive: e.target.checked, filterByType: this.state.filtered.filterByType } })} />
                                        <label htmlFor="Include"><span></span><small className="pl-1">Include Inactive</small></label>
                                    </span>
                                </div>
                            </div>

                            <div className="col-lg-2 col-xs-3">
                                <div className="box">
                                    <Select name="filterByType" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.filtered.filterByType || ''} onChange={(e) => this.setState({ filtered: { filterByType: e, include_inactive: this.state.include_inactive } })}
                                        options={dashboardFilterByType(0, 'clearable')} placeholder="Filter by type" />
                                </div>
                            </div>
                        </div>
                    </form>
                    <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div>
                </div>

                <div className="col-lg-12 col-md-12" ></div>

                <MembersList filtered={this.state.filtered} />


            </div>
        );
    }
}
const mapStateToProps = state => ({
})

const mapDispatchtoProps = (dispach) => {
    return {
        setSubmenuShow: (result) => dispach(setSubmenuShow(result))
    }
};
export default connect(mapStateToProps, mapDispatchtoProps)(MemberDashboard);

//
class MemberCount extends React.Component {
    constructor(props) {
        super(props);
        this.state = { MemberCount: 0, view_type: 'month', AllMemberCount: 0 };

    }

    componentDidMount() {
        this.memberAjax();
    }

    memberCountBy(type) {
        this.setState({ view_type: type }, function () {
            this.memberAjax();
        });
    }

    //
    memberAjax() {
        postData('member/MemberDashboard/member_count', this.state).then((result) => {
            if (result.status) {
                this.setState({ MemberCount: result.member_count, AllMemberCount: result.all_member_count });
            } else {
                this.setState({ error: result.error });
            }
        });
    }

    render() {
        return (



            <li className="radi_2">
                <h2 className="text-center cl_black">New Member Counter:</h2>
                <div className="row  pb-3 align-self-center w-100 mx-auto Graph_flex">
                    <div className="col-md-8 col-xs-12 d-inline-flex align-self-center justify-content-center mb-3">
                    <CounterShowOnBox counterTitle={this.state.MemberCount} classNameAdd=""/>
                    </div>
                    {/* <div className="col-md-8 col-xs-12 d-inline-flex align-self-center"><span id="odometer" className="odometer w-100 text-center">{this.state.MemberCount}</span></div> */}
                    <div className="W_M_Y_box  col-md-4 col-xs-12 d-inline-flex align-self-center">
                        <div className="vw_bx12 mx-auto">
                            <h5><b>View By</b></h5>
                            <span onClick={() => this.memberCountBy('week')} className={this.state.view_type == 'week' ? 'color' : ''}>Week</span><br />
                            <span onClick={() => this.memberCountBy('month')} className={this.state.view_type == 'month' ? 'color' : ''}>Month </span><br />
                            <span onClick={() => this.memberCountBy('year')} className={this.state.view_type == 'year' ? 'color' : ''}> Year</span>
                        </div>
                    </div>
                </div>
            </li>

        );
    }
}

class MemberGraphCount extends React.Component {
    constructor(props) {
        super(props);
        this.state = { MemberCount: 0, view_type: 'month', AllMemberCount: 0 };

    }

    componentDidMount() {
        this.memberAjax();
    }

    memberCountBy(type) {
        this.setState({ view_type: type }, function () {
            this.memberAjax();
        });
    }

    //
    memberAjax() {
        postData('member/MemberDashboard/member_count', this.state).then((result) => {
            if (result.status) {
                this.setState({ MemberCount: result.member_count, AllMemberCount: result.all_member_count });
            } else {
                this.setState({ error: result.error });
            }
        });
    }

    render() {
        const data = {
            labels: ['New', 'Total'],
            datasets: [{
                data: [(this.state.AllMemberCount - this.state.MemberCount), this.state.MemberCount],
                backgroundColor: ['#fe7b31', '#ee5802'] ,
                hoverBackgroundColor: ['#fe7b31', '#ee5802'],
                borderWidth: 0,
            }],
            
        };

        return (
            <li className="radi_2">
                <h2 className="text-center P_15_b cl_black">New Member Graph:</h2>
                <div className="row  pb-3 align-self-center w-100 mx-auto Graph_flex">
                    <div className="text-center col-md-5 col-xs-12">
                        <div className="myChart12 mx-auto" >
                            <Doughnut height={210} data={data} legend={{ display: false }} />
                        </div>
                    </div>

                    <div className="col-md-4 col-xs-12 text-center d-inline-flex align-self-center">
                        <div className="myLegend mx-auto">
                            <h4 className="clr1">New = {this.state.MemberCount}</h4>
                            <h4 className="clr2 color">Total = {this.state.AllMemberCount}</h4>
                        </div>
                    </div>


                    <div className="W_M_Y_box P_15_T col-md-3 col-xs-12 pb-3 d-inline-flex align-self-center">
                        <div className="vw_bx12 mx-auto">
                            <h5><b>View By</b></h5>
                            <span onClick={() => this.memberCountBy('week')} className={this.state.view_type == 'week' ? 'color' : ''}>Week</span><br />
                            <span onClick={() => this.memberCountBy('month')} className={this.state.view_type == 'month' ? 'color' : ''}>Month </span><br />
                            <span onClick={() => this.memberCountBy('year')} className={this.state.view_type == 'year' ? 'color' : ''}> Year</span>
                        </div>
                    </div>
                </div>
            </li>
        );
    }
}


