import React from 'react';
import { postData } from '../../../service/common.js';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import MemberProfile from './MemberProfile';
import { CounterShowOnBox } from 'service/CounterShowOnBox.js';
import { connect } from 'react-redux';

class MemberShiftOverView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            CountRecord: [],
            CounterFilled: 0,
            CounterRejected: 0,
            CounterCancelled: 0,
            ViewByRejected: 'quarter',
            ViewByCancelled: 'quarter',
            ViewByFilled: 'quarter',
            from_date: '',
            to_date: ''
        };
    }

    componentDidMount() {
        this.memberAjax();
    }

    memberCountBy(ViewBy, ReportType) {
        this.setState({ view_type: ViewBy }, function () {
            this.memberAjax();
        });
    }

    memberAjax() {
        var member_id = this.props.props.match.params.id;
        var requestData = { member_id: member_id, from_date: this.state.from_date, to_date: this.state.to_date };
        postData('member/MemberDashboard/member_shift_count', requestData).then((response) => {
            this.setState({
                CountRecord: response,
                CounterFilled: response.filled_count_Q,
                CounterRejected: response.rejected_count_Q,
                CounterCancelled: response.cancelled_count_Q,
            });
        });
    }

    //
    onClickFilter = (type, viewBy) => {
        if (type == 'reject' && viewBy == 'year') {
            this.setState({ CounterRejected: this.state.CountRecord.rejected_count_Y, ViewByRejected: viewBy });
        }
        else if (type == 'reject' && viewBy == 'Week') {
            this.setState({ CounterRejected: this.state.CountRecord.rejected_count_W, ViewByRejected: viewBy });
        }
        else if (type == 'reject' && viewBy == 'quarter') {
            this.setState({ CounterRejected: this.state.CountRecord.rejected_count_Q, ViewByRejected: viewBy });
        }

        else if (type == 'cancelled' && viewBy == 'year') {
            this.setState({ CounterCancelled: this.state.CountRecord.cancelled_count_Y, ViewByCancelled: viewBy });
        }
        else if (type == 'cancelled' && viewBy == 'Week') {
            this.setState({ CounterCancelled: this.state.CountRecord.cancelled_count_W, ViewByCancelled: viewBy });
        }
        else if (type == 'cancelled' && viewBy == 'quarter') {
            this.setState({ CounterCancelled: this.state.CountRecord.cancelled_count_Q, ViewByCancelled: viewBy });
        }

        else if (type == 'filled' && viewBy == 'year') {
            this.setState({ CounterFilled: this.state.CountRecord.filled_count_Y, ViewByFilled: viewBy });
        }
        else if (type == 'filled' && viewBy == 'Week') {
            this.setState({ CounterFilled: this.state.CountRecord.filled_count_W, ViewByFilled: viewBy });
        }
        else if (type == 'filled' && viewBy == 'quarter') {
            this.setState({ CounterFilled: this.state.CountRecord.filled_count_Q, ViewByFilled: viewBy });
        }
    }

    selectChange(selectedOption, fieldname) {
        var state = {};
        state[fieldname] = selectedOption;
        this.setState(state, () => {
            this.memberAjax();
        });
    }

    render() {
        return (

            <div>
                <MemberProfile MemberData={this.state} ProfileId={this.props.props.match.params.id} Active={'overview'} pageTypeParms="overview" />

                <div className="row">

                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">

                        <div className="row">
                            <div className="col-lg-12">
                                <div className="row">
                                    <div className="col-md-12"><div className="bor_T"></div></div>
                                    <div className="col-sm-12 P_7_TB"><h3><b>Shift Analysis for {this.props.MemberProfile.first_name}</b></h3></div>
                                    <div className="col-sm-12"><div className="bor_T"></div></div>
                                </div>
                            </div>
                        </div>

                        <div className="row P_15_T">
                            <div className="col-lg-3 col-sm-4">
                                <div className="row">
                                    <div className="col-lg-6 col-sm-6">
                                        <label><b>From</b></label>
                                        <DatePicker className="text-center" selected={this.state.from_date} onChange={(e) => this.selectChange(e, 'from_date')} dateFormat="DD/MM/YYYY" utcOffset={0} />
                                    </div>

                                    <div className="col-lg-6 col-sm-6">
                                        <label><b>To</b></label>
                                        <DatePicker className="text-center" selected={this.state.to_date} name="expiry_date" onChange={(e) => this.selectChange(e, 'to_date')} minDate={moment(this.state.from_date)} dateFormat="DD/MM/YYYY" utcOffset={0} />
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-9 col-sm-8">&nbsp;</div>
                        </div>
                    </div>

                </div>

                <div className="row mt-5">
                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                        <div className="row">
                        <div className="col-md-12">
                            <div className="Over_counter_">
                                <div className="Over_counter_div_">
                                    <h3 className="mb-4"><b>Filled by {this.props.first_Name}:</b></h3>
                                    <CounterShowOnBox counterTitle={this.state.CounterFilled} classNameAdd="Overview_conter" />
                                    <div className="Over_counter_tex_">
                                        <span className={(this.state.ViewByFilled && this.state.ViewByFilled == 'Week') ? 'active' : ''} onClick={(e) => this.onClickFilter('filled', 'Week')}>Week</span>
                                        <span className={(this.state.ViewByFilled && this.state.ViewByFilled == 'quarter') ? 'active' : ''} onClick={(e) => this.onClickFilter('filled', 'quarter')}>Quarter </span>
                                        <span className={(this.state.ViewByFilled && this.state.ViewByFilled == 'year') ? 'active' : ''} onClick={(e) => this.onClickFilter('filled', 'year')}>Year</span>
                                    </div>
                                </div>

                                <div className="Over_counter_div_">
                                    <h3  className="mb-4"><b>Cancelled by {this.props.first_Name}:</b></h3>
                                    <CounterShowOnBox counterTitle={this.state.CounterCancelled} classNameAdd="Overview_conter" />
                                    <div className="Over_counter_tex_">
                                        <span className={(this.state.ViewByCancelled && this.state.ViewByCancelled == 'Week') ? 'active' : ''} onClick={(e) => this.onClickFilter('cancelled', 'Week')}>Week</span>
                                        <span className={(this.state.ViewByCancelled && this.state.ViewByCancelled == 'quarter') ? 'active' : ''} onClick={(e) => this.onClickFilter('cancelled', 'quarter')}>Quarter </span>
                                        <span className={(this.state.ViewByCancelled && this.state.ViewByCancelled == 'year') ? 'active' : ''} onClick={(e) => this.onClickFilter('cancelled', 'year')}> Year</span>
                                    </div>
                                </div>

                                <div className="Over_counter_div_">
                                    <h3  className="mb-4"><b>Rejected by {this.props.first_Name}:</b></h3>
                                    <CounterShowOnBox counterTitle={this.state.CounterRejected} classNameAdd="Overview_conter" />
                                    <div className="Over_counter_tex_">
                                        <span className={(this.state.ViewByCancelled && this.state.ViewByCancelled == 'Week') ? 'active' : ''} onClick={(e) => this.onClickFilter('cancelled', 'Week')}>Week</span>
                                        <span className={(this.state.ViewByCancelled && this.state.ViewByCancelled == 'quarter') ? 'active' : ''} onClick={(e) => this.onClickFilter('cancelled', 'quarter')}>Quarter </span>
                                        <span className={(this.state.ViewByCancelled && this.state.ViewByCancelled == 'year') ? 'active' : ''} onClick={(e) => this.onClickFilter('cancelled', 'year')}> Year</span>
                                    </div>
                                </div>
                            </div>


                            {/* <div className="col-lg-4 P_25_T">
                            <h3 className="text-center P_7_b">Filled by {this.props.first_Name}:</h3>
                            <div className="text-center P_7_T"><span id="odometer" className="odometer counter_1">{this.state.CounterFilled}</span></div>
                            <div className="W_M_Y_box P_15_T">
                                <span className={(this.state.ViewByFilled && this.state.ViewByFilled=='Week')?'active':''} onClick={(e)=>this.onClickFilter('filled','Week')}>Week</span>
                                <span className={(this.state.ViewByFilled && this.state.ViewByFilled=='quarter')?'active':''} onClick={(e)=>this.onClickFilter('filled','quarter')}>Quarter </span>
                                <span className={(this.state.ViewByFilled && this.state.ViewByFilled=='year')?'active':''} onClick={(e)=>this.onClickFilter('filled','year')}>Year</span>
                            </div>
                        </div>
                        <div className="col-lg-4 P_25_T">
                            <h3 className="text-center P_7_b">Cancelled by {this.props.first_Name}:</h3>
                            <div className="text-center P_7_T"><span id="odometer1" className="odometer counter_1">{this.state.CounterCancelled}</span></div>
                            <div className="W_M_Y_box P_15_T">
                                <span className={(this.state.ViewByCancelled && this.state.ViewByCancelled=='Week')?'active':''} onClick={(e)=>this.onClickFilter('cancelled','Week')}>Week</span>
                                <span className={(this.state.ViewByCancelled && this.state.ViewByCancelled=='quarter')?'active':''} onClick={(e)=>this.onClickFilter('cancelled','quarter')}>Quarter </span>
                                <span className={(this.state.ViewByCancelled && this.state.ViewByCancelled=='year')?'active':''} onClick={(e)=>this.onClickFilter('cancelled','year')}> Year</span>
                            </div>
                        </div>
                        <div className="col-lg-4 P_25_T">
                            <h3 className="text-center P_7_b">Rejected by {this.props.first_Name}:</h3>
                            <div className="text-center P_7_T"><span id="odometer2" className="odometer counter_1">{this.state.CounterRejected}</span></div>
                            <div className="W_M_Y_box P_15_T">
                                <span className={(this.state.ViewByRejected && this.state.ViewByRejected=='Week')?'active':''} onClick={(e)=>this.onClickFilter('reject','Week')}>Week</span>
                                <span className={(this.state.ViewByRejected && this.state.ViewByRejected=='quarter')?'active':''} onClick={(e)=>this.onClickFilter('reject','quarter')}>Quarter </span>
                                <span className={(this.state.ViewByRejected && this.state.ViewByRejected=='year')?'active':''} onClick={(e)=>this.onClickFilter('reject','year')}> Year</span>
                            </div>
                        </div> */}

                        </div></div>
                        </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    MemberProfile: state.MemberReducer.memberProfile
})

const mapDispatchtoProps = (dispach) => {
    return {}
}
const MemberOverView = connect(mapStateToProps, mapDispatchtoProps)(MemberShiftOverView);

export default MemberOverView;




