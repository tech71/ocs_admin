import React from 'react';
import { Link } from 'react-router-dom';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { postData } from '../../../service/common.js';
import { statusOptionMember } from '../../../dropdown/memberdropdown.js';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { CustomProfileImage, customHeading } from '../../../service/CustomContentLoader.js';
import { connect } from 'react-redux'
import { setMemberProfileData, setMemberActiveClass,setActiveSelectPage } from './actions/MemberAction.js';
import {setSubmenuShow} from 'components/admin/actions/SidebarAction';

class MemberProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = { enable_app_access: '', gender: 1 };
    }

    componentDidMount() {
        this.props.setSubmenuShow(1);
        var urltemp = window.location.href;
        var the_arr = urltemp.split('/');
        var current_seg = the_arr[5];
        if (current_seg) { this.setState({ 'selectedShiftOpt': current_seg }); }

        if (this.props.MemberProfile.ocs_id == '' || this.props.MemberProfile.ocs_id == null || this.props.MemberProfile.ocs_id == undefined || (this.props.MemberProfile.ocs_id != '' && this.props.ProfileId != '' && this.props.MemberProfile.ocs_id != this.props.ProfileId)) {
            this.getMemberProfile(this.props.ProfileId);
        }

        if (this.props.Active != undefined && this.props.Active != '') {
            this.props.changeMemberNavigationClass(this.props.Active);
        }
        this.getPageSelect();
    }

    getMemberProfile = (member_id)=> {
        var requestData = { member_id: member_id };
        this.setState({ loading: true });
        postData('member/MemberDashboard/get_member_profile', requestData).then((result) => {
            if (result.status) {
                this.props.getMemberProfileData(result.data.basic_detail);
            } else {
                window.location = '/admin/member/dashboard';
                this.setState({ error: result.error });
            }
            this.setState({ loading: false });
        });
    }

    componentWillReceiveProps(newProps) {
        this.setState(newProps.MemberProfile);
    }

    enableAppExcess(e) {
        var member_id = this.props.ProfileId;
        this.setState({ enable_app_access: e.target.checked });
        var requestData = { status: (e.target.checked) ? 1 : 0, member_id: member_id };
        postData('member/MemberDashboard/member_app_access', requestData).then((result) => {
            this.setState({ loading: false });
            if (result.status) {

            } else {
                this.setState({ error: result.error });
            }
        });
    }

    selectChange = (selectedOption, fieldname) => {
        var member_id = this.props.ProfileId;
        var state = {};
        state[fieldname] = selectedOption;
        var requestData = { status: selectedOption, member_id: member_id };
        this.setState(state);
        postData('member/MemberDashboard/change_status', requestData).then((result) => {
            this.setState({ loading: false });
            if (result.status) {

            } else {
                this.setState({ error: result.error });
            }
        });
    }

    getPageSelect(){
        this.props.setActivePage(this.props.pageTypeParms);
      }

    componentDidUpdate(){
          if(this.props.pageTypeParms!=undefined && this.props.showTypePage!=this.props.pageTypeParms){
              this.getPageSelect();
          }
      }

    render() {
        return (
            <div>

                <div className="row  _Common_back_a">
                    <div className="col-lg-10 col-lg-offset-1 col-md-12">
                        <Link className="d-inline-flex" to={'/admin/member/dashboard'}><span className="icon icon-back-arrow back_arrow"></span></Link>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1 col-md-12">
                        <div class="bor_T"></div>
                    </div>
                </div>
                <div className="row _Common_He_a">
                    <div className="col-lg-8 col-lg-offset-1  col-md-12">
                      <ReactPlaceholder defaultValue={''} showLoadingAnimation ={true}  customPlaceholder={ customHeading(20)} ready={!this.state.loading}>
                        <h1 className="color">{this.props.MemberProfile.full_name}</h1>
                        </ReactPlaceholder>
                    </div>
                </div>

                <div className="row"> <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div></div>


                <div className="row P_20_TB">
                    <div className="col-lg-10 col-lg-offset-1 col-sm-12">
                        <ReactPlaceholder showLoadingAnimation customPlaceholder={CustomProfileImage} ready={!this.state.loading}>
                            <table width="100%" className="user_profile" cellPadding="0" cellSpacing="0">
                                <tbody>
                                    <tr>
                                        <td className="width_130">
                                            <div className="user_img">
                                                <img alt="user" src={'/assets/images/admin/' + ((this.state.gender) && this.state.gender == 1 ? 'boy.svg' : 'girls.svg')} />
                                            </div>
                                        </td>
                                        <td>

                                            <ul>
                                                <li><span>HCM-ID:</span>{this.state.ocs_id}</li>
                                                <li><span>Department:</span> LifeChoices</li>
                                                <li><span>Consultant:</span> Ben Fleming</li>
                                                <li className="d-inline-block mt-4"><div className="pull-left pr-2"><span>Status:</span></div>  <span className="pull-left box_wide">
                                                    <Select name="status" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.status} onChange={(e) => this.selectChange(e, 'status')}
                                                        options={statusOptionMember()} placeholder="Change Status" />
                                                </span>

                                                    <span className="pull-left">
                                                        <p className="select_all">
                                                            <input className="input_c" type="checkbox" name="enable_app_access" checked={this.state.enable_app_access == 1 || ''} value={this.state.enable_app_access || ''} id="thing" onChange={this.enableAppExcess.bind(this)} />
                                                            <label htmlFor="thing"></label> <span>Enable App Access</span></p>
                                                    </span>  </li>
                                                <li className="pt-2"></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ReactPlaceholder>
                    </div>
                </div>
            </div>
        );
    }
}

//
const mapStateToProps = state => ({
    MemberProfile: state.MemberReducer.memberProfile,
    ActiveClass: state.MemberReducer.ActiveClassProfilePage,
    showPageTitle: state.MemberReducer.activePage.pageTitle,
})

const mapDispatchtoProps = (dispach) => {
    return {
        getMemberProfileData: (value) => dispach(setMemberProfileData(value)),
        changeMemberNavigationClass: (value) => dispach(setMemberActiveClass(value)),
        setSubmenuShow: (result) => dispach(setSubmenuShow(result)),
        setActivePage:(result) => dispach(setActiveSelectPage(result))
    }
}

export default connect(mapStateToProps, mapDispatchtoProps, null, { withRef: true })(MemberProfile)