import React from 'react';
import jQuery from "jquery";
import { ROUTER_PATH } from '../../../config.js';
import { postData, checkItsNotLoggedIn } from '../../../service/common.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { memberAboutViewBy, memberAboutTravelDistance, memberYesNo, memberPreferContactDropdown } from '../../../dropdown/memberdropdown.js';
import MemberUpdatePopUp from './MemberUpdatePopUp';
import Modal from 'react-bootstrap/lib/Modal'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { customProfile, customHeading } from '../../../service/CustomContentLoader.js';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { PanelGroup, Panel } from 'react-bootstrap';
import { connect } from 'react-redux'
import { setMemberActiveClass } from './actions/MemberAction.js';
import MemberProfile from './MemberProfile';
import { ToastUndo } from 'service/ToastUndo.js'


const divStyle = {
    display: 'none'
};

class DottedLine extends React.Component {
    render() {
        return <div className="row f_color_size py-3">
            <div className="col-lg-3 col-md-3 f align_e_2 col-xs-3"></div>
            <div className="col-lg-9 col-md-9 f align_e_1 col-xs-9 dotted_line"></div>
        </div>;
    }
}

//
class MembersAbout extends React.Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        // var all=[]  ;
        this.state = {
            pref: [],
            avail: [],
            openModal: false,
            updateData: [],
            is_update: false,
            phone_ary: [],
            email_ary: [],
            completeAddress: [],
            kin_ary: []
        };
    }

    componentDidMount() {
        this._isMounted = true;
        var member_id = this.props.props.match.params.id;
        this.getMemberPreference(member_id);
        this.getMemberAvailability();
        this.setState(this.props.MemberProfile);
        this.props.changeMemberNavigationClass('')
    }


    componentWillReceiveProps(newProps) {
        this.setState(newProps.MemberProfile);
    }

    getMemberPreference = (member_id) => {
        var requestData = { member_id: member_id };
        postData('member/MemberDashboard/get_member_preference', requestData).then((result) => {
            if (result.status) {
                this.setState({ pref: result.data });
            } else {
                this.setState({ error: result.error });
            }
            this.setState({ loading: false });
        });
    }

    getMemberAvailability = () => {
        var member_id = this.props.props.match.params.id;
        var requestData = { member_id: member_id, status: (this.state.view_by_status) ? this.state.view_by_status : 'Active' };
        postData('member/MemberDashboard/get_member_availability', requestData).then((result) => {
            if (result.status) {
                this.setState({ avail: result.data });
                //var x = JSON.parse(this.state.avail[0].first_week).all;
            } else {
                this.setState({ avail: result.data });
            }
        });
    }

    //bind
    closeAvailabiltyPopUp = () => {
        this.setState({ openModal: false });
    }

    viewByselectChange(selectedOption, fieldname) {
        this.setState({ view_by_status: selectedOption }, () => {
            this.getMemberAvailability();
        });
    }

    openAvailability = () => {

        this.setState({
            openModal: false,
            is_update: false,
        })


        jQuery('#AmyModal').modal('show');
    }

    editAvailabiltyPopUp = (val) => {
        this.setState({ openModal: true, 'updateData': val, 'is_update': true }, () => {
            //openNextWeek();
            jQuery('#AmyModal').modal('show');
        });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    closeMemberUpdatePopUp = (param) => {
        this.setState({ member_update_modal: false },()=>{
            if(param){
                this.refs.profileData.getWrappedInstance().getMemberProfile(this.props.props.match.params.id);
            }
        });
    }

    render() {
        return (
            <div>

             


                <div className="row">

                    <MemberProfile MemberData={this.state} ProfileId={this.props.props.match.params.id} Active={'about'} ref="profileData" pageTypeParms={this.props.props.match.params.page} />

                    <div className="col-lg-10 col-sm-12 col-lg-offset-1 px-0">
                      
                        <div className="tab-content">
                            <div role="tabpanel" className={this.props.showTypePage=='details' ? "tab-pane active" :"tab-pane"} id="Barry_details">
                                <ReactPlaceholder showLoadingAnimation type='textRow' customPlaceholder={customProfile} ready={!this.state.loading}>
                                    <div className="row">
                                        <div className="col-md-12">
                                        <div className="bor_T"></div>
                                            <div className="P_7_TB"><h3><b>About
                                                 {/* {this.state.first_name} */}
                                                 </b></h3></div>
                                        </div>
                                        <div className="col-md-12"><div className="bor_T"></div></div>
                                    </div>
                                    <div className="px-3 mt-4">

                                        <div className="row f_color_size">
                                            <div className="col-md-3 f align_e_2 col-xs-3">Name: </div>
                                            <div className="col-md-9 f align_e_1 col-xs-9">{this.state.full_name}</div>
                                        </div>
                                        <DottedLine />

                                        <div className="row f_color_size">
                                            <div className="col-md-3 f align_e_2 col-xs-3">HCM-ID:</div>
                                            <div className="col-md-9 f align_e_1 col-xs-9">{this.state.ocs_id}</div>
                                        </div>
                                        <DottedLine />

                                        <div className="row f_color_size">
                                            <div className="col-md-3 f align_e_2 col-xs-3">Preferred Contact: </div>
                                            <div className="col-md-9 f align_e_1 col-xs-9">{(this.state.prefer_contact && this.state.prefer_contact > 0) ? memberPreferContactDropdown(this.state.prefer_contact) : 'N/A'}</div>
                                        </div>
                                        <DottedLine />

                                        {(this.state.phone_ary.length > 0) ? <span>
                                            {this.state.phone_ary.map((value, idx) => (
                                                <div className="row f_color_size" key={idx}>
                                                    <div className="col-md-3 f align_e_2 col-xs-3">Phone ({(value.primary_phone) && value.primary_phone == 1 ? 'Primary' : 'Secondary'}):</div>
                                                    <div className="col-md-9 f align_e_1 col-xs-9"><u>{value.phone.indexOf('+') == -1 ? '+' : ''}{value.phone}</u></div>
                                                </div>

                                            ))}  <DottedLine /></span> : ''
                                        }



                                        {(this.state.email_ary.length > 0) ? <span>
                                            {this.state.email_ary.map((value, idx) => (
                                                <div className="row f_color_size" key={idx}>
                                                    <div className="col-md-3 f align_e_2 col-xs-3">Email ({(value.primary_email) && value.primary_email == 1 ? 'Primary' : 'Secondary'}):</div>
                                                    <div className="col-md-9 f align_e_1 col-xs-9"><u>{value.email}</u></div>
                                                </div>
                                            ))}  <DottedLine /></span> : ''
                                        }



                                        {(this.state.completeAddress.length > 0) ? <span>
                                            {this.state.completeAddress.map((val, idx) => (
                                                <div className="row f_color_size" key={idx + 2}>
                                                    <div className="col-md-3 f align_e_2 col-xs-3">Address ({(val.primary_address) && val.primary_address == 1 ? 'Primary' : 'Secondary'}): </div>
                                                    <div className="col-md-9 f align_e_1 col-xs-9">{val.street + ',' + (val != null && val.hasOwnProperty('city') && val.city != null && val.city.hasOwnProperty('label') ? val.city.label : '') + ',' + val.postal + ',' + val.statename}</div>
                                                </div>
                                            ))} <DottedLine /> </span> : ''
                                        }

                                        <div className="row f_color_size">
                                            <div className="col-md-3 f align_e_2 col-xs-3">D.O.B: </div>
                                            <div className="col-md-9 f align_e_1 col-xs-9">{((typeof (this.state.dob) == 'object') ? moment(this.state.dob).format('DD/MM/YYYY') : this.state.dob + ' - 36 years old')  || 'N/A'}</div>
                                        </div>

                                        {(this.state.kin_ary.length > 0) ? <DottedLine /> : ''}

                                        {(this.state.kin_ary != undefined && this.state.kin_ary != null && this.state.kin_ary.length > 0) ? <span>
                                            {this.state.kin_ary.map((val, idx) => (
                                                <div className="row f_color_size" key={idx}>
                                                    <div className="col-md-3 f align_e_2 col-xs-3">Next of Kin ({(val.primary_kin) && val.primary_kin == 1 ? 'Primary' : 'Secondary'}): </div>
                                                    <div className="col-md-9 f align_e_1 col-xs-9">{val.firstname + (val.lastname != '' ? ' ' + val.lastname : '')} ({val.relation}) - <u className="font_w_3"> {val.phone} </u> - {val.email}</div>
                                                </div>
                                            ))}  </span> : ''
                                        }
                                    </div>
                                    <div className="row P_15_T">
                                        <div className="col-lg-3 col-md-4 pull-right">
                                            <button className="but" onClick={() => this.setState({ member_update_modal: true })}>
                                                Update {this.state.first_name + '\'s'} Details
                                    </button>
                                        </div>
                                    </div>

                                    <MemberUpdatePopUp
                                        memberId={this.props.props.match.params.id}
                                        member_update_modal={this.state.member_update_modal}
                                        closeMemberUpdatePopUp={this.closeMemberUpdatePopUp}
                                    />

                                </ReactPlaceholder>
                            </div>

                            <div role="tabpanel" className={this.props.showTypePage=='availability' ? "tab-pane active" :"tab-pane"} id="Barry_availabitity">
                                <div className="row">
                                    <div className="col-lg-9 col-md-9">
                                    <div className="bor_T"></div>
                                        <div className="P_7_TB"><h3><b>
                                        {/* {this.state.first_name + '\'s'} */}
                                         Availability</b></h3></div>
                                        <div className="bor_T"></div>
                                    </div>

                                    <div className="col-lg-3 col-md-3">
                                        <div className="box">
                                            <Select name="view_by_status" required={true} simpleValue={true} searchable={false} clearable={false} options={memberAboutViewBy()} placeholder="View By" value={this.state.view_by_status} onChange={(e) => this.viewByselectChange(e, 'view_by_status')} />
                                        </div>
                                    </div>
                                </div>

                                <div className="row P_15_TB">
                                    <div className="col-lg-9 col-md-9">
                                        <div className="panel-group accordion_me accordion_member" id="accordion" role="tablist" aria-multiselectable="true">
                                            <PanelGroup accordion id="accordion-uncontrolled-example" defaultActiveKey="2">
                                                {(this.state.avail.length > 0) ? <span>
                                                    {this.state.avail.map((val, aval_id) => (

                                                        <Panel eventKey={aval_id} className="px-0 py-0" key={aval_id} >
                                                            <Panel.Heading>

                                                                <Panel.Title toggle>

                                                                    <span role="button" data-toggle="collapse" data-parent="#accordion" href={'#collapse' + val.id} aria-expanded="true" aria-controls="collapseOne">
                                                                        <i className="more-less glyphicon glyphicon-plus"></i>
                                                                        <ul>
                                                                            <li>{val.title}</li>
                                                                            <li>{val.startDate} {(val.endDate) && val.endDate != '0/00/0000' ? 'thru ' + val.endDate : ''}</li>
                                                                        </ul>
                                                                    </span>

                                                                </Panel.Title>
                                                            </Panel.Heading>
                                                            <Panel.Body collapsible>

                                                                <div id={'collapse' + val.id} className="panel-collapse collapse" role="tabpanel" aria-labelledby={'accordion' + val.id}>
                                                                    <MembersAvailabilityCalendarList week={(val.first_week) ? JSON.parse(val.first_week) : []} />
                                                                    <MembersAvailabilityCalendarList week={(val.second_week) ? JSON.parse(val.second_week) : []} />


                                                                    <div className="text-right">
                                                                        <div className="row">
                                                                            <div className="col-md-3 P_20_LR P_7_TB text-left">


                                                                                <span style={divStyle} className="ml-1 ho" onClick={() => this.editAvailabiltyPopUp(val)}><i className="icon icon-update upda_Icon"></i></span>

                                                                            </div>

                                                                            <div className="col-md-6 P_20_LR P_7_TB text-center">
                                                                                <span>
                                                                                    <input type="checkbox" className="checkbox1" readOnly={true} checked={(val.flexible_availability == 1) ? true : false} value={val.flexible_availability} />
                                                                                    <label htmlFor="c1"><span></span>Flexible Availability</label>
                                                                                </span>

                                                                                <span>
                                                                                    <input type="checkbox" className="checkbox1" readOnly={true} checked={(val.flexible_km > 0) ? true : false} value={val.flexible_km} />

                                                                                    <label htmlFor="c2"><span></span>{(val.flexible_km) && val.flexible_km > 0 ? 'Flexible Travel (' + (val.flexible_km) + 'KMs)' : 'Flexible Travel'}</label>

                                                                                </span>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </Panel.Body>
                                                        </Panel>
                                                    ))}  </span> : <div className="no_record py-2">No record found.</div>
                                                }
                                            </PanelGroup>
                                        </div>
                                        <div className="pull-right P_30_T">
                                            <button className="button_plus__" onClick={() => this.openAvailability()}><i className="icon icon-add-icons Add-2-1"></i></button>
                                        </div>

                                        <MembersAvailabilityModal
                                            updateData={this.state.updateData}
                                            is_update={this.state.is_update}
                                            isModalShow={this.state.openModal}
                                            closePopup={this.closeAvailabiltyPopUp}
                                            firstName={this.props.firstName}
                                            full_name={this.props.full_name}
                                            memberId={this.props.props.match.params.id}
                                            getMemberAvailability={this.getMemberAvailability}
                                        />
                                    </div>

                                    <div className="col-lg-3 col-md-3 key_color">
                                        <h3 className="P_7_TB">Key</h3>
                                        <div className="bor_T P_7_TB"></div>
                                        <ul>
                                            <li className="green_dark"><small></small>Avaliable for all</li>
                                            <li className="red_dark"><small></small>A/O (10pm - 6am)</li>
                                            <li className="yello_dark"><small></small>S/O (10pm - 6am, non-active)</li>
                                            <li className="green_regular"><small></small>PM (12pm - 10pm)</li>
                                            <li className="green_very_light"><small></small>AM (6am - 12pm)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" className={this.props.showTypePage=='preferences' ? "tab-pane active" :"tab-pane"} id="Barry_preferences">
                                <MembersPreference preferenceData={this.state.pref} firstName={this.props.firstName} full_name={this.props.full_name} getMemberPreference={this.getMemberPreference} memberId={this.props.props.match.params.id} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


//
class MembersPreference extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activity: [],
            places: [],
            preference: [],
            modal: false
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({ preferenceData: newProps });

    }

    closeModal = () => {
        this.setState({ modal: false });
    }
    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-lg-12 col-md-12">
                    <div className="bor_T"></div>
                        <div className="P_7_TB"><h3><b>
                        {/* {this.props.firstName}  */}
                        Prefers</b></h3></div>
                        <div className="bor_T"></div>
                    </div>
                </div>
                <div className="px-3 mt-5">
                    <div className="row f_color_size align_top_to_1">
                        <div className="col-md-3 f align_e_2 col-xs-3">Places (Favourite): </div>
                        <div className="col-md-9 f align_e_1 col-xs-9">{(this.props.preferenceData.placeFav) ? <span>{this.props.preferenceData.placeFav}</span> : 'N/A'}</div>
                    </div>

                    <div className="row f_color_size align_top_to_1">
                        <div className="col-md-3 f align_e_2 col-xs-3">Places (Least Favourite): </div>
                        <div className="col-md-9 f align_e_1 col-xs-9">{(this.props.preferenceData.placeLeastFav) ? <span>{this.props.preferenceData.placeLeastFav} </span> : 'N/A'}</div>
                    </div>
                    <DottedLine />

                    <div className="row text-right mt-2">
                        <a><i className="icon icon-update update_button" onClick={() => this.setState({ modal: true, preference: this.props.preferenceData.places, type: 'places' })}></i></a>
                    </div>

                    <div className="row f_color_size align_top_to_1">
                        <div className="col-md-3 f align_e_2 col-xs-3">Activities (Favourite):</div>
                        <div className="col-md-9 f align_e_1 col-xs-9">{(this.props.preferenceData.activityFav) ? <span>{this.props.preferenceData.activityFav} </span> : 'N/A'}</div>
                    </div>

                    <div className="row f_color_size align_top_to_1">
                        <div className="col-md-3 f align_e_2 col-xs-3">Activities (Least Favourite):</div>
                        <div className="col-md-9 f align_e_1 col-xs-9">{(this.props.preferenceData.activityLeastFav) ? <span>{this.props.preferenceData.activityLeastFav}</span> : 'N/A'}</div>
                    </div>
                    <DottedLine />

                    <div className="row text-right mt-2">
                        <span><i className="icon icon-update update_button ho" onClick={() => this.setState({ modal: true, preference: this.props.preferenceData.activity, type: 'activity' })}></i></span>
                    </div>

                    <MemberPrefersEditPopUp type={this.state.type} getPreferers={this.props.getMemberPreference} closeModal={this.closeModal} preference={this.state.preference} modal={this.state.modal} memberId={this.props.memberId} full_name={this.props.full_name} />
                </div>
            </div>
        );
    }
}

//
class MembersAvailabilityModal extends React.Component {
    constructor(props) {
        super(props);
        this.initialPopupState = {
            flexible_availability: false,
            flexible_travel: false,
            first_step: true,
            second_step: false,
            third__step: false,
            fourth_step: false,
            extra_travel_popup: false,
            start_date: moment(),
            first_week: '',
            second_week: '',
            open_second_week: false,
            classBtn: 'add_i_icon',
            classIcon: 'icon icon-add-icons',
            availability_exist: false,
            //previous_shift:[]
        };

        this.state = this.initialPopupState;
    }

    componentWillReceiveProps(newProps) {
        if (newProps.updateData) {
            if (newProps.updateData.first_week) {
                this.setState({ first_week: JSON.parse(newProps.updateData.first_week) });
                this.setState({ second_week: (newProps.updateData.second_week) ? JSON.parse(newProps.updateData.second_week) : {} });
                this.setState({ title: newProps.updateData.title });
                this.setState({ flexible_km: newProps.updateData.flexible_km });
                this.setState({ flexible_availability: newProps.updateData.flexible_availability });
                this.setState({ travel_km: newProps.updateData.travel_km });
                this.setState({ flexible_km: newProps.updateData.flexible_km });
                this.setState({ is_default: newProps.updateData.is_default });
                this.setState({ id: newProps.updateData.id });
                this.setState({ status: newProps.updateData.status });
                this.setState({ end_date: (newProps.updateData.end_date == '0000-00-00 00:00:00') ? "" : newProps.updateData.end_date });
            }
        }
    }

    componentDidMount() { }

    //check state is selected or not
    checkSingleShiftIsSelected = () => {
        var x = this.arrayColumn(this.state.first_week)
        var y = this.arrayColumn(this.state.second_week)

        if (!x || !y) {
            this.setState({ errorDisplay: false });
            return true;
        } else {
            this.setState({ errorDisplay: true });
            return false;
        }
        return true;
    }

    arrayColumn = (givenState) => {
        var initalState = true;

        if (givenState) {
            for (var key in givenState) {
                if (givenState.hasOwnProperty(key)) {
                    var innerKey = givenState[key];

                    for (var i = innerKey.length - 1; i >= 0; i--) {
                        Object.keys(innerKey[i]).forEach(function (keyNew) {
                            if (innerKey[i][keyNew]) {
                                initalState = false;
                            }
                            else {
                                if (initalState) {
                                    initalState = true;
                                }
                            }
                        });
                    }
                }
            }
        } else {
            initalState = true;
        }
        return initalState;
    }


    hideShow = (hideDiv, ShowDiv) => {
        jQuery("#availability_form").validate({ /* */ });
        if (jQuery("#availability_form").valid() && this.checkSingleShiftIsSelected()) {
            var state = {};
            state[ShowDiv] = true;
            state[hideDiv] = false;
            this.setState(state);
        }
    }

    /* checkboxResolveCollapse = (id, value, selectAll) => 
     {
         var List = this.state.previous_shift;
           
         if(selectAll){
             List.map((shift, id) => {
                 List[id]['status'] = value;
             })
             this.setState({collapseSelect: value})
         }else{
             List[id]['status'] = value;
             this.setState({collapseSelect: false})
         }
         
         this.setState({previous_shift: List});
     }*/

    handleChange = (e) => {
        var state = {};
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }

    flexiableHandleChange = (e) => {
        var state = {};
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        state['open_flexible'] = true;
        this.setState(state);
    }

    selectChange(selectedOption, fieldname) {
        var state = {};
        state[fieldname] = selectedOption;
        this.setState(state);
    }

    //Get calendar selected data 
    getAllSelectedState = (selectedState) => {
        this.setState({ selectedState: selectedState });
    }

    getWeekData = (key, data) => {
        var state = {};
        state[key] = data;
        this.setState(state);
    }

    onSubmit(e) {
        e.preventDefault();
        jQuery("#availability_form").validate({ /* */ });
        if (jQuery("#availability_form").valid()) {
            this.setState({ loading: true });

            postData('member/MemberDashboard/save_member_availability', { data: this.state, 'memberId': this.props.memberId, is_update: this.props.is_update, full_name: this.props.full_name }).then((result) => {
                if (result.status) {
                     toast.success(<ToastUndo message={result.response_msg} showType={'s'} />, {
                    // toast.success(result.response_msg, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    jQuery('#AmyModal').modal('hide');
                    this.setState(this.initialPopupState);
                    this.setState({ title: '', travel_km: '', is_default: '', end_date: '', selectedState: '' });
                    this.props.closePopup();
                    this.clearState();
                    this.props.getMemberAvailability();
                    setTimeout(() => this.setState({ success: true, loading: false }), 1000);
                } else {
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                    // toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    this.setState({ loading: false });
                }
            });
        }
    }

    clearState = () => {
        jQuery('#AmyModal').modal('hide');
        this.setState({
            first_step: true,
            second_step: false,
            third__step: false,
            fourth_step: false,
            fifth_step: false,
            flexible_availability: false,
            errorDisplay: false,
            is_default: '',
            end_date: '',
            title: '',
            availability_exist: false,
            first_week: {
                all: [{ Mon: false }, { Tue: false }, { Wed: false }, { Thu: false }, { Fri: false }, { Sat: false }, { Sun: false }],
                ao: [{ Mon: false }, { Tue: false }, { Wed: false }, { Thu: false }, { Fri: false }, { Sat: false }, { Sun: false }],
                so: [{ Mon: false }, { Tue: false }, { Wed: false }, { Thu: false }, { Fri: false }, { Sat: false }, { Sun: false }],
                pm: [{ Mon: false }, { Tue: false }, { Wed: false }, { Thu: false }, { Fri: false }, { Sat: false }, { Sun: false }],
                am: [{ Mon: false }, { Tue: false }, { Wed: false }, { Thu: false }, { Fri: false }, { Sat: false }, { Sun: false }],
            },
            second_week: {
                all: [{ Mon: false }, { Tue: false }, { Wed: false }, { Thu: false }, { Fri: false }, { Sat: false }, { Sun: false }],
                ao: [{ Mon: false }, { Tue: false }, { Wed: false }, { Thu: false }, { Fri: false }, { Sat: false }, { Sun: false }],
                so: [{ Mon: false }, { Tue: false }, { Wed: false }, { Thu: false }, { Fri: false }, { Sat: false }, { Sun: false }],
                pm: [{ Mon: false }, { Tue: false }, { Wed: false }, { Thu: false }, { Fri: false }, { Sat: false }, { Sun: false }],
                am: [{ Mon: false }, { Tue: false }, { Wed: false }, { Thu: false }, { Fri: false }, { Sat: false }, { Sun: false }],
            },
        })

    }

    openNextWeek = () => {
        if (this.state.open_second_week) {
            this.setState({ open_second_week: false, classBtn: 'add_i_icon', classIcon: 'icon icon-add-icons' });
        }
        else {
            this.setState({ open_second_week: true, classBtn: 'button_unadd', classIcon: 'icon icon-decrease-icon icon_cancel_1' });
        }
    }

    render() {
        return (
            <div>
                <div className="modal fade Modal_A Modal_B" id="AmyModal">
                    {/* <div className="modal-dialog"> */}
                    <div className={'modal-dialog ' + ((this.state.first_step) ? 'modal_size_in' : '')}>
                        <div className="modal-content">
                            <div className="modal-body">


                                <form id="availability_form" method="post" autoComplete="false">
                                    <div className="">
                                        <div className="text text-left">{this.props.firstName} Availability
                                    <span type="button" className="close_i pull-right mt-1 ho" onClick={() => this.clearState()}><i className="icon icon-cross-icons"></i></span>
                                        </div>

                                        <div id="first_step" style={(this.state.first_step) ? { 'display': 'block' } : { 'display': 'none' }}>
                                            <div className="row">
                                                <div className="col-md-12  mt-4">
                                                    <div className="row w-90 avalibilty_div">
                                                        <div className="col-lg-12 col-md-12 px-0">

                                                            <MembersAvailabilityCalendar is_update={this.props.is_update} random={Math.random()} week_data={this.state.first_week} getAllSelectedState={this.getAllSelectedState} week_key={'first_week'} getWeekData={this.getWeekData} />


                                                            {((this.state.open_second_week) || (this.props.is_update)) ?
                                                                <MembersAvailabilityCalendar is_update={this.props.is_update} random={Math.random()} week_data={this.state.second_week} getAllSelectedState={this.getAllSelectedState} week_key={'second_week'} getWeekData={this.getWeekData} /> : ''}

                                                        </div>

                                                        <div className="col-lg-12 col-md-12 P_10_TB P_15_T px-0">

                                                            <span>
                                                                <input type="checkbox" className="checkbox_flex" onClick={this.handleChange} id="flexible_availability" name="flexible_availability" value={this.state.flexible_availability || ''} checked={(this.state.flexible_availability > 0) ? true : false} />
                                                                <label htmlFor="flexible_availability" >
                                                                    <span onClick={this.handleChange} ></span>
                                                                    <small onClick={this.handleChange} >Flexible Availability</small></label>
                                                            </span>

                                                            <span  >
                                                                <input type="checkbox" className="checkbox_flex" id="extra_travel_popup" name="extra_travel_popup" value={this.state.extra_travel_popup || ''} checked={this.state.extra_travel_popup || ((this.state.flexible_km > 0) ? true : false) || ''} />
                                                                <label onClick={this.flexiableHandleChange} htmlFor="extra_travel_popup"><span onClick={this.flexiableHandleChange}></span><small>Flexible Travels (KMs)</small></label>
                                                            </span>

                                                            <div className="pull-right icons_NEW">
                                                                <button className={this.state.classBtn} onClick={() => this.openNextWeek()}><i className={this.state.classIcon}></i></button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-4 col-md-offset-4 P_20_TB">
                                                    <input className="but_submit" type="button" value="Save and Continue" onClick={(e) => this.hideShow('first_step', 'second_step')} />
                                                </div>
                                                <div className="col-md-12">
                                                    <p className="error_login text-center" style={(this.state.errorDisplay) ? { 'display': 'block' } : { 'display': 'none' }}>Please select atleast one day availability to Continue.</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="second_step" style={(this.state.second_step) ? { 'display': 'block' } : { 'display': 'none' }}>
                                            <h4 className="P_20_T">How far is this Member willing to travel to each day?</h4>
                                            <div className="row mt-4">
                                                <div className="col-lg-4 col-md-4">
                                                    <label>Travel Distance</label>
                                                    <span className="required">
                                                        <Select name="memberAboutTravelDistance" required={true} simpleValue={true} searchable={false} clearable={false} options={memberAboutTravelDistance()} placeholder="Travel Distance" value={this.state.travel_km || 10} onChange={(e) => this.setState({ 'travel_km': e })} />
                                                    </span>
                                                </div>
                                                <div className="col-md-4 col-md-offset-8">
                                                    <input className="but_submit" type="button" value="Save and Continue" onClick={(e) => this.hideShow('second_step', 'third__step')} />
                                                </div>
                                            </div>
                                        </div>

                                        <div id="third__step" style={(this.state.third__step) ? { 'display': 'block' } : { 'display': 'none' }}>
                                            <h4 className="P_20_T">Will this be the DEFAULT availability for this member?</h4>
                                            <div className="row py-4">
                                                <div className="col-md-4">
                                                    <Select className="default_validation" name="is_default" required={true} simpleValue={true} searchable={true} clearable={false} options={memberYesNo()} placeholder="DEFAULT availability" value={this.state.is_default} onChange={(e) => this.setState({ 'is_default': e }, () => { })} />
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-3">
                                                    <label>Start Date:</label>
                                                    <span className="required">
                                                        <DatePicker className="text-center" selected={this.state.start_date ? moment(this.state.start_date, 'DD-MM-YYYY') : null} dateFormat='DD-MM-YYYY' name="start_date" onChange={(e) => this.selectChange(e, 'start_date')} required minDate={moment()} autoComplete="off" />
                                                    </span></div>

                                                <div className="col-md-3">
                                                    <div id="end_date" style={(this.state.is_default) && this.state.is_default == 2 ? { 'display': 'block' } : { 'display': 'none' }}>
                                                        <label>End Date: </label>
                                                        <span className="required">
                                                            <DatePicker className="text-center" selected={(this.state.end_date) ? moment(this.state.end_date, 'DD-MM-YYYY') : null} name="end_date" onChange={(e) => this.selectChange(e, 'end_date')} required minDate={moment()} dateFormat='DD-MM-YYYY' minDate={this.state.start_date} autoComplete="off" />
                                                        </span></div>
                                                </div>

                                            </div>

                                            {(this.state.is_default && this.state.is_default == 1) ?
                                                <span><small><i>*Please note, it will override your default availability. </i></small></span>
                                                : ''}

                                            <div className="col-md-4 col-md-offset-8">


                                                {(this.state.availability_exist) ? <span><small><i>*This availability is already exist.Please change date to continue. </i></small></span> : ''}

                                                <input className="but_submit" type="button" value="Save and Continue" onClick={(e) => {
                                                    if (this.state.is_default && this.state.is_default == 2) {
                                                        if (this.state.end_date != undefined) {
                                                            this.setState({ member_id: this.props.memberId }, () => {
                                                                var requestData = this.state;
                                                                postData('member/MemberDashboard/check_availibility_already_exist', requestData).then((result) => {
                                                                    if (result.rows_count > 0) {
                                                                        this.setState({ availability_exist: true });
                                                                    }
                                                                    else {
                                                                        this.hideShow('third__step', 'fifth_step');
                                                                    }
                                                                });
                                                            });
                                                        }
                                                    }
                                                    else {
                                                        this.hideShow('third__step', 'fifth_step');
                                                    }
                                                }}
                                                />
                                            </div>
                                        </div>

                                        <div id="fifth_step" style={(this.state.fifth_step) ? { 'display': 'block' } : { 'display': 'none' }}>
                                            <h4 className="P_20_T">Please give this availability a title.</h4>
                                            <div className="row P_15_T">
                                                <div className="col-md-7">
                                                    <label>Title: </label>
                                                    <span className="required"><input type="text" name="title" onChange={(e) => this.setState({ 'title': e.target.value })} value={(this.state.title) ? this.state.title : ''} data-rule-required="true" /></span>
                                                </div>
                                                <div className="col-md-4 col-md-offset-8">
                                                    <input type="submit" className="but" value={'Save'} name="content" onClick={(e) => this.onSubmit(e)} disabled={this.state.loading} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <Modal className="modal fade Modal_A Modal_B" show={this.state.open_flexible} onHide={this.handleHide} container={this} aria-labelledby="contained-modal-title" >
                    <Modal.Body>
                        <div className="dis_cell">
                            <div className="text text-left">{this.props.firstName} Availability
                        <span type="button" className="close_i pull-right mt-1 ho" onClick={(e) => this.setState({ extra_travel_popup: false, open_flexible: false, flexible_km: false, second_step: false })}><i className="icon icon-cross-icons"></i></span>
                            </div>

                            <div>
                                <h4 className="P_20_TB">How far is this Member willing to travel to each day?</h4>
                                <div className="row">
                                    <div className="col-lg-4 col-md-4">
                                        <label>Travel Distance </label>
                                        <span className="required">
                                            <Select name="flexible_km" required={true} simpleValue={true} searchable={false} clearable={false} options={memberAboutTravelDistance()} placeholder="Travel Distance" value={this.state.flexible_km || '10km or <'} onChange={(e) => this.setState({ 'flexible_km': e })} />
                                        </span>
                                    </div>
                                    <div className="col-md-4 col-md-offset-8">

                                        <input className="but_submit" type="button" value="Save" onClick={(e) => this.setState({ open_flexible: false })} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}

//
class MembersAvailabilityCalendar extends React.Component {
    constructor(props) {
        super(props);

        //this.getDay
        var dayAry = [{ Mon: false }, { Tue: false }, { Wed: false }, { Thu: false }, { Fri: false }, { Sat: false }, { Sun: false }];
        this.state = {
            all: [{ Mon: false }, { Tue: false }, { Wed: false }, { Thu: false }, { Fri: false }, { Sat: false }, { Sun: false }],
            ao: [{ Mon: false }, { Tue: false }, { Wed: false }, { Thu: false }, { Fri: false }, { Sat: false }, { Sun: false }],
            so: [{ Mon: false }, { Tue: false }, { Wed: false }, { Thu: false }, { Fri: false }, { Sat: false }, { Sun: false }],
            pm: [{ Mon: false }, { Tue: false }, { Wed: false }, { Thu: false }, { Fri: false }, { Sat: false }, { Sun: false }],
            am: [{ Mon: false }, { Tue: false }, { Wed: false }, { Thu: false }, { Fri: false }, { Sat: false }, { Sun: false }],
        };
    }

    setSchedule = (value, idx, key) => {
        var stateCol = this.state;
        var stateAry = {};
        var tempField = {};
        var List = this.state[key];

        if (!this.state['all'][idx][this.getDay(idx)] || key == 'all') {
            // after select all selected
            if (key == 'all') {
                stateCol['ao'][idx][this.getDay(idx)] = false;
                stateCol['so'][idx][this.getDay(idx)] = false;
                stateCol['pm'][idx][this.getDay(idx)] = false;
                stateCol['am'][idx][this.getDay(idx)] = false;
                this.setState(stateCol);
            }

            //single select un-select
            if (List[idx][this.getDay(idx)] == true) {
                List[idx][this.getDay(idx)] = false;
            }
            else {
                List[idx][this.getDay(idx)] = true;
            }
        }

        stateAry[key] = List;
        this.setState(stateAry);
        this.props.getAllSelectedState(stateAry);
        this.props.getWeekData(this.props.week_key, this.state);
    }

    getDay = (myVal) => {
        var myDayAry = { 0: 'Mon', 1: 'Tue', 2: 'Wed', 3: 'Thu', 4: 'Fri', 5: 'Sat', 6: 'Sun' };
        return myDayAry[myVal];
    }

    componentWillReceiveProps(newProps) {
        if (newProps.week_data) {
            this.setState(newProps.week_data);
        }
    }

    render() {
        return (
            <div>
                <table className="calendar_me">
                    <tbody>
                        <tr>
                            {this.state.all.map((value, idx) => (
                                Object.keys(value).map(key =>
                                    <th key={idx} onClick={(e) => this.setSchedule(value, idx, 'all')} className={(this.state.all[idx][key]) ? 'avaliable' : ''}>{key}</th>
                                )
                            ))}
                        </tr>

                        <tr>
                            {this.state.ao.map((value, idx) => (
                                <td key={idx} onClick={(e) => this.setSchedule(value, idx, 'ao')} className={((this.state.all[idx][this.getDay(idx)]) ? "avaliable" : (this.state.ao[idx][this.getDay(idx)]) ? 'unavaliable' : '')}></td>
                            ))}
                        </tr>

                        <tr>
                            {this.state.so.map((value, idx) => (
                                <td key={idx} onClick={(e) => this.setSchedule(value, idx, 'so')} className={((this.state.all[idx][this.getDay(idx)]) ? "avaliable" : (this.state.so[idx][this.getDay(idx)]) ? 'DarkGolden' : '')}></td>
                            ))}
                        </tr>

                        <tr>
                            {this.state.pm.map((value, idx) => (
                                <td key={idx} onClick={(e) => this.setSchedule(value, idx, 'pm')} className={((this.state.all[idx][this.getDay(idx)]) ? "avaliable" : (this.state.pm[idx][this.getDay(idx)]) ? 'pm_green_regular' : '')}></td>
                            ))}
                        </tr>

                        <tr>
                            {this.state.am.map((value, idx) => (
                                <td key={idx} onClick={(e) => this.setSchedule(value, idx, 'am')} className={((this.state.all[idx][this.getDay(idx)]) ? "avaliable" : (this.state.am[idx][this.getDay(idx)]) ? 'DarkOliveGreen' : '')}></td>
                            ))}
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

//
class MembersAvailabilityCalendarList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            all: [],
            ao: [],
            so: [],
            pm: [],
            am: [],
        }
    }

    componentDidMount() {
        if (this.props.week) {
            this.setState(this.props.week);
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps.week) {
            this.setState(newProps.week);
        }
    }
    reinitialiseVar = (objecy_ary) => {
        return Object.keys(objecy_ary).map(function (key2) {
            return (<th key={key2} className={(objecy_ary[key2]) ? 'avaliable' : ''}>{key2}</th>);
        });
    }
    //
    reinitialiseVarTd = (objecy_ary, className) => {
        var days = { Mon: 0, Tue: 1, Wed: 2, Thu: 3, Fri: 4, Sat: 5, Sun: 6 };
        var allData = this.state.all;
        return Object.keys(objecy_ary).map(function (key2) {
            var xx = days[key2];
            return (<td key={key2} className={(allData[xx][key2]) ? 'avaliable' : ((objecy_ary[key2]) ? className : '')}>&nbsp;</td>);
        });
    }

    render() {
        return (
            <div className="calendar_repeat">
                <table className="calendar_me">
                    <tbody>
                        <tr>
                            {this.state.all.map((val, aval_id) => (
                                this.reinitialiseVar(val)
                            ))}
                        </tr>

                        <tr>
                            {this.state.ao.map((vall, aval_id) => (
                                this.reinitialiseVarTd(vall, 'unavaliable')
                            ))}
                        </tr>

                        <tr>
                            {this.state.so.map((val, aval_id) => (
                                this.reinitialiseVarTd(val, 'DarkGolden')
                            ))}
                        </tr>

                        <tr>
                            {this.state.pm.map((val, aval_id) => (
                                this.reinitialiseVarTd(val, 'pm_green_regular')
                            ))}
                        </tr>

                        <tr>
                            {this.state.am.map((val, aval_id) => (
                                this.reinitialiseVarTd(val, 'DarkOliveGreen')
                            ))}
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

//
class MemberPrefersEditPopUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            preference: [],
            full_name: this.props.full_name,
            submit_form: true,
        }
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.setState({ submit_form: false });
        postData('member/MemberDashboard/update_member_preference', this.state).then((result) => {
            if (result.status) {
                toast.success(<ToastUndo message={'Preferences updated successfully'} showType={'s'} />, {
                // toast.success("Preferences updated successfully", {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
                this.props.closeModal();
                this.props.getPreferers(this.props.memberId);
                this.setState({ submit_form: true });
            } else {
                toast.error(<ToastUndo message={'Api error'} showType={'e'} />, {
                // toast.error('Api error', {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
            }
        });
    }

    handleChange = (e) => {
        var state = {};
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }
    componentWillReceiveProps(newProps) {
        this.setState(newProps);
    }

    setRadio = (e, idx, tagType, value) => {
        var List = this.state[tagType];
        List[idx].type = value;

        var state = {};
        state[tagType] = List;
        this.setState(state);
    }

    render() {
        return (
            <div>
                <Modal
                    className="modal fade Modal_A Modal_B preferences_modal"
                    show={this.props.modal}
                    onHide={this.handleHide}
                    container={this}
                    aria-labelledby="contained-modal-title"
                >
                    <Modal.Body>
                        <div className="row">
                            <form onSubmit={this.onSubmit}>
                                <div className="col-md-12 multiple_checkbox_tooltip">
                                    <h2 className="color mb-4">{(this.props.type == 'activity') ? 'Update Your Activities' : 'Update Your Places'}: <span className="close_i pull-right mt-1 ho" onClick={this.props.closeModal}><i className="icon icon-cross-icons"></i></span></h2>
                                    <div className="Schedules_Multiple_checkbox h-45 overflow-hidden">
                                        <div className="scroll_active_modal px-0 py-0">
                                            {this.state.preference.map((req, idx) => (
                                                <span className="d-flex s-w-d_div" key={idx + 1}>
                                                    <h4 className="font_w_4"> {req.label}</h4>
                                                    <div className="cartoon_div">

                                                        <div className="d-inline danger-cartoon">
                                                            <input type="radio" className="radio_input" onChange={(e) => this.setRadio(e, idx, 'preference', 'none')} name={req.label} checked={(req.type == 'none') ? true : false} />
                                                            <label onClick={(e) => this.setRadio(e, idx, 'preference', 'none')} ><span><span></span></span></label>
                                                        </div>

                                                        <div className="d-inline worning-cartoon">
                                                            <input type="radio" className="radio_input" onChange={(e) => this.setRadio(e, idx, 'preference', '2')} name={req.label} checked={(req.type == '2') ? true : false} />
                                                            <label onClick={(e) => this.setRadio(e, idx, 'preference', '2')} ><span><span></span></span></label>
                                                        </div>

                                                        <div className="d-inline pr-3  success-cartoon">
                                                            <input type="radio" className="radio_input" onChange={(e) => this.setRadio(e, idx, 'preference', '1')} name={req.label} checked={(req.type == '1') ? true : false} />
                                                            <label onClick={(e) => this.setRadio(e, idx, 'preference', '1')} ><span><span></span></span></label>
                                                        </div>

                                                    </div>
                                                </span>
                                            ))}
                                        </div>
                                    </div>

                                </div>
                                <div className="col-md-4 col-md-offset-4 mt-3">
                                    <button type="submit" className="default_but_remove button but" disabled={(this.state.submit_form) ? false : true}>Submit</button>
                                </div>
                            </form>
                        </div>

                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}



const mapStateToProps = state => ({
    MemberProfile: state.MemberReducer.memberProfile,
    ActiveClass: state.MemberReducer.ActiveClassProfilePage,
    showTypePage: state.MemberReducer.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
        changeMemberNavigationClass: (value) => dispach(setMemberActiveClass(value)),
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(MembersAbout)