import React from 'react';
import { ROUTER_PATH, PAGINATION_SHOW } from '../../../config.js';
import MemberNavigation from './MemberNavigation';
import Select from 'react-select-plus';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import { checkItsNotLoggedIn, postData, checkPinVerified } from '../../../service/common.js';
import { ViewByFms } from '../../../dropdown/memberdropdown.js';
import { TotalShowOnTable } from '../../../service/TotalShowOnTable';
import MemberProfile from './MemberProfile';
import Pagination from "../../../service/Pagination.js";

const requestData = (pageSize, page, sorted, filtered, memberId) => {
    return new Promise((resolve, reject) => {
        // request json
        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered, memberId: memberId });
        postData('member/MemberDashboard/get_member_fms', Request).then((result) => {
            let filteredData = result.data;

            const res = {
                rows: filteredData,
                pages: (result.count),
                all_count: result.all_count,
                total_duration: result.total_duration
            };
            resolve(res);
        });

    });
};

class MembersFms extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            caseListing: [],
            status: '2',//'0,1',
            filtered: {status:'2'}
        };
        
        checkItsNotLoggedIn(ROUTER_PATH);
        checkPinVerified('fms');
        this.urlParam = this.props.props.match.params.id;
    }

    componentDidMount() { }

    fetchData = (state, instance) => {
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered,
            this.urlParam
        ).then(res => {
            this.setState({
                caseListing: res.rows,
                pages: res.pages,
                all_count: res.all_count,
                loading: false,
                total_duration: res.total_duration
            });
        })
    }

    render() {

        const columns = [
            { 
                Header: 'Case ID', accessor: 'id', filterable: false, maxWidth: 80,
                headerClassName: 'Th_class_d1',
                className: (this.state.activeCol === 'Case ID') && this.state.resizing ? 'borderCellCls' : '_align_c__',
                Cell: props => <span>
                    {props.original.id}</span>
            },
            {
                Header: 'Timer', accessor: 'timelapse', filterable: false, sortable: false,
                headerClassName: 'Th_class_d1',
                className: (this.state.activeCol === 'Timer') && this.state.resizing ? 'borderCellCls' : '_align_c__',
                Cell: props => <span>
                    {props.original.timelapse}</span>
            },
            {
                Header: 'Category', accessor: 'category', filterable: false, sortable: false,
                headerClassName: 'Th_class_d1',
                className: (this.state.activeCol === 'Category') && this.state.resizing ? 'borderCellCls' : '_align_c__',
                Cell: props => <span>
                    {props.original.category}</span>
            },
            {
                Header: 'Date of Event ', accessor: 'event_date', filterable: false,
                headerClassName: 'Th_class_d1',
                className: (this.state.activeCol === 'Date of Event') && this.state.resizing ? 'borderCellCls' : '_align_c__',
                Cell: props => <span>
                    {props.original.event_date}</span>
            },
            {
                Header: 'Initiated By ', accessor: 'initiated_by', sortable: false,
                headerClassName: 'Th_class_d1',
                className: (this.state.activeCol === 'Initiated By') && this.state.resizing ? 'borderCellCls' : '_align_c__',
                Cell: props => <span>
                    {props.original.initiated_by}</span>
            },
            {
                Header: 'Reason', accessor: 'description', sortable: false,
                headerClassName: 'Th_class_d1',
                className: (this.state.activeCol === 'Against To') && this.state.resizing ? 'borderCellCls' : '_align_c__',
                Cell: props => <span>
                    {props.original.description}</span>
            },
            {
                Cell: (props) => <span><a href={'/admin/fms/case/' + props.original.id}><i className="icon icon-views"></i></a></span>, 
                Header: <TotalShowOnTable countData={this.state.all_count} />,
                style: {
                    "textAlign": "right",
                }, headerStyle: { border: "0px solid #fff" }, sortable: false
            },
        ]

        return (
            <div>


               
                    <MemberProfile MemberData={this.state} ProfileId={this.props.props.match.params.id} Active={'fms'} pageTypeParms="fms" />
                    <div className="row">
                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                        <div className="row mt-3">
                            <div className="col-md-9">
                                <div className="bor_T"></div>
                                <h3 className="P_7_TB"><b>FMS Cases Relating to {this.props.firstName}
                                </b></h3></div>

                            <div className="col-lg-3 col-sm-3">
                                <div className="box">
                                    <Select name="status" required={true} simpleValue={true} searchable={false} clearable={false}
                                        options={ViewByFms()} placeholder="View By" value={(this.state.filtered.status) ? this.state.filtered.status : ''} onChange={(e) => this.setState({ filtered: { status: e } }, () => console.log(this.state.filtered.status))} />
                                </div>
                            </div>

                            <div className="col-sm-9 P_25_b"><div className="bor_T"></div></div>
                        </div>

                        <div className="row">
                            <div className="col-md-12 schedule_listings  th_txt_center__ px_text__">
                                <ReactTable
                                 PaginationComponent={Pagination}
                                    columns={columns}
                                    manual
                                    data={this.state.caseListing}
                                    pages={this.state.pages}
                                    loading={this.state.loading}
                                    onFetchData={this.fetchData}
                                    filtered={this.state.filtered}
                                    defaultFiltered={{ status: '2' }}//'0,1'
                                    defaultPageSize={10}
                                    className="-striped -highlight"
                                    noDataText="No Record Found"
                                    minRows={2}
                                    previousText = {<span className="icon icon-arrow-left privious"></span>}
                                   nextText = {<span className="icon icon-arrow-right next"></span>}
                                    showPagination={this.state.caseListing.length > PAGINATION_SHOW ? true : false}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                        <div className="row">
                            <div className="col-lg-3 col-sm-3 pull-right">
                                <a href="/admin/fms/create_case" className="but">Initiate FMS Case</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}
export default MembersFms;