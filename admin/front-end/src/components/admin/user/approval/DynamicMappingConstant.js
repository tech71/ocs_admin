 import {CommonDisplay, PreferPlaces, AddressUpdate, PreferContact, ParticipantPreferredLanguage, ParticipantLinguisticInterpreter, ParticipantHearingInterpreter, AddNewGaolParticipant, ParticipantKinUpdate, ParticipantBookerUpdate, ParticipantUpdateGoal, ParticipantArchiveGoal, CreateParticipantShift} from './ExternalSubConstant';
 
 export const DynamicComponet = {
        PreferPlaces: PreferPlaces,
        CommonDisplay: CommonDisplay,
        AddressUpdate: AddressUpdate,
        PreferContact: PreferContact,
        ParticipantPreferredLanguage: ParticipantPreferredLanguage,
        ParticipantLinguisticInterpreter: ParticipantLinguisticInterpreter,
        ParticipantHearingInterpreter: ParticipantHearingInterpreter,
        AddNewGaolParticipant: AddNewGaolParticipant,
        ParticipantKinUpdate: ParticipantKinUpdate,
        ParticipantBookerUpdate: ParticipantBookerUpdate,
        ParticipantUpdateGoal: ParticipantUpdateGoal,
        ParticipantArchiveGoal: ParticipantArchiveGoal,
        CreateParticipantShift: CreateParticipantShift,
      
 };