import React from 'react';
import { interpretertDropdown, prefterLanguageDropdown,HearingOption,  preferContactDropdown, sitCategoryListDropdown } from '../../../../dropdown/ParticipantDropdown.js';
import moment from 'moment';

export const CommonDisplay = (props) => {
    return <div className="other_conter">

        <div className="col-sm-12">

            <ul className='inl_ulAp'>
                <li><span className="color">From:</span> {props.props.previous}</li>
                <li> <span className="color"> To:</span> {props.props.new}</li>
            </ul>
           
        </div>
        
        <div className="col-sm-12 text-right apr_col">
            <i onClick={(e) => props.action(e, props.index, false)} className={"icon icon-cross-icons deny_request " + (props.props.approve ? '' : 'active')}></i>
            <i onClick={(e) => props.action(e, props.index, true)} className={"icon icon-accept-approve1-ie approve_request " + (props.props.approve ? 'active' : '')}></i>
        </div>
    </div>
}

export const PreferPlaces = (props) => {
    return <div className="other_conter">
        <div className="col-sm-12">
            <ul className='inl_ulAp'>
                <li><span class="color">From:</span> </li>
                <li><span class="color">Favourite: </span>{props.props.previous.fav}</li>
                <li><span class="color">Least Favourite: </span>{props.props.previous.least_fav}<span></span></li>
            </ul>
            
        </div>

        <div className="col-sm-12 text-left">
            <ul className='inl_ulAp'>
                <li><span class="color">To:</span> </li>
                <li><span class="color">Favourite: </span>{props.props.new.fav}</li>
                <li><span class="color">Least Favourite: </span>{props.props.new.least_fav}<span></span></li>
            </ul>
           
        </div>
        <div className="col-sm-12 text-right apr_col">
            <i onClick={(e) => props.action(e, props.index, false)} className={"icon icon-cross-icons deny_request " + (props.props.approve ? '' : 'active')}></i>
            <i onClick={(e) => props.action(e, props.index, true)} className={"icon icon-accept-approve1-ie approve_request " + (props.props.approve ? 'active' : '')}></i>
        </div>
    </div>
}

export const AddressUpdate = (props) => {
    return <div className="other_conter">
        <div className="col-sm-12">

            <ul className='inl_ulAp'>
                <li>
                    <span className="color">From:</span>
                        {props.props.previous.map((val, index) => (
                        <div key={index+1}>
                            {val.street}, {val.city}, {props.props.stateLabel[val.state]}, {val.postal} {val.site_category? <span>- {sitCategoryListDropdown(val.site_category)}</span>: '' } 
                        </div>
                    ))}
                </li>
                <li> 
                    <span className="color"> To:</span>
                        {props.props.new_address.map((val, index) => (
                        <div key={index+1}>
                            {val.street}, {val.city}, {props.props.stateLabel[val.state]}, {val.postal}  {val.site_category? <span>- {sitCategoryListDropdown(val.site_category)}</span>: '' } 
                        </div>
                    ))}
                </li>
            </ul>
            
        </div>

        <div className="col-sm-12 text-right apr_col">
            <i onClick={(e) => props.action(e, props.index, false)} className={"icon icon-cross-icons deny_request " + (props.props.approve ? '' : 'active')}></i>
            <i onClick={(e) => props.action(e, props.index, true)} className={"icon icon-accept-approve1-ie approve_request " + (props.props.approve ? 'active' : '')}></i>
        </div>
    </div>
}

export const PreferContact = (props) => {
    return <div className="other_conter">
        <div className="col-sm-12">
            <ul className='inl_ulAp'>
                <li> <span className="color">From:</span> {preferContactDropdown(props.props.previous)}</li>
                <li><span className="color"> To:</span> {preferContactDropdown(props.props.new)}</li>    
            </ul>
           
        </div>

       
        <div className="col-sm-12 text-right apr_col">
            <i onClick={(e) => props.action(e, props.index, false)} className={"icon icon-cross-icons deny_request " + (props.props.approve ? '' : 'active')}></i>
            <i onClick={(e) => props.action(e, props.index, true)} className={"icon icon-accept-approve1-ie approve_request " + (props.props.approve ? 'active' : '')}></i>
        </div>
    </div>
}

export const ParticipantPreferredLanguage = (props) => {
    return <div className="other_conter">
        <div className="col-sm-12">
            <ul className='inl_ulAp'>
                <li>  <span className="color">From:</span> {prefterLanguageDropdown(props.props.previous)}</li>
                <li><span className="color"> To:</span> {prefterLanguageDropdown(props.props.new)}</li>    
            </ul>
           
        </div>

   
        <div className="col-sm-12 text-right apr_col">
            <i onClick={(e) => props.action(e, props.index, false)} className={"icon icon-cross-icons deny_request " + (props.props.approve ? '' : 'active')}></i>
            <i onClick={(e) => props.action(e, props.index, true)} className={"icon icon-accept-approve1-ie approve_request " + (props.props.approve ? 'active' : '')}></i>
        </div>
    </div>
}

export const ParticipantLinguisticInterpreter = (props) => {
    return <div className="other_conter">
        <div className="col-sm-12">
            <ul className='inl_ulAp'>
                <li>  <span className="color">From:</span> {interpretertDropdown(props.props.previous)}</li>
                <li><span className="color"> To:</span> {interpretertDropdown(props.props.new)}</li>    
            </ul>
          
        </div>

        
        <div className="col-sm-12 text-right apr_col">
            <i onClick={(e) => props.action(e, props.index, false)} className={"icon icon-cross-icons deny_request " + (props.props.approve ? '' : 'active')}></i>
            <i onClick={(e) => props.action(e, props.index, true)} className={"icon icon-accept-approve1-ie approve_request " + (props.props.approve ? 'active' : '')}></i>
        </div>
    </div>
}

export const ParticipantHearingInterpreter = (props) => {
    return <div className="other_conter">
        <div className="col-sm-12">
            <ul className='inl_ulAp'>
                <li>  <span className="color">From:</span> {HearingOption(props.props.previous)}</li>
                <li><span className="color"> To:</span> {HearingOption(props.props.new)}</li>    
            </ul>
            {/* <span className="color">From:</span> {HearingOption(props.props.previous)} */}
        </div>

        <div className="col-sm-12 text-right apr_col">
            <i onClick={(e) => props.action(e, props.index, false)} className={"icon icon-cross-icons deny_request " + (props.props.approve ? '' : 'active')}></i>
            <i onClick={(e) => props.action(e, props.index, true)} className={"icon icon-accept-approve1-ie approve_request " + (props.props.approve ? 'active' : '')}></i>
        </div>
    </div>
}

export const AddNewGaolParticipant = (props) => {
    return <div className="other_conter">
   
        <div className="col-sm-12"> 
            <ul className='inl_ulAp'>
                <li><span class="color">Goal:</span> {props.props.new.title}</li>
                <li><span class="color">Start Date: </span>{moment(props.props.new.start_date).format("DD/MM/YYYY")}</li>
                <li><span class="color">End Date: </span>{moment(props.props.new.end_date).format("DD/MM/YYYY")}<span></span></li>
            </ul>
             
        </div>

        <div className="col-sm-12 text-right apr_col">
            <i onClick={(e) => props.action(e, props.index, false)} className={"icon icon-cross-icons deny_request " + (props.props.approve ? '' : 'active')}></i>
            <i onClick={(e) => props.action(e, props.index, true)} className={"icon icon-accept-approve1-ie approve_request " + (props.props.approve ? 'active' : '')}></i>
        </div>
    </div>
}

export const ParticipantKinUpdate = (props) => {
    return <div className="other_conter">
   
        <div className="col-sm-12">
            <ul className='inl_ulAp'>
                <li>
                    <span className="color">From:</span>
                    {props.props.previous.map((val, index) => (
                    <div key={index+1}>
                        {val.firstname} {val.lastname} {'('+val.relation+')'} - {val.phone} - {val.email} {'('+ ((val.primary_kin == 1)? 'Primary': 'Secondary' ) +')'}
                    </div>
                    ))}
                </li>
                <li>
                    <span className="color"> To:</span>
                    {props.props.new.map((val, index) => (
                    <div key={index+1}>
                        {val.firstname} {val.lastname} {'('+val.relation+')'} - {val.phone} - {val.email} {'('+ ((val.primary_kin == 1)? 'Primary': 'Secondary' ) +')'}
                    </div>
                    ))}
                </li>
               
            </ul>
            
        </div>

        <div className="col-sm-12 text-left">
            
        </div>

        <div className="col-sm-12 text-right apr_col">
            <i onClick={(e) => props.action(e, props.index, false)} className={"icon icon-cross-icons deny_request " + (props.props.approve ? '' : 'active')}></i>
            <i onClick={(e) => props.action(e, props.index, true)} className={"icon icon-accept-approve1-ie approve_request " + (props.props.approve ? 'active' : '')}></i>
        </div>
    </div>
}

export const ParticipantBookerUpdate = (props) => {
    return <div className="other_conter">
   
        <div className="col-sm-12">

        <ul className='inl_ulAp'>
                <li>
                     <span className="color">From:</span>
                    {props.props.previous.map((val, index) => (
                    <div key={index+1}>
                        {val.firstname} {val.lastname} {'('+val.relation+')'} - {val.phone} - {val.email}
                    </div>
                    ))}
                </li>
                <li>
                    <span className="color"> To:</span>
                    {props.props.new.map((val, index) => (
                    <div key={index+1}>
                        {val.firstname} {val.lastname} {'('+val.relation+')'} - {val.phone} - {val.email}
                    </div>
                    ))}
                </li>
               
            </ul>
            

            
        </div>

     
        <div className="col-sm-12 text-right apr_col">
            <i onClick={(e) => props.action(e, props.index, false)} className={"icon icon-cross-icons deny_request " + (props.props.approve ? '' : 'active')}></i>
            <i onClick={(e) => props.action(e, props.index, true)} className={"icon icon-accept-approve1-ie approve_request " + (props.props.approve ? 'active' : '')}></i>
        </div>
    </div>
}

export const ParticipantUpdateGoal = (props) => {
    return <div className="other_conter">
        <div className="col-sm-12">
            <span className="color">From:</span>
        <div className="col-sm-12"> 
            <ul className='inl_ulAp'>
                <li><span class="color">Goal:</span> {props.props.previous.title}</li>
                <li><span class="color">Start Date: </span>{moment(props.props.new.start_date).format("DD/MM/YYYY")}</li>
                <li><span class="color">End Date: </span>{moment(props.props.new.end_date).format("DD/MM/YYYY")}<span></span></li>
            </ul>
             
        </div>
        </div>
        
        <div className="col-sm-12">
            <span className="color">To:</span>
        <div className="col-sm-12"> 
            <ul className='inl_ulAp'>
                <li><span class="color">Goal:</span> {props.props.new.title}</li>
                <li><span class="color">Start Date: </span>{moment(props.props.new.start_date).format("DD/MM/YYYY")}</li>
                <li><span class="color">End Date: </span>{moment(props.props.new.end_date).format("DD/MM/YYYY")}<span></span></li>
            </ul>
             
        </div>
        </div>
        
        <div className="col-sm-12 text-right apr_col">
            <i onClick={(e) => props.action(e, props.index, false)} className={"icon icon-cross-icons deny_request " + (props.props.approve ? '' : 'active')}></i>
            <i onClick={(e) => props.action(e, props.index, true)} className={"icon icon-accept-approve1-ie approve_request " + (props.props.approve ? 'active' : '')}></i>
        </div>
    </div>
}

export const ParticipantArchiveGoal = (props) => {
    return <div className="other_conter">
   
        <div className="col-sm-12"> 
            <ul className='inl_ulAp'>
                <li><span class="color">Goal:</span> {props.props.previous.title}</li>
                <li><span class="color">Start Date: </span>{moment(props.props.new.start_date).format("DD/MM/YYYY")}</li>
                <li><span class="color">End Date: </span>{moment(props.props.new.end_date).format("DD/MM/YYYY")}<span></span></li>
            </ul>
             
        </div>
       
        
        <div className="col-sm-12 text-right apr_col">
            <i onClick={(e) => props.action(e, props.index, false)} className={"icon icon-cross-icons deny_request " + (props.props.approve ? '' : 'active')}></i>
            <i onClick={(e) => props.action(e, props.index, true)} className={"icon icon-accept-approve1-ie approve_request " + (props.props.approve ? 'active' : '')}></i>
        </div>
    </div>
}

export const CreateParticipantShift = (props) => {
    return <div className="other_conter">
   
        <div className="col-sm-12"> 
            <ul className='inl_ulAp'>
                <li><span class="color">Shift Date: </span> {moment(props.props.new.start_time).format("DD/MM/YYYY")}</li>
                <li><span class="color">Start Date Time: </span>{moment(props.props.new.start_time).format("DD/MM/YYYY LT")}</li>
                <li><span class="color">End Date Time: </span>{moment(props.props.new.end_time).format("DD/MM/YYYY LT")}<span></span></li>
            </ul>
             
        </div>
        
        {props.props.new.location.map((val, index) => (
            <div key={index +1} className="col-sm-12"> 
                 <span>Location:</span> {val.address}, {val.suburb.value}, {props.props.stateLabel[val.state]} - {val.postal}
            </div>
        ))}

        <div className="col-sm-12"> 
             <span class="color">Shift Requirement:</span> {props.props.new.requirement || 'N/A'}
        </div>
        <div className="col-sm-12"> 
             <span class="color">Preferred Member:</span> {props.props.new.preferred_member || 'N/A'}
        </div>
        
        <div className="col-sm-12 text-right apr_col">
            <i onClick={(e) => props.action(e, props.index, false)} className={"icon icon-cross-icons deny_request " + (props.props.approve ? '' : 'active')}></i>
            <i onClick={(e) => props.action(e, props.index, true)} className={"icon icon-accept-approve1-ie approve_request " + (props.props.approve ? 'active' : '')}></i>
        </div>
    </div>
}