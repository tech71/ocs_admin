import React, { Component } from 'react';
import jQuery from "jquery";

import ReactTable from "react-table";
import 'react-table/react-table.css'

import { checkItsNotLoggedIn } from '../../../service/common.js';
import { ROUTER_PATH, BASE_URL, PAGINATION_SHOW } from '../../../config.js';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';


// globale varibale to stote data
var rawData = [];


const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {
        
        // request json
        var Request = JSON.stringify({pageSize: pageSize, page: page, sorted: sorted, filtered: filtered});

        jQuery.ajax({
            url: BASE_URL + 'admin/Dashboard/list_role',
            type: 'POST',
            dataType: 'JSON',
            cache: false,
            data: {request: Request},
            success: function (responseJson) {
                let filteredData = responseJson.data;
                const res = {
                    rows: filteredData,
                    pages: (responseJson.count)
                };
                setTimeout(() => resolve(res), 10);
            }.bind(this),
            error: function (xhr) {
                console.log(xhr);
            }.bind(this)
        });
        
        
    });
    
     
};

/*
 * class Roles 
 */
class Roles extends Component {
    constructor(props) {
        super(props);

        this.state = {
            role_id : '', 
            role_name: '',
            role_error :'',
            loading: false,
            rolesList: [],
            counter: 0,
            startDate: new Date()

        };

        this.handleFilter = this.handleFilter.bind(this); // this function use for search function
        this.fetchData = this.fetchData.bind(this); // this function use for pass data in react table
        this.handleChange = this.handleChange.bind(this); // this function use for reflected change in input field
        this.onSubmit = this.onSubmit.bind(this) // this function use for edit and add new role in system mean submit data
        
        this.editRole = this.editRole.bind(this); // this function use for edit role
        this.resetState = this.resetState.bind(this); // this function use for reset state
       
    }
    
    resetState (e){
        // this function only for reset update
        this.setState({loading: false, role_id : '', role_name : '',role_error:''});
    }
    
    refreashReactTable(){
        // if filter is update then react table automaticallty update on change to filter 
        // and even odd functionality use beacuse same change it does not refreash react table
        if(this.state.counter%2==0){
            this.setState({counter : this.state.counter+1, filtered: ''});
        }else{
            this.setState({counter : this.state.counter+1, filtered: false});
        }
    }

    handleChange(e) {
        var state = {};
        this.setState({role_error: ''});
        state[e.target.name] = e.target.value;
        this.setState(state);
    }
    
    handleFilter(e) {
        // reflec value of search box
          this.setState({  filtered: e.target.value })
    };
    
    editRole(id, rollName){
        // for edit role set state role name and role id
       // this.setState({role_name: rollName, role_id : id});
        //jQuery('#addRoles').modal('show');
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.state.role_name) {
            jQuery.ajax({
                url: BASE_URL + 'admin/Dashboard/add_role',
                type: 'POST',
                dataType: 'JSON',
                data: {'role_name': this.state.role_name, 'role_id': this.state.role_id},
                beforeSend: function () {
                      this.setState({loading:true});
                }.bind(this),
                success: function (data) {
                    if(data.status){
                        this.resetState();
                        this.refreashReactTable();
                        jQuery('#addRoles').modal('hide');
                    }else{
                         this.setState({'role_error': data.error});
                    }
                    this.setState({loading:false});
                }.bind(this),
                error: function (xhr) {
                    this.setState({loading: false});
                }.bind(this)
            });
        }else{
            this.setState({'role_error': 'Please enter role name'});
        }
    }    
    
    archiveHandle(id){
            jQuery.ajax({
                url: BASE_URL + 'admin/Dashboard/delete_role',
                type: 'POST',
                dataType: 'JSON',
                data: {'role_id': id},
                beforeSend: function () {
                      this.setState({loading:true});
                }.bind(this),
                success: function (data) {
                    this.setState({loading: false });
                     this.refreashReactTable();
                }.bind(this),
                error: function (xhr) {
                }.bind(this)
            });
        
    }
    
     activeDeactiveHandle(id,status){
             jQuery.ajax({
                url: BASE_URL + 'admin/Dashboard/active_inactive_role',
                type: 'POST',
                dataType: 'JSON',
                data: {'role_id': id, 'status': status},
                beforeSend: function () {
                      this.setState({loading:true});
                }.bind(this),
                success: function (data) {
                    this.setState({loading: false });
                     this.refreashReactTable();
                }.bind(this),
                error: function (xhr) {
                }.bind(this)
            });
     }

    componentDidMount() {
        
    }
    
    fetchData(state, instance) {
        // function for fetch data from database
        this.setState({loading: true});
        requestData(
                state.pageSize,
                state.page,
                state.sorted,
                state.filtered
                ).then(res => {
            this.setState({
                rolesList: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }

    render() {
        const {data, pages, loading} = this.state;
        const columns = [{Header: 'Role-ID', accessor: 'id',filterable: false,},
            {Header: 'Role Name', accessor: 'name',filterable: false,},
            {Header: 'Creat Date', accessor: 'created',filterable: false,},
            {
                Header: 'Action',
                accessor: 'id',
                sortable: false,
                filterable: false,
                Cell: props => <span>
                    {console.log(props)}
                    <a href="javascript:void(0)" className="btn btn-primary edit" onClick={() => this.editRole(props.value ,props.original.name)}>Edit</a>
                    <button className="btn btn-danger" onClick={() => this.archiveHandle(props.value)} >Archive</button>
                    {props.original.status == 1? <button className="btn btn-danger" onClick={() => this.activeDeactiveHandle(props.value, 0)} >Inactive</button> : 
                            <button className="btn btn-danger" onClick={() => this.activeDeactiveHandle(props.value, 1)} >Active</button> }
                </span>
            }
        ]
        

        return (
                <div>
                <section className="manage_top">
                    <div className="container-fluid">
                        
                        <div className="row  _Common_back_a">
                            <div className="col-lg-10 col-lg-offset-1 col-md-12">
                            <Link to={ROUTER_PATH+'admin/user'}><div className="icon icon-back-arrow back_arrow"></div></Link>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bt-1"></div>
                            </div>
                        </div>
                        <div className="row _Common_He_a">
                            <div className="col-lg-8 col-lg-offset-1  col-md-12">
                                <h1 className="color">Roles</h1>
                            </div>
                        </div>

                
                        <div className="row _Common_He_a">
                            <div className="col-lg-5 col-lg-offset-2 col-md-7 search_bar">
                                <div className="input_search change_b"> 
                                    <input type="search" id="searchRoles"  onChange={this.handleFilter} className="form-control" placeholder="Search" />
                                    <button type="submit">
                                        <span className="icon icon-search"></span>
                                    </button>  
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-2">
                                <Link className="but" to={ROUTER_PATH+'admin/user/roles/add'} >Add role</Link>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-10 col-lg-offset-1 col-md-12 P_15_T"><div className="bb-1"></div></div>
                        </div>
                
                        <div className="row">
                            <div className="col-lg-8 col-lg-offset-2 col-md-12">
                                <ReactTable
                                columns={columns}
                                manual 
                                data={this.state.rolesList}
                                pages={this.state.pages}
                                loading={this.state.loading} 
                                onFetchData={this.fetchData} 
                                filtered={this.state.filtered}
                                defaultPageSize={10}
                                className="-striped -highlight"  
                                noDataText="No roles"
                                minRows={1}
                                    showPagination={this.state.rolesList.length >= PAGINATION_SHOW ? true : false }
                                />
                            </div>
                        </div>
                
                    </div>
                
                <div className="modal fade-scale Modal_A" id="addRoles" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                            <form id="add_roles_form">
                                <div className="lock_icon"><i className="icon icon-lock-icons"></i></div>
                                <div className="text text-center bor_TB color">Add roles</div>
                                <div className="six_digit">
                                
                                <input type="text" name="role_name" placeholder="Role name"  onChange={this.handleChange} value={this.state.role_name} />
                                <label id="role_name-error" className="error">{this.state.role_error }</label>
                                <a href="javascript:void(0)" onClick={this.onSubmit} className="but">{this.state.role_id>0 ? 'Upadate': 'Add' } role</a>
                                </div>
                                
                                <div className="text-center"><a href="javascript:void(0)" data-dismiss="modal" aria-label="Close" onClick={this.resetState } className="close_i"><i className="icon icon-cross-icons"></i></a></div>
                                </form>
                               </div>
                        </div>
                    </div>
                </div>
                
                </section>
                </div>
                );
    }
}
export default Roles;
   