import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ProgressBar } from 'react-bootstrap';
import 'react-table/react-table.css';
import 'react-select-plus/dist/react-select-plus.css';
import { ROUTER_PATH } from '../../../config.js';
import CrmPage from './CrmPage';
import { connect } from 'react-redux'

class FundingDetails extends Component {
  constructor(props, context) {
      super(props, context);
      this.participantDetailsRef = React.createRef();
      this.state = { participant_id:''};

  }

  componentDidMount(){
    this.setState({participant_id:this.props.props.match.params.id});
    this.participantDetailsRef.current.wrappedInstance.getParticipantDetails(this.props.props.match.params.id);
  }

    render() {

        const now = 60;
        const nownew = 12;
        const startdate = 15;
        return (
            <div className="container-fluid">
            <CrmPage ref={this.participantDetailsRef} pageTypeParms={'participant_funding_details'} />
                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1 col-md-12">
                        <div className="back_arrow py-4 bb-1">
                            <Link to={ROUTER_PATH + 'admin/crm/prospectiveparticipants'}><span className="icon icon-back1-ie"></span></Link>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1  col-md-12">
                        <div className="row d-flex py-4">
                            <div className="col-md-6 align-self-center br-1">
                                <div className="h-h1 ">
                                    {this.props.showPageTitle}
                            </div>
                            </div>
                            <div className="col-md-6">
                                <div className="Lates_up_1">
                                    <div className="Lates_up_a col-md-3 align-self-center">
                                        Latest
                                        Update:
                                    </div>
                                    <div className="col-md-9 justify-content-between pr-0">
                                        <div className="Lates_up_b">
                                            <div className="Lates_up_txt"><b>Stage 2:</b> Attachment added- Service Agreement Doc</div>
                                            <div className="Lates_up_btn br-1 bl-1"><i className="icon icon-view1-ie"></i><span>View Attachment</span></div>
                                            <div className="Lates_up_btn"><i className="icon icon-view1-ie"></i><span>View all Updates</span></div>
                                        </div>
                                        <div className="Lates_up_2">
                                            <div className="Lates_up_txt2 btn-1">Susan McDonald (Recruiter)</div>
                                            <div className="Lates_up_time_date"> Date: 01/01/01 - 11:32AM</div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div className="row"><div className="col-md-12"><div className="bt-1"></div></div></div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1 col-md-12 mt-5">
                        <div className="progress-b1">
                            <ProgressBar className="progress-b2" now={now} label={'Intake Progress: ' + `${now}%` + 'Complete'} />
                        </div>
                    </div>
                </div>

                <div className="row">

                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="shift_h1 py-2 by-1">FundingDetails</div>
                            </div>
                        </div>
                        <div className="row">

                            <div className="col-md-12">
                                <div className="Partt_d1_txt_3 my-2"><strong>Participants Plan: </strong><br /><span>Portal Managed</span></div>
                            </div>
                            <div className="col-md-12">
                                <div className="Partt_d1_txt_3 mt-4"><strong>Confirmed Plans</strong></div>
                            </div>

                            <div className="col-md-12">
                                <div className="flex_Table">
                                    <div className="flex_Table-row">
                                        <div className="flex_Table-row-item ft_1"><b>Plan ID#:</b> 00000</div>
                                        <div className="flex_Table-row-item ft_2"><b>ONCALL Plan ID:</b> 00000</div>
                                        <div className="flex_Table-row-item ft_3"><b>Plan Name:</b> Plan 01 - Lorem ipsum</div>
                                        <div className="flex_Table-row-item"><b>Plan Length:</b> 01/01/01 - 01/01/02</div>
                                    </div>

                                    <div className="flex_Table-row">
                                        <div className="flex_Table-row-item ft_1"><b>Plan ID#:</b> 00000</div>
                                        <div className="flex_Table-row-item ft_2"><b>ONCALL Plan ID:</b> 00000</div>
                                        <div className="flex_Table-row-item ft_3"><b>Plan Name:</b> Plan 01 - Lorem ipsum</div>
                                        <div className="flex_Table-row-item"><b>Plan Length:</b> 01/01/01 - 01/01/02</div>
                                    </div>

                                    <div className="flex_Table-row">
                                        <div className="flex_Table-row-item ft_1"><b>Plan ID#:</b> 00000</div>
                                        <div className="flex_Table-row-item ft_2"><b>ONCALL Plan ID:</b> 00000</div>
                                        <div className="flex_Table-row-item ft_3"><b>Plan Name:</b> Plan 01 - Lorem ipsum</div>
                                        <div className="flex_Table-row-item"><b>Plan Length:</b> 01/01/01 - 01/01/02</div>
                                    </div>


                                </div>
                            </div>


                        </div>

                        <div className="row mt-4">
                            <div className="col-md-12"><div className=" bt-1 mb-3"></div></div>
                            <div className="col-md-12">
                                <div className="Partt_d1_txt_3"><strong>NDIS Plan #: </strong> <span>93450</span></div>
                                <div className="Partt_d1_txt_3"><strong>ONCALL Plan ID #:</strong> <span>862991</span></div>
                            </div>
                            <div className="col-md-3">
                                <a className="v-c-btn1"><span>NDIS Plan Document.PDF</span> <i className="icon icon-view1-ie"></i></a>
                            </div>
                            <div className="col-md-6 col-md-offset-1">
                                <div className=" d-flex justify-content-between">
                                    <div className="Partt_d1_txt_3"><strong>Start Date:  </strong> <span>0/12/2020</span></div>
                                    <div className="Partt_d1_txt_3"><strong>End Date: </strong> <span>0/12/2020</span></div>
                                </div>
                                <div className="progress-b3">
                                    <ProgressBar className="progress-b2" now={startdate} />
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="shift_h1 py-2 by-1">Funds Breakdown</div>
                            </div>
                            <div className="col-md-12">
                                <div className="Partt_d1_txt_3 py-2"><strong>Finance Breakdown:</strong> </div>
                            </div>
                            <div className="col-md-12"> <img src="/assets/images/total_funding.jpg" /></div>


                            <div className="col-md-12"><div className=" bt-1 mb-3"></div></div>
                        </div>

                        <div className="row d-flex">
                            <div className="col-md-4 br-1">
                                <div className="Partt_d1_txt_3 py-2"><strong>Percentage Breakdown:</strong> </div>
                                <div className="px-5"> <img src="/assets/images/Percentage_img.jpg" /></div>
                            </div>
                            <div className="col-md-8">
                                <div className="Partt_d1_txt_3 py-2 pl-5"><strong>Plan Breakdown:</strong> </div>
                                <div className="vertical_scroll Break_dow_Scroll_D px-5">

                                    <div className="Break_dow_SD">
                                        <div className="Break_dow_T">
                                            <div className="Break_dow_T_a"><strong>01_011_0107_1_1:</strong> Assistance With Self-Care Activities (Standard - Weekday day </div>
                                            <div className="Break_dow_T_b"><strong>Allocated:</strong> $30,000</div>
                                        </div>
                                        <div className="Break_dow_P">
                                            <ProgressBar className="Break_dow_P1" now={nownew} label={`${nownew}%` + ' (Used)'} />
                                        </div>
                                    </div>

                                       <div className="Break_dow_SD">
                                        <div className="Break_dow_T">
                                            <div className="Break_dow_T_a"><strong>01_011_0107_1_1:</strong> Assistance With Self-Care Activities (Standard - Weekday day </div>
                                            <div className="Break_dow_T_b"><strong>Allocated:</strong> $30,000</div>
                                        </div>
                                        <div className="Break_dow_P">
                                            <ProgressBar className="Break_dow_P1" now={nownew} label={`${nownew}%` + ' (Used)'} />
                                        </div>
                                    </div>


                                       <div className="Break_dow_SD">
                                        <div className="Break_dow_T">
                                            <div className="Break_dow_T_a"><strong>01_011_0107_1_1:</strong> Assistance With Self-Care Activities (Standard - Weekday day </div>
                                            <div className="Break_dow_T_b"><strong>Allocated:</strong> $30,000</div>
                                        </div>
                                        <div className="Break_dow_P">
                                            <ProgressBar className="Break_dow_P1" now={nownew} label={`${nownew}%` + ' (Used)'} />
                                        </div>
                                    </div>

                                      <div className="Break_dow_SD">
                                        <div className="Break_dow_T">
                                            <div className="Break_dow_T_a"><strong>01_011_0107_1_1:</strong> Assistance With Self-Care Activities (Standard - Weekday day </div>
                                            <div className="Break_dow_T_b"><strong>Allocated:</strong> $30,000</div>
                                        </div>
                                        <div className="Break_dow_P">
                                            <ProgressBar className="Break_dow_P1" now={nownew} label={`${nownew}%` + ' (Used)'} />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>



                        <div className="row d-flex justify-content-end mt-4">
                            <div className="col-md-3"> <span className="btn-3">Edit Participants Shifts</span></div>
                        </div>


                    </div>


                </div>
            </div>
        );
    }
}
const mapStateToProps = state => {
      return {
        showPageTitle: state.DepartmentReducer.activePage.pageTitle,
        showTypePage: state.DepartmentReducer.activePage.pageType,
  
      }
  };
export default connect(mapStateToProps)(FundingDetails);
