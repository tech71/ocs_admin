import React, { Component } from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Select from 'react-select-plus';
import moment from 'moment';
import { checkItsNotLoggedIn, postData, IsValidJson,getOptionsCrmMembers,input_kin_lastname, getOptionsSuburb, handleRemoveShareholder, handleShareholderNameChange, handleAddShareholder, getQueryStringValue }
  from '../../../service/common.js';
var options = [
    { value: 'one', label: 'One' },
    { value: 'two', label: 'Two' }
];

export const AllUpdates = (props) => {

    let allnotes={};

    if (typeof(props.allupdates.notes) !== 'undefined' || props.allupdates.notes != null) {
          allnotes =  props.allupdates.notes;
    }


    return (
        <Modal className="modal fade Crm" bsSize="large" show={props.showModal} onHide={() => props.handleClose} >
            <form id="special_agreement_form" method="post" autoComplete="off">
                <Modal.Body className="px-0 py-0">

                    <div className="custom-modal-header bb-1">
                        <div className="Modal_title">All  Upatates - Anthonyh Johnston</div>
                        <i className="icon icon-close3-ie Modal_close_i" onClick={() => props.handleClose()}></i>
                    </div>
               <div className="row px-5 d-flex mt-5">
                        <div class="col-md-7">
                            <div class="search_icons_right modify_select  " >
                            <Select.Async
                                cache={false}
                                clearable={false}
                                name="assign_to"
                                value={props.onSelectDisp.assign_to}
                                loadOptions={getOptionsCrmMembers}
                                placeholder='Search'
                                onChange={(e)=>props.selectedChange(e,'assign_to')}
                                className="custom_select"
                            />
                                {/*
                                <input type="text" name="search" />
                                <button type="submit"><span class="icon icon-search1-ie"></span></button> */}
                            </div>

                        </div>

                        <div class="col-md-2 align-self-center text-right">
                            Filter by:
                        </div>

                        <div class="col-md-3">
                            <div className="s-def1 s1 mt-2 mb-5">
                                <Select
                                    name="view_by_status"
                                    options={props.stages}
                                    required={true}
                                    simpleValue={true}
                                    searchable={false}
                                    clearable={false}
                                    placeholder="Filter by: Unread"
                                    onChange={(e)=>props.selectedChange1(e,'view_by_status')}
                                    value={props.onSelectDisp.view_by_status}
                                />
                            </div>
                        </div>
                    </div>

                    <div className="custom-modal-body w-100 mx-auto px-5 pb-5">
                        <div className="row">
                            <div className="col-md-12 pt-4 pb-3 title_sub_modal">Updates:</div>
                            <div className="col-md-12"><div className="bb-1"></div></div>
                        </div>

                        <div className="row w-90 mx-auto">
                            <div className="horizontal_scroll Update-all-main">





  
{
           Object.keys(allnotes).map((key) => (

              <div className="Update-all-1">
              <div className="Update-all-1a">
                  <span className="Update-all-1aa">{allnotes[key].member_name} <em>(Recruiter)</em></span>
                  <span className="Update-all-1ab">Date: {moment(allnotes[key].created_at).format('DD/MM/YYYY - HH:MM A') }</span>
              </div>
              <div className="Update-all-1b">
                  <span className="Update-all-1ba"><strong>{allnotes[key].stage_name} &nbsp; </strong> {allnotes[key].notes}</span>
                  <span className="Update-all-1bb">
                      <div>
                          <a href=""> <i className="icon icon-view1-ie"></i>
                              <small> View Stage</small>
                          </a>
                      </div>
                  </span>
              </div>
          </div>
         ))

    }




                                {/* <div className="Update-all-1">
                                    <div className="Update-all-1a">
                                        <span className="Update-all-1aa">Johnny McDonald<em>(Recruiter)</em></span>
                                        <span className="Update-all-1ab">Date: 01/01/01 - 08:32AM</span>
                                    </div>
                                    <div className="Update-all-1b">
                                        <span className="Update-all-1ba"><strong>Stage 2.4:</strong> Recieve Documents (Participants Docs)</span>
                                        <span className="Update-all-1bb">
                                            <div>
                                                <a href=""> <i className="icon icon-view1-ie"></i>
                                                    <small> View Stage</small>
                                                </a>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <div className="Update-all-1">
                                    <div className="Update-all-1a">
                                        <span className="Update-all-1aa">Johnny McDonald <em>(Recruiter)</em></span>
                                        <span className="Update-all-1ab">Date: 01/01/01 - 08:32AM</span>
                                    </div>
                                    <div className="Update-all-1b">
                                        <span className="Update-all-1ba"><strong>Stage 2.4:</strong> Recieve Documents (Participants Docs)</span>
                                        <span className="Update-all-1bb">
                                            <div>
                                                <a href=""> <i className="icon icon-view1-ie"></i>
                                                    <small> View Stage</small>
                                                </a>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <div className="Update-all-1">
                                    <div className="Update-all-1a">
                                        <span className="Update-all-1aa">Johnny McDonald <em>(Recruiter)</em></span>
                                        <span className="Update-all-1ab">Date: 01/01/01 - 08:32AM</span>
                                    </div>
                                    <div className="Update-all-1b">
                                        <span className="Update-all-1ba"><strong>Stage 2.4:</strong> Recieve Documents (Participants Docs)</span>
                                        <span className="Update-all-1bb">
                                            <div>
                                                <a href=""> <i className="icon icon-view1-ie"></i>
                                                    <small> View Stage</small>
                                                </a>
                                            </div>
                                        </span>
                                    </div>
                                </div> */}



                            </div>
                        </div>


                    </div>

                    {/* <div className="custom-modal-footer bt-1 mt-5">
                        <div className="row d-flex justify-content-end">
                            <div className="col-md-3"><a className="btn-1">Save and Lock Away Funds</a></div>
                        </div>
                    </div> */}


                </Modal.Body>
            </form>
        </Modal>
    );

}
