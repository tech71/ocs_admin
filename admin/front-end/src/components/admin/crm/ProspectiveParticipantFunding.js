import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ProgressBar } from 'react-bootstrap';
import 'react-table/react-table.css';
import 'react-select-plus/dist/react-select-plus.css';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { ROUTER_PATH } from '../../../config.js';
import CrmPage from './CrmPage';
import Select from 'react-select-plus';
import { connect } from 'react-redux'

class ProspectiveParticipantFunding extends Component {
    constructor(props, context) {
        super(props, context);
        this.participantDetailsRef = React.createRef();
        this.state = { participant_id: '' };

    }

    componentDidMount() {
        this.setState({ participant_id: this.props.props.match.params.id });
        this.participantDetailsRef.current.wrappedInstance.getParticipantDetails(this.props.props.match.params.id);
    }



    render() {

        const data = [{
            planid: '36746',
            planday: 'Plan 3 - Monday- Thurs',
        }, {
            planid: '36746',
            planday: 'Plan 3 - Monday- Thurs',
        }]
        var options = [
            { value: 'Plan id #:00000', label: 'Plan id #:00000' },
            { value: 'Plan id #:00000', label: 'Plan id #:00000' }
        ];

        const columns = [{
            Header: 'Plan id',
            accessor: 'planid',
            headerClassName: 'Th_class_d1 _align_c__',
            maxWidth: 95,
            className: (this.state.activeCol === 'name') && this.state.resizing ? 'borderCellCls' : 'Tb_class_d1',
            Cell: props => <span className="h-100" style={{ justifyContent: 'center' }}>
                <div>
                    {props.value}
                </div>
            </span>
        }, {
            Header: 'Plan Day',
            accessor: 'planday',
            headerClassName: 'Th_class_d1 _align_c__',
            className: (this.state.activeCol === 'name') && this.state.resizing ? 'borderCellCls' : 'Tb_class_d1',
            Cell: props => <span className="h-100" style={{ justifyContent: 'center' }}>
                <div>
                    {props.value}
                </div>
            </span>
        }, {
            Header: 'Start Date and End Date',
            accessor: 'startend',
            Cell: props => <div>
                <div className=" d-flex justify-content-between sed_set_0_">
                    <div className="Partt_d1_txt_3"><strong>Start Date:  </strong> <span>0/12/2020</span></div>
                    <div className="Partt_d1_txt_3"><strong>End Date: </strong> <span>0/12/2020</span></div>
                </div>
                <div className="progress-b3 progress_b4">
                    <ProgressBar className="progress-b2" now={startdate} label={`${now}%` + 'Complete'} />
                </div>
            </div>

        }]

        const now = 60;
        const nownew = 12;
        const startdate = 15;
        return (
            <div className="container-fluid">
                <CrmPage ref={this.participantDetailsRef} pageTypeParms={'prospective_participant_funding'} />
                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1 col-md-12">
                        <div className="back_arrow py-4 bb-1">
                            <Link to={ROUTER_PATH + 'admin/crm/prospectiveparticipants'}><span className="icon icon-back1-ie"></span></Link>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1  col-md-12">
                        <div className="row d-flex py-4">
                            <div className="col-md-6 align-self-center br-1">
                                <div className="h-h1 ">
                                    {this.props.showPageTitle}
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="Lates_up_1">
                                    <div className="Lates_up_a col-md-3 align-self-center">
                                        Latest
                                        Update:
                                    </div>
                                    <div className="col-md-9 justify-content-between pr-0">
                                        <div className="Lates_up_b">
                                            <div className="Lates_up_txt"><b>Stage 2:</b> Attachment added- Service Agreement Doc</div>
                                            <div className="Lates_up_btn br-1 bl-1"><i className="icon icon-view1-ie"></i><span>View Attachment</span></div>
                                            <div className="Lates_up_btn"><i className="icon icon-view1-ie"></i><span>View all Updates</span></div>
                                        </div>
                                        <div className="Lates_up_2">
                                            <div className="Lates_up_txt2 btn-1">Susan McDonald (Recruiter)</div>
                                            <div className="Lates_up_time_date"> Date: 01/01/01 - 11:32AM</div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div className="row"><div className="col-md-12"><div className="bt-1"></div></div></div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1 col-md-12 mt-5">
                        <div className="progress-b1">
                            <ProgressBar className="progress-b2" now={now} label={'Intake Progress: ' + `${now}%` + 'Complete'} />
                        </div>
                    </div>
                </div>

                <div className="row">

                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="shift_h1 py-2 by-1">Plan</div>
                            </div>
                        </div>
                        <div className="row">

                            <div className="col-md-9">
                                <div className="Partt_d1_txt_1 my-3"><strong>Participants Plan: </strong><span>Portal Managed</span></div>
                            </div>
                            <div className="col-md-3">
                            <div className="s-def1 s1 mt-3">
                                    <Select
                                        name="form-field-name"
                                        value="Plan id #:00000"
                                        options={options}
                                    />
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="PPartt_d1_txt_1 mt-4 mb-2"><strong>Plan Details</strong></div>
                            </div>

                            <div className="col-md-12 re-table re-table-collapse th_txt_center__ reacttable_header_none">
                                <ReactTable
                                    data={data}
                                    columns={columns}
                                    onPageSizeChange={this.onPageSizeChange}
                                    defaultPageSize={2}
                                    showPagination={false} />

                            </div>
                        </div>

                        <div className="row mt-4">
                            <div className="col-md-12">
                                <div className="shift_h1 py-2 by-1">Funds Breakdown</div>
                            </div>
                        </div>

                        <div className="row d-flex mt-5">
                            <div className="col-md-6 br-1 pr-5">
                                <div className="w-80 mb-4 s-def1 s-def1-color">
                                    <Select
                                        name="form-field-name"
                                        value="Plan id #:00000"
                                        options={options}
                                    />
                                </div>
                                <div className="Partt_d1_txt_1 py-2"><strong>Plans 1- Weekends</strong> </div>
                                <div className="row">
                                    <div className="col-md-10 col-md-offset-1">
                                        <div className="Partt_d1_txt_3"><strong>Start Date:  </strong> <span>0/12/2020</span></div>
                                        <div className="Partt_d1_txt_3"><strong>End Date: </strong> <span>0/12/2020</span></div>
                                    </div>
                                </div>
                                <div className="Partt_d1_txt_1 py-2 mt-4"><strong>Finance Breakdown: </strong> </div>
                                <div className="row">
                                    <div className="col-md-12">

                                        <div className="to_used__">
                                            <div className="Partt_d1_txt_1 mb-3"><strong>Totals:</strong><span> $3000000</span></div>
                                            <div className="progress-b3 progress-b5">
                                                <ProgressBar className="progress-b2" now={startdate} />
                                            </div>
                                            <div className=" d-flex justify-content-between">
                                                <div className="Partt_d1_txt_3"><strong>Used:  </strong> <span>$15,611.61</span></div>
                                                <div className="Partt_d1_txt_3"><strong>Remaining: </strong> <span>$205,717.69</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="Partt_d1_txt_1 py-2 pl-5"><strong>Plan Breakdown:</strong> </div>
                                <div className="vertical_scroll Break_dow_Scroll_D px-5">

                                    <div className="Break_dow_SD">
                                        <div className="Break_dow_T">
                                            <div className="Break_dow_T_a"><strong>01_011_0107_1_1:</strong> Assistance With Self-Care Activities (Standard - Weekday day </div>
                                            <div className="Break_dow_T_b"><strong>Allocated:</strong> $30,000</div>
                                        </div>
                                        <div className="Break_dow_P">
                                            <ProgressBar className="Break_dow_P1" now={nownew} label={`${nownew}%` + ' (Used)'} />
                                        </div>
                                    </div>

                                    <div className="Break_dow_SD">
                                        <div className="Break_dow_T">
                                            <div className="Break_dow_T_a"><strong>01_011_0107_1_1:</strong> Assistance With Self-Care Activities (Standard - Weekday day </div>
                                            <div className="Break_dow_T_b"><strong>Allocated:</strong> $30,000</div>
                                        </div>
                                        <div className="Break_dow_P">
                                            <ProgressBar className="Break_dow_P1" now={nownew} label={`${nownew}%` + ' (Used)'} />
                                        </div>
                                    </div>


                                    <div className="Break_dow_SD">
                                        <div className="Break_dow_T">
                                            <div className="Break_dow_T_a"><strong>01_011_0107_1_1:</strong> Assistance With Self-Care Activities (Standard - Weekday day </div>
                                            <div className="Break_dow_T_b"><strong>Allocated:</strong> $30,000</div>
                                        </div>
                                        <div className="Break_dow_P">
                                            <ProgressBar className="Break_dow_P1" now={nownew} label={`${nownew}%` + ' (Used)'} />
                                        </div>
                                    </div>

                                    <div className="Break_dow_SD">
                                        <div className="Break_dow_T">
                                            <div className="Break_dow_T_a"><strong>01_011_0107_1_1:</strong> Assistance With Self-Care Activities (Standard - Weekday day </div>
                                            <div className="Break_dow_T_b"><strong>Allocated:</strong> $30,000</div>
                                        </div>
                                        <div className="Break_dow_P">
                                            <ProgressBar className="Break_dow_P1" now={nownew} label={`${nownew}%` + ' (Used)'} />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div className="row d-flex justify-content-end mt-4">
                            <div className="col-md-3"> <span className="btn-3">Edit Participants Shifts</span></div>
                        </div>
                    </div>


                </div>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        showPageTitle: state.DepartmentReducer.activePage.pageTitle,
        showTypePage: state.DepartmentReducer.activePage.pageType,

    }
};
export default connect(mapStateToProps)(ProspectiveParticipantFunding);
