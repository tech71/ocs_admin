import React, { Component } from 'react';

import { ROUTER_PATH, BASE_URL } from '../../../config.js';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import DashboardAdmin from '../../admin/crm/Participantadmin';
import NewParticipant from '../../admin/crm/CreateParticipant';
import DashboardUser from '../../admin/crm/ParticipantUser';
import ParticipantDetails from '../../admin/crm/ParticipantDetails';
import ProspectiveParticipants from '../../admin/crm/ProspectiveParticipants';
import Participantability from '../../admin/crm/Participantability';
import Reporting from '../../admin/crm/Reporting';
import Schedules from '../../admin/crm/Schedules';
import FundingDetails from '../../admin/crm/FundingDetails';
import Shifts from '../../admin/crm/Shifts';
import Tasks from '../../admin/crm/Tasks';
import LocationAnalytics from '../../admin/crm/LocationAnalytics';
import UserMangement from '../../admin/crm/UserMangement';
import Departments from '../../admin/crm/Departments';
import StaffDetails from '../../admin/crm/StaffDetails';
import ProspectiveParticipantFunding from '../../admin/crm/ProspectiveParticipantFunding';
import CRMDashboard from '../../admin/crm/Dashboard';
import ServiceAnalytics from '../../admin/crm/ServiceAnalytics';
import { setFooterColor } from '../../admin/notification/actions/NotificationAction.js';
import Sidebar from '../Sidebar';

import { connect } from 'react-redux'
import {crmJson,crmLinkHideShowSubmenus} from 'menujson/crm_menu_json';
import {setSubmenuShow} from 'components/admin/actions/SidebarAction';
const menuJson = () => {
  let menu = crmJson;
  return menu;
}
let cssLoaded = false;

class AppCrm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      subMenuShowStatus:true,
      menus:menuJson(),
      replaceData:{':id':0}
    }

  }

  componentWillUnmount() {
    this.props.setFooterColor('');
  }

  componentDidMount() {
    this.props.setSubmenuShow(1);
    this.props.setFooterColor('Crm');
    if (cssLoaded === false) {
      cssLoaded = true;
    }
    setTimeout(() => { this.setState({ loadState: false }) }, 1000)
  }

  getObjectValue(obj){
    let objectDataReturn = {};
    let menuState = obj.state.menus;
    let datasubmenu = crmLinkHideShowSubmenus;
    var i;
    for(i in datasubmenu){
        let objdata = menuState.find(x => x.id == i);
        let objIndex = menuState.indexOf(objdata);
        objectDataReturn[i+'Index'] = objIndex;
        for(var z in datasubmenu[i]){
            let objIndexSubmenu = '';
            let objIndexSubmenuIndex = "-1";
            let objIndexSubSubmenuLinkHide = "-1";
            if(objIndex > -1){
                objIndexSubmenu = menuState[objIndex]['submenus'].find(x => x.id == z);
                objIndexSubmenuIndex = menuState[objIndex]['submenus'].indexOf(objIndexSubmenu);
                if(objIndexSubmenuIndex > -1){
                    let objIndexSubSubmenu = menuState[objIndex]['submenus'][objIndexSubmenuIndex]['subSubMenu'].find(x => x.id == datasubmenu[i][z]);
                    objIndexSubSubmenuLinkHide = menuState[objIndex]['submenus'][objIndexSubmenuIndex]['subSubMenu'].indexOf(objIndexSubSubmenu);
                }
            }
                objectDataReturn[z+'Index'] = objIndexSubmenuIndex;
                objectDataReturn[z+'LinkHide'] = objIndexSubSubmenuLinkHide;

        }
    }
    return objectDataReturn;
}

  allSubSubMenuLinkHide(menuStateData,dataIndex){
    let datasubmenu =crmLinkHideShowSubmenus;
    for(var i in datasubmenu){
        for(var z in datasubmenu[i]){
            if(dataIndex[i+'Index']>-1 && dataIndex[z+'Index']>-1 && dataIndex[z+'LinkHide']>-1){
                menuStateData[dataIndex[i+'Index']]['submenus'][dataIndex[z+'Index']]['subSubMenu'][dataIndex[z+'LinkHide']]['linkOnlyHide']=true;
            }
        }
    }

    return menuStateData;
}

showSubSubMenuLinkSpecific(menuStateData,dataIndex,submenuIndex,nameData){
    let datasubmenu =crmLinkHideShowSubmenus;
    for(var i in datasubmenu){
        for(var z in datasubmenu[i]){
            if(dataIndex[i+'Index']>-1 && dataIndex[z+'Index']>-1 && dataIndex[z+'LinkHide']>-1 && submenuIndex==z){
                menuStateData[dataIndex[i+'Index']]['submenus'][dataIndex[z+'Index']]['subSubMenu'][dataIndex[z+'LinkHide']]['linkOnlyHide']=false;
                menuStateData[dataIndex[i+'Index']]['submenus'][dataIndex[z+'Index']]['subSubMenu'][dataIndex[z+'LinkHide']]['name']=nameData;
            }
        }
    }

    return menuStateData;
}


  componentWillReceiveProps(nextProps){
    let linkShowParticipant = ['participant_details','participant_ability','participant_shift','participant_funding_details'];
    if(this.props.participaintDetails.id !=nextProps.participaintDetails.id || this.props.staffDetails.id !=nextProps.staffDetails.id ){
      let dataIndex = this.getObjectValue(this);
      this.setState({replaceData:{':id':nextProps.participaintDetails.id,':staffId':nextProps.staffDetails.id}},()=>{
        let menuState = this.state.menus;
            menuState = this.allSubSubMenuLinkHide(menuState,dataIndex)
            let obj = menuState.find(x => x.id === 'crm_participant_details');
            let objIndex = menuState.indexOf(obj);
            if(objIndex>-1 && nextProps.getSidebarMenuShow.subMenuShow){
              if(linkShowParticipant.indexOf(nextProps.showTypePage)>-1){
                menuState[objIndex]['linkShow'] = true;
                menuState[objIndex]['name'] = nextProps.participaintDetails.FullName;
              }else if(nextProps.showTypePage == 'user_staff_members_details'){
                menuState = this.showSubSubMenuLinkSpecific(menuState,dataIndex,'staff_member','Staff Details -'+ nextProps.staffDetails.id)
              }else{
                menuState[objIndex]['linkShow'] = false;
              }
            }
            this.setState({menus:menuState});
        });
    }

    if(this.props.showTypePage!=nextProps.showTypePage){
        let menuState = this.state.menus;
        let dataIndex = this.getObjectValue(this);
        menuState = this.allSubSubMenuLinkHide(menuState,dataIndex)
        let obj = menuState.find(x => x.id === 'crm_participant_details');
        let objIndex = menuState.indexOf(obj);
        if(objIndex>-1 && nextProps.getSidebarMenuShow.subMenuShow){
          if(linkShowParticipant.indexOf(nextProps.showTypePage)>-1){
            menuState[objIndex]['linkShow'] = true;
            menuState[objIndex]['name'] = nextProps.participaintDetails.FullName;
          }else if(nextProps.showTypePage == 'user_staff_members_details'){
            menuState = this.showSubSubMenuLinkSpecific(menuState,dataIndex,'staff_member','Staff Details -'+ nextProps.staffDetails.id)
          }else{
            menuState[objIndex]['linkShow'] = false;
          }
        }
        this.setState({menus:menuState});
    }

}

  render() {

    return (
      <section className='asideSect__ Crm' style={{background:'none'}}>
        <Sidebar
          heading={'CRM'}
          menus={this.state.menus}
          subMenuShowStatus={this.state.subMenuShowStatus}
          replacePropsData={this.state.replaceData}
        />
        <div className='bodyNormal Crm' >
          <div className={this.state.loadState ? 'Rloader' : ''}></div>

          <Switch>
          {  // <Route exact path={ROUTER_PATH + 'admin/crm/dashboard'} render={(props) => <Dashboard props={props} />} />
          }
            <Route exact path={ROUTER_PATH + 'admin/crm/participantadmin'} render={(props) => <DashboardAdmin props={props} />} />
            <Route exact path={ROUTER_PATH + 'admin/crm/createParticipant'} render={(props) => <NewParticipant props={props} />} />
            <Route exact path={ROUTER_PATH + 'admin/crm/participantuser'} render={(props) => <DashboardUser props={props} />} />
            <Route exact path={ROUTER_PATH + 'admin/crm/participantdetails/:id'} render={(props) => <ParticipantDetails props={props} />} />
            <Route exact path={ROUTER_PATH + 'admin/crm/prospectiveparticipants'} render={(props) => <ProspectiveParticipants props={props} />} />
            <Route exact path={ROUTER_PATH + 'admin/crm/participantability/:id'} render={(props) => <Participantability props={props} />} />
            <Route exact path={ROUTER_PATH + 'admin/crm/reporting'} render={(props) => <Reporting props={props} />} />
            <Route exact path={ROUTER_PATH + 'admin/crm/schedules'} render={(props) => <Schedules props={props} />} />
            <Route exact path={ROUTER_PATH + 'admin/crm/fundingdetails/:id'} render={(props) => <FundingDetails props={props} />} />
            <Route exact path={ROUTER_PATH + 'admin/crm/tasks'} render={(props) => <Tasks props={props} />} />
            <Route exact path={ROUTER_PATH + 'admin/crm/shifts/:id'} render={(props) => <Shifts props={props} />} />
            <Route exact path={ROUTER_PATH + 'admin/crm/locationanalytics'} render={(props) => <LocationAnalytics props={props} />} />
            <Route exact path={ROUTER_PATH + 'admin/crm/usermangement'} render={(props) => <UserMangement props={props} />} />
            <Route exact path={ROUTER_PATH + 'admin/crm/departments'} render={(props) => <Departments props={props} />} />
            <Route exact path={ROUTER_PATH + 'admin/crm/staffdetails/:id'} render={(props) => <StaffDetails props={props} />} />
            <Route exact path={ROUTER_PATH + 'admin/crm/serviceanalytics'} render={(props) => <ServiceAnalytics props={props} />} />
            <Route exact path={ROUTER_PATH + 'admin/crm/prospectivefunding'} render={(props) => <ProspectiveParticipantFunding props={props} />} />
          </Switch>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  getSidebarMenuShow: state.sidebarData,
  showTypePage: state.DepartmentReducer.activePage.pageType,
  participaintDetails: state.DepartmentReducer.participaint_details,
  staffDetails: state.DepartmentReducer.staff_details,

})

const mapDispatchtoProps = (dispach) => {
  return {
    setFooterColor: (result) => dispach(setFooterColor(result)),
    setSubmenuShow: (result) => dispach(setSubmenuShow(result))
  }
}

const AppCrmData = connect(mapStateToProps, mapDispatchtoProps)(AppCrm)
export { AppCrmData as AppCrm };
