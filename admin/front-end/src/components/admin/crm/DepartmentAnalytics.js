import React, { Component } from 'react';
import Modal from 'react-bootstrap/lib/Modal';

export const DepartmentAnalytics = (props) => {
    return (
        <Modal className="modal fade Crm" bsSize="large" show={props.showModal} onHide={() => props.handleClose} >
            <form id="special_agreement_form" method="post" autoComplete="off">
                <Modal.Body className="px-0 py-0">

                    <div className="custom-modal-header bb-1">
                        <div className="Modal_title">Department Analytics</div>
                        <i className="icon icon-close3-ie Modal_close_i" onClick={() => props.handleClose()}></i>
                    </div>


                    <div className="custom-modal-body w-100 mx-auto px-5 pb-5">

                        <div className='row pd_lr_30'>
                            <div className='col-sm-12'>
                                <h3 >Department <strong>NDIS Intake</strong></h3>
                            </div>
                        </div>

                        <div className='row pd_lr_30 d-flex justify-content-center mr_tb_20'>

                            <div className='col-sm-6'>
                                <div className='Analytics_box1__'>
                                    <h5 className='cmn_font_crm'><strong>Tasks Completed</strong></h5>
                                    <ul className='durationtk_ul__'>
                                    <li className="active">
                                            <span>Week</span>
                                                </li>
                                                <li>
                                                    <span>Month</span>
                                                </li>
                                                <li>
                                                    <span>Year</span>
                                                </li>
                                    </ul>
                                    <h6>Total Tasks: <strong>71 Tasks</strong></h6>
                                </div>
                            </div>

                            <div className='col-sm-6'>
                                <div className='Analytics_box1__'>
                                    <h5 className='cmn_font_crm'><strong>Department's Participant Intake</strong></h5>
                                    <ul className='durationtk_ul__'>
                                                <li className="active">
                                                    <span>Week</span>
                                                </li>
                                                <li>
                                                    <span>Month</span>
                                                </li>
                                                <li>
                                                    <span>Year</span>
                                                </li>
                                    </ul>
                                    <div className='showCase_amnt'>
                                        <h1><strong>27</strong></h1>
                                        <h6>Total Amount of Participants fully Onboarded</h6>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>

                    {/* <div className="custom-modal-footer bt-1 mt-5">
                        <div className="row d-flex justify-content-end">
                            <div className="col-md-3"><a className="btn-1">Save and Lock Away Funds</a></div>
                        </div>
                    </div> */}


                </Modal.Body>
            </form>
        </Modal>
    );

}