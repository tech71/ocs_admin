import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ProgressBar, Tabs, Tab } from 'react-bootstrap';
import { ROUTER_PATH } from '../../../config.js';
import { PartShiftModal } from './ParticipantShiftModal';



class Shifts extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            participant_id: '',
            PartShiftModal: false
        };
    }
    componentDidMount() {
        this.setState({ participant_id: this.props.props.match.params.id });
    }

    closePartShiftModal() {
        this.setState({ PartShiftModal: false });
    }

    showPartShiftModal() {
        this.setState({ PartShiftModal: true });
    }

    render() {
        const now = 60;
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1">
                        <div className="back_arrow py-4 bb-1">
                            <Link to={ROUTER_PATH + 'admin/crm/participantadmin'}><span className="icon icon-back1-ie"></span></Link>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1">
                        <div className="row d-flex py-4">
                            <div className="col-md-9">
                                <div className="h-h1">
                                    Reporting
                                    </div>
                            </div>
                        </div>
                        <div className="row"><div className="col-md-12"><div className="bt-1"></div></div></div>
                    </div>
                </div>
             
                <div className="row mt-5">
                  

                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                        <h3 className="mb-2">Service Funding Analytics</h3>
                        <div className="row mx-0 Ser_Anal_a1">
                            <div className="col-md-5 Ser_Anal_aa text-center">
                                <img src="/assets/images/ServiceAnalytics.png" />
                            </div>
                            <div className="col-md-7 Ser_Anal_ab">
                                <div className="w-100">
                                    <div className="Ser_Anal_aba Oncall_P">
                                        <div className="Ser_Anal_aba-1"><i className="icon icon-circle"><span></span></i></div>
                                        <div className="Ser_Anal_aba-2">
                                            <h4>ONCALL Portal Managed Funding</h4>
                                            <div><em>253</em> People Onboarded with<br />
                                                ONCALL managed Funding</div>
                                        </div>
                                    </div>

                                    <div className="Ser_Anal_aba thread_P">
                                        <div className="Ser_Anal_aba-1"><i className="icon icon-circle"><span></span></i></div>
                                        <div className="Ser_Anal_aba-2">
                                            <h4> 3rd Party Managed Funding</h4>
                                            <div><em>85</em> People Onboarded with<br />
                                                ONCALL managed Funding</div>
                                        </div>
                                    </div>

                                    <div className="Ser_Anal_aba Private_M">
                                        <div className="Ser_Anal_aba-1"><i className="icon icon-circle"><span></span></i></div>
                                        <div className="Ser_Anal_aba-2">
                                            <h4>Private Managed Funding</h4>
                                            <div><em>22</em> People Onboarded with<br />
                                                ONCALL managed Funding</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-100">
                                    <a className="btn-1 col-md-5 pull-right"> Export Area Data Report</a>
                                </div>
                            </div>
                        </div>

                        <div className="row mt-5">
                            <h3 className="pb-3 bt-1 pt-5">Service Funding Demographic Analytics</h3>
                            <ul className="nav nav-tabs Ser_Anal_tab_1">
                                <li className="col-md-4 Oncall_P active"><a data-toggle="tab" href="#home">ONCALL Participants</a></li>
                                <li className="col-md-4 thread_P"><a data-toggle="tab" href="#menu1">3rd Party Participants</a></li>
                                <li className="col-md-4 Private_M"><a data-toggle="tab" href="#menu2">Private Participants</a></li>
                            </ul>

                            <div className="tab-content">
                                <div id="home" className="tab-pane Oncall_P in active">
                                    <MsgGraph/>
                                </div>

                                <div id="menu1" className="tab-pane thread_P">
                                    <MsgGraph/>
                                </div>


                                <div id="menu2" className="tab-pane Private_M">
                                 <MsgGraph/>
                                </div>
                            </div>
                        </div>



                    </div>

                </div>

            </div>

        );
    }
}

const MsgGraph = props => {
    return (
    <div className="DG_a1 mt-5">

                                    <div className="row DG_aa">
                                        <div className="col-md-5 DG_aa-1 br-1">
                                            <h4 className="mb-3"> Gender Demographic Analysis:</h4>
                                            <div className="px-5">
                                                <div className="DG-aa-2a">
                                                    Of the 50% of Participants that are
                                                        ONCALL Managed
                                                    </div>
                                            </div>
                                        </div>
                                        <div className="col-md-7 DG-aa-2">
                                            <h4 className="mb-3"> Area Demographic Analysis:</h4>
                                            <div className="row">
                                                <div className="col-md-8 px-5">
                                                    <div className="DG-aa-2a">
                                                        Hot spots of Victoria, ONCALL Managed
                                                        Participants.
                                                    </div>
                                                </div>
                                                <div className="col-md-4 DG-aa-3">
                                                    Top 5 Hot Spots for VIC:
                                                    <div className="DG-aa-3a">
                                                        <span className="DG-aa-3b"><span>1</span></span>
                                                        <div className="DG-aa-3c"><span>Melbourne (City)</span>
                                                            - 147 Participants</div>
                                                    </div>
                                                    <div className="DG-aa-3a">
                                                        <span className="DG-aa-3b"><span>2</span></span>
                                                        <div className="DG-aa-3c"> <span>Geelong (City)</span>
                                                            -41 Participants</div>
                                                    </div>
                                                    <div className="DG-aa-3a">
                                                        <span className="DG-aa-3b"><span>3</span></span>
                                                        <div className="DG-aa-3c"> <span>Apollo Bay (Area)</span>
                                                            -23 Participants</div>
                                                    </div>
                                                    <div className="DG-aa-3a">
                                                        <span className="DG-aa-3b"><span>4</span></span>
                                                        <div className="DG-aa-3c"> <span>Mildura (Area)</span>
                                                            -23 Participants</div>
                                                    </div>
                                                    <div className="DG-aa-3a">
                                                        <span className="DG-aa-3b"><span>5</span></span>
                                                        <div className="DG-aa-3c"> <span>Sale (Area)</span>
                                                            -8 Participants</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <a className="btn-1 col-md-3 pull-right"> Export Demographic Data</a>
                                    </div>
                                </div>
                                );

 }

export default Shifts;
