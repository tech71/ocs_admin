import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import 'react-table/react-table.css';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { ROUTER_PATH } from '../../../config.js';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import Toolbar from 'react-big-calendar/lib/Toolbar';
import ReactTable from "react-table";
import { checkItsNotLoggedIn, postData, getQueryStringValue } from '../../../service/common.js';
import { connect } from 'react-redux';
import CrmPage from './CrmPage';


const getScheduleList = () => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = JSON.stringify({});
        postData('crm/CrmSchedule/schedules_calendar_data', Request).then((result) => {
            let filteredData = result;
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            setTimeout(() => resolve(res), 10);
        });
    });
};


BigCalendar.momentLocalizer(moment); // or globalizeLocalizer
const localizer = BigCalendar.momentLocalizer(moment)




class Schedules extends Component {
    constructor(props, context) {
        super(props, context);

        this.handleSelect = this.handleSelect.bind(this);

        this.state = {
            key: 1, filterVal: '',dueTask:[],subTaskData:[],date:''
        };
    }

    handleSelect(key) {
        this.setState({ key });
    }
    componentWillMount(){
      getScheduleList().then(res => {
        this.setState({
              dueTask: res.rows,
              pages: res.pages,
              loading: false
          });
      });

    }
    showModal = () => {
        this.setState({ showModal: true })

    }
    closeModal = () => {
        this.setState({ showModal: false,subTaskData:[] })
    }
    openSortShiftDetails = (event) => {
        this.setState({selelctShiftId :event.id,date:event.start})
        var Request =({"date":moment(event.start).format("YYYY-MM-DD"),"type":event.type});
        postData('crm/CrmTask/list_sub_task', Request).then((result) => {
          if(result.status){
            let subTaskData = result.data;
            this.setState({subTaskData:subTaskData});
          }


        })
          this.showModal();
    }
    onSlotChange =(event)=>{
      this.showModal();
      this.setState({date:event.start});
      var Request =({"date":moment(event.start).format("YYYY-MM-DD"),"type":event.type});
      postData('crm/CrmTask/list_sub_task', Request).then((result) => {
        if(result.status){
          let subTaskData = result.data;
          this.setState({subTaskData:subTaskData});
        }


      })
    }
    EventList =()=>{
    let   eventList = [];
      if(typeof this.state.dueTask !== 'undefined' && this.state.dueTask.length > 0){
        this.state.dueTask.map((key,i)=>(
          eventList.push({
            'title':this.state.dueTask[i].title,
            'start': new Date(this.state.dueTask[i].start),
            'end': new Date(this.state.dueTask[i].end),
            'type': this.state.dueTask[i].type
          })
        ))
      }
      return eventList;
    }

    render() {
        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];


        const task = [
            {
                taskname: 'Sub-Task 1',
                assigned: 'Jimmy Arms',
                duedate: '01/01/01',
                status: 'Not Completed'
            },
            {
                taskname: 'Sub-Task 1',
                assigned: 'Jimmy Arms',
                duedate: '01/01/01',
                status: 'Not Completed'
            },
            {
                taskname: 'Sub-Task 1',
                assigned: 'Jimmy Arms',
                duedate: '01/01/01',
                status: 'Not Completed'
            }
        ]



        return (
            <div className="container-fluid">
            <CrmPage pageTypeParms={'schedule_user_schedule'}/>
                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1">
                        <div className="back_arrow py-4 bb-1">
                            <Link to={ROUTER_PATH + 'admin/crm/participantadmin'}><span className="icon icon-back1-ie"></span></Link>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1">
                        <div className="row d-flex py-4">
                            <div className="col-md-9">
                                <div className="h-h1">
                                    {this.props.showPageTitle}
                                </div>
                            </div>
                        </div>
                        <div className="row"><div className="col-md-12"><div className="bt-1"></div></div></div>
                    </div>
                </div>
          

                <div className="row mt-5">
                  
                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                        <div className="row d-flex">
                            <div className="col-md-9">
                                <div className="big-search l-search">
                                    <input />
                                    <button><span className="icon icon-search1-ie"></span></button>
                                </div>
                            </div>
                            <div className="col-md-3 d-inline-flex align-self-center">
                                <div className="s-def1 w-100">
                                    <Select
                                        name="view_by_status"
                                        options={options}
                                        required={true}
                                        simpleValue={true}
                                        searchable={false}
                                        clearable={false}
                                        placeholder="Filter by: Unread"
                                        onChange={(e) => this.setState({ filterVal: e })}
                                        value={this.state.filterVal}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12 mt-4">
                                <div className="Schedule_calendar">
                                    <BigCalendar
                                        localizer={localizer}
                                        view='month'
                                        views={['month']}
                                        events={this.EventList()}
                                        onSelectEvent={(event) => this.openSortShiftDetails(event)}
                                        components={{ toolbar: CalendarToolbar }}
                                        startAccessor="start"
                                        endAccessor="end"
                                    />

<div className={this.state.showModal ? 'customModal show' : 'customModal'}>
                        <div className="custom-modal-dialog Information_modal task_modal">
                          {/* Modal content Start */}
                          <div className="text text-left">{moment(this.state.date).format("dddd, MMMM Do YYYY")}
                              <a onClick={this.closeModal} className="close_i pull-right mt-1"><i className="icon icon-cross-icons"></i></a>
                          </div>
                          <div className="re-table">
                        <div className="col-md-12 task_table">

                            <ReactTable
                                columns={[

                                    { Header: "Sub-Task Name:", accessor: "taskname", },
                                    { Header: "Assigned To:", accessor: "staff", },
                                    { Header: "Due Date:", accessor: "duedate", },
                                    { Header: "Status:", accessor: "status", },
                                    {
                                        Header: "Actions:", accessor: "action", headerStyle: { border: "0px solid #fff" },

                                        Cell: ({ original }) => {
                                            return (
                                                <span className='icon_table1 justify-content-between'>
                                                    <span className="add_tab_i"><span className="icon icon-add1-ie"></span></span>
                                                    <span className="unat_tab_i"><span className="icon icon-attatchments3-ie"></span></span>
                                                    <span className="Arch_tab_i"><span className="icon icon-archive1-ie"></span></span>
                                                </span>
                                            );
                                        },


                                    }
                                ]}

                                defaultPageSize={3}
                                data={this.state.subTaskData}
                                pageSize={this.state.subTaskData.length}
                                showPagination={false}
                            />
                        </div>
                        </div>
                        {/* Modal content end */}
                      </div>
                  </div>

                                </div>
                            </div>

                            <div className="col-md-12 d-flex Shift_times_div my-4">
                                <div><span className="am_shift"></span>Upcoming Tasks</div>
                                <div><span className="pm_shift"></span>Selected Task</div>
                                {/* <div><span className="so_shift"></span>S/O (10pm - 6am, non active)</div> */}
                                <div><span className="na_shift"></span>Completed Tasks</div>
                            </div>

                        </div>



                    </div>

                </div>
            </div>
        );
    }
}
const mapStateToProps = state => {
      return {
        showPageTitle: state.DepartmentReducer.activePage.pageTitle,
        showTypePage: state.DepartmentReducer.activePage.pageType,
  
      }
  };

export default connect(mapStateToProps)(Schedules);

class CalendarToolbar extends Toolbar {
    render() {
        return (
            <div>
                <div className="rbc-btn-group">
                <span className="" onClick={() => this.navigate('TODAY')} >Today</span>
                    <span className="icon icon-arrow-left" onClick={() => this.navigate('PREV')}></span>
                    <span className="icon icon-arrow-right" onClick={() => this.navigate('NEXT')}></span>
                </div>
                <div className="rbc-toolbar-label">{this.props.label}</div>
            </div>
        );
    }
}
