import React from 'react';
import Select from 'react-select-plus';
import {AssistanceCheckbox,MobilityCheckbox ,LanguageCheckbox,CognitiveLevel} from  '../../../dropdown/CrmDropdown.js';

export const ParticipantAbilityPopup = (props) => {

    return (
                <React.Fragment>
                <form id="partcipant_ability">
<div className="row mx-0 my-4">
        <div className="col-md-3">
            <label className="title_input pl-0">Participant Cognitive Level:</label>
            <div className="required">
                <div className="s-def1 s1">
                {props.errorTooltip('cognitive_level', 'Select Cognitive Level','participantAbility')}
                <Select
            
                  name="cognitive_level" options={CognitiveLevel(0)}  required={true} simpleValue={true}
                  searchable={false} clearable={false}
                  onChange={(e) => props.updateSelect(e,'cognitive_level','participantAbility')}
                  value={props.sts.cognitive_level}
                />
                      {//   <Select
                      //   name="cognitive_level" options={CognitiveLevel} required={true} simpleValue={true}
                      //   data-rule-required="true"
                      //   searchable={false} clearable={false} placeholder="Please Select" onChange={(e) => props.updateSelect(e,'cognitive_level','participantAbility')}
                      //    value={props.sts.cognitive_level}
                      // />
                    }
                </div>
            </div>
        </div>

        <div className="col-md-3">
            <label className="title_input pl-0">Communication:</label>
            {props.errorTooltip('communication', 'Select Communication','participantAbility')}
            <div className="required">
                <div className="s-def1 s1">
                        <Select
                        name="communication" options={CognitiveLevel(0)} required={true} simpleValue={true}
                        searchable={false} clearable={false} placeholder="Please Select"
                        onChange={(e) => props.updateSelect(e,'communication','participantAbility')} value={props.sts.communication}
                      />
                </div>
            </div>
        </div>

        <div className="col-md-6">
            <label className="title_input pl-0">Hearing impaired interpreter required</label>
            <div className="row pl-5">
                <div className="col-md-3">
                    <label className="radio_F1">Yes
                     <input type="radio" name="hearing_interpreter" value={1} onChange={(e) =>props.handleChanges(e,'participantAbility')} checked={(props.sts.hearing_interpreter)==1?true:false}  />
                        <span className="checkround"></span>
                    </label>
                </div>
            </div>
            <div className="row pl-5">
                <div className="col-md-3">
                    <label className="radio_F1">No
                      <input type="radio" name="hearing_interpreter" value={0} onChange={(e) =>props.handleChanges(e,'participantAbility')} checked={(props.sts.hearing_interpreter)==0?true:false}  />
                        <span className="checkround"></span>
                    </label>
                </div>
            </div>
        </div>

    </div>



    <div className="row mb-5">
        <div className="col-md-4">
            <label className="title_input pl-0">Participant Mobility Requirements: </label>
            <div className="Scroll_div_parents">
                <div className="Scroll_div">
                    <div className="row">
                           <span>
                               {


                                   MobilityCheckbox(props.sts.require_mobility).map((value, idxx) => (
                                      <span key={idxx}>
                                        <div className="col-md-12 mb-2">
                                          <label className="c-custom-checkbox CH_010">
                                              <input type="checkbox" className="checkbox1" id={value.value} name="require_mobility" value={value.value }checked={value.checked} onChange={(e)=>props.checkboxHandler(e,'participantAbility')}  />
                                              <i className="c-custom-checkbox__img"></i>
                                              <div>{value.label}</div>
                                          </label>
                                        </div>
                                      </span>
                                  ))
                                }
                                {
                                  // <div className="col-md-12 mb-2">
                                // <label className="c-custom-checkbox CH_010">
                                   // <input type="checkbox" className="checkbox1" />
                                   // <i className="c-custom-checkbox__img"></i>
                                   // <div>Shower Chair</div>
                             //   </label>
                             // </div>
                             // <div className="col-md-12 mb-2">
                             //    <input className="add_input_b1__ w-85"></input>
                             // </div>
                           }
                           </span>
                    </div>
                </div>
            </div>
        </div>
        <div className="col-md-6">
            <label className="title_input pl-0">Participant Assistance Requirements: </label>
            <div className="Scroll_div_parents">
                <div className="Scroll_div">
                    <div className="row">
                         <span>
                              {  // <div className="col-md-6 mb-2">
                                  // <label className="c-custom-checkbox CH_010">

                                      // <input type="checkbox" className="checkbox1" />
                                      // <i className="c-custom-checkbox__img"></i>
                                      // <div>Independent</div>

                                  // </label>
                                // </div>
                              }
                                <div className="col-md-6 mb-2">
                                {

                                    AssistanceCheckbox(props.sts.require_assistance).map((value, idxx) => (
                                       <span key={idxx}>
                                         <div className="col-md-6 mb-2">
                                           <label className="c-custom-checkbox CH_010">
                                               <input type="checkbox" className="checkbox1" id={value.value} name="require_assistance" value={value.value }checked={value.checked} onChange={(e)=>props.checkboxHandler(e,'participantAbility')} />
                                               <i className="c-custom-checkbox__img"></i>
                                               <div>{value.label}</div>
                                           </label>
                                         </div>
                                       </span>
                                   ))
                                 }
                                  {// <label className="c-custom-checkbox CH_010">
                                  //     <input type="checkbox" className="checkbox1" />
                                  //     <i className="c-custom-checkbox__img"></i>
                                  //     <div>Assist</div>
                                  // </label>
                                }
                                </div>
                             {//    <div className="col-md-6 mb-2">
                             //    <input className="add_input_b1__ w-85"></input>
                             // </div>
                           }
                              </span>
                    </div>
                </div>
            </div>
        </div>


    </div>



    <div className="row mx-0 py-4">
        <div className="col-md-3">
            <label className="title_input pl-0">Is the participant of<br /> culturally and linguistically<br /> diverse background?: </label>
            <div className="row pl-5">
                <div className="col-md-6">
                    <label className="radio_F1">Yes
                    <input type="radio" name="linguistic_diverse" value={1} onChange={(e) =>props.handleChanges(e,'participantAbility')} checked={(props.sts.linguistic_diverse)==1?true:false}  />
                      <span className="checkround"></span>
                    </label>
                </div>
            </div>
            <div className="row pl-5">
                <div className="col-md-6">
                    <label className="radio_F1">No
                    <input type="radio" name="linguistic_diverse" value={0} onChange={(e) =>props.handleChanges(e,'participantAbility')} checked={(props.sts.linguistic_diverse)==0?true:false}  />
                      <span className="checkround"></span>
                    </label>
                </div>
            </div>
        </div>

        <div className="col-md-3">
            <label className="title_input pl-0 my-4">Language interpreter required:</label>
            <div className="row pl-5">
                <div className="col-md-6">
                    <label className="radio_F1">Yes
                    <input type="radio" name="language_interpreter" value={1} onChange={(e) =>props.handleChanges(e,'participantAbility')} checked={(props.sts.language_interpreter)==1?true:false}  />
                       <span className="checkround"></span>
                    </label>
                </div>
            </div>
            <div className="row pl-5">
                <div className="col-md-6">
                    <label className="radio_F1">No
                     <input type="radio" name="language_interpreter" value={0} onChange={(e) =>props.handleChanges(e,'participantAbility')} checked={(props.sts.language_interpreter)==0?true:false}  />
                        <span className="checkround"></span>
                    </label>
                </div>
            </div>
        </div>



        <div className="col-md-6">
            <label className="title_input pl-0">Participant Assistance Requirements: </label>
            <div className="Scroll_div_parents">
                <div className="Scroll_div">
                    <div className="row">
                    <span>
                    {


                        LanguageCheckbox(props.sts.languages_spoken).map((value, idxx) => (
                           <span key={idxx}>
                             <div className="col-md-6 mb-2">
                               <label className="c-custom-checkbox CH_010">
                                   <input type="checkbox" className="checkbox1" id={value.value} name="languages_spoken" value={value.value }checked={value.checked} onChange={(e)=>props.checkboxHandler(e,'participantAbility')} />
                                   <i className="c-custom-checkbox__img"></i>
                                   <div>{value.label}</div>
                               </label>
                             </div>
                           </span>
                       ))
                     }
                           </span>
                    </div>
                </div>

            </div>
        </div>

    </div>


    <div className="row bt-1 pt-5">
        <h3>Participant Disability</h3>
        <div className="col-md-8 mt-5">
            <label className="title_input pl-0">Fomal Diagnosis (Primary): </label>
            <div className='w-100'>
                <textarea className="int_textarea w-100 textarea-max-size"  name="primary_fomal_diagnosis_desc" onChange={(e) =>props.handleChanges(e,'participantAbility')}></textarea>
            </div>
        </div>
        <div className="col-md-8  mt-5">
            <label className="title_input pl-0">Fomal Diagnosis (Secondary): </label>
            <div className='w-100'>
                <textarea className="int_textarea w-100 textarea-max-size" name="secondary_fomal_diagnosis_desc" onChange={(e) =>props.handleChanges(e,'participantAbility')}></textarea>
            </div>
        </div>
        <div className="col-md-8  mt-5">
            <label className="title_input pl-0">Other Relevant Conformation</label>
            <div className='w-100'>
                <textarea className="int_textarea w-100 textarea-max-size" name="other_relevant_conformation" onChange={(e) =>props.handleChanges(e,'participantAbility')}></textarea>
            </div>
        </div>
    </div>

    <div className="row py-5">
        {/* <div className="col-md-6">
            <h3>Relevant Attachments:</h3>
            <div className="row d-flex flex-wrap align-items-center">
                <div className="col-md-8">
                    <ul className="file_down quali_width P_15_TB">
                        <li className="w-50 br_das">
                            <div className="text-right file_D1"><i className="icon icon-close3-ie color"></i></div>
                            <div className="path_file mt-0 mb-4"><b>Hearing Imparement</b></div>
                            <span className="icon icon-file-icons d-block"></span>
                            <div className="path_file">lgHearingHearing2.png</div>
                        </li>
                        <li className="w-50 br_das">
                        <div className="text-right file_D1"><i className="icon icon-close3-ie color"></i></div>
                            <div className="path_file mt-0 mb-4"><b>Formal Diagnosis Doc</b></div>
                            <span className="icon icon-file-icons d-block"></span>
                            <div className="path_file">lgHlgHearing2earing2.png</div>
                        </li>
                    </ul>
                </div>
                    <div className="col-md-4">
                        <a className="v-c-btn1">
                            <span>Browser</span> <i className="icon icon-export1-ie"></i>
                        </a>
                    </div>

                </div>
            </div> */}
            <div className="col-md-6">
                <label className="title_input pl-0">Are there any legal issues that may affect services? (eg. apprehended violence order):  </label>
                <div className="row pl-5">
                    <div className="col-md-3">
                        <label className="radio_F1">Yes
                        <input type="radio" name="legal_issues" value={1} onChange={(e) =>props.handleChanges(e,'participantAbility')} checked={(props.sts.legal_issues)==1?true:false}  />
                          <span className="checkround"></span>
                        </label>
                    </div>
                </div>
                <div className="row pl-5">
                    <div className="col-md-3">
                        <label className="radio_F1">No
                            <input type="radio" name="legal_issues" value={0} onChange={(e) =>props.handleChanges(e,'participantAbility')} checked={(props.sts.legal_issues)==0?true:false}  />
                            <span className="checkround"></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div className="row d-flex justify-content-end">
          <div className="col-md-3"><a className="btn-1" onClick={props.submitParticipantAbility}>Save And Changes</a></div>
        </div>
        </form>
    </React.Fragment>

    );
}
