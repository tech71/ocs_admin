import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import 'react-table/react-table.css';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import ReactResponsiveSelect from 'react-responsive-select';
import { confirmAlert, createElementReconfirm } from 'react-confirm-alert';
import { Doughnut } from 'react-chartjs-2';
import { checkItsNotLoggedIn, getJwtToken, postData, reFreashReactTable } from '../../../service/common.js';
import BigCalendar from 'react-big-calendar';
import { ToastContainer, toast } from 'react-toastify';
import moment from 'moment';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import Toolbar from 'react-big-calendar/lib/Toolbar';
import { ROUTER_PATH } from '../../../config.js';
import CrmPage from './CrmPage';
import { ToastUndo } from 'service/ToastUndo.js'
import { connect } from 'react-redux';
import { CounterShowOnBox } from 'service/CounterShowOnBox.js';
import {DisableStaff} from './UserMangement';
const getScheduleList = () => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = JSON.stringify({});
        postData('crm/CrmSchedule/schedules_calendar_data', Request).then((result) => {
            let filteredData = result;
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            setTimeout(() => resolve(res), 10);
        });
    });
};
BigCalendar.momentLocalizer(moment); // or globalizeLocalizer
const localizer = BigCalendar.momentLocalizer(moment)


const multiSelectOptionMarkup = (text) => (
    <div>
        <span className="rrs_select"> {text}</span>
        <span className="checkbox">
            <i className="icon icon-star2-ie"></i>
        </span>

    </div>
);

// By default no caret icon is supplied - any valid jsx markup will do
const caretIcon = (
    <i className="icon icon-edit1-ie"></i>
);

class StaffDetails extends Component {
    constructor(props, context) {
        super(props, context);
        this.participantDetailsRef = React.createRef();
        this.handleSelect = this.handleSelect.bind(this);

        this.state = {
            key: 1, filterVal: '',
            showModal: false, details: [], crmparticipantCount: 0,
            view_type: 'month',
            grapheachdata: '', dueTask: []
        };
    }
    handleSelect(key) {
        this.setState({ key });
    }
    componentWillMount() {
        getScheduleList().then(res => {
            this.setState({
                dueTask: res.rows,
                pages: res.pages,
                loading: false
            });
        });

    }
    EnableRecruiter=(id)=> {
          this.setState({
              staff_disable: {
                  staff_id: id,
              }
          });
          let msg = <span>Are you sure you want to enable this account?</span>;
          return new Promise((resolve, reject) => {
              confirmAlert({
                  customUI: ({ onClose }) => {
                      return (
                          <div className='custom-ui'>
                              <div className="confi_header_div">
                                  <h3>Confirmation</h3>
                                  <span className="icon icon-cross-icons" onClick={() => {
                                      onClose();
                                      resolve({ status: false })
                                  }}></span>
                              </div>
                              <p>{
                                  msg}</p>
                              <div className="confi_but_div">
                                  <button className="Confirm_btn_Conf" onClick={
                                      () => {
                                          postData('crm/CrmStaff/enable_crm_user', id).then((result) => {
                                              if (result) {
                                                  toast.success(<ToastUndo message='Enabled successfully'  showType={'s'} />, {
                                                      // toast.success("Note Deleted successfully", {
                                                      position: toast.POSITION.TOP_CENTER,
                                                      hideProgressBar: true
                                                  });
                                                  onClose();
                                                  this.setState({ success: true })
                                              } else {
                                                  toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                                                      // toast.error(result.error, {
                                                      position: toast.POSITION.TOP_CENTER,
                                                      hideProgressBar: true
                                                  });

                                              }

                                          })
                                      }}>Confirm</button>
                                  <button className="Cancel_btn_Conf" onClick={
                                      () => {
                                          onClose();
                                          resolve({ status: false });
                                      }}> Cancel</button>
                              </div>
                          </div>
                      )
                  }
              })
          });
      }
    archiveTask = (id) => {
        let msg = <span>Are you sure you want to archive this item? <br /> Once archived, this action can not be undone.</span>;
        return new Promise((resolve, reject) => {
            confirmAlert({
                customUI: ({ onClose }) => {
                    return (
                        <div className='custom-ui'>
                            <div className="confi_header_div">
                                <h3>Confirmation</h3>
                                <span className="icon icon-cross-icons" onClick={() => {
                                    onClose();
                                    resolve({ status: false })
                                }}></span>
                            </div>
                            <p>{
                                msg}</p>
                            <div className="confi_but_div">
                                <button className="Confirm_btn_Conf" onClick={
                                    () => {
                                        postData('crm/CrmTask/archive_task', { task_id: id }).then((result) => {
                                            if (result) {
                                                toast.success(<ToastUndo message={'Task Archived successfully'} showType={'s'} />, {
                                                    // toast.success("Note Deleted successfully", {
                                                    position: toast.POSITION.TOP_CENTER,
                                                    hideProgressBar: true
                                                });
                                                onClose();
                                                this.setState({ success: true })
                                            } else {
                                                toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                                                    // toast.error(result.error, {
                                                    position: toast.POSITION.TOP_CENTER,
                                                    hideProgressBar: true
                                                });

                                            }

                                        })
                                    }}>Confirm</button>
                                <button className="Cancel_btn_Conf" onClick={
                                    () => {
                                        onClose();
                                        resolve({ status: false });
                                    }}> Cancel</button>
                            </div>
                        </div>
                    )
                }
            })
        });

    }
    completeTask = (id) => {
        let msg = <span>Are you sure you want to complete this task? <br /> Once completed, this action can not be undone.</span>;
        return new Promise((resolve, reject) => {
            confirmAlert({
                customUI: ({ onClose }) => {
                    return (
                        <div className='custom-ui'>
                            <div className="confi_header_div">
                                <h3>Confirmation</h3>
                                <span className="icon icon-cross-icons" onClick={() => {
                                    onClose();
                                    resolve({ status: false })
                                }}></span>
                            </div>
                            <p>{
                                msg}</p>
                            <div className="confi_but_div">
                                <button className="Confirm_btn_Conf" onClick={
                                    () => {
                                        postData('crm/CrmTask/complete_task', { task_id: id }).then((result) => {
                                            if (result) {
                                                toast.success(<ToastUndo message={'Task Completed successfully'} showType={'s'} />, {
                                                    // toast.success("Note Deleted successfully", {
                                                    position: toast.POSITION.TOP_CENTER,
                                                    hideProgressBar: true
                                                });
                                                onClose();
                                                this.setState({ success: true })
                                            } else {
                                                toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                                                    // toast.error(result.error, {
                                                    position: toast.POSITION.TOP_CENTER,
                                                    hideProgressBar: true
                                                });

                                            }

                                        })
                                    }}>Confirm</button>
                                <button className="Cancel_btn_Conf" onClick={
                                    () => {
                                        onClose();
                                        resolve({ status: false });
                                    }}> Cancel</button>
                            </div>
                        </div>
                    )
                }
            })
        });

    }

    showModal = () => {
        this.setState({ showModal: true })
    }
    closeModal = () => {
        this.setState({ showModal: false })
    }
    options = (data) => {
        let opt = [];

        if (typeof (data) != 'undefined') {
            opt.push({ text: 'Select Allocations:', optHeader: true });
            data.map((key, i) => (
                opt.push({
                    value: data[i].deptList,
                    text: data[i].deptList,
                    markup: multiSelectOptionMarkup(data[i].deptList),
                })
            ))


        }
        return opt;
    }
    crmParticipantAjax = () => {
        postData('crm/Dashboard/crm_participant_status', this.state).then((result) => {
            if (result.status) {
                this.setState({ crmparticipantCount: result.participant_count });
                this.setState({ grapheachdata: result.grapheachdata });

            } else {
                this.setState({ error: result.error });
            }
        });
    }
    crmParticipantCountBy = (type) => {
        this.setState({ view_type: type }, function () {
            this.crmParticipantCountAjax();

        });
    }
    crmParticipantStatusBy = (type) => {
        this.setState({ view_type: type }, function () {
            this.crmParticipantAjax();
        });
    }
    //
    crmParticipantCountAjax = () => {
        postData('crm/Dashboard/crm_participant_count', this.state).then((result) => {
            if (result.status) {
                this.setState({ crmparticipantCount: result.crm_participant_count });
                this.setState({ all_crmparticipant_count: result.all_crmparticipant_count });

            } else {
                this.setState({ error: result.error });
            }
        });
    }
    componentDidMount() {
        this.setState({ participant_id: this.props.props.match.params.id, staff_id: this.props.props.match.params.id });
        this.getStaffDetails();
        this.crmParticipantCountAjax();
        this.crmParticipantAjax();
    }
    EventList = () => {
        let eventList = [];
        if (typeof this.state.dueTask !== 'undefined' && this.state.dueTask.length > 0) {
            this.state.dueTask.map((key, i) => (
                eventList.push({
                    'title': this.state.dueTask[i].title,
                    'start': new Date(this.state.dueTask[i].start),
                    'end': new Date(this.state.dueTask[i].end),
                    'type': this.state.dueTask[i].type
                })
            ))
        }
        return eventList;
    }
    getStaffDetails = () => {
        this.setState({ loading: true }, () => {
            postData('crm/CrmStaff/get_staff_details', { id: this.props.props.match.params.id }).then((result) => {
                if (result.status) {
                    this.setState({ details: result.data });
                    this.participantDetailsRef.current.wrappedInstance.setStaffDetails(result.data);
                }
                this.setState({ loading: false });
            });
        });
    }
    render() {
        const Graphdata = {
            labels: ['Successful', 'Processing', 'Rejected'],
            datasets: [{
                data: [this.state.grapheachdata.successful, this.state.grapheachdata.processing, this.state.grapheachdata.rejected],
                backgroundColor: ['#be77ff', '#7c00ef', '#5300a7'],

            }],

        };
        var myEventsList = [
            {
                'title': 'Event 1',
                'startDate': new Date(2018, 1, 2, 8),
                'endDate': new Date(2018, 1, 2, 10)
            },
            {
                'title': 'Event 2',
                'startDate': new Date(2018, 1, 3, 12),
                'endDate': new Date(2018, 1, 3, 15)
            }];

        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];


        const options2 = [
            { text: 'Select Allocations:', optHeader: true },
            {
                value: 'Alfa Romeo Alfa RomeoAlfa RomeoAlfa RomeoAlfa RomeoAlfa Romeo',
                text: 'Alfa Romeo Alfa RomeoAlfa RomeoAlfa RomeoAlfa RomeoAlfa Romeo',
                markup: multiSelectOptionMarkup('Alfa Romeo Alfa RomeoAlfa RomeoAlfa RomeoAlfa RomeoAlfa Romeo'),
            },
            {
                value: 'fiat',
                text: 'Fiat',
                markup: multiSelectOptionMarkup('Fiat'),
            },
            {
                value: 'ferrari',
                text: 'Ferrari',
                markup: multiSelectOptionMarkup('Ferrari'),
            },
            {
                value: 'mercedes',
                text: 'Mercedes',
                markup: multiSelectOptionMarkup('Mercedes'),
            },
            {
                value: 'tesla',
                text: 'Tesla',
                markup: multiSelectOptionMarkup('Tesla'),
            },
            {
                value: 'volvo',
                text: 'Volvo',
                markup: multiSelectOptionMarkup('Volvo'),
            },
            {
                value: 'zonda',
                text: 'Zonda',
                markup: multiSelectOptionMarkup('Zonda'),
            },
        ]

        const Applicantdata = {
            labels: ['Successful', 'Processing', 'Rejected'],
            datasets: [{
                data: ['250', '333', '250'],
                backgroundColor: ['#ed97fa', '#b968c7', '#702f75'],

            }],

        };
        const pageSize = (typeof (this.state.details.task_list) == 'undefined' ? 0 : this.state.details.task_list.length)
        const status = ['none', 'NDIS Intake', 'Intake', 'Plan Delegation'];
        const contact = ['none', 'Phone Call', 'Email'];
        const classname = ['none', 'priority_high_task', 'priority_medium_task', 'priority_low_task'];

        const subIconMapper = (row) => {
            let data = row.original;
            return (
                <span className={"Staff_TIcon " + classname[data.stage_id]}><i className="icon icon-circle1-ie"></i></span>
            )
        }

        const subComponentDataMapper = (row) => {
            let data = row.original;
            return (
                <div className="col-md-12 task_table bt-1">

                    <div className="row">
                        <div className="col-md-12">
                            <div className="w-80 mx-auto  mt-5">
                                <div className="Partt_d1_txt_3  mb-3"><strong>Task Name:  </strong><span> {data.task_name}</span></div>
                                <div className="Partt_d1_txt_3  mb-3"><strong>Task Note:  </strong><span> {data.note}</span></div>
                                <div className="Partt_d1_txt_3  mb-3"><strong>Date:  </strong><span> {data.date}</span></div>
                                {  // <div className="Partt_d1_txt_3  mb-3"><strong>Update Specifics Notes:  </strong><span> {data.note}</span></div>
                                    // <div className="Partt_d1_txt_3  mb-3"><strong>Contact Method: </strong><span> <u>{contact[data.prefer_contact]}</u></span></div>
                                }
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="pt-3 pb-4 task_table_footer">
                                {/* <a><u>Add Note</u></a>
                            <a><u>Attach to Sub-Task</u></a> */}
                                <a><u><Link to={ROUTER_PATH + 'admin/crm/tasks'}>View</Link></u></a>
                                <a onClick={() => this.completeTask(data.task_id)}><u>Complete</u></a>
                                <a onClick={() => this.archiveTask(data.task_id)}><u>Archive</u></a>

                            </div>
                        </div>

                    </div>

                </div>
            );
        }
        return (
            <div className="container-fluid">
                <CrmPage ref={this.participantDetailsRef} pageTypeParms={'user_staff_members_details'} />

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1 col-md-12">
                        <div className="back_arrow py-4 bb-1">
                            <Link to={ROUTER_PATH + 'admin/crm/Usermangement'}><span className="icon icon-back1-ie"></span></Link>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1  col-md-12">
                        <div className="row d-flex">
                            <div className="col-md-12 align-self-center py-4 bb-1">
                                <div className="h-h1 ">{this.props.showPageTitle}</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1  col-md-12 py-4 bb-1">
                        <div className="row">
                            <div className="col-md-5">

                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="Partt_d1_txt_1"><strong>{this.state.details.FullName}</strong></div>
                                        <div className="Partt_d1_txt_2"><strong>HCMGR-ID: </strong> <u>{this.state.details.hcmgr_id}</u></div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="Partt_d1_txt_2"><strong>Phone: </strong> <u> {this.state.details.phone}</u> </div>
                                        <div className="Partt_d1_txt_2"><strong>Email: </strong> <u>{this.state.details.email}</u> </div>
                                    </div>
                                    <div className="col-md-12 mt-3">
                                        <div className="Partt_d1_txt_2"><strong>Position: </strong> <u>{this.state.details.position}</u></div>
                                    </div>
                                </div>

                            </div>


                            <div className="col-md-7">
                                <div className="row">
                                    <div className="col-lg-5 col-md-6">
                                        <div className="Partt_d1_txt_2 mb-3 justify-content-center"><strong>Allocated Service Area</strong></div>
                                        <div className="mb-3 Cust_Sel_my A2">
                                            {/* <ReactResponsiveSelect
                                                multiselect
                                                name="make6"
                                                options={this.options(this.state.details.DepartmentList)}
                                                onSubmit={() => { console.log("Handle form submit here") }}
                                                noSelectionLabel="Please select"
                                                caretIcon={caretIcon}
                                                onChange={(newValue) => { console.log(newValue) }}
                                            /> */}
                                            <p style={{ textAlign: 'center' }}>NDIS</p>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 col-lg-offset-1 col-md-6">
                                        <div className="Partt_d1_txt_2 mb-3 justify-content-center"><strong>CRM Access Permissions:</strong></div>
                                        <div className="mb-3 Cust_Sel_my A2">
                                            {/* <ReactResponsiveSelect
                                                multiselect
                                                name="make6"
                                                options={options2}
                                                onSubmit={() => { console.log("Handle form submit here") }}
                                                noSelectionLabel="Please select"
                                                caretIcon={caretIcon}
                                                onChange={(newValue) => { console.log(newValue) }}
                                            /> */}
                                            <p style={{ textAlign: 'center' }}>All Permissions</p>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>




                <div className="row d-flex">

                    <div className="col-lg-4 col-lg-offset-1 col-md-5  mt-5">
                        <div className='Staff_details_left'>
                            <div className="row d-flex my-3">
                                <div className="col-md-8 d-inline-flex align-self-center">
                                    <h3 className="Rej_task">Tuesday, December 02</h3>
                                </div>
                                <div className="col-md-4">
                                    <a className="v-c-btn1 n2"><span>New Task</span> <i className="icon icon-add3-ie"></i></a>
                                </div>
                            </div>

                            <div className="row d-flex my-3">
                                <div className="col-md-4 d-inline-flex align-self-center">

                                </div>
                                <div className="col-md-8">
                                    <h3 className="color">Task Priority:</h3>
                                    <ul className="Staff_task_div1">
                                        <li className="Suc_task"><i className="icon icon-circle1-ie"></i><span>Low</span></li>
                                        <li className="Pro_task"><i className="icon icon-circle1-ie"></i><span>Medium</span></li>
                                        <li className="Rej_task"><i className="icon icon-circle1-ie"></i><span>High</span></li>
                                    </ul>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-12 re-table Staff_Tab mt-4">
                                    <ReactTable

                                        columns={[
                                            {
                                                Header: "Type",

                                                Cell: row => (<span className={"Staff_TIcon " + classname[row.original.priority]}><i className="icon icon-circle1-ie"></i></span>),

                                                // Task color class
                                                // 1. Suc_task
                                                // 2. Pro_task
                                                // 3. Rej_task

                                                headerStyle: { border: '0px solid #000', },
                                                width: 65,
                                            },
                                            {
                                                Header: "HCMGR-ID:", accessor: "hcmgr_id",
                                            },
                                            {
                                                Header: "Name:", accessor: "FullName",
                                                headerStyle: { border: "0px solid #fff" },
                                            },
                                            {
                                                Header: "",
                                                Cell: row => (<span className="Staff_VIcon"><i className="icon icon-view1-ie"></i></span>),
                                                headerStyle: { border: '0px solid #000', },
                                                width: 65,
                                            },
                                            {
                                                expander: true,
                                                Header: () => <strong></strong>,
                                                width: 35,
                                                headerStyle: { border: "0px solid #fff" },
                                                Expander: ({ isExpanded, ...rest }) =>
                                                    <div className="rec-table-icon">
                                                        {isExpanded
                                                            ? <i className="icon icon-arrow-up"></i>
                                                            : <i className="icon icon-arrow-down"></i>}
                                                    </div>,
                                                style: {
                                                    cursor: "pointer",
                                                    fontSize: 25,
                                                    padding: "0",
                                                    textAlign: "center",
                                                    userSelect: "none"
                                                },

                                            }

                                        ]}
                                        data={this.state.details.task_list}
                                        pageSize={pageSize}
                                        showPagination={false}
                                        className="-striped -highlight"
                                        previousText={<span className="icon icon-arrow-1-left privious"></span>}
                                        nextText={<span className="icon icon-arrow-1-right next"></span>}
                                        SubComponent={subComponentDataMapper}
                                    />

                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-4 col-md-offset-4"><Link to={ROUTER_PATH + 'admin/crm/tasks'} className="btn-1 s2" >View All Tasks</Link></div>
                            </div>

                        </div>


                    </div> {/* col-4 */}

                    <div className="col-lg-6 col-md-7">

                        <div className="row d-flex mt-5">
                            <div className="col-lg-6 col-md-6">
                                <div className="circul-c pb-4">
                                    <h3 className="text-center pb-3">Participant Status:</h3>
                                    <div className="col-md-5 pr-0 pt-0">
                                        <Doughnut data={Graphdata} height={250} className="myDoughnut" legend={""} />
                                    </div>
                                    <div className="col-md-7 text-left mt-3">
                                        <div className="chart_txt_1"><b>Successful:</b> {this.state.grapheachdata.successful}</div>
                                        <div className="chart_txt_2"><b>Processing:</b> {this.state.grapheachdata.processing}</div>
                                        <div className="chart_txt_3"><b>Rejected:</b> {this.state.grapheachdata.rejected}</div>
                                        <div className="chart_txt_4">
                                            <div className="mt-5"><b>View by:</b></div>

                                            <span onClick={() => this.crmParticipantStatusBy('week')} className={this.state.view_type == 'week' ? 'color' : ''}>Week</span><br />
                                            <span onClick={() => this.crmParticipantStatusBy('month')} className={this.state.view_type == 'month' ? 'color' : ''}>Month </span><br />
                                            <span onClick={() => this.crmParticipantStatusBy('year')} className={this.state.view_type == 'year' ? 'color' : ''}> Year</span><br />


                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-6 col-md-6">
                                <div className="circul-c pb-4">
                                    <h3 className="text-center pb-3">Prospective Participants:</h3>
                                    <div className="col-md-5 pr-0">
                                        <CounterShowOnBox counterTitle={this.state.crmparticipantCount} classNameAdd="" />
                                    </div>
                                    <div className="col-md-7 text-center mt-3"><br></br><br></br><br></br>
                                        <div className="chart_txt_4 chart_txt_5">
                                            <div className="mt-5" ><b>View by:</b></div>
                                            <span onClick={() => this.crmParticipantCountBy('week')} className={this.state.view_type == 'week' ? 'color' : ''}>Week</span><br />
                                            <span onClick={() => this.crmParticipantCountBy('month')} className={this.state.view_type == 'month' ? 'color' : ''}>Month </span><br />
                                            <span onClick={() => this.crmParticipantCountBy('year')} className={this.state.view_type == 'year' ? 'color' : ''}> Year</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div className="row d-flex my-3 mt-5">
                            <div className="col-md-4 d-inline-flex align-self-center">
                                <h3 className="">Current Schedule:</h3>
                            </div>
                            <div className="col-md-8">
                                <h5 className="mb-3">Current Schedule:</h5>
                                <ul className="Staff_task_div1">
                                    <li className="Suc_task"><i className="icon icon-circle1-ie"></i><span>Low</span></li>
                                    <li className="Pro_task"><i className="icon icon-circle1-ie"></i><span>Medium</span></li>
                                    <li className="Rej_task"><i className="icon icon-circle1-ie"></i><span>High</span></li>
                                </ul>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12 mt-4">
                                <div className="Schedule_calendar">
                                    <BigCalendar
                                        localizer={localizer}
                                        views={['month']}
                                        events={this.EventList()}
                                        components={{ toolbar: CalendarToolbar }}
                                        startAccessor="start"
                                        endAccessor="end"
                                    />

                                </div>
                            </div>
                        </div>


                    </div> {/* col-6 */}

                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1">
                        <div className="task_table_footer Staff_f bt-1">
                            {/* <a><u>View Analytics </u></a> */}
                            {this.state.details.status==1?<a onClick={() => this.setState({showModal:true})}><u>Disable Recruiter</u></a>:<a onClick={() => this.EnableRecruiter(this.state.participant_id)}><u>Enable Recruiter</u></a>}

                        </div>
                    </div>
                </div>
                <DisableStaff showModal={this.state.showModal} staffId={this.state.participant_id}  closeModal={this.closeModal}  />
              </div>





        );
    }
}
const mapStateToProps = state => {
    return {
        showPageTitle: state.DepartmentReducer.activePage.pageTitle,
        showTypePage: state.DepartmentReducer.activePage.pageType
    }
};
export default connect(mapStateToProps)(StaffDetails);

class CalendarToolbar extends Toolbar {
    render() {
        return (
            <div>
                <div className="rbc-btn-group">
                    <span className="" onClick={() => this.navigate('TODAY')} >Today</span>
                    <span className="icon icon-arrow-left" onClick={() => this.navigate('PREV')}></span>
                    <span className="icon icon-arrow-right" onClick={() => this.navigate('NEXT')}></span>
                </div>
                <div className="rbc-toolbar-label">{this.props.label}</div>
            </div>
        );
    }
}
