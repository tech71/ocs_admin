import React from 'react';
import Select from 'react-select-plus';
import jQuery from "jquery";
import '../../../service/jquery.validate.js';
import { ROUTER_PATH, BASE_URL }from '../../../config.js';
import { postData, getOptionsSuburb, handleAddShareholder } from '../../../service/common.js';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { listViewSitesOption, relationDropdown, sitCategoryListDropdown, ocDepartmentDropdown, getAboriginalOrTSI, LivingSituationOption} from '../../../dropdown/ParticipantDropdown.js';

export const RefererData = (props) => {
    return (

                <React.Fragment>
                 <form id="referral_details">
                  <div className="row">
                    <div className="col-md-12 py-4 title_sub_modal">Referer Details</div>
                  </div>
                  <div className="row">
                    <div className="col-md-3 mb-4">
                      <label className="title_input">First name: </label>
                      <div className="required"><input name="first_name" className="default-input"  onChange={(e) =>props.handleChanges(e,'refererDetails')} maxLength="30" data-rule-required="true"/></div>
                    </div>
                    <div className="col-md-3 mb-4">
                      <label className="title_input">Last Name: </label>
                      <div className="required"><input name="last_name" onChange={(e) =>props.handleChanges(e,'refererDetails')} className="default-input" data-rule-required="true" /></div>
                    </div>
                    <div className="col-md-3 mb-4">
                      <label className="title_input">Organisation: </label>
                      <div className="required"><input name="organisation" className="default-input"  data-rule-required="true" onChange={(e) =>props.handleChanges(e,'refererDetails')}/></div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-3 mb-4">
                      <label className="title_input">Email: </label>
                      <div className="required"><input name="email" className="default-input" data-rule-required="true" data-rule-email="true" onChange={(e) =>props.handleChanges(e,'refererDetails')}/></div>
                    </div>
                    <div className="col-md-3 mb-4">
                      <label className="title_input">Phone Number: </label>
                      <div className="required"><input name="phone_number" className="default-input" data-rule-required="true" data-rule-phonenumber onChange={(e) =>props.handleChanges(e,'refererDetails')}/></div>
                    </div>
                    <div className="col-md-3 mb-4">
                      <label className="title_input">Relationship to Partiapnt: </label>
                      <div className="required">
                        <div className="s-def1">
                        {props.errorTooltip('relation', 'Select Referral','refererDetails')}
                          <Select
                            data-rule-required="true"
                            name="relation" options={relationDropdown(0)} required={true} simpleValue={true}
                            searchable={false} clearable={false}
                            onChange={(e) => props.updateSelect(e,'relation','refererDetails')}
                            value={props.sts.relation}
                          />
                        </div>
                      </div>
                    </div>

                  </div>
                  <div className="row d-flex justify-content-end">
                    <div className="col-md-3"><a className="btn-1" onClick={props.submitReferDetail}>Save And Changes</a></div>
                  </div>
                  </form>
                </React.Fragment>

    );
}
