import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { Panel, Button, ProgressBar,  PanelGroup } from 'react-bootstrap';
import Select from 'react-select-plus';
import Modal from 'react-bootstrap/lib/Modal';
import { confirmAlert, createElementReconfirm } from 'react-confirm-alert';
import 'react-select-plus/dist/react-select-plus.css';
import { postImageData,checkItsNotLoggedIn, postData, IsValidJson, getOptionsSuburb, handleRemoveShareholder, handleShareholderNameChange, handleAddShareholder, getQueryStringValue }
  from '../../../service/common.js';
import jQuery from "jquery";
import DatePicker from 'react-datepicker';
import Autocomplete from 'react-google-autocomplete';
import moment from 'moment';
import { ToastContainer, toast } from 'react-toastify';
import ReactPlaceholder from 'react-placeholder';
import {ROUTER_PATH} from '../../../config.js';
import {LeftManubar, DetailsPage, IntakeProcess} from '../../../service/CrmLoader.js';
import { CallReference } from './CallReference';
import { LockedFunding } from './LockedFunding';
import { AllUpdates } from './ParticipantAllUpdates';
import { listViewSitesOption, relationDropdown, sitCategoryListDropdown, ocDepartmentDropdown, getAboriginalOrTSI, LivingSituationOption} from '../../../dropdown/ParticipantDropdown.js';
import {getStagesStatus,getParticipantState} from '../../../dropdown/CrmDropdown.js';
import CrmPage from './CrmPage';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { ToastUndo } from 'service/ToastUndo.js'
const download = require('image-downloader')
{// require("downloadjs");
}
var fileDownload = require('js-file-download');
  let a=[];
const marital_status = ['none','Married','Single','Divorced','Widowed'];
const status = ['No','Yes'];
class ParticipantDetails extends Component {
    constructor(props, context) {
        super(props, context);
        this.participantDetailsRef = React.createRef();
        this.state = {
            AllupdateModalShow:false,
             showModal_PE: false,
             allupdates:{},
             percent:[],
             documents:[],
             activeWord:[],
         filterVal: '', details:{},participant_id:'',notesList:[],kindetails:{},
         participant_id:this.props.props.match.params.id
       }

    }
    notes_list =(str)=>{
      postData('crm/CrmStage/notes_list_with_staff_member_and_stage_id', str).then((result) => {
          if(result.status){
              this.setState({allupdates:result.allupdates});
          }
        })
    }
    showModal_PE = (id) => {
        if(id=="AllupdateModalShow")
        {
          var str = {staff_id:'',stage_id:''};
            this.notes_list(str);
            this.getAllStages();
        }

        let state ={};
        state[id] = true;

        this.setState(state)
    }
    closeModal_PE = (id) => {
        let state ={};
        state[id] = false;

        this.setState(state)
    }


    showModal2 = (id) => {
        this.getIntakeInfomation(id);
        this.setState({ showModal2: true,stageId:id })
        this.getStages();
    }

    fileChangedHandler = (event) => {
		 let filenames=[];
        for(let i=0;i<event.target.files.length;i++)
        {
           filenames.push(event.target.files[i].name)
        }
        this.setState({selectedFile: event.target.files, filename: filenames})
     
    }


    componentDidMount() {
        this.participantDetailsRef.current.wrappedInstance.getParticipantDetails(this.state.participant_id);
       this.getIntakeInfomation();
       this.getStages();
       this.getLatestSage();
       this.getAllStages();
      this.getIntakePercentage();
      this.getStageDocs();
    }
    componentWillReceiveProps(nextProps){
        if(  (nextProps.participaintDetails.id>0 || this.props.participaintDetails.id!=nextProps.participaintDetails.id) ){
            this.setState({details:nextProps.participaintDetails});
        }
    }
    uploadHandler = (e,form_id) => {
        e.preventDefault();
        jQuery("#"+form_id).validate({ /* */});
        if (jQuery("#"+form_id).valid())
        {
            this.setState({submit_form:false});
            const formData = new FormData()
            for(var x = 0; x<this.state.selectedFile.length; x++) {
                formData.append('crmParticipantFiles[]', this.state.selectedFile[x])
            }
            formData.append('crmParticipantId', this.state.participant_id)
            formData.append('docsTitle', this.state.docsTitle)
            formData.append('category', this.state.category)
            const config = {
                                headers: {
                                    'content-type': 'multipart/form-data'
                                }
            }
            postImageData('crm/CrmParticipant/uploading_crm_paricipant_stage_docs', formData,config).then((result) => {
            if (result.status) {
                this.closeModal_PE("showModal1");
                toast.success(<ToastUndo message={'Uploaded successfully.'} showType={'s'} />, {
                    // toast.success("uploaded successfully.", {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                } else {
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                    // toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
                this.setState({submit_form: true});
            });
        }
    }
    getParticipantDetails = () => {
        this.setState({loading: true}, () => {
            postData('crm/CrmParticipant/get_prospective_participant_details', {id:this.state.participant_id}).then((result) => {

                if (result.status) {


                  this.setState({
                      details:result.data,

                    });
                }
                this.setState({loading: false});
            });
        });
    }
        getStages = () => {
            if(this.state.stageId){
            let stage_name='';
            this.setState({loading: true}, () => {
            var intakeStr = JSON.stringify({id:this.state.stageId});
                postData('crm/CrmStage/get_stage_info_by_id', intakeStr).then((result) => {

                    if (result[0].name) {

                    this.setState({stage_name:result[0].name});
                    }
                    this.setState({loading: false});
                });
            });
        }
    }
    getAllStages = () => {
        let stage_name='';
        this.setState({loading: true}, () => {
        var intakeStr = "{}";
            postData('crm/CrmStage/get_all_stage', intakeStr).then((result) => {

                if (result) {

                this.setState({stage_info:result});
                }
                this.setState({loading: false});
            });
        });
    }
    getLatestSage = () => {
        let latestStage='';

        var intakeStr = JSON.stringify({crm_participant_id:this.state.participant_id});
            postData('crm/CrmStage/get_latest_stage', intakeStr).then((result) => {

                if (result) {
                    this.setState({latestStage:result[0].latest_stage_name,latestStageState:result[0].latest_stage});
                  }
                  this.setState({loading: false});
            });

    }
    getIntakeInfomation = (id=1) => {
       let notesList=[];
        this.setState({loading: true}, () => {
            var intakeStr = JSON.stringify({crm_participant_id:this.state.participant_id,stage_id:id});
            postData('crm/CrmStage/list_intake_info',intakeStr ).then((result) => {
                if (result) {
                  this.setState({notesList:result});
                  this.getLatestSage();
                }
                this.setState({loading: false});
            });
        });
    }
  getStageDocs = (e) => {
     this.setState({loading: true}, () => {
             var id = JSON.stringify({crm_participant_id:this.props.props.match.params.id});
             postData('crm/CrmParticipant/get_participant_stage_docs',id ).then((result) => {
                 if (result.status) {
                   this.setState({documents:result.data});
                    // console.log(result.data);
                 }
                  this.setState({loading: false});
            });
        });
    }
    deleteNote = (id) => {
        let  msg = <span>Are you sure you want to archive this item? <br/> Once archived, this action can not be undone.</span>;
        return new Promise((resolve, reject) => {
            confirmAlert({
                customUI: ({ onClose }) => {
                    return (
                            <div className='custom-ui'>
                                <div className="confi_header_div">
                                    <h3>Confirmation</h3>
                                    <span className="icon icon-cross-icons" onClick={() => {
                                    onClose();
                                    resolve({status: false})}}></span>
                                </div>
                                <p>{
                                        msg}</p>
                                <div className="confi_but_div">
                                    <button className="Confirm_btn_Conf" onClick={
                                        () => {
                                            postData('crm/CrmStage/delete_intake', {intake_id:id}).then((result) => {
                                                if (result) {
                                                toast.success(<ToastUndo message={'Note Deleted successfully'} showType={'s'} />, {
                                                // toast.success("Note Deleted successfully", {
                                                    position: toast.POSITION.TOP_CENTER,
                                                    hideProgressBar: true
                                                    });
                                                    onClose();
                                                    this.getIntakeInfomation(this.state.stageId);
                                                    this.getLatestSage();
                                                    this.setState({success: true})
                                                    } else{
                                                        toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                                                        // toast.error(result.error, {
                                                            position: toast.POSITION.TOP_CENTER,
                                                            hideProgressBar: true
                                                        });

                                                    }

                                               })
                                            }}>Confirm</button>
                                    <button className="Cancel_btn_Conf" onClick={
                                                () => {
                                                    onClose();
                                                    resolve({status: false});}}> Cancel</button>
                                </div>
                            </div>
                                            )
                        }
                    })
                });

    }

      submitNote = (e) => {

        e.preventDefault();

        var validator = jQuery("#add_note").validate({ignore: []});
        if (!this.state.loading && jQuery("#add_note").valid()) {
        var inputNote = {};
        inputNote['notes'] = this.state.notes_txt_area;
        inputNote['crm_participant_id'] = this.state.participant_id;
        inputNote['stage_id'] = this.state.stageId;

        var str = JSON.stringify(inputNote);
        this.setState({loading: true},() => {
            postData('crm/CrmStage/create_intake_info', str).then((result) => {
                if (result.status) {
                    toast.success(<ToastUndo message={'Note Added successfully.'} showType={'s'} />, {
                // toast.success("Note Added successfully", {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                    });


                    this.getIntakeInfomation(this.state.stageId);
                    this.getLatestSage();
                    this.setState({success: true,notes_txt_area:''})
                    } else{
                        toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                        // toast.error(result.error, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });


                    }
                this.setState({loading: false})
            });
        });
        } else {
            validator.focusInvalid();
        }
      }


      onClickFunction = (idx) => {

      (this.state.activeWord.indexOf(idx)==-1)?
          a.push(idx):
         a.splice( a.indexOf(idx), 1 );
          this.setState({activeWord: a})
      }
      archiveDocuments = (e) => {
           this.setState({loading: true}, () => {
              var data = JSON.stringify({ids:this.state.activeWord});
              var msg = <span>Are you sure you want to archive this item? <br /> Once archived, this action can not be undone.</span>;
              var url = "crm/CrmParticipant/archive_partcipant_stage_docs";

              return new Promise((resolve, reject) => {
                  confirmAlert({
                      customUI: ({ onClose }) => {
                          return (
                              <div className='custom-ui'>
                                  <div className="confi_header_div">
                                      <h3>Confirmation</h3>
                                      <span className="icon icon-cross-icons" onClick={() => {
                                          onClose();
                                          resolve({ status: false })
                                      }}></span>
                                  </div>
                                  <p>{
                                      msg}</p>
                                  <div className="confi_but_div">
                                      <button className="Confirm_btn_Conf" onClick={
                                          () => {
                                              postData(url, data).then((result) => {
                                                  resolve({result});
                                                  onClose();
                                              })
                                          }}>Confirm</button>
                                      <button className="Cancel_btn_ Conf" onClick={
                                          () => {
                                              onClose();
                                              resolve({ status: false });
                                          }}> Cancel</button>
                                  </div>
                              </div>
                          )
                      }
                  })
              });
          });
      }

      downloadDocuments = (e) =>{
         let fd = new FormData();
        fd.append("rows",JSON.stringify({ids:this.state.activeWord}));
        postImageData('crm/CrmParticipant/download_participant_stage_docs', fd,).then((res) => {
        // postImageData('crm/CrmParticipant/download_participant_stage_docs', fd,).then((res) => {
        const options = {
          url: 'http://localhost/ocs_admin/admin/back-end/crm/CrmParticipant/download_participant_stage_docs',
          dest: res.file_name,
          headers: {
             'Access-Control-Allow-Origin': '*',
             'Content-Type': 'application/json',
           }
                     // Save to /path/to/dest/image.jpg
        }
        download.image(options)
    .then(({ filename, image }) => {
      console.log('File saved to', filename)
    }).catch((err) => {
      console.error(err)
    })
  // download('http://localhost/ocs_admin/admin/back-end/uploads/crmparticipant/2/Webp_net-resizeimage_(2)26.png');
  //         var blob = new Blob([res.file_content], {
  //           type: "image/png;"
  //         });
  console.log(typeof(res.file_content));

  // fileDownload(res.file_content, res.file_name);
  // fileDownload(res.file_content, res.file_name);
        //  var fileUrl = window.URL.createObjectURL(blob);
          //   var fileUrl = window.URL.createObjectURL(blob);
          // console.log( window.URL.createObjectURL(blob));
          // var element = document.createElement("a");
          // element.href = fileUrl;
          // //'http://localhost/ocs_admin/admin/back-end/uploads/crmparticipant/2/Webp_net-resizeimage_(2)26.png'
          // element.setAttribute("download", res.file_name);
          //element.setAttribute("href", "");

        //  document.body.appendChild(element);
          // element.click();
        //  element.style.display = "";

          // document.body.removeChild(element);
          // fileDownload(res['file'], res['name']);
         // const link = document.createElement('a');
         // link.href = res['file'];
         // link.setAttribute('download', res.name); //or any other extension
         // document.body.appendChild(link);
         // link.click();

                  });
      }
    selectChange = (selectedOption, fieldname) => {
        var newSelectedValue = (this.state.newSelectedValue)?this.state.newSelectedValue:{};
        let staff_id='';
        newSelectedValue[fieldname] = selectedOption;
        newSelectedValue[fieldname+'_error'] = false;
        if(typeof newSelectedValue.assign_to!='undefined'){
          staff_id=newSelectedValue.assign_to.value;
        }
        let stage_id=newSelectedValue.view_by_status;
        this.setState({newSelectedValue});

            var str = {staff_id:staff_id,stage_id:stage_id};
            this.notes_list(str)

    }

    getIntakePercentage = (e) => {
             var intake = JSON.stringify({crm_participant_id:this.props.props.match.params.id});
             postData('crm/CrmParticipant/get_intake_percent',intake ).then((result) => {
                 if (result) {
                   this.setState({percent:result});
                 }
             });
    }
    setParticipantState = (e) => {
      console.log(e);
      this.setState({filterVal:e});
             var intake = JSON.stringify({crm_participant_id:this.props.props.match.params.id,state:e});
             postData('crm/CrmParticipant/change_participant_state',intake ).then((result) => {
                 if (result) {
                   this.setState({percent:result});
                 }
             });
    }

    render() {
      let now = 0;
      // if(this.state.percent.length!=0){
      //     now = this.state.percent.data.level;
      // }


        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];





        const data = [{
            caseid: '12116133148',
            category: 'CAT 1',
            eventdate: '01/01/1920',
            description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        }, {
            caseid: '12116133148',
            category: 'CAT 1',
            eventdate: '01/01/1920',
            description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        },
        , {
            caseid: '12116133148',
            category: 'CAT 1',
            eventdate: '01/01/1920',
            description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        }]
        const columns = [{
            Header: 'Case ID:',
            accessor: 'caseid',
            headerClassName: 'Th_class_d1 _align_c__',
            maxWidth: 140,
            className: (this.state.activeCol === 'name') && this.state.resizing ? 'borderCellCls' : 'Tb_class_d1',
            Cell: props => <span className="h-100" style={{ justifyContent: 'center' }}>
                <div>
                    {props.value}
                </div>
            </span>
        }, {
            Header: 'Category:',
            accessor: 'category',
            maxWidth: 120,
            headerClassName: 'Th_class_d1 _align_c__',
            className: (this.state.activeCol === 'name') && this.state.resizing ? 'borderCellCls' : 'Tb_class_d1',
            Cell: props => <span className="h-100" style={{ justifyContent: 'center' }}>
                <div>
                    {props.value}
                </div>
            </span>
        }, {
            Header: 'Event Date',
            accessor: 'eventdate',
            maxWidth: 120,
            Cell: props =>  <div>
            {props.value}
        </div>

        }, {
            Header: 'Description',
            accessor: 'description',
            headerStyle: { border: "0px solid #fff" },
        },
        {
            expander: true, sortable: false,
            Expander: ({ isExpanded, ...rest }) =>
                <div>{isExpanded ? <i className="icon icon-arrow-up"></i> : <i className="icon icon-arrow-down"></i>}</div>,
            headerStyle: { border: "0px solid #fff" },

        }
    ]



        return (
            <div className="container-fluid">
            <CrmPage ref={this.participantDetailsRef} pageTypeParms={'participant_details'} />
                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1">
                        <div className="back_arrow py-4 bb-1">
                        <Link to='../prospectiveparticipants'><span className="icon icon-back1-ie"></span></Link>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1">
                        <div className="row d-flex py-4">
                            <div className="col-md-6 align-self-center br-1">
                                <div className="h-h1 ">
                                    {this.props.showPageTitle}
                            </div>
                            </div>
                            <div className="col-md-6">
                                <div className="Lates_up_1">
                                    <div className="Lates_up_a col-md-3 align-self-center">
                                        Latest
                                        Update:
                                    </div>
                                    <div className="col-md-9 justify-content-between pr-0">
                                        <div className="Lates_up_b">
                                         <div className="Lates_up_txt"><b>{(this.state.latestStage)?this.state.latestStage:"Stage 1:NDIS Intake Participant Submission"} {(this.state.stageId !=6)?"Information":""}</b></div>
                                            <div className="Lates_up_btn br-1 bl-1"><i className="icon icon-view1-ie"></i><span>View Attachment</span></div>
                                            <div className="Lates_up_btn"  onClick={()=>this.showModal_PE("AllupdateModalShow")}><i className="icon icon-view1-ie"></i><span>View all Updates</span></div>
                                            <AllUpdates allupdates={this.state.allupdates}  stages={(this.state.stage_info)?this.state.stage_info:''} onSelectDisp={(this.state.newSelectedValue)?this.state.newSelectedValue:''} selectedChange1={(e)=>this.selectChange(e,'view_by_status')} selectedChange={(e)=>this.selectChange(e,'assign_to')} showModal={this.state.AllupdateModalShow} handleClose={()=>this.closeModal_PE("AllupdateModalShow")}/>
                                        </div>
                                        <div className="Lates_up_2">
                                            <div className="Lates_up_txt2 btn-1">Susan McDonald (Recruiter)</div>
                                            <div className="Lates_up_time_date"> Date: 01/01/01 - 11:32AM</div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div className="row"><div className="col-md-12"><div className="bt-1"></div></div></div>
                    </div>
                </div>


{/* version 4 start */}
                <div className="row mb-5">
                    <div className="col-lg-10 col-lg-offset-1 mt-5">
                        <div className="v4_pro_d1__">
                            <div className="progress-b1">
                            <ProgressBar className="progress-b2" now={now} label={'Intake Progress: ' + `${now}%`+ 'Complete'} />
                            </div>

                            <div className="v4_pro_d1_a1__">
                                <div className="v4_1">
                                    <div className="Partt_d1_txt_4"><strong>{(this.state.details.FullName)?this.state.details.FullName:'N/A'}</strong><span></span></div>
                                    <a className="in_prog_btn">In Progress</a>
                                </div>
                                <div className="v4_1">
                                    <div className="Partt_d1_txt_2 pt-4"><strong>Assigned to:</strong></div>
                                    <div className="Partt_d1_txt_1 my-3"><strong> {(this.state.details.assigned_to)?this.state.details.assigned_to:'N/A'}</strong></div>
                                </div>
                                <div className="v4_1">
                                    <div className="Partt_d1_txt_2 pt-4"><strong>Department:</strong></div>
                                    <div className="Partt_d1_txt_1 my-3"><strong> {(this.state.details.participant_department)?this.state.details.participant_department:'N/A'}</strong></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
                <div className="row mb-5">
                    <div className="col-lg-10 col-lg-offset-1 mt-5">
                        <div className="v4_pro_d2__">
                            <div className="row">
                                <div className="col-md-9 pt-5 pb-3">
                                <div class="Partt_d1_txt_1"><strong> Participant Information</strong><span></span></div>
                                </div>
                                <div className="col-md-3 pt-3">
                                    <span className="btn-1" onClick={()=>{this.showModal_PE("showModal")}}>Edit Participants Info</span>
                                </div>
                            </div>
                            <div className="row"><div className="col-md-12"><div className="bt-1"></div></div></div>
                            <div className="row">
                            <div className="col-md-12 col-lg-10">
                            <div className="row">
                            <div className="col-md-3 col-lg-3">
                            <div className="Partt_d1_txt_2 my-3"><strong> Plan Management:</strong></div>
                                <div className="s-def1 s1 mt-2 mb-5">
                                    <Select
                                        name="view_by_status"
                                        options={options}
                                        required={true}
                                        simpleValue={true}
                                        searchable={false}
                                        clearable={false}
                                        placeholder="Filter by: Unread"
                                        onChange={(e) => this.setState({ filterVal: e })}
                                        value={this.state.filterVal}
                                    />
                                </div>
                                </div>
                                <div className="col-md-3 col-lg-3">
                            <div className="Partt_d1_txt_2 my-3"><strong> Primary Contact:</strong></div>
                                <div className="s-def1 s1 mt-2 mb-5">
                                    <Select
                                        name="view_by_status"
                                        options={options}
                                        required={true}
                                        simpleValue={true}
                                        searchable={false}
                                        clearable={false}
                                        placeholder="Filter by: Unread"
                                        onChange={(e) => this.setState({ filterVal: e })}
                                        value={this.state.filterVal}
                                    />
                                </div>
                                </div>
                                <div className="col-md-3 col-lg-3">
                            <div className="Partt_d1_txt_2 my-3"><strong> Preferred Contact:</strong></div>
                                <div className="s-def1 s1 mt-2 mb-5">
                                    <Select
                                        name="view_by_status"
                                        options={options}
                                        required={true}
                                        simpleValue={true}
                                        searchable={false}
                                        clearable={false}
                                        placeholder="Filter by: Unread"
                                        onChange={(e) => this.setState({ filterVal: e })}
                                        value={this.state.filterVal}
                                    />
                                </div>
                                </div>
                             </div>
                             </div>
                             </div>

                             <div className="row">
                            <div className="col-md-3">
                            <div className="Partt_d1_txt_2 my-3"><strong>NDIS No.:</strong><span> {(this.state.details.ndis_num)?this.state.details.ndis_num:'N/A'}</span></div>
                            <div className="Partt_d1_txt_2 my-3"><strong>Medicare No.:</strong><span> {(this.state.details.medicare_num)?this.state.details.medicare_num:'N/A'}</span></div>
                            <div className="Partt_d1_txt_2"><strong>Phone:</strong><span> {(this.state.details.phone)?this.state.details.phone:'N/A'}</span></div>
                            <div className="Partt_d1_txt_2"><strong>Email:</strong><span> {(this.state.details.email)?this.state.details.email:'N/A'}</span></div>
                            <div className="Partt_d1_txt_2 my-3"><strong>D.O.B:</strong><span>{(this.state.details.dob)?this.state.details.dob:'N/A'}</span></div>
                            <div className="Partt_d1_txt_2 my-3"><strong>Marital Status:</strong><span>  {(marital_status[this.state.details.marital_status])?marital_status[this.state.details.marital_status]:'N/A'}</span></div>
                                <div className="Partt_d1_txt_2 my-3"><strong>Of Aboriginal or Torres
                                Strait Islander descent?:</strong><span>{this.state.details.aboriginal_tsi}</span></div>
                            </div>

                            <div className="col-md-6">
                            <div className="Partt_d1_txt_2 my-3"><strong>Living Situation: </strong> <span>{(this.state.details.living_situation)?this.state.details.living_situation:'N/A'}</span></div>
                                        <div className="Partt_d1_txt_2 my-3"><strong>Primary Address: </strong> <span>{(this.state.details.primary_address)?this.state.details.primary_address:'N/A'}</span></div>
                                        <div className="Partt_d1_txt_2 my-3"><strong>Secondary Address: </strong> <span>{(this.state.details.secondary_address)?this.state.details.secondary_address:'N/A'}</span></div>
                                        <div className="Partt_d1_txt_2 my-3"><strong>Next Of Kin:</strong><span> {this.state.details.kin_fullname} ({this.state.details.kin_relation})</span></div>
                                        <div className="Partt_d1_txt_2"><strong>Email:</strong><span>{(this.state.details.kin_email)?this.state.details.kin_email:'N/A'}</span></div>
                                                <div className="Partt_d1_txt_2"><strong>Phone:</strong><span> {(this.state.details.kin_phone)?this.state.details.kin_phone:'N/A'}</span></div>
                                                <div className="Partt_d1_txt_2"><strong>Address:</strong><span>87 Flinders Cresent, Melton, VIC, 3000</span></div>                                           
                            </div>

                             <div className="col-md-3 Parti_details_div_3">
                                <div className="Partt_d1_txt_1"><strong>Reference Details:</strong></div>
                                <div className="Partt_d1_txt_2 my-3"><strong>{(this.state.details.ref_fullName)?this.state.details.ref_fullName:'N/A'}</strong></div>
                                <div className="Partt_d1_txt_2 my-3"><strong>Email:</strong><span> {(this.state.details.referral_email)?this.state.details.referral_email:'N/A'}</span></div>
                                <div className="Partt_d1_txt_2 my-3"><strong>Phone:</strong><span>{(this.state.details.referral_phone)?this.state.details.referral_phone:'N/A'}</span></div>
                                <div className="Partt_d1_txt_2"><strong>Organisation:</strong><span>{(this.state.details.referral_org)?this.state.details.referral_org:'N/A'}</span></div>
                                <div className="Partt_d1_txt_2"><strong>Relationship to Participant:</strong></div>
                                <div className="Partt_d1_txt_2"><span> {(this.state.details.referral_relation)?this.state.details.referral_relation:'N/A'}</span></div>
                            </div>

                             </div>
                        </div>
                    </div>
                </div>

                <div className="row mb-5">
                    <div className="col-lg-10 col-lg-offset-1 mt-5">
                        <div className="V4_pro_d3__">
                        <PanelGroup accordion id="accordion-controlled-example" activeKey={this.state.activeKey} onSelect={this.handleSelect}>
                            <Panel eventKey="1">
                                    <Panel.Heading>
                                        <Panel.Title toggle className="v4_panel_title_ mb-0">
                                            <div>
                                                <div className="Partt_d1_txt_1"><strong>FMS Cases</strong></div> 
                                            <i className="more-less glyphicon glyphicon-plus"></i>
                                            </div>
                                        </Panel.Title>
                                    </Panel.Heading>

                                    <Panel.Body collapsible className="px-1 py-3">
                                    <div className="col-md-12 schedule_listings">
                                        <ReactTable
                                            data={data}
                                            columns={columns}
                                            onPageSizeChange={this.onPageSizeChange}
                                            defaultPageSize={2}
                                            showPagination={false}
                                            previousText={<span className="icon icon-arrow-left privious"></span>}
                                            nextText={<span className="icon icon-arrow-right next"></span>}
                                            SubComponent={(props) =><div className="other_conter">
                                                sdafdsafsdfasdfadsff dfasdfasd fasdfasd
                                            </div> 
                                        }
                                             />

                                    </div>
                                    </Panel.Body>
                            </Panel>
                        </PanelGroup>
                        </div>
                    </div>
                </div>


                   
                <div className="row mb-5">
                    <div className="col-lg-10 col-lg-offset-1 mt-5">
                        <div className="v4_pro_d2__">
                            <div className="row">
                                <div className="col-md-6 pt-5 pb-3">
                                <div class="Partt_d1_txt_1"><strong> Plans and Attachments:</strong><span></span></div>
                                </div>
                                <div className="col-md-3 pt-3">
                                    <a className="btn-1">
                                        Manage  Attachments
                                    </a>
                                </div>
                                <div className="col-md-3 pt-3">
                                    <a className="btn-1">
                                        Add  Attachments
                                    </a>
                                </div>
                            </div>
                            <div className="row"><div className="col-md-12"><div className="bt-1"></div></div></div>
                            <div className="row mt-5">
                            <div className="col-md-6 br-1">
                                <div className="row">
                                    <div className="col-md-6 pl-5 pr-5">
                                        <div className="my-5">
                                            <div className="Partt_d1_txt_2 my-3"><strong>NDIS Plan:</strong></div>
                                            <div className="my-3"><a className="v-c-btn1 n2"><span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i></a></div>
                                        </div>
                                        <div  className="my-5">
                                            <div className="Partt_d1_txt_2 my-3"><strong>Current behavioural Support plan?:</strong></div>
                                            <div className="my-3">yes</div>
                                            <div ><a className="v-c-btn1 n2"><span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i></a></div>
                                        </div>
                                        <div  className="my-5">
                                            <div className="Partt_d1_txt_2 my-3"><strong>Any Other relevant plans?:</strong></div>
                                            <div className="my-3">yes</div>
                                            <div ><a className="v-c-btn1 n2"><span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i></a></div>
                                        </div>
                                    </div> 
                                    <div className="col-md-6 pl-5 pr-5">
                                        <div className="my-5">
                                            <div className="Partt_d1_txt_2 my-3"><strong>Service Agreement Doc:</strong></div>
                                            <div className="my-3"><a className="v-c-btn1 n2"><span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i></a></div>
                                        </div>
                                        <div  className="my-5">
                                            <div className="Partt_d1_txt_2 my-3"><strong>Funding Consent Doc</strong></div>
                                            <div ><a className="v-c-btn1 n2"><span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i></a></div>
                                        </div>
                                        <div  className="my-5">
                                            <div className="Partt_d1_txt_2 my-3"><strong>Final Service agreement:</strong></div>
                                            <div ><a className="v-c-btn1 n2"><span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i></a></div>
                                        </div>
                                    </div> 
                                </div>
                            </div>                            
                            <div className="col-md-6">
                                <div className="row">
                                    <div className="col-md-12 pl-5 pr-5 mt-5">
                                     <div className="Partt_d1_txt_2"><strong>NDIS Plan:</strong></div>
                                     </div>
                                    <div className="col-md-6 pl-5 pr-5 my-3">
                                         <div ><a className="v-c-btn1 n2"><span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i></a></div>
                                    </div>
                                    <div className="col-md-6 pl-5 pr-5 my-3">
                                         <div ><a className="v-c-btn1 n2"><span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i></a></div>
                                    </div>
                                    <div className="col-md-6 pl-5 pr-5 my-3">
                                         <div ><a className="v-c-btn1 n2"><span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i></a></div>
                                    </div>
                                    <div className="col-md-6 pl-5 pr-5 my-3">
                                         <div ><a className="v-c-btn1 n2"><span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i></a></div>
                                    </div>
                                    <div className="col-md-6 pl-5 pr-5 my-3">
                                         <div ><a className="v-c-btn1 n2"><span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i></a></div>
                                    </div>
                                    <div className="col-md-6 pl-5 pr-5 my-3">
                                         <div ><a className="v-c-btn1 n2"><span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i></a></div>
                                    </div>
                                    <div className="col-md-6 pl-5 pr-5 my-3">
                                         <div ><a className="v-c-btn1 n2"><span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i></a></div>
                                    </div> 
                                    <div className="col-md-6 pl-5 pr-5 my-3">
                                         <div ><a className="v-c-btn1 n2"><span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i></a></div>
                                    </div>
                                    <div className="col-md-6 pl-5 pr-5 my-3">
                                         <div ><a className="v-c-btn1 n2"><span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i></a></div>
                                    </div>
                                    <div className="col-md-6 pl-5 pr-5 my-3">
                                         <div ><a className="v-c-btn1 n2"><span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i></a></div>
                                    </div>
                                </div>
                            </div>                               
                            </div>
                        </div>
                    </div>
                </div>
{/* version 4 start */}




                <div className="row d-flex">



                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                    {/* <ReactPlaceholder showLoadingAnimation  type="media" ready={!this.state.loading} customPlaceholder={DetailsPage}>
                        <div className="row d-flex">
                            <div className="col-md-3 Parti_details_div_1">
                                <div className="Partt_d1_txt_1"><strong>{(this.state.details.FullName)?this.state.details.FullName:'N/A'}</strong><span></span></div>
                                <div className="Partt_d1_txt_2 my-3"><strong>D.O.B:</strong><span>{(this.state.details.dob)?this.state.details.dob:'N/A'}</span></div>
                                <div className="Partt_d1_txt_2 my-3"><strong>NDIS No.:</strong><span> {(this.state.details.ndis_num)?this.state.details.ndis_num:'N/A'}</span></div>
                                <div className="Partt_d1_txt_2 my-3"><strong>Medicare No.:</strong><span> {(this.state.details.medicare_num)?this.state.details.medicare_num:'N/A'}</span></div>
                                <div className="Partt_d1_txt_2"><strong>Phone:</strong><span> {(this.state.details.phone)?this.state.details.phone:'N/A'}</span></div>
                                <div className="Partt_d1_txt_2"><strong>Email:</strong><span> {(this.state.details.email)?this.state.details.email:'N/A'}</span></div>
                                <div className="Partt_d1_txt_2 my-3"><strong>Marital Status:</strong><span>  {(marital_status[this.state.details.marital_status])?marital_status[this.state.details.marital_status]:'N/A'}</span></div>
                                <div className="Partt_d1_txt_2 my-3"><strong>Of Aboriginal or Torres
                                Strait Islander descent?:</strong><span>{this.state.details.aboriginal_tsi}</span></div>
                                <span className="btn-3" onClick={()=>{this.showModal_PE("showModal")}}>Edit Participants Info</span>
                                <div className="s-def1 s1 mt-3">
                                    <Select
                                        name="view_by_status"
                                        options={getParticipantState(0)}
                                        required={true}
                                        simpleValue={true}
                                        searchable={false}
                                        clearable={false}
                                        onChange={(e) => this.setParticipantState(e)}
                                        value={this.state.filterVal}
                                    />
                                </div>
                            </div>
                            <div className="col-md-6 bl-1 br-1 Parti_details_div_2">
                                <div className="row">
                                   <div className="col-md-12">
                                        <div className="Partt_d1_txt_2 my-3"><strong>Living Situation: </strong> <span>{(this.state.details.living_situation)?this.state.details.living_situation:'N/A'}</span></div>
                                        <div className="Partt_d1_txt_2 my-3"><strong>Primary Address: </strong> <span>{(this.state.details.primary_address)?this.state.details.primary_address:'N/A'}</span></div>
                                        <div className="Partt_d1_txt_2 my-3"><strong>Secondary Address: </strong> <span>{(this.state.details.secondary_address)?this.state.details.secondary_address:'N/A'}</span></div>
                                    </div>

                                    <div className="col-md-12">
                                        <div className="bt-1 mt-2"></div>
                                        <div className="row d-flex mt-4 mb-4">
                                            <div className="col-md-3 br-1">
                                                <div className="Partt_d1_txt_2"><strong>Next Of Kin:</strong></div>
                                                <div className="Partt_d1_txt_2"><span> {this.state.details.kin_fullname} ({this.state.details.kin_relation})</span></div>
                                            </div>
                                            <div className="col-md-9">
                                                <div className="Partt_d1_txt_2"><strong>Email:</strong><span>{(this.state.details.kin_email)?this.state.details.kin_email:'N/A'}</span></div>
                                                <div className="Partt_d1_txt_2"><strong>Phone:</strong><span> {(this.state.details.kin_phone)?this.state.details.kin_phone:'N/A'}</span></div>
                                             <div className="Partt_d1_txt_2"><strong>Address:</strong><span>87 Flinders Cresent, Melton, VIC, 3000</span></div>
                                            </div>
                                        </div>
                                        <div className="bt-1 mt-2"></div>
                                    </div>

                                    <div className="col-md-12">
                                        <div className="row d-flex mt-4 mb-4">
                                        <div className="col-md-6">
                                                <div className="Partt_d1_txt_2"><strong>Current behaviouralsupport plan?:</strong></div>
                                                <div className="Partt_d1_txt_2"><span>{(status[this.state.details.behavioural_support_plan])?status[this.state.details.behavioural_support_plan]:'N/A'}</span></div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="Partt_d1_txt_2"><strong>Any other relevant plans?:</strong></div>
                                                <div className="Partt_d1_txt_2"><span>{(status[this.state.details.other_relevant_plans])?status[this.state.details.other_relevant_plans]:'N/A'}</span></div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6 col-md-offset-6">
                                                <a className="v-c-btn1 n2"  onClick={()=>{this.showModal_PE("showModal3")}}>
                                                    <span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col-md-3 Parti_details_div_3">
                                <div className="Partt_d1_txt_1"><strong>Reference:</strong></div>
                                <div className="Partt_d1_txt_2 my-3"><strong>{(this.state.details.ref_fullName)?this.state.details.ref_fullName:'N/A'}</strong></div>
                                <div className="Partt_d1_txt_2 my-3"><strong>Email:</strong><span> {(this.state.details.referral_email)?this.state.details.referral_email:'N/A'}</span></div>
                                <div className="Partt_d1_txt_2 my-3"><strong>Phone:</strong><span>{(this.state.details.referral_phone)?this.state.details.referral_phone:'N/A'}</span></div>
                                <div className="Partt_d1_txt_2"><strong>Organisation:</strong><span>{(this.state.details.referral_org)?this.state.details.referral_org:'N/A'}</span></div>
                                <div className="Partt_d1_txt_2"><strong>Relationship to Participant:</strong></div>
                                <div className="Partt_d1_txt_2"><span> {(this.state.details.referral_relation)?this.state.details.referral_relation:'N/A'}</span></div>

                                <div className="Partt_d1_txt_2 mt-3"><strong> Plan Management:</strong></div>
                                <div className="s-def1 s1 mt-2 mb-5">
                                    <Select
                                        name="view_by_status"
                                        options={options}
                                        required={true}
                                        simpleValue={true}
                                        searchable={false}
                                        clearable={false}
                                        placeholder="Filter by: Unread"
                                        onChange={(e) => this.setState({ filterVal: e })}
                                        value={this.state.filterVal}
                                    />
                                </div>


                                <div className="Partt_d1_txt_1 bt-1 pt-4"><strong>Assigned to:</strong></div>
                                <div className="Partt_d1_txt_2 my-3"><strong> {(this.state.details.assigned_to)?this.state.details.assigned_to:'N/A'}</strong></div>
                                <div className="Partt_d1_txt_2 my-3"><strong>Department:</strong><span> {(this.state.details.participant_department)?this.state.details.participant_department:'N/A'}</span></div>

                            </div>
                        </div>

                        <div className="row d-flex bt-1 mt-4 pt-4">
                            <div className="col-md-3 br-1">
                                <div className="Partt_d1_txt_1"><strong>Attachments:</strong></div>
                                <div className="Partt_d1_txt_2 mt-3">NDIS Plan:</div>
                                <div className="mt-2">
                                    <a className="v-c-btn1 n2">
                                        <span>NDIS-Plan.PDF</span> <i className="icon icon-view1-ie"></i>
                                    </a>
                                </div>
                            </div>
                            <div className="col-md-9">
                                <div className="row pt-5 pb-3">
                                    <div className="col-md-4">
                                        <div className="Partt_d1_txt_2 mt-3">Other Documents:</div>
                                        <div className="mt-2">
                                        <a className="v-c-btn1 n2" onClick={()=>{this.showModal_PE("showModal1")}}>
                                                <span>Document 1</span> <i className="icon icon-view1-ie"></i>
                                            </a>
                                        </div>
                                        <div className="mt-4">
                                        <a className="v-c-btn1 n2"  onClick={()=>{this.showModal_PE("showModal1")}}>
                                                <span>Document 2</span> <i className="icon icon-view1-ie"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="mt-5">
                                        <a className="v-c-btn1 n2"  onClick={()=>{this.showModal_PE("showModal1")}}>
                                                <span>Document 3</span> <i className="icon icon-view1-ie"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div className="col-md-4">

                                        <div className="mt-5">
                                        <div className="upload_btn">
                                            <label className="btn-file">
                                                <div className="v-c-btn1 n2"><span>Browse</span><i className="icon icon-export1-ie" aria-hidden="true"></i></div>
                                                <input className="p-hidden" type="file" />
                                            </label>
                                        </div>
                                        <a className="btn-3 mt-2" onClick={()=>this.showModal_PE('showModal_PE')}>
                                                Edit Attachments
                                                    </a>

                                         <div className={this.state.showModal_PE ? 'customModal show' : 'customModal'} style={{ zIndex: '2' }}>
                                         <form method="POST" id="crm_participant_stage_docs" encType="multipart/form-data">
                                                  <div className="custom-modal-dialog Information_modal">
                                                      <div className="custom-modal-header bb-1">
                                                          <div className="Modal_title">Particpant Details - Edit Attachments</div>
                                                          <i className="icon icon-close3-ie Modal_close_i" onClick={()=>this.closeModal_PE('showModal_PE')}></i>
                                                      </div>

                                                      <div className="custom-modal-body mx-auto w-85">
                                                          <div className="row mx-0 my-4">
                                                              <div class="col-md-12 py-4 px-0 title_sub_modal">Current Attachments</div>
                                                          </div>
                                                          <div className="row">
                                                              <div className="col-md-8">
                                                                  <label class="title_input mb-0 ml-0 pl-0">Stage 1 Docs: </label>
                                                                  <div className="bb-1 d-block"></div></div>
                                                              <div className="col-md-4">
                                                                  <div className="s-def1">
                                                                      <Select
                                                                          name="view_by_status"
                                                                          options={options}
                                                                          required={true}
                                                                          simpleValue={true}
                                                                          searchable={false}
                                                                          clearable={false}
                                                                          placeholder="11"
                                                                          onChange={(e) => this.setState({ filterVal: e })}
                                                                          value={this.state.filterVal}
                                                                      />
                                                                  </div>
                                                              </div>
                                                          </div>

                                                          <div className="row">
                                                              <div className="col-md-12">
                                                                  <ul className="file_down quali_width P_15_TB">

                                                                  	{this.state.documents.map((documents, i) =>  (
                                                                          <li data-id={documents.id}  onClick={(e)=>this.onClickFunction(documents.id)}
                                            className={`segmentsList${this.state.activeWord.indexOf(documents.id) !== -1 ? ' selected' : ''}`}>
                                                                              <div className="path_file mt-0 mb-4"><b>{documents.title}</b></div>
                                                                              <span className="icon icon-file-icons d-block"></span>
                                                                              <div className="path_file">{documents.file_path}</div>
                                                                          </li>
                                                                      ))}

                                                                  </ul>
                                                              </div>
                                                          </div>

                                                          <div className="row">
                                                              <div className="col-md-6">
                                                                  <div className="upload_btn mb-4 mt-2">
                                                                      <label className="btn btn-default btn-sm center-block btn-file">
                                                                          <i className="but" aria-hidden="true">Upload New Doc(s)</i>
                                                                          <input className="p-hidden" type="file" onChange={this.fileChangedHandler} data-rule-required="true" date-rule-extension="jpg|jpeg|png|xlx|xls|doc|docx|pdf" multiple/>
                                                                      </label>
                                                                  </div>
                                                                  <a className="btn-1 mb-4" onClick={(e) =>this.archiveDocuments()} >Archive Selected Documents</a>
                                                                  <a className="btn-1 mb-4" onClick={(e) =>this.downloadDocuments()}>Download Selected Documents</a>
                                                              </div>
                                                              <div className="col-md-6">
                                                                  <div class="title_sub_modal">New Documents Information</div>
                                                                  <div className="mt-3">
                                                                      <label class="title_input mb-0">Doc Title: </label>
                                                                      <span className="required">
                                                                      <input name="docsTitle" onChange={(e) => this.setState({ 'docsTitle': e.target.value })} value={(this.state.docsTitle) ? this.state.docsTitle : ''} data-rule-required="true" />
                                                                      </span>
                                                                  </div>
                                                                  <div>
                                                                      <div className="">
                                                                          <label class="title_input mb-0">Doc Category: </label>
                                                                          <div className="s-def1 required">
                                                                              <Select
                                                                                  name="category"
                                                                                  options={this.state.stage_info}
                                                                                  required={true}
                                                                                  simpleValue={true}
                                                                                  searchable={false}
                                                                                  clearable={false}
                                                                                  placeholder="Select Document Category"
                                                                                  onChange={(e) => this.setState({ category: e })}
                                                                                  value={this.state.category}
                                                                              />
                                                                          </div>
                                                                      </div>
                                                                      <div className="my-3">
                                                                          <div className="Doc_D1_01">
                                                                              <div className="Doc_D1_02">
                                                                                  <label class="title_input mb-0 pl-0  w-100 pb-2">File Name: </label>
                                                                                  <div className="Doc_D1_03"><i  className="icon icon-document2-ie"></i>   {(this.state.filename) ?  <small>{this.state.filename}</small> : <small>No files selected</small>}</div>
                                                                              </div>
                                                                              <i className="icon icon-close3-ie Doc_D1_04"></i>
                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>

                                              <div className="custom-modal-footer bt-1 mt-5">
                                                      <div className="row d-flex justify-content-end">
                                                          <div className="col-md-5"><a className="btn-1" onClick={(e)=>this.uploadHandler(e,"crm_participant_stage_docs")}>Apply Changes</a></div>
                                                      </div>
                                                  </div>

                                          </div>
                                                  </form>
                    </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </ReactPlaceholder> */}
                        <div className="row">
                            <div className="col-md-12">
                                <div className="Partt_d1_txt_1 mt-4 bt-1 pt-4"><strong>Intake Progress:</strong></div>
                                <Timeline intakeFormOpen={this.showModal2} paricipantId={this.state.participant_id} latestStageState={this.state.latestStageState}  />

                            </div>
                        </div>


                    </div>


                   
                  <EditParticipant selectedFile={this.state.selectedFile} crmParticipantId={this.state.participant_id} editparticipant={this.state.details} closeingModel={()=>this.closeModal_PE("showModal")} docsTitleFun={(e)=>this.setState({ 'docsTitle': e.target.value })} closeModal={()=>this.closeModal_PE("showModal")} showModal={this.state.showModal} fileChange={this.fileChangedHandler} docsTitle={this.state.docsTitle} filename={this.state.filename} />


        {/*  upload doc model strat */}

        <Modal  className="modal fade Modal_A  Modal_B Crm" show={this.state.showModal1} onHide={()=>this.closeModal_PE("showModal1")}  >
                        <form id="crm_participant_doc" method="post" autoComplete="off">
                            <Modal.Body>
                                <div className="dis_cell">
                                    <div className="text text-left">Relevant Attachments:
                               <a data-dismiss="modal" aria-label="Close" className="close_i pull-right mt-1"  onClick={()=>this.closeModal_PE("showModal1")}><i className="icon icon-cross-icons"></i></a>
                                    </div>

                                    <div className="row P_15_T">
                                        <div className="col-md-8">
                                            <div className="row P_15_T">
                                                <div className="col-md-12">
                                                    <label>Title</label>
                                                    <span className="required">
                                                        <input type="text" placeholder="Please Enter Your Title" onChange={(e) => this.setState({ 'docsTitle': e.target.value })} value={(this.state.docsTitle) ? this.state.docsTitle : ''} data-rule-required="true"/>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row P_15_T">
                                        <div className="col-md-12"> <label>Please select a file to upload</label></div>
                                        <div className="col-md-5">
                                            <span className="required upload_btn">
                                                <label className="btn btn-default btn-sm center-block btn-file">
                                                    <i className="but" aria-hidden="true">Upload New Doc(s)</i>
                                                    <input className="p-hidden" type="file" onChange={this.fileChangedHandler}  data-rule-required="true" date-rule-extension="jpg|jpeg|png|xlx|xls|doc|docx|pdf" />
                                                </label>
                                            </span>
                                            {(this.state.filename) ? <p>File Name: <small>{this.state.filename}</small></p> : ''}

                                        </div>
                                        <div className="col-md-7"></div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-7"></div>
                                        <div className="col-md-5">
                                            <a className="btn-1"  onClick={(e)=>this.uploadHandler(e,"crm_participant_doc")}>Save</a>
                                        </div>
                                    </div>

                                </div>
                            </Modal.Body>
                        </form>
                    </Modal>
                     {/*  upload doc model end */}
                    <div className={this.state.showModal2 ? 'customModal show' : 'customModal'}>
                    <ReactPlaceholder showLoadingAnimation  type="media" ready={!this.state.loading} customPlaceholder={IntakeProcess}>
                        <div className="custom-modal-dialog Information_modal">
                            <div className="custom-modal-header bb-1">
                                <div className="Modal_title stage" >{this.state.stage_name} {(this.state.stageId !=6)?"Information":""}</div>
                                <i className="icon icon-close3-ie Modal_close_i" onClick={()=>this.closeModal_PE("showModal2")}></i>
                            </div>

                            <div className="custom-modal-body w-85 mx-auto">
                                <div className="row">

                                    <div className="col-md-12">
                                        <div className="py-4 title_sub_modal">Participant Details</div>
                                    </div>


                                    <div className="col-md-12 all_notes">
                                        <div className="all_notes_1 horizontal_scroll">
                                      { (this.state.notesList !='')?
                                          this.state.notesList.map((noteInfo, i) =>  (
                                            <div className="single_notes" key={noteInfo.id}>
                                                <div className="flex_break">
                                                    <div className="single_note_data">
                                                        <p >{noteInfo.notes}</p>
                                                        <div className="text-right">
                                                            <a className="icon icon-add1-ie add1_a1"></a>
                                                            <a className="icon icon-imail1-ie"></a>
                                                            <a  className="icon icon-archive5-ie" onClick={() =>this.deleteNote(noteInfo.id)}></a>
                                                        </div>
                                                    </div>
                                                    <div className="Single_note_history">Date:{moment(noteInfo.created_at).format('DD/MM/YYYY')}</div>
                                                </div>
                                            </div>
                                           	)):<p>No Records</p>}



                                        </div>
                                    </div>
                                </div>


                                <div className="row ">
                                    <div className="col-md-12 py-4"><div className="bt-1"></div></div>
                                    <div className="col-md-12 pt-1 pb-4 title_sub_modal">Add New Note</div>
                                </div>
                                <form id="add_note">
                                <div className="row d-flex  mb-4">
                                    <div className="col-md-8">
                                        <textarea data-rule-required='true' data-msg-required="Add Note" placeholder="Note" className='notes_txt_area textarea-max-size' name="notes_txt_area" onChange={(e) =>  this.setState({notes_txt_area:e.target.value})}>{this.state.notes_txt_area}</textarea>
                                    </div>
                                    <div className="col-md-3 align-items-end d-inline-flex"><a className="btn-1 w-100" onClick={this.submitNote}>Add New Note</a></div>
                                </div>
                                </form>


                                <div className="row">
                                    <div className="col-md-12 py-4"><div className="bt-1"></div></div>
                                    <div className="col-md-12 pt-1 pb-4 title_sub_modal">Attachments</div>
                                </div>

                                <div className="row">
                                    <div className="col-md-3 mb-4">
                                        <label className="title_input">NDIS Plan Document: </label>
                                        <a className="v-c-btn1">
                                            <span>Document 1</span> <i className="icon icon-view1-ie"></i>
                                        </a>
                                    </div>
                                    <div className="col-md-3 mb-4">
                                        <label className="title_input">Agreement Document: </label>
                                        <a className="v-c-btn1">
                                            <span>Document 1</span> <i className="icon icon-view1-ie"></i>
                                        </a>
                                    </div>
                                    <div className="col-md-3 mb-4">
                                        <label className="title_input">Signed Concent Document: </label>
                                        <a className="v-c-btn1">
                                            <span>Document 1</span> <i className="icon icon-view1-ie"></i>
                                        </a>
                                    </div>
                                </div>

                                <div className="row d-flex mb-5">
                                    <div className="col-md-9 align-items-end d-inline-flex"><div className="bt-1 w-100"></div></div>
                                    <div className="col-md-3">
                                        <div className="upload_btn">
                                            <label className="btn-file">
                                                <div className="v-c-btn1"><span>Browse</span><i className="icon icon-export1-ie" aria-hidden="true"></i></div>
                                                <input className="p-hidden" type="file" />
                                            </label>
                                        </div>
                                    </div>
                                </div>





                            </div>

                            <div className="custom-modal-footer bt-1 mt-5">
                                <div className="row d-flex justify-content-end">
                                    <div className="col-md-3"><a className="btn-1">Apply Changes</a></div>
                                </div>
                            </div>

                        </div>
                        </ReactPlaceholder>
                    </div>{/* Modal 2 end */}



                    <div className={this.state.showModal3 ? 'customModal show' : 'customModal'}>
                    <ReactPlaceholder showLoadingAnimation  type="media" ready={!this.state.loading} customPlaceholder={IntakeProcess}>
                        <div className="custom-modal-dialog Information_modal">
                            <div className="custom-modal-header bb-1">
                                <div className="Modal_title">Plan Delegation - Services</div>
                                <i className="icon icon-close3-ie Modal_close_i" onClick={()=>this.closeModal_PE("showModal3")}></i>
                            </div>

                            <div className="custom-modal-body w-80 mx-auto">

                                <div className="row">
                                    <div className="col-md-12 pt-4 pb-3 title_sub_modal">Services Search:</div>
                                    <div className="col-md-8">
                                        <div className="small-search">
                                            <input />
                                            <button><span className="icon icon-search1-ie"></span></button>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="row">
                                            <div className="col-md-8 pt-3 text-right">
                                                ter by Code:
                                            </div>
                                            <div className="col-md-4 pt-2 pl-0">
                                                <div className="s-def1 s1">
                                                    <Select
                                                        name="view_by_status"
                                                        options={options}
                                                        required={true}
                                                        simpleValue={true}
                                                        searchable={false}
                                                        clearable={false}
                                                        placeholder="11"
                                                        onChange={(e) => this.setState({ filterVal: e })}
                                                        value={this.state.filterVal}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 py-4"><div className="border-das_line"></div></div>
                                </div>

                                <div className="row">
                                    <div className="col-md-12 d-flex justify-content-between Ser_div_txt1">
                                        <div className="pl-5">Service Name: </div>
                                        <div>Attach to Plan: </div>
                                    </div>
                                    <div className="col-md-12 mb-3">
                                        <div className="Ser_div1">
                                            <div className="col-md-11 d-inline-flex align-self-center pl-5"><b>11_022_0110_7_3:</b> Specialist Behavioural Intervention Support</div>
                                            <div className="col-md-1 text-left bl-1 my-2">
                                                <label className="c-custom-checkbox pt-1">
                                                    <input type="checkbox" />
                                                    <i className="c-custom-checkbox__img"></i>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 mb-3">
                                        <div className="Ser_div1">
                                            <div className="col-md-11 d-inline-flex align-self-center pl-5"><b>11_022_0110_7_3:</b> Behaviour Management Plan (Incl. Training In Behaviour Management Strategies)</div>
                                            <div className="col-md-1 text-left bl-1 my-2">
                                                <label className="c-custom-checkbox pt-1">
                                                    <input type="checkbox" />
                                                    <i className="c-custom-checkbox__img"></i>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 mb-3">
                                        <div className="Ser_div1">
                                            <div className="col-md-11 d-inline-flex align-self-center pl-5"><b>11_024_0117_7_3:</b>  Individual Social Skills Development</div>
                                            <div className="col-md-1 text-left bl-1 my-2">
                                                <label className="c-custom-checkbox pt-1">
                                                    <input type="checkbox" />
                                                    <i className="c-custom-checkbox__img"></i>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div className="row">
                                    <div className="col-md-12 pt-4"><div className="bt-1"></div></div>
                                    <div className="col-md-12 pt-4 pb-3 title_sub_modal">Allocate Ammounts to Selected Services:</div>
                                </div>

                                <div className="row">
                                    <div className="col-md-12 d-flex justify-content-between Ser_div_txt1"><div className="pl-5">Service Name: </div><div></div></div>
                                </div>

                                <div className="row mb-4">
                                    <div className="col-md-12 pb-4"><div className="border-das_line"></div></div>
                                    <div className="col-md-10 Ser_sel_div">
                                        <div className="sel_div1 pl-5"><b>11_024_0117_7_3:</b> Individual Social Skills Development</div>
                                        <div className="sel_div2 col-md-5 ml-5 mt-3">
                                            <div className="row sel_div3">
                                                <div className="col-md-6 allocate_title">Allocate Funds:</div>
                                                <div className="col-md-6 dollar_input">
                                                    <input /><span>$:</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-2"><a className="btn-1 s_txt">Add</a></div>
                                </div>

                                <div className="row  mb-4">
                                    <div className="col-md-12 pb-4"><div className="border-das_line"></div></div>
                                    <div className="col-md-10 Ser_sel_div">
                                        <div className="sel_div1 pl-5"><b>11_024_0117_7_3:</b> Individual Social Skills Development</div>
                                        <div className="sel_div2 col-md-5 ml-5 mt-3">
                                            <div className="row sel_div3">
                                                <div className="col-md-6 allocate_title">Allocate Funds:</div>
                                                <div className="col-md-6 dollar_input">
                                                    <input /><span>$:</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-2"><a className="btn-1 s_txt">Add</a></div>
                                </div>

                                <div className="row">
                                    <div className="col-md-12 pt-4"><div className="bt-1"></div></div>
                                    <div className="col-md-12 pt-4 pb-3 title_sub_modal">Selected Services & Funding:</div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12 mb-3">
                                        <div className="d-flex ser_fund_1">
                                            <div className="ser_fund_a"><span><b>11_024_0117_7_3:</b> Individual Social Skills Development</span></div>
                                            <div className="ser_fund_b"><span>Allocated Funding: $3,500</span> <a className="btn-3">remove</a></div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 mb-3">
                                        <div className="d-flex ser_fund_1">
                                            <div className="ser_fund_a"><span><b>11_024_0117_7_3:</b> Individual Social Skills Developmentt</span></div>
                                            <div className="ser_fund_b"><span>Allocated Funding: $3,500</span> <a className="btn-3">remove</a></div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 mb-3">
                                        <div className="d-flex ser_fund_1">
                                            <div className="ser_fund_a"><span><b>11_024_0117_7_3:</b> Individual Social Skills Developmentt</span></div>
                                            <div className="ser_fund_b"><span>Allocated Funding: $3,500</span> <a className="btn-3">remove</a></div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div className="custom-modal-footer bt-1 mt-5">
                                <div className="row d-flex justify-content-end">
                                    <div className="col-md-4"><a className="btn-1">Save and Send Servive Agreement</a></div>
                                </div>
                            </div>

                        </div>
                        </ReactPlaceholder>
                    </div>{/* Modal 3 end */}






                </div>





            </div>
        );

    }
}





class Timeline extends React.Component {
    constructor(props) {
        super(props);
        this.state = { filterVal: '',
        callReferenceModalShow : false,
        LockedFundingModalShow : false };
    }
    stage2Note = (id) => {
        this.props.intakeFormOpen(id);

    }

     closeCallRefModal(){
        this.setState({callReferenceModalShow:false});
    }

    showCallRefModal(){
        this.setState({callReferenceModalShow:true});
    }


    closeLockedFunModal(){
        this.setState({LockedFundingModalShow:false});
    }

    showLockedFunModal(){
        this.setState({LockedFundingModalShow:true});
    }
    componentWillMount(){
        this.getStageOption1(this.props.paricipantId);
    }
          // Get Status of Stage after on change
        getStageOption1 = (pid) => {
            var state = {};
            var intakeStr = JSON.stringify({'participant_id':pid});

                postData('crm/CrmStage/get_stage_option', intakeStr).then((result) => {

                    if(result){
                        Object.keys(result).map(function(key) {

                            switch(result[key].stage_id)
                            {

                                case '2':
                                state['intake']=result[key].status;
                                break;
                                case '3':
                                state['plan_delegation']=result[key].status;
                                break;
                                case '4':
                                state['participant_assessment']=result[key].status;
                                break;
                                case '5':
                                state['client_contact']=result[key].status;

                                break;
                                case '6':
                                state['information_screening']=result[key].status;
                                break;
                                case '7':
                                state['recieve_documents']=result[key].status;
                                break;
                                case '8':
                                state['service_agreement_doc']=result[key].status;
                                break;
                                case '9':
                                state['funding_concent']=result[key].status;
                                break;
                                case '10':
                                state['locked_funding_confirmation']=result[key].status;
                                break;
                                case '11':
                                state['send_service_agreement']=result[key].status;
                                break;
                                case '12':
                                state['engage_services_into_hcm']=result[key].status;
                                break;
                            }

                        });
                        this.setState(state);
                    }
                });


        }


        // On change of status change by stages
        onSelectStage = (selectedOption, stage_id, stageName) => {
        var state = {};
        state[stageName] = selectedOption;
        var statusData = JSON.stringify({'stage_id':stage_id, 'crm_participant_id':this.props.paricipantId, 'status':selectedOption});
        postData('crm/CrmStage/update_stage_status',statusData ).then((result) => {
            if(result.status){

                this.getStageOption1(this.props.paricipantId,stageName);
            }
        });
        this.setState(state);
        }
    render() {
        var options = [
            { value: '0', label: 'Pending' },
            { value: '1', label: 'Success' }
        ];
        return (
            <div className="row">



<PanelGroup accordion id="accordion-controlled-example" activeKey={this.state.activeKey} onSelect={this.handleSelect}>
    
                      


                <div className="Version_timeline_4 timeline_1">

                    <div className="time_l_1">
                        <div className="time_no_div">
                            <div className="time_no"><span>1</span></div>
                            <div className="line_h"></div>
                        </div>

                        

                        <div className="time_d_1" >
                            <div className="time_d_2">
                            <Panel eventKey="1">
                                <div className="time_txt w-100">
                                    <div className="time_d_style v4-1_">
                                    <Panel.Heading>
                                        <Panel.Title toggle className="v4_panel_title_ v4-2_ mb-0">
                                        <div className="timeline_h"><span>Stage 1</span><i className="icon icon-arrow-down"></i></div>
                                        <div className="timeline_h">NDIS</div>
                                        <div className="timeline_h">Intake Participant Submission</div>
                                        </Panel.Title>
                                 </Panel.Heading>

                                        <div className="task_table_v4-1__">
                                            <div className="t_t_v4-1">
                                                <div  className="t_t_v4-1a complete_msg">
                                                    <div className="ci_btn">Complete</div>
                                                    <div className="ci_date">Date: 01/11/2017 - 9:42am</div>
                                                </div>
                                                <div  className="t_t_v4-1b">
                                                    <a>View Attachments</a> <span>&</span> <a onClick={()=>this.stage2Note("2")}>Notes</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <Panel.Body collapsible className="px-1 py-3">
                                <div className="time_line_parent w-100">

                                </div>
                                </Panel.Body>

                            </Panel>


                            </div>
                        </div>
                    </div>

                    <div className="time_l_1">
                        <div className="time_no_div">
                            <div className="time_no"><span>2</span></div>
                            <div className="line_h"></div>
                        </div>

                        <div className="time_d_1">
                            <div className="time_d_2">
                            <Panel eventKey="2">
                                  
                                <div className="time_txt w-100">
                                    <div className="time_d_style v4-1_">
                                    <Panel.Heading>
                                        <Panel.Title toggle className="v4_panel_title_ v4-2_ mb-0">
                                        <div className="timeline_h"><span>Stage 2</span><i className="icon icon-arrow-down"></i></div>
                                        <div className="timeline_h">Intake</div>
                                        </Panel.Title>
                                 </Panel.Heading>

                                        <div className="task_table_v4-1__">
                                            <div className="t_t_v4-1">
                                                <div  className="t_t_v4-1a complete_msg">
                                                    <div className="ci_btn">Complete</div>
                                                    <div className="ci_date">Date: 01/11/2017 - 9:42am</div>
                                                </div>
                                                <div  className="t_t_v4-1b">
                                                    <a>View Attachments</a> <span>&</span> <a onClick={()=>this.stage2Note("2")}>Notes</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               

<Panel.Body collapsible className="px-1 py-3">
                                <div className="time_line_parent w-100">

                                    <div className={(this.props.latestStageState>=2)?" time_l_1":"disable-stage-pointer time_l_1"}>
                                        <div className="time_no_div">
                                            <div className="time_no">2.1</div>
                                            <div className="line_h"></div>
                                        </div>
                                        <div className="time_d_1">
                                            <div className="time_d_2">

                                                <div className="time_d_style">
                                                    <div className="timeline_h">Stage 2.1 - Participant Assessment</div>
                                                    <div className="d-flex">
                                                        <div className="time_txt">
                                                            <div className="s-def1 s1 col-md-8 col-md-offset-2">
                                                                <Select name="participant_assessment" options={getStagesStatus(0)} required={true} simpleValue={true} searchable={false} clearable={false} placeholder="Filter by: Unread" onChange={(e) =>this.onSelectStage(e,'4','participant_assessment')} value={this.state['participant_assessment']} />
                                                            </div>
                                                            <div className="task_table_footer col-md-12 pt-3 bt-1 mt-3"><a onClick={()=>this.stage2Note("4")}><u>Notes</u></a><span>Completed by: Jane Smith</span></div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div className="time_line_date">
                                                    Date: 01/01/01 - 9:42am
                                        </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div className={(this.props.latestStageState>=3)?"time_l_1":"disable-stage-pointer  time_l_1"}>
                                        <div className="time_no_div">
                                            <div className="time_no">2.2</div>
                                            <div className="line_h"></div>
                                        </div>
                                        <div className="time_d_1">
                                            <div className="time_d_2">

                                                <div className="time_d_style">
                                                    <div className="d-flex">

                                                        <div className="time_txt">
                                                            <div className="timeline_h">Stage 2.2 - Client Contact </div>
                                                            <div className="s-def1 s1 col-md-8 col-md-offset-2">
                                                                <Select name="client_contact" options={getStagesStatus(0)} required={true} simpleValue={true} searchable={false} clearable={false} placeholder="Filter by: Unread" onChange={(e) =>this.onSelectStage(e,'5','client_contact')} value={this.state['client_contact']}  />
                                                            </div>
                                                            <div className="task_table_footer col-md-12 pt-3 bt-1 mt-3"><a onClick={()=>this.stage2Note("5")}><u>Notes</u></a><span>Completed by: Jane Smith</span></div>
                                                        </div>
                                                        <div className="call_refer_div pl-2 ml-3 bl-1">
                                                            Call Reference
                                                    <br />
                                                          <a onClick={()=>{this.showCallRefModal()}}><i className="icon icon-call1-ie"></i></a>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div className="time_line_date">
                                                    Date: 01/01/01 - 9:42am
                                        </div>
                                         <CallReference showModal={this.state.callReferenceModalShow} handleClose={()=>this.closeCallRefModal()} />

                                            </div>
                                        </div>
                                    </div>

                                    <div className={(this.props.latestStageState>=5)?"time_l_1":"disable-stage-pointer time_l_1"}>
                                        <div className="time_no_div">
                                            <div className="time_no">2.3</div>
                                            <div className="line_h"></div>
                                        </div>
                                        <div className="time_d_1">
                                            <div className="time_d_2">

                                                <div className="time_d_style">
                                                    <div className="d-flex">

                                                        <div className="time_txt">
                                                            <div className="timeline_h">Stage 2.3 - Information Screening</div>
                                                            <div className="s-def1 s1 col-md-8 col-md-offset-2">
                                                                <Select name="information_screening" options={getStagesStatus(0)} required={true} simpleValue={true} searchable={false} clearable={false} placeholder="Filter by: Unread" onChange={(e) =>this.onSelectStage(e,'6','information_screening')} value={this.state['information_screening']} />
                                                            </div>
                                                            <div className="task_table_footer col-md-12 pt-3 bt-1 mt-3"><a onClick={()=>this.stage2Note("6")}><u>Notes</u></a><span>Completed by: Jane Smith</span></div>
                                                        </div>
                                                        <div className="call_refer_div pl-2 ml-3 bl-1">
                                                            Call Reference
                                                    <br />
                                                            <i className="icon icon-export1-ie"></i>
                                                            <br />
                                                            <a>View</a> | <a>Edit</a>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div className="time_line_date">
                                                    Date: 01/01/01 - 9:42am
                                        </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div className={(this.props.latestStageState>=6)?"time_l_1":"disable-stage-pointer  time_l_1"}>
                                        <div className="time_no_div">
                                            <div className="time_no">2.4</div>
                                            <div className="line_h"></div>
                                        </div>
                                        <div className="time_d_1">
                                            <div className="time_d_2">

                                                <div className="time_d_style">
                                                    <div className="timeline_h">Stage 2.4 - Recieve Documents</div>
                                                    <div className="d-flex">
                                                        <div className="time_txt">
                                                            <div className="s-def1 s1 col-md-8 col-md-offset-2">
                                                                <Select name="recieve_documents" options={getStagesStatus(0)} required={true} simpleValue={true} searchable={false} clearable={false} placeholder="Filter by: Unread" onChange={(e) =>this.onSelectStage(e,'7','recieve_documents')} value={this.state['recieve_documents']} />
                                                            </div>
                                                            <div className="task_table_footer col-md-12 pt-3 bt-1 mt-3"><a onClick={()=>this.stage2Note("7")}><u>Notes</u></a><span>Completed by: Jane Smith</span></div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div className="time_line_date">
                                                    Date: 01/01/01 - 9:42am
                                        </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                </Panel.Body>
                                </Panel>

                            </div>
                        </div>
                    </div>

                    <div className="time_l_1">
                        <div className="time_no_div">
                            <div className="time_no"><span>3</span></div>
                            <div className="line_h"></div>
                        </div>
                        <div className="time_d_1">
                            <div className="time_d_2">
                            <Panel eventKey="3">
                                <div className={(this.props.latestStageState>=7)?"time_txt w-100":"disable-stage-pointer  time_txt w-100"}>
                            
                                  
                                  <div className="time_txt w-100">
                                      <div className="time_d_style v4-1_">
                                      <Panel.Heading>
                                          <Panel.Title toggle className="v4_panel_title_ v4-2_ mb-0">
                                          <div className="timeline_h"><span>Stage 3</span><i className="icon icon-arrow-down"></i></div>
                                          <div className="timeline_h">Plan Delegation</div>
                                          </Panel.Title>
                                   </Panel.Heading>
  
                                          <div className="task_table_v4-1__">
                                              <div className="t_t_v4-1">
                                                  <div  className="t_t_v4-1a incomplete_msg">
                                                      <div className="ci_btn">In-Complete</div>
                                                      <div className="ci_date">Date: 01/11/2017 - 9:42am</div>
                                                  </div>
                                                  <div  className="t_t_v4-1b">
                                                      <a>View Attachments</a> <span>&</span> <a onClick={()=>this.stage2Note("3")}>Notes</a>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>

                                    {/* <div className="time_d_style">

                                        <div className="timeline_h">Stage 3 - Plan Delegation </div>
                                        <div className="d-flex  py-3">
                                            <div className="s-def1 s1 col-md-6">
                                                <Select name="view_by_status" options={options} required={true} simpleValue={true} searchable={false} clearable={false} placeholder="Filter by: Unread" onChange={(e) => this.setState({ filterVal: e })} value={this.state.filterVal} />
                                            </div>
                                            <div className="s-def1 s1 col-md-6">
                                                <Select name="plan_delegation" options={getStagesStatus(0)} required={true} simpleValue={true} searchable={false} clearable={false} placeholder="Filter by: Unread" onChange={(e) =>this.onSelectStage(e,'3','plan_delegation')} value={this.state['plan_delegation']} />
                                            </div>
                                        </div>
                                        <div className="d-flex py-3 bt-1 bb-1">
                                            <div className="col-md-4">
                                                <a className="v-c-btn1 n2"><span>Doc 1</span> <i className="icon icon-view1-ie"></i></a>
                                            </div>
                                            <div className="col-md-4">
                                                <a className="v-c-btn1 n2"><span>Doc 1</span> <i className="icon icon-view1-ie"></i></a>
                                            </div>
                                            <div className="col-md-4">
                                                <a className="v-c-btn1 n2"><span>Doc 1</span> <i className="icon icon-view1-ie"></i></a>
                                            </div>
                                        </div>
                                        <div className="task_table_footer pt-3"><a><u>Attach</u></a><a onClick={()=>this.stage2Note("3")}><u>Notes</u></a><span>Completed by: Jane Smith</span></div>


                                    </div>

                                    <div className="time_line_date">
                                        Date: 01/01/01 - 9:42am 
                            </div> */}
                                </div>

                                <Panel.Body collapsible className="px-1 py-3">
                                <div className="time_line_parent w-100">

                                    <div className={(this.props.latestStageState>=7)?"time_l_1":"disable-stage-pointer time_l_1"} >
                                        <div className="time_no_div">
                                            <div className="time_no">3.1</div>
                                            <div className="line_h"></div>
                                        </div>
                                        <div className="time_d_1">
                                            <div className="time_d_2">

                                                <div className="time_d_style">
                                                    <div className="d-flex">

                                                        <div className="time_txt">
                                                            <div className="timeline_h">Stage 3.1 - Service Agreement Doc</div>
                                                            <div className="s-def1 s1 col-md-8 col-md-offset-2">
                                                                <Select name="service_agreement_doc" options={getStagesStatus(0)} required={true} simpleValue={true} searchable={false} clearable={false} placeholder="Filter by: Unread"  onChange={(e) =>this.onSelectStage(e,'8','service_agreement_doc')} value={this.state['service_agreement_doc']} />
                                                            </div>
                                                            <div className="task_table_footer col-md-12 pt-3 bt-1 mt-3"><a onClick={()=>this.stage2Note("8")}><u>Notes</u></a><span>Completed by: Jane Smith</span></div>
                                                        </div>
                                                        <div className="call_refer_div pl-2 ml-3 bl-1">
                                                            Send Documents
                                                    <br />
                                                            <i className="icon icon-export1-ie"></i>
                                                            <br />
                                                            <a>View</a> | <a>Edit</a>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div className="time_line_date">
                                                    Date: 01/01/01 - 9:42am
                                        </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div className={(this.props.latestStageState>=8)?"time_l_1":"disable-stage-pointer time_l_1"}>
                                        <div className="time_no_div">
                                            <div className="time_no">3.2</div>
                                            <div className="line_h"></div>
                                        </div>
                                        <div className="time_d_1">
                                            <div className="time_d_2">

                                                <div className="time_d_style">
                                                    <div className="d-flex">

                                                        <div className="time_txt">
                                                            <div className="timeline_h">Stage 3.2 - Funding Concent</div>
                                                            <div className="col-md-6">
                                                                <a className="v-c-btn1 n2"><span>Doc 1</span> <i className="icon icon-view1-ie"></i></a></div>
                                                            <div className="s-def1 s1 col-md-6">
                                                                <Select name="funding_concent" options={getStagesStatus(0)} required={true} simpleValue={true} searchable={false} clearable={false} placeholder="Filter by: Unread" onChange={(e) =>this.onSelectStage(e,'9','funding_concent')} value={this.state['funding_concent']} />
                                                            </div>
                                                            <div className="task_table_footer col-md-12 pt-3 bt-1 mt-3"><a onClick={()=>this.stage2Note("9")}><u>Notes</u></a><span>Completed by: Jane Smith</span></div>
                                                        </div>
                                                        <div className="call_refer_div pl-2 ml-3 bl-1">
                                                            Send Document
                                                    <br />
                                                            <i className="icon icon-export1-ie"></i>
                                                            <br />
                                                            <a>View</a> | <a>Edit</a>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div className="time_line_date">
                                                    Date: 01/01/01 - 9:42am
                                        </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div className={(this.props.latestStageState>=9)?"time_l_1":"disable-stage-pointer time_l_1"}>
                                        <div className="time_no_div">
                                            <div className="time_no">3.3</div>
                                            <div className="line_h"></div>
                                        </div>
                                        <div className="time_d_1">
                                            <div className="time_d_2 d-flex">
                                                <div className="time_txt ">
                                                    <div className="time_d_style">
                                                        <div className="timeline_h">Stage 3.3 - Locked Funding Confirmation </div>
                                                        <div className="d-flex  py-3">
                                                            <div className="s-def1 s1 col-md-8 col-md-offset-2">
                                                                <Select name="locked_funding_confirmation" options={getStagesStatus(0)} required={true} simpleValue={true} searchable={false} clearable={false} placeholder="Filter by: Unread" onChange={(e) =>this.onSelectStage(e,'10','locked_funding_confirmation')} value={this.state['locked_funding_confirmation']} />
                                                            </div>
                                                        </div>

                                                        <div className="task_table_footer bt-1 mt-3"><a onClick={()=>this.stage2Note("10")}><u>Notes</u></a><span>Completed by: Jane Smith</span></div>
                                                    </div>
                                                    <div className="time_line_date">
                                                        Date: 01/01/01 - 9:42am
                                            </div>
                                                </div>

                                                <div className="">

                                                    <span className="reminder_icon">
                                                          <a onClick={()=>{this.showLockedFunModal()}}>
                                                            <i className="icon icon-warning2-ie"></i>
                                                            <i className="icon icon-circle1-ie"></i>
                                                        </a>
                                                        <LockedFunding showModal={this.state.LockedFundingModalShow} handleClose={()=>this.closeLockedFunModal()} />

                                                    </span>

                                                    <h5 className="pl-3 txt_sms">SMS<br /> Reminder</h5>

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div className={(this.props.latestStageState>=10)?"time_l_1":"disable-stage-pointer time_l_1"}>
                                        <div className="time_no_div">
                                            <div className="time_no">3.4</div>
                                            <div className="line_h"></div>
                                        </div>
                                        <div className="time_d_1">
                                            <div className="time_d_2">

                                                <div className="time_d_style">
                                                    <div className="d-flex">

                                                        <div className="time_txt">
                                                            <div className="timeline_h">Stage 3.4 - Send Service Agreement</div>
                                                            <div className="s-def1 s1 col-md-8 col-md-offset-2">
                                                                <Select name="send_service_agreement" options={getStagesStatus(0)} required={true} simpleValue={true} searchable={false} clearable={false} placeholder="Filter by: Unread" onChange={(e) =>this.onSelectStage(e,'11','send_service_agreement')} value={this.state['send_service_agreement']}  />
                                                            </div>
                                                            <div className="task_table_footer col-md-12 pt-3 bt-1 mt-3"><a onClick={()=>this.stage2Note("11")}><u>Notes</u></a><span>Completed by: Jane Smith</span></div>
                                                        </div>
                                                        <div className="call_refer_div pl-2 ml-3 bl-1">
                                                            Call Reference
                                                    <br />
                                                            <i className="icon icon-export1-ie"></i>
                                                            <br />
                                                            <a>View</a> | <a>Edit</a>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div className="time_line_date">
                                                    Date: 01/01/01 - 9:42am
                                        </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div className={(this.props.latestStageState>=11)?"time_l_1":"disable-stage-pointer time_l_1"}>
                                        <div className="time_no_div">
                                            <div className="time_no">3.5</div>
                                            <div className="line_h"></div>
                                        </div>
                                        <div className="time_d_1">
                                            <div className="time_d_2">

                                                <div className="time_d_style">
                                                    <div className="d-flex">

                                                        <div className="time_txt">
                                                            <div className="timeline_h">Stage 3.5 - Engage Services Into HCM</div>
                                                            <div className="s-def1 s1 col-md-8 col-md-offset-2">
                                                                <Select name="engage_services_into_hcm" options={getStagesStatus(0)} required={true} simpleValue={true} searchable={false} clearable={false} placeholder="Filter by: Unread" onChange={(e) =>this.onSelectStage(e,'12','engage_services_into_hcm')} value={this.state['engage_services_into_hcm']}  />
                                                            </div>
                                                            <div className="task_table_footer col-md-12 pt-3 bt-1 mt-3"><a onClick={()=>this.stage2Note("12")}><u>Notes</u></a><span>Completed by: Jane Smith</span></div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div className="time_line_date">
                                                    Date: 01/01/01 - 9:42am
                                        </div>

                                            </div>
                                        </div>
                                    </div>
                                   
                                </div> 
                                </Panel.Body>
                                </Panel>
                                

                            </div>
                        </div>
                    </div>

                </div>
                </PanelGroup>
            </div>

        );

    }
}



class EditParticipant extends React.Component {
    constructor(props) {
        super(props);
        this.kindetails = [{ name: '', lastname: '', contact: '', email: '', relation: '', relation_error: '', name_error: '', lastname_error: '', contact_error: '', email_error: '' }];

        this.state = {
          filterVal: '',
          showModal: false,
          ParticipantSituation: '',
          Preferredcontacttype: 1,
          ParticipantTSI: 1,
          second_step: false,
          PhoneInput: [{ name: '' }],
          EmailInput: [{ name: '' }],
          AddressInput: [{ type: '', city: 'ewe', state: 'sdf', street: '', postal: '98763', type_error: false }],
          kindetails: [{ name:'', lastname: '', contact: '', email: '', relation: '' }],
          bookerdetails: [{ name: '', lastname: '', contact: '', email: '', relation: '' }],
          success: false,
          participantReferral: [{ value: 1, label: 'Yes' }, { value: 2, label: 'No' }],
          gender_option: [{ value: 1, label: 'Male' }, { value: 2, label: 'Female' }],
          department_option: [{ value: 1, label: 'HCM' }, { value: 2, label: 'Healthcare' }],
          contact_option: [{ value: 1, label: 'Phone' }, { value: 2, label: 'Email' }],
          to_org_option: [{ value: 1, label: 'To Org' }, { value: 2, label: 'To House' }],
          stateList: [],
          referral: 1,
          selectedState: '',
          selectedState1: '',
          selectedState2: '',
          selectedState3: '',
		  showModal1:false,
          Dob: '', pAddress: '', pAddress2: '',assign_to:1

        }

      }
     showModal1 = () => {

        this.setState({ showModal1: true })
      }
    
      closeModal1 = () => {
        this.setState({ showModal1: false })
      }
      submit = (e) => {
        e.preventDefault();
        var custom_validate = this.custom_validation({ errorClass: 'tooltip-default' });
        var validator = jQuery("#create_participant").validate({ ignore: [] });
        if (!this.state.loading && jQuery("#create_participant").valid() && custom_validate) {
          const formData = new FormData();
          const config = {
                        headers: {
                            'content-type': 'multipart/form-data'
                        }
            }
          for(var x = 0; x<this.props.selectedFile.length; x++) {
            formData.append('crmParticipantFiles[]', this.props.selectedFile[x])
        } 
           formData.append('crm_participant_id', this.props.crmParticipantId)
           formData.append('docsTitle', this.props.docsTitle)
           formData.append('PhoneInput', [{ 'name': '43434' }]) 
           formData.append('EmailInput', [{ 'name': 'aa@aa.com' }])
           formData.append('AddressInput', [{ 'state': 'sds', 'postal': '1234', 'street': this.state.address_primary, 'city': { 'value': 1 }, 'department': 'dsd' }, { 'state': 'sds', 'postal': '1234', 'street': this.state.address_secondary, 'city': { 'value': 1 }, 'department': 'dsd' }])  
           formData.append('username', 'wewew') 
           formData.append('gender', 'male') 
           formData.append('firstname', this.state['firstname']) 
           formData.append('lastname', this.state['lastname']) 
           formData.append('dob', this.state['dob'])
           formData.append('prefer_contact', this.state['prefer_contact'])
           formData.append('referral', 1) 
           formData.append('assign_to', this.state.oc_departments)           
           formData.append('medicare_num', this.state['medicare_num']) 
           formData.append('ndis_num', this.state['ndis_num']) 
           formData.append('middlename', "") 
           formData.append('preferredname', "") 
           formData.append('ParticipantTSI', "sdsd") 
           formData.append('formaldiagnosisprimary', "dsdsd") 
           formData.append('participantCognition', "sdsd") 
           formData.append('participantCommunication', "sdsd")
           formData.append('participantenglish', 'wewew') 
           formData.append('participantPreferredlang', 'asdsd') 
           formData.append('CarersInput', [{ "Gender": "1", "Ethnicity": "sd", "Religious": "sdsd" }]) 
           formData.append('bookerdetails', [{ "name": "", "lastname": "", "contact": "", "email": "", "relation": "father" }]) 
           
         
          var str = '';
          sessionStorage.setItem("participant_step_1", str);

          this.setState({ loading: true }, () => {
            postImageData('crm/CrmParticipant/update_crm_participant', formData,config).then((result) => {
              if (result.status) {
                toast.success(<ToastUndo message={"Participant updated successfully"} showType={'s'} />, {
                // toast.success("Participant created successfully", {
                  position: toast.POSITION.TOP_CENTER,
                  hideProgressBar: true
                });

                this.setState({ success: true })
                this.props.closeingModel();
              } else {
                toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                // toast.error(result.error, {
                  position: toast.POSITION.TOP_CENTER,
                  hideProgressBar: true
                });
                this.props.closeingModel();

              }
              this.setState({ loading: false })
            });
          });
        } else {
          validator.focusInvalid();
        }
      }
      componentWillReceiveProps(newsProps)
      {
        this.setState({ AddressInput: [{ 'state': 'sds', 'postal': '1234', 'street': this.state['primary_address'], 'city': { 'value': 1 }, 'department': 'dsd','type':this.state['address_primary_type'] }]})
         this.setState(newsProps.editparticipant)
         this.setState({ kindetails: [{ name:this.state['kin_firstname'], lastname: this.state['kin_lastname'], contact:  this.state['kin_phone'], email: this.state['kin_email'], relation: this.state['kin_relation'] }]})

      }
      handleChange = (e) => {
          var inputFields = this.state;
          this.setState({error: ''});
          inputFields[e.target.name] = e.target.value;
          this.setState(inputFields);
      }
      selectChanges = (selectedOption, fieldname) => {
          var selectField =this.state;
          selectField[fieldname] = selectedOption;
          selectField[fieldname+'_error'] = false;
          this.setState(selectField);
      }


      handleShareholderNameChange = (index, stateKey, fieldtkey, fieldValue) => {
        var state = {};
        var tempField = {};
        var List = this.state[stateKey];
        List[index][fieldtkey] = fieldValue

        state[stateKey] = List;

        this.setState(state);
      }
      handleAddShareholder = (e, tagType) => {
        e.preventDefault();
        var state = [];
        state[tagType] = this.state[tagType].concat([{ name: '', lastname: '', contact: '', email: '', relation: '' }]);
        this.setState(state);
      }

      handleRemoveShareholder = (e, idx, tagType) => {
        e.preventDefault();
        var state = {};
        var List = this.state[tagType];

        state[tagType] = List.filter((s, sidx) => idx !== sidx);
        this.setState(state);
      }

      custom_validation = () => {
        var return_var = true;
        var state = {};
        var List = [{ key: 'referral_relation' }, { key: 'living_situation' }];
        List.map((object, sidx) => {
          if (object.key == 'referral_relation') {

            if ((this.state['referral_relation'] == undefined || this.state['referral_relation'] == '')) {
              state[object.key + '_error'] = true;
              this.setState(state);
              return_var = false;
            }
          }
          else  if (this.state[object.key] == null || this.state[object.key] == undefined || this.state[object.key] == '') {
            state[object.key + '_error'] = true;
            this.setState(state);
            return_var = false;
          }
        });

        const newShareholders = this.state.AddressInput.map((object, sidx) => {
          if (object.type == '' || object.type == undefined || object.type == null) {

            return_var = false;
            return { ...object, type_error: true };
          } else {
            return { ...object, type_error: false };
          }
        });
        this.setState({ AddressInput: newShareholders });

        return return_var;
      }


      errorShowInTooltip = ($key, msg) => {
        //alert($key);
        return (this.state[$key + '_error']) ? <div className={'tooltip custom-tooltip fade top in' + ((this.state[$key + '_error']) ? ' select-validation-error' : '')} role="tooltip">
          <div className="tooltip-arrow"></div><div className="tooltip-inner">{msg}.</div></div> : '';

      }

      errorShowInTooltipForLoop = (key, msg) => {
        return (key == true) ? <div className={'tooltip custom-tooltip fade top in' + ((key == true) ? ' select-validation-error' : '')} role="tooltip">
          <div className="tooltip-arrow"></div><div className="tooltip-inner">{msg}.</div></div> : '';

      }


      render() {

        if (this.state.success) {
          return (<EditParticipant />)
        }

        return (

          <div>

            <div className={this.props.showModal ? 'customModal show' : 'customModal'}>
            <ReactPlaceholder showLoadingAnimation  type="media" ready={!this.state.loading} customPlaceholder={IntakeProcess}>
              <div className="custom-modal-dialog Information_modal">
                <div className="custom-modal-header bb-1">
                  <div className="Modal_title">Edit Prospective Participant</div>
                  <i className="icon icon-close3-ie Modal_close_i" onClick={this.props.closeModal}></i>
                 </div>
                <form id="create_participant">
                  <div className="custom-modal-body w-80 mx-auto">

                    <div className="row">
                      <div className="col-md-12 py-4 title_sub_modal">Participant Details</div>
                      <div className="col-md-4 mb-4">
                        <label className="title_input">First name: </label>
                          <div className="required">
                              <input data-rule-required='true' data-msg-required="Add First Name" placeholder="First Name" type="text" name="firstname" value={this.state['firstname'] || ''} onChange={this.handleChange} maxLength="30" />
                              <input type="hidden" name="crm_participant_id" className="default-input"   value={this.props.crmParticipantId} />
                        </div>
                      </div>
                      <div className="col-md-4 mb-4">
                        <label className="title_input">Last name: </label>
                          <div className="required">
                            <input placeholder="Last Name" data-msg-required="Add Last Name" type="text" name="lastname" value={this.state['lastname'] || ''} onChange={this.handleChange} data-rule-required="true" maxLength="30" />
                        </div>
                      </div>
                      <div className="col-md-4 mb-4">
                        <label className="title_input">Date of Birth: </label>
                        <div className="required">
                            <DatePicker showYearDropdown scrollableYearDropdown yearDropdownItemNumber={110} dateFormat="DD/MM/YYYY" required={true} data-placement={'bottom'} maxDate={moment()}
                            name="dob" onChange={(date) => this.setState({ Dob: date })} selected={this.state['dob'] ? moment(this.state['dob'], 'DD-MM-YYYY') : null} className="text-center " placeholderText="DD/MM/YYYY" maxLength="30" />
                        </div>
                      </div>
                      <div className="col-md-4 mb-4">
                        <label className="title_input">NDIS Number: </label>
                         <div className="required">
                            <input type="text" data-rule-required='true' data-msg-required="NDIS Number Required" placeholder="000 000 000" name="ndis_num" value={this.state['ndis_num'] || ''} onChange={this.handleChange} maxLength="30" />
                        </div>
                      </div>
                      <div className="col-md-4 mb-4">
                        <label className="title_input">Medicare Number: </label>
                          <div className="required">
                           <input type="text" data-rule-required='true' data-msg-required="Medicare Number Required" placeholder="0000 00000 0" name="medicare_num" value={this.state['medicare_num'] || ''} onChange={this.handleChange} maxLength="30" />
                         </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-12 py-4"><div className="bt-1"></div></div>
                      <div className="col-md-12 pt-1 pb-4 title_sub_modal">Reference Details</div>
                      <div className="col-md-4 mb-4">
                        <label className="title_input">First name: </label>
                        <div className="required">
                          <input placeholder="First name" data-msg-required="Add First Name" type="text" value={this.state['referral_firstname'] || ''} name="referral_firstname" onChange={this.handleChange} data-rule-required="true" maxLength="30" />
                        </div>
                      </div>
                      <div className="col-md-4 mb-4">
                        <label className="title_input">Last name: </label>
                         <div className="required">
                           <input placeholder="Last Name" data-msg-required="Add Last Name" type="text" value={this.state['referral_lastname'] || ''} name="referral_lastname" onChange={this.handleChange} data-rule-required="true" maxLength="30" />
                         </div>
                      </div>
                      <div className="col-md-4 mb-4">
                        <label className="title_input">Organisation: </label>
                        <div className="required">
                            <input type="text" className="default-input" data-rule-required='true' data-msg-required="Organisation Name Required" value={this.state['referral_org']} name="referral_org" onChange={this.handleChange} placeholder="Organisation" />
                        </div>
                      </div>
                      <div className="col-md-4 mb-4">
                        <label className="title_input">Email: </label>
                          <div className="required">
                            <input placeholder="Email" type="text" data-msg-required="Add Email Address" name="referral_email" value={this.state['referral_email']} onChange={this.handleChange} data-rule-required="true" data-rule-email="true" maxLength="70" />
                        </div>
                      </div>
                      <div className="col-md-4 mb-4">
                        <label className="title_input">Phone Number: </label>
                         <div className="required">
                            <input placeholder="Phone" type="text" data-msg-required="Add Phone Number" name="referral_phone" value={this.state['referral_phone']} onChange={this.handleChange} data-rule-required="true" maxLength="30" maxLength="30" />
                        </div>
                      </div>
                      <div className="col-md-4 mb-4">
                        <label className="title_input">Relationship to Participant: </label>
                        <div className="required">
                            <div className="s-def1">
                            <Select className="custom_select"
                                clearable={false}
                                searchable={false}
                                simpleValue={true}
                                value={this.state['referral_relation'] || ''}
                                name="referral_relation"
                                onChange={(e) => this.selectChanges(e, 'referral_relation')}
                                options={relationDropdown(0)}
                                required={true}
                                placeholder="Please Select" />

                            {this.errorShowInTooltip('referral_relation', 'Add Relation')}

                            </div>
                        </div>
                        </div>
                    </div>

                    <div className="row">
                      <div className="col-md-12 py-4"><div className="bt-1"></div></div>
                      <div className="col-md-12 pt-1 pb-4 title_sub_modal">Living Details:</div>
                      <div className="col-md-4 mb-4">
                        <label className="title_input">Living Situation: </label>
                        <div className="required">
                            <div className="s-def1">
                            <Select className="custom_select" clearable={false}
                                name="living_situation" simpleValue={true}
                                value={this.state['living_situation'] || ''}
                                onChange={(e) => this.selectChanges(e, 'living_situation')}
                                required={true} searchable={false}
                                options={LivingSituationOption(0)} placeholder="Please Select" />
                            {this.errorShowInTooltip('living_situation', 'Add ParticipantSituation')}
                            </div>
                        </div>
                      </div>
                    </div>
                    {this.state.AddressInput.map((AddressInput, idx) => (
                      <div key={idx + 1}>
                        <div className="row">
                          <label className="title_input col-md-12 pl-5"><b>{idx > 0 ? 'Secondray Address:' : 'Primary Address:'} </b></label>
                        </div>
                        <div key={idx + 1} className="row d-flex">
                          <div className="col-md-7 mb-4">
                            <label className="title_input">Address: </label>
                            <div className="small-search l-search">

                              <Autocomplete className="form-control"
                                style={{ width: '90%' }}
                                name={"primary_address" + idx}
                                onPlaceSelected={(place) => this.handleShareholderNameChange(idx, 'AddressInput', 'street', place.formatted_address)}
                                types={['(regions)']}
                                value={AddressInput.street || ''}
                                onChange={(evt) => this.handleShareholderNameChange(idx, 'AddressInput', 'street', evt.target.value)}
                                onKeyDown={(evt) => this.handleShareholderNameChange(idx, 'AddressInput', 'street', evt.target.value)}

                              />

                              <button><span className="icon icon-location1-ie"></span></button>
                            </div>
                          </div>
                          <div className="col-md-4 mb-4">
                            <label className="title_input">Address Type: </label>
                            <div className="required">
                            <div className="s-def1">

                              <Select clearable={false} searchable={false}
                                className="custom_select"
                                simpleValue={true}
                                name={"address_primary_type" + idx}
                                value={AddressInput.type || ''}
                                onChange={(e) => this.handleShareholderNameChange(idx, 'AddressInput', 'type', e)}
                                options={sitCategoryListDropdown(0)}
                                data-rule-required="true"
                                data-msg-required="Select Address Type"
                                placeholder="Please Select" />
                              {this.errorShowInTooltipForLoop(AddressInput.type_error, 'Select Address Type')}
                            </div>
                           </div>
                          </div>
                          {idx > 0 ? <div className="col-md-1 align-items-end d-inline-flex mb-4"> <button className="button_plus__" onClick={(e) => this.handleRemoveShareholder(e, idx, 'AddressInput')}>
                          <i className="icon icon-decrease-icon Add-2-2"></i>
                          </button></div> : (this.state.AddressInput.length == 3) ? '' : <div className="col-md-1 align-items-end d-inline-flex mb-4"><button className="button_plus__"
                            onClick={(e) => handleAddShareholder(this, e, 'AddressInput', AddressInput)}>
<i  className="icon icon-add-icons Add-2-1"></i>
                          </button></div>}

                        </div>

                      </div>
                    ))}



                    <RalativeDetails title="Next of Kin" stateKey="kindetails" kindetails={this.state['kindetails']} errorShowInTooltipForLoop={this.errorShowInTooltipForLoop}
                      handleShareholderNameChange={this.handleShareholderNameChange}
                      handleRemoveShareholder={this.handleRemoveShareholder}
                      handleAddShareholder={this.handleAddShareholder}
                    />
                    <div className="row">
                      <div className="col-md-12 py-4"><div className="bt-1"></div></div>
                      <div className="col-md-12 pt-1 pb-4 title_sub_modal">Attachments</div>
                    </div>

                    <div className="row ">
                      <div className="col-md-3 mb-4">
                        <label className="title_input">NDIS Plan Document: </label>
                        <a className="v-c-btn1">
                          <span>Document 1</span> <i className="icon icon-view1-ie"></i>
                        </a>
                      </div>
                      <div className="col-md-3 mb-4">
                        <label className="title_input">Agreement Document: </label>
                        <a className="v-c-btn1">
                          <span>Document 1</span> <i className="icon icon-view1-ie"></i>
                        </a>
                      </div>
                      <div className="col-md-3 mb-4">
                        <label className="title_input">Signed Concent Document: </label>
                        <a className="v-c-btn1">
                          <span>Document 1</span> <i className="icon icon-view1-ie"></i>
                        </a>
                      </div>
                    </div>

                    <div className="row d-flex mb-5">
                      <div className="col-md-9 align-items-end d-inline-flex"><div className="bt-1 w-100"></div></div>
                      <div className="col-md-3"><a className="btn-1" onClick={this.showModal1}>Browse</a></div>
                      {/* <div className="col-md-3">
                      <div className="upload_btn">
                        <label className="btn-file">
                          <div className="v-c-btn1"><span>Browse</span><i className="icon icon-export1-ie" aria-hidden="true"></i></div>
                          <input className="p-hidden" type="file" />
                        </label>
                      </div>
                    </div> */}
                    </div>

                  </div>

                  <div className="custom-modal-footer bt-1 mt-5">
                    <div className="row d-flex justify-content-end">
                      <div className="col-md-3"><a className="btn-1" onClick={this.submit}>Apply Changes</a></div>
                    </div>
                  </div>
                </form>
              </div>

              </ReactPlaceholder>
            </div>
           <Modal  className="modal fade Modal_A  Modal_B Crm" show={this.state.showModal1} onHide={()=>this.closeModal1()}  >
                        <form id="crm_participant_create_doc" method="post" autoComplete="off">
                            <Modal.Body>
                                <div className="dis_cell">
                                    <div className="text text-left">Relevant Attachments:
                               <a data-dismiss="modal" aria-label="Close" className="close_i pull-right mt-1"  onClick={()=>this.closeModal1()}><i className="icon icon-cross-icons"></i></a>
                                    </div>

                                    <div className="row P_15_T">
                                        <div className="col-md-8">
                                            <div className="row P_15_T">
                                                <div className="col-md-12">
                                                    <label>Title</label>
                                                    <span className="required">
                                                        <input type="text" placeholder="Please Enter Your Title" onChange={(e) => this.props.docsTitleFun(e)} value={(this.props.docsTitle) ? this.props.docsTitle : ''} data-rule-required="true"/>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row P_15_T">
                                        <div className="col-md-12"> <label>Please select a file to upload</label></div>
                                        <div className="col-md-5">
                                            <span className="required upload_btn">
                                                <label className="btn btn-default btn-sm center-block btn-file">
                                                    <i className="but" aria-hidden="true">Upload New Doc(s)</i>
                                                    
                                                    <input className="p-hidden" type="file" onChange={this.props.fileChange} data-rule-required="true" date-rule-extension="jpg|jpeg|png|xlx|xls|doc|docx|pdf" />
                                                </label>
                                            </span>                                            
                                             {(this.props.filename) ? <p>File Name: <small>{this.props.filename}</small></p> : ''}
                                        </div>
                                        <div className="col-md-7"></div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-7"></div>
                                        <div className="col-md-5">
                                            <a className="btn-1" onClick={()=>this.closeModal1()} >Save</a>
                                        </div>
                                    </div>

                                </div>
                            </Modal.Body>
                        </form>
                    </Modal>

          </div>
        )
    }
}
class RalativeDetails extends Component {
    render() {

      return (
        <div>
          <div className="row P_25_T">
            <div className="col-lg-12  col-md-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
            <div className="col-lg-12  col-md-12 P_15_TB title_sub_modal"><h3 ><b>{this.props.title} Details:</b></h3></div><div className="col-lg-1"></div>
            <div className="col-lg-12  col-md-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
          </div>

          {this.props.kindetails.map((obj, index) => (
            <div key={index + 1} className="P_25_T row">
              <div className="col-lg-3 col-md-3">
                <label className="title_input"> Name:</label>
                <div className="required">
                    <input className="input_f mb-1 "
                    value={obj.name || ''}
                    name={this.props.stateKey + 'input_kin_first_name' + index}
                    placeholder='First Name'
                    onChange={(evt) => this.props.handleShareholderNameChange(index, this.props.stateKey, 'name', evt.target.value)}
                    maxLength="30"
                    required={true}
                    />
                 </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <label></label>
                <div className="required">
                    <input className="input_f mb-1 mt-1"
                    value={obj.lastname || ''}
                    name={this.props.stateKey + 'input_kin_lastname' + index}
                    placeholder='Last Name'
                    onChange={(evt) => this.props.handleShareholderNameChange(index, this.props.stateKey, 'lastname', evt.target.value)}
                    maxLength="30"
                    required={true}
                    />
                 </div>
              </div>
              <div className="col-lg-2 col-md-3">
                <label className="title_input"> Contact:</label>
                <div className="required">
                <input className="input_f mb-1  distinctKinCOntact"
                  value={obj.contact || ''}
                  name={this.props.stateKey + 'input_kin_contact' + index}
                  placeholder='Contact'
                  onChange={(evt) => this.props.handleShareholderNameChange(index, this.props.stateKey, 'contact', evt.target.value)}
                  maxLength="30"
                  data-rule-notequaltogroup='[".distinctKinCOntact"]'
                  data-msg-notequaltogroup='Please enter unique contact number'
                  required={true}
                />

              </div>
              </div>
              <div className="col-lg-3 col-md-3">
                <label></label>
                <div className="required">
                    <input className="input_f mb-1 mt-1 distinctEmail"
                    type="text"
                    value={obj.email || ''}
                    name={this.props.stateKey + 'input_kin_email' + index}
                    placeholder='Email'
                    maxLength="70"
                    data-rule-notequaltogroup='[".distinctEmail"]'
                    data-rule-email="true"
                    required={true}
                    onChange={(evt) => this.props.handleShareholderNameChange(index, this.props.stateKey, 'email', evt.target.value)}
                    />
               </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <label className="title_input">Relation:</label>
                <div className="row">
                  <div className="col-lg-9 col-md-9">
                  <div className="required">
                    <span className="default_validation">
                      <Select className=""
                        clearable={false}
                        searchable={true}
                        simpleValue={true}
                        value={obj.relation || ''}
                        name={this.props.stateKey + 'input_relation_primary' + index}
                        onChange={(evt) => this.props.handleShareholderNameChange(index, this.props.stateKey, 'relation', evt)}
                        options={relationDropdown(0)}
                        required={true}
                        placeholder="Please Select" />
                    </span>
                    </div>
                  </div>
                  <div className="col-lg-3 col-md-3">
                    {index > 0 ? <button className="button_plus__" onClick={(e) => this.props.handleRemoveShareholder(e, index, this.props.stateKey)}>
                      <i className="icon icon-decrease-icon Add-2-2" ></i>
                    </button> : (this.props.kindetails.length == 3) ? '' : <button className="button_plus__" onClick={(e) => this.props.handleAddShareholder(e, this.props.stateKey)}>
                      <i className="icon icon-add-icons Add-2-1" ></i>
                    </button>}
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      )
    }
  }

  const mapStateToProps = state => {
      return {
        showPageTitle: state.DepartmentReducer.activePage.pageTitle,
        showTypePage: state.DepartmentReducer.activePage.pageType,
        participaintDetails :state.DepartmentReducer.participaint_details

      }
  };
  export default connect(mapStateToProps)(ParticipantDetails);
