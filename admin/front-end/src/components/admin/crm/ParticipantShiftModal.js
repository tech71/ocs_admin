import React, { Component } from 'react';
import Modal from 'react-bootstrap/lib/Modal';


export const PartShiftModal = (props) => {
    return (
        <Modal className="modal fade Crm" bsSize="large" show={props.showModal} onHide={() => props.handleClose} >
            <form id="special_agreement_form" method="post" autoComplete="off">
                <Modal.Body className="px-0 py-0">

                    <div className="custom-modal-header bb-1">
                        <div className="Modal_title">Edit Participant Shifts</div>
                        <i className="icon icon-close3-ie Modal_close_i" onClick={() => props.handleClose()}></i>
                    </div>
                    <div className="custom-modal-body w-100 px-5 mx-auto">

                 
                        <div className="row mt-5">
                        <div className="col-md-6">
                      
                           <div className="row">
                           <div className=" title_sub_modal">Selected Services & Funding:</div>
                            <div className="col-md-6">
                           <label className="title_input pl-0">Participant Mobility Requirements: </label>
                            <input /></div>
                           </div>
                        </div>
                        <div className="col-md-6 bl_das">
                                    <div className="title_sub_modal mb-3">Shift Support Required:</div>
                                    <div className="Scroll_div_parents">
                                        <div className="Scroll_div">

                                            <div className="row">
                                                <div className="col-md-12 mb-2">
                                                    <label className="c-custom-checkbox CH_010">
                                                        <input type="checkbox" defaultChecked />
                                                        <i className="c-custom-checkbox__img"></i>
                                                        <div>Shower Chair</div>
                                                    </label>
                                                </div>
                                                <div className="col-md-12 mb-2">
                                                    <label className="c-custom-checkbox CH_010">
                                                        <input type="checkbox" defaultChecked />
                                                        <i className="c-custom-checkbox__img"></i>
                                                        <div>Wheel Chair</div>
                                                    </label>
                                                </div>
                                                <div className="col-md-12 mb-2">
                                                    <label className="c-custom-checkbox CH_010">
                                                        <input type="checkbox" defaultChecked />
                                                        <i className="c-custom-checkbox__img"></i>
                                                        <div>Ceiling Hoist</div>
                                                    </label>
                                                </div>
                                                <div className="col-md-12 mb-2">
                                                    <label className="c-custom-checkbox CH_010">
                                                        <input type="checkbox" defaultChecked />
                                                        <i className="c-custom-checkbox__img"></i>
                                                        <div>Other</div>
                                                    </label>
                                                </div>
                                                <div className="col-md-12 mb-2 pull-right">
                                                    <span className="Other-option_div"> Mobility Scooter</span>
                                                    {/* <button className="Other-option_div"> Mobility Scooter</button> */}
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                        </div>


                        <div className="row mt-5">
                            <div className="col-md-12">
                            <div className="title_sub_modal mb-3">Shift Support Required:</div>
                                <div className="shift_week_1">
                                    <div className="shift_week_title">Week 1</div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Sun</div>
                                        <div className="Shift_day_blank am_shift"></div>
                                        <div className="Shift_day_blank pm_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Mon</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank so_shift"></div>
                                        <div className="Shift_day_blank na_shift"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Tue</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank am_shift"></div>
                                        <div className="Shift_day_blank pm_shift"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Wed</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank so_shift"></div>
                                        <div className="Shift_day_blank na_shift"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Thur</div>
                                        <div className="Shift_day_blank am_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank so_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Fri</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank na_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Sat</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank na_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                    </div>
                                    <div className="shift_week_add">
                                        <span className="add_btn1"><i className="icon icon-ios-plus-empty"></i></span>
                                        {/* <span className="add_btn1"><i className="icon icon-ios-minus-empty"></i></span> */}
                                    </div>
                                </div>
                                <div className="shift_week_1">
                                    <div className="shift_week_title">Week 1</div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Sun</div>
                                        <div className="Shift_day_blank am_shift"></div>
                                        <div className="Shift_day_blank pm_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Mon</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank so_shift"></div>
                                        <div className="Shift_day_blank na_shift"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Tue</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank am_shift"></div>
                                        <div className="Shift_day_blank pm_shift"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Wed</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank so_shift"></div>
                                        <div className="Shift_day_blank na_shift"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Thur</div>
                                        <div className="Shift_day_blank am_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank so_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Fri</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank na_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Sat</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank na_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                    </div>
                                    <div className="shift_week_add">
                                        {/* <span className="add_btn1"><i className="icon icon-ios-plus-empty"></i></span> */}
                                        <span className="add_btn1"><i className="icon icon-ios-minus-empty"></i></span>
                                    </div>
                                </div>
                                <div className="shift_week_1">
                                    <div className="shift_week_title">Week 1</div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Sun</div>
                                        <div className="Shift_day_blank am_shift"></div>
                                        <div className="Shift_day_blank pm_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Mon</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank so_shift"></div>
                                        <div className="Shift_day_blank na_shift"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Tue</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank am_shift"></div>
                                        <div className="Shift_day_blank pm_shift"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Wed</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank so_shift"></div>
                                        <div className="Shift_day_blank na_shift"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Thur</div>
                                        <div className="Shift_day_blank am_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank so_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Fri</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank na_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Sat</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank na_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                    </div>
                                    <div className="shift_week_add">
                                        {/* <span className="add_btn1"><i className="icon icon-ios-plus-empty"></i></span> */}
                                        <span className="add_btn1"><i className="icon icon-ios-minus-empty"></i></span>
                                    </div>
                                </div>
                            </div>

                        </div>



                    </div>
                    <div className="custom-modal-footer bt-1 mt-5">
                        <div className="row d-flex justify-content-end">
                            <div className="col-md-3"><a className="btn-1">Save Change</a></div>
                        </div>
                    </div>


                </Modal.Body>
            </form>
        </Modal>
    );

}