import React from 'react';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { listViewSitesOption, relationDropdown, sitCategoryListDropdown, ocDepartmentDropdown, getAboriginalOrTSI, LivingSituationOption,getmartialStatus} from '../../../dropdown/CrmDropdown.js';

export const ParticipantDetailsPopup = (props) => {

    return (
<React.Fragment>
                 <form id="partcipant_details">
                  <div className="row">
                    <div className="col-md-12 py-4 title_sub_modal">Participant Details</div>
                  </div>

                  <div className="row">
                    <div className="col-md-3 mb-4">
                      <label className="title_input">NDIS Number: </label>
                      <div className="required"><input name="ndisno" className="default-input" data-rule-required="true" onChange={(e) =>props.handleChanges(e,'participantDetails')}/></div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-3 mb-4">
                      <label className="title_input">First Name: </label>
                      <div className="required"><input name="firstname" className="default-input" data-rule-required="true" onChange={(e) =>props.handleChanges(e,'participantDetails')}/></div>
                    </div>
                    <div className="col-md-3 mb-4">
                      <label className="title_input">Last Name: </label>
                      <div className="required"><input name="lastname" className="default-input" data-rule-required="true" onChange={(e) =>props.handleChanges(e,'participantDetails')}/></div>
                    </div>

                    <div className="col-md-3 mb-4">
                      <label className="title_input">Preferred First Name: </label>
                      <div className="required"><input name="preferredfirstname" className="default-input" data-rule-required="true" onChange={(e) =>props.handleChanges(e,'participantDetails')}/></div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-3 mb-4">
                      <label className="title_input">Email: </label>
                      <div className="required"><input name="email" className="default-input" data-rule-required="true" onChange={(e) =>props.handleChanges(e,'participantDetails')}/></div>
                    </div>
                    <div className="col-md-3 mb-4">
                      <label className="title_input">Phone Number: </label>
                      <div className="required"><input name="phonenumber" className="default-input" data-rule-required="true" onChange={(e) =>props.handleChanges(e,'participantDetails')}/></div>
                    </div>
                  </div>


                    <div className="row">
                    <div className="col-md-3 mb-4">
                      <label className="title_input">Date of Birth: </label>
                      <div className="required">
                        <DatePicker showYearDropdown scrollableYearDropdown yearDropdownItemNumber={110}
                        dateFormat="DD/MM/YYYY" required={true} data-placement={'bottom'} maxDate={moment()}
                               name="Dob" onChange={(e) => props.updateSelect(e, 'Dob','participantDetails')}
                                className="text-center px-0" placeholderText="DD/MM/YYYY"
                                selected={props.sts.Dob ? moment(props.sts.Dob, 'DD-MM-YYYY') : null}/>
                      </div>
                    </div>
                  </div>


                  <div className="row">
                    <div className="col-md-6 mb-4">
                      <label className="title_input">Street Address: </label>
                      <div className="required"><input name="Address" className="default-input" data-rule-required="true" onChange={(e) =>props.handleChanges(e,'participantDetails')}/></div>
                    </div>
                    <div className="col-md-2 mb-4">
                      <label className="title_input">state</label>
                      <div className="required">
                        <div className="s-def1">
                        { props.errorTooltip('state', 'Select State','participantDetails') }
                        <Select
                            data-rule-required="true"
                            name="state" options={props.propData.statesValue} required={true} simpleValue={true}
                            searchable={false} clearable={false} placeholder="Please Select"
                            onChange={(e) => props.updateSelect(e,'state','participantDetails')}
                            value={props.sts.state}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-md-2 mb-4">
                      <label className="title_input">Post code</label>
                      <div className="required"><input name="postcode" className="default-input" data-rule-required="true" data-rule-number="true" data-rule-postcodecheck="true" data-msg-required="Add Postcode" data-msg-number="Please enter valid postcode" onChange={(e) =>props.handleChanges(e,'participantDetails')}/></div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-12 py-4 title_sub_modal">Participant Living Situation</div>
                  </div>

                  <div className="row">
                    <div className="col-md-3 mb-4">
                      <label className="title_input">Marital Status</label>
                      <div className="required">
                        <div className="s-def1">
                          { props.errorTooltip('martialstatus', 'Select Martial Status','participantDetails') }
                        <Select
                          data-rule-required="true"
                            name="martialstatus" options={getmartialStatus(0)}
                            simpleValue={true}  required={true}
                            searchable={false} clearable={false} placeholder="Please Select"
                            onChange={(e) => props.updateSelect(e,'martialstatus','participantDetails')} value={props.sts.martialstatus}
                          />
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-6 mb-4">
                    { props.errorTooltip('livingsituation', 'Select Living Situation','participantDetails') }
                      <label className="title_input">Living Situation</label>
                      <div className="required">
                        <div className="s-def1">
                        <Select
                            name="livingsituation"  options={LivingSituationOption(0)}  required={true} simpleValue={true}
                            searchable={false} clearable={false} placeholder="Please Select"
                            onChange={(e) => props.updateSelect(e,'livingsituation','participantDetails')} value={props.sts.livingsituation}
                          />
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="row mb-5 pl-3">
                    <div className="col-md-3">
                      <label className="title_input t_input-01__ pl-0">Is the participant of Aboriginal or Torres Strait Islander descent? </label>
                      <div className="row">
                        <div className="col-md-6">
                          <label className="radio_F1 justify-content-center">Yes<input type="radio" name="aboriginal" onChange={(e) =>props.handleChanges(e,'participantDetails')} value={1} checked={(props.sts.aboriginal)==1?true:false} /><span className="checkround ml-2"></span></label>
                        </div>
                        <div className="col-md-6">
                          <label className="radio_F1 justify-content-center">No<input type="radio" name="aboriginal" value={0} onChange={(e) =>props.handleChanges(e,'participantDetails')} checked={(props.sts.aboriginal)==0?true:false} /><span className="checkround ml-2"></span></label>
                        </div>
                      </div>
                    </div>
                  </div>


                  <div className="row mb-5 pl-3 bb-1">

                    <div className="col-md-3">
                      <label className="title_input t_input-01__ pl-0">Does the participant have a current behavioural support plan? </label>
                      <div className="row">
                        <div className="col-md-6">
                          <label className="radio_F1 justify-content-center">Yes<input type="radio" name="current_behavioural" value={1} onChange={(e) =>props.handleChanges(e,'participantDetails')} checked={(props.sts.current_behavioural)==1?true:false} /><span className="checkround ml-2"></span></label>
                        </div>
                        <div className="col-md-6">
                          <label className="radio_F1 justify-content-center">No<input type="radio" name="current_behavioural" value={0} onChange={(e) =>props.handleChanges(e,'participantDetails')} checked={(props.sts.current_behavioural)==0?true:false} /><span className="checkround ml-2"></span></label>
                        </div>
                      </div>
                    </div>


                    <div className="col-md-9">
                      <label className="title_input t_input-01__ pl-0 w-25 mb-0"><b>New Behavioural Support Plan Doc</b></label>
                      <ul className="file_down quali_width  w-100">
                        <li className="w-25 px-2">
                          <div className="path_file mt-0 mb-4"><b>Hearing Imparement</b></div>
                          <span className="icon icon-file-icons d-block"></span>
                          <div className="path_file _pf_"><span>{props.sts.hearing_file_name}</span>  <div className=""><i className="icon icon-close3-ie color"></i></div></div>
                        </li>
                      </ul>
                      <div className="w-25 px-2">
                        <label className="btn-file">
                          <div className="v-c-btn1 n2"><span>Upload File</span><i className="icon " aria-hidden="true"></i></div>
                          <input className="p-hidden" type="file" name="hearing_file" onChange={(e) =>props.handleChanges(e,'participantDetails')}/>
                        </label>
                        {// <label className="btn-file">
                        //   <div className="v-c-btn1 n2"><span>Browse</span><i className="icon " aria-hidden="true"></i></div>
                        //   <input className="p-hidden" type="file" />
                        // </label>
                      }
                      </div>
                    </div>

                  </div>

                  <div className="row mb-5">
                    <div className="col-md-3">
                      <label className="title_input t_input-01__ pl-0">Does the participant have any other relevent plans? </label>
                      <div className="row">
                        <div className="col-md-6">
                          <label className="radio_F1 justify-content-center">Yes<input type="radio" name="other_relevent_plans" value={1} onChange={(e) =>props.handleChanges(e,'participantDetails')} checked={(props.sts.other_relevent_plans)==1?true:false}  /><span className="checkround ml-2"></span></label>
                        </div>
                        <div className="col-md-6">
                          <label className="radio_F1 justify-content-center">No<input type="radio" name="other_relevent_plans" value={0} onChange={(e) =>props.handleChanges(e,'participantDetails')} checked={(props.sts.other_relevent_plans)==0?true:false}  /><span className="checkround ml-2"></span></label>
                        </div>
                      </div>
                    </div>
                  </div>


                  <div className="row">
                    <div className="col-md-12 py-4 title_sub_modal">Participant NDIS Plan:</div>
                  </div>

                  <div className="row pl-3">
                    <div className="col-md-3">
                      <label className="title_input t_input-01__ pl-0">How is the Participant Plan Managed? </label>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-9 pl-5">

                      <div className="row my-3">
                        <div className="col-md-3">
                          <label className="radio_F1">Self Managed<input type="radio" name="plan_management" value={1} onChange={(e) =>props.handleChanges(e,'participantDetails')} checked={(props.sts.plan_management)==1?true:false} /><span className="checkround ml-2" style={{ minWidth: '20px' }}></span></label>
                        </div>
                        <div className="col-md-6 col-md-offset-1"></div>
                      </div>

                      <div className="row my-3">
                        <div className="col-md-3">
                          <label className="radio_F1">Portal Managed<input type="radio" name="plan_management" value={2} onChange={(e) =>props.handleChanges(e,'participantDetails')} checked={(props.sts.plan_management)==2?true:false}  /><span className="checkround ml-2" style={{ minWidth: '20px' }}></span></label>
                        </div>
                        <div className="col-md-6 col-md-offset-1"></div>
                      </div>

                      <div className="row my-3">
                        <div className="col-md-3">
                          <label className="radio_F1">Using a Plan Management Provider<input type="radio" name="plan_management" value={3} onChange={(e) =>props.handleChanges(e,'participantDetails')} checked={(props.sts.plan_management)==3?true:false}  /><span className="checkround ml-2" style={{ minWidth: '20px' }}></span></label>
                        </div>
                        <div className="col-md-6 col-md-offset-1">
                          <div className="row">
                            <div className="col-md-12 mb-4">
                              <label className="title_input">Please provide the name of the Participant plan Management Provider: </label>
                              <div className="required"><input name="provide_phone_number" className="default-input" onChange={(e) =>props.handleChanges(e,'participantDetails')} value={props.sts.provide_phone_number} data-rule-required="true"/></div>
                            </div>
                            <div className="col-md-12 mb-4">
                              <label className="title_input">Please Provide the Plan Manager's Email Address: </label>
                              <div className="required"><input name="provide_email" className="default-input" onChange={(e) =>props.handleChanges(e,'participantDetails')} value={props.sts.provide_email} data-rule-required="true"  data-rule-email="true"/></div>
                            </div>
                            <div className="col-md-12 mb-4">
                              <label className="title_input">Please Provide the Participant Plan Managements Address: </label>
                              <div className="required"><input name="provide_address" className="default-input" onChange={(e) =>props.handleChanges(e,'participantDetails')} value={props.sts.provide_address} data-rule-required="true" /></div>
                            </div>
                            <div className="col-md-6 mb-4">
                              <label className="title_input">state</label>
                              <div className="required">
                                <div className="s-def1">
                                { props.errorTooltip('provide_state', 'Select State','participantDetails') }
                                <Select
                            name="provide_state" options={props.propData.statesValue} required={true} simpleValue={true}
                            searchable={false} clearable={false} placeholder="Please Select"
                            onChange={(e) => props.updateSelect(e,'provide_state','participantDetails')} value={props.sts.provide_state}
                          />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6 mb-4">
                              <label className="title_input">Post code</label>
                              <div className="required"><input name="provide_postcode" className="default-input" onChange={(e) =>props.handleChanges(e,'participantDetails')} data-rule-required="true" /></div>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>


                  <div className="row">
                    <div className="col-md-12 bt-1">
                      <label className="title_input t_input-01__ pl-0 w-100 mb-0"><b>Attach NDIS plan (or relevant section of plan)</b></label>
                      <ul className="file_down quali_width  w-100">
                        <li className="w-20 px-2">
                          <div className="path_file mt-0 mb-4"><b>Hearing Imparement</b></div>
                          <span className="icon icon-file-icons d-block"></span>
                          <div className="path_file _pf_"><span>{props.sts.ndis_file_name}</span>  <div className=""><i className="icon icon-close3-ie color"></i></div></div>
                        </li>
                      </ul>
                      <div className="w-20 px-2">
                        <label className="btn-file">
                          <div className="v-c-btn1 n2"><span>Upload File</span><i className="icon " aria-hidden="true" ></i></div>
                          <input className="p-hidden" type="file" name="ndis_file" onChange={(e) =>props.handleChanges(e,'participantDetails')}/>
                        </label>
                        {// <label className="btn-file">
                        //   <div className="v-c-btn1 n2"><span>Browse</span><i className="icon " aria-hidden="true" ></i></div>
                        //   <input className="p-hidden" type="file" />
                        // </label>
                      }
                      </div>
                    </div>
                  </div>

                  <div className="row d-flex justify-content-end">
                    <div className="col-md-3"><a className="btn-1" onClick={props.submitParticipantDetail}>Save And Changes</a></div>
                  </div>
                  </form>
                </React.Fragment>

    );
}
