import React from 'react';
import { ROUTER_PATH, PAGINATION_SHOW } from '../../../config.js';
import OrganisationAddressUpdatePopUp from './OrganisationAddressUpdatePopUp';
import { postData,checkItsNotLoggedIn,archiveALL,reFreashReactTable,handleChangeChkboxInput } from '../../../service/common.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import {subOrgViewBy} from '../../../dropdown/Orgdropdown.js';
import "react-placeholder/lib/reactPlaceholder.css";
import 'react-toastify/dist/ReactToastify.css';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import { connect } from 'react-redux'
import { setOrgActiveClassProfilePage } from './actions/OrganisationAction.js';
import { setOrgProfileData } from './actions/OrganisationAction.js';
import  OrganisationCreate  from './OrganisationCreate.js';
import Modal from 'react-bootstrap/lib/Modal';
import Pagination from "../../../service/Pagination.js";
import { setActiveSelectPage } from './actions/OrganisationAction.js'

const requestData = (pageSize, page, sorted, filtered,orgId) => {
    return new Promise((resolve, reject) => {
        var Request = JSON.stringify({pageSize: pageSize, page: page, sorted: sorted, filtered: filtered,orgId: orgId});
        postData('organisation/OrgDashboard/get_sub_org', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.hasOwnProperty('count') ? result.count:1)
            };
            resolve(res);
        });

    });
};

class OrganisationSubOrg extends React.Component {
    constructor(props) {
    super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        var all=[]  ;
        this.state = {
            sub_org_list:[],
            view_by_status:1,
            modal_show:false,
            search_value:'',
            isSuborgCreate_modal:false
        };
    this.reactTable = React.createRef();
    }

    handleHidePopup(res){
        this.setState({isSuborgCreate_modal:false},()=>{
            if(res!=undefined){
                reFreashReactTable (this,'fetchData');
            }
        });
    }

    componentDidMount() { 
        this.props.setActiveSelectPage('org_sub_orgs_listing');
        var orgId = this.props.props.match.params.id;
        this.getOrganisationDetail(orgId);
        this.props.changeNavigationClass('subOrgActive');  //call redux for active class     
    }

    getOrganisationDetail(orgId){        
        var requestData = {orgId: orgId};
        this.setState({loading: true});
        postData('organisation/OrgDashboard/get_organisation_about', requestData).then((result) => {
            if (result.status) {
                this.setState(result.data.basic_detail);
            } else {
                this.setState({error: result.error});
            }
             this.setState({loading: false});
        });
    }

    fetchData = (state, instance) => {
        if(!isNaN(state.page)){
            this.setState({loading: true});
            requestData(
                    state.pageSize,
                    state.page,
                    state.sorted,
                    state.filtered,
                    this.props.props.match.params.id
                    ).then(res => {
                this.setState({
                    sub_org_list: res.rows,
                    pages: res.pages,
                    loading: false
                });
            });
        }
    }

    getOrgProfile(){
        let postUrl = 'organisation/OrgDashboard/get_organisation_profile';
        this.setState({loading: true});
        postData(postUrl, this.props.props.match.params).then((result) => {
            if (result.status) {
                this.props.getOrgProfileData(result.data.basic_detail);
            } else {
                window.location = '/admin/organisation/dashboard';
                this.setState({error: result.error});
            }
            this.setState({loading: false});
        });
    }

    archived(id){ 
        var parent_org_id = this.props.props.match.params.id;
        archiveALL({id:id,parent_org_id:parent_org_id},'','organisation/OrgDashboard/archive_organisation').then((result) => {
            if(result.status){
                var requestData = {status:'1',srch_box:this.state.search_value};
                this.setState({filtered:requestData}, () => {
                    this.getOrgProfile();
                });
            }
        })
    }

    viewByselectChange(selectedOption, fieldname) { 
       var requestData = {status:selectedOption,srch_box:this.state.search_value};
       this.setState({filtered:requestData,view_by_status:selectedOption});       
    }   

    closeEditPopUp=(param)=>{
        if(param)
        reFreashReactTable (this,'fetchData');
    
        this.setState({modal_show:false,selectedData:[]});
    }

    searchData =(e)=>
    {
        e.preventDefault();
        var srch_ary = {
            'srch_box':this.state.search_value,
            'status':this.state.view_by_status
        }
        this.setState({filtered: srch_ary});
    }

    render() {  
        const columns = [
            {Header: '', accessor: 'id', filterable: false,Cell: (props) => <span><a className="color-initial" href={'/admin/organisation/overview/'+this.props.props.match.params.id+'/'+props.original.ocs_id}>{props.original.name}</a></span>, Header: <span></span>} ,
            {Cell: (props) => <span></span>, Header: <span></span>, style: {
                    "textAlign": "right", 
                  }, headerStyle: {border:"0px solid #fff" }, headerClassName: 'margin_right_side', sortable: false}, 
                   {expander : true, sortable: false,
                       Expander: ({ isExpanded, ...rest }) => 
                       <div>{isExpanded? <i className="icon icon-arrow-up"></i> : <i className="icon icon-arrow-down"></i>}</div>,
                     headerStyle: {border:"0px solid #fff" },
              }
        ]

     return (
            <div>             
              <div className="row">
                               <div className="col-lg-8 col-sm-9 col-lg-offset-1">
                                    <div className="bor_T"></div>
                                    <div className="P_7_TB"><h3>Sub-Orgs Belonging to '{this.props.OrganisationProfile.name}':</h3></div>
                                    <div className="bor_T"></div>
                                </div>

                                <div className="col-lg-2 col-sm-3">
                                    <div className="box">
                                         <Select name="view_by_status" required={true} simpleValue={true} searchable={false} clearable={false} options={subOrgViewBy()} placeholder="View By" value={this.state.view_by_status} onChange={(e)=> this.viewByselectChange(e,'view_by_status')}/>
                                    </div>
                                </div>
                            </div>

                            <div className="w-100">
                            <div className="row _Common_Search_a">
                                <form className="col-lg-8 col-lg-offset-1 col-sm-9"  method='get' onSubmit={(e)=>this.searchData(e)} autoComplete="off">
                                <div className="row d-flex flex-wrap align-items-center">
                                    <div className="col-lg-7  col-sm-7">
                                    <div className="search_bar">
                                        <div className="input_search">
                                           <input type="text" className="form-control" placeholder="Search" name="search_value" value={this.state.search_value} onChange={(e)=>handleChangeChkboxInput(this,e)}/> 
                                            <button type="submit"><span className="icon icon-search"></span></button>
                                        </div>
                                    </div>
                                    </div>
                                    <div className="col-lg-5 col-sm-5">
                                        <div className="row d-flex flex-wrap align-items-center">
                                            <div className="col-lg-4 col-sm-5 text-right">Filter By:</div>
                                            <div className="col-lg-5 col-sm-7">
                                                <Select required={true} simpleValue={true} searchable={false} clearable={false} placeholder="Newest" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                   
                                </form>
                               {/* <div className="col-sm-3 py-2">
                                    <a className="Plus_button" href="">
                                      <i className="icon icon-add-icons create_add_but"></i>
                                      <span>Create New Site</span>
                                    </a>
                                </div>*/}   
                             </div>
                            </div>

                            <div className="row P_15_TB">

                                <div className="col-lg-10 col-lg-offset-1 col-sm-12">
                                    <div className="schedule_listings header_none_react_table PL_site">
                                        <ReactTable
                                            PaginationComponent={Pagination} 
                                            columns={columns}
                                            manual 
                                            data={this.state.sub_org_list}
                                            pages={this.state.pages}
                                            loading={this.state.loading} 
                                            onFetchData={this.fetchData} 
                                            filtered={this.state.filtered}
                                            defaultFiltered ={{status: 1,srch_box:''}}
                                            defaultPageSize={10}
                                            className="-striped -highlight"  
                                            noDataText="No Record Found"
                                            minRows={2}
                                            ref={this.reactTable}
                                            previousText={<span className="icon icon-arrow-left privious"></span>}
                                            nextText={<span className="icon icon-arrow-right next"></span>}
                                            SubComponent={(props) => <div className="other_conter">
                                            <div className="col-sm-10">
                                            <div className="my-0">
                                                <span className="color">Address: </span> {props.original.primary_address}
                                            </div>
                                            <div className="my-0">
                                                <span className="color">Key Contact: </span>{props.original.key_contact}
                                            </div>
                                            <div className="my-0">
                                                <span className="color">Phone: </span> 
                                                {props.original.hasOwnProperty('OrganisationPh') && props.original.OrganisationPh != null && props.original.OrganisationPh.length>0 ? props.original.OrganisationPh.map((value, idx) => (
                                                    <span><span className="color">{value.primary_phone == '1' ? '(Primary) ' : '(Secondary) '}</span><span key={idx+2}>{value.phone} <i className="icon icon-pending-icons color"></i></span><br/></span>
                                                )): <React.Fragment />}
                                                
                                            </div>
                                            <div className="my-0">
                                                <span className="color">Email: </span> 
                                                {props.original.hasOwnProperty('OrganisationEmail') && props.original.OrganisationEmail != null && props.original.OrganisationEmail.length>0 ? props.original.OrganisationEmail.map((value, idx) => (
                                                    <span><span className="color">{value.primary_email == '1' ? '(Primary) ' : '(Secondary) '}</span> <span key={idx+5}>{value.email}</span><br/></span>
                                                )): <React.Fragment />}
                                                
                                            </div>
                                                
                                            </div>
                                              {(this.state.view_by_status ==1)?
                                                <div className="col-sm-2 text-right mt-5">
                                                    <a onClick={()=>this.setState({modal_show:true,selectedData:props.original,formUrl:'organisation/OrgDashboard/update_sub_org',pageTitle:'Update Sub-Orgs'})}><i className="icon icon-update update_button mr-2" ></i></a>
                                                    <a onClick={()=> this.archived(props.original.ocs_id)}><i className="icon icon-email-pending archive_Icon"></i></a>
                                                </div>
                                            :''}
                                            </div>
                                        }
                                        showPagination={this.state.sub_org_list.length > PAGINATION_SHOW ? true : false }
                                        />        
                                        <OrganisationAddressUpdatePopUp 
                                            modal_show={this.state.modal_show}
                                            formUrl={this.state.formUrl} 
                                            selectedData={this.state.selectedData}
                                            closeEditPopUp={this.closeEditPopUp}
                                            pageTitle={this.state.pageTitle}
                                        />  
                                        {this.state.isSuborgCreate_modal?
                                            <Modal bsSize="large" className="modal fade Green" show={this.state.isSuborgCreate_modal} onHide={this.handleHide} container={this} aria-labelledby="myModalLabel" id="modal_1" tabIndex="-1" role="dialog" >
                                            <Modal.Body> 
                                            <div className="row">
                                                <div className="col-lg-12 bb-1 text-left px-0 pb-3">
                                                    <h2 className="color">Creating New Sub Organisation:<a className="close_i pull-right mt-1" onClick={()=>{this.handleHidePopup()}}><i className="icon icon-cross-icons"></i></a></h2>
                                                </div>
                                            </div>
                                                <OrganisationCreate isShowModal={true}  is_parent_org={0} parent_organisation={{label: this.props.OrganisationProfile.name, value: this.props.props.match.params.id}} closePopup={(e)=>{this.handleHidePopup(e)}}/> 
                                            </Modal.Body>
                                            </Modal>
                                            : <React.Fragment />
                                        }
                                              
                                    </div>
                                    
                                        <a className="pull-right P_30_T">
                                            <button className="button_plus__" onClick={()=>this.setState({isSuborgCreate_modal:true,selectedData:{}})}><i className="icon icon-add-icons Add-2-1"></i></button>
                                        </a>
                                </div>
                                                     
                            </div>
                       
                   
                </div>
             
        );
    }
}


const mapStateToProps = state => ({
    ActiveClass : state.OrganisationReducer.ActiveClassProfilePage,
    OrganisationProfile : state.OrganisationReducer.orgProfile
})

 const mapDispatchtoProps = (dispach) => {
       return {
           changeNavigationClass: (value) => dispach(setOrgActiveClassProfilePage(value)),
           getOrgProfileData: (value) => dispach(setOrgProfileData(value)),
           setActiveSelectPage: (value) => dispach(setActiveSelectPage(value)),
      }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(OrganisationSubOrg)