import React, { Component } from 'react';
import jQuery from "jquery";
import { postData,handleChangeChkboxInput,handleRemoveShareholder,handleAddShareholder,handleShareholderNameChange } from '../../../service/common.js';
import 'react-select-plus/dist/react-select-plus.css';
import Modal from 'react-bootstrap/lib/Modal'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {orgContactType} from '../../../dropdown/Orgdropdown.js';
import Select from 'react-select-plus';
import { ToastUndo } from 'service/ToastUndo.js'

class OrganisationAddContactPopUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            OrganisationPh:[{'phone':''}],
            OrganisationEmail:[{'email':''}],
        };        
    }
    
    componentWillReceiveProps(newProps) {
        
        this.setState(newProps.selectedData,()=>{
            if(this.state.OrganisationPh.length == 0)
            {
                this.setState({OrganisationPh:[{'phone':''}]});
            }

            if(this.state.OrganisationEmail.length == 0)
            {
                this.setState({OrganisationEmail:[{'email':''}]});
            }
            if(newProps.hasOwnProperty('houseId')){
                this.setState({houseId:newProps.houseId});
            }
        });
       
        this.setState({orgId:newProps.orgId,edit_mode:newProps.edit_mode,pageTitle:newProps.pageTitle});
    }

    componentDidMount() {
        this.setState(this.props.selectedData,()=>{
            if(this.state.OrganisationPh.length == 0)
            {
                this.setState({OrganisationPh:[{'phone':''}]});
            }

            if(this.state.OrganisationEmail.length == 0)
            {
                this.setState({OrganisationEmail:[{'email':''}]});
            }
            if(this.props.hasOwnProperty('houseId')){
                this.setState({houseId:this.props.houseId});
            }
        });
       
        this.setState({orgId:this.props.orgId,edit_mode:this.props.edit_mode,pageTitle:this.props.pageTitle});
     }
    
    onSubmit = (e) => {
        e.preventDefault()
         jQuery('#updateAddress').validate({  ignore: []});
         if(jQuery('#updateAddress').valid()){
            this.setState({loading: true},() => {
            postData(this.props.fromUrl, this.state).then((result) => {  
       
            if (result.status) {
                toast.success(<ToastUndo message={result.msg} showType={'s'} />, {
                // toast.success(result.msg, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                }); 
                this.props.closeEditPopUp(true);
            }
            else
            {
                toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                // toast.error(result.error, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                }); 
            }
            this.setState({loading: false})
        });
            });
       }
    }

    handleAddHtml =(e, tagType, object_array)=> {
        e.preventDefault();
        var state = {};
        var temp = object_array
        var list = this.state[tagType];
        state[tagType] = list.concat([this.reInitializeObject(temp)]);
        this.setState(state);
    }

    handleRemoveHtml =(e, idx, tagType)=> {
        e.preventDefault();
         var state = {};
         var List = this.state[tagType];
        state[tagType] = List.filter((s, sidx) => idx !== sidx);
        this.setState(state);
    }

    reInitializeObject = (object_array) => {
         var state = {}
         Object.keys(object_array).forEach(function(key) {
            state[key] = '';
        });
        return state;
    }
       
    closeAndRefresh()
    {
        this.props.closeEditPopUp(false);
        this.setState({
            OrganisationPh:[{'phone':''}],
            OrganisationEmail:[{'email':''}],
            loading:false,
            edit_mode:false
        })
    }

    render() {
        return (
             <Modal
               className="Modal fade Modal_A Modal_B Green"
               show={this.props.modal_show}
               container={this}
               aria-labelledby="contained-modal-title"
             >
              <Modal.Body>
            <form id="updateAddress" autoComplete="off">
                <div className="dis_cell-1">
                    <div className="text text-left by-1">{this.state.pageTitle}:
                        <a onClick={() => this.closeAndRefresh() } className="close_i pull-right mt-1"><i className="icon icon-cross-icons"></i></a>
                    </div>
                               

                    <div className="row P_15_T" >
                            <div className="col-sm-4 mt-4">
                                <label>Contact type: </label> 
                                <span className="required">
                                    <Select name="contact_type" value={this.state.contact_type} required={true} simpleValue={true} searchable={true} clearable={false}  placeholder="Contact type" options={orgContactType()} onChange={(e)=> this.setState({'contact_type':e})} className="default_validation"/>
                                </span>
                            </div>

                            <div className="col-sm-4 mt-4">
                                <label>Name: </label> 
                                <span className="required">
                                    <input type="text" value={this.state.firstname || ''} onChange={(e) => handleChangeChkboxInput(this, e)}  data-rule-required="true" className="text-left" name="firstname" placeholder="first" autoComplete="new-password" />
                                </span>
                            </div>
                        
                        <div className="col-sm-4 mt-4">
                            <label></label> 
                            <span className="required">
                                <input type="text" value={this.state.lastname || ''} onChange={(e) => handleChangeChkboxInput(this, e)}  data-rule-required="true" className="text-left" name="lastname" placeholder="last" autoComplete="new-password"/>
                            </span>
                        </div>
                        </div>

                         <div className="row" >
                         <div className="col-sm-4 mt-4">
                            <label>Position: </label> 
                            <span className="required">
                                <input type="text" value={this.state.position || ''} onChange={(e) => handleChangeChkboxInput(this, e)}  data-rule-required="true" className="text-left" name="position" placeholder="EG, CEO" autoComplete="new-password"/>
                            </span>
                        </div>

                         <div className="col-sm-4 mt-4">
                            <label></label> 
                            <span className="required">
                                <input type="text" value={this.state.department} onChange={(e) => handleChangeChkboxInput(this, e)}  data-rule-required="true" className="text-left" name="department" placeholder="EG, Accounts" autoComplete="new-password"/>
                            </span>
                        </div>
                        </div>
                       
                        <div className="row" >
                    <div className="col-sm-4 mt-4" >
                        {this.state.OrganisationPh.map((value, idx) => (
                            <div className="mb-4" key={idx+1}>
                                <label>Phone ({(idx == 0)?'Primary':'Secondary'}):</label>
                                <span className="required">
                                    <div className="input_plus__ mb-1">
                                        <input type="text" className="input_f distinctPh" placeholder="Can Include Area Code" value={value.phone} name={'org_phone'+idx} required onChange={(e) => handleShareholderNameChange(this,'OrganisationPh',idx,'phone',e.target.value)} autoComplete="new-password" data-rule-notequaltogroup='[".distinctPh"]' data-rule-phonenumber data-msg-notequaltogroup="Please enter a unique contact number"/>
                                        {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this,e, idx , 'OrganisationPh')} >
                                            <i  className="icon icon-decrease-icon Add-2" ></i>
                                            </button> : (this.state.OrganisationPh.length == 3)? '': <button onClick={(e) =>handleAddShareholder(this,e, 'OrganisationPh',value)}>
                                            <i  className="icon icon-add-icons Add-1" ></i>
                                            </button>
                                        }
                                    </div>
                                </span>
                            </div>
                         ))}
                    </div>

                    <div className="col-sm-4 mt-4">
                    {
                        this.state.OrganisationEmail.map((value, idx) => (
                        <div className="mb-4" key={idx+1}>
                        <label>Email ({(idx == 0)?'Primary':'Secondary'}):</label>
                        <span className="required">
                            <div className="input_plus__ mb-1">
                                <input type="email" className="input_f distinctEmail" placeholder="example@example.com" value={value.email} name={'org_email'+idx} required onChange={(e) => handleShareholderNameChange(this,'OrganisationEmail',idx,'email',e.target.value)} autoComplete="new-password" data-rule-notequaltogroup='[".distinctEmail"]'/>
                                {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this,e, idx , 'OrganisationEmail')} >
                                            <i  className="icon icon-decrease-icon Add-2" ></i>
                                            </button> : (this.state.OrganisationEmail.length == 3)? '': <button onClick={(e) =>handleAddShareholder(this,e, 'OrganisationEmail',value)}>
                                            <i  className="icon icon-add-icons Add-1" ></i>
                                            </button>
                                        } 
                            </div>
                            </span>
                        </div>
                    ))}
                    </div>
                    </div>
                        
                    <div className="row">
                        <div className="col-sm-7"></div>
                        <div className="col-sm-5 P_15_T">
                            <button disabled={this.state.loading} onClick={this.onSubmit} className="but_submit">Save Changes</button>
                        </div>
                    </div>

                </div>
            </form>
            </Modal.Body>
        </Modal>

            )
    }
}
OrganisationAddContactPopUp.defaultProps ={
    fromUrl: 'organisation/OrgDashboard/add_org_contact'
};
export default OrganisationAddContactPopUp;