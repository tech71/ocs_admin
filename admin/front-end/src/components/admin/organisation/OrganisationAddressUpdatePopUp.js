import React, { Component } from 'react';
import jQuery from "jquery";
import { postData,getOptionsSuburb,handleChangeChkboxInput,handleRemoveShareholder,handleAddShareholder,handleShareholderNameChange,handleChangeSelectDatepicker } from '../../../service/common.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import Modal from 'react-bootstrap/lib/Modal'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ToastUndo } from 'service/ToastUndo.js'

class OrganisationAddressUpdatePopUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            OrganisationPh: [{ 'phone': '' }],
            OrganisationEmail: [{ 'email': '' }],
        }
    }
    
    componentWillReceiveProps(newProps) {
        this.setState(newProps.selectedData);
        this.setState({mode:newProps.mode,pageTitle:newProps.pageTitle,orgId:newProps.orgId});
    }

    selectChange=(selectedOption, fieldname) => {
        var state = {};
        state[fieldname] = selectedOption; 
        if(fieldname == 'city' && selectedOption)       
            state['postal'] = state.city.postcode; 

        this.setState(state);        
    } 
 
    componentDidMount() {
        postData('common/Common/get_state', {}).then((result) => {
            if (result.status) {
                var details = result.data
                this.setState({stateList: details});
            }
        });
    }

    onSubmit = (e) => {
        e.preventDefault()
         jQuery('#updateAddress').validate({  ignore: []});
         if(jQuery('#updateAddress').valid()){
            this.setState({loading: true},() => {
            postData(this.props.formUrl, this.state).then((result) => {  
       
            if (result.status) {
                toast.success(<ToastUndo message={result.msg} showType={'s'} />, {
                // toast.success(result.msg, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                }); 
                this.props.closeEditPopUp(true);
            }
            else
            {
                toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                // toast.error(result.error, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                }); 
            }
            this.setState({loading: false})
        });
            });
       }
    }
    
    render() {
        return (
             <Modal
               className="Modal fade Modal_A Modal_B Green"
               show={this.props.modal_show}
               container={this}
               aria-labelledby="contained-modal-title"
             >
              <Modal.Body>
            <form id="updateAddress">
                <div className="dis_cell-1">
                    <div className="text text-left by-1">{this.state.pageTitle}:
                        <a onClick={() => this.props.closeEditPopUp(false) } className="close_i pull-right mt-1"><i className="icon icon-cross-icons"></i></a>
                    </div>
                                             
                    <div className="row P_15_T" >                    
                            <div className="col-sm-4">
                                <label>Street: </label>
                                <span className="required">
                                    <input type="text" data-rule-required="true" onChange={(e) => handleChangeChkboxInput(this, e)} value={this.state.street} name="street" placeholder="" />
                                </span>
                            </div>
                            <div className="col-sm-2">
                                <label>State: </label>
                                <Select clearable={false} className="default_validation" simpleValue={true} required={true}
                                value={this.state.state}  onChange={(e) => handleChangeSelectDatepicker(this, e,'state')} 
                                options={this.state.stateList} placeholder="Please Select" />
                            </div>
                            <div className="col-sm-4">
                                <label>Suburb:</label> 
                                <span className="required search_icons_right modify_select">
                                         <Select.Async clearable={false} cache={false} className="default_validation"
                                    value={this.state.city} disabled={(this.state.state)? false: true} loadOptions={(val) => getOptionsSuburb(val, this.state.state)} onChange={(e) => this.selectChange(e,'city')} 
                                     placeholder="Please Select" required />
                                </span>
                            </div>

                            <div className="col-sm-2">
                                <label>Postcode: </label> 
                                <span className="required">
                                    <input type="text" value={this.state.postal} onChange={(e) => handleChangeChkboxInput(this, e)}  data-rule-required="true" className="text-center" name="postal" placeholder="" data-rule-number="true" minLength="4" maxLength="4" data-rule-postcodecheck="true"/>
                                </span>
                            </div>
                            
                            <div className="col-sm-4 mt-4">
                                <label>Site Name: </label> 
                                <span className="required">
                                    <input type="text" defaultValue={this.state.name} readOnly={true} data-rule-required="true" className="text-center" name="name" placeholder="" />
                                </span>
                                
                            </div>
                       
                            {/*
                            <div className="col-sm-4 mt-4">
                                <label>Key Contact: </label> 
                                <span className="required">
                                    <input type="text" value={this.state.key_contact_label} onChange={(e) => handleChangeChkboxInput(this, e)}  data-rule-required="true" className="text-left" name="key_contact_label" placeholder="" />
                                </span>
                            </div>
                           
                            <div className="col-sm-4 mt-4">
                                <label>Position: </label> 
                                <span className="required">
                                    <input type="text" value={this.state.position} onChange={(e) => handleChangeChkboxInput(this, e)}  data-rule-required="true" className="text-left" name="position" placeholder="" />
                                </span>
                            </div>

                            <div className="col-sm-4 mt-4">
                                <label>Department: </label> 
                                <span className="required">
                                    <input type="text" value={this.state.department} onChange={(e) => handleChangeChkboxInput(this, e)}  data-rule-required="true" className="text-left" name="department" placeholder="" />
                                </span>
                            </div>
                        */}
                        <div className="col-sm-4 mt-4">
                            {this.state.OrganisationPh.map((value, idx) => (
                                <div className="mb-4" key={idx + 1}>
                                    <label>Phone ({(idx == 0) ? 'Primary' : 'Secondary'}):</label>
                                    <span className="required">
                                        <div className="input_plus__ mb-1">
                                            <input type="text" className="input_f distinctOrgPh" placeholder="Can Include Area Code" value={value.phone} name={'org_phone' + idx} required onChange={(e) => handleShareholderNameChange(this, 'OrganisationPh', idx, 'phone', e.target.value)} autoComplete="new-password" data-rule-notequaltogroup='[".distinctOrgPh"]' data-rule-phonenumber/>
                                            {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this, e, idx, 'OrganisationPh')} >
                                                <i className="icon icon-decrease-icon Add-2" ></i>
                                            </button> : (this.state.OrganisationPh.length == 3) ? '' : <button onClick={(e) => handleAddShareholder(this, e, 'OrganisationPh', value)}>
                                                <i className="icon icon-add-icons Add-1" ></i>
                                            </button>
                                            }
                                        </div>
                                    </span>
                                </div>
                            ))}
                        </div>

                        <div className="col-sm-4 mt-4">
                            {
                                this.state.OrganisationEmail.map((value, idx) => (
                                    <div className="mb-4" key={idx + 1}>
                                        <label>Email ({(idx == 0) ? 'Primary' : 'Secondary'}):</label>
                                        <span className="required">
                                            <div className="input_plus__ mb-1">
                                                <input type="email" className="input_f distinctEmail" placeholder="example@example.com" value={value.email} name={'org_email' + idx} required onChange={(e) => handleShareholderNameChange(this, 'OrganisationEmail', idx, 'email', e.target.value)} autoComplete="new-password" data-rule-notequaltogroup='[".distinctEmail"]' />
                                                {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this, e, idx, 'OrganisationEmail')} >
                                                    <i className="icon icon-decrease-icon Add-2" ></i>
                                                </button> : (this.state.OrganisationEmail.length == 3) ? '' : <button onClick={(e) => handleAddShareholder(this, e, 'OrganisationEmail', value)}>
                                                    <i className="icon icon-add-icons Add-1" ></i>
                                                </button>
                                                }
                                            </div>
                                        </span>
                                    </div>
                                ))}
                        </div>

                        </div>
                   
                        
                    <div className="row">
                        <div className="col-sm-7"></div>
                        <div className="col-sm-5 P_15_T">
                            <button disabled={this.state.loading} onClick={this.onSubmit} className="but_submit">Save Changes</button>
                        </div>
                    </div>

                </div>
            </form>
            </Modal.Body>
        </Modal>

            )
    }
}
export default OrganisationAddressUpdatePopUp;