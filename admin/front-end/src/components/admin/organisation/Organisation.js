import React from 'react';
import BlockUi from 'react-block-ui';
import  'react-block-ui/style.css';

class Organisation extends React.Component {
	constructor(props) {
		super(props);
        this.state = {
            loading:true,
        };       
    }

render() {	
	return (
		<div>
		   <BlockUi tag="div" blocking={this.state.loading}>
            <section className="manage_top">
            <div className="container-fluid Green">
                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1 P_15_TB">
                        <img className="back_icons" src={'/assets/images/Members_icons/back_arrow.svg '} />
                    </div>
                    <div className="col-lg-1"></div>
                    <div className="col-lg-10 col-lg-offset-1"><div className="bor_T"></div></div>
                </div>
                   
            </div>          
        </section>	
        </BlockUi>	
		</div>
		);
	}
}
export default Organisation;

