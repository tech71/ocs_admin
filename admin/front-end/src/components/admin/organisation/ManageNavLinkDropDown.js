import { OrgWillAdded} from 'menujson/org_menu_json';

export const showOrgMenu = (obj, tempMenu, name) => {
    var index = tempMenu.findIndex(x => x.id == 'sub_orgs');
    tempMenu[index]['name'] = name;
    tempMenu[index]['className'] = 'active';
    tempMenu[index]['submenus'] = JSON.parse(JSON.stringify(OrgWillAdded));
    return tempMenu;
}

export const closeOrgDetailsMenu = (obj, tempMenu) => {
    var index = tempMenu.findIndex(x => x.id == 'sub_orgs');

    var tempSubMenu = tempMenu[index]['submenus'];
    var subIndex = tempSubMenu.findIndex(x => x.id == 'org_details');
    tempMenu[index]['submenus'][subIndex]['closeMenus'] = true;
    return tempMenu;
}

export const showSubOrgMenu = (obj, tempMenu, name) => {
    var index = tempMenu.findIndex(x => x.id == 'suborg_details_menu');
    tempMenu[index]['name'] = name;
    tempMenu[index]['className'] = 'active'
    tempMenu[index]['linkShow'] = true;
    tempMenu[index]['closeMenus'] = false;
    
    var tempSubMenu = tempMenu[index]['submenus'];
    var subIndex = tempSubMenu.findIndex(x => x.id == 'suborg_details');
    tempMenu[index]['submenus'][subIndex]['closeMenus'] = false;
    
    return tempMenu;
}

export const closeSubOrgDetailsMenu = (obj, tempMenu) => {
    var index = tempMenu.findIndex(x => x.id == 'suborg_details_menu');

    var tempSubMenu = tempMenu[index]['submenus'];
    var subIndex = tempSubMenu.findIndex(x => x.id == 'suborg_details');
    tempMenu[index]['submenus'][subIndex]['closeMenus'] = true;
    return tempMenu;
}

export const showSubOrgHouseMenu = (obj, tempMenu, name) => {
    var sbindex = tempMenu.findIndex(x => x.id == 'suborg_house_menu');
    tempMenu[sbindex]['name'] = name;
    tempMenu[sbindex]['className'] = 'active'
    tempMenu[sbindex]['linkShow'] = true;
    tempMenu[sbindex]['closeMenus'] = false;

    return tempMenu;
}

export const removeSubOrgHouseMenu = (obj, tempMenu, name) => {
    var sbindex = tempMenu.findIndex(x => x.id == 'suborg_house_menu');
    tempMenu[sbindex]['name'] = '';
    tempMenu[sbindex]['linkShow'] = false;

    return tempMenu;
}

export const removeSubOrgMenu = (obj, tempMenu, name) => {
    var sbindex = tempMenu.findIndex(x => x.id == 'suborg_details_menu');
    tempMenu[sbindex]['name'] = '';
    tempMenu[sbindex]['linkShow'] = false;

    return tempMenu;
}

export const resetOrgMenu = (obj, tempMenu, name) => {
    var index = tempMenu.findIndex(x => x.id == 'sub_orgs');
    tempMenu[index]['name'] = 'Sub-Orgs';
    tempMenu[index]['submenus'] = [];

    return tempMenu;
}