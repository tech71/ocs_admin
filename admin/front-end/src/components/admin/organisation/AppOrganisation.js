import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';


import { checkItsNotLoggedIn, getPermission, postData } from '../../../service/common.js';
import { ROUTER_PATH, BASE_URL } from '../../../config.js';
import BlockUi from 'react-block-ui';
import {orgJson, OrgWillAdded} from 'menujson/org_menu_json';
import OrganisationProfile from './OrganisationProfile';
import OrganisationOverview from './OrganisationOverview';
import OrganisationSubOrg from './OrganisationSubOrg';
import OrganisationSites from './OrganisationSites';
import OrganisationContacts from './OrganisationContacts';
import OrganisationFms from './OrganisationFms';
import OrganisationCreate from './OrganisationCreate';

import SubOrganisationAbout from './sub_org/SubOrganisationAbout';
import SubOrganisationContact from './sub_org/SubOrganisationContacts';
import SubOrganisationFms from './sub_org/SubOrganisationFms';
import SubOrganisationHouseFms from './sub_org/SubOrganisationHouseFms';

import SubOrganisationContactBilling from './sub_org/SubOrganisationContactBilling';
import SubOrganisationSites from './sub_org/SubOrganisationSites';
import SubOrganisationDocs from './sub_org/SubOrganisationDocs';
import SubOrganisationHouseAbout from './sub_org/SubOrganisationHouseAbout';
import { connect } from 'react-redux'
import { setOrgProfileData } from './actions/OrganisationAction.js';
import { setFooterColor } from '../../admin/notification/actions/NotificationAction.js';
import Sidebar from '../Sidebar';

import OrganisationDashboard from './OrganisationDashboard';
import {setSubmenuShow} from 'components/admin/actions/SidebarAction';
import {showOrgMenu, closeOrgDetailsMenu, showSubOrgMenu, closeSubOrgDetailsMenu, showSubOrgHouseMenu, removeSubOrgMenu, removeSubOrgHouseMenu, resetOrgMenu} from './ManageNavLinkDropDown';

class AppOrganisation extends Component {
    constructor(props) {
        super(props);
        this.permission = (getPermission() == undefined) ? [] : JSON.parse(getPermission());
        checkItsNotLoggedIn(ROUTER_PATH);
     
        this.state = {
            loading: false,
            name: '',
            menus: orgJson,
            replaceData:{':id':0},
            subMenuShowStatus:true,
        };

         this.sideBar = React.createRef();
        this.props.setFooterColor('Green');
    }

    permissionRediect() {
        checkItsNotLoggedIn();
        return <Redirect to={'/admin/no_access'} />;
    }

    componentWillUnmount() {
        this.props.setFooterColor('');
    }
    
    updateMenu = (activePage, newProps) => {
       let tempMenu = JSON.parse(JSON.stringify(this.state.menus)); 
       
       tempMenu = resetOrgMenu(this, tempMenu);
       tempMenu = removeSubOrgMenu(this, tempMenu);
       tempMenu = removeSubOrgHouseMenu(this, tempMenu);
           

       if(activePage === 'org_overview' || activePage === 'org_sub_orgs_listing' || activePage === 'org_sites_listing'){
           this.setState({replaceData:{':orgId' : newProps.props.match.params.id}});

            tempMenu = showOrgMenu(this, tempMenu, newProps.OrganisationProfile.name, true);
            
            if((activePage === 'org_sub_orgs_listing') || (activePage === 'org_sites_listing')){
                tempMenu = closeOrgDetailsMenu(this, tempMenu);
            }
       }
    
       if(activePage === 'suborg_overview' || activePage === 'suborg_sites_listing'){
           this.setState({replaceData:{':orgId': newProps.props.match.params.id, ':subOrgId': newProps.props.match.params.subOrgId}});
      
           tempMenu = showOrgMenu(this, tempMenu, newProps.OrganisationProfile.parent_org, false);
           tempMenu = showSubOrgMenu(this, tempMenu, newProps.OrganisationProfile.name, true);
           
           this.openNavLink(newProps.OrganisationProfile.parent_org+'_action', true)
           
           if(activePage === 'suborg_sites_listing'){
               tempMenu = closeSubOrgDetailsMenu(this, tempMenu, newProps);
           }
        }
        
       if(activePage === 'suborg_house_overview'){
              
              if(newProps.OrganisationProfile.anySubOrg){
                    this.setState({replaceData:{':orgId': newProps.OrganisationProfile.ocs_id,':subOrgId': newProps.props.match.params.orgId, ':houseid': newProps.props.match.params.houseId}});
                    tempMenu = showSubOrgMenu(this, tempMenu, newProps.OrganisationProfile.linked_to, false);
              }else{
                  this.setState({replaceData:{':orgId': newProps.props.match.params.orgId, ':subOrgId': newProps.props.match.params.orgId, ':houseid': newProps.props.match.params.houseId}});
              }
           
             tempMenu = showOrgMenu(this, tempMenu, newProps.OrganisationProfile.parent_org, false);
             tempMenu = showSubOrgHouseMenu(this, tempMenu, newProps.OrganisationProfile.name, true);
           
            this.openNavLink(newProps.OrganisationProfile.parent_org+'_action', true)
            this.openNavLink(newProps.OrganisationProfile.linked_to+'_action', true)
        }
        
        this.setState({menus : JSON.parse(JSON.stringify(tempMenu))});
    }

    componentDidMount() {
        this.updateMenu(this.props.activePage.pageType, this.props);
        this.props.setSubmenuShow(1);
        this.getOrgProfile(this.props);
    }
    
    componentWillReceiveProps = (newProps) => {
     
        if(JSON.stringify(this.props.props.match.params) !== JSON.stringify(newProps.props.match.params)){
            this.getOrgProfile(newProps);
        }
        this.updateMenu(newProps.activePage.pageType, newProps);
    }
    
    componentDidUpdate() {
        if (!this.profileFetch) {
            this.getOrgProfile(this.props);
        }
        if (this.props.props.location.pathname == '/admin/organisation/dashboard' || this.props.props.location.pathname == '/admin/organisation/createOrganisation') {
            this.profileFetch = false;
        }
    }


    getOrgProfile = (newProps) => {        
           console.log(newProps, 'check_props');
        var requestedUrl = newProps.props.location.pathname;

        var postUrl;
        if (newProps.props.match.params.houseId){
            postUrl = 'organisation/OrgDashboard/get_house_profile';
        } else if (requestedUrl == '/admin/organisation/dashboard' || requestedUrl == '/admin/organisation/createOrganisation') {
            this.profileFetch = false;
            return false
        }
        else {
            postUrl = 'organisation/OrgDashboard/get_organisation_profile';
        }
        this.profileFetch = true;
        this.setState({ loading: true });

        postData(postUrl, newProps.props.match.params).then((result) => {
            if (result.status) {
                this.props.getOrgProfileData(result.data.basic_detail);
            } else {
                window.location = '/admin/organisation/dashboard';
                this.setState({ error: result.error });
            }
            this.setState({ loading: false });
        });
    }
    
    openNavLink(menuActionName, sts) {
        this.sideBar.current.hideShowNavDropDown(menuActionName, sts);
    }

    render() {
        const profileFetchShow = this.props.props.location.pathname == '/admin/organisation/dashboard' || this.props.props.location.pathname == '/admin/organisation/createOrganisation' ? false : true;
        return (

            <div className="asideSect__ Green">
                <Sidebar
                    heading={'Organisations'}
                    menus={this.state.menus}
                    subMenuShowStatus={this.state.subMenuShowStatus}
                    replacePropsData={this.state.replaceData}
                    ref={this.sideBar}
                />
                <BlockUi tag="div" blocking={this.state.loading}>
                    <section className="manage_top">
                        <div className="container-fluid">
                            {profileFetchShow ? <div className="row">
                                <div className="col-lg-10 col-lg-offset-1 P_15_TB pb-2">
                                    <a onClick={this.props.props.history.goBack}><span className="icon icon-back-arrow back_arrow"></span></a>
                                </div>
                                <div className="col-lg-1"></div>
                                <div className="col-lg-10 col-lg-offset-1">
                                    <div className="bor_T"></div>
                                </div>
                            </div> : <React.Fragment />}

                            {profileFetchShow ? <OrganisationProfile /> : <React.Fragment />}

                            <Switch >
                                <Route exact path={ROUTER_PATH + 'admin/organisation/dashboard'} render={(props) => this.permission.access_organization ? <OrganisationDashboard props={props} /> : this.permissionRediect()} />

                                <Route exact path={ROUTER_PATH + 'admin/organisation/overview/:id'} render={(props) => this.permission.access_organization ? <OrganisationOverview InnerMenu={'0'} props={props} getOrgProfile={this.getOrgProfile} /> : this.permissionRediect()} />

                                <Route exact path={ROUTER_PATH + 'admin/organisation/suborg/:id'} render={(props) => this.permission.access_organization ? <OrganisationSubOrg InnerMenu={'0'} props={props} /> : this.permissionRediect()} />

                                <Route exact path={ROUTER_PATH + 'admin/organisation/sites/:id'} render={(props) => this.permission.access_organization ? <OrganisationSites InnerMenu={'0'} props={props} /> : this.permissionRediect()} />

                                <Route exact path={ROUTER_PATH + 'admin/organisation/contacts/:id'} render={(props) => this.permission.access_organization ? <OrganisationContacts InnerMenu={'0'} props={props} /> : this.permissionRediect()} />

                                <Route exact path={ROUTER_PATH + 'admin/organisation/fms/:id'} render={(props) => this.permission.access_organization ? <OrganisationFms InnerMenu={'0'} props={props} /> : this.permissionRediect()} />


                                {/*Sub-Organisation Route*/}
                                <Route exact path={ROUTER_PATH + 'admin/organisation/overview/:id/:subOrgId'} render={(props) => this.permission.access_organization ? <SubOrganisationAbout InnerMenu={'1'} props={props} /> : this.permissionRediect()} />

                                <Route exact path={ROUTER_PATH + 'admin/organisation/contacts/:id/:subOrgId'} render={(props) => this.permission.access_organization ? <SubOrganisationContact InnerMenu={'1'} props={props} /> : this.permissionRediect()} />

                                <Route exact path={ROUTER_PATH + 'admin/organisation/fms/:id/:subOrgId'} render={(props) => this.permission.access_organization ? <SubOrganisationFms InnerMenu={'1'} props={props} /> : this.permissionRediect()} />


                                {/*House Route*/}
                                <Route exact path={ROUTER_PATH + 'admin/organisation/house_about/:houseId/:orgId'} render={(props) => this.permission.access_organization ? <SubOrganisationHouseAbout props={props} /> : this.permissionRediect()} />
                                <Route exact path={ROUTER_PATH + 'admin/organisation/contactBilling/:houseId/:orgId'} render={(props) => this.permission.access_organization ? <SubOrganisationContactBilling InnerMenu={'2'} props={props} /> : this.permissionRediect()} />
                                <Route exact path={ROUTER_PATH + 'admin/organisation/docs/:houseId/:orgId'} render={(props) => this.permission.access_organization ? <SubOrganisationDocs orgName={this.state.orgName} InnerMenu={'2'} props={props} /> : this.permissionRediect()} />
                                <Route exact path={ROUTER_PATH + 'admin/organisation/house_fms/:houseId/:orgId'} render={(props) => this.permission.access_organization ? <SubOrganisationHouseFms InnerMenu={'2'} callFor='house_fms' props={props} /> : this.permissionRediect()} />
                                <Route exact path={ROUTER_PATH + 'admin/organisation/sites/:id/:subOrgId'} render={(props) => this.permission.access_organization ? <SubOrganisationSites InnerMenu={true} props={props} /> : this.permissionRediect()} />
                                <Route exact path={ROUTER_PATH + 'admin/organisation/createOrganisation'} render={(props) => this.permission.create_organization ? <OrganisationCreate props={props} /> : this.permissionRediect()} />

                            </Switch>

                        </div>
                    </section>
                </BlockUi>
            </div>

        );
    }
}
//export default AppOrganisation

const mapStateToProps = state => ({
    OrganisationProfile: state.OrganisationReducer.orgProfile,
    activePage: state.OrganisationReducer.activePage,
})

const mapDispatchtoProps = (dispach) => {
    return {
        getOrgProfileData: (value) => dispach(setOrgProfileData(value)),
        setFooterColor: (result) => dispach(setFooterColor(result)),
        setSubmenuShow: (result) => dispach(setSubmenuShow(result)),
    }
}

const AppOrganisationData = connect(mapStateToProps, mapDispatchtoProps)(AppOrganisation)
export { AppOrganisationData as AppOrganisation };                