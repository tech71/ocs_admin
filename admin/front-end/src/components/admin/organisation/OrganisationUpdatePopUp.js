import React, { Component } from 'react';
import jQuery from "jquery";
import { postData,getOptionsSuburb,handleChangeChkboxInput,handleChangeSelectDatepicker,handleShareholderNameChange,handleAddShareholder,handleRemoveShareholder,postImageData } from '../../../service/common.js';
import {orgGst,orgTax} from '../../../dropdown/Orgdropdown.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import Modal from 'react-bootstrap/lib/Modal'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux'
import { setOrgBookingData } from './actions/OrganisationAction.js';
import { ToastUndo } from 'service/ToastUndo.js'

class OrganisationUpdatePopUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
           OrganisationPh:[{'phone':''}],
           OrganisationEmail:[{'email':''}],
           selectedFile: null,
           is_upload_img:false
        }
    }
    
    fileChangedHandler = (event) => {
         this.setState({selectedFile: event.target.files[0], filename: event.target.files[0].name,is_upload_img:true})
    }

    componentWillReceiveProps(newProps) {
        this.setState(newProps.OrganisationProfile,()=>{
            if(this.state.OrganisationPh.length == 0)
            {
                this.setState({OrganisationPh:[{'phone':''}]});
            }

            if(this.state.OrganisationEmail.length == 0)
            {
                this.setState({OrganisationEmail:[{'email':''}]});
            }
        });
        this.setState({pageTitle:newProps.pageTitle});
    }

    selectChange=(selectedOption, fieldname) => {
        var state = {};
        state[fieldname] = selectedOption; 
        if(fieldname == 'city' && selectedOption)       
            state['postal'] = state.city.postcode; 

        this.setState(state);        
    } 
 
    componentDidMount() {
        postData('common/Common/get_state', {}).then((result) => {
            if (result.status) {
                var details = result.data
                this.setState({stateList: details});
            }
        });
    }
    
    onSubmit = (e) => {
        e.preventDefault()
         jQuery('#updateAddress').validate({  ignore: []});
         if(jQuery('#updateAddress').valid()){
            this.setState({loading: true},() => {
            const formData = new FormData()
            if(this.state.is_upload_img)
            formData.append('myFile', this.state.selectedFile, this.state.selectedFile.name)
            formData.append('current_state', JSON.stringify(this.state))
            postImageData('organisation/OrgDashboard/update_org', formData).then((result) => {  
            if (result.status) {
                toast.success(<ToastUndo message={result.msg} showType={'s'} />, {
                // toast.success(result.msg, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                }); 
              
                var NewReduxData = {
                                    //logo_file:this.state.booking_status,  
                                    street:this.state.street,
                                    city:this.state.city,
                                    postal:this.state.postal,
                                    statename:this.state.statename,
                                    primary_address:this.state.street+', '+this.state.city.label+', '+this.state.postal+' '+this.state.statename,
                                    OrganisationEmail:this.state.OrganisationEmail,
                                    OrganisationPh:this.state.OrganisationPh,
                                    gst:this.state.gst,
                                    payroll_tax:this.state.payroll_tax,
                                    website:this.state.website
                                };
                this.props.org_update_booking_comp_fun(NewReduxData);
                this.props.closeOrgUpdatePopUp(true);
            }
            else
            {
                toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                // toast.error(result.error, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                }); 
            }
            this.setState({loading: false})
        });
            });
       }
    }   
    
  
    render() {
        return (
             <Modal
               className="Modal fade Modal_A Modal_B Green"
               show={this.props.org_update_modal}
               container={this}
               aria-labelledby="contained-modal-title"
             >
              <Modal.Body>
            <form id="updateAddress">
                <div className="dis_cell-1">
                    <div className="text text-left by-1"><div className="M_t0">{this.state.pageTitle}:</div>
                        <a onClick={() => this.props.closeOrgUpdatePopUp() } className="close_i pull-right mt-1"><i className="icon icon-cross-icons"></i></a>
                    </div>
                                             
                    <div className="row P_15_T" >                    
                            <div className="col-sm-4">
                                <label>Street: </label>
                                <span className="required">
                                    <input type="text" data-rule-required="true" onChange={(e) => handleChangeChkboxInput(this, e)} value={this.state.street} name="street" placeholder="" />
                                </span>
                            </div>
                            <div className="col-sm-2">
                                <label>State: </label>
                                <Select clearable={false} className="default_validation" simpleValue={true} required={true}
                                value={this.state.state}  onChange={(e) => handleChangeSelectDatepicker(this, e,'state')} 
                                options={this.state.stateList} placeholder="Please Select" />
                            </div>
                            <div className="col-sm-4">
                                <label>Suburb:</label> 
                                <span className="required">
                                    <Select.Async clearable={false} cache={false} className="default_validation"
                                    value={this.state.city} disabled={(this.state.state)? false: true} loadOptions={(val) => getOptionsSuburb(val, this.state.state)} onChange={(e) => this.selectChange(e,'city')} 
                                     placeholder="Please Select" required />
                                </span>
                            </div>

                            <div className="col-sm-2">
                                <label>Postcode: </label> 
                                <span className="required">
                                    <input type="text" value={this.state.postal} onChange={(e) => handleChangeChkboxInput(this, e)}  data-rule-required="true" className="text-center" name="postal" placeholder="" data-rule-number="true" minlength="4" maxlength="4" data-rule-postcodecheck="true"/>
                                </span>
                            </div>
                         
                        <div className="col-sm-4 mt-4">                       
                        <label>Payroll Tax:</label>
                        <span className="required">
                            <div className="add_input">
                                <Select name="payroll_tax" required={true} simpleValue={true} searchable={false} clearable={false} options={orgTax()} value={this.state.payroll_tax || 2} onChange={(e)=> this.setState({'payroll_tax':e})} />
                            </div>
                            </span>
                    </div>
                    
                    <div className="col-sm-4 mt-4">
                        <label>GST:</label>
                        <span className="required">
                            <div className="add_input">
                                <Select name="gst" required={true} simpleValue={true} searchable={false} clearable={false} options={orgGst()} value={this.state.gst || 2} onChange={(e)=> this.setState({'gst':e})}/>
                            </div>
                            </span>
                    </div>

                     <div className="col-sm-4 mt-4">
                            <label>Website: </label> 
                            <span className="required">
                                <input type="text" value={this.state.website} onChange={(e) => handleChangeChkboxInput(this, e)}  data-rule-required="true" className="text-left" name="website" placeholder="" />
                            </span>
                        </div>


                      <div className="col-sm-4 mt-4">
                           {this.state.OrganisationPh.map((value, idx) => (
                            <div className="mb-4" key={idx+1}>
                                <label>Phone ({(idx == 0)?'Primary':'Secondary'}):</label>
                                <span className="required">
                                    <div className="input_plus__ mb-1">
                                        <input type="text" className="input_f distinctOrgPh" placeholder="Can Include Area Code" value={value.phone} name={'org_phone'+idx} required onChange={(e) => handleShareholderNameChange(this,'OrganisationPh',idx,'phone',e.target.value)} autoComplete="new-password" data-rule-notequaltogroup='[".distinctOrgPh"]' data-msg-notequaltogroup="Please enter a unique contact number"/>
                                        {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this,e, idx , 'OrganisationPh')} >
                                            <i  className="icon icon-decrease-icon Add-2" ></i>
                                            </button> : (this.state.OrganisationPh.length == 3)? '': <button onClick={(e) =>handleAddShareholder(this,e, 'OrganisationPh',value)}>
                                            <i  className="icon icon-add-icons Add-1" ></i>
                                            </button>
                                        }
                                    </div>
                                </span>
                            </div>
                         ))}
                        </div>

                        <div className="col-sm-4 mt-4">
                            {
                            this.state.OrganisationEmail.map((value, idx) => (
                            <div className="mb-4" key={idx+1}>
                            <label>Email ({(idx == 0)?'Primary':'Secondary'}):</label>
                            <span className="required">
                                <div className="input_plus__ mb-1">
                                    <input type="email" className="input_f distinctEmail" placeholder="example@example.com" value={value.email} name={'org_email'+idx} required onChange={(e) => handleShareholderNameChange(this,'OrganisationEmail',idx,'email',e.target.value)} autoComplete="new-password" data-rule-notequaltogroup='[".distinctEmail"]'/>
                                    {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this,e, idx , 'OrganisationEmail')} >
                                                <i  className="icon icon-decrease-icon Add-2" ></i>
                                                </button> : (this.state.OrganisationEmail.length == 3)? '': <button onClick={(e) =>handleAddShareholder(this,e, 'OrganisationEmail',value)}>
                                                <i  className="icon icon-add-icons Add-1" ></i>
                                                </button>
                                            } 
                                        </div>
                                    </span>
                                </div>
                            ))}
                        </div> 

                        <div className="col-sm-4 mt-4">
                            <label>Logo:</label> 
                                <span className="upload_btn">
                                <label className="btn btn-default btn-sm center-block btn-file">
                                <i className="but" aria-hidden="true">Upload logo</i>
                                <input className="p-hidden" type="file" name="myFile" onChange={this.fileChangedHandler} />                         
                             </label>
                             </span>
                            {(this.state.filename)? <p>File Name: <small>{this.state.filename}</small></p>: ''}
                        </div> 

                        </div>
                   
                        
                    <div className="row">
                        <div className="col-sm-7"></div>
                        <div className="col-sm-5 P_15_T">
                            <button disabled={this.state.loading} onClick={this.onSubmit} className="but_submit">Save Changes</button>
                        </div>
                    </div>

                </div>
            </form>
            </Modal.Body>
        </Modal>

            )
    }
}

const mapStateToProps = state => ({
    OrganisationProfile : state.OrganisationReducer.orgProfile
})

 const mapDispatchtoProps = (dispach) => {
       return {
            org_update_booking_comp_fun: (value) => dispach(setOrgBookingData(value)),
      }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(OrganisationUpdatePopUp)
