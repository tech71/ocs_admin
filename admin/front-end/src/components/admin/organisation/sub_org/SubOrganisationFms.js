import React, { Component } from 'react';
import jQuery from "jquery";
import {BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { ROUTER_PATH, BASE_URL, PAGINATION_SHOW } from '../../../../config.js';
import Select from 'react-select-plus';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import {orgFmsViewBy} from '../../../../dropdown/Orgdropdown.js';
import { checkItsNotLoggedIn, postData, checkPinVerified } from '../../../../service/common.js';
import { connect } from 'react-redux';
import { TotalShowOnTable } from '../../../../service/TotalShowOnTable';
import Pagination from "../../../../service/Pagination.js";
import { setActiveSelectPage } from './../actions/OrganisationAction.js'

const requestData = (pageSize, page, sorted, filtered,orgId) => {
    return new Promise((resolve, reject) => {
        // request json
        var Request = JSON.stringify({pageSize: pageSize, page: page, sorted: sorted, filtered: filtered,orgId:orgId});
        postData('organisation/OrgDashboard/get_org_fms', Request).then((result) => {
            let filteredData = result.data;

            const res = {
                rows: filteredData,
                pages: (result.count),
                all_count:result.all_count,
                total_duration: result.total_duration
            };
            resolve(res);
        });
    });
};

class OrganisationFms extends React.Component {
	constructor(props) {
		super(props);
        this.state = {
            loading:true,
            fmsListing:[],
            view_by_org:0,
        };
        checkItsNotLoggedIn(ROUTER_PATH);
        checkPinVerified('fms');
        this.urlParam = this.props.props.match.params.id;
    }

    componentDidMount() { 
         this.props.setActiveSelectPage('suborg_overview');
    }
    
    

    fetchData = (state, instance) => {
    this.setState({loading: true});
        requestData(
                state.pageSize,
                state.page,
                state.sorted,
                state.filtered,
                this.urlParam
                ).then(res => {
            this.setState({
                fmsListing: res.rows,
                pages: res.pages,
                all_count:res.all_count,
                loading: false,
                total_duration:res.total_duration
            });
        })
    }

    viewByselectChange(selectedOption, fieldname) { 
       var requestData = {archive:selectedOption};
       this.setState({filtered:requestData,view_by_org:selectedOption});       
    }  

    render() {
    const columns = [ 
            {Header: 'ID', accessor: 'id', filterable: false,  },
            {Header: 'Timer', accessor: 'timelapse', filterable: false,sortable:false },
            {Header: 'Category', accessor: 'case_category', filterable: false,sortable:false },
            {Header: 'Date of Event ', accessor: 'event_date', filterable: false, },
            {Header: 'Initiated By ', accessor: 'initiated_by', sortable:false},
            {Header: 'Against To ', accessor: 'against_for', sortable:false},
            {Cell: (props) => <span><a href={'/admin/fms/case/'+ props.original.id }><i className="icon icon-views"></i></a></span>, Header: <div className="">Action</div>,
            Header: <TotalShowOnTable countData={this.state.all_count} />,
            style: {
                    "textAlign": "right", 
                  }, headerStyle: {border:"0px solid #fff" }, sortable: false}, 
        ];
        const propsData = {};
        if(this.props.props.match.params.hasOwnProperty('subOrgId') && this.props.props.match.params.subOrgId!=''){
            propsData['subOrgId'] = this.props.props.match.params.subOrgId;
        }

        if(this.props.props.match.params.hasOwnProperty('id') && this.props.props.match.params.id!=''){
            propsData['organisationId'] = this.props.props.match.params.id;
        }
     return (
      <div>
      
                
                <div className="row">
                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                   
                        <div className="row">
                            <div className="col-lg-9 col-sm-8">
                                <div className="row">
                                <div className="col-sm-12"><div className="bor_T"></div></div>
                                    <div className="col-sm-12 P_7_TB"><h3>'{this.props.OrganisationProfile.name}',Sub-Org FMS Cases:</h3></div>
                                    <div className="col-sm-12"><div className="bor_T"></div></div>
                                </div>
                            </div>
                            <div className="col-lg-3 col-sm-4">
                        <div className="box">
                            <Select name="view_by_org" required={true} simpleValue={true} searchable={false} clearable={false} 
                            options={orgFmsViewBy()} placeholder="View By" onChange={(e)=> this.viewByselectChange(e,'view_by_org')} value={this.state.view_by_org}/> 
                        </div>
                    </div>
                        </div>
                        
                         <div className="row P_15_TB">
                                <div className="col-lg-12 col-sm-12">
                            <div className="schedule_listings  PL_site">
                                <ReactTable
                                 PaginationComponent={Pagination} 
                                columns={columns}
                                manual 
                                data={this.state.fmsListing}
                                pages={this.state.pages}
                                loading={this.state.loading} 
                                onFetchData={this.fetchData} 
                                filtered={this.state.filtered}
                                defaultFiltered ={{archive: '0'}}
                                defaultPageSize={10}
                                className="-striped -highlight"  
                                noDataText="No Record Found"
                                minRows={2}
                                previousText={<span className="icon icon-arrow-left privious"></span>}
                                            nextText={<span className="icon icon-arrow-right next"></span>}
                                showPagination={this.state.fmsListing.length > PAGINATION_SHOW ? true : false }
                                />
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-1 pull-right"></div>
                    <div className="col-lg-2 col-sm-3 pull-right">
                        <a className="but" href={'/admin/fms/create_case'}>Initiate FMS Case</a>
                    </div>
                </div>
      </div>
      );
  }
}

//
const mapStateToProps = state => ({
    //ActiveClass : state.OrganisationReducer.ActiveClassProfilePage,
    OrganisationProfile : state.OrganisationReducer.orgProfile
})

 const mapDispatchtoProps = (dispach) => {
       return {
           setActiveSelectPage: (value) => dispach(setActiveSelectPage(value)),
      }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(OrganisationFms)