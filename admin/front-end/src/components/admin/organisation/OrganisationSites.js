import React from 'react';
import { ROUTER_PATH, PAGINATION_SHOW } from '../../../config.js';
import { postData,checkItsNotLoggedIn,archiveALL,reFreashReactTable,handleChangeChkboxInput } from '../../../service/common.js';
//import OrganisationAddressUpdatePopUp from './OrganisationAddressUpdatePopUp';  // old pop up
import OrganisationAddSitePopUp from './OrganisationAddSitePopUp';  // dashboard pop to add site

import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import {siteViewBy} from '../../../dropdown/Orgdropdown.js';
import "react-placeholder/lib/reactPlaceholder.css";
import 'react-toastify/dist/ReactToastify.css';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import { connect } from 'react-redux'
import { setOrgActiveClassProfilePage } from './actions/OrganisationAction.js';
import  AttachSites from './AttachSites.js';
import Pagination from "../../../service/Pagination.js";
import { setActiveSelectPage } from './actions/OrganisationAction.js'

const requestData = (pageSize, page, sorted, filtered,orgId) => {
    return new Promise((resolve, reject) => {
        var Request = JSON.stringify({pageSize: pageSize, page: page, sorted: sorted, filtered: filtered,orgId: orgId});
        postData('organisation/OrgDashboard/get_org_sites', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            resolve(res);
        });

    });
};

class OrganisationSites extends React.Component {
    constructor(props) {
    super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        var all=[]  ;
        this.state = {
            sites_list:[],
            view_by_status:0,
            modal_show:false,
            search_value:'',
            isSiteModalShow:false
        };
    this.reactTable = React.createRef();
    }

    componentDidMount() {   
        this.props.setActiveSelectPage('org_sites_listing');
        this.props.changeNavigationClass('orgSiteActive');  //call redux for active class     
    }
    handleHidePopup(res){
        this.setState({isSiteModalShow:false},()=>{
            if(res!=undefined){
                reFreashReactTable (this,'fetchData');
            }
        });
    }

    fetchData = (state, instance) => {
        this.setState({loading: true});
        requestData(
                state.pageSize,
                state.page,
                state.sorted,
                state.filtered,
                this.props.props.match.params.id
                ).then(res => {
            this.setState({
                sites_list: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }

    archived(id){ 
        var org_id = this.props.props.match.params.id;
        archiveALL({id:id,org_id:org_id},'','organisation/OrgDashboard/archive_organisation_site').then((result) => {
            if(result.status){
                var requestData = {archive:'0', srch_box:this.state.search_value};
                this.setState({filtered:requestData});
            }
        })
    }

    viewByselectChange(selectedOption, fieldname) { 
       var requestData = {archive:selectedOption,srch_box:this.state.search_value};
       this.setState({filtered:requestData,view_by_status:selectedOption});       
    }  

    closeEditPopUp=(param)=>
    {
        if(param)
        reFreashReactTable (this,'fetchData');

        this.setState({modal_show:false});
    }

    searchData =(e)=>
    {
        e.preventDefault();
        var srch_ary = {
            'srch_box':this.state.search_value,
            'archive':this.state.view_by_status,
        }
        this.setState({filtered: srch_ary}, function () {  });
    } 

    closeModal=()=>
    {
        this.setState({modal_show:false});
        reFreashReactTable (this,'fetchData');
    }

    render() {  
        const columns = [
        {Header: '', accessor: 'id', filterable: false,Cell: (props) => <span>{this.state.view_by_status==1 ? props.original.title : <a className="color-initial" href={'/admin/organisation/house_about/'+props.original.ocs_id+'/'+this.props.props.match.params.id}>{props.original.title}</a>}</span>, Header: <span></span>} ,
            {Cell: (props) => <span></span>, Header: <span></span>, style: {
                    "textAlign": "right", 
                  }, headerStyle: {border:"0px solid #fff" }, headerClassName: 'margin_right_side', sortable: false}, 
                   {expander : true, sortable: false,
                       Expander: ({ isExpanded, ...rest }) => 
                       <div>{isExpanded? <i className="icon icon-arrow-up"></i> : <i className="icon icon-arrow-down"></i>}</div>,
                    headerStyle: {border:"0px solid #fff" },
            }
        ]

     return (
            <div>             
                <div className="row">                
               
                    <div className="tab-content col-sm-12">                       
                        <div role="tabpanel" className="tab-pane active" id="siteList">
                            <div className="row">
                               <div className="col-lg-8 col-sm-9 col-lg-offset-1">
                                <div className="bor_T"></div>
                                    <div className="P_7_TB"><h3>Sub-Orgs Belonging to '{this.props.OrganisationProfile.name}':</h3></div>
                                    <div className="bor_T"></div>
                                </div>

                                <div className="col-lg-2 col-sm-3">
                                    <div className="box">
                                         <Select name="view_by_status" required={true} simpleValue={true} searchable={false} clearable={false} options={siteViewBy()} placeholder="View By" value={this.state.view_by_status} onChange={(e)=> this.viewByselectChange(e,'view_by_status')}/>
                                    </div>
                                </div>
                            </div>

                            <div className="">
                            <div className="row _Common_Search_a">
                                <form className="col-lg-8 col-lg-offset-1 col-sm-9" method='get' onSubmit={(e)=>this.searchData(e)} autoComplete="off">
                                <div className="row d-flex flex-wrap align-items-center">
                                    <div className="col-lg-6  col-sm-6">
                                    <div className="search_bar">
                                        <div className="input_search">
                                          <input type="text" className="form-control" placeholder="Search" name="search_value" value={this.state.search_value} onChange={(e)=>handleChangeChkboxInput(this,e)}/> 
                                            <button type="submit"><span className="icon icon-search"></span></button>
                                        </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 col-sm-5">
                                        <div className="row d-flex flex-wrap align-items-center">
                                             <div className="col-sm-4 text-right">Filter By:</div>
                                             <div className="col-sm-5">
                                             <Select required={true} simpleValue={true} searchable={false} clearable={false} placeholder="All"/>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </form>
                             
                                <div className="col-lg-2 col-sm-3">
                                         <a className="but" onClick={()=>{this.setState({isSiteModalShow:true})}}>
                                          Attach Site to Org
                                        </a>                                       
                                </div>
                                </div>
                                <AttachSites 
                                    isSiteModalShow={this.state.isSiteModalShow} 
                                    handleHide={(e)=>this.handleHidePopup(e)} 
                                    orgDetails={this.props.props.match.params.id}
                                />                             
                            
                            </div>

                            <div className="row P_15_TB">
                                <div className="col-lg-10 col-lg-offset-1 col-sm-12">
                                   
                                    <div className="schedule_listings header_none_react_table PL_site">
                                        <ReactTable
                                         PaginationComponent={Pagination}
                                            columns={columns}
                                            manual 
                                            data={this.state.sites_list}
                                            pages={this.state.pages}
                                            loading={this.state.loading} 
                                            onFetchData={this.fetchData} 
                                            filtered={this.state.filtered}
                                            defaultFiltered ={{archive: 0,srch_box:''}}
                                            defaultPageSize={10}
                                            className="-striped -highlight"  
                                            noDataText="No Record Found"
                                            minRows={2}
                                            ref={this.reactTable}
                                            previousText={<span className="icon icon-arrow-left privious"></span>}
                                            nextText={<span className="icon icon-arrow-right next"></span>}
                                            SubComponent={(props) => <div className="other_conter">
                                            <div className="col-sm-10">
                                            <div className="my-0">
                                                <span className="color">Address: </span> {props.original.address}
                                            </div>
                                            <div className="my-0">
                                                <span className="color">Key Contact: </span>{props.original.hasOwnProperty('siteDetails') && props.original.siteDetails[0].hasOwnProperty('key_contact')? props.original.siteDetails[0].key_contact :''}
                                            </div>
                                            <div className="my-0">
                                                <span className="color">Phone: </span> 
                                                { props.original.hasOwnProperty('SitePhone') && props.original.SitePhone != null && props.original.SitePhone.length>0 ? props.original.SitePhone.map((value, idx) => (
                                                    <span><span className="color">{value.primary_phone == '1' ? '(Primary) ' : '(Secondary) '}</span><span key={idx+2}>{value.phone} <i className="icon icon-pending-icons color"></i></span><br/></span>
                                                )):<React.Fragment />}
                                            </div>
                                            <div className="my-0">
                                                <span className="color">Email: </span> 
                                                {props.original.hasOwnProperty('SiteEmail') && props.original.SiteEmail != null && props.original.SiteEmail.length>0 ? props.original.SiteEmail.map((value, idx) => (
                                                    <span><span className="color">{value.primary_email == '1' ? '(Primary) ' : '(Secondary) '}</span> <span key={idx+5}>{value.email}</span><br/></span>
                                                )): <React.Fragment />}
                                                
                                            </div>
                                                
                                            </div>
                                            <div className="col-sm-2 text-right mt-5">
                                                {
                                                    (this.state.view_by_status ==0)?
                                                    <span>
                                                        {
                                                            
                                                        /*<a onClick={()=>this.setState({modal_show:true,selectedData:props.original,formUrl:'organisation/OrgDashboard/add_org_site',pageTitle:'Site update',mode:'edit'})}><i className="icon icon-update update_button mr-2"></i></a>*/}

                                                        <a onClick={()=>this.setState({modal_show:true,selectedData:props.original,pageTitle:'Site update'})}><i className="icon icon-update update_button mr-2"></i></a>

                                                        <a onClick={()=> this.archived(props.original.ocs_id)}><i className="icon icon-email-pending archive_Icon"></i></a>
                                                </span>
                                                :''}
                                            </div>
                                            </div>}
                                            showPagination={this.state.sites_list.length > PAGINATION_SHOW ? true : false }
                                        />

                                        {/*<OrganisationAddressUpdatePopUp 
                                            modal_show={this.state.modal_show} 
                                            formUrl={this.state.formUrl} 
                                            selectedData={this.state.selectedData} 
                                            closeEditPopUp={this.closeEditPopUp} 
                                            pageTitle={this.state.pageTitle}
                                            mode={this.state.mode}
                                            orgId={this.props.props.match.params.id}
                                        />*/}

                                        {(this.state.modal_show)?
                                        <OrganisationAddSitePopUp 
                                        modal_show={this.state.modal_show} 
                                        closeModal={this.closeModal} 
                                        callType="0"
                                        orgId={this.props.props.match.params.id}
                                        selectedData={this.state.selectedData} 
                                        />  
                                        :''}    
                                    </div>
                                </div>
                                {this.state.view_by_status ==1 ? <React.Fragment /> :
                                    <div className="col-lg-2 col-lg-offset-9 col-sm-3 col-sm-offset-9 py-2">
                                    <a className="Plus_button" onClick={()=>this.setState({modal_show:true,selectedData:{}})}>
                                        <i className="icon icon-add-icons create_add_but"></i>
                                        <span>Create New Site</span>
                                    </a>                                       
                                    </div> 
                                }                  
                            </div>
                        </div>
                                               
                    </div>
                </div>
                </div>            
          
        );
    }
}


const mapStateToProps = state => ({
    ActiveClass : state.OrganisationReducer.ActiveClassProfilePage,
    OrganisationProfile : state.OrganisationReducer.orgProfile
})

 const mapDispatchtoProps = (dispach) => {
       return {
           changeNavigationClass: (value) => dispach(setOrgActiveClassProfilePage(value)),
           setActiveSelectPage: (value) => dispach(setActiveSelectPage(value)),
      }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(OrganisationSites)