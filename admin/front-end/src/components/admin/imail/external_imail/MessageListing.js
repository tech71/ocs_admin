import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import moment from 'moment';
import { getExternalMailContent } from '../actions/ExternalImailAction';

import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";

import { customSingleChatingLoader } from '../../../../service/CustomContentLoader.js';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';

class MessageListing extends React.Component {
    constructor(props) {
        super(props);
        this.active = this.props.active;
        this.state = {
            activeMsg: null
        }
    }

    setAllContentData = (messageId, i) => {
        this.props.getExternalMailContent({ messageId: messageId, type: this.props.mail_type });
        this.setState({activeMsg: i})
    }

    render() {
        return (
            <div className="Internal_M_D">
                <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customSingleChatingLoader()} ready={!this.props.is_fetching}>
                    <div className="custom_scolling">

                        {this.props.mail_listing.length > 0 ?
                            this.props.mail_listing.map((val, index) => (
                                <div key={index + 1} className={"mess_V1_1 " + ((val.is_block == 1) ? 'block_MSG ' : '') + (this.state.activeMsg == index ? 'active ' : '')}>
                                    <div className="mess_v1">
                                        <div className="mess_vn_1">
                                            <Link onClick={() => this.setAllContentData(val.id, index)} to={"/admin/imail/external/" + this.props.mail_type + '/' + val.id}>
                                                <div className="mess_vn_in_1">
                                                    <div className="mess_V_1">
                                                        <div className="nav_apps text-left px-0 py-0">
                                                            <div className="ml-0 mb-0"><span className={"add_access " + (val.sender_type) + "-colr"}>{val.sender_type.toUpperCase()}</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="mess_V_2">
                                                        <div className="mess_V_a">
                                                            <div className="mess_V_a1">From: {val.user_name} </div>
                                                            <div className="mess_V_a2">{val.title}</div>
                                                        </div>
                                                        <div className="mess_V_b">{moment(val.mail_date).format('DD/MM/YYYY LT')}</div>
                                                        <div className="mess_vn_2">
                                                            {(val.is_block == 1) ? <OverlayTrigger placement="bottom" overlay={<Tooltip id='is_block' className="MSG_Tooltip" placement={'top'}>Blocked</Tooltip>}><i className="icon icon-block-im attach_im color"></i></OverlayTrigger> : ''}
                                                            {(val.is_flage == 1) ? <OverlayTrigger placement="bottom" overlay={<Tooltip id='is_flage' className="MSG_Tooltip" placement={'top'}>Favorite</Tooltip>}><i className="icon icon-flag-im flag-im color"></i></OverlayTrigger> : ''}
                                                            {(val.have_attachment == 1) ? <OverlayTrigger placement="bottom" overlay={<Tooltip id='have_attachment' className="MSG_Tooltip" placement={'top'}>Attach File</Tooltip>}><i className="icon icon-attach-im attach_im color"></i></OverlayTrigger> : ''}
                                                            {(val.is_priority == 1) ? <OverlayTrigger placement="bottom" overlay={<Tooltip id='is_priority' className="MSG_Tooltip" placement={'top'}>High Priority</Tooltip>}><i className="icon icon-warning-im warning-im color"></i></OverlayTrigger> : ''}
                                                        </div>
                                                    </div>
                                                </div>
                                            </Link>

                                            <div className="mess_vn_in_2">
                                                <p className="mb-0 mt-2"></p>
                                                <p className="my-0 ML_show_fixed">{val.content}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )) : <h1 className="not_msg_v1">No Result</h1>}
                    </div> </ReactPlaceholder>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    mail_listing: state.ExternalImailReducer.mail_listing,
    mail_type: state.ExternalImailReducer.mail_type,
    is_fetching: state.ExternalImailReducer.is_fetching,
})

const mapDispatchtoProps = (dispach) => {
    return {
        getExternalMailContent: (value) => dispach(getExternalMailContent(value)),
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(MessageListing)   