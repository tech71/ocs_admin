import React from 'react';
import { connect } from 'react-redux'
import {PanelGroup, Panel} from 'react-bootstrap';

import { postData} from '../../../../service/common.js';
import CreateUpdateTeam from './CreateUpdateTeam';
import {  Link } from 'react-router-dom';
import { composeNewMailStatus} from '../actions/InternalImailAction';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { groupChating } from '../../../../service/CustomContentLoader.js';


class GroupDepartmentListing extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            activeKey: '2',
            teamList: [],
            department: [],
        };
    }
    
    handleSelect = (activeKey) => {
        this.setState({activeKey: activeKey });
    }
    
    stateValueChange = (key, value) => {
         var state = {}
         state[key] = value;
         this.setState(state);
    }
    
    componentDidMount() {
        this.setActivePanel(this.props.params.team_type);
        this.getTeamDetails();
        
    }
    
    setActivePanel = (team_type) => {
        var type = (team_type == 'department'? '1' : '2')
        this.setState({activeKey: type});
    }
    
    componentWillReceiveProps(newProps) {
        this.setActivePanel(newProps.params.team_type);
    }
    
    getTeamDetails = () => {
        this.setState({loading:true})
         postData('imail/Internal_imail/get_team_datails_and_department', this.state).then((result) => {
               if (result.status) {
                   this.setState(result.data)
               }
               this.setState({loading:false})
         });
    }

    render() {
        return (
            <div className="Internal_GroupMsg pt-3">
                <PanelGroup
                    accordion
                    id="accordion-controlled-example"
                    activeKey={this.state.activeKey}
                    onSelect={this.handleSelect}
                >
                    <Panel eventKey="1">
                        <Panel.Heading>
                            <Panel.Title toggle>
                                <span className="icon icon-arrow-down"></span>
                                <span className="Group_head_v1">Departments</span></Panel.Title>
                        </Panel.Heading>
                        <Panel.Body collapsible>
                            <div className="GroupMSG_team_v1">
                                {this.state.department.map((val,index) => (
                                      <Link onClick={() => this.props.composeNewMailStatus(false)} key={index+1} to={"/admin/imail/internal/group_message/department/"+val.id}><div className={"GroupMSG_team " +((this.props.params.id == val.id && this.props.params.team_type == 'department')? 'active': '')}>{val.name} 
                                      {this.props.department_count[val.id] > 0?<span className="mess_V1_noti">{this.props.department_count[val.id]}</span>: ''}</div></Link>                        
                                ))}
                            </div>
                        </Panel.Body>
                    </Panel>

                    <Panel eventKey="2">
                        <Panel.Heading>
                            <Panel.Title toggle>
                                <span className="icon icon-arrow-down"></span>
                                <span className="Group_head_v1">My Team</span></Panel.Title>
                        </Panel.Heading>
                        <Panel.Body collapsible>
                        <ReactPlaceholder showLoadingAnimation ={true}  customPlaceholder={ groupChating()} ready={!this.state.loading}>
                            <div className="GroupMSG_team_v1">
                            {this.state.teamList.length > 0 ?
                                this.state.teamList.map((team, index) => ( <div key={index+1} className="d-flex">
                                      <Link onClick={() => this.props.composeNewMailStatus(false)} className="w-85" to={"/admin/imail/internal/group_message/team/"+team.id}>
                                        <div className={"GroupMSG_team w-100 mr-2 "+((this.props.params.id == team.id && this.props.params.team_type == 'team')? 'active': '')}>{team.team_name}
                                        {this.props.team_count[team.id] > 0?<span className="mess_V1_noti">{this.props.team_count[team.id]}</span>: ''}
                                        </div>
                                      </Link>
                                      {team.update_permission == 1?<i onClick={() => this.setState({updateData: team, openModalTeam: true, mode: 'update'})} className="icon icon-update update_team mt-2 ml-3"></i>: ''}        
                                     </div> 
                                )):<div className="GroupMSG_team_v1"><div className="GroupMSG_team w-100 mr-2">No Record (Create your team)</div></div>}
                            </div>
                            </ReactPlaceholder>
                            <div className="col-md-12 text-right">
                                <div className="button_plus__">
                                    <i onClick={() => this.setState({openModalTeam: true, mode: 'new', updateData: false})} className="icon icon-add-icons Add-2-1"></i>
                                </div>
                            </div>
                        </Panel.Body>
                    </Panel>
                </PanelGroup>
                
                <CreateUpdateTeam openModalTeam={this.state.openModalTeam} stateValueChange={this.stateValueChange} 
                   getTeamDetails={this.getTeamDetails} updateData={this.state.updateData} mode={this.state.mode}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    socketObj: state.Permission.socketObj,
    team_count: state.NotificationReducer.team_count,
    department_count: state.NotificationReducer.department_count,
})

const mapDispatchtoProps = (dispatch) => {
    return {
        composeNewMailStatus: (status) => dispatch(composeNewMailStatus(status)),
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(GroupDepartmentListing)    