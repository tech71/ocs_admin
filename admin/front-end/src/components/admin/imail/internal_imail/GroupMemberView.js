import React, { Component } from 'react';

import Modal from 'react-bootstrap/lib/Modal';
import { connect } from 'react-redux'


class CreateUpdateTeam extends Component {
    
    render() {
        return (
                <Modal className="modal fade-scale Modal_A Gold  Modal_B bg_grey" show={this.props.openModel} onHide={this.handleHide} container={this} 
                aria-labelledby="myModalLabel" id="modal_1" tabIndex="-1" role="dialog" >
                
                <Modal.Body>
                    <div className="dis_cell">
                        <div className="text text-left by-1">Team Members:
                            <a data-dismiss="modal" aria-label="Close" className="close_i pull-right mt-1" ><i onClick={this.props.closeModel} className="icon icon-cross-icons"></i></a>
                        </div>
                       
                        <div className="row P_15_TB by-1">
                            <div className="col-md-5">
                            <label></label>
                                {this.props.groupMember.map((val, index) => (
                                    <div key={index+1} className="team_members_div">
                                        <span className="t_m_d_1"><img src={'/assets/images/admin/chats_2.png'} /></span>
                                        <span  className="t_m_d_2">{val.adminName}</span>
                                    </div>
                                ))}
                            </div>
                        </div>      
                    </div>  
                </Modal.Body>
            </Modal>
          );
    }
}

const mapStateToProps = state => ({

})

const mapDispatchtoProps = (dispatch) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(CreateUpdateTeam) 