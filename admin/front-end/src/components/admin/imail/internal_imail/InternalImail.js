import React from 'react';
import {  Link } from 'react-router-dom';


import Select from 'react-select-plus';
import InternalDetails from './InternalDetails';
import MessageListing from './MessageListing';
import InternalNavigation from './InternalNavigation';
import ComposeNewMessage from './ComposeNewMessage';
import {NoMessageSelected} from './NoMessageSelected';
import GroupDepartmentListing from './GroupDepartmentListing';
import Chating from './Chating';
import {imailActiveTitle} from 'menujson/imail_menu_json';
import { checkItsNotLoggedIn, postData, handleChange} from '../../../../service/common.js';
import { connect } from 'react-redux'
import { getInternalMailListing, setInternalMailType, composeNewMailStatus} from '../actions/InternalImailAction';
import {setUnreadGroupMessage_count} from './../../notification/actions/NotificationAction';
import { externalFilterOption} from '../../../../dropdown/imailDropdown.js';


class InternalImail extends React.Component {
    constructor(props) {
        super(props);

        checkItsNotLoggedIn();

        this.state = {
            externalMessage: [],
            composeNewMail: false,
            singleChataData: [],
            filtered: {search: '', start_date: '', end_date: ''}
        }
    }
    
     setTitle = (type) => {
        var x =  imailActiveTitle[type];
        this.setState({activeTitle : x});
    }
    
    getMessageCount = () => {
          postData('imail/Internal_imail/get_message_count', {}).then((result) => {
               if (result.status) {
                    this.props.setMessageCount(result.data);
               }
         });
    }

    submitSearch = (e) => {
        e.preventDefault();
        // reflect value of search box
        this.getInternalMail(this.state.type);
    }
    
    onChnageFilter = (key, value) => {
        var state = {}
        state[key] = value;
        this.setState(state, () => {
            this.getInternalMail(this.state.type);
        });
    }

    componentDidMount() {
        this.getMessageCount();
        var type = this.props.props.match.params.type;
        this.props.setInternalMailType(type);
        this.setTitle(type);
        this.setState({type: type}, () => {
            this.getInternalMail(type);
        })
    }

    componentWillReceiveProps(newProps) {
        var type = newProps.props.match.params.type;

        if (type != this.state.type){
            this.props.setInternalMailType(type);
            this.setTitle(type);
            this.setState({type: type}, () => {
                this.getInternalMail(type);
            });
          
        }
    }

    getInternalMail = (type) => {
        if(type == 'inbox' || type == 'archive' || type == 'draft'){
            this.setState({loading: true});
            var request = {type: type, select: this.state.select, search_box: this.state.search};
            const {dispatch} = this.props
            this.props.setInternalMailListing(request);
        }
    }

    composeMail = () => {
        this.props.composeNewMailStatus(true);
    }

    closeCompose = () => {
        this.props.composeNewMailStatus(false);
    }
    
    returnRequiredView =() => {
        if(this.props.create_mail){
            return <ComposeNewMessage closeCompose={this.closeCompose} compose_title={'New Message'} submitUri={'imail/Internal_imail/compose_new_mail'} />
        }else if(this.props.props.match.params.id && (this.state.type == 'inbox' || this.state.type == 'draft' || this.state.type == 'archive')){
           
            return <InternalDetails mail_type={this.props.props.match.params.type} messageId={this.props.props.match.params.id} re={this.props.props.match.params.re} contentId={this.props.props.match.params.contentId}/>
            
        }else if(this.props.props.match.params.id && (this.state.type == 'group_message')){
            return <Chating teamId ={this.props.props.match.params.id} team_type={this.props.props.match.params.team_type} />;
        }else{
            return <NoMessageSelected/>
        }                                     
    }

    render() {
        
        return (
                <section className="manage_top Gold">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-10 col-lg-offset-1 col-md-12 P_15_TB">
                                <Link to={'/admin/imail/dashboard'}><span className="icon icon-back-arrow back_arrow"></span></Link>
                            </div>
                            <div className="col-lg-1"></div>
                            <div className="col-lg-10 col-lg-offset-1 col-md-12">
                                <div className="bor_T"></div>
                            </div>
                        </div>
                
                        <div className="row">
                            <div className="col-lg-8 col-lg-offset-1 col-md-9 P_25_TB">
                                <h1 className="my-0 color">{this.state.activeTitle}</h1>
                            </div>
                            <div className="col-lg-2 col-md-3 P_25_TB">
                                <a className="Plus_button" onClick={this.composeMail} ><i className="icon icon-add-icons create_add_but"></i><span>Compose New Message</span></a>
                            </div>
                            <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div>
                        </div>
                
                
                        <div className="row">
                            <div className="col-lg-1"></div>
                
                
                            <article className="col-lg-10 col-md-12">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="row">
                                            <form  id="srch_feedback" autoComplete="off" onSubmit={this.searchData} method="post">
                                                <div className="col-md-6">
                                                <div className="search_bar">
                                                    <div className="input_search P_25_TB">
                                                        <input value={this.state.search} onChange={(e) => handleChange(this, e)} type="text" className="form-control" placeholder="Search" name="search" />
                                                        <button onClick={this.submitSearch} type="submit"><span className="icon icon-search"></span></button>
                                                    </div>
                                                    </div>
                                                </div>
                
                                                <div className="col-md-3">
                                                    <div className="box P_30_TB font_big">
                                                        <Select onChange={(e) => this.onChnageFilter('select', e)} value={this.state.select} options={externalFilterOption()} simpleValue={true} searchable={false} clearable={false} placeholder="Filter by: Select" />
                                                    </div>
                                                </div>
                                                <div className="col-md-3">
                                                    <div className="box P_30_TB font_big">
                                                        <Select required={true} simpleValue={true} searchable={false} Clearable={false} placeholder="Sort by: Date" />
                                                    </div>
                                                </div>
                
                                                <div className="col-md-12">
                                                    <div className="bor_T"></div>
                                                </div>
                                            </form>
                                        </div>
                
                                        <div className="row mt-5">
                                            <div className="col-md-4">
                                                {(this.props.props.match.params.type === 'group_message') ?
                                                    <GroupDepartmentListing params={this.props.props.match.params} />
                                                     :
                                                    <MessageListing />
                                                }
                
                                            </div>
                
                                            <div className="col-md-8">
                                                {this.returnRequiredView()}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                
                        </div>
                    </div>
                </section>
                );
    }
}

const mapStateToProps = state => ({
        mail_type: state.InternalImailReducer.mail_type,
        create_mail: state.InternalImailReducer.create_mail,
    })

const mapDispatchtoProps = (dispatch) => {
    return {
        setInternalMailListing: (request) => dispatch(getInternalMailListing(request)),
        setInternalMailType: (mail_type) => dispatch(setInternalMailType(mail_type)),
        composeNewMailStatus: (status) => dispatch(composeNewMailStatus(status)),
        setMessageCount: (obj) => dispatch(setUnreadGroupMessage_count(obj)),
    }
}



export default connect(mapStateToProps, mapDispatchtoProps)(InternalImail)    