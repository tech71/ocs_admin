import React, { Component } from 'react';
import { connect } from 'react-redux'
import { checkItsNotLoggedIn} from '../../../service/common.js';
import ParticipantNavigation from '../../admin/participant/ParticipantNavigation';
import ParticipantProfile from '../../admin/participant/ParticipantProfile';

class ParticipantFounds extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();
       
        this.state = {
            loading: true,
            success: false
        }

    }

    componentDidMount() {
        this.setState({loading: true});
    }
    
    selectChange(selectedOption, fieldname) {
        var state = {};
        state[fieldname] = selectedOption;
        this.setState(state);
    }
    
    render() {
        return (
            <React.Fragment>
               
                <div className="row">
                <ParticipantProfile id={this.props.props.match.params.id} pageTypeParms={this.props.props.match.params.page}/>
                    <ParticipantNavigation active="funds" activeClass={'funding'} />
                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                  
                        <div className="tab-content" >
                        <div role="tabpanel" className={this.props.showTypePage=='totals' ? "tab-pane active" : "tab-pane"} id="Barry_details">
                            <div className="bt-1"></div>
                            <h1>Coming Soon</h1>
                        </div>
                        <div role="tabpanel" className={this.props.showTypePage=='analysis' ? "tab-pane active" : "tab-pane"} id="Barry_availabitity">
                            <div className="bt-1"></div>
                            <h1>Coming Soon</h1>
                        </div>
                    </div>
                        
                    </div>

                </div>

          
                </React.Fragment>
                );
    }
}

const mapStateToProps = state => ({
     firstname: state.ParticipantReducer.participantProfile.firstname,
     fullName: state.ParticipantReducer.participantProfile.fullName,
     showTypePage:state.ParticipantReducer.activePage.pageType
})

export default connect(mapStateToProps)(ParticipantFounds)
