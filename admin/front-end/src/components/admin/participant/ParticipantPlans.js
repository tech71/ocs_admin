import React, { Component } from 'react';
import ParticipantProfile from '../../admin/participant/ParticipantProfile';
import { checkItsNotLoggedIn, postData } from '../../../service/common.js';
import ParticipantNavigation from '../../admin/participant/ParticipantNavigation';
import { connect } from 'react-redux'


class ParticipantPlans extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();
        
        this.bookingList = [{value:'Plan',label:'Plan(s)'},{value:'Goals',label:'Goals'},{value:'Documents',label:'Documents'},{value:'Funding_Tracker',label:'Funding Tracker'}];
        
        this.initialState = {
            loading: true,
            success: false
        }

      
        this.state = this.initialState;
        this.selectChange = this.selectChange.bind(this);

    }

    componentDidMount() {
        this.setState({loading: true});
        
        postData('participant/Participant_profile/get_participant_plans', {id:this.props.props.match.params.id}).then((result) => {
            if (result.status) {
                var details = result.data
                this.setState({name: details.name});
                this.setState({ocsID: details.oc_id});         
            } 
            this.setState({loading: false});
        });
    }
    
    selectChange(selectedOption, fieldname) {
        var state = {};
        state[fieldname] = selectedOption;
        this.setState(state);
    }
    
    render() {
        return (
                <div>
    <ParticipantProfile id={this.props.props.match.params.id} pageTypeParms={this.props.props.match.params.page}/>

        <div className="row">
            <ParticipantNavigation  active="plans" activeClass={'funding'} />
            <div className="col-lg-10 col-sm-12 col-lg-offset-1">

             
                <div className="tab-content">
                    <div role="tabpanel" className={this.props.showTypePage=='current_plan' ? "tab-pane active" : "tab-pane"} id="Barry_details">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="row">
                                    <div className="col-md-12"><h3 className="P_7_TB by-1">Current Plan(s){/* '{this.props.firstname}' Plans: */}</h3></div>
                                   
                                </div>
                            </div>
                        </div>


                        <div className="row P_15_TB">
                            <div className="panel-group Nds_plan" id="accordion" role="tablist" aria-multiselectable="true">
                                <div className="col-lg-6 col-sm-6">
                                    <div className="panel panel-default">
                                        <div className="panel-heading" role="tab" id="headingOne">
                                            <h4 className="panel-title">
                                                <span role="button" data-toggle="collapse" data-parent="#accordion" href="#demo_1" aria-expanded="true" aria-controls="demo_1">
                                                    <i className="icon icon-arrow-down"></i>
                                                    <div className="Plan_div_">
                                                            <div className="Plan_heading_"><span>Plan Type :</span></div>
                                                            <div className="Plan_heading_details_">NDIS - Self Managed </div>
                                                    </div>
                                                </span>
                                            </h4>
                                        </div>
                                        <div id="demo_1" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                            <div className="panel-body">
                                                    <div className="Plan_div_">
                                                        <div className="Plan_heading_">NDIS # :</div>
                                                        <div className="Plan_heading_details_">93450</div>
                                                    </div>
                                                    <div className="Plan_div_ by-1">
                                                        <div className="Plan_heading_">Plan-ID # :</div>
                                                        <div className="Plan_heading_details_">862991</div>
                                                    </div>
                                                    <div className="Plan_div_ ">
                                                        <div className="Plan_heading_">Start Date :</div>
                                                        <div className="Plan_heading_details_">19/12/2019</div>
                                                    </div>
                                                    <div className="Plan_div_ bb-1">
                                                        <div className="Plan_heading_">End Date :</div>
                                                        <div className="Plan_heading_details_">30/12/2019</div>
                                                    </div>
                                                    <div className="Plan_div_">
                                                        <div className="Plan_heading_">Total Funds :</div>
                                                        <div className="Plan_heading_details_">$300 000.00</div>
                                                    </div>
                                                    <div className="Plan_div_">
                                                        <div className="Plan_heading_">Funds Used :</div>
                                                        <div className="Plan_heading_details_">$94 282.31</div>
                                                    </div>
                                                    <div className="Plan_div_">
                                                        <div className="Plan_heading_">Remaining :</div>
                                                        <div className="Plan_heading_details_">$205 717.69 </div>
                                                    </div>
                                                    <div className="Plan_div_ by-1">
                                                        <div className="Plan_heading_">Plan site(s) :</div>
                                                        <div className="Plan_heading_details_">
                                                            <div className="Plan_div_site_">
                                                                <span>19 Mc Donald Way North Melbourne, VIC 3001 </span>
                                                                <i className="icon icon-plus"></i>
                                                            </div>
                                                            </div>
                                                    </div>
                                                </div>

                                                <div className="Plan_div_">
                                                        <div className="d-flex  justify-content-end w-100 Plan_icon_div_">
                                                        {/* <a className="d-inline-flex" href="#"><i className="icon icon-email-pending font_ic_1"></i></a> */}
                                                        <a className="d-inline-flex" href="#"><i className="icon icon-invoice font_ic_2"></i></a>
                                                        </div>
                                                    </div>
                                              

                                           
                                        </div>

                                    </div>
                                </div>



                                <div className="col-lg-6 col-sm-6">
                                    <div className="panel panel-default">
                                        <div className="panel-heading" role="tab" id="headingOne">
                                            <h4 className="panel-title">
                                                <span role="button" data-toggle="collapse" data-parent="#accordion" href="#demo_2" aria-expanded="true" aria-controls="demo_2">
                                                    <i className="icon icon-arrow-down"></i>
                                                    <div className="Plan_div_">
                                                            <div className="Plan_heading_"><span>Plan Type :</span></div>
                                                            <div className="Plan_heading_details_">Private </div>
                                                        </div>
                                                </span>
                                            </h4>
                                        </div>
                                        <div id="demo_2" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                        <div className="panel-body">
                                                    <div className="Plan_div_">
                                                        <div className="Plan_heading_">NDIS # :</div>
                                                        <div className="Plan_heading_details_">93450</div>
                                                    </div>
                                                    <div className="Plan_div_ by-1">
                                                        <div className="Plan_heading_">Plan-ID # :</div>
                                                        <div className="Plan_heading_details_">862991</div>
                                                    </div>
                                                    <div className="Plan_div_ ">
                                                        <div className="Plan_heading_">Start Date :</div>
                                                        <div className="Plan_heading_details_">19/12/2019</div>
                                                    </div>
                                                    <div className="Plan_div_ bb-1">
                                                        <div className="Plan_heading_">End Date :</div>
                                                        <div className="Plan_heading_details_">30/12/2019</div>
                                                    </div>
                                                    <div className="Plan_div_">
                                                        <div className="Plan_heading_">Total Funds :</div>
                                                        <div className="Plan_heading_details_">$300 000.00</div>
                                                    </div>
                                                    <div className="Plan_div_">
                                                        <div className="Plan_heading_">Funds Used :</div>
                                                        <div className="Plan_heading_details_">$94 282.31</div>
                                                    </div>
                                                    <div className="Plan_div_">
                                                        <div className="Plan_heading_">Remaining :</div>
                                                        <div className="Plan_heading_details_">$205 717.69 </div>
                                                    </div>
                                                    <div className="Plan_div_ by-1">
                                                        <div className="Plan_heading_">Plan site(s) :</div>
                                                        <div className="Plan_heading_details_">
                                                            <div className="Plan_div_site_">
                                                                <span>19 Mc Donald Way North Melbourne, VIC 3001 </span>
                                                                <i className="icon icon-plus"></i>
                                                            </div>
                                                            </div>
                                                    </div>
                                                </div>

                                                <div className="Plan_div_">
                                                        <div className="d-flex  justify-content-end w-100 Plan_icon_div_">
                                                        {/* <a className="d-inline-flex" href="#"><i className="icon icon-email-pending font_ic_1"></i></a> */}
                                                        <a className="d-inline-flex" href="#"><i className="icon icon-invoice font_ic_2"></i></a>
                                                        </div>
                                                    </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>

                    </div>

                    <div role="tabpanel" className={this.props.showTypePage=='plan_history' ? "tab-pane active" : "tab-pane"} id="Barry_preferences">
                    <div className="bt-1"></div>
                    <h1>Coming Soon</h1>
                    </div>

                </div>
            </div>
        </div>
</div>
                );
    }
}

const mapStateToProps = state => ({
     firstname: state.ParticipantReducer.participantProfile.firstname,
     fullName: state.ParticipantReducer.participantProfile.fullName,
     showTypePage:state.ParticipantReducer.activePage.pageType
})

export default connect(mapStateToProps)(ParticipantPlans)
