import React, { Component } from 'react';

import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';

import { listViewSitesOption, sitCategoryListDropdown } from '../../../dropdown/ParticipantDropdown.js';
import { checkItsNotLoggedIn, postData, archiveALL } from '../../../service/common.js';
import { connect } from 'react-redux'
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { customHeading, customNavigation,custNumberLine } from '../../../service/CustomContentLoader.js';
import ParticipantNavigation from '../../admin/participant/ParticipantNavigation';
import ParticipantProfile from '../../admin/participant/ParticipantProfile';

class ParticipantSites extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();

        this.state = {
            loading: true,
            success: false,
            address: [],
            view_by: 2
        }
    }
    
    archiveAddress(index, id){
        archiveALL({id: id},'', 'participant/Participant_profile/archive_participant_address').then((result) => {
            if(result.status){
                var state = {}
                state['address'] = this.state.address.filter((s, sidx) => index !== sidx);
                this.setState(state);
            }
        })
    }
    
    getParticipantSite = (view_by) => {
        this.setState({loading: true})
        postData('participant/Participant_profile/get_participant_sites', {id: this.props.props.match.params.id, view_by: view_by}).then((result) => {
            if (result.status) {
                var details = result.data
                this.setState({address: details});
            }
            this.setState({loading: false});
        });
    }

    componentDidMount() {
        this.setState({loading: true});
        this.getParticipantSite(this.state.view_by);
    }

    selectChange = (selectedOption, fieldname) => {
        var state = {};
        state[fieldname] = selectedOption;
        this.getParticipantSite(selectedOption);
        this.setState(state);
    }

    render() {
        return (
            <React.Fragment>
                   
                <ParticipantProfile id={this.props.props.match.params.id} pageTypeParms={this.props.props.match.params.page} loading={this.state.loading} state={this.state} />

                        <div className="row">
                            <ReactPlaceholder defaultValue={''} showLoadingAnimation ={true}  customPlaceholder={ customNavigation()} ready={!this.props.loading}>
                            <ParticipantNavigation active="sites" activeClass={'about'} />
                            </ReactPlaceholder>
                            <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                            
                                <div className="row mt-3">
                                    <div className="col-lg-9 col-xs-8">
                                    <div className="row">
                                    <div className="col-md-12"><div className="bor_T"></div></div>
                                        <ReactPlaceholder defaultValue={''} showLoadingAnimation ={true}  customPlaceholder={ customHeading(20)} ready={!this.props.loading}>
                                            <div className="col-md-12 P_7_TB"><h3>{/* this.props.firstname */} Site(s)</h3></div>
                                            </ReactPlaceholder>
                                            <div className="col-md-12"><div className="bor_T"></div></div>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 col-xs-4">
                                    <ReactPlaceholder defaultValue={''} showLoadingAnimation ={true}  customPlaceholder={ customHeading(60)} ready={!this.props.loading}>
                                       <Select name="status" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.view_by || ''} onChange={(e) => this.selectChange(e, 'view_by')} 
                                                                options={listViewSitesOption()} placeholder="View by" />
                                                                </ReactPlaceholder>
                                    </div>
                                       
                                    <div className="col-md-12">
                                        <ReactPlaceholder defaultValue={''} type='textRow'  customPlaceholder={custNumberLine(3)} ready={!this.state.loading}>
                                        <table className="Quali_table even_odd">
                                            <tbody>
                                               {(this.state.address.length > 0)?
                                                this.state.address.map((obj, index) => (
                                                    <tr className={(obj%2 == 0)? 'even': 'odd' } key={obj+1}>
                                                        <td className="py-3 px-3"><span className="color">{obj.site_category > 0? sitCategoryListDropdown(obj.site_category)+':' : "N/A" } </span></td> 
                                                        <td className="py-3 px-3 br-0"> {obj.street}, {obj.city.label}, {obj.stateName}, {obj.postal}</td>
                                                        <td className="px-3 text-right">{(obj.primary_address == 1)? '' : <a><i onClick={() => this.archiveAddress(index ,obj.id)} className="icon icon-email-pending archive_button"></i></a> }</td>
                                                    </tr>
                                                )): <tr>
                                                        <td className="py-3 px- br-0 text-center" ><span className="color">No address</span></td> 
                                                    </tr>}
                                                
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td align="right" colSpan="3">
                                                        
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        </ReactPlaceholder>
                                    </div>
                                </div>

                                {/*<div className="row P_30_T">
                                    <div className="col-lg-9 col-md-8"></div>
                                    <div className="col-lg-3 col-md-4"><a className="but">'{this.props.firstname}' Do NOT Book List</a></div>
                                </div>
                                <div className="row P_15_T">
                                    <div className="col-lg-9 col-md-8"></div>
                                    <div className="col-lg-3 col-md-4"><a className="but">Update '{this.props.firstname}' Requirements</a></div>
                                </div>*/}
                            </div>
                        </div>
                        </React.Fragment>
                );
    }
}

const mapStateToProps = state => ({
     firstname: state.ParticipantReducer.participantProfile.firstname,
     fullName: state.ParticipantReducer.participantProfile.fullName
})

export default connect(mapStateToProps)(ParticipantSites)
