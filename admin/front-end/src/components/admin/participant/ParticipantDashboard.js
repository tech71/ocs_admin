import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { listParticipantDropdown } from '../../../dropdown/ParticipantDropdown.js';
import { checkItsNotLoggedIn, postData } from '../../../service/common.js';
import { Doughnut } from 'react-chartjs-2';
import Header from '../../../components/admin/Header';
import Footer from '../../../components/admin/Footer';
import ListParticipant from '../../../components/admin/participant/ListParticipant';
import { connect } from 'react-redux'
import {setSubmenuShow} from 'components/admin/actions/SidebarAction';
import { CounterShowOnBox } from 'service/CounterShowOnBox.js';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();
        this.state = {
            loading: false,
            searching: false,
            inactive: false,
            incomplete: false,
            testimonial: []
        };

    }

    getParticipantTestimonial = () => {
        postData('participant/ParticipantDashboard/get_testimonial', this.state).then((result) => {
            if (result.status) {
                this.setState({ testimonial: result.data });
            } else {
                this.setState({ error: result.error });
            }
        });
    }

    componentDidMount() {
        this.props.setSubmenuShow(0);
        this.getParticipantTestimonial();
    }

    submitSearch = (e) => {
        e.preventDefault();
        var srch_ary = { search: this.state.search, inactive: this.state.inactive, incomplete: this.state.incomplete, search_by: this.state.search_by }
        this.setState({ filtered: srch_ary });
    }

    searchData = (key, value) => {
        var srch_ary = { search: this.state.search, inactive: this.state.inactive, search_by: this.state.search_by };

        srch_ary[key] = value;
        this.setState(srch_ary);
        this.setState({ filtered: srch_ary });
    }

    render() {
        return (<React.Fragment>
         
                    <div className="row  _Common_back_a">
                        <div className="col-lg-10 col-lg-offset-1 col-md-12">
                        <Link className="d-inline-flex" to={'/admin/dashboard'}><div className="icon icon-back-arrow back_arrow"></div></Link></div>
                    </div>
                    <div className="row"><div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div></div>

                    <div className="row _Common_He_a">
                        <div className="col-lg-8 col-lg-offset-1 col-md-9">
                            <h1 className="my-0 color">Participants</h1>
                        </div>
                        <div className="col-lg-2 col-md-3">
                            <Link className="Plus_button" to={'/admin/participant/create'} >
                            <i className="icon icon-add-icons create_add_but"></i><span>Create New Participant</span></Link>
                        </div>
                    </div>

                    <div className="row"><div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div> </div>

                    <div className="row">
                        <div className="col-lg-1"></div>
                        <div className="col-lg-10">
                            {this.state.testimonial.map((val, index) => (
                                <ul key={index + 1} className="landing_graph mt-5">

                                    <li className="radi_2 w-100 text-center">
                                        <h3 className="text-center  d-inline-block">{val.title}:</h3>
                                        <h3 className="d-inline-block color">"{val.testimonial}"</h3>
                                        <span className="name d-inline-block"><small>- {val.full_name}</small></span>
                                    </li>

                                </ul>
                            ))}
                        </div>
                        <div className="col-lg-1"></div>
                    </div>

                    <div className="row">
                        <div className="col-lg-10 col-lg-offset-1 col-md-12">
                            <ul className="landing_graph landing_graph_item_2 mt-5">
                                <NewParticipantGraph />
                                <NewParticipantCount />
                            </ul>
                        </div>
                    </div>

                    <React.Fragment>
                        <form onSubmit={this.submitSearch}>
                        <div className="row mt-5"><div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div></div>
                        <div className="row _Common_Search_a">
                            <div className="col-lg-6 col-lg-offset-1 col-md-7 col-sm-6 my-1">
                            <div className="search_bar">
                                <div className="input_search">
                                    <input type="text" className="form-control" name="search" value={this.state.search || ''} onChange={(e) => this.setState({ 'search': e.target.value })} placeholder="Search" />
                                    <button type="submit"><span className="icon icon-search"></span></button>
                                </div>
                                </div>
                            </div>
                            <div className="col-lg-2 col-md-2 col-sm-2">
                                <div className="row ">
                                    <div className="col-lg-12 col-md-12">
                                        <div className="include_box">
                                        <input type="checkbox" name="inactive" className="checkbox_big" id="c1" checked={this.state.inactive} value={this.state.inactive || ''} onChange={(e) => this.searchData('inactive', e.target.checked)} />
                                        <label htmlFor="c1"><span></span><small className="pl-1">Include Inactive</small> </label>
                                        </div>
                                    </div>
                                  {/*   <span className="col-lg-6 col-md-6 include_box">
                                        <input type="checkbox" name="incomplete" className="checkbox_big" id="incomplete" checked={this.state.incomplete} value={this.state.incomplete || ''} onChange={(e) => this.searchData('incomplete', e.target.checked)} />
                                        <label htmlFor="incomplete"><span></span><small className="pl-1">Incomplete Only</small> </label>
                                    </span> */}
                                </div>
                            </div>
                            <div className="col-lg-2 col-md-3 col-sm-4">
                                <div className="box w-100">
                                    <Select className="wide" required={true} simpleValue={true} searchable={false} clearable={false} options={listParticipantDropdown(0)}
                                        value={this.state.search_by || ''} placeholder="Filter by type" onChange={(e) => this.searchData('search_by', e)} />
                                </div>
                            </div>
                        </div>
                        <div className="row"><div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div></div>
                        </form>
                        </React.Fragment>

                    <ListParticipant filtered={this.state.filtered} />
              
                    </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({
})

const mapDispatchtoProps = (dispach) => {
    return {
        setSubmenuShow: (result) => dispach(setSubmenuShow(result))
    }
};
export default connect(mapStateToProps, mapDispatchtoProps)(Dashboard);

class NewParticipantCount extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            participantCount: 0,
            view_type: 'month',
            all_participant_count: ''
        };

    }

    componentDidMount() {
        this.participantAjax();
    }

    participantCountBy = (type) => {
        this.setState({ view_type: type }, function () {
            this.participantAjax();
        });
    }

    participantAjax = () => {
        postData('participant/ParticipantDashboard/participant_count', this.state).then((result) => {
            if (result.status) {
                this.setState({ participantCount: result.participant_count });
                this.setState({ all_participant_count: result.all_participant_count });
            } else {
                this.setState({ error: result.error });
            }
        });
    }

    render() {
        return (
            <li className="radi_2">
                <h2 className="text-center P_15_b cl_black">New Participant Counter:</h2>
                <div className="row pb-3 align-self-center w-100 mx-auto Graph_flex">

                <div className="col-md-8 col-xs-12 d-inline-flex align-self-center justify-content-center mb-3">
                    <CounterShowOnBox counterTitle={this.state.participantCount} classNameAdd=""/>
                    </div>
                   
                    <div className="W_M_Y_box P_15_T col-md-4 col-xs-12 d-inline-flex align-self-center">
                        <div className="vw_bx12 mx-auto">
                            <h5 className=""><b>View By</b></h5>
                            <span onClick={() => this.participantCountBy('week')} className={this.state.view_type == 'week' ? 'color' : ''}>Week</span><br />
                            <span onClick={() => this.participantCountBy('month')} className={this.state.view_type == 'month' ? 'color' : ''}>Month </span><br />
                            <span onClick={() => this.participantCountBy('year')} className={this.state.view_type == 'year' ? 'color' : ''}> Year</span>
                        </div>
                    </div>
                </div>
            </li>);
    }
}

class NewParticipantGraph extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            participantCount: 0,
            view_type: 'month',
            all_participant_count: ''
        };
    }

    componentDidMount() {
        this.participantAjax();
    }

    participantCountBy = (type) => {
        this.setState({ view_type: type }, function () {
            this.participantAjax();
        });
    }

    participantAjax = () => {
        postData('participant/ParticipantDashboard/participant_count', this.state).then((result) => {
            if (result.status) {
                this.setState({ participantCount: result.participant_count });
                this.setState({ all_participant_count: result.all_participant_count });
            } else {
                this.setState({ error: result.error });
            }
        });
    }

    render() {
        const data = {
            labels: ['Participant Other', 'Participant New'],
            datasets: [{
                data: [(this.state.all_participant_count - this.state.participantCount), this.state.participantCount],
                backgroundColor: ['#6f4dee', '#672ea7'],
                hoverBackgroundColor: ['#6f4dee', '#672ea7'],
                borderWidth: 0,
            }],
        };
        return (
            <li className="radi_2">
                <h2 className="text-center P_15_b cl_black">New Participant Graph:</h2>
                <div className="row pb-3 align-self-center w-100 mx-auto Graph_flex">
                    <div className="text-center align-self-center col-md-5 col-xs-12">
                        <div className="myChart12" >
                            <Doughnut data={data} height={210} className="myDoughnut" legend={{ display: false }} />
                        </div>
                    </div>

                    <div className="col-md-4 col-xs-12 text-center d-inline-flex align-self-center">
                        <div className="myLegend mx-auto">
                            <h4 className="clr1">New = {this.state.participantCount}</h4>
                            <h4 className="clr2">Total = {this.state.all_participant_count}</h4>

                        </div>
                    </div>

                    <div className="W_M_Y_box P_15_T col-md-3 col-xs-12 pb-3 d-inline-flex align-self-center">
                        <div className="vw_bx12 mx-auto">
                            <h5 className=""><b>View By</b></h5>
                            <span onClick={() => this.participantCountBy('week')} className={this.state.view_type == 'week' ? 'color' : ''}>Week</span><br />
                            <span onClick={() => this.participantCountBy('month')} className={this.state.view_type == 'month' ? 'color' : ''}>Month </span><br />
                            <span onClick={() => this.participantCountBy('year')} className={this.state.view_type == 'year' ? 'color' : ''}> Year</span>
                        </div>
                    </div>
                </div>
            </li>
        );
    }
}