import React, { Component } from 'react';

import 'react-select-plus/dist/react-select-plus.css';
import { connect } from 'react-redux'
import 'react-toastify/dist/ReactToastify.css';
import BigCalendar from 'react-big-calendar'
import Toolbar from 'react-big-calendar/lib/Toolbar';
import 'react-big-calendar/lib/css/react-big-calendar.css'
import moment from 'moment-timezone';

import { checkItsNotLoggedIn, postData, calendarColorCode } from '../../../service/common.js';

import ParticipantNavigation from '../../admin/participant/ParticipantNavigation';
import ShortShiftDetails from '../../admin/externl_component/ShortShiftDetails';

import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { customHeading, customNavigation } from '../../../service/CustomContentLoader.js';
import ParticipantProfile from '../../admin/participant/ParticipantProfile';


class ParticipantCurrentShift extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();

        this.state = {
            myEventsList: [],
            participantId: this.props.props.match.params.id,

        }
    }

    componentDidMount() {
        this.getParticipantEvent(moment());
    }

    closeModel = () => {
        this.setState({ openShit: false, shiftId: '' })
    }

    openSortShiftDetails = (event) => {
        this.setState({ selelctShiftId: event.id, openShit: true })
    }

    eventStyleGetter(event, start, end, isSelected) {
        var backgroundColor = calendarColorCode(event.status);
        var style = {
            backgroundColor: backgroundColor,

        };
        return {
            style: style
        };
    }

    getParticipantEvent = (e) => {
        var requestData = { participantId: this.state.participantId, date: e };
        postData('participant/Participant_profile/get_participant_upcoming_shift', requestData).then((result) => {
            if (result.status) {
                var tempAry = result.data;
                if (tempAry.length > 0) {

                    tempAry.map((value, idx) => {
                        tempAry[idx]['end'] = value.end
                        tempAry[idx]['start'] = value.start
                    })

                    this.setState({ myEventsList: tempAry });
                }

            } else {
                this.setState({ error: result.error });
            }
            this.setState({ loading: false });
        });

    }

    onSelectEvent = (date) => {
        //        console.log(date);
    }


    render() {
        moment.locale('ko', {
            week: {
                dow: 1,
                doy: 1,
            },
        });
      
        moment.tz.setDefault('Australia/Melbourne');
        const localizer = BigCalendar.momentLocalizer(moment)
        return (
            <React.Fragment>
                <ParticipantProfile id={this.props.props.match.params.id}  pageTypeParms="current_shift" />
                <section >
                  
                        <div className="row">
                           <ReactPlaceholder defaultValue={''} showLoadingAnimation={true} customPlaceholder={customNavigation()} ready={!this.props.loading}>
                                <ParticipantNavigation activeClass={'shiftRoster'} active="current_shift" />
                            </ReactPlaceholder>
                            <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                               
                                <div className="tab-content">
                                    <div role="tabpanel" className="tab-pane active" id="Barry_details">
                                      
                                                <div className="row">
                                                <div className="col-md-12"><div className="bor_T"></div></div>
                                                    <ReactPlaceholder defaultValue={''} showLoadingAnimation={true} customPlaceholder={customHeading(20)} ready={!this.props.loading}>
                                                        <div className="col-md-12 P_7_TB"><h3>Shifts for '{this.props.firstname}'</h3></div>
                                                    </ReactPlaceholder>
                                                    <div className="col-md-12"><div className="bor_T"></div></div>
                                                </div>
                                           
                                        <div className="member_shift">
                                            <BigCalendar
                                                localizer={localizer}
                                                events={this.state.myEventsList}
                                                startAccessor="start"
                                                endAccessor="end"
                                                eventPropGetter={(this.eventStyleGetter)}
                                                views={['month']}
                                                defaultDate={this.state.default_date}
                                                onNavigate={this.getParticipantEvent}
                                                components={{ toolbar: CalendarToolbar }}
                                                onSelectEvent={(event) => this.openSortShiftDetails(event)}

                                            />
                                            <div className="row px-5">
                                                <div className="col-md-8 mt-3">
                                                    <ul className="status_new">
                                                        <li><span className="Confirmed"></span>Confirmed</li>
                                                        <li><span className="Unfilled"></span>Unfilled</li>
                                                        <li><span className="Unconfirmed"></span>Unconfirmed</li>
                                                        <li><span className="Cancelled"></span>Cancelled</li>
                                                    </ul>
                                                </div>
                                                {/* <div className="col-md-4"><div className="add_i_icon mt-3 pull-right"><a><span className="icon icon-add-icons"></span></a></div></div>*/}
                                            </div>

                                            <ShortShiftDetails shiftId={this.state.selelctShiftId} openShit={this.state.openShit} closeModel={this.closeModel} />
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                   

                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    firstname: state.ParticipantReducer.participantProfile.firstname,
    fullName: state.ParticipantReducer.participantProfile.fullName
})

export default connect(mapStateToProps)(ParticipantCurrentShift)

class CalendarToolbar extends Toolbar {
    componentDidMount() {
        const view = this.props.view;
    }

    render() {
        return (
            <div>
                <div className="rbc-btn-group">
                    <span className="" onClick={() => this.navigate('TODAY')} >Today</span>
                    <span className="icon icon-arrow-left" onClick={() => this.navigate('PREV')}></span>
                    <span className="icon icon-arrow-right" onClick={() => this.navigate('NEXT')}></span>
                </div>
                <div className="rbc-toolbar-label">{this.props.label}</div>
            </div>
        );
    }
}