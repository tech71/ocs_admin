import React from 'react';

import { postData } from '../../../service/common.js';
import Modal from 'react-bootstrap/lib/Modal'

import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux'
import ReactTable from "react-table";
import {CSVLink} from 'react-csv';
import Pagination from "../../../service/Pagination.js";
import {PAGINATION_SHOW} from '../../../config';

const requestData = (pageSize, page, sorted, filtered, participantId) => {
    return new Promise((resolve, reject) => {
        var Request = {pageSize: pageSize, page: page, sorted: sorted, filtered: filtered, participantId: participantId};

            postData('participant/Participant_profile/get_contact_log', Request).then ((result) => {
                    let filteredData = result.data;
                           const res = {
                                rows: filteredData,
                                pages: (result.count)
                            };
                   resolve(res);
            });

    });
};

class ContactHistoryLog extends React.Component {
    constructor(props) {
        super(props); 
        this.state = {   
            booking_date:'',
            booking_status:'',
            userList: [],
        }
    } 
    
    downloadCSV = () => {
        
    }
    
    fetchData = (state, instance)=> {
        // function for fetch data from database
        this.setState({loading: true});
        requestData(
                state.pageSize,
                state.page,
                state.sorted,
                state.filtered,
                this.props.participantId
                ).then(res => {
            this.setState({
                userList: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }
    
    onSubmit = (e) => {
        e.preventDefault(); 
        this.setState({filtered: {search: this.state.search}})
    }

    render() {
        const csv_header = [
                            {label: 'Type', key: 'type'},
                            {label: 'Initiated By HCMID', key: 'HCMID'},
                            {label: 'Contact Used', key: 'contact_used'},
                            {label: 'Date', key: 'created'},
                        ];
        
        const columns = [{ Header: 'Type', accessor: 'type', filterable: false, sortable: false,},
        { Header: 'Initiated By HCMID', accessor: 'HCMID', filterable: false },
        { Header: 'Contact Used', accessor: 'contact_used', filterable: false },
        { Header: 'Date', accessor: 'created', filterable: false, Cell: props => <span>{props.value}</span>},
        { Header: 'Duration', accessor: 'duration', filterable: false },
        {
            Header: 'Action',
            accessor: 'ID',
            sortable: false,
            filterable: false,
            sortable: false,
            Cell: props => <span className="booking_L_T1 color">
                            {props.original.type == 'Imail'? <a href={"/admin/imail/external/inbox/"+ props.value}><i className="icon icon-view1-ie color"></i></a>
                            : <a href=""><i className="icon icon-view1-ie color"></i></a>}
                            
                             <CSVLink data={[props.original]} headers={csv_header} filename={"loges.csv"}>
                                    <i className="icon icon-download1-ie"></i>
                             </CSVLink>
                            <i className="icon icon-invoice"></i>
            </span>
        }
        ]
        return (
        <div className="ConH_modal">
            <Modal bsSize="large" className="modal fade-scale" show={this.props.openModel} onHide={this.handleHide} container={this} aria-labelledby="myModalLabel" id="modal_1" tabIndex="-1" role="dialog" >
               <form id="booking_form" method="post" autoComplete="off">
                <Modal.Body>
                    <div className="dis_cell">
                        <div className="text text-left by-1">Contact History Log:
                            <a data-dismiss="modal" aria-label="Close" className="close_i pull-right mt-1" onClick={()=> this.props.cloaseModel()}><i className="icon icon-cross-icons"></i></a>
                        </div>
                        <div>
                              <form onSubmit={this.onSubmit}>
                              <div className="row py-5">
                              <div className="col-md-6">
                              <div className="table_search_new ConH_Sear">
                                  <input type="text" onChange={(e) => this.setState({search: e.target.value})} name="" value={this.state.search || ''} />
                                  <button type="submit">
                                      <span className="icon icon-search"></span>
                                  </button>
                              </div>
                              </div>
                              </div>
                              </form>
                        </div>
                        <div className="row ">
                        <div className="col-md-12 listing_table PL_site">
                            <ReactTable
                                   PaginationComponent={Pagination}                             
                                    columns={columns}
                                    manual 
                                    ref={this.reactTable}
                                    data={this.state.userList}
                                    pages={this.state.pages}
                                    loading={this.state.loading} 
                                    onFetchData={this.fetchData} 
                                    filtered={this.state.filtered}
                                    defaultPageSize={10}
                                    noDataText="No Records"
                                    className="-striped -highlight"  

                                    minRows={2}
                                    previousText = {<span className="icon icon-arrow-1-left privious"></span>}
                                    nextText = {<span className="icon icon-arrow-1-right next"></span>}
                                    showPagination={this.state.userList.length > PAGINATION_SHOW ? true : false }
                                />
                        </div>
                        </div>
                    </div> 
                    
                   
                </Modal.Body>
               </form>
            </Modal>
        </div>
        );
    }
}

const mapStateToProps = state => ({
    participantId : state.ParticipantReducer.participantProfile.id,
})


export default connect(mapStateToProps)(ContactHistoryLog)