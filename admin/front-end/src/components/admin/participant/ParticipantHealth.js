import React, { Component } from 'react';

import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import jQuery from "jquery";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Modal from 'react-bootstrap/lib/Modal';
import Panel from 'react-bootstrap/lib/Panel';
import { connect } from 'react-redux'
import { checkItsNotLoggedIn, postData, archiveALL } from '../../../service/common.js';
import { customHeading, customNavigation, custNumberLine } from '../../../service/CustomContentLoader.js';
import ParticipantProfile from '../../admin/participant/ParticipantProfile';
import ParticipantNavigation from '../../admin/participant/ParticipantNavigation';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { ToastUndo } from 'service/ToastUndo.js';

class ParticipantHealth extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();

        this.initialState = {
            loading: true,
            view_by: 'current',
            diagnosis: [],
            health_notes: [],
            misc: {},
        }

        this.state = this.initialState;
        this.selectChange = this.selectChange.bind(this);

    }

    componentDidMount() {
        this.getDataHealthAndMisc(this.state.view_by);
    }

    getDataHealthAndMisc = (type) => {
        this.setState({ loading: true }, () => {
            postData('participant/Participant_profile/get_participant_health_and_misc', { id: this.props.props.match.params.id, type: type }).then((result) => {
                if (result.status) {
                    this.setState(result.data);
                    this.setState({ loading: false });
                }
            });
        })
    }


    selectChange(selectedOption, fieldname) {
        var state = {};
        state[fieldname] = selectedOption;
        this.setState(state);
        this.getDataHealthAndMisc(selectedOption);
    }

    render() {
        return (
            <React.Fragment>
                <ParticipantProfile id={this.props.props.match.params.id} pageTypeParms={this.props.props.match.params.page}/>

                <div className="row">
                    <ReactPlaceholder defaultValue={''} showLoadingAnimation ={true}  customPlaceholder={ customNavigation()} ready={!this.props.loading}>
                    <ParticipantNavigation  active="health" activeClass={'about'} />
                    </ReactPlaceholder>
                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                        <div className="tab-content">
                            <ParticipantCareHealth loading={this.state.loading} showTypePage={this.props.showTypePage} upLoading={this.props.loading} notes={this.state.diagnosis} participantID={this.props.props.match.params.id} getDataHealthAndMisc={this.getDataHealthAndMisc} view_by={this.state.view_by} selectChange={this.selectChange} firstname={this.props.firstname} fullName={this.props.fullName} id={'diagnosis'} active={'active'} title={'Formal Diagnosis'} noPrimary={true} />
                            <ParticipantCareHealth loading={this.state.loading} showTypePage={this.props.showTypePage} notes={this.state.health_notes} participantID={this.props.props.match.params.id} getDataHealthAndMisc={this.getDataHealthAndMisc} view_by={this.state.view_by} selectChange={this.selectChange} firstname={this.props.firstname} fullName={this.props.fullName} id={'health_notes'} active={''} title={'Health Notes'} noPrimary={false} />
                            <ParticipantMisc loading={this.state.loading}  showTypePage={this.props.showTypePage} firstname={this.props.firstname} misc={this.state.misc} />
                        </div>
                    </div>

                </div>

                </React.Fragment>
                );
    }
}

const mapStateToProps = state => ({
    firstname: state.ParticipantReducer.participantProfile.firstname,
    fullName: state.ParticipantReducer.participantProfile.fullName,
    showTypePage:state.ParticipantReducer.activePage.pageType
})


export default connect(mapStateToProps)(ParticipantHealth)


class ParticipantCareHealth extends Component {
    constructor(props) {
        super(props);

        this.viewByOption = [{ value: 'current', label: 'Current' }, { value: 'archive', label: 'Archive' }]

        this.state = {
            updataData: false,
            notes: [],
            success: false,
            view_by: 'current'
        }
    }

    archiveAddress = (index, id, keyType) => {
        let msg = keyType == 1 ? (<span>Are you sure you want to archive this Primary item?<br /> Primary only archive when another secondary item exists other wise not archive. <br /> Once archived, this action can not be undone.</span>) : (<span>Are you sure you want to archive this item? <br /> Once archived, this action can not be undone.</span>);
        
        let url = 'participant/Participant_profile/archive_participant_about_care';
        
//        archiveALL(id, 'participant_about_care', msg, url, loges).then((result) => {
        archiveALL({id: id, participantId: this.props.participantID}, msg, url).then((result) => {
            if (result.status) {
                var state = {}
                state['notes'] = this.state.notes.filter((s, sidx) => index !== sidx);
                this.setState(state, function () {
                    this.props.getDataHealthAndMisc(this.props.view_by);
                });
            } else{
                toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                // toast.error(result.msg, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
            }
        })
    }

    updateData = (data) => {
        this.setState({ updataData: data }, () => {
            this.setState({ create: true });
        });
    }

    createData = () => {
        this.setState({ updataData: false }, () => {
            this.setState({ create: true })
        });
    }

    componentWillReceiveProps(newProps) {
        this.setState({ notes: newProps.notes });
    }

    closeCreate = (response) => {
        if (response) {
            this.props.getDataHealthAndMisc(this.props.view_by);
        }
        this.setState({ create: false });
    }

    render() {
        return (
            <div role="tabpanel" className={this.props.showTypePage==this.props.id ? 'tab-pane active' :'tab-pane'} id={this.props.id}>
                <div className="row d-flex">
                    <div className="col-lg-9 col-md-9 col-xs-9">
                        <div className="row">
                        <div className="col-md-12"><div className="bor_T"></div></div>
                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(60)} ready={!this.props.upLoading}>
                                <div className="col-md-12 P_7_TB"><h3>{/* this.props.firstname */}{this.props.title}</h3></div>
                            </ReactPlaceholder>
                            <div className="col-md-12"><div className="bor_T"></div></div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-3 col-xs-3">
                        <div className="box">
                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(60)} ready={!this.props.upLoading}>
                                <Select name="view_by" searchable={false} clearable={false} simpleValue={true}
                                    value={this.props.view_by} onChange={(e) => this.props.selectChange(e, 'view_by')}
                                    options={this.viewByOption} placeholder="View By:" />
                            </ReactPlaceholder>
                        </div>
                    </div>
                </div>
                <div className="row P_15_TB">
                    <div className="col-md-12">
                        <div className="panel-group accordion_me house_accordi Par_care_ac" id="accordion" role="tablist" aria-multiselectable="true">
                            <ReactPlaceholder type='textRow' customPlaceholder={custNumberLine(4)} ready={!this.props.loading}>

                                {(this.state.notes.length > 0) ?
                                    this.state.notes.map((object, index) => (
                                        <Panel key={index + 1} eventKey={index + 1}>
                                            <Panel.Heading>
                                                <Panel.Title toggle>
                                                    <i className="more-less glyphicon glyphicon-plus"></i>
                                                    <div className="resident_name py-0">
                                                        <div className="row">
                                                            <div className="col-md-4 py-3 col-xs-4">
                                                                <span className="color">{object.created}</span>
                                                                {this.props.noPrimary ? <span className="color pull-right">{(object.primary_key == 1) ? 'Primary' : 'Secondary'}:</span> : ''}
                                                            </div>
                                                            <div className="col-md-8 col-xs-8 collapsible collapsed_me  py-3 bl_1_gray P_30_R">
                                                                {object.content}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Panel.Title>
                                            </Panel.Heading>
                                            <Panel.Body collapsible className="px-1 py-0">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="col-md-4 col-sm-4"></div>
                                                        {(this.props.view_by == 'current') ?
                                                            <div className="col-md-8 col-sm-8 bl_1_gray text-right pb-2">
                                                                <a><i onClick={() => this.updateData({ title: object.title, content: object.content, notesID: object.id, primary_key: object.primary_key })} className="icon icon-update update_button"></i></a>
                                                                {(object.primary_key != 0) ? <a className="ml-2"><i onClick={() => this.archiveAddress(index, object.id, object.primary_key)} className="icon icon-email-pending archive_button"></i></a> : ''}
                                                            </div> : ''}
                                                    </div>
                                                </div>

                                            </Panel.Body>
                                        </Panel>

                                    )) : <div className="no_record py-2">No record available</div>}

                            </ReactPlaceholder>

                            <ParticipantCretesNotes fullName={this.props.fullName} modal_show={this.state.create} categories={this.props.id} closeModal={this.closeCreate} participantID={this.props.participantID} updataData={this.state.updataData} title={this.props.title} firstname={this.props.firstname} />

                            <div className="mt-3 pull-right">
                                <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(20)} ready={!this.props.loading}>
                                    {(this.props.view_by == 'current') ? <a className="button_plus__" onClick={this.createData}><span className="icon icon-add-icons  Add-2-1"></span></a> : ''}
                                </ReactPlaceholder>
                            </div>


                        </div>
                    </div>

                </div>
            </div>

        )
    }

}

class ParticipantMisc extends Component {
    constructor(props) {
        super(props);

        this.state = {
            notesID: false,
            loading: true,
            success: false,
        }
    }

    render() {
        return (
            <div role="tabpanel" className={this.props.showTypePage=='misc' ? 'tab-pane active':'tab-pane'} id="misc">
                <div className="row">
                    <div className="col-lg-12 col-md-12">
                        <div className="row">
                        <div className="col-md-12"><div className="bor_T"></div></div>
                            <div className="col-md-12 P_7_TB"><h3>{/* this.props.firstname */}Miscellaneous Details</h3></div>
                            <div className="col-md-12"><div className="bor_T"></div></div>
                        </div>
                    </div>

                </div>
                <div className="row P_15_TB">
                    <div className="col-md-12">
                        <div className="panel-group accordion_me house_accordi Par_care_ac" id="accordion" role="tablist" aria-multiselectable="true">
                            <div className="col-lg-12">
                                <div className="row f_color_size">
                                    <div className="col-lg-3 col-md-3 f align_e_2 col-xs-3">Medicare No.:</div>
                                    <div className="col-lg-9 col-md-9 f align_e_1 col-xs-9">{this.props.misc.medicare_num || 'N/A'} </div>
                                </div>
                                <div className="row f_color_size my-3">
                                    <div className="col-lg-3 col-md-3 f align_e_2 col-xs-3"></div>
                                    <div className="col-lg-9 col-md-9 f align_e_1 col-xs-9 dotted_line"></div>
                                </div>
                                <div className="row f_color_size">
                                    <div className="col-lg-3 col-md-3 f align_e_2 col-xs-3">CRN :</div>
                                    <div className="col-lg-9 col-md-9 f align_e_1 col-xs-9">{this.props.misc.crn_num || 'N/A'} </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        )
    }
}


class ParticipantCretesNotes extends Component {
    constructor(props) {
        super(props);

        this.state = {
            notesID: false,
            loading: false,
            success: false,
            primary_key: 2
        }
        this.onChange = this.onChange.bind(this);

    }

    onSubmit(e) {
        e.preventDefault();
        jQuery('#create_notes').validate({
            rules: {
                content: "required",
            }
        });
        if (jQuery('#create_notes').valid()) {
            this.setState({ loading: true }, () => {
                var data = { notesID: this.state.notesID, title: this.state.title, content: this.state.content, categories: this.props.categories, participantID: this.props.participantID, primary_key: this.state.primary_key, fullName: this.props.fullName };

                postData('participant/Participant_profile/create_cares_notes', data).then((result) => {
                    if (result.status) {
                        this.props.closeModal(true);
                        this.setState({ title: '', content: '' });
                    } else {
                        toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                        // toast.error(result.error, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                    }
                    this.setState({ loading: false })
                });
            })
        }
    }

    onChange(e) {
        var state = {};
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    closeModal() {
        this.props.closeModal(false);
    }

    componentWillReceiveProps() {
        if (this.props.updataData) {
            var data = this.props.updataData;
            this.setState({ title: '', content: data.content, notesID: data.notesID, primary_key: data.primary_key });
        } else {
            this.setState({ title: '', content: '', notesID: false, primary_key: 2 });
        }
    }
    render() {
        return (
            <div>
                <Modal
                    className="modal fade Modal_A Modal_B"
                    show={this.props.modal_show}
                    onHide={this.handleHide}
                    container={this}
                    aria-labelledby="contained-modal-title"
                >
                    <Modal.Body>
                        <form onSubmit={this.onSubmit.bind(this)} id="create_notes">

                            <div className="text text-left">{this.props.updataData ? 'Update' : 'Add New'} {this.props.title} For '{this.props.firstname}'
                                        <a type="button" className="close_i pull-right mt-1"><i onClick={this.closeModal.bind(this)} className="icon icon-cross-icons"></i></a>
                            </div>

                            <div className="row P_15_T">
                                <div className="col-md-12">
                                    <label>{this.props.title}:</label>
                                    <span className="">
                                        <textarea className="col-md-12 min-h-120 textarea-max-size" onChange={this.onChange} name="content" value={this.state['content'] || ''} data-rule-required="true" data-rule-maxlength="500" ></textarea>
                                    </span>
                                </div>

                            </div>

                            <div className="row P_15_T">
                                <div className="col-md-7"></div>
                                <div className="col-md-5">
                                    <input type="submit" disabled={this.state.loading} className="but" value={'Save ' + this.props.title} name="content" />
                                </div>
                            </div>

                        </form>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}