import React, { Component } from 'react';
import jQuery from "jquery";
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import Modal from 'react-bootstrap/lib/Modal';
import { ToastContainer, toast }
    from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import DatePicker from 'react-datepicker';

import { checkItsNotLoggedIn, getJwtToken, postData, handleRemoveShareholder, handleShareholderNameChange, handleAddShareholder, handleChange, getOptionsSuburb, archiveALL, reFreashReactTable }
    from '../../../service/common.js';
import { customProfile, customHeading, customNavigation }
    from '../../../service/CustomContentLoader.js';
import { BrowserRouter as Router, Switch, Route, Link, Redirect }
    from 'react-router-dom';
import moment from 'moment';
import { interpretertDropdown, cognitionDropdown, prefterLanguageDropdown, HearingOption, communicationDropdown, religiousDropdown, ethnicityDropdown, genderDropdown, preferContactDropdown, sitCategoryListDropdown, relationDropdown }
    from '../../../dropdown/ParticipantDropdown.js';
import { connect }
    from 'react-redux'
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import ReactTable from "react-table";
import 'react-table/react-table.css'
import ParticipantNavigation from '../../admin/participant/ParticipantNavigation';
import ParticipantProfile from '../../admin/participant/ParticipantProfile';
import BookingDatePopup from '../../admin/participant/BookingDatePopup';
import ContactHistoryLog from '../../admin/participant/ContactHistoryLog';
import ParticipantBookingList from '../../admin/participant/ParticipantBookingList';
import ParticipantPrefers from './inner_component/ParticipantPrefers';
import SingleRow from './inner_component/SingleRow';
import DottedLine from './inner_component/DottedLine';
import { setSubmenuShow }
    from 'components/admin/actions/SidebarAction';
import { ToastUndo }
    from 'service/ToastUndo.js'

import UpdateParticipantAboutUs from './UpdateParticipantAboutUs';
//import {setActiveSelectPage} from 'components/admin/participant/actions/ParticipantAction';

class ParticipantAbout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            success: false,
        }
        this.selectChange = this.selectChange.bind(this);
    }

    componentDidMount() {
        this.getAboutDetails();
    }

    getAboutDetails = () => {
        this.setState({ loading: true }, () => {
            postData('participant/Participant_profile/get_participant_about', { id: this.props.props.match.params.id }).then((result) => {
                if (result.status) {
                    var details = result.data;
                    this.setState(details);
                }
                this.setState({ loading: false });
            });
        });
    }

    selectChange(selectedOption, fieldname) {
        var state = {};
        state[fieldname] = selectedOption;
        this.setState(state);
    }

    render() {
        return (
            <React.Fragment>
                <ParticipantProfile id={this.props.props.match.params.id} pageTypeParms={this.props.props.match.params.page} />

                <div className="row">
                    <ReactPlaceholder defaultValue={''} showLoadingAnimation={true} customPlaceholder={customNavigation()} ready={!this.props.loading}>
                        <ParticipantNavigation active="about" activeClass={'about'} />
                    </ReactPlaceholder>
                    <div className="col-lg-10 col-md-12 col-lg-offset-1 ">

                        <div className="tab-content">

                            <ParticipantAboutDetails loading={this.state.loading} fullName={this.props.fullName} about={this.state} showTypePage={this.props.showTypePage} getAboutDetails={this.getAboutDetails} />
                            <ParticipantAboutRequirment loading={this.state.loading} fullName={this.props.fullName} requirment={this.state} showTypePage={this.props.showTypePage} getAboutDetails={this.getAboutDetails} />
                            <ParticipantPrefers loading={this.state.loading} participantId={this.props.props.match.params.id} />
                            <ParticipantBookingList loading={this.state.loading} participantId={this.props.props.match.params.id} />

                        </div>
                    </div>
                </div>

            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({
    getSidebarMenuShow: state.sidebarData,
    showTypePage: state.ParticipantReducer.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {

    }
};
export default connect(mapStateToProps, mapDispatchtoProps)(ParticipantAbout);
class ParticipantAboutDetails extends Component {
    constructor(props) {
        super(props);
        this.bookingList = [{ value: 'Plan', label: 'Plan(s)' }, { value: 'Goals', label: 'Goals' }, { value: 'Documents', label: 'Documents' }, { value: 'Funding_Tracker', label: 'Funding Tracker' }];
        this.state = {
            phones: [],
            emails: [],
            address: [],
            kin_detials: [],
            update: false,
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState(newProps.about);
    }

    selectChange = (selectedOption, fieldname) => {
        var state = {};
        state[fieldname] = selectedOption;
        this.setState(state);
    }

    cloaseModel = () => {
        this.setState({ update: false });
    }
    cloaseConctactLogModel = () => {
        this.setState({ call_log_model: false });
    }
    cloaseBookingModel = () => {
        this.setState({ booking_status_model: false });
    }

    render() {
        return (
            <div role="tabpanel" className={this.props.showTypePage == 'details' ? "tab-pane active" : "tab-pane"} id="Barry_details">
                <div className="row">
                    <div className="col-lg-12 col-md-12">
                        <div className="row"> <div className="col-sm-12"><div className="bor_T"></div></div></div>
                        <div className="row d-flex  align-items-center py-3">

                            <div className="col-lg-6 col-sm-6">
                                <ReactPlaceholder defaultValue={''} showLoadingAnimation={true} customPlaceholder={customHeading(30)} ready={!this.state.loading}>
                                    <h3>About
                            {/* '{this.state.firstname}': */}
                                    </h3>
                                </ReactPlaceholder>
                            </div>



                            <div className="col-lg-3 col-lg-offset-3 col-sm-3 col-sm-offset-3">
                                <ReactPlaceholder defaultValue={
                                    ''} showLoadingAnimation={
                                        true} customPlaceholder={
                                            customHeading(60)} ready={
                                                !this.state.loading}>
                                    <a className="Plus_button ConH_Btn" onClick={
                                        () => this.setState({
                                            call_log_model: true
                                        })}><span>Contact History</span><i className="icon icon-pending-icons create_add_but"></i></a>
                                </ReactPlaceholder>
                            </div>


                        </div>
                        <div className="row"> <div className="col-sm-12"><div className="bor_T"></div></div></div>
                    </div>

                </div>

                <ReactPlaceholder defaultValue={
                    ''} showLoadingAnimation type='textRow' customPlaceholder={
                        customProfile} ready={
                            !this.props.loading}>

                    <div className="col-lg-12 mt-4">
                        <SingleRow Myclass={'row f_color_size'} title={'Name'} value={this.state['fullname']} /><DottedLine />
                        <SingleRow Myclass={
                            'row f_color_size'} title={
                                'HCM-ID'} value={
                                    this.state['ocs_id']} /><DottedLine />
                        <SingleRow Myclass={
                            'row f_color_size'} title={
                                'NDIS #'} value={
                                    this.state['ndis_num'] || 'N/A'} /><DottedLine />
                        <SingleRow Myclass={
                            'row f_color_size'} title={
                                'Preferred Contact'} value={
                                    (this.state['prefer_contact'] == 1) ? 'Contact' : 'Email'} /><DottedLine />

                        {
                            this.state.phones.map((object, index) => (
                                <div className="row f_color_size" key={index + 1}>
                                    <div className={'col-lg-3 col-md-3 f align_e_2 col-sm-3 col-xs-4 ' + ((index > 0) ? '' : '')} >Phone ({(object.primary_phone == 1) ? 'Primary' : 'Secondary'}): </div>
                                    <div className={'col-lg-9 col-md-9 f align_e_1 col-sm-9 col-xs-8' + ((this.state.phones.length == (index + 1)) ? '' : '') + ((index > 0) ? '' : '')}><u>{object.phone}</u><span className="h_small"><a href="javascript:void(0)"><i className="icon icon-pending-icons history_button"></i></a></span></div>
                                </div>
                            ))
                        }
                        <DottedLine />

                        {
                            this.state.emails.map((object, index) => (
                                <div className="row f_color_size" key={index + 1}>
                                    <div className={'col-lg-3 col-md-3 f align_e_2 col-sm-3 col-xs-4' + ((index > 0) ? '' : '')}>Email ({(object.primary_email == 1) ? 'Primary' : 'Secondary'}):</div>
                                    <div className={'col-lg-9 col-md-9 f align_e_1 col-sm-9 col-xs-8' + ((this.state.emails.length == (index + 1)) ? ' ' : '') + ((index > 0) ? '' : '')}>{object.email}</div>
                                </div>
                            ))
                        }
                        <DottedLine />

                        {
                            this.state.address.map((object, index) => (
                                <div className="row f_color_size" key={index + 1}>
                                    <div className={'col-lg-3 col-md-3 f align_e_2 col-sm-3 col-xs-4' + ((index > 0) ? '' : '')}>Address ({(object.primary_address == 1) ? 'Primary' : 'Secondray'}):</div>
                                    <div className={'col-lg-9 col-md-9 f align_e_1 col-sm-9 col-xs-8' + ((this.state.address.length == (index + 1)) ? ' ' : '') + ((index > 0) ? '' : '')}>{object.street}, {object.city.label}, {object.stateName} {object.postal}&nbsp;{(object.site_category > 0) ? <span> - {sitCategoryListDropdown(object.site_category)}</span> : ''}</div>
                                </div>
                            ))
                        }
                        <DottedLine />

                        <SingleRow Myclass={
                            'row f_color_size'} title={
                                'D.O.B'} value={
                                    moment(this.state.dob).format('DD-MM-YYYY') + '     -  ' + this.state['age'] + '  years old'} />
                        <DottedLine />

                        <div className="py-2">
                            {
                                this.state.kin_detials.map((object, index) => (
                                    <div className="row f_color_size" key={index + 1}>
                                        <div className={'col-lg-3 col-md-3 f align_e_2 col-sm-3 col-xs-4' + ((index > 0) ? '' : '')}>Next of Kin ({(object.primary_kin == 1) ? 'Primary' : (object.primary_kin == 2) ? 'Secondary' : 'N/a'}): </div>
                                        <div className={'col-lg-9 col-md-9 f align_e_1 col-sm-9 col-xs-8' + ((index > 0) ? '' : '')}>{(object.kinfullname) ? <span> {object.kinfullname} ({object.relation}) -  <u>{object.phone}</u> -  {object.email}
                                            <span className="h_small"><a href=""><i className="icon icon-pending-icons history_button"></i></a></span></span> : 'N/A'}</div>
                                    </div>
                                ))
                            }
                        </div>

                        <div className="row mt-4">
                            {
                /*<div className="col-lg-3 col-md-4 col-lg-offset-9 col-md-offset-8 mb-3" ><a onClick={()=> this.setState({booking_status_model: true})} className="but">Update Booking Status</a></div>*/}
                            <div className="col-lg-3 col-sm-4 col-lg-offset-9 col-sm-offset-8 " onClick={
                                () => this.setState({
                                    update: true
                                })} ><a className="but">Update About '{
                                    this.state.firstname}'</a></div>

                        </div>
                        <BookingDatePopup openModel={
                            this.state.booking_status_model} cloaseModel={
                                this.cloaseBookingModel} />
                        {
                            this.state.call_log_model ? <ContactHistoryLog openModel={this.state.call_log_model} cloaseModel={this.cloaseConctactLogModel} /> : ''
                        }
                        <UpdateParticipantAboutUs fullName={
                            this.props.fullName} update={
                                this.state.update} about={
                                    this.state} cloaseModel={
                                        this.cloaseModel} id={
                                            this.state['ocs_id']} getAboutDetails={
                                                this.props.getAboutDetails} />
                    </div>
                </ReactPlaceholder>
            </div>
        );
    }
}

class ParticipantAboutRequirment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            oc_service: [],
            support_required: [],
            assistanceId: [],
            care_not_tobook: [],
            care_not_tobook_n:{Ethnicity_female_option:'',Ethnicity_male_option:'',Religious_female_option:'',Religious_male_option:''},
            update: false,
            Ethnicity_male_option: ethnicityDropdown(0),
            Religious_male_option: religiousDropdown(0),
            Ethnicity_female_option: ethnicityDropdown(0),
            Religious_female_option: religiousDropdown(0),
        }
    }

    cloaseModel = () => {
        this.setState({ update: false });
    }

    componentWillReceiveProps(newProps) {
        this.setState(JSON.parse(JSON.stringify(newProps.requirment)));
    }
    render() {
        return (
            <div role="tabpanel" className={this.props.showTypePage == 'requirements' ? "tab-pane active" : "tab-pane"} id="Barry_availabitity">
                <div className="row">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="col-md-12"><div className="bor_T"></div></div>
                            <div className="col-md-12 P_7_TB">
                            <h3>{this.state.firstname+"'s" } Requirements</h3></div>
                            <div className="col-md-12"><div className="bor_T"></div></div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 mt-4">
                    <SingleRow Myclass={'row f_color_size'} title={'Services'} value={this.state['oc_serviceName'] || 'N/A'} /><DottedLine />
                    <SingleRow Myclass={'row f_color_size'} title={'Support Required'} value={this.state['supportName'] || 'N/A'} /><DottedLine />
                    <SingleRow Myclass={'row f_color_size'} title={'Assistance Required With/Mobility'} value={this.state['assistanceName'] || 'N/A'} /><DottedLine />
                        
                    <div className="row f_color_size">
                        <div className="col-lg-3 col-md-3 f align_e_2">Carers NOT to Book:</div>
                    </div>                     
                    
                    <span className="">
                        <div className="row P_25_T">

                        <div className="col-lg-3 col-md-3">
                                <label><span className="gendar_txt_">Male</span></label><br />
                                <span className="pull-left">
                                    <label htmlFor="a3"><span></span>Selected options for Males</label>
                                </span>
                            </div>
                            <div className="col-lg-9 col-md-9">
                                <div className="row">
                                <div className="col-md-5 col-md-offset-1">
                                        <label className="">Ethnicity:</label>
                                        <span className="">
                                            <label htmlFor="select_ethnicity[]" className="error CheckieError" style={{ display: "block", width: "100%;" }} ></label>
                                            <div className="Schedules_Multiple_checkbox overflow-hidden">
                                                <div className="scroll_active_modal">
                                                    {this.state.Ethnicity_male_option.map((req, idx) => (
                                                        (req.active)?
                                                        <span key={idx + 1} className="w_50 w-100 mb-2">
                            <input type='checkbox' name="select_ethnicity[]" className="checkbox1" checked={req.active || ''}/>
                                                            <label>
                            <div className="d_table-cell"> <span ></span></div>
                                                                <div className="d_table-cell"> {req.label}</div></label>
                                                        </span>:''
                                                    ))}
                                                </div>
                                            </div>
                                        </span>
                                    </div>

                                    <div className="col-md-5 col-md-offset-1">
                                        <label className="">Religion Beliefs :</label>
                                        <span className="">
                                            <label htmlFor="select_religious[]" className="error CheckieError" style={{ display: "block", width: "100%;" }} ></label>
                                            <div className="Schedules_Multiple_checkbox overflow-hidden">
                                                <div className="scroll_active_modal">
                                                    {this.state.Religious_male_option.map((req, idx) => (
                                                        (req.active)?
                                                        <span key={idx + 1} className="w_50 w-100 mb-2">
                                <input type='checkbox' name="select_religious[]" className="checkbox1" checked={req.active || ''} />
                                                            <label>
                                <div className="d_table-cell"> <span ></span></div>
                                                                <div className="d_table-cell"> {req.label}</div></label>
                                                        </span>:''
                                                    ))}
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </span>

                    <span className="">
                        <div className="row P_25_T">
                        <div className="col-lg-3 col-md-3">
                        <label><span className="gendar_txt_">Female</span></label><br />
                                <span className="pull-left">
                                
                                <label htmlFor="a4"><span></span>Selected options for Female</label>
                                </span>

                            </div>

                            <div className="col-lg-9  col-md-9">
                                <div className="row">
                                    <div className="col-md-5  col-md-offset-1">
                                        <label className="">Ethnicity:</label>
                                        <span className="">
                                <label htmlFor="Ethnicity_female_option[]" className="error CheckieError" style={{display: "block", width: "100%;"}} ></label>
                                            <div className="Schedules_Multiple_checkbox overflow-hidden">
                                                <div className="scroll_active_modal">
                                {this.state.Ethnicity_female_option.map((req, idx) => (
                                                        (req.active)?
                                                        <span key={idx + 1} className="w_50 w-100 mb-2">
                                    <input type='checkbox' name="Ethnicity_female_option[]" className="checkbox1"  checked={req.active || ''}  />
                                                            <label>
                                    <div className="d_table-cell"> <span ></span></div>
                                                                <div className="d_table-cell"> {req.label}</div></label>
                                                        </span>:''

                                                    ))}
                                                </div>
                                            </div>
                                        </span>
                                    </div>

                                    <div className="col-md-5 col-md-offset-1">
                                        <label className="">Religion Beliefs :</label>
                                        <span className="">
                                    <label htmlFor="Religious_female_option[]" className="error CheckieError" style={{display: "block", width: "100%;"}} ></label>
                                            <div className="Schedules_Multiple_checkbox overflow-hidden">
                                                <div className="scroll_active_modal">
                                    {this.state.Religious_female_option.map((req, idx) => (
                                        (req.active)?
                                                        <span key={idx + 1} className="w_50 w-100 mb-2">
                                        <input type='checkbox' name="Religious_female_option[]" className="checkbox1" checked={req.active || ''} />
                                                            <label>
                                        <div className="d_table-cell"> <span onClick={(e) => this.setMutipleCheckbox(e, idx, 'Religious_female_option','female')}></span></div>
                                                                <div className="d_table-cell"> {req.label}</div></label>
                                                        </span>:''
                                                    ))}
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </span>


                    <DottedLine />
                    <SingleRow Myclass={
                        'row f_color_size'} title={
                            'Cognition'} value={
                                cognitionDropdown(this.state.cognition)} />
                    <DottedLine />
                    <SingleRow Myclass={
                        'row f_color_size'} title={
                            'Communication'} value={
                                communicationDropdown(this.state.communication)} />
                    <DottedLine />
                    <SingleRow Myclass={
                        'row f_color_size'} title={
                            'Language(s)'} value={
                                prefterLanguageDropdown(this.state.preferred_language) + ' ' + ((this.state.preferred_language == 11) ? '(' + this.state.preferred_language_other + ')' : '')} />
                    <DottedLine />
                    <SingleRow Myclass={
                        'row f_color_size'} title={
                            'Interpreter'} value={
                                (this.state.linguistic_interpreter > 0) ? interpretertDropdown(this.state.linguistic_interpreter) : 'N/A'} />

                    <div className="row">
                        {
                /*<div className="col-lg-3 col-md-4 col-lg-offset-9 col-md-offset-8 mt-4"><a className="but">'{this.state.firstname}' Do NOT Book List</a></div>*/}
                        <div className="col-lg-3 col-sm-4 col-lg-offset-9 col-sm-offset-8 mt-4"><a onClick={
                            () => this.setState({ update: true })} className="but">Update '{
                                this.state.firstname}' Requirements</a></div>
                    </div>

                    <UpdateRequirements fullName={this.props.fullName} update={this.state.update} requirement={this.state} cloaseModel={this.cloaseModel} id={this.state['ocs_id']} getAboutDetails={this.props.getAboutDetails} />
                </div>
            </div>
        );
    }
}



class UpdateRequirements extends Component {
    constructor(props) {
        super(props);
        this.English_option = [{ value: 1, label: 'Yes preferred' }, { value: 2, label: 'Yes but not preferred' }];
        this.Gender_option = [{ value: 1, label: 'Male' }, { value: 2, label: 'Female' }];
        this.state = {
            require_assistance: [],
            mobality: [],
            oc_service: [],
            support_required: [],
            care_not_tobook: [],
            care_not_tobook_n:{Ethnicity_female_option:'',Ethnicity_male_option:'',Religious_female_option:'',Religious_male_option:''},
            fullName: this.props.fullName,
            Ethnicity_male_option: ethnicityDropdown(0),
            Religious_male_option: religiousDropdown(0),
            Ethnicity_female_option: ethnicityDropdown(0),
            Religious_female_option: religiousDropdown(0), 
            all_option_male:false,
            all_option_female:false,  
        }
        this.validator = false;
    }

    checkCarersNotBookOption = (evt,ethnicityState,religionState,checkBoxName) =>{
    let ethnicity = this.state[ethnicityState];
    let religion = this.state[religionState];

    var tempState = {};
    tempState[checkBoxName] = evt.target.checked;
    this.setState(tempState);

        ethnicity.forEach(ethn => {
            ethn.active = evt.target.checked
        })

        religion.forEach(relgn => {
            relgn.active = evt.target.checked
        })

        this.setState({ ethnicityState: ethnicity });
        this.setState({ religionState: religion });

    }

    onSubmit = (e) => {
        e.preventDefault();
        this.validator = jQuery('#update_requirement').validate({ ignore: [] });
        //
        
        if (!this.state.loading && jQuery("#update_requirement").valid()) {
            this.setState({ loading: true }, () => {
                postData('participant/ParticipantDashboard/update_participant_requirement', this.state).then((result) => {
                    if (result.status) {
                        toast.success(<ToastUndo message={'Participant update successfully'} showType={'s'} />, {
                            //    toast.success("Participant update successfully", {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                        this.props.cloaseModel();
                        this.props.getAboutDetails();
                    } else {
                        toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                            // toast.error(result.error, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                    }
                    this.setState({ loading: false });
                });
            })
        }else {
            this.validator.focusInvalid();
        }
    }

    handleChange = (e) => {
        var state = {};
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }


    componentWillReceiveProps(newProps) {
        this.setState(newProps.requirement);
        this.checkBookFor(newProps.requirement.care_not_tobook_n) 
    }

    checkBookFor(dataBook)
    {
        if(dataBook)
        {
            var selected_male_ethn = dataBook.Ethnicity_male_option;
            var selected_male_religious = dataBook.Religious_male_option;
            var male_option_count = parseInt( selected_male_religious.length) + parseInt(selected_male_ethn.length);
            var male_default_option_count = parseInt( this.state.Religious_male_option.length) + parseInt(this.state.Ethnicity_male_option.length);
            
            var selected_female_ethn = dataBook.Ethnicity_female_option;
            var selected_female_religious = dataBook.Religious_female_option;
            var female_option_count = parseInt( selected_female_religious.length) + parseInt(selected_female_ethn.length);
            var female_default_option_count = parseInt( this.state.Religious_female_option.length) + parseInt(this.state.Ethnicity_female_option.length);

            if(male_default_option_count == male_option_count)
            {
                this.setState({'all_option_male':true});
            }

            if(female_default_option_count == female_option_count)
            {
                this.setState({'all_option_female':true});
            }

            this.state.Ethnicity_male_option.map((req, idx) => {
                if(selected_male_ethn.indexOf(req.value.toString()) > -1)
                {
                    req.active = true;
                }
            })

            this.state.Religious_male_option.map((req, idx) => {
                if(selected_male_religious.indexOf(req.value.toString()) > -1)
                {
                    req.active = true;
                }
            })

            this.state.Ethnicity_female_option.map((req, idx) => {
                if(selected_female_ethn.indexOf(req.value.toString()) > -1)
                {
                    req.active = true;
                }
            })

            this.state.Religious_female_option.map((req, idx) => {
                if(selected_female_religious.indexOf(req.value.toString()) > -1)
                {
                    req.active = true;
                }
            })
        }
    }

    setMutipleCheckbox(e, idx, tagType,carersFor) {
        var List = this.state[tagType];

        if(typeof carersFor !== 'undefined' && (carersFor == 'male' || carersFor == 'female'))
        {
            if(List[idx]['active']){
                if(carersFor == 'male'){
                    this.setState({all_option_male:false})
                }
                else{
                    this.setState({all_option_female:false})
                }
            }
        }

        if (List[idx].active === true) {
            List[idx].active = false;
        } else {
            List[idx].active = true;
        }

        
        var state = {};
        state[tagType] = List;
        this.setState(state, () => {
            if(this.validator){
                jQuery("#update_requirement").valid();
            }
        });
    }

    selectChange(selectedOption, fieldname) {
        var state = {};
        if (fieldname == 'english' && selectedOption == 1) {
            state["preferred_language"] = 1;
        }

        state[fieldname] = selectedOption;
        this.setState(state);
    }

    render() {
        // for require assitance required 
        var requireIndex = this.state.require_assistance.findIndex(x => x.label == 'Other');
        var requireOther = false;
        if (requireIndex > - 1) {
            requireOther = (this.state.require_assistance[requireIndex].active) ? true : false;
        }

        // for require support required 
        var supportIndex = this.state.support_required.findIndex(x => x.label == 'Other');
        var supportOther = false;
        if (supportIndex > - 1) {
            supportOther = (this.state.support_required[supportIndex].active) ? true : false;
        }
        return (
            <div>
                <Modal
                    className="modal fade"
                    bsSize="large"
                    show={this.props.update}
                    onHide={this.handleHide}
                    container={this}
                    aria-labelledby="contained-modal-title"
                >
                    <Modal.Body>
                        <form id="update_requirement">
                            <div className="container-fluid Red">

                                <div className="row">
                                    <div className="col-lg-12  P_15_b bb-1">
                                        <h2 className="color">Participant Care Focus Points: <a className="close_i pull-right mt-1" onClick={this.props.cloaseModel}><i className="icon icon-cross-icons"></i></a></h2>
                                    </div>
                                </div>

                                <div className="row P_25_T">
                                    <div className="col-md-5 ">
                                        <label className="">Requires Assistance:</label>
                                        <span className="required">
                                            <label htmlFor="assistance[]" className="error " style={{display: "block", width: "100%;"}} ></label>
                                            <div className="Schedules_Multiple_checkbox overflow-hidden">
                                                <div className="scroll_active_modal">
                                                    {this.state.require_assistance.map((req, idx) => (
                                                        <span key={idx + 1} className="w_50  mb-2">
                                                            <input type='checkbox' name="assistance[]" className="checkbox1" onChange={(e) => this.setMutipleCheckbox(e, idx, 'require_assistance')} checked={req.active}  data-rule-required="true" data-msg-required="Please select at least one require assistance" />
                                                            <label>
                                                                <div className="d_table-cell"><span onClick={(e) => this.setMutipleCheckbox(e, idx, 'require_assistance')}></span></div><div className="d_table-cell">{req.label}</div></label>
                                                        </span>
                                                    ))}
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>


                                <div className="row P_25_T">
                                    <div className="col-md-2">
                                        <label>Cognition:</label>
                                        <span className="required">
                                            <Select clearable={false} className="custom_select" name="participantCognition" simpleValue={true} required={true} searchable={false} value={this.state['cognition']} onChange={(e) => this.selectChange(e, 'cognition')}
                                                options={cognitionDropdown(0)} placeholder="Please Select" />

                                        </span>
                                    </div>
                                    <div className="col-md-3">
                                        <label>Communication:</label>
                                        <span className="required">
                                            <Select clearable={false} className="custom_select" name="participantCommunication" simpleValue={true} required={true} searchable={false} value={this.state['communication']} onChange={(e) => this.selectChange(e, 'communication')}
                                                options={communicationDropdown(0)} placeholder="Please Select" />

                                        </span>

                                    </div>
                                    {requireOther ?
                                        <div className="col-md-5 col-md-offset-1">
                                            <label>If 'Other' Please Describe:</label>
                                            <input required="true" onChange={this.handleChange} type="text" value={this.state['support_require_other']} name="support_require_other" placeholder="Short descriptions separated by commas." />
                                        </div> : ''}
                                </div>


                                <div className="row P_25_T">
                                    <div className="col-md-12 P_15_TB py-3 by-1"><h3 className="color">Participant Language(s):</h3></div>
                                </div>

                                <div className="row P_25_T">
                                    <div className="col-md-2">
                                        <label>English:</label>
                                        <span className="required">
                                            <Select clearable={false} className="custom_select" name="participantenglish" simpleValue={true} required={true} searchable={false} value={this.state['english']} onChange={(e) => this.selectChange(e, 'english')}
                                                options={this.English_option} placeholder="Please Select" />
                                        </span>
                                    </div>
                                    <div className="col-md-3">
                                        <label>Preferred Language:</label>
                                        <span className="required">
                                            <Select clearable={false} className="custom_select" name="participantPreferredlang" simpleValue={true} required={true} searchable={false} value={this.state['preferred_language']} onChange={(e) => this.selectChange(e, 'preferred_language')}
                                                options={prefterLanguageDropdown(0)} placeholder="Please Select" disabled={(this.state.english == 1) ? true : false} />

                                        </span>
                                    </div>
                                    <div className="col-md-3">
                                        <label>Linguistic Interpreter:</label>
                                        <span >
                                            <Select clearable={false} className="custom_select" name="participantLinguistic" simpleValue={true} required={true} searchable={false} value={this.state['linguistic_interpreter']} onChange={(e) => this.selectChange(e, 'linguistic_interpreter')}
                                                options={interpretertDropdown(0, 'clearable')} placeholder="Please Select" />

                                        </span>
                                    </div>
                                    <div className="col-md-4">
                                        <label>Hearing Impaired Interpreter:</label>
                                        <div className="col-md-9">
                                            <div className="row">
                                                <span>
                                                    <Select clearable={false} className="custom_select" simpleValue={true} name="participantHearing" required={true} searchable={false} value={this.state['hearing_interpreter']} onChange={(e) => this.selectChange(e, 'hearing_interpreter')}
                                                        options={HearingOption()} placeholder="Please Select" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div className="row P_25_T">
                                    <div className="col-md-12 P_15_TB  px-0 by-1"><h3 className="color">Participant Shift Requirements:</h3></div>
                                </div>


                                <div className="row P_25_T">
                                    <div className="col-md-5 ">
                                        <label className="">Services Required:</label>
                                        <span className="required">
                                            <label htmlFor="oc_service[]" className="error " style={{display: "block", width: "100%;"}} ></label>
                                            <div className="multiple_checkbox radi_2 overflow-hidden px-0 py-0">
                                                <div className="scroll_active_modal">
                                                    {this.state.oc_service.map((req, idx) => (
                                                        <span key={idx + 1} className="w_50 d-inline-block pb-2">
                                                            <input type='checkbox' name="oc_service[]" className="checkbox1" onChange={(e) => this.setMutipleCheckbox(e, idx, 'oc_service')} checked={req.active} data-rule-required="true" data-msg-required="Please select at least one oc service" />
                                                            <label>
                                                                <div className="d_table-cell"><span onClick={(e) => this.setMutipleCheckbox(e, idx, 'oc_service')}></span></div><div className="d_table-cell">{req.label}</div></label>
                                                        </span>
                                                    ))}

                                                </div>
                                            </div>
                                        </span>

                                    </div>
                                    <div className="col-md-5 col-md-offset-1 textarea_height ">
                                        <label>Support Required:</label>
                                        <span className="required">
                                            <label htmlFor="support[]" className="error " style={{display: "block", width: "100%;"}} ></label>
                                            <div className="multiple_checkbox radi_2 overflow-hidden px-0 py-0">
                                                <div className="scroll_active_modal">
                                                    {this.state.support_required.map((req, idx) => (
                                                        <span key={idx + 1} className="w_50 d-inline-block pb-2">
                                                            <input type='checkbox' name="support[]" className="checkbox1" onChange={(e) => this.setMutipleCheckbox(e, idx, 'support_required')} checked={req.active} data-rule-required="true" data-msg-required="Please select at least one support required" />
                                                            <label>
                                                                <div className="d_table-cell"><span onClick={(e) => this.setMutipleCheckbox(e, idx, 'support_required')}></span></div><div className="d_table-cell">{req.label}</div></label>
                                                        </span>
                                                    ))}
                                                </div>
                                            </div>
                                        </span>


                                        {supportOther ? <React.Fragment>
                                            <label className="P_25_T">If 'Other' Please Describe:</label>
                                            <input type="text" required="true" onChange={this.handleChange} name="require_assistance_other" value={this.state['require_assistance_other']} placeholder="Short descriptions separated by commas." /></React.Fragment> : ''}
                                    </div>

                                </div>


                                <div className="row P_25_T">
                                    <div className="col-md-12 P_15_TB px-0 by-1"><h3 className="color">Carers (Members) NOT to Book</h3></div>
                                </div>
                                
                                
                    <span className="">

                        <div className="row P_25_T">

                        <div className="col-lg-3 col-md-3">
                                <label><span className="gendar_txt_">Male</span></label><br />
                                <span className="pull-left">
                        <input type="checkbox" className="checkbox2" id="a3" name="all_option_male" onChange={(e) => this.checkCarersNotBookOption(e,'Ethnicity_male_option','Religious_male_option','all_option_male')} checked={this.state['all_option_male'] }  />
                                    <label htmlFor="a3"><span></span>Select all options for Males</label>
                                </span>
                            </div>
                            <div className="col-lg-9 col-md-9">
                                <div className="row">
                                <div className="col-md-5 col-md-offset-1">
                                        <label className="">Ethnicity:</label>
                                        <span className="required">
                                            <label htmlFor="select_ethnicity[]" className="error CheckieError" style={{ display: "block", width: "100%;" }} ></label>
                                            <div className="Schedules_Multiple_checkbox overflow-hidden">
                                                <div className="scroll_active_modal">
                                                    {this.state.Ethnicity_male_option.map((req, idx) => (
                                                        <span key={idx + 1} className="w_50 w-100 mb-2">
                            <input type='checkbox' name="select_ethnicity[]" className="checkbox1" data-rule-required="true" onChange={(e) => this.setMutipleCheckbox(e, idx, 'Ethnicity_male_option','male')} checked={req.active || ''} data-rule-required="true" data-msg-required="Please select at least one Ethnicity" />
                                                            <label>
                            <div className="d_table-cell"> <span onClick={(e) => this.setMutipleCheckbox(e, idx, 'Ethnicity_male_option','male')}></span></div>
                                                                <div className="d_table-cell"> {req.label}</div></label>
                                                        </span>
                                                    ))}
                                                </div>
                                            </div>
                                        </span>
                                    </div>

                                    <div className="col-md-5 col-md-offset-1">
                                        <label className="">Religion Beliefs :</label>
                                        <span className="required">
                                            <label htmlFor="select_religious[]" className="error CheckieError" style={{ display: "block", width: "100%;" }} ></label>
                                            <div className="Schedules_Multiple_checkbox overflow-hidden">
                                                <div className="scroll_active_modal">
                                                    {this.state.Religious_male_option.map((req, idx) => (
                                                        <span key={idx + 1} className="w_50 w-100 mb-2">
                                <input type='checkbox' name="select_religious[]" className="checkbox1" data-rule-required="true" onChange={(e) => this.setMutipleCheckbox(e, idx, 'Religious_male_option','male')} checked={req.active || ''} data-rule-required="true" data-msg-required="Please select at least one Religion Beliefs" />
                                                            <label>
                                <div className="d_table-cell"> <span onClick={(e) => this.setMutipleCheckbox(e, idx, 'Religious_male_option','male')}></span></div>
                                                                <div className="d_table-cell"> {req.label}</div></label>
                                                        </span>
                                                    ))}
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </span>

                    <span className="">

                        <div className="row P_25_T">

                        <div className="col-lg-3 col-md-3">
                        <label><span className="gendar_txt_">Female</span></label><br />
                                <span className="pull-left">
                                <input type="checkbox" className="checkbox2" id="a4" name="all_option_female" onChange={(e) => this.checkCarersNotBookOption(e,'Ethnicity_female_option','Religious_female_option','all_option_female')} checked={this.state['all_option_female'] }   data-msg-required="Please select at least one Ethnicity"/>
                                <label htmlFor="a4"><span></span>Select all options for Female</label>
                                </span>

                            </div>

                            <div className="col-lg-9  col-md-9">
                                <div className="row">
                                    <div className="col-md-5  col-md-offset-1">
                                        <label className="">Ethnicity:</label>
                                        <span className="required">
                                <label htmlFor="Ethnicity_female_option[]" className="error CheckieError" style={{display: "block", width: "100%;"}} ></label>
                                            <div className="Schedules_Multiple_checkbox overflow-hidden">
                                                <div className="scroll_active_modal">
                                {this.state.Ethnicity_female_option.map((req, idx) => (
                                                        <span key={idx + 1} className="w_50 w-100 mb-2">
                                    <input type='checkbox' name="Ethnicity_female_option[]" className="checkbox1" onChange={(e) => this.setMutipleCheckbox(e, idx, 'Ethnicity_female_option','female')} checked={req.active || ''}  data-rule-required="true" data-msg-required="Please select at least one Ethnicity" />
                                                            <label>
                                    <div className="d_table-cell"> <span onClick={(e) => this.setMutipleCheckbox(e, idx, 'Ethnicity_female_option','female')}></span></div>
                                                                <div className="d_table-cell"> {req.label}</div></label>
                                                        </span>
                                                    ))}
                                                </div>
                                            </div>
                                        </span>
                                    </div>

                                    <div className="col-md-5 col-md-offset-1">
                                        <label className="">Religion Beliefs :</label>
                                        <span className="required">
                                    <label htmlFor="Religious_female_option[]" className="error CheckieError" style={{display: "block", width: "100%;"}} ></label>
                                            <div className="Schedules_Multiple_checkbox overflow-hidden">
                                                <div className="scroll_active_modal">
                                    {this.state.Religious_female_option.map((req, idx) => (
                                                        <span key={idx + 1} className="w_50 w-100 mb-2">
                                        <input type='checkbox' name="Religious_female_option[]" className="checkbox1" onChange={(e) => this.setMutipleCheckbox(e, idx, 'Religious_female_option','female')} checked={req.active || ''} data-rule-required="true" data-msg-required="Please select at least one Religion Beliefs" />
                                                            <label>
                                        <div className="d_table-cell"> <span onClick={(e) => this.setMutipleCheckbox(e, idx, 'Religious_female_option','female')}></span></div>
                                                                <div className="d_table-cell"> {req.label}</div></label>
                                                        </span>
                                                    ))}
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </span>


                                <div className="row P_25_T">
                                    <div className="col-md-3 col-md-offset-9">
                                        <button type="submit" disabled={this.state.loading} onClick={this.onSubmit} className="but" >Update requirement</button></div>
                                </div>
                            </div>
                        </form>

                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}


