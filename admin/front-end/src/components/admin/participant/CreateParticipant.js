import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import jQuery from "jquery";
import ReactGoogleAutocomplete from './../externl_component/ReactGoogleAutocomplete';
import '../../../service/jquery.validate.js';
import { ROUTER_PATH, BASE_URL }
    from '../../../config.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import Header from '../../../components/admin/Header';
import Footer from '../../../components/admin/Footer';
import { postData, getOptionsSuburb, handleAddShareholder }
    from '../../../service/common.js';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { relationDropdown, sitCategoryListDropdown, ocDepartmentDropdown, getAboriginalOrTSI, LivingSituationOption } from '../../../dropdown/ParticipantDropdown.js';
import 'react-datepicker/dist/react-datepicker.css';

const formId = 'create_participant_'+parseInt((Math.random() * (10 - 20000) + 20000));
//const formId = 'create_participant';
           
class CreateParticipant extends Component {
    constructor(props) {
        super(props);

        this.kindetails = [{ name: '', lastname: '', contact: '', email: '', relation: '', relation_error: '', name_error: '', lastname_error: '', contact_error: '', email_error: '' }];

        this.state = {
            ParticipantSituation: 1,
            Preferredcontacttype: 1,
            ParticipantTSI: 1,
            second_step: false,
            PhoneInput: [{ name: '' }],
            EmailInput: [{ name: '' }],
            AddressInput: [{ name: '', site_category: '', state: '', pincode: '', state_error: false }],
            kindetails: [{ name: '', lastname: '', contact: '', email: '', relation: '' }],
            bookerdetails: [{ name: '', lastname: '', contact: '', email: '', relation: '' }],
            success: false,
            participantReferral: [{ value: 1, label: 'Yes' }, { value: 2, label: 'No' }],
            gender_option: [{ value: 1, label: 'Male' }, { value: 2, label: 'Female' }],
            department_option: [{ value: 1, label: 'OCS' }, { value: 2, label: 'On Call system' }],
            contact_option: [{ value: 1, label: 'Phone' }, { value: 2, label: 'Email' }],
            to_org_option: [{ value: 1, label: 'To Org' }, { value: 2, label: 'To House' }],
            stateList: [],
            referral: 2
        }


    }
    submit = (e) => {
        e.preventDefault();
        var custom_validate = this.custom_validation({ errorClass: 'tooltip-default' });
      
        var validator = jQuery("#"+ formId).validate({   
            rules: {
              Dob: { required: true},
              username: {
                  required : true,
                  synchronousremote: {url: BASE_URL + 'participant/ParticipantDashboard/check_participant_username_already_exist', type: "get"},
              }
            }
         });

        if (!this.state.loading && jQuery("#"+ formId).valid() && custom_validate) {
            var str = JSON.stringify(this.state);

            sessionStorage.setItem("participant_step_1", str);
            this.setState({ success: true })

        } else {
            validator.focusInvalid();
        }
    }
    handleChange = (e) => {
        var state = {};
        this.setState({ error: '' });
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value.replace(/\s/g, ' '));
        this.setState(state);
    }

    selectChange = (selectedOption, fieldname) => {
        var state = {};
        state[fieldname] = selectedOption;
        state[fieldname + '_error'] = false;
        this.setState(state);
    }

    handleShareholderNameChange = (index, stateKey, fieldtkey, fieldValue) => {
        var state = {};
        var tempField = {};
        var List = this.state[stateKey];
        List[index][fieldtkey] = fieldValue

        
        if (fieldtkey == 'state') {
            List[index]['city'] = {}
            List[index]['postal'] = ''
            List[index]['state_error'] = false;
        }

        if (fieldtkey == 'city' && fieldValue) {
            List[index]['postal'] = fieldValue.postcode
        }

        state[stateKey] = List;
        this.setState(state);
    }
    
    googleAddressFill = (index, stateKey, fieldtkey, fieldValue) => {
         if (fieldtkey == 'street') {
         var componentForm = {street_number: 'short_name', route: 'long_name',locality: 'long_name',administrative_area_level_1: 'short_name', postal_code: 'short_name'};
         var addess_key = {street: ''};

         console.log(fieldValue);
         for (var i = 0; i < fieldValue.address_components.length; i++) {
            var addressType = fieldValue.address_components[i].types[0];
            if (componentForm[addressType]) {
              var val = fieldValue.address_components[i][componentForm[addressType]];
              
              if(addressType === 'route'){
                   addess_key['street'] = addess_key['street'] +' '+ val;
              }else if(addressType === 'street_number'){
                   addess_key['street'] =  val;
              }else if(addressType === 'locality'){
                   addess_key['city'] =  {value: val, label: val};
              }else if(addressType === 'administrative_area_level_1'){
                   var t_index = this.state.stateList.findIndex(x => x.label == val);
                   addess_key['state'] =  this.state.stateList[t_index].value;
              }else if(addressType === 'postal_code'){
                   addess_key['postal'] =  val;
              }
            }
          }
           
          var List = this.state[stateKey];
    
          List[index] = addess_key;
          var state = {};
          state[stateKey] = List;
          this.setState(state);
           
        }
    }

    handleAddShareholder = (e, tagType) => {
        e.preventDefault();
        var state = [];
        state[tagType] = this.state[tagType].concat([{ name: '', lastname: '', contact: '', email: '', relation: '' }]);
        this.setState(state);
    }

    handleRemoveShareholder = (e, idx, tagType) => {
        e.preventDefault();
        var state = {};
        var List = this.state[tagType];

        state[tagType] = List.filter((s, sidx) => idx !== sidx);
        this.setState(state);
    }

    sameAsReferral = (evt, tagType, referral_type) => {
        var state = {};
        var List = this.state[tagType];
        if (evt.target.checked) {
            List[0].name = this.state[referral_type];
        }
        else {
            List[0].name = '';
        }

        state[tagType] = List;
        state[evt.target.name] = evt.target.checked;
        this.setState(state);

        this.setState(state);
    }

    referralChange = (val) => {
        this.setState({ referral: val, 'referral_error': false });
        if (val == 2) {
            this.setState({ refferalRelation: false });
            this.setState({ refferalRelation: '', Referrer_first_name: '', Referrer_last_name: '', Referrer_phone: '', Referrer_email: '', check_preferred_phone: false, check_preferred_email: false })
        }
    }

    componentDidMount() {
        if (sessionStorage.getItem("participant_step_2") && this.props.props.location.state) {
            this.setState({ loading: true });
            var firstTempData = JSON.parse(sessionStorage.getItem("participant_step_1"));
            this.setState(firstTempData);
            this.setState({ loading: false, second_step: true });
        } else {
            this.setState({ loading: true });
            postData('participant/ParticipantDashboard/get_state', []).then((result) => {
                if (result.status) {
                    this.setState({ stateList: result.data });
                }
                this.setState({ loading: false });
            });
        }


    }

    custom_validation = () => {
        var return_var = true;
        var state = {};
        var List = [{ key: 'referral' }, { key: 'gender' }, { key: 'Preferredcontacttype' }, { key: 'Preferredcontacttype' }, { key: 'ParticipantTSI' }, { key: 'refferalRelation' }];
        List.map((object, sidx) => {
            if (object.key == 'refferalRelation') {

                if (this.state['referral'] == 1 && (this.state['refferalRelation'] == undefined || this.state['refferalRelation'] == '')) {
                    state[object.key + '_error'] = true;
                    this.setState(state);
                    return_var = false;
                }
            } else if (this.state[object.key] == null) {
                state[object.key + '_error'] = true;
                this.setState(state);
                return_var = false;
            }
        });

        const newShareholders = this.state.AddressInput.map((object, sidx) => {
            if (object.state == '') {
                return_var = false;
                return { ...object, state_error: true };
            } else {
                return object;
            }
        });
        this.setState({ AddressInput: newShareholders });

        return return_var;
    }

    errorShowInTooltip = ($key, msg) => {
        return (this.state[$key + '_error']) ? <div className={'tooltip custom-tooltip fade top in' + ((this.state[$key + '_error']) ? ' select-validation-error' : '')} role="tooltip">
            <div className="tooltip-arrow"></div><div className="tooltip-inner">{msg}.</div></div> : '';

    }

    errorShowInTooltipForLoop = (key, msg) => {
        return (key == true) ? <div className={'tooltip custom-tooltip fade top in' + ((key == true) ? ' select-validation-error' : '')} role="tooltip">
            <div className="tooltip-arrow"></div><div className="tooltip-inner">{msg}.</div></div> : '';

    }

    render() {

        return (
            <React.Fragment>
                {(this.state.success) ? <Redirect to={{ pathname: '/admin/participant/create_step_second', state: { first_step: true, second_step: this.state.second_step } }} /> : ''}
                   <form id={formId}>
                      <div className="row  _Common_back_a">
                                <div className="col-lg-10 col-lg-offset-1 col-md-12"><Link className="d-inline-flex" to={ROUTER_PATH + 'admin/participant/dashboard'}><div className="icon icon-back-arrow back_arrow"></div></Link></div>
                            </div>

                            <div className="row"><div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div></div>

                            <div className="row  _Common_He_a">
                                <div className="col-lg-10 col-lg-offset-1 col-md-12 text-left">
                                    <h1 className="color">Creating New Participant</h1>
                                </div>
                            </div>

                            <div className="row"><div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div></div>

                            <div className="row P_25_TB">
                                <div className="col-lg-1"></div>
                                <div className="col-lg-2 col-md-3 col-xs-12">
                                    <label>User Name:</label>
                                    <span className="required">
                                        <input data-rule-required='true' data-msg-required="Add Username" placeholder="User Name" type="text" name="username" value={this.state['username'] || ''} onChange={(e) => this.setState({ username: e.target.value.replace(/\s/g, '') })}  data-msg-synchronousremote="This username already exist" maxLength="25" />
                                    </span>
                                </div>
                                <div className="col-lg-2 col-md-3 col-xs-12">
                                <label style={{visibility:"hidden"}}>First Name:</label>
                                    <span className="required">
                                        <input data-rule-required='true' data-msg-required="Add First Name" placeholder="First Name" type="text" name="firstname" value={this.state['firstname'] || ''} onChange={this.handleChange} maxLength="30" />
                                    </span>
                                </div>
                                <div className="col-lg-2 col-md-3 col-xs-12">
                                <label style={{visibility:"hidden"}}>Middle  Name</label>
                                    <input placeholder="Middle Name" type="text" name="middlename" value={this.state['middlename'] || ''} onChange={this.handleChange} maxLength="30" />

                                </div>
                                <div className="col-lg-2 col-md-3 col-xs-12">
                                <label style={{visibility:"hidden"}}>Last Name</label>
                                    <span className="required">
                                        <input placeholder="Last Name" data-msg-required="Add Last Name" type="text" name="lastname" value={this.state['lastname'] || ''} onChange={this.handleChange} data-rule-required="true" maxLength="30" />
                                    </span>
                                </div>
                                <div className="col-lg-1 col-md-2 col-xs-12">
                                    <label>Gender</label>
                                    <span className="required">
                                        <Select name="gender" clearable={false} className="custom_select" simpleValue={true} required={true} value={this.state['gender'] || ''} onChange={(e) => this.selectChange(e, 'gender')} options={this.state.gender_option}
                                            searchable={false} placeholder="No Preference" />
                                        {this.errorShowInTooltip('gender', 'Add Gender')}
                                    </span>

                                </div>
                                <div className="col-lg-1 col-md-3 col-xs-12">
                                    <label>D.O.B</label>
                                    <span className="required">
                                        <DatePicker showYearDropdown scrollableYearDropdown yearDropdownItemNumber={110} dateFormat="DD/MM/YYYY" required={true} data-placement={'bottom'} maxDate={moment()}
                                            name="Dob" onChange={(date) => this.setState({ Dob: date })} selected={this.state['Dob'] ? moment(this.state['Dob'], 'DD-MM-YYYY') : null} className="text-center px-0" placeholderText="DD/MM/YYYY" />

                                    </span>
                                </div>
                                <div className="col-lg-1"></div>
                            </div>

                            <div className="row">
                                <div className="col-lg-1 col-lg-offset-1 col-md-2 col-xs-12">
                                    <label>Referrer:</label>
                                    <span className="required">
                                        <Select className="custom_select" name="referral" required={true} simpleValue={true} clearable={false} searchable={false} value={this.state['referral'] || ''} onChange={(e) => this.referralChange(e)} options={this.state.participantReferral} placeholder="Referral" />
                                        {this.errorShowInTooltip('referral', 'Select Referral')}
                                    </span>
                                </div>
                                {(this.state.referral == 1) ?
                                    <div>
                                        <div className="col-lg-1 col-md-2">
                                            <label>Relation:</label>
                                            <span className="required">
                                                <Select className="custom_select" name="refferalRelation" simpleValue={true} required={true} simpleValue={true} clearable={false} searchable={false} onChange={(e) => this.selectChange(e, 'refferalRelation')}
                                                    options={relationDropdown(0)} placeholder="Relation" value={this.state['refferalRelation']} disabled={(this.state['referral'] == 1) ? false : true} />
                                                {this.errorShowInTooltip('refferalRelation', 'Select Relation')}
                                            </span>
                                        </div>
                                        <div className="col-lg-2 col-md-3 col-xs-12">
                                            <label>Referrer: {/* First Name */}</label>
                                            <span className="required">
                                                <input placeholder="First name" data-msg-required="Add First Name" type="text" value={this.state['Referrer_first_name'] || ''} name="Referrer_first_name" onChange={this.handleChange} disabled={(this.state['referral'] == 1) ? false : true} data-rule-required="true" maxLength="30" />
                                            </span>
                                        </div>
                                        <div className="col-lg-2 col-md-3 col-xs-12">
                                            <label style={{visibility:"hidden"}}>Last Name</label>
                                            <span className="required">
                                                <input placeholder="Last Name" data-msg-required="Add Last Name" type="text" value={this.state['Referrer_last_name'] || ''} name="Referrer_last_name" onChange={this.handleChange} disabled={(this.state['referral'] == 1) ? false : true} data-rule-required="true" maxLength="30" />
                                            </span>
                                        </div>
                                        <div className="col-lg-2 col-md-3 col-xs-12">
                                            <label>Phone</label>
                                            <span className="required">
                                                <input placeholder="Phone" type="text" data-msg-required="Add Phone Number" data-rule-phonenumber name="Referrer_phone" value={this.state['Referrer_phone'] || ''} onChange={this.handleChange} disabled={(this.state['referral'] == 1) ? false : true} data-rule-required="true" maxLength="30" maxLength="30" />
                                            </span>
                                        </div>
                                        <div className="col-lg-2 col-md-3 col-xs-12">
                                            <label>Email</label>
                                            <span className="required">
                                                <input placeholder="Email" type="text" data-msg-required="Add Email Address" name="Referrer_email" value={this.state['Referrer_email'] || ''} onChange={this.handleChange} disabled={(this.state['referral'] == 1) ? false : true} data-rule-required="true" data-rule-email="true" maxLength="70" />
                                            </span>
                                        </div>
                                    </div> : ''}
                            </div>

                            <div className="row">
                                <div className="col-lg-10 col-lg-offset-1 col-md-12 mt-5"><div className="bor_T"></div></div>
                                <div className="col-lg-10 col-lg-offset-1 col-md-12 P_15_TB">
                                    <h3 className="color">New Participant Personal Details:</h3>
                                </div>
                                <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_B"></div></div>
                            </div>

                            <div className="row P_25_T">
                                <div className="col-lg-2 col-md-3 col-xs-12 col-lg-offset-1">
                                    <label>Preferred Name:</label>
                                    <input type="text" placeholder="Preferred Name" value={this.state['Preferredname'] || ''} name="Preferredname" onChange={this.handleChange} maxLength="30" />
                                </div>

                                <div className="col-lg-2 col-md-2">
                                    <label>NDIS #:</label>
                                    <input type="text" placeholder="000 000 000" name="Preferredndis" value={this.state['Preferredndis'] || ''} onChange={this.handleChange} maxLength="30" />
                                </div>
                                <div className="col-lg-2 col-md-3 col-xs-12">
                                    <label>Medicare #:</label>

                                    <input type="text" placeholder="0000 00000 0" name="Preferredmedicare" value={this.state['Preferredmedicare'] || ''} onChange={this.handleChange} maxLength="30" />

                                </div>
                                <div className="col-lg-2 col-md-2">
                                    <label>CRN:</label>
                                    <input type="text" placeholder="000000" name="Preferredcrn" value={this.state['Preferredcrn'] || ''} onChange={this.handleChange} maxLength="30" />
                                </div>
                            </div>
                            <div className="row P_25_T">
                                <div className="col-lg-2 col-lg-offset-1 col-md-3 col-xs-12">
                                    <label>Phone (Primary):</label>
                                    <span className="required">
                                        {this.state.PhoneInput.map((PhoneInput, idx) => (
                                            <div className="input_plus__ mb-1" key={idx + 1}>
                                                <input className="input_f" type="text"
                                                    value={PhoneInput.name}
                                                    name={'phone_primary' + idx}
                                                    placeholder="Can include area code"
                                                    onChange={(evt) => this.handleShareholderNameChange(idx, 'PhoneInput', 'name', evt.target.value)}
                                                    data-rule-required="true"
                                                    maxLength="18"
                                                    data-msg-required="Add Phone Number"
                                                    data-rule-phonenumber
                                                />

                                                {idx > 0 ? <button onClick={(e) => this.handleRemoveShareholder(e, idx, 'PhoneInput')}>
                                                    <i className="icon icon-decrease-icon Add-2" ></i>
                                                </button> : (this.state.PhoneInput.length == 2) ? '' : <button onClick={(e) => handleAddShareholder(this, e, 'PhoneInput', PhoneInput)}>
                                                    <i className="icon icon-add-icons  Add-1" ></i>
                                                </button>}
                                            </div>
                                        ))}
                                    </span>
                                    {(this.state.referral == 1) ?
                                        <div className="P_15_T right_align_C">
                                            <span className="pull-right">
                                                <input type="checkbox" className="checkbox2" id="a3" name="check_preferred_phone" onChange={(e) => this.sameAsReferral(e, 'PhoneInput', 'Referrer_phone')} checked={this.state['check_preferred_phone']} disabled={(this.state['referral'] == 2) ? true : false} />
                                                <label htmlFor="a3"><span></span>Same as Referrer</label>
                                            </span>
                                        </div> : ''}
                                </div>


                                <div className="col-lg-2 col-md-3 col-xs-12">
                                    <label>Email</label>

                                    <span className="required">
                                        {this.state.EmailInput.map((EmailInput, idx) => (
                                            <div className="input_plus__ mb-1" key={idx + 1}>
                                                <input className="input_f distinctEmail" type="email"
                                                    value={EmailInput.name || ''}
                                                    name={'email_' + idx}
                                                    ref="name"
                                                    placeholder={idx > 0 ? 'Secondary Email' : 'Primary Email'}
                                                    onChange={(evt) => this.handleShareholderNameChange(idx, 'EmailInput', 'name', evt.target.value)}
                                                    data-rule-required="true"
                                                    data-rule-email="true"
                                                    data-msg-required="Add Email Address"
                                                    maxLength="70"
                                                />
                                                {idx > 0 ? <button onClick={(e) => this.handleRemoveShareholder(e, idx, 'EmailInput')}>
                                                    <i className="icon icon-decrease-icon Add-2" ></i>
                                                </button> : (this.state.EmailInput.length == 2) ? '' : <button onClick={(e) => handleAddShareholder(this, e, 'EmailInput', EmailInput)}>
                                                    <i className="icon icon-add-icons Add-1" ></i>
                                                </button>}
                                            </div>
                                        ))}
                                    </span>
                                    {(this.state.referral == 1) ?
                                        <div className="P_15_T right_align_C">
                                            <span className="pull-right">
                                                <input type="checkbox" className="checkbox2" id="a4" name="check_preferred_email" onChange={(e) => this.sameAsReferral(e, 'EmailInput', 'Referrer_email')} checked={this.state['check_preferred_email']} disabled={(this.state['referral'] == 2) ? true : false} />
                                                <label htmlFor="a4"><span></span>Same as Referrer</label>
                                            </span>
                                        </div> : ''}
                                </div>
                                <div className="col-lg-2 col-md-3 col-xs-12">
                                    <label>Preferred Contact:</label>
                                    <span className="required">
                                        <Select className="custom_select" clearable={false} required={true} simpleValue={true} name="Preferredcontacttype" onChange={(e) => this.selectChange(e, 'Preferredcontacttype')} value={this.state['Preferredcontacttype']} searchable={false} options={this.state.contact_option} placeholder="Contact" data-rule-required="true" />
                                        {this.errorShowInTooltip('Preferredcontacttype', 'Select Preferred Contact')}
                                    </span>
                                </div>
                            </div>
                            <div className="row P_15_T">
                           
                                <div className="col-lg-10 col-lg-offset-1 col-md-12">
                                    <div className="row">
                                        {this.state.AddressInput.map((AddressInput, idx) => (
                                            <div key={idx + 1} className="col-lg-6 col-md-6 padding-top-50">
                                                <div className="row">
                                                    <div className="col-lg-7 col-md-7">
                                                        <label>{idx > 0 ? 'Address (Secondray):' : 'Address (Primary):'} </label>
                                                        <span key={idx + 1}>
                                                            <span className="required">
                                                                <div className="add_input mb-1" key={idx + 1}>
                                                                    
                                                                    <ReactGoogleAutocomplete className="add_input mb-1" key={idx + 1}
                                                                        required={true}
                                                                        data-msg-required="Add address"
                                                                        name={"address_primary" + idx}
                                                                        onPlaceSelected={(place) => this.googleAddressFill(idx, 'AddressInput', 'street', place)}     
                                                                        types={['address']}
                                                                        returnType={'array'}
                                                                        value={AddressInput.street || ''}
                                                                        onChange={(evt) => this.handleShareholderNameChange(idx, 'AddressInput', 'street', evt.target.value)}
                                                                        onKeyDown={(evt) => this.handleShareholderNameChange(idx, 'AddressInput', 'street', evt.target.value)}
                                                                        componentRestrictions={{country: "au"}}
                                                                      />
                                                                      
                                                                </div>
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <div className="col-lg-5 col-md-5">
                                                        <label>{idx > 0 ? ' Site Category:' : ' Site Category:'} </label>
                                                        <Select clearable={false} searchable={false}
                                                            simpleValue={true}
                                                            value={AddressInput.site_category || ''}
                                                            name={'input_address_primary_department' + idx}
                                                            onChange={(evt) => this.handleShareholderNameChange(idx, 'AddressInput', 'site_category', evt)}
                                                            options={sitCategoryListDropdown(0)}
                                                            placeholder="Please Select" />
                                                    </div>
                                                </div>
                                                <div className="row P_15_T AL_flex">
                                                    <div className="col-md-3 col-xs-12">

                                                        <label>State:</label>
                                                        <span className="required">
                                                            <Select clearable={false}
                                                                className="custom_select"
                                                                simpleValue={true}
                                                                name={'select_state_primary' + idx}
                                                                searchable={false}
                                                                value={AddressInput.state || ''}
                                                                onChange={(evt) => this.handleShareholderNameChange(idx, 'AddressInput', 'state', evt)}
                                                                options={this.state.stateList}
                                                                placeholder="Please Select" />
                                                            {this.errorShowInTooltipForLoop(AddressInput.state_error, 'Select State')}
                                                        </span>
                                                    </div>
                                                    <div className="col-md-3 col-xs-12 modify_select">

                                                        <label>Suburb:</label>
                                                        <span className="required">
                                                            <Select.Async clearable={false}
                                                                className="default_validation" required={true}
                                                                value={AddressInput.city}
                                                                cache={false}
                                                                disabled={(AddressInput.state) ? false : true}
                                                                loadOptions={(val) => getOptionsSuburb(val, AddressInput.state)}
                                                                onChange={(evt) => this.handleShareholderNameChange(idx, 'AddressInput', 'city', evt)}
                                                                options={this.state.stateList} placeholder="Please Select" />
                                                        </span>
                                                    </div>

                                                    <div className="col-md-3 col-xs-12">
                                                        <label>Postcode:</label>
                                                        <span className="required">

                                                            <input className="input_f distinctEmail"
                                                                value={AddressInput.postal || ''}
                                                                name={'input_postcode_primary' + idx}
                                                                placeholder='0000'
                                                                onChange={(evt) => this.handleShareholderNameChange(idx, 'AddressInput', 'postal', evt.target.value)}
                                                                data-rule-required="true" data-rule-number="true" data-rule-postcodecheck="true"
                                                                data-msg-required="Add Postcode" data-msg-number="Please enter valid postcode" />
                                                        </span>
                                                    </div>
                                                    <div className="col-md-3 col-xs-12 d-inline-flex">
                                                        <label></label>


                                                        {idx > 0 ? <button className="button_plus__" onClick={(e) => this.handleRemoveShareholder(e, idx, 'AddressInput')}>
                                                            <i className="icon icon-decrease-icon Add-2-2" ></i>
                                                        </button> : (this.state.AddressInput.length == 3) ? '' : <button className="button_plus__"
                                                            onClick={(e) => handleAddShareholder(this, e, 'AddressInput', AddressInput)}>
                                                            <i className="icon icon-add-icons Add-2-1"></i>
                                                        </button>}

                                                    </div>

                                                </div>
                                            </div>
                                        ))}
                                    </div>
                                </div>
                            </div>



                            <RalativeDetails title="Next of Kin" stateKey="kindetails" kindetails={this.state.kindetails} errorShowInTooltipForLoop={this.errorShowInTooltipForLoop}
                                handleShareholderNameChange={this.handleShareholderNameChange}
                                handleRemoveShareholder={this.handleRemoveShareholder}
                                handleAddShareholder={this.handleAddShareholder}
                            />

                            <RalativeDetails title="Booker" stateKey="bookerdetails" kindetails={this.state.bookerdetails} errorShowInTooltipForLoop={this.errorShowInTooltipForLoop}
                                handleShareholderNameChange={this.handleShareholderNameChange}
                                handleRemoveShareholder={this.handleRemoveShareholder}
                                handleAddShareholder={this.handleAddShareholder}
                            />

                            <div className="row P_25_T">
                                <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                                <div className="col-lg-10 col-lg-offset-1 col-md-12 P_15_TB">
                                    <h3 className="color">Other Participant Details:</h3>
                                </div>
                                <div className="col-lg-1"></div>
                                <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                            </div>


                            <div className="row P_25_T">
                                <div className="col-lg-1"></div>
                                <div className="col-lg-2 col-md-3 col-xs-12">
                                    <label>Living Situation:</label>
                                    <span className="required">
                                        <Select className="custom_select" clearable={false} name="ParticipantSituation" simpleValue={true} onChange={(e) => this.selectChange(e, 'ParticipantSituation')} value={this.state['ParticipantSituation']} required={true} searchable={false}
                                            options={LivingSituationOption(0)} placeholder="" />
                                    </span>
                                </div>
                                <div className="col-lg-2 col-md-3 col-xs-12">
                                    <label>Aboriginal or TSI:</label>
                                    <span className="required">
                                        <Select className="custom_select" clearable={false} name="ParticipantTSI" simpleValue={true} value={this.state['ParticipantTSI']} required={true}
                                            searchable={false} onChange={(e) => this.selectChange(e, 'ParticipantTSI')} options={getAboriginalOrTSI()} placeholder="Unknown" />
                                    </span>
                                </div>
                            </div>

                            <div className="row P_25_T">
                                <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                                <div className="col-lg-10 col-lg-offset-1 col-md-12 P_15_TB">
                                    <h3 className="color">Assign New Participant:</h3>
                                </div>
                                <div className="col-lg-1"></div>
                                <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                            </div>

                            <div className="row P_25_T AL_flex">

                                <div className="col-lg-2 col-md-3 col-xs-12 col-lg-offset-1">
                                    <label>To Department:</label>
                                    <Select name="AssignOcDepartment" clearable={false} simpleValue={true} value={this.state['AssignOcDepartment']} required={true} searchable={false} onChange={(e) => this.selectChange(e, 'AssignOcDepartment')} options={ocDepartmentDropdown(0)} placeholder="Assign to Department" />
                                </div>
                                <div className="col-lg-2 col-md-3 col-xs-12">
                                    <label>To Org or House:</label>
                                    <Select name="AssignOcHouse" clearable={false} simpleValue={true} value={this.state['AssignOcHouse']} required={true} searchable={false} onChange={(e) => this.selectChange(e, 'AssignOcHouse')} options={this.state.to_org_option} placeholder="Assign to Org or House" />
                                </div>
                               
                            </div>
                            <div className="row P_25_T AL_flex">
                            <div className="col-lg-2  col-lg-offset-9 col-md-3 col-xs-12  col-md-offset-9 ">
                                    <label></label>
                                    <a href="#" className="but" onClick={this.submit}>Save and Continue</a>
                                </div>
                            </div>

                        </form>
                        </React.Fragment>
        );
    }
}
export default CreateParticipant;

class RalativeDetails extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="row P_25_T">
                    <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                    <div className="col-lg-10 col-lg-offset-1 col-md-12 P_15_TB"><h3 className="color">{this.props.title} Details:</h3></div><div className="col-lg-1"></div>
                    <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                </div>

                {this.props.kindetails.map((obj, index) => (
                    <div key={index + 1} className="P_25_T row AL_flex">
                        <div className="col-lg-2 col-lg-offset-1 col-md-3 col-xs-12">
                            <label>{this.props.title} Name:</label>
                        
                            <input className="input_f mb-1 "
                                value={obj.name || ''}
                                name={this.props.stateKey + 'input_kin_first_name' + index}
                                placeholder='First Name'
                                onChange={(evt) => this.props.handleShareholderNameChange(index, this.props.stateKey, 'name', evt.target.value)}
                                maxLength="30"
                                required={obj.lastname ? true : (obj.contact ? true : (obj.email ? true : (obj.relation ? true : false)))}
                            />

                        </div>

                        <div className="col-lg-2 col-md-3 col-xs-12 pt-4 mt-2">
                            <label></label>

                            <input className="input_f"
                                value={obj.lastname || ''}
                                name={this.props.stateKey + 'input_kin_lastname' + index}
                                placeholder='Last Name'
                                onChange={(evt) => this.props.handleShareholderNameChange(index, this.props.stateKey, 'lastname', evt.target.value)}
                                maxLength="30"
                                required={obj.name ? true : (obj.contact ? true : (obj.email ? true : (obj.relation ? true : false)))}
                            />

                        </div>
                        <div className="col-lg-2 col-md-3 col-xs-12">
                            <label>{this.props.title} Phone:</label>

                            <input className={"input_f distinctKinContact"}
                                value={obj.contact || ''}
                                name={this.props.stateKey + 'input_kin_contact' + index}
                                placeholder="Phone"
                                onChange={(evt) => this.props.handleShareholderNameChange(index, this.props.stateKey, 'contact', evt.target.value)}
                                maxLength="30"
                                data-rule-notequaltogroup={'[".distinctKinContact"]'}
                                data-msg-notequaltogroup='Please enter unique Phone number'
                                data-rule-phonenumber
                                required={obj.name ? true : (obj.lastname ? true : (obj.email ? true : (obj.relation ? true : false)))}
                            />

                        </div>
                        <div className="col-lg-2 col-md-3 col-xs-12 pt-4 mt-2">
                            <label></label>
                            <input className={"input_f distinctKinEmail"}
                                type="text"
                                value={obj.email || ''}
                                name={this.props.stateKey + 'input_kin_email' + index}
                                placeholder='Email'
                                maxLength="70"
                                data-rule-notequaltogroup={'[".distinctKinEmail"]'}
                                data-rule-email="true"
                                required={obj.name ? true : (obj.lastname ? true : (obj.contact ? true : (obj.relation ? true : false)))}
                                onChange={(evt) => this.props.handleShareholderNameChange(index, this.props.stateKey, 'email', evt.target.value)}
                            />

                        </div>

                        <div className="col-lg-2 col-md-3 col-xs-12">
                            <label>Relation:</label>
                            <div className="row AL_flex">
                                <div className="col-lg-9 col-md-9 col-xs-12">
                                    <span className="default_validation">
                                        <Select className=""
                                            clearable={false}
                                            searchable={false}
                                            simpleValue={true}
                                            value={obj.relation || ''}
                                            name={this.props.stateKey + 'input_relation_primary' + index}
                                            onChange={(evt) => this.props.handleShareholderNameChange(index, this.props.stateKey, 'relation', evt)}
                                            options={relationDropdown(0, 'clearable')}
                                            
                                            inputRenderer={() => <input className="define_input" name={"relation-kin"+ this.props.stateKey + index} required={obj.name ? true : (obj.lastname ? true : (obj.contact ? true : (obj.email ? true : false)))} value={obj.relation || ''} />}
                                            placeholder="Please Select" />

                                    </span>
                                </div>
                                <div className="col-lg-3 col-md-3 col-xs-12 d-inline-flex">
                                    {index > 0 ? <button className="button_plus__" onClick={(e) => this.props.handleRemoveShareholder(e, index, this.props.stateKey)}>
                                        <i className="icon icon-decrease-icon Add-2-2" ></i>
                                    </button> : (this.props.kindetails.length == 3) ? '' : <button className="button_plus__" onClick={(e) => this.props.handleAddShareholder(e, this.props.stateKey)}>
                                        <i className="icon icon-add-icons Add-2-1" ></i>
                                    </button>}
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
             </React.Fragment>
           
        )
    }
}   

