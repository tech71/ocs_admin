import React, { Component } from 'react';
import jQuery from "jquery";
import { ROUTER_PATH, BASE_URL } from '../../../config.js';
import { checkItsNotLoggedIn,postData,handleChangeChkboxInput,handleChangeSelectDatepicker,reFreashReactTable } from '../../../service/common.js';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import Header from '../../admin/Header';
import Footer from '../../admin/Footer';
import Panel from 'react-bootstrap/lib/Panel';
import moment from 'moment';
import Modal from 'react-bootstrap/lib/Modal';
import { ToastContainer, toast } from 'react-toastify';
import DatePicker from 'react-datepicker';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import {CSVLink} from 'react-csv';

class CaseMonitor extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        this.caseId = this.props.props.match.params.id;
        this.state = {  
              loading: false,  
              caseLog:[],
              case_detail:[],
        };
    }

    componentDidMount() {  
        this.get_case_detail();  
    }

    get_case_detail()
    {    
        var requestData = {case_id: this.props.props.match.params.id};
        this.setState({loading: true});
        postData('fms/FmsDashboard/get_case_detail', requestData).then((result) => {
            if (result.status) {
                this.setState({case_detail:result.data},()=>{ });
            } else {
                this.setState({error: result.error});
            }
             this.setState({loading: false});
        });
    }

    render() {
        return (
            <div>
                <section className="manage_top">
      <div className="container-fluid">
            <div className="row">
                  <div className="col-lg-10 col-lg-offset-1 col-md-12 P_15_TB"><a href={'/admin/fms/case/'+this.props.props.match.params.id}><span className="icon icon-back-arrow back_icons"></span></a></div>
                  <div className="col-lg-1"></div>
                  <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div>
            </div>
            <div className="row">
                  <div className="col-lg-10 col-lg-offset-1 col-md-12 P_15_TB text-center">
                    <h1 className="color">Case ID<span> - {this.state.case_detail.id}</span></h1>
                    <ul className="user_info P_15_T">
                        <li>Initiated by: <span>Yooralla West </span></li>
                        <li>ID: <span> {this.state.case_detail.id} </span></li>
                        <li>Initiator Name: <span> {this.state.case_detail.initiated_by}</span></li>
                        <li>Site: <span>Fawkner </span></li>
                    </ul>
                  </div>
                  <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div>
            </div>
            <div className="row P_20_TB">
                  <div className="col-lg-2 col-lg-offset-4  col-md-3 col-md-offset-3 filter_select">
                       <a className="moni_btn active">Respond</a>                      
                  </div>
                  <div className="col-lg-2 col-md-3  filter_select">
                    <a className="moni_btn">Monitor</a>                      
                  </div>
                  <div className="col-lg-offset-4"></div>
            </div>
            <div className="row P_45_T">
                  <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div>
            </div>
            <div className="row">

                  <div className="col-lg-4 col-md-2"></div>
                  <ul className="nav nav-tabs Category_tap col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-1 P_20_TB" role="tablist">
                        <li role="presentation" className="col-lg-4 col-md-6 active">
                              <a  href="#Logs" aria-controls="Logs" role="tab" data-toggle="tab">Logs</a>
                        </li>
                        <li role="presentation" className="col-lg-4 col-md-6">
                              <a  href="#Analysis" aria-controls="Analysis" role="tab" data-toggle="tab">Analysis</a>
                        </li>
                  </ul>
                  <div className="col-lg-3"></div>
                  <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div>
            </div>

            <div className="tab-content">
                <CaseLog caseId = {this.caseId} />
                <CaseAnalysis caseId = {this.caseId} />
            </div>
      </div>
</section>
            </div>
        );
    }
}
export default CaseMonitor;

//
const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {
        var url = 'fms/fmsDashboard/get_fms_log';
        var Request = JSON.stringify({pageSize: pageSize, page: page, sorted: sorted, filtered: filtered});
        postData(url, Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count),
                total_duration: result.total_duration
            };
            resolve(res);
        });

    });
};

//case Log
class CaseLog extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            caseLog:[],
        }
    }

    componentDidMount() { 
        this.setState({
        caseId:this.props.caseId,
        },()=>{ 
            
        });        
    } 

    fetchData = (state, instance) => {
        this.setState({loading: true});
        requestData(
                state.pageSize,
                state.page,
                state.sorted,
                state.filtered,
                ).then(res => {
            this.setState({
                caseLog: res.rows,
                pages: res.pages,
                loading: false,
            });
        })
    }

    searchBox = (key, value) => {
        var state = {}
        state[key] = value;
        this.setState(state, () => {
            var filter = {on: this.state.on, from_date: this.state.from_date, to_date: this.state.to_date}
            this.setState({filtered: filter});
        });
    }

    render() {  
        const logColumns = [ 
            {Header: 'Title:', accessor: 'title', filterable: false,  },
            {Header: 'Date:', accessor: 'created', filterable: false,sortable:false },
            {Header: 'By:', accessor: 'created_by', filterable: false,sortable:false },
        ]

        const csvHeaders = [
            {label: 'Title', key: 'title'},
            {label: 'Date', key: 'created'},
            {label: 'By', key: 'created_by'},
        ];

            return (
               <div role="tabpanel" className="tab-pane active" id="Logs">
                        <div className="row">
                              <div className="col-lg-4 col-lg-offset-1 col-md-4"><h2 className="P_20_TB">Logs for Case ID - {this.state.caseId}</h2></div>
                              <div className="col-lg-5 col-lg-offset-1 P_15_TB col-md-7 col-md-offset-1">
                                    <ul className="on_f_to_ul">
                                          <li><label>On: </label></li>
                                          <li>
                                               <DatePicker  isClearable={true} name="on" onChange={(e) => this.searchBox('on',e)} selected={this.state['on'] ? moment(this.state['on'], 'DD-MM-YYYY') : null}   className="text-center px-0" dateFormat="DD-MM-YYYY" autoComplete="off" />
                                          </li>
                                          <li><label>From: </label></li>
                                          <li>
                                                <DatePicker  isClearable={true} name="from_date" onChange={(e) => this.searchBox('from_date',e)} selected={this.state['from_date'] ? moment(this.state['from_date'], 'DD-MM-YYYY') : null}   className="text-center px-0" dateFormat="DD-MM-YYYY" autoComplete="off" />
                                          </li>
                                          <li><label>to: </label></li>
                                          <li>
                                                <DatePicker  isClearable={true} name="to_date" onChange={(e) => this.searchBox('to_date',e)} selected={this.state['to_date'] ? moment(this.state['to_date'], 'DD-MM-YYYY') : null}   className="text-center px-0" dateFormat="DD-MM-YYYY" autoComplete="off" />
                                          </li>
                                    </ul>
                              </div>
                              <div className="col-lg-1"></div>
                        </div>      
                        <div className="row">
                              <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bor_T"></div></div>
                        </div>
                        <div className="row mt-3">
                              <div className="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 listing_table">
                                    <ReactTable
                                    columns={logColumns}
                                    manual 
                                    data={this.state.caseLog}
                                    pages={this.state.pages}
                                    loading={this.state.loading} 
                                    onFetchData={this.fetchData} 
                                    filtered={this.state.filtered}
                                    defaultFiltered ={{caseId: this.props.caseId}}
                                    defaultPageSize={10}
                                    className="-striped -highlight"  
                                    noDataText="No Record Found"
                                    minRows={2}
                                    previousText = {<span className="icon icon-arrow-1-left privious"></span>}
                                    nextText = {<span className="icon icon-arrow-1-right next"></span>}
                                    getTheadThProps={ (state, rowInfo, column) => {
                                                        return {
                                                            style: { display: 'none' }
                                                        }
                                                    }}                          
                                    />
                              </div>
                        </div>
                        <div className="row">
                              <div className="col-lg-2 col-lg-offset-9 col-md-3 col-md-offset-9">
                                  <CSVLink data={this.state.caseLog} headers={csvHeaders} className="but" filename="upcoming_shift.csv"
                                    onClick={(event) => {                                   
                                        if(this.state.caseLog.length == 0)
                                        {
                                            toast.warning("No log to export.", {
                                                position: toast.POSITION.TOP_CENTER,
                                                hideProgressBar: true
                                            }); 
                                            return false;  
                                        }
                                    }} >
                                    Export
                                </CSVLink>
                              </div>
                        </div>
                  </div>
            )
        }
}

//case Analysis
class CaseAnalysis extends React.Component {
    constructor(props) {
        super(props);       
            this.state = {

            }
    }
                
    render() {  
        return (
            <div role="tabpanel" className="tab-pane" id="Analysis">
             Coming Soon.
            </div>
        )
    }
}
    
