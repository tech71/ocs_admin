import React, { Component } from 'react';
import jQuery from "jquery";
import { ROUTER_PATH, BASE_URL } from '../../../config.js';
import { checkItsNotLoggedIn,postData,handleChangeChkboxInput, handleChangeSelectDatepicker,reFreashReactTable,checkPinVerified } from '../../../service/common.js';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import Panel from 'react-bootstrap/lib/Panel';
import moment from 'moment';
import Modal from 'react-bootstrap/lib/Modal';
import { ToastContainer, toast } from 'react-toastify';
import DatePicker from 'react-datepicker';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import ReactTable from "react-table";
import 'react-table/react-table.css'
//import {CSVLink} from 'react-csv';

class CaseRespond extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        ////checkPinVerified();
        this.caseId = this.props.caseId;
        this.state = {  
            loading: false,
            show_msg: false,
            search_data:[],
            contact_list:[]  
        };
    }

    componentDidMount() {
        this.get_contact_list();
    }

    searchData =(e)=> {
        e.preventDefault();
        var srch_str = this.state.searchbox;
        // if(srch_str.length >= 3)
        {
            var requestData = {case_id: this.props.caseId,searchbox:this.state.searchbox,except_record:this.state.contact_list};
            this.setState({loading: true});
            postData('fms/FmsDashboard/search_address_book', requestData).then((result) => {
                if (result.status) {
                    this.setState({search_data:result.data,show_msg:true},()=>{ });
                } else {
                    this.setState({error: result.error});
                }
                this.setState({loading: false});
            });
        }
    }

    add_2_contact_list =(record,index) =>{
        var state = [];
        var list = this.state.contact_list;
        state['contact_list'] = list.concat(record);
        state['search_data'] = this.state.search_data.filter((s, sidx) => index !== sidx);
        this.setState(state);
        this.save_contact_searchbook(record,this.props.caseId);
    }

    get_contact_list()
    {
        var requestData = {case_id: this.props.caseId};
        this.setState({loading: true});
        postData('fms/FmsDashboard/get_contact_list', requestData).then((result) => {
            if (result.status) {
                this.setState({contact_list:result.data},()=>{ console.log(this.state)});
            } else {
                this.setState({error: result.error});
            }
        });
    }

    save_contact_searchbook(record,caseId)
    {
        var requestData = {case_id: caseId,record:record};
        postData('fms/FmsDashboard/save_contact', requestData).then((result) => {
           
        });
    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-lg-4 col-sm-1"></div>
                    <ul className="nav nav-tabs Category_tap col-lg-4 col-sm-10 P_20_TB" role="tablist">
                        <li role="presentation" className="col-lg-6 col-sm-6 active">
                            <a  href="#phonetap" aria-controls="phonetap" role="tab" data-toggle="tab">Phone</a>
                        </li>
                        {/*<li role="presentation" className="col-lg-4 col-sm-4">
                            <a  href="#emailtap" aria-controls="emailtap" role="tab" data-toggle="tab">Email</a>
                        </li>*/}
                        <li role="presentation" className="col-lg-6 col-sm-6">
                            <a  href="#lmailtap" aria-controls="lmailtap" role="tab" data-toggle="tab">Imail</a>
                        </li>
                    </ul>
                    <div className="col-lg-4 col-sm-1"></div>
                    <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div>
                </div>

                <div className="tab-content">
                    <div role="tabpanel" className="tab-pane active" id="phonetap">
                        <div className="row">
                            <div className="col-lg-10 col-lg-offset-1 col-sm-12"><h2 className="P_15_TB">Call History:</h2></div>
                            <div className="col-lg-1"></div>
                        </div>
                        <div className="row">
                            <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div>
                            <div className="col-lg-1"></div>
                        </div>
                        <div className="row">
                            <div className="col-lg-8 col-lg-offset-1 col-md-9 col-sm-12">
                                <table width="100%" className="call_hist">
                                <tbody>
                                    <tr>
                                        <td className="py-4 px-3"><span>Called:</span> David Marshall</td>
                                        <td className="py-4 px-3"><span>Date/Time:</span> 10/12/2018—09:13 </td>  
                                        <td className="py-4 px-3"><span>Duration:</span> 07:12  </td> 
                                        <td className="py-4 px-3"><span>Caller:</span> 872343</td>
                                        <td className="px-3 text-center"><i className="icon icon-caller-icons volume_button"></i></td>
                                    </tr><tr>
                                        <td className="py-4 px-3"><span>Called:</span> David Marshall</td>
                                        <td className="py-4 px-3"><span>Date/Time:</span> 10/12/2018—09:13 </td>  
                                        <td className="py-4 px-3"><span>Duration:</span> 07:12  </td> 
                                        <td className="py-4 px-3"><span>Caller:</span> 872343</td>
                                        <td className="px-3 text-center"><i className="icon icon-caller-icons volume_button"></i></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="col-lg-3"></div>
                        </div>
                        <div className="row">
                            <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div>
                            <div className="col-lg-1"></div>
                        </div>
                        <div className="row">
                            <div className="col-lg-10 col-lg-offset-1 col-sm-12"><h2 className="P_15_TB">Address Book:</h2></div>
                            <div className="col-lg-1"></div>
                        </div>
                        <div className="row">
                            <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div>
                            <div className="col-lg-1"></div>
                        </div>
                        <div className="row P_15_TB">
                            <div className="col-lg-1"></div>
                            <div className="col-lg-10 col-sm-12">
                                <div className="row">
                                    <div className="col-sm-7">
                                        <label>Search:</label>
                                    </div>

                                    {
                                        (this.state.contact_list.length>0)?
                                        <span>
                                            <div className="col-sm-1"></div>
                                            <div className="col-sm-4">
                                                <label>Contact us:</label>
                                            </div>
                                        </span>:''
                                    }

                                </div>
                                <div className="row">
                                    <div className="col-sm-7 P_15_b">
                                        <form method='post' onSubmit={(e)=>this.searchData(e)} autoComplete="off">
                                            <div className="event_loca search_i_right">
                                                <input name="searchbox" type="text" className="form-control" placeholder="Search" onChange={(e)=>handleChangeChkboxInput(this,e)} value={this.state.searchbox} />
                                                <button type="submit">
                                                    <span className="icon icon-search"></span>
                                                </button>  
                                            </div>
                                        </form>
                                    <div className="row">
                                        <div className="col-sm-12 P_25_T">
                                            <div className='AdInline'>
                                                {
                                                    (this.state.search_data.length>0)?<div>  
                                                    {this.state.search_data.map((value, index) => (
                                                        <span className='adrsBlSP' key={index}>
                                                             <div className='adrsName' >
                                                                <span>{value.label}</span>
                                                           
                                                                {(value.phone)?<i className='icon icon-phone-icons '></i>:'<i >NA</i>'}
                                                            </div>
                                                            <div className="add_i_icon mt-2 adIc12" >
                                                                <a onClick={()=>this.add_2_contact_list(value,index)}><span className="icon icon-add-icons"></span></a>
                                                            </div>
                                                        </span>
                                                    
                                                    ))} 
                                                    <div className='clearfix'></div>
                                                    </div> : (this.state.show_msg)?'No record found':''
                                                }
                                             </div>
                                        </div>
                                    </div>
                                </div>
                                    <div className="col-sm-4 col-sm-offset-1">
                                        <div className='clearfix'></div>
                                        <div className='wd80M'>
                                        <div className='AdBlock'>
                                             {
                                                (this.state.contact_list.length>0)?<span>  
                                                {this.state.contact_list.map((value, index) => (
                                                <div className='adrsName'>
                                                  <span>{value.label}</span>
                                                  <i className='icon icon-phone-icons '></i>
                                                </div>

                                             ))} </span> : ''
                                            }
                                        </div>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                            <div className="col-sm-1"></div>
                        </div>
                    </div>
                    {/*<div role="tabpanel" className="tab-pane" id="emailtap"></div>*/}
                    <div role="tabpanel" className="tab-pane" id="lmailtap"> 
                        <CaseImail caseId={this.props.caseId}/>
                    </div>           
            </div>
            </div>
        );
    }
}
export default CaseRespond;

//case Imail
class CaseImail extends React.Component {
    constructor(props) {
        super(props);       
            this.state = {

            }
    }
                
    render() {  
        return (
            <div role="tabpanel" className={(this.props.activeMenu == 'Analysis')?'tab-pane active':'tab-pane'} id="Analysis">
                <div className="row">
                    <div className="col-lg-4 col-lg-offset-1 col-sm-4"><h2 className="P_20_TB">Imail for Case ID - {this.props.caseId}</h2></div>
                    <div className="col-lg-5 col-lg-offset-1 P_15_TB col-sm-7 col-sm-offset-1"></div>
                    <div className="col-lg-1"></div>
                </div>  
                
                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div>
                   <div className="col-sm-12 text-center">                         <div className="row">
                            <div className="col-lg-8 col-lg-offset-1 col-md-9 col-xs-12">
                                <table width="100%" className="call_hist">
                                <tbody>
                                    <tr>
                                        <td className="py-4 px-3"><span>Called:</span> David Marshall</td>
                                        <td className="py-4 px-3"><span>Date/Time:</span> 10/12/2018—09:13 </td>  
                                        <td className="py-4 px-3"><span>Duration:</span> 07:12  </td> 
                                        <td className="py-4 px-3"><span>Caller:</span> 872343</td>
                                        <td className="px-3 text-center"><i className="icon icon-caller-icons volume_button"></i></td>
                                    </tr><tr>
                                        <td className="py-4 px-3"><span>Called:</span> David Marshall</td>
                                        <td className="py-4 px-3"><span>Date/Time:</span> 10/12/2018—09:13 </td>  
                                        <td className="py-4 px-3"><span>Duration:</span> 07:12  </td> 
                                        <td className="py-4 px-3"><span>Caller:</span> 872343</td>
                                        <td className="px-3 text-center"><i className="icon icon-caller-icons volume_button"></i></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="col-lg-3"></div>
                        </div></div>
                </div>  
            </div>
        )
    }
}
