import React, { Component } from 'react';
import jQuery from "jquery";
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { ToastContainer, toast } from 'react-toastify';
import { Panel, Button, ProgressBar, Modal, PanelGroup } from 'react-bootstrap';
import { postData, handleChangeChkboxInput, changeTimeZone, archiveALL, reFreashReactTable } from '../../../service/common.js';
import QuestionAnalytics from './QuestionAnalytics';
import SetUpIpadModal from './SetUpIpadModal';
import { connect } from 'react-redux'
import RecruitmentPage from 'components/admin/recruitment/RecruitmentPage';
import { ToastUndo } from 'service/ToastUndo.js'



const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {
        var Request = { pageSize: pageSize, page: page, sorted: sorted, filtered: filtered };
        postData('recruitment/Recruitment_question/get_questions_list', Request)
        .then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            resolve(res);
        });
    });
};

class Training extends Component {
    constructor() {
        super();
        this.state = {
            showModal: false,
            trainingCategory: '',
            SelectTopic: '',
            SetStatus: '',
            SetQuesType: '',
            question_topic: '',
            QuestionAnalyticsModal: false,
            filterVal: '',
            ActiveClass: 'groupInterview',
            setUpIpad:false
        }
        this.reactTable = React.createRef();
    }
    
    componentDidMount() {

    }
    
    showModal = () => { this.setState({ showModal: true }) }
    closeModal = () => { this.setState({ showModal: false }) }
    handleAddClose = () => { this.setState({ showadd: false }); }
    handleAddShow = (val) => { this.setState({ showadd: true, mode: 'update', question: val }); }
    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered,
        ).then(res => {
            this.setState({
                shiftListing: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }
    archiveHandle = (questionId) => {
        archiveALL({id:questionId}, 'Are you sure want to archive', 'recruitment/Recruitment_question/delete_Question').then((result) => {
            if (result.status) {
                reFreashReactTable(this, 'fetchData');
            }
        })
    }

    handleQuestionAnalytics = (value) => {
        this.setState({
            QuestionAnalyticsModal: true,
            question: value
        })

    }

    render() {
        var options = [{ value: 'one', label: 'Option 1' }, { value: 'two', label: 'Option 2' }];
        var options_trainging_category = [{ value: 1, label: 'Cab Day' }, { value: 2, label: 'iPad' }];
        var options_status = [{ value: 1, label: 'Active' }, { value: 2, label: 'Inactive' }];
        var options_type = [{ value: '0', label: 'Single Choice' }, { value: '1', label: 'Multiple Choice' }];


        const groupInterview = [
            { Question: 'GI-Q10', Topic: 'Fire Management', Status: 'Active', Created: 'Jane Marciano', Updated: '02/02/02' },
            { Question: 'GI-Q10', Topic: 'Fire Management', Status: 'Active', Created: 'Jane Marciano', Updated: '02/02/02' },
            { Question: 'GI-Q10', Topic: 'Fire Management', Status: 'Active', Created: 'Jane Marciano', Updated: '02/02/02' }
        ];

        const ipadDay = [
            { ipadId: 'IPad-107', allocated: 'Jimmy Smith', Status: 'Active', set: 'Jane Marciano', date: '02/02/02' },
            { ipadId: 'IPad-108', allocated: 'Jimmy Smith', Status: 'Active', set: 'Jane Marciano', date: '02/02/02' },
            { ipadId: 'IPad-109', allocated: 'Jimmy Smith', Status: 'Active', set: 'Jane Marciano', date: '02/02/02' }
        ];

        var ipadStatusOptions = [{ value: '1', label: 'Active' }, { value: '2', label: 'Inactive' }];

        return (
            <React.Fragment>
                <RecruitmentPage pageTypeParms={this.props.props.match.params.page}/>
               
                        <div className="row">
                            <div className="col-lg-12 col-md-12 no-pad back_col_cmn-">
                                <span onClick={(e) => window.history.back()} className="icon icon-back1-ie"></span>
                            </div>
                        </div>
                        {/* row ends */}
                        <div className="row">
                            <div className="col-lg-12 col-md-12 main_heading_cmn-">
                                <h1>{this.props.showPageTitle}</h1>
                            </div>
                        </div>
                        {/* row ends */}

                        <div className="row action_cont_row">
                          
                            <div className="col-lg-12 col-md-12 col-sm-12 no_pd_r noPd_l_ipd">
                                <div className="tab-content">
                                    <div role="tabpanel" className={this.props.showTypePage=='group_interview' ? "tab-pane active" : "tab-pane"} id="groupInterview">

                                        <div className="tasks_comp ">
                                            <div className="row">

                                                <div className='col-lg-8 col-md-8 col-sm-8 mr_b_20'>
                                                    <div className="search_bar left">
                                                        <input type="text" className="srch-inp" placeholder="Search.." />
                                                        <i className="icon icon-search2-ie"></i>
                                                    </div>
                                                </div>

                                                <div className="col-lg-4 col-md-4 col-sm-4 no_pd_r">

                                                    <div className="filter_flx lab_vrt">
                                                        <label>Filter by:</label>
                                                        <div className="filter_fields__ cmn_select_dv">
                                                            <Select name="view_by_status"
                                                                required={true} simpleValue={true}
                                                                searchable={true} Clearable={false}
                                                                placeholder="Filter by: Unread"
                                                                options={options}
                                                                onChange={(e) => this.setState({ filterVal: e })}
                                                                value={this.state.filterVal}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-sm-12">
                                                    <div className="data_table_cmn dataTab_accrdn_cmn aplcnt_table hdng_cmn2 trainTable">
                                                        <ReactTable
                                                            columns={[
                                                                { Header: "Question ID:", accessor: "Question" },
                                                                { Header: "Related Topic:", accessor: "Topic" },
                                                                { Header: "Status:", accessor: "Status" },
                                                                { Header: "Created by:", accessor: "Created" },
                                                                { Header: "Last Updated:", accessor: "Updated" },
                                                                {
                                                                    expander: true,
                                                                    Header: () => <strong></strong>,
                                                                    width: 55,
                                                                    headerStyle: { border: "0px solid #fff" },
                                                                    Expander: ({ isExpanded, ...rest }) =>
                                                                        <div className="rec-table-icon">
                                                                            {isExpanded
                                                                                ? <i className="icon icon-arrow-down icn_ar1"></i>
                                                                                : <i className="icon icon-arrow-right icn_ar1"></i>}

                                                                        </div>,
                                                                    style: {
                                                                        cursor: "pointer",
                                                                        fontSize: 25,
                                                                        padding: "0",
                                                                        textAlign: "center",
                                                                        userSelect: "none"
                                                                    },
                                                                }
                                                            ]}
                                                            defaultPageSize={groupInterview.length}
                                                            data={groupInterview}
                                                            showPagination={false}
                                                            className="-striped -highlight"
                                                            SubComponent={() =>
                                                                <div className='applicant_info1 training_info1'>
                                                                    <div className='trngBoxAc'>
                                                                        <div className='row '>
                                                                            <div className='col-lg-6 col-md-6 col-sm-6'>
                                                                                <h4><b>GI-Q01</b></h4>
                                                                                <div className='qShwcse'>
                                                                                    <h4><b>Question</b></h4>
                                                                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisienim ad minim veniam?</p>
                                                                                </div>
                                                                            </div>
                                                                            <div className='col-lg-6 col-md-6 col-sm-6 bor_left ans_colTr'>
                                                                                <h4><b>Single Answer:</b></h4>


                                                                                <div className='singleAnswer__'>
                                                                                    <label>Answer:</label>
                                                                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna</p>
                                                                                </div>



                                                                            </div>

                                                                        </div>
                                                                        {/* row ends */}

                                                                        <div className='row accFootRow1'>
                                                                            <div className='col-md-6 col-sm-12 '>
                                                                                <div className=''>
                                                                                    <h4 className='crtdByH'><b>Created by:</b> Jane Marciano (01/01/01 - 3:42pm)</h4>
                                                                                </div>
                                                                            </div>
                                                                            <div className='col-md-6 col-sm-12'>

                                                                                <ul className="subTasks_Action__">
                                                                                    <li><span className="sbTsk_li">Question Analytics</span></li>
                                                                                    <li><span className="sbTsk_li">Edit Question</span></li>
                                                                                    <li><span className="sbTsk_li">Archive Question</span></li>
                                                                                </ul>

                                                                            </div>
                                                                        </div>
                                                                        {/* row ends */}


                                                                    </div>

                                                                </div>
                                                            }
                                                        />
                                                    </div>
                                                </div>




                                            </div>
                                        </div>

                                    </div>
                                    {/* group Interview ends */}


                                    <div role="tabpanel" className={this.props.showTypePage=='cab_day' ? "tab-pane active" : "tab-pane"} id="cabDay">

                                        <div className="tasks_comp ">
                                            <div className="row">
                                                <div className='col-lg-8 col-md-8 col-sm-8 mr_b_20'>
                                                    <div className="search_bar left">
                                                        <input type="text" className="srch-inp" placeholder="Search.." />
                                                        <i className="icon icon-search2-ie"></i>
                                                    </div>
                                                </div>

                                                <div className="col-lg-4 col-md-4 col-sm-4 no_pd_r">

                                                    <div className="filter_flx lab_vrt">
                                                        <label>Filter by:</label>
                                                        <div className="filter_fields__ cmn_select_dv">
                                                            <Select name="view_by_status"
                                                                required={true} simpleValue={true}
                                                                searchable={true} Clearable={false}
                                                                placeholder="Filter by: Unread"
                                                                options={options}
                                                                onChange={(e) => this.setState({ filterVal: e })}
                                                                value={this.state.filterVal}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-sm-12">
                                                    <div className="data_table_cmn dataTab_accrdn_cmn aplcnt_table hdng_cmn2 trainTable">
                                                        <ReactTable
                                                            columns={[
                                                                { Header: "Question ID:", accessor: "id" },
                                                                { Header: "Related Topic:", accessor: "topic" },
                                                                { Header: "Status:", accessor: "status", Cell: props => <span className='status'>{props.value == "1" ? 'Active' : 'Inactive'}</span> },
                                                                { Header: "Created by:", accessor: "Created" },
                                                                { Header: "Last Updated:", accessor: "updated", filterable: false, Cell: props => <span>{changeTimeZone(props.value, 'DD/MM/YYYY')}</span> },
                                                                {
                                                                    expander: true,
                                                                    Header: () => <strong></strong>,
                                                                    width: 55,
                                                                    headerStyle: { border: "0px solid #fff" },
                                                                    Expander: ({ isExpanded, ...rest }) =>
                                                                        <div className="rec-table-icon">
                                                                            {isExpanded
                                                                                ? <i className="icon icon-arrow-down icn_ar1"></i>
                                                                                : <i className="icon icon-arrow-right icn_ar1"></i>}
                                                                        </div>,
                                                                    style: {
                                                                        cursor: "pointer",
                                                                        fontSize: 25,
                                                                        padding: "0",
                                                                        textAlign: "center",
                                                                        userSelect: "none"
                                                                    },
                                                                }
                                                            ]}
                                                            ref={this.reactTable}
                                                            data={this.state.shiftListing}
                                                            onFetchData={this.fetchData}
                                                            pageSize={this.state.pages}
                                                            showPagination={false}
                                                            className="-striped -highlight"
                                                            SubComponent={(props) =>
                                                                <div className='applicant_info1 training_info1'>
                                                                    <div className='trngBoxAc'>
                                                                        <div className='row '>
                                                                            <div className='col-lg-6 col-md-6 col-sm-6'>
                                                                                <h4><b>GI-Q{props.original.id}</b></h4>
                                                                                <div className='qShwcse'>
                                                                                    <h4><b>Question</b></h4>
                                                                                    <p>{props.original.question}</p>
                                                                                </div>
                                                                            </div>
                                                                            <div className='col-lg-6 col-md-6 col-sm-6 bor_left ans_colTr'>
                                                                                <h4><b>Multiple Choice Answer:</b></h4>
                                                                                <ul className='answrShw'>
                                                                                    {
                                                                                        props.original.answers.map((v, idx) => (
                                                                                            <li key={'index' + idx} className={v.checked == 1 ? 'rightAnswer' : 'wrongAnswer'}><b> {v.lebel} :</b><span>{v.value}</span></li>
                                                                                        ))
                                                                                    }
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        {/* row ends */}
                                                                        <div className='row accFootRow1'>
                                                                            <div className='col-md-6 col-sm-12 '>
                                                                                <div className=''>
                                                                                    <h4 className='crtdByH'><b>Created by:</b> Jane Marciano ({changeTimeZone(props.original.created, 'DD/MM/YYYY LT')} )</h4>
                                                                                </div>
                                                                            </div>
                                                                            <div className='col-md-6 col-sm-12'>
                                                                                <ul className="subTasks_Action__">
                                                                                    <li onClick={() => { this.handleQuestionAnalytics(props.original.question) }}><span className="sbTsk_li">Question Analytics</span></li>
                                                                                    <li>
                                                                                        <a align="right" className="sbTsk_li" onClick={() => this.handleAddShow(props.original)} >Edit Question</a>
                                                                                    </li>

                                                                                    <li><a align="right" className="sbTsk_li" onClick={() => this.archiveHandle(props.original.id)}>archived Question</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        {/* row ends */}
                                                                    </div>
                                                                </div>
                                                            }
                                                        />
                                                    </div>
                                                </div>
                                                <div className="col-sm-12 TaskAccordion_col jobOpeningCol__">
                                                    <button onClick={() =>
                                                        this.setState({ showadd: true, mode: 'add' })} className="btn cmn-btn1 new_task_btn" >Create New Question</button>
                                                </div>
                                            </div>
                                            {/* row ends */}
                                        </div>
                                        {/* tasks_comp ends */}

                                    </div>

                                    {/* cab day ends */}


                                    <div role="tabpanel" className={this.props.showTypePage=='ipad' ? "tab-pane active" : "tab-pane"} id="ipad">

                                        <div className="tasks_comp ">
                                            <div className="row">

                                                <div className='col-lg-8 col-md-8 col-sm-8 mr_b_20'>
                                                    <div className="search_bar left">
                                                        <input type="text" className="srch-inp" placeholder="Search.." />
                                                        <i className="icon icon-search2-ie"></i>
                                                    </div>
                                                </div>

                                                <div className="col-lg-4 col-md-4 col-sm-4 no_pd_r">

                                                    <div className="filter_flx lab_vrt">
                                                        <label>Filter by:</label>
                                                        <div className="filter_fields__ cmn_select_dv">
                                                            <Select name="view_by_status"
                                                                required={true} simpleValue={true}
                                                                searchable={true} Clearable={false}
                                                                placeholder="Filter by: Unread"
                                                                options={options}
                                                                onChange={(e) => this.setState({ filterVal: e })}
                                                                value={this.state.filterVal}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-sm-12">
                                                    <div className="data_table_cmn dataTab_accrdn_cmn aplcnt_table hdng_cmn2 trainTable">
                                                        <ReactTable
                                                            columns={[
                                                                { Header: "IPad ID:", accessor: "ipadId" },
                                                                { Header: "Allocated To:", accessor: "allocated" },
                                                                { Header: "Status:", accessor: "Status" },
                                                                { Header: "Set by:", accessor: "set" },
                                                                { Header: "Date:", accessor: "date" },
                                                                {
                                                                    expander: true,
                                                                    Header: () => <strong></strong>,
                                                                    width: 55,
                                                                    headerStyle: { border: "0px solid #fff" },
                                                                    Expander: ({ isExpanded, ...rest }) =>
                                                                        <div className="rec-table-icon">
                                                                            {isExpanded
                                                                                ? <i className="icon icon-arrow-down icn_ar1"></i>
                                                                                : <i className="icon icon-arrow-right icn_ar1"></i>}

                                                                        </div>,
                                                                    style: {
                                                                        cursor: "pointer",
                                                                        fontSize: 25,
                                                                        padding: "0",
                                                                        textAlign: "center",
                                                                        userSelect: "none"
                                                                    },
                                                                }
                                                            ]}
                                                            collapseOnDataChange={false}
                                                            data={ipadDay}
                                                            showPagination={false}
                                                            className="-striped -highlight"
                                                            SubComponent={() =>
                                                                <div className='applicant_info1 training_info1'>
                                                                    <div className='trngBoxAc'>
                                                                        <div className='row '>
                                                                            <div className='col-lg-4 col-md-4 col-sm-4'>

                                                                                <div className='qShwcse'>
                                                                                    <h4><b>CAB Day Information</b></h4>
                                                                                    <div className='CABinfo_lstBox__ '>
                                                                                        <div className='row d-flex wordWrap'>
                                                                                            <div className='col-sm-4 col-xs-12'><strong>Date:</strong></div>
                                                                                            <div className='col-sm-8 col-xs-12'>01/01/2011</div>
                                                                                        </div>
                                                                                        <div className='row d-flex wordWrap'>
                                                                                            <div className='col-sm-4 col-xs-12'><strong>Time:</strong></div>
                                                                                            <div className='col-sm-8 col-xs-12'> 09:30 AM</div>
                                                                                        </div>
                                                                                        <div className='row d-flex wordWrap'>
                                                                                            <div className='col-sm-4 col-xs-12'><strong>Attendees:</strong></div>
                                                                                            <div className='col-sm-8 col-xs-12'>15</div>
                                                                                        </div>
                                                                                        <div className='row d-flex wordWrap'>
                                                                                            <div className='col-sm-4 col-xs-12'><strong>Run By:</strong></div>
                                                                                            <div className='col-sm-8 col-xs-12'>Roc Marciano</div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className='col-lg-5 col-md-5 col-sm-4 bor_left ans_colTr'>
                                                                                <div className='qShwcse'>
                                                                                    <h4><b>IPad Information</b></h4>
                                                                                    <div className='CABinfo_lstBox__ '>
                                                                                        <div className='row d-flex wordWrap'>
                                                                                            <div className='col-sm-5 col-xs-12'><strong>Allocated To:</strong></div>
                                                                                            <div className='col-sm-7 col-xs-12'>Jimmy Smith</div>
                                                                                        </div>
                                                                                        <div className='row d-flex wordWrap'>
                                                                                            <div className='col-sm-5 col-xs-12'><strong>Attached Docs:</strong></div>
                                                                                            <div className='col-sm-7 col-xs-12'>
                                                                                                <div>Contract</div>
                                                                                                <div>Power Pres</div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className='row d-flex wordWrap'>
                                                                                            <div className='col-sm-5 col-xs-12'><strong>Members App Pin:</strong></div>
                                                                                            <div className='col-sm-7 col-xs-12'>Active</div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className='col-lg-3 col-md-4 col-sm-4 bor_left ans_colTr'>
                                                                                <div className='qShwcse'>
                                                                                    <h4><b>Set Status</b></h4>
                                                                                    <div className="filter_fields__ cmn_select_dv">
                                                                                        <Select name="view_by_status"
                                                                                            required={true} simpleValue={true}
                                                                                            searchable={false} Clearable={false}
                                                                                            placeholder="Set Status"
                                                                                            options={ipadStatusOptions}
                                                                                            onChange={(e) => this.setState({ ipadstatus: e })}
                                                                                            value={this.state.ipadstatus}
                                                                                        />
                                                                                    </div>
                                                                                    
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        {/* row ends */}

                                                                        <div className='row accFootRow1'>
                                                                            <div className='col-md-6 col-sm-12 '>
                                                                                <div className=''>
                                                                                    <h4 className='crtdByH'><b>Created by:</b> Jane Marciano (01/01/01 - 3:42pm)</h4>
                                                                                </div>
                                                                            </div>
                                                                            <div className='col-md-6 col-sm-12 text-right'>

                                                                                <button className='btn cmn-btn1 edt_ipad' onClick={()=>{this.setState({setUpIpad:true})}}>Edit IPad Set-Up</button>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                        {/* row ends */}


                                                                    </div>

                                                                </div>
                                                            }
                                                        />
                                                    </div>
                                                </div>




                                            </div>
                                        </div>

                                    </div>
                                    {/* ipad ends */}
                                </div>
                            </div>
                            {/* col-lg-10 ends */}

                        </div>
                        {/* row ends */}
                   
                <AddQuestion showadd={this.state.showadd} handleAddClose={this.handleAddClose} address={this.state.address} options={options} question={this.state.question} mode={this.state.mode} />
                
                <QuestionAnalytics question={this.state.question} show={this.state.QuestionAnalyticsModal} close={() => { this.setState({ QuestionAnalyticsModal: false }) }} />
                
                <SetUpIpadModal show={this.state.setUpIpad} close={()=>{this.setState({setUpIpad:false})}} />
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({
    showPageTitle: state.RecruitmentReducer.activePage.pageTitle,
    showTypePage: state.RecruitmentReducer.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
       
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(Training);
    
class AddQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentWillReceiveProps(newProps) {
        var newObj = JSON.parse(JSON.stringify(newProps));
        console.log(newObj);
        this.setState({ mode: newObj.mode })
        if (newObj.mode == 'update') {
            this.setState({
                question_id: newObj.question.id,
                answers: newObj.question.answers,
                question_status: newObj.question.status,
                question_category: newObj.question.training_category,
                question_topic: newObj.question.question_topic,
                answer_type: newObj.question.question_type,
                question: newObj.question.question,
            });

        }
        if (newObj.mode == 'add') {
            this.setState({
                question_id: 0,
                answers: [{ 'checked': false, 'value': '', 'lebel': 'A' }, { 'checked': false, 'value': '', 'lebel': 'B' }, { 'checked': false, 'value': '', 'lebel': 'C' }, { 'checked': false, 'value': '', 'lebel': 'D' }],
                question_category: '',
                question_topic: '',
                question_status: '',
                question: '',
                answer_type: ''
            });
        }
    }
    componentDidMount() {
        postData('recruitment/Recruitment_question/get_recruitment_topic_list', {}).then((result) => {
            if (result.status) {
                this.setState({ topicList: result.data });
            }
        });
    }
    submitquestion = (e) => {
        e.preventDefault();

        jQuery('#form_question').validate();
        if (jQuery('#form_question').valid()) {
            postData('recruitment/Recruitment_question/insert_update_question', this.state).then((result) => {

                if (result.status) {
                    toast.success(<ToastUndo message={'New Question is submitted successfully.'} showType={'s'} />, {
                    // toast.success('New Question is submitted successfully.', {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    this.props.handleAddClose();
                } else {
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                    // toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }

            });
        }
    }
    multiple_answer_Change(obj, stateName, index, fieldName, value) {
        if (obj.state.answer_type == 0) {
            obj.state.answers.map((value, idx) => {
                var state = {};
                var List = obj.state[stateName];
                List[idx]['checked'] = false
                state[stateName] = List;
                obj.setState(state);
            })
        }
        var state = {};
        var List = obj.state[stateName];
        List[index][fieldName] = value
        state[stateName] = List;
        obj.setState(state);
    }
    selectChange(selectedOption, fieldname) {
        if (fieldname == 'answer_type') {
            this.state.answers.map((value, idx) => {
                var stateName = 'answers';
                var state = {};
                var List = this.state[stateName];
                List[idx]['checked'] = false
                state[stateName] = List;
                this.setState(state);
            })
        }
        var state = this.state;
        state[fieldname] = selectedOption;
        this.setState(state);
    }
    render() {
        var options = [{ value: 'one', label: 'Option 1' }, { value: 'two', label: 'Option 2' }];
        var options_trainging_category = [{ value: 1, label: 'Cab Day' }, { value: 2, label: 'iPad' }];
        var options_status = [{ value: 1, label: 'Active' }, { value: 2, label: 'Inactive' }];
        var options_type = [{ value: '0', label: 'Single Choice' }, { value: '1', label: 'Multiple Choice' }];
        return (
            
                <div className={'customModal ' + (this.props.showadd ? ' show' : ' ')} id='newActionModal'>
                    <div className="cstomDialog widBig">
                        <h3 className="cstmModal_hdng1--">
                            New Question
                    <span className="closeModal icon icon-close1-ie" onClick={this.props.handleAddClose}></span>
                        </h3>
                        <form id="form_question" onSubmit={this.submitquestion}>
                            <div className="row bor_bot2 pd_b_20 mr_b_20">
                                <div className='col-md-2'>
                                    <div className="csform-group">
                                        <label>Question ID:</label>
                                        <h3 className='QId'><b>Q{this.state.question_id}</b></h3>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="csform-group">
                                        <label>Training Category:</label>
                                        <div className="cmn_select_dv dropDwnType2">
                                            <Select name="question_category"
                                                required={true} simpleValue={true}
                                                searchable={false} Clearable={false}
                                                placeholder="Select Category"
                                                options={options_trainging_category}
                                                onChange={(e) => this.selectChange(e, 'question_category')}
                                                value={this.state.question_category}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className='col-md-3'>
                                    <div className="csform-group ">
                                        <label>Question Topic:</label>
                                        <div className="cmn_select_dv dropDwnType2">
                                            <Select name="question_topic"
                                                required={true} simpleValue={true}
                                                searchable={false} Clearable={false}
                                                placeholder="Select Topic"
                                                options={this.state.topicList}
                                                onChange={(e) => this.selectChange(e, 'question_topic')}
                                                value={this.state.question_topic}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="csform-group">
                                        <label>Set Status:</label>
                                        <div className="cmn_select_dv dropDwnType2">
                                            <Select name="question_status"
                                                required={true} simpleValue={true}
                                                searchable={false} Clearable={false}
                                                placeholder="Set Status"
                                                options={options_status}
                                                onChange={(e) => this.selectChange(e, 'question_status')}
                                                value={this.state.question_status}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* row ends */}
                            <div className='row bor_bot2 pd_b_20 ' >
                                <div className="col-md-12">
                                    <div className="csform-group">
                                        <label>Question:</label>
                                        <textarea className="csForm_control txt_area brRad10 textarea-max-size" name="question" value={this.state.question} onChange={(e) => handleChangeChkboxInput(this, e)} data-rule-required="true"></textarea>
                                    </div>
                                </div>
                            </div>
                            {/* row ends */}
                            <div className='row'>
                                <div className='col-md-12 mr_b_20'>
                                    <h4><b>Answer</b></h4>
                                </div>
                                <div className="col-md-3">
                                    <div className="csform-group">
                                        <label className='pd_l_15'>Type:</label>
                                        <div className="cmn_select_dv dropDwnType2">
                                            <Select name="answer_type"
                                                required={true} simpleValue={true}
                                                searchable={false} Clearable={false}
                                                placeholder="Select Type"
                                                options={options_type}
                                                onChange={(e) => this.selectChange(e, 'answer_type')}
                                                value={this.state.answer_type}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* row ends */}
                            <div className='row multiple_choice_row'>
                                {this.state.answers ?
                                    this.state.answers.map((value, idx) => (
                                        <div key={idx} className='col-md-12 no-pad ques_col'>
                                            <div className="col-xs-9">
                                                <div className="csform-group">
                                                    <label className='pd_l_15'>Answer {value.lebel}:</label>
                                                    <input type="text" name={'answer' + idx} className="csForm_control" data-rule-required="true" value={value.value} onChange={(e) => this.multiple_answer_Change(this, 'answers', idx, 'value', e.target.value)} />
                                                </div>
                                            </div>
                                            <div className='col-xs-3 text-center'>
                                                <p className="radio_form_control1_ mr_t_30">
                                                    <input type="checkbox" id={'ans_' + idx}
                                                        name="radio-group"
                                                        checked={value.checked == "1" ? 'checked' : ''}
                                                        onChange={(e) => this.multiple_answer_Change(this, 'answers', idx, 'checked', e.target.checked)}
                                                        data-rule-required="true"

                                                    />
                                                    <label htmlFor={'ans_' + idx}></label>
                                                </p>
                                            </div>
                                        </div>))
                                    : ''}
                                {/* ques_col ends */}
                            </div>
                            {/* multiple_choice_row ends */}
                            <div className='row trnMod_Foot__'>
                                <div className='col-sm-12 no-pad text-right'>
                                    <button type="submit" disabled={this.state.loading} className="btn cmn-btn1 create_quesBtn">Save and Create Question</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
           
        )
    }
}