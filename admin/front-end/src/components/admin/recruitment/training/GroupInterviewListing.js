import React, { Component } from 'react';
import jQuery from "jquery";
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import Navigation from './../Navigation';
import { ToastContainer, toast } from 'react-toastify';
import { Panel, Button, ProgressBar, Modal, PanelGroup } from 'react-bootstrap';
import { postData, handleChangeChkboxInput, changeTimeZone, archiveALL, reFreashReactTable } from 'service/common.js';
import { recruitmentQuestionFilter} from 'dropdown/recruitmentdropdown.js';
import QuestionAnalytics from './../QuestionAnalytics';
import SetUpIpadModal from './../SetUpIpadModal';
import AddQuestion from './AddQuestion';
import TrainingNavigation from './TrainingNavigation';


const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {
        var Request = { pageSize: pageSize, page: page, sorted: sorted, filtered: filtered };
        postData('recruitment/Recruitment_question/get_questions_list', Request)
        .then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            resolve(res);
        });
    });
};

class GroupInterviewListing extends Component {
    constructor() {
        super();
        this.state = {
            showModal: false,
            trainingCategory: '',
            SelectTopic: '',
            SetStatus: '',
            SetQuesType: '',
            question_topic: '',
            QuestionAnalyticsModal: false,
            filterVal: '',
            ActiveClass: 'groupInterview',
            setUpIpad:false
        }
        this.reactTable = React.createRef();
    }
    
    componentDidMount() {

    }
    
    showModal = () => { this.setState({ showModal: true }) }
    closeModal = () => { this.setState({ showModal: false })}
    handleAddClose = () => { this.setState({ showadd: false }); }
    handleAddShow = (val) => { this.setState({ showadd: true, mode: 'update', question: val }); }
    
    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered,
        ).then(res => {
            this.setState({
                shiftListing: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }
    archiveHandle = (questionId) => {
        archiveALL({id: questionId}, 'Are you sure want to archive', 'recruitment/Recruitment_question/delete_Question').then((result) => {

            if (result.status) {
                reFreashReactTable(this, 'fetchData');
            }
        })
    }

    handleQuestionAnalytics = (value) => {
        this.setState({
            QuestionAnalyticsModal: true,
            question: value
        })

    }

    render() {
        const groupInterview = [
            { Question: 'GI-Q10', Topic: 'Fire Management', Status: 'Active', Created: 'Jane Marciano', Updated: '02/02/02' },
            { Question: 'GI-Q10', Topic: 'Fire Management', Status: 'Active', Created: 'Jane Marciano', Updated: '02/02/02' },
            { Question: 'GI-Q10', Topic: 'Fire Management', Status: 'Active', Created: 'Jane Marciano', Updated: '02/02/02' }
        ];
        
        return (
            <React.Fragment>
                        <div className="row">
                            <div className="col-lg-12 col-md-12 no-pad back_col_cmn-">
                                <Link to={'/admin/recruitment/dashboard'} className="icon icon-back1-ie"></Link>
                            </div>
                        </div>
                        {/* row ends */}
                        <div className="row">
                            <div className="col-lg-12 col-md-12 main_heading_cmn-">
                                <h1>Training</h1>
                            </div>
                        </div>
                        {/* row ends */}
                       
                        <div className="row action_cont_row">
                            <div className="col-lg-12 col-md-12 col-sm-12 no_pd_r noPd_l_ipd">
                                    <div className="tasks_comp ">
                                            <div className="row">
                                                <div className='col-lg-8 col-md-8 col-sm-8 mr_b_20'>
                                                    <div className="search_bar left">
                                                        <input type="text" className="srch-inp" placeholder="Search.." />
                                                        <i className="icon icon-search2-ie"></i>
                                                    </div>
                                                </div>

                                                <div className="col-lg-4 col-md-4 col-sm-4 no_pd_r">
                                                    <div className="filter_flx lab_vrt">
                                                        <label>Filter by:</label>
                                                        <div className="filter_fields__ cmn_select_dv">
                                                            <Select name="view_by_status"
                                                                simpleValue={true}
                                                                searchable={false}
                                                                placeholder="Filter by"
                                                                options={recruitmentQuestionFilter}
                                                                onChange={(e) => this.setState({ filterVal: e })}
                                                                value={this.state.filterVal}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-sm-12">
                                                    <div className="data_table_cmn dataTab_accrdn_cmn aplcnt_table hdng_cmn2 trainTable">
                                                        <ReactTable
                                                            columns={[
                                                                { Header: "Question ID:", accessor: "Question" },
                                                                { Header: "Related Topic:", accessor: "Topic" },
                                                                { Header: "Status:", accessor: "Status" },
                                                                { Header: "Created by:", accessor: "Created" },
                                                                { Header: "Last Updated:", accessor: "Updated" },
                                                                {
                                                                    expander: true,
                                                                    Header: () => <strong></strong>,
                                                                    width: 55,
                                                                    headerStyle: { border: "0px solid #fff" },
                                                                    Expander: ({ isExpanded, ...rest }) =>
                                                                        <div className="rec-table-icon">
                                                                            {isExpanded
                                                                                ? <i className="icon icon-arrow-down icn_ar1"></i>
                                                                                : <i className="icon icon-arrow-right icn_ar1"></i>}

                                                                        </div>,
                                                                    style: {
                                                                        cursor: "pointer",
                                                                        fontSize: 25,
                                                                        padding: "0",
                                                                        textAlign: "center",
                                                                        userSelect: "none"
                                                                    },
                                                                }
                                                            ]}
                                                            defaultPageSize={groupInterview.length}
                                                            data={groupInterview}
                                                            showPagination={false}
                                                            className="-striped -highlight"
                                                            SubComponent={() =>
                                                                <div className='applicant_info1 training_info1'>
                                                                    <div className='trngBoxAc'>
                                                                        <div className='row '>
                                                                            <div className='col-lg-6 col-md-6 col-sm-6'>
                                                                                <h4><b>GI-Q01</b></h4>
                                                                                <div className='qShwcse'>
                                                                                    <h4><b>Question</b></h4>
                                                                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisienim ad minim veniam?</p>
                                                                                </div>
                                                                            </div>
                                                                            <div className='col-lg-6 col-md-6 col-sm-6 bor_left ans_colTr'>
                                                                                <h4><b>Single Answer:</b></h4>


                                                                                <div className='singleAnswer__'>
                                                                                    <label>Answer:</label>
                                                                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna</p>
                                                                                </div>



                                                                            </div>

                                                                        </div>
                                                                        {/* row ends */}

                                                                        <div className='row accFootRow1'>
                                                                            <div className='col-md-6 col-sm-12 '>
                                                                                <div className=''>
                                                                                    <h4 className='crtdByH'><b>Created by:</b> Jane Marciano (01/01/01 - 3:42pm)</h4>
                                                                                </div>
                                                                            </div>
                                                                            <div className='col-md-6 col-sm-12'>

                                                                                <ul className="subTasks_Action__">
                                                                                    <li><span className="sbTsk_li">Question Analytics</span></li>
                                                                                    <li><span className="sbTsk_li">Edit Question</span></li>
                                                                                    <li><span className="sbTsk_li">Archive Question</span></li>
                                                                                </ul>

                                                                            </div>
                                                                        </div>
                                                                        {/* row ends */}
                                                                    </div>
                                                                </div>
                                                            }
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {/* group Interview ends */}

                            {/* col-lg-10 ends */}

                        </div>
                        {/* row ends */}
                   
                <AddQuestion showadd={this.state.showadd} handleAddClose={this.handleAddClose} address={this.state.address} question={this.state.question} mode={this.state.mode} />
                
                <QuestionAnalytics question={this.state.question} show={this.state.QuestionAnalyticsModal} close={() => { this.setState({ QuestionAnalyticsModal: false }) }} />
               
            </React.Fragment>
        );
    }
}
export default GroupInterviewListing;
