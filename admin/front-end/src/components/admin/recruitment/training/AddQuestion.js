import React, { Component } from 'react';
import jQuery from "jquery";
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import Navigation from './../Navigation';
import { ToastContainer, toast } from 'react-toastify';
import { Panel, Button, ProgressBar, Modal, PanelGroup } from 'react-bootstrap';
import { postData, handleChangeChkboxInput, changeTimeZone, archiveALL, reFreashReactTable } from 'service/common.js';
import { ToastUndo } from 'service/ToastUndo.js'


    
class AddQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    
    componentWillReceiveProps(newProps) {
        var newObj = JSON.parse(JSON.stringify(newProps));
        this.setState({ mode: newObj.mode })
        
        if (newObj.mode == 'update') {
            this.setState({
                question_id: newObj.question.id,
                answers: newObj.question.answers,
                question_status: newObj.question.status,
                question_category: newObj.question.training_category,
                question_topic: newObj.question.question_topic,
                answer_type: newObj.question.question_type,
                question: newObj.question.question,
            });
        }
        
        if (newObj.mode == 'add') {
            this.setState({
                question_id: 0,
                answers: [{ 'checked': false, 'value': '', 'lebel': 'A' }, { 'checked': false, 'value': '', 'lebel': 'B' }, { 'checked': false, 'value': '', 'lebel': 'C' }, { 'checked': false, 'value': '', 'lebel': 'D' }],
                question_category: '',
                question_topic: '',
                question_status: '',
                question: '',
                answer_type: ''
            });
        }
    }
    
    componentDidMount() {
        postData('recruitment/Recruitment_question/get_recruitment_topic_list', {}).then((result) => {
            if (result.status) {
                this.setState({ topicList: result.data });
            }
        });
    }
    
    submitquestion = (e) => {
        e.preventDefault();

        jQuery('#form_question').validate();
        if (jQuery('#form_question').valid()) {
            postData('recruitment/Recruitment_question/insert_update_question', this.state).then((result) => {

                if (result.status) {
                    toast.success(<ToastUndo message={'New Question is submitted successfully.'} showType={'s'} />, {
                    // toast.success('New Question is submitted successfully.', {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    this.props.handleAddClose();
                } else {
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                    // toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }

            });
        }
    }
    
    multiple_answer_Change(obj, stateName, index, fieldName, value) {
        if (obj.state.answer_type == 0) {
            obj.state.answers.map((value, idx) => {
                var state = {};
                var List = obj.state[stateName];
                List[idx]['checked'] = false
                state[stateName] = List;
                obj.setState(state);
            })
        }
        var state = {};
        var List = obj.state[stateName];
        List[index][fieldName] = value
        state[stateName] = List;
        obj.setState(state);
    }
    
    selectChange(selectedOption, fieldname) {
        if (fieldname == 'answer_type') {
            this.state.answers.map((value, idx) => {
                var stateName = 'answers';
                var state = {};
                var List = this.state[stateName];
                List[idx]['checked'] = false
                state[stateName] = List;
                this.setState(state);
            })
        }
        var state = this.state;
        state[fieldname] = selectedOption;
        this.setState(state);
    }
    
    render() {
        var options = [{ value: 'one', label: 'Option 1' }, { value: 'two', label: 'Option 2' }];
        var options_trainging_category = [{ value: 1, label: 'Cab Day' }, { value: 2, label: 'iPad' }];
        var options_status = [{ value: 1, label: 'Active' }, { value: 2, label: 'Inactive' }];
        var options_type = [{ value: '0', label: 'Single Choice' }, { value: '1', label: 'Multiple Choice' }];
        return (
            
                <div className={'customModal ' + (this.props.showadd ? ' show' : ' ')} id='newActionModal'>
                    <div className="cstomDialog widBig">
                        <h3 className="cstmModal_hdng1--">
                            New Question
                    <span className="closeModal icon icon-close1-ie" onClick={this.props.handleAddClose}></span>
                        </h3>
                        <form id="form_question" onSubmit={this.submitquestion}>
                            <div className="row bor_bot2 pd_b_20 mr_b_20">
                                <div className='col-md-2'>
                                    <div className="csform-group">
                                        <label>Question ID:</label>
                                        <h3 className='QId'><b>Q{this.state.question_id}</b></h3>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="csform-group">
                                        <label>Training Category:</label>
                                        <div className="cmn_select_dv dropDwnType2">
                                            <Select name="question_category"
                                                required={true} simpleValue={true}
                                                searchable={false} clearable={false}
                                                placeholder="Select Category"
                                                options={options_trainging_category}
                                                onChange={(e) => this.selectChange(e, 'question_category')}
                                                value={this.state.question_category}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className='col-md-3'>
                                    <div className="csform-group ">
                                        <label>Question Topic:</label>
                                        <div className="cmn_select_dv dropDwnType2">
                                            <Select name="question_topic"
                                                required={true} simpleValue={true}
                                                searchable={false} clearable={false}
                                                placeholder="Select Topic"
                                                options={this.state.topicList}
                                                onChange={(e) => this.selectChange(e, 'question_topic')}
                                                value={this.state.question_topic}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="csform-group">
                                        <label>Set Status:</label>
                                        <div className="cmn_select_dv dropDwnType2">
                                            <Select name="question_status"
                                                required={true} simpleValue={true}
                                                searchable={false} clearable={false}
                                                placeholder="Set Status"
                                                options={options_status}
                                                onChange={(e) => this.selectChange(e, 'question_status')}
                                                value={this.state.question_status}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* row ends */}
                            <div className='row bor_bot2 pd_b_20 ' >
                                <div className="col-md-12">
                                    <div className="csform-group">
                                        <label>Question:</label>
                                        <textarea className="csForm_control txt_area brRad10 textarea-max-size" name="question" value={this.state.question} onChange={(e) => handleChangeChkboxInput(this, e)} data-rule-required="true"></textarea>
                                    </div>
                                </div>
                            </div>
                            {/* row ends */}
                            <div className='row'>
                                <div className='col-md-12 mr_b_20'>
                                    <h4><b>Answer</b></h4>
                                </div>
                                <div className="col-md-3">
                                    <div className="csform-group">
                                        <label className='pd_l_15'>Type:</label>
                                        <div className="cmn_select_dv dropDwnType2">
                                            <Select name="answer_type"
                                                required={true} simpleValue={true}
                                                searchable={false} clearable={false}
                                                placeholder="Select Type"
                                                options={options_type}
                                                onChange={(e) => this.selectChange(e, 'answer_type')}
                                                value={this.state.answer_type}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* row ends */}
                            <div className='row multiple_choice_row'>
                                {this.state.answers ?
                                    this.state.answers.map((value, idx) => (
                                        <div key={idx} className='col-md-12 no-pad ques_col'>
                                            <div className="col-xs-9">
                                                <div className="csform-group">
                                                    <label className='pd_l_15'>Answer {value.lebel}:</label>
                                                    <input type="text" name={'answer' + idx} className="csForm_control" data-rule-required="true" value={value.value} onChange={(e) => this.multiple_answer_Change(this, 'answers', idx, 'value', e.target.value)} />
                                                </div>
                                            </div>
                                            <div className='col-xs-3 text-center'>
                                                <p className="radio_form_control1_ mr_t_30">
                                                    <input type="checkbox" id={'ans_' + idx}
                                                        name="radio-group"
                                                        checked={value.checked == "1" ? 'checked' : ''}
                                                        onChange={(e) => this.multiple_answer_Change(this, 'answers', idx, 'checked', e.target.checked)}
                                                        data-rule-required="true"

                                                    />
                                                    <label htmlFor={'ans_' + idx}></label>
                                                </p>
                                            </div>
                                        </div>))
                                    : ''}
                                {/* ques_col ends */}
                            </div>
                            {/* multiple_choice_row ends */}
                            <div className='row trnMod_Foot__'>
                                <div className='col-sm-12 no-pad text-right'>
                                    <button type="submit" disabled={this.state.loading} className="btn cmn-btn1 create_quesBtn">Save and Create Question</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
           
        )
    }
}

export default AddQuestion;