import React, { Component } from 'react';
import 'react-select-plus/dist/react-select-plus.css';
import jQuery from "jquery";
import "react-datepicker/dist/react-datepicker.css";
import { ROUTER_PATH, BASE_URL } from '../../../config.js';
import { postData, handleChangeChkboxInput } from '../../../service/common.js';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../../../service/jquery.validate.js';
import "../../../service/custom_script.js";
import { ToastUndo } from 'service/ToastUndo.js'

import { setDepartmentData } from './actions/RecruitmentAction.js';
import { connect } from 'react-redux'

class AddStaffMember extends Component {

    constructor() {
        super();
        this.state = {
            filterVal: '',
            statusSelect: '',
            locationSel: '',
            filterSearch: '',
            loading: false,
            department_option: [],
            department_error: false,
            PhoneInput: [{ name: '' }],
            EmailInput: [{ name: '' }],
            edit_mode: false
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps.staffData) {
            this.setState(newProps.staffData);
            this.setState({ edit_mode: true });
        } else {
            this.setState({ EmailInput: [{ name: '' }], PhoneInput: [{ name: '' }], username: '', firstname: '', lastname: '', position: '' });
        }
        this.setState({ pageTitle: newProps.title })
    }

    handleAddShareholder = (e, tagType) => {
        e.preventDefault();
        if (tagType === 'PhoneInput') {
            this.setState({ PhoneInput: this.state.PhoneInput.concat([{ name: '' }]) });
        } else {
            this.setState({ EmailInput: this.state.EmailInput.concat([{ name: '' }]) });
        }
    }

    handleRemoveShareholder = (e, idx, tagType) => {
        e.preventDefault();
        if (tagType === 'PhoneInput') {
            this.setState({ PhoneInput: this.state.PhoneInput.filter((s, sidx) => idx !== sidx) });
        } else {
            this.setState({ EmailInput: this.state.EmailInput.filter((s, sidx) => idx !== sidx) });
        }
    }


    selectChange = (selectedOption) => {
        this.setState({ department: selectedOption, department_error: false });
    }

    onSubmit = (e) => {
        e.preventDefault();
        var isSubmit = 1;
        jQuery("#create_user").validate({ /* */ });
        var customValid = true;
        var state = {};
        /* if(!this.state.department)
         {  
             isSubmit = 0;
             state['department'+'_error'] = true;          
         }*/

        if (!this.state.loading && jQuery("#create_user").valid() && isSubmit) {
            this.setState({ loading: true });
            postData('recruitment/RecruitmentDashboard/create_user', this.state).then((result) => {
                if (result.status) {
                    this.props.closeModal(true);
                    toast.success((this.state.edit_mode) ?<ToastUndo message={'User updated successfully'} showType={'s'} />:<ToastUndo message={'User updated successfully'} showType={'s'} />, {
                    // toast.success((this.state.edit_mode) ? "User updated successfully" : "User created successfully", {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });

                    //setTimeout(() => this.setState({success: true}), 2000);
                    //
                } else {
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                    // toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
                this.setState({ loading: false });
            });
        }
    }

    /*componentDidMount()
    {
        this.setState({department_option:this.props.DepartmentData});
        //DepartmentData is coming from redux
    }*/

    handleShareholderNameChange = (idx, evt, tagType) => {
        let list = this.state[tagType]
        var state = {}

        if (tagType == 'EmailInput') {
            list[idx].name = evt.target.value.replace(/\s/g, '');
        } else {
            list[idx].name = evt.target.value;
        }

        state[tagType] = list
        this.setState(state);
    }

    callHtmlForValidation = (currentState) => {
        return (this.state[currentState + '_error']) ? <div className={'tooltip fade top in' + ((this.state[currentState + '_error']) ? ' select-validation-error' : '')} role="tooltip">
            <div className="tooltip-arrow"></div><div className="tooltip-inner">This field is required.</div></div> : '';
    }
    render() {

        var options = [
            { value: 'one', label: 'Group Interview' },
            { value: 'two', label: 'Personal Interview', clearableValue: false }
        ];

        return (
            <div className={this.props.showModal ? 'customModal show' : 'customModal'}>
                <div className="cstomDialog widBig">

                    <h3 className="cstmModal_hdng1--">
                        {this.state.pageTitle}
                        <span className="closeModal  icon icon-close1-ie" onClick={() => this.props.closeModal(false)}></span>
                    </h3>

                    <form id="create_user" autoComplete="off">

                        <div className="row bor_row_bef">

                            <div className="col-md-4">
                                <div className="csform-group">
                                    <label>Name:</label>
                                    <input type="text" className="csForm_control" placeholder="First name" name="firstname"
                                        onChange={(e) => handleChangeChkboxInput(this, e)} value={this.state.firstname || ''} data-rule-required="true" maxLength="30" data-msg-required="Add First Name" />
                                </div>
                            </div>

                            <div className="col-md-4">
                                <div className="csform-group">
                                    <label></label>
                                    <div className="cmn_select_dv">
                                        <input type="text" className="csForm_control" placeholder="Last name" name="lastname"
                                            onChange={(e) => handleChangeChkboxInput(this, e)} value={this.state.lastname || ''} data-rule-required="true" maxLength="30" data-msg-required="Add Last Name" />
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-4">
                                <div className="csform-group">
                                    <label>Position:</label>
                                    <div className="cmn_select_dv">
                                        <input type="text" className="csForm_control" placeholder="Position" name="position" onChange={(e) => handleChangeChkboxInput(this, e)} value={this.state.position || ''} data-rule-required="true" maxLength="30" data-msg-required="Enter Position" />
                                    </div>
                                </div>
                            </div>

                            {/*<div className="col-md-3">
                                <div className="csform-group">
                                    <label>Department:</label>
                                    <div className="cmn_select_dv">
                                        
                                        <Select className="custom_select" required name="department" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.department || ''} onChange={this.selectChange} options={this.state.department_option} placeholder="Department" />
                                         {this.callHtmlForValidation('department')}
                                    </div>
                                </div>
                            </div>*/}

                            <div className="col-md-3 ">
                                <div className="csform-group">
                                    <label>Phone:</label>

                                    {this.state.PhoneInput.map((PhoneInput, idx) => (
                                        <div className="ad_dv" key={idx + 1}>
                                            <input className="csForm_control distinctPhone" type="text"
                                                value={PhoneInput.name || ''}
                                                name={'phone_' + idx}
                                                placeholder="Can include area code"
                                                onChange={(evt) => this.handleShareholderNameChange(idx, evt, 'PhoneInput')}
                                                data-rule-required="true"
                                                maxLength="18"
                                                data-rule-notequaltogroup='[".distinctPhone"]'
                                                data-msg-notequaltogroup='Please enter uniqe phone number'
                                                data-msg-required="Add Phone Number"
                                            />

                                            {idx > 0 ? <button className='btn btn_AddFld_cmn' onClick={(e) => this.handleRemoveShareholder(e, idx, 'PhoneInput')}>
                                                <i className="icon icon-decrease-icon icn_remove" ></i>
                                            </button> : (this.state.PhoneInput.length == 3) ? '' : <button className='btn btn_AddFld_cmn' onClick={(e) => this.handleAddShareholder(e, 'PhoneInput')}>
                                                <i className="icon icon-add-icons icn_add" ></i>
                                            </button>}

                                        </div>
                                    ))}
                                </div>
                            </div>

                            <div className="col-md-3 ">
                                <div className="csform-group">
                                    <label>Email:</label>
                                    {this.state.EmailInput.map((EmailInput, idx) => (
                                        <div className="ad_dv" key={idx + 1}>
                                            <input className="csForm_control distinctEmail" type="text"
                                                value={EmailInput.name || ''}
                                                name={'email_' + idx}
                                                placeholder={idx > 0 ? 'Secondary email' : 'Primary Email'}
                                                onChange={(evt) => this.handleShareholderNameChange(idx, evt, 'EmailInput')}
                                                data-rule-required="true"
                                                data-rule-email="true"
                                                data-rule-remote={BASE_URL + 'recruitment/RecruitmentDashboard/check_user_emailaddress_already_exist' + ((this.state.edit_mode) ? '?adminId=' + this.state.id : '')}
                                                data-msg-remote="This email already exist"
                                                data-rule-notequaltogroup='[".distinctEmail"]'
                                                maxLength="70"
                                                data-msg-required="Add Email Address"
                                            />
                                            {idx > 0 ? <button onClick={(e) => this.handleRemoveShareholder(e, idx, 'EmailInput')} className='btn btn_AddFld_cmn'>
                                                <i className="icon icon-decrease-icon icn_remove" ></i>
                                            </button> : (this.state.EmailInput.length == 3) ? '' : <button className='btn btn_AddFld_cmn' onClick={(e) => this.handleAddShareholder(e, 'EmailInput')}>
                                                <i className="icon icon-add-icons icn_add" ></i>
                                            </button>}
                                        </div>
                                    ))}
                                </div>
                            </div>

                            <div className="col-md-3">
                                <label>Username:</label>
                                <div className="csform-group ">
                                    <input className="csForm_control input_f mb-1" type="text"
                                        value={this.state.username || ''}
                                        name="username"
                                        placeholder="Username"
                                        onChange={(e) => this.setState({ username: e.target.value.replace(/\s/g, '') })}
                                        data-rule-required="true"
                                        data-rule-minlength="6"
                                        maxLength="25"
                                        readOnly={(this.state.edit_mode)}
                                        data-rule-remote={BASE_URL + 'recruitment/RecruitmentDashboard/check_username_already_exist/' + ((this.state.edit_mode) ? '?adminId=' + this.state.id : '')}
                                        data-msg-remote="This username already exist"
                                    />
                                </div>
                            </div>
                            <div className="col-md-3">
                                <label>Password:</label>
                                <div className="csform-group">
                                    <input className="csForm_control input_f mb-1" type="password"
                                        value={this.state.password || ''}
                                        name="password"
                                        maxLength="20"

                                        placeholder="Password"
                                        onChange={(e) => handleChangeChkboxInput(this, e)}
                                        data-rule-required={this.state.edit_mode ? false : true}
                                        data-rule-minlength="6" autoComplete="new-password"
                                    />
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="bor_line"></div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <span className="">
                                    <button disabled={this.state.loading} onClick={this.onSubmit} className="btn cmn-btn1 creat_task_btn__">Save User</button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    DepartmentData: state.RecruitmentReducer.departmentData
})

const mapDispatchtoProps = (dispach) => {
    return {
        //departmentData: (value) => dispach(setDepartmentData(value)),

    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(AddStaffMember)