import React, { Component } from 'react';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';

class RecruiterDisable extends Component {

    constructor() {
        super();
        this.state = {
            disableSel:'',
            allocationSel:''
        }
    }



    render() {
        var disabeSelOptions = [
            { value: 'one', label: 'Temp Pause User Account' },
            { value: 'two', label: 'Permanently Disable' },
            { value: 'three', label: 'Schedule pause or disable' }
        ];

        var allocateSelOptions = [
            { value: 'one', label: 'Next Available Staff Member' },
            { value: 'two', label: 'By department Preference' },
            { value: 'three', label: 'Custom Selection (Search)' }
        ];

        return (
            <div className={'customModal ' + (this.props.showDisModal? ' show' : '')}>
                <div className="cstomDialog widBig">

                    <h3 className="cstmModal_hdng1--">
                        Disable Recruiter
                            <span className="closeModal icon icon-close1-ie" onClick={this.props.closeDisModal}></span>
                    </h3>

                    <form>


                        <div className='row justify-content-center d-flex'>
                            <div className='col-md-10 '>

                                <div className='row mr_tb_20'>
                                    <div className='col-sm-12'>
                                        <div className="csform-group">
                                            <label className='bg_labs2 mr_b_20'>Disable <strong>Janet Marriot</strong> account:</label>
                                            <div className="cmn_select_dv " style={{width:'250px'}}>
                                                <Select name="view_by_status "
                                                    required={true} simpleValue={true}
                                                    searchable={false} Clearable={false}
                                                    placeholder="Select Type"
                                                    options={disabeSelOptions}
                                                    onChange={(e) => this.setState({ disableSel: e })}
                                                    value={this.state.disableSel}
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    
                                </div>

                                <div className='row mr_tb_20'>
                                    <div className='col-sm-12'>
                                        <div className="csform-group">
                                            <label className='bg_labs2'>Account Allocations:</label>
                                            <p className='mr_b_20'>Where would you like to Re-allocate all recruiters current assigned Incomplete participants to:</p>
                                            <div className="cmn_select_dv "  style={{width:'300px'}}>
                                                <Select name="view_by_status "
                                                    required={true} simpleValue={true}
                                                    searchable={false} Clearable={false}
                                                    placeholder="Select Type"
                                                    options={allocateSelOptions}
                                                    onChange={(e) => this.setState({ allocationSel: e })}
                                                    value={this.state.allocationSel}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div className='row dashedLine_1__ mr_tb_40'></div>

                                <div className='row mr_tb_20'>
                                    <div className='col-sm-12'>
                                        <div className="csform-group">
                                            <label className='bg_labs2 mr_b_20'>Add Relevant Notes:</label>                   
                                            <textarea className="csForm_control txt_area brRad10 textarea-max-size" name="question" data-rule-required="true"></textarea>
                                        </div>
                                    </div>
                                </div>


                                

                            </div>
                        </div>



                        <div className="row trnMod_Foot__ disFoot1__">
                            <div className="col-sm-12 no-pad text-right">
                                <button type="submit" className="btn cmn-btn1 create_quesBtn">Disable</button>
                            </div>
                        </div>

                    </form>


                </div>
            </div>

        );
    }
}

export default RecruiterDisable;

