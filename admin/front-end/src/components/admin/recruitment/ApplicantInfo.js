import React, { Component } from 'react';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { TextBlock, MediaBlock, TextRow, RectShape, RoundShape } from 'react-placeholder/lib/placeholders';
import {Link } from 'react-router-dom';
import ReactPlaceholder from 'react-placeholder';
import { customProfile, customHeading, customNavigation } from '../../../service/CustomContentLoader.js';
import "react-placeholder/lib/reactPlaceholder.css";

import ApplicantNotesModal from './ApplicantNotesModal';
import ApplicantAttachmentModal from './ApplicantAttachmentModal';
import EditApplicantInfo from './EditApplicantInfo';

class ApplicantInfo extends Component {

    constructor() {
        super();
        this.state = {
            filterVal: '',
            ReviewOnline: '',
            timeLineStatus: '',
            documentStatus: 'Inactive',
            InterviewStatus: '',
            grpInterviewStatus: '',
            refcheckStatus: '',
            hiredStatus: '',
            cabStatus: '',
            loading: true,
            ApplicantNotes:false,
            ApplicantAttachment:false,
            EditApplicant:false
        }
    }

    changeIntStatus = (key, value) => {
        var state = {};
        state[key] = value;
        state[key + 'Color'] = value;
        this.setState(state);
    }


    componentDidMount() {
        setTimeout(() => {
            this.setState({ loading: false })
        }, 700)
    }

    render() {

        var options = [
            { value: 'one', label: 'Skilled' },
            { value: 'two', label: 'Unskilled', }
        ];

        var Timelineoptions = [
            { value: 'success', label: 'Successfull' },
            { value: 'pending', label: 'Pending', },
            { value: 'cancelled', label: 'Cancelled', }
        ];

        var Documentoptions = [
            { value: 'one', label: 'Active' },
            { value: 'two', label: 'Inactive', }
        ];

        const topPlaceholder = (
            <div className='row bor_bot1 pd_tb_15'>
                <div className='col-sm-5'>

                    <div className='row'>
                        <div className='col-md-7 no_pd_l '>
                            <div className='apli_nmDv'>
                                <h2>
                                    <span><b><RoundShape color='#b1b1b1' style={{ width: '200px', height: 20 }} /></b> </span> &nbsp;
                                    <span><RoundShape color='#d5d3d3' style={{ width: '100px', height: 20 }} /></span>
                                </h2>
                                <p><b><RectShape color='#b1b1b1' style={{ width: '70%', height: 13,marginTop:'10px' }} /></b> </p>
                            </div>

                            <div>

                                <h4><b><RoundShape color='#b1b1b1' style={{ width: '100px', height: 20 }} /></b></h4>
                                <div className='cntct_info'>

                                    <table style={{ width: '90%', marginTop: '15px' }}>
                                        <tbody>
                                            <tr>
                                                <td width={'30%'}><RectShape color='#b1b1b1' style={{ width: '100%', height: 20, borderTopLeftRadius: '10px', borderBottomLeftRadius: '10px' }} /></td>
                                                <td><RectShape color='#d5d3d3' style={{ width: '100%', height: 20, borderTopRightRadius: '10px', borderBottomRightRadius: '10px' }} /></td>
                                            </tr>
                                            <tr>
                                                <td width={'30%'}><RectShape color='#b1b1b1' style={{ width: '100%', height: 20, borderTopLeftRadius: '10px', borderBottomLeftRadius: '10px' }} /></td>
                                                <td><RectShape color='#d5d3d3' style={{ width: '100%', height: 20, borderTopRightRadius: '10px', borderBottomRightRadius: '10px' }} /></td>
                                            </tr>
                                            <tr>
                                                <td width={'30%'}><RectShape color='#b1b1b1' style={{ width: '100%', height: 20, borderTopLeftRadius: '10px', borderBottomLeftRadius: '10px' }} /></td>
                                                <td><RectShape color='#d5d3d3' style={{ width: '100%', height: 20, borderTopRightRadius: '10px', borderBottomRightRadius: '10px' }} /></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>

                                <RoundShape color='#d5d3d3' style={{ width: '200px', height: 35, marginTop: '15px' }} />

                            </div>
                            {/* row ends */}

                        </div>

                        <div className='col-md-5 text-center'>
                            <div className='form-group'>
                                <p ><RoundShape color='#d5d3d3' style={{ width: '60%', height: 13, marginLeft: '20%' }} /></p>

                                <div className="filter_fields__ cmn_select_dv apInfo_Slct" style={{ marginTop: '15px' }}>
                                    <RoundShape color='#b1b1b1' style={{ width: '100%', height: 40 }} />
                                </div>
                            </div>
                            <div className='form-group'>
                                <p ><RoundShape color='#d5d3d3' style={{ width: '60%', height: 13, marginLeft: '20%',marginTop:'25px' }} /></p>

                                <div className="filter_fields__ cmn_select_dv apInfo_Slct" style={{ marginTop: '15px' }}>
                                    <RoundShape color='#b1b1b1' style={{ width: '100%', height: 40 }} />
                                </div>
                            </div>
                        </div>

                    </div>




                </div>

                <div className='col-sm-7'>

                    <div className='row'>

                        <div className='col-lg-4 col-md-6 col-sm-6 bor_left '>
                            <div className='srve_dwdnld_col col_hgCmn'>
                                <h4><RectShape color='#d5d3d3' style={{ width: '200px', height: 15 }} /></h4>

                                <ul className='srve_li23'>
                                    <li>
                                        <div className='srve_liDv'>
                                            <RoundShape color='#d5d3d3' style={{ width: '50%', height: 35, marginRight: '10px' }} />
                                            <span className=''><RoundShape color='#d5d3d3' style={{ width: 35, height: 35 }} /></span>
                                        </div>
                                    </li>

                                    <li>
                                        <div className='srve_liDv'>
                                            <RoundShape color='#d5d3d3' style={{ width: '50%', height: 35, marginRight: '10px' }} />
                                            <span className=''><RoundShape color='#d5d3d3' style={{ width: 35, height: 35 }} /></span>
                                        </div>
                                    </li>

                                    <li>
                                        <div className='srve_liDv'>
                                            <RoundShape color='#d5d3d3' style={{ width: '50%', height: 35, marginRight: '10px' }} />
                                            <span className=''><RoundShape color='#d5d3d3' style={{ width: 35, height: 35 }} /></span>
                                        </div>
                                    </li>

                                    <li>
                                        <div className='srve_liDv'>
                                            <RoundShape color='#b1b1b1' style={{ width: '73%', height: 35 }} />
                                        </div>
                                    </li>

                                </ul>
                            </div>

                        </div>
                        {/* col-md-4 ends */}

                        <div className='col-lg-4 col-md-6 col-sm-6 bor_left2'>

                            <div className='srve_dwdnld_col col_hgCmn'>
                                <h4><RectShape color='#d5d3d3' style={{ width: '200px', height: 15 }} /></h4>

                                <ul className='srve_li23'>
                                    <li>
                                        <div className='srve_liDv'>
                                            <RoundShape color='#d5d3d3' style={{ width: '60%', height: 35, marginRight: '10px' }} />
                                            <span className=''><RoundShape color='#d5d3d3' style={{ width: 35, height: 35 }} /></span>
                                        </div>
                                    </li>

                                    <li>
                                        <div className='srve_liDv'>
                                            <RoundShape color='#d5d3d3' style={{ width: '60%', height: 35, marginRight: '10px' }} />
                                            <span className=''><RoundShape color='#d5d3d3' style={{ width: 35, height: 35 }} /></span>
                                        </div>
                                    </li>

                                    <li>
                                        <div className='srve_liDv'>
                                            <RoundShape color='#d5d3d3' style={{ width: '60%', height: 35, marginRight: '10px' }} />
                                            <span className=''><RoundShape color='#d5d3d3' style={{ width: 35, height: 35 }} /></span>
                                        </div>
                                    </li>

                                    <li>
                                        <div className='srve_liDv'>
                                            <RoundShape color='#b1b1b1' style={{ width: '80%', height: 35 }} />
                                        </div>
                                    </li>

                                </ul>
                            </div>


                        </div>
                        {/* col-md-4 ends */}

                        <div className='col-lg-4 col-md-12 col-sm-12 notify_bor'>

                            <div className='srve_dwdnld_col col_hgCmn noty_col'>

                                <ul className='notify_ul__'>
                                    <li>
                                        <RoundShape color='#d5d3d3' style={{ width: 50, height: 50, margin: '0 auto' }} />
                                        <RoundShape color='#b1b1b1' style={{ width: 50, height: 15, margin: '3px auto' }} />
                                    </li>
                                    <li>
                                        <RoundShape color='#d5d3d3' style={{ width: 50, height: 50, margin: '0 auto' }} />
                                        <RoundShape color='#b1b1b1' style={{ width: 50, height: 15, margin: '3px auto' }} />
                                    </li>
                                    <li>
                                        <RoundShape color='#d5d3d3' style={{ width: 50, height: 50, margin: '0 auto' }} />
                                        <RoundShape color='#b1b1b1' style={{ width: 50, height: 15, margin: '3px auto' }} />
                                    </li>
                                    <li>
                                        <RoundShape color='#d5d3d3' style={{ width: 50, height: 50, margin: '0 auto' }} />
                                        <RoundShape color='#b1b1b1' style={{ width: 50, height: 15, margin: '3px auto' }} />
                                    </li>
                                </ul>
                                <a href='/Applicants/ApplicantResult'>
                                    <RoundShape color='#b1b1b1' style={{ width: '100%', height: 35 }} />
                                </a>

                            </div>


                        </div>
                        {/* col-md-4 ends */}


                    </div>
                    {/* row ends */}

                </div>




            </div>
        );


        return (
            <React.Fragment>
              
                    <div className="row">
                        <div className="col-lg-12 col-md-12 no-pad back_col_cmn-">
                            <Link to="../applicants">
                                <span className="icon icon-back1-ie"></span>
                            </Link>

                        </div>
                    </div>
                    {/* row ends */}

                    <div className="row">
                        <div className="col-lg-12 col-md-12 main_heading_cmn-">
                            <h1>Applicant Info</h1>
                        </div>
                    </div>
                    {/* row ends */}
                    <ReactPlaceholder showLoadingAnimation ready={!this.state.loading} customPlaceholder={topPlaceholder}>
                        <div className='row bor_bot1 pd_tb_15'>

                            <div className='col-sm-5'>

                                <div className='row'>



                                    <div className='col-md-7 no_pd_l '>

                                        <div className='apli_nmDv'>
                                            <h2>
                                                <b>Susan McSmith</b> &nbsp;
                                                    <span>(ID: APP-10844)</span>
                                            </h2>
                                            <p><b>Application: </b> Qualified Carer (Casual)</p>
                                        </div>

                                        <div>
                                            <h4><b>Contact</b></h4>
                                            <div className='cntct_info'>

                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td width={'30%'}>Phone:</td>
                                                            <td>04879 99221</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email:</td>
                                                            <td>susan.mcsmith@gmail.com</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Address:</td>
                                                            <td>342 John Street, South Melbourne, 3000, VIC</td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </div>

                                            <button className='btn cmn-btn1 edit_aplctn' onClick={()=>{this.setState({EditApplicant:true})}}>Edit Applicant Info</button>
                                        </div>


                                    </div>

                                    <div className='col-md-5 text-center'>
                                        <div className='form-group'>
                                            <p>Applicant Classification:</p>
                                            <div className="filter_fields__ cmn_select_dv apInfo_Slct">
                                                <Select name="view_by_status "
                                                    required={true} simpleValue={true}
                                                    searchable={false} Clearable={false}
                                                    placeholder="Skilled"
                                                    options={options}
                                                    onChange={(e) => this.setState({ filterVal: e })}
                                                    value={this.state.filterVal}

                                                />
                                            </div>
                                        </div>
                                        <div className='form-group'>
                                            <p>Applicant Status:</p>
                                            <div className="filter_fields__ cmn_select_dv apInfo_Slct">
                                                <Select name="view_by_status "
                                                    required={true} simpleValue={true}
                                                    searchable={false} Clearable={false}
                                                    placeholder="Skilled"
                                                    options={options}
                                                    onChange={(e) => this.setState({ filterVal: e })}
                                                    value={this.state.filterVal}

                                                />
                                            </div>
                                        </div>
                                    </div>

                                </div>{/* row ends */}


                            </div>



                            <div className='col-sm-7'>

                                <div className='row'>

                                    <div className='col-lg-4 col-md-6 col-sm-6 bor_left '>
                                        <div className='srve_dwdnld_col col_hgCmn'>
                                            <h4>Communication logs:</h4>

                                            <ul className='srve_li23'>
                                                <li>
                                                    <div className='srve_liDv'>
                                                        <button className='btn cmn-btn1 eye-btn s_bt'>SMS</button>
                                                        <span className='icon icon-download1-ie dwndldie'></span>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div className='srve_liDv'>
                                                        <button className='btn cmn-btn1 eye-btn s_bt'>Email</button>
                                                        <span className='icon icon-download1-ie dwndldie'></span>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div className='srve_liDv'>
                                                        <button className='btn cmn-btn1 eye-btn s_bt'>Phone</button>
                                                        <span className='icon icon-download1-ie dwndldie'></span>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div className='srve_liDv'>
                                                        <button className='btn cmn-btn1 eye-btn browse_btn b_bt'>Browse</button>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>

                                    </div>
                                    {/* col-md-4 ends */}

                                    <div className='col-lg-4 col-md-6 col-sm-6 bor_left2'>

                                        <div className='srve_dwdnld_col col_hgCmn'>
                                            <h4>Application Attachments:</h4>

                                            <ul className='srve_li23'>
                                                <li>
                                                    <div className='srve_liDv'>
                                                        <button className='btn cmn-btn1 eye-btn s_bt2'>Resume</button>
                                                        <span className='icon icon-download1-ie dwndldie'></span>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div className='srve_liDv'>
                                                        <button className='btn cmn-btn1 eye-btn s_bt2'>Cover Letter</button>
                                                        <span className='icon icon-download1-ie dwndldie'></span>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div className='srve_liDv'>
                                                        <button className='btn cmn-btn1 eye-btn s_bt2'>Qualifications</button>
                                                        <span className='icon icon-download1-ie dwndldie'></span>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div className='srve_liDv'>
                                                        <button className='btn cmn-btn1 eye-btn browse_btn b_bt2'>Browse</button>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>


                                    </div>
                                    {/* col-md-4 ends */}

                                    <div className='col-lg-4 col-md-12 col-sm-12 notify_bor'>

                                        <div className='srve_dwdnld_col col_hgCmn noty_col'>

                                            <ul className='notify_ul__'>
                                                <li>
                                                    <i className='icon icon-notification1-ie'></i>
                                                    <div>Alerts</div>
                                                </li>
                                                <li>
                                                    <i className='icon icon-notes1-ie'></i>
                                                    <div>Notes</div>
                                                </li>
                                                <li>
                                                    <i className='icon icon-edit6-ie'></i>
                                                    <div>To Do</div>
                                                </li>
                                                <li>
                                                    <i className='icon icon-attatchments2-ie'></i>
                                                    <div>Alerts</div>
                                                </li>
                                            </ul>
                                            <a href='./applicant_result'>
                                                <button className='btn cmn-btn1 quizRslt_btn'>View Quiz Results</button>
                                            </a>

                                        </div>


                                    </div>
                                    {/* col-md-4 ends */}


                                </div>
                                {/* row ends */}

                            </div>
                        </div>
                        {/* row ends */}
                    </ReactPlaceholder>

                    <div className='row timeLine_row__'>

                        <div className='main_timelineBox'>

                            <div className='timeLine_hdng'>
                                <div className='hdng_pos11'>
                                    <h3><b>Application Submission</b></h3>
                                    <p>Through: Seek</p>
                                    <p>Date: 01/01/01 - 9:42am</p>

                                </div>
                                <div className='clearfix'></div>
                            </div>

                            <div className='timeline_step done'>
                                <div className='timeline_stepNum '>1</div>

                                <div className='boxesDiv'>
                                    <div className='timeLine_detaiBox '>
                                        <h4>Stage 1 - Review Online Application</h4>


                                        <div className="filter_fields__ cmn_select_dv timeLine_slct">
                                            <Select name="view_by_status "
                                                required={true} simpleValue={true}
                                                searchable={false} Clearable={false}
                                                placeholder="Select"
                                                options={Timelineoptions}
                                                onChange={(value) => this.changeIntStatus('ReviewOnline', value)}
                                                value={this.state.ReviewOnline}
                                                className={this.state.ReviewOnline}
                                            />
                                        </div>



                                        <div className='btn_li'>
                                            <button className='btn cmn-btn1 eye-btn'>Resume</button>
                                            <button className='btn cmn-btn1 eye-btn'>Cover Letter</button>
                                            <button className='btn cmn-btn1 eye-btn'>Qualification</button>
                                        </div>

                                        <ul className="subTasks_Action__ tsk_center">
                                          
                                            <li onClick={()=>{this.setState({ApplicantNotes:true})}}><span className="sbTsk_li">Notes</span></li>
                                            <li onClick={()=>{this.setState({ApplicantAttachment:true})}}><span className="sbTsk_li">Attachments</span></li>
                                        </ul>
                                    </div>

                                    <div className='timeLine_detaiBox '>
                                        <h4>Stage 1.1 - Review Online Application</h4>


                                        <div className="filter_fields__ cmn_select_dv timeLine_slct">
                                            <Select name="view_by_status "
                                                required={true} simpleValue={true}
                                                searchable={false} Clearable={false}
                                                placeholder="Select"
                                                options={Timelineoptions}
                                                onChange={(value) => this.changeIntStatus('ReviewOnline', value)}
                                                value={this.state.ReviewOnline}
                                                className={this.state.ReviewOnline}
                                            />
                                        </div>



                                        <div className='btn_li'>
                                            <button className='btn cmn-btn1 eye-btn'>Resume</button>
                                            <button className='btn cmn-btn1 eye-btn'>Cover Letter</button>
                                            <button className='btn cmn-btn1 eye-btn'>Qualification</button>
                                        </div>

                                        <ul className="subTasks_Action__ tsk_center">
                                           
                                            <li><span className="sbTsk_li">Notes</span></li>
                                            <li><span className="sbTsk_li">Attachments</span></li>
                                        </ul>
                                    </div>

                                </div>
                                <div className='dareStatus'>
                                    <p>Date Completed: 01/01/01</p>
                                </div>
                            </div>
                            {/* timeline_step ends */}

                            <div className='timeline_step done'>
                                <div className='timeline_stepNum'>2</div>

                                <div className='boxesDiv'>
                                    <div className='timeLine_detaiBox'>
                                        <h4>Stage 2 - Phone Interview</h4>

                                        <div className="filter_fields__ cmn_select_dv timeLine_slct">
                                            <Select name="view_by_status "
                                                required={true} simpleValue={true}
                                                searchable={false} Clearable={false}
                                                placeholder="Select"
                                                options={Timelineoptions}
                                                onChange={(value) => this.changeIntStatus('InterviewStatus', value)}
                                                value={this.state.InterviewStatus}
                                                className={this.state.InterviewStatus}
                                            />
                                        </div>

                                        <div className='btn_li'>
                                            <button className='btn cmn-btn1 eye-btn'>Call Log</button>
                                            <button className='btn cmn-btn1 eye-btn'>Emails</button>
                                        </div>



                                        <ul className="subTasks_Action__ tsk_center">
                                            <li><span className="sbTsk_li">Notes</span></li>
                                            <li><span className="sbTsk_li">Attachments</span></li>
                                            <li><span className="sbTsk_li">Edit Applicant</span></li>
                                        </ul>

                                    </div>
                                </div>
                                <div className='dareStatus'>
                                    <p>Date Completed: 01/01/01</p>
                                </div>
                            </div>
                            {/* timeline_step ends */}

                            <div className='timeline_step '>
                                <div className='timeline_stepNum'>3</div>
                                <div className='boxesDiv'>
                                    <div className='timeLine_detaiBox'>
                                        <h4>Stage 3 - Group Interview</h4>

                                        <div className="filter_fields__ cmn_select_dv timeLine_slct">
                                            <Select name="view_by_status "
                                                required={true} simpleValue={true}
                                                searchable={false} Clearable={false}
                                                placeholder="Select"
                                                options={Timelineoptions}
                                                onChange={(value) => this.changeIntStatus('grpInterviewStatus', value)}
                                                value={this.state.grpInterviewStatus}
                                                className={this.state.grpInterviewStatus}
                                            />
                                        </div>
                                        <div className='awaitMsg'>Awaiting Confirmation</div>
                                        <button className='btn cmn-btn2 rmndr_btn'>Send Reminder SMS</button>

                                        <div className="add_task22">
                                            <span className='icon icon-add2-ie'></span>
                                        </div>

                                        <ul className="subTasks_Action__ tsk_center">
                                            <li><span className="sbTsk_li">Notes</span></li>
                                            <li><span className="sbTsk_li">Attachments</span></li>
                                            <li><span className="sbTsk_li">Edit Applicant</span></li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            {/* timeline_step ends */}

                            <div className='timeline_step '>
                                <div className='timeline_stepNum'>4</div>

                                <div className='boxesDiv'>
                                    <div className='timeLine_detaiBox'>
                                        <h4>Stage 4 - Mandatory Documentation</h4>
                                        <div className="filter_fields__ cmn_select_dv timeLine_slct">
                                            <Select name="view_by_status "
                                                required={true} simpleValue={true}
                                                searchable={false} Clearable={false}
                                                placeholder="Select"
                                                options={Documentoptions}
                                                onChange={(e) => this.setState({ documentStatus: e })}
                                                value={this.state.documentStatus}

                                            />
                                        </div>

                                        <div className='btn_li'>
                                            <button className='btn cmn-btn2 eye-btn add_btn'>Mandatory Docs</button>
                                            <button className='btn cmn-btn2 eye-btn add_btn'>Security Checks</button>
                                        </div>

                                        <ul className="subTasks_Action__ tsk_center">
                                            <li><span className="sbTsk_li">Notes</span></li>
                                            <li><span className="sbTsk_li">Attachments</span></li>
                                            <li><span className="sbTsk_li">Edit Applicant</span></li>
                                        </ul>

                                    </div>
                                    <div className='timeLine_detaiBox'>
                                        <h4>Stage 4.5 - Position/Awards Levels</h4>

                                        <div className='row awrd_row1__'>
                                            <div className='col-sm-12 positn_drps'>
                                                <div className="filter_fields__ cmn_select_dv bigSlct">
                                                    <Select name="view_by_status "
                                                        required={true} simpleValue={true}
                                                        searchable={false} Clearable={false}
                                                        placeholder="Select"
                                                        options={Documentoptions}
                                                        onChange={(e) => this.setState({ documentStatus: e })}
                                                        value={this.state.documentStatus}

                                                    />
                                                </div>
                                                <span className='add_bgSl__ icon icon-decrease-icon'></span>

                                            </div>
                                            <div className='col-sm-6'>
                                                <div className="filter_fields__ cmn_select_dv bigSlct">
                                                    <Select name="view_by_status "
                                                        required={true} simpleValue={true}
                                                        searchable={false} Clearable={false}
                                                        placeholder="Select"
                                                        options={Documentoptions}
                                                        onChange={(e) => this.setState({ documentStatus: e })}
                                                        value={this.state.documentStatus}

                                                    />
                                                </div>
                                            </div>

                                            <div className='col-sm-6'>
                                                <div className="filter_fields__ cmn_select_dv bigSlct">
                                                    <Select name="view_by_status "
                                                        required={true} simpleValue={true}
                                                        searchable={false} Clearable={false}
                                                        placeholder="Select"
                                                        options={Documentoptions}
                                                        onChange={(e) => this.setState({ documentStatus: e })}
                                                        value={this.state.documentStatus}

                                                    />
                                                </div>
                                            </div>


                                        </div>

                                        <div className='row awrd_row1__'>
                                            <div className='col-sm-12 positn_drps'>
                                                <div className="filter_fields__ cmn_select_dv bigSlct">
                                                    <Select name="view_by_status "
                                                        required={true} simpleValue={true}
                                                        searchable={false} Clearable={false}
                                                        placeholder="Select"
                                                        options={Documentoptions}
                                                        onChange={(e) => this.setState({ documentStatus: e })}
                                                        value={this.state.documentStatus}

                                                    />
                                                </div>
                                                <span className='add_bgSl__ icon icon-event-location'></span>

                                            </div>
                                            <div className='col-sm-6'>
                                                <div className="filter_fields__ cmn_select_dv bigSlct">
                                                    <Select name="view_by_status "
                                                        required={true} simpleValue={true}
                                                        searchable={false} Clearable={false}
                                                        placeholder="Select"
                                                        options={Documentoptions}
                                                        onChange={(e) => this.setState({ documentStatus: e })}
                                                        value={this.state.documentStatus}

                                                    />
                                                </div>
                                            </div>

                                            <div className='col-sm-6'>
                                                <div className="filter_fields__ cmn_select_dv bigSlct">
                                                    <Select name="view_by_status "
                                                        required={true} simpleValue={true}
                                                        searchable={false} Clearable={false}
                                                        placeholder="Select"
                                                        options={Documentoptions}
                                                        onChange={(e) => this.setState({ documentStatus: e })}
                                                        value={this.state.documentStatus}

                                                    />
                                                </div>
                                            </div>


                                        </div>




                                        <ul className="subTasks_Action__ tsk_center">
                                            <li><span className="sbTsk_li">Notes</span></li>
                                            <li><span className="sbTsk_li">Attachments</span></li>
                                            <li><span className="sbTsk_li">Edit Applicant</span></li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            {/* timeline_step ends */}

                            <div className='timeline_step '>
                                <div className='timeline_stepNum'>5</div>
                                <div className='boxesDiv'>
                                    <div className='timeLine_detaiBox'>
                                        <h4>Stage 5 - Reference Checks</h4>
                                        <div className="filter_fields__ cmn_select_dv timeLine_slct">
                                            <Select name="view_by_status "
                                                required={true} simpleValue={true}
                                                searchable={false} Clearable={false}
                                                placeholder="Select"
                                                options={Timelineoptions}
                                                onChange={(value) => this.changeIntStatus('refcheckStatus', value)}
                                                value={this.state.refcheckStatus}
                                                className={this.state.refcheckStatus}
                                            />
                                        </div>
                                        <div className='awaitMsg'>Awaiting Reference check</div>

                                        <div className='btn_li'>
                                            <button className='btn cmn-btn1 eye-btn'>Referee Details</button>
                                            <button className='btn cmn-btn2 eye-btn check_complete'><span>Reference check<br />completed</span></button>
                                        </div>

                                        <ul className="subTasks_Action__ tsk_center">
                                            <li><span className="sbTsk_li">Notes</span></li>
                                            <li><span className="sbTsk_li">Attachments</span></li>
                                            <li><span className="sbTsk_li">Edit Applicant</span></li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            {/* timeline_step ends */}

                            <div className='timeline_step disabled'>
                                <div className='timeline_stepNum'>6</div>
                                <div className='boxesDiv'>
                                    <div className='timeLine_detaiBox'>
                                        <h4>Stage 6 - CAB</h4>
                                        <div className="filter_fields__ cmn_select_dv timeLine_slct">
                                            <Select name="view_by_status "
                                                required={true} simpleValue={true}
                                                searchable={false} Clearable={false}
                                                placeholder="Select"
                                                options={Documentoptions}
                                                onChange={(e) => this.setState({ cabStatus: e })}
                                                value={this.state.cabStatus}

                                            />

                                        </div>

                                        <ul className='cab_doneList'>
                                            <li className='done'>Invitation Sent</li>
                                            <li className='done'>Employment Docs sent</li>
                                            <li className='done'>Employment Docs received </li>
                                            <li className='notDone'>Flag Participant</li>
                                        </ul>

                                        <ul className="subTasks_Action__ tsk_center">
                                            <li><span className="sbTsk_li">Notes</span></li>
                                            <li><span className="sbTsk_li">Attachments</span></li>
                                            <li><span className="sbTsk_li">Edit Applicant</span></li>
                                        </ul>


                                    </div>
                                </div>
                            </div>
                            {/* timeline_step ends */}

                            <div className='timeline_step '>
                                <div className='timeline_stepNum'>7</div>
                                <div className='boxesDiv'>
                                    <div className='timeLine_detaiBox'>
                                        <h4>Stage 7 - Hired</h4>

                                        <div className="filter_fields__ cmn_select_dv timeLine_slct">
                                            <Select name="view_by_status "
                                                required={true} simpleValue={true}
                                                searchable={false} Clearable={false}
                                                placeholder="Select"
                                                options={Documentoptions}
                                                onChange={(e) => this.setState({ hiredStatus: e })}
                                                value={this.state.hiredStatus}

                                            />

                                        </div>

                                        <ul className='cab_doneList'>
                                            <li className='done'>Alocate Shifts</li>
                                        </ul>

                                        <ul className="subTasks_Action__ tsk_center">
                                            <li><span className="sbTsk_li">Notes</span></li>
                                            <li><span className="sbTsk_li">Attachments</span></li>
                                            <li><span className="sbTsk_li">Edit Applicant</span></li>
                                        </ul>


                                    </div>
                                </div>
                            </div>
                            {/* timeline_step ends */}

                        </div>
                        {/* main_timelineBox ends */}


                    </div>
                    {/* row ends */}

                    <ul className="subTasks_Action__ tsk_center">
                        <li><span className="sbTsk_li">Position Analytics</span></li>
                        <li><span className="sbTsk_li">Archive Applicant</span></li>
                        <li><span className="sbTsk_li">Flag Applicant</span></li>
                    </ul>


                    <div className='row FlaggedRow'>

                        <div className='col-md-12 col-sm-12'>
                            <div>
                                <i className='icon icon-flag3'></i>
                                <p><strong>This person has been flagged non suitable permanently</strong></p>
                            </div>

                        </div>


                    </div>

                    <ApplicantNotesModal show={this.state.ApplicantNotes} close ={()=> this.setState({ApplicantNotes:false})} />
                    <ApplicantAttachmentModal show={this.state.ApplicantAttachment} close ={()=> this.setState({ApplicantAttachment:false})}/>
                    <EditApplicantInfo show={this.state.EditApplicant} close={()=> {this.setState({EditApplicant:false})}} />

            </React.Fragment>
        );
    }
}

export default ApplicantInfo;