import React, { Component } from 'react';
import Slider from "react-slick";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Link } from 'react-router-dom';
import CKEditor from "react-ckeditor-component";
import moment from 'moment';
import { ToastContainer, toast } from 'react-toastify';
import { postData, handleChangeChkboxInput, changeTimeZone } from '../../../service/common.js';
import QuickPreviewModal from './QuickPreviewModal';
import { ToastUndo } from 'service/ToastUndo.js'


class CreateJob extends Component {

    constructor() {
        super();
        this.state = {

            Partnersettings: {
                dots: false,
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 1,
               

            },
            PostDate: new Date(),
            job_id: 0,
            EndDate: '',
            content: '',
            job_name: '',
            job_position: '',
            job_phone: '',
            job_email: '',
            job_weblink: '',
            website_link: '',
            job_content: '',
            mode: 'add',
            resTemplates: [
                { name: 'Template 1', thumb: '/assets/images/resume_template.png' },
                { name: 'Template 2', thumb: '/assets/images/resume_template.png' },
                { name: 'Template 3', thumb: '/assets/images/resume_template.png' },
                { name: 'Template 4', thumb: '/assets/images/resume_template.png' },
                { name: 'Template 5', thumb: '/assets/images/resume_template.png' },

            ],
            activeTemplate: 0,

            additionQuesAr: [
                { id: 1, name: 'Ques1', inpValue: '' },
                { id: 2, name: 'Ques2', inpValue: '' },
                { id: 3, name: 'Ques3', inpValue: '' },
                { id: 4, name: 'Ques4', inpValue: '' }
            ],
            quickModal:false

        }
    }

    PostDate = (date) => { this.setState({ PostDate: date }); }

    EndDate = (date) => { this.setState({ EndDate: date }); }


    selectTemplate(i) {
        this.setState({
            activeTemplate: i
        })
    }

    AddQuesInput = () => {

        const { additionQuesAr } = this.state;
        const numRows = additionQuesAr.length;
        const inpQuesObject = { id: numRows + 1, name: 'Ques' + (numRows + 1), inpValue: '' };
        additionQuesAr.push(inpQuesObject);

        this.setState({
            additionQuesAr
        })

    }


    changeQuesText = (e) => {
        const { id, value } = e.target;
        let { additionQuesAr } = this.state;
        const targetIndex = additionQuesAr.findIndex(AdQues => {
            return AdQues.id == id;
        });


        if (targetIndex !== -1) {
            additionQuesAr[targetIndex].inpValue = value;
            this.setState({ additionQuesAr });
        }
    }

    submitjobs = (e) => {
        e.preventDefault();
        console.log(this.state);
    }
    submitjobsTempl = (e) => {
        e.preventDefault();
        console.log(this.state);
        postData('recruitment/Recruitment_job/insert_update_job', this.state).then((result) => {
            if (result.status) {
                toast.success(<ToastUndo message={'New Job is submitted successfully.'} showType={'s'} />, {
                // toast.success('New Job is submitted successfully.', {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });

            } else {
                toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                // toast.error(result.error, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
            }
        });


    }

    onChangeEditor = (evt) => {
        var newContent = evt.editor.getData();

        this.setState({
            job_content: newContent
        })
    }
    render() {

        return (
            <React.Fragment>
              
                    <form id="form_jobs" >
                     

                            <div className="row">
                                <div className="col-lg-12 col-md-12 no-pad back_col_cmn-">
                                    <Link to='/admin/recruitment/job_opening'>
                                        <span className="icon icon-back1-ie"></span>
                                    </Link>
                                </div>
                            </div>
                            {/* row ends */}

                            <div className="row">
                                <div className="col-lg-12 col-md-12 main_heading_cmn-">
                                    <h1>Create New Job</h1>
                                </div>
                            </div>
                            {/* row ends */}

                            <div className='row creaJobRow1__ bor_bot1'>

                                <div className="col-md-7">
                                    <div className="csform-group">
                                        <label>Job Name:</label>
                                        <input type="text" className="csForm_control" name="job_name" value={this.state.job_name} onChange={(e) => handleChangeChkboxInput(this, e)} />
                                    </div>
                                </div>

                                <div className="col-md-5">
                                    <div className="csform-group">
                                        <label>Job Position:</label>
                                        <input type="text" className="csForm_control" name="job_position" value={this.state.job_position} onChange={(e) => handleChangeChkboxInput(this, e)} />
                                    </div>
                                </div>

                                <div className="col-md-4">
                                    <div className="csform-group">
                                        <label>Phone:</label>
                                        <input type="text" className="csForm_control" name="job_phone" value={this.state.job_phone} onChange={(e) => handleChangeChkboxInput(this, e)} />
                                    </div>
                                </div>

                                <div className="col-md-4">
                                    <div className="csform-group">
                                        <label>Email:</label>
                                        <input type="text" className="csForm_control" name="job_email" value={this.state.job_email} onChange={(e) => handleChangeChkboxInput(this, e)} />
                                    </div>
                                </div>

                                <div className="col-md-4">
                                    <div className="csform-group">
                                        <label>Website Link:</label>
                                        <input type="text" className="csForm_control" name="website_link" value={this.state.website_link} onChange={(e) => handleChangeChkboxInput(this, e)} />
                                    </div>
                                </div>

                            </div>
                            {/* creaJobRow1__ ends */}


                            <div className='row creaJobRow1__ bor_bot1'>

                                <div className="col-lg-7 col-md-12 no_pd_l">
                                    <div className="csform-group">
                                        <label>Job Text Layout:</label>
                                        <div className='cstmEditor bigHg'>
                                            <CKEditor activeClass="p10" content={this.state.job_content} events={{ "change": this.onChangeEditor }} />
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-5 col-md-12 no_pd_r">
                                    <div className="csform-group">
                                        <label>Select Job Style Template:</label>
                                        <div className='resume_slider'>
                                            <Slider {...this.state.Partnersettings}>

                                                {
                                                    this.state.resTemplates.map((template, i) => {
                                                        return (
                                                            <div key={i}>
                                                                <div className={this.state.activeTemplate === i ? 'template_slide selected' : 'template_slide'} onClick={this.selectTemplate.bind(this, i)} >
                                                                    <h5>{template.name}</h5>
                                                                    <img src={template.thumb} alt="1" />
                                                                </div>
                                                            </div>
                                                        )
                                                    })
                                                }



                                            </Slider>
                                        </div>


                                        <div className='additional_ques'>
                                            <label>Additional Questions:</label>

                                            {
                                                this.state.additionQuesAr.map((AdQues, i) => {
                                                    return (
                                                        <div className="csform-group" key={i}>
                                                            <input
                                                                type="text"
                                                                className="csForm_control inp_blue"
                                                                placeholder='Type Ques..'
                                                                id={AdQues.id}
                                                                name={AdQues.name}
                                                                onChange={this.changeQuesText}
                                                                value={AdQues.inpValue}
                                                            />
                                                        </div>
                                                    )
                                                })
                                            }


                                        </div>
                                        <div className='add_Ques'>
                                            <span className='add_ic icon icon-add3-ie' onClick={this.AddQuesInput}></span>
                                            <div className='clearfix'></div>
                                        </div>

                                    </div>
                                </div>



                            </div>
                            {/* creaJobRow1__ ends */}

                            <div className='row creaJobRow1__ bor_bot1'>

                                <div className='col-md-6 bor_right'>

                                    <div className='title_frameDv'>
                                        <label>Job Ad Timeframe:</label>
                                        <div className='row'>
                                            <div className='col-sm-6 '>
                                                <div className='csform-group'>
                                                    <DatePicker showYearDropdown scrollableYearDropdown yearDropdownItemNumber={110} dateFormat="DD/MM/YYYY" required={true} data-placement={'bottom'} name="PostDate" onChange={(date) => this.setState({ PostDate: date })} selected={this.state.PostDate ? moment(this.state.PostDate, 'DD-MM-YYYY') : null} className="csForm_control text-center" placeholderText="DD/MM/YYYY" />
                                                </div>
                                            </div>
                                            <div className='col-sm-6'>
                                                <div className='csform-group'>
                                                    <input type='button' className='btn cmn-btn1 pst_btn' value='Post Now' />
                                                </div>
                                            </div>

                                        </div>

                                        <div className='row '>
                                            <div className='col-sm-6'>
                                                <div className='csform-group'>
                                                    <DatePicker showYearDropdown scrollableYearDropdown yearDropdownItemNumber={110} dateFormat="DD/MM/YYYY" required={true} data-placement={'bottom'} name="EndDate" onChange={(date) => this.setState({ EndDate: date })} selected={this.state.EndDate ? moment(this.state.EndDate, 'DD-MM-YYYY') : null} className="csForm_control text-center" placeholderText="DD/MM/YYYY" />
                                                </div>
                                            </div>
                                            <div className='col-sm-6'>
                                                <div className='csform-group'>
                                                    <input type='button' className='btn cmn-btn1 pst_btn' value='Recurring' />
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                                <div className='col-md-6'>

                                    <div className='title_frameDv'>
                                        <label>Publish To:</label>
                                        <div className='row '>

                                            <div className='col-sm-6'>
                                                <div className='csform-group'>
                                                    <input type='button' className='btn cmn-btn1 pst_btn' value='Website' />
                                                </div>
                                            </div>
                                            <div className='col-sm-6'>
                                                <div className='csform-group'>
                                                    <input type='button' className='btn cmn-btn1 pst_btn' value='Seek' />
                                                </div>
                                            </div>

                                            <div className='col-sm-12 '>
                                                <div className='csform-group'>
                                                    <input type="text" className="csForm_control  text-center" placeholder='Internal Position' />
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <div className='col-md-12 mr_tp_20'>

                                    <div className='title_frameDv'>

                                        <div className='row csform-group'>

                                            <div className='col-sm-10 col-sm-offset-1'>
                                                <div className='csform-group'>
                                                    <input type="text" className="csForm_control  text-center" placeholder='Job Weblink: oncall.com.au/careers/current-vacancies/012-8912323-disability-support-worker' name='job_weblink' value={this.state.job_weblink} onChange={(e) => handleChangeChkboxInput(this, e)} />
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>
                            {/* creaJobRow1__ ends */}

                            <div className='row'>

                                <div className='col-md-6 no_pd_l'>
                                    <ul className='subTasks_Action__ left'>
                                        <li><span className='sbTsk_li'>Undo Change</span></li>
                                        <li onClick={()=> this.setState({quickModal:true})}><span className='sbTsk_li'>Quick Preview Ad</span></li>
                                        <li><span className='sbTsk_li'>Clear All Fields</span></li>
                                    </ul>

                                </div>
                                <div className='col-md-6 text-right no_pd_r crJoBtn_Col__'>
                                    <input type='button' className='btn cmn-btn1 crte_svBtn' value='Save As Template' onClick={this.submitjobsTempl} />
                                    <input type='button' className='btn cmn-btn1 crte_svBtn' value='Duplicate Job' />
                                    <input type='button' className='btn cmn-btn1 crte_svBtn' value='Save & Post Job' onClick={this.submitjobs} />
                                </div>

                            </div>
                        
                    </form>
              


                <QuickPreviewModal showModal={this.state.quickModal} closeModal={()=> this.setState({quickModal:false})} />


            </React.Fragment>
        );
    }
}

export default CreateJob;