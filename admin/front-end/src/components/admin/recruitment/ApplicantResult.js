import React, { Component } from 'react';
import {Link} from 'react-router-dom';


class ApplicantResult extends Component {
    render() {
      return (
        <React.Fragment>
           

                    <div className="row">
                        <div className="col-lg-12 col-md-12 no-pad back_col_cmn-">
                            <Link to='../JobOpening'>
                            <span className="icon icon-back1-ie"></span>
                            </Link>
                        </div>
                    </div>
                    {/* row ends */}

                    <div className="row">
                        <div className="col-lg-12 col-md-12 main_heading_cmn-">
                            <h1>Applicant Info - Group Interview Results</h1>   
                        </div>
                    </div>
                    {/* row ends */}

                    <div className='row resInfo_row__'>

                        <div className='col-sm-5'>

                            <div className='apli_info2 bor_bot1 apli_nmDv'>
                                <h4><b>Applicant Info:</b></h4>
                                <h2>
                                        <b>Susan McSmith</b> &nbsp;
                                        <span>(ID: APP-10844)</span>
                                </h2>
                                <h5><b>Application: </b>Qualified Carer (Casual)</h5>
                            </div>


                            <div className='cntct_info cntct_inf2'>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td width={'30%'}><b>Phone:</b></td>
                                                <td>04879 99221</td>
                                            </tr>
                                            <tr>
                                                <td><b>Email:</b></td>
                                                <td>susan.mcsmith@gmail.com</td>
                                            </tr>
                                            <tr>
                                                <td><b>Address:</b></td>
                                                <td>342 John Street, South Melbourne, 3000, VIC</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                            
                        </div>
                        {/* col-sm-5 ends */}

                        <div className='col-sm-3 bor_left_right'>
                            <div className='res_info2D'>
                                <h4><b>CAB Day Info:</b></h4>
                                <ul>
                                    <li><b>Date:</b>01/01/01</li>
                                    <li><b>Recruiter:</b>John Smith</li>
                                    <li><b>Duration:</b>1h:30min</li>
                                    <li><b>Location:</b>Trainging Center</li>
                                </ul>
                                <div><b>Overseen by:</b> Jane McDonald</div>

                            </div>
                        </div>


                        <div className='col-sm-4 '>

                            <h4><b>Results:</b></h4>
                            <ul className='ansRatngUl'>
                                <li className="active">
                                    <div className='labelLit'>
                                        <p>Literacy:</p>
                                        <span>8/10</span>
                                    </div>
                                    <div className='answer_bar'>
                                        <div className='answer bar1' style={{'width' : '80%'}}></div>
                                        <div className='total bar1'></div>
                                    </div>
                                    <div className='iconEye'>
                                        <span className='icon icon-view1-ie'></span>
                                    </div>

                                </li>
                                <li>
                                    <div className='labelLit'>
                                        <p>Maths:</p>
                                        <span>5/10</span>
                                    </div>
                                    <div className='answer_bar'>
                                        <div className='answer bar1' style={{'width' : '50%'}}></div>
                                        <div className='total bar1'></div>
                                    </div>
                                    <div className='iconEye'>
                                        <span className='icon icon-view1-ie'></span>
                                    </div>

                                </li>
                                <li>
                                    <div className='labelLit'>
                                        <p>Emergency:</p>
                                        <span>6/10</span>
                                    </div>
                                    <div className='answer_bar'>
                                        <div className='answer bar1' style={{'width' : '60%'}}></div>
                                        <div className='total bar1'></div>
                                    </div>
                                    <div className='iconEye'>
                                        <span className='icon icon-view1-ie'></span>
                                    </div>

                                </li>
                            </ul>


                        </div>




                    
                    </div>
                    {/* row ends */}



                    <div className='row resColrow'>

                        <div className='col-sm-12 resultColM'>
                            <div className='col_100'>
                                <div className='circQues QuesNo__ '>Q1</div>
                            </div>
                            <div className='quesAns_box__'>
                                <div className='row'>
                                    <div className='col-sm-6 '>
                                        <h4><b>GI-Q01</b></h4>
                                        <div className='qShwcse'>
                                            <h4><b>Question</b></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisienim ad minim veniam?</p>
                                        </div>
                                    </div>
                                    <div className='col-sm-6 bor_left'>
                                        <h4><b>Multiple Choice Answer:</b></h4>
                                        
                                        <ul className='answrShw'>
                                            <li className='wrongAnswer'>
                                                <b>A:</b>
                                                <span>Lorem ipsum</span>
                                                
                                            </li>
                                            <li className='wrongAnswer'>
                                                <b>B:</b>
                                                <span>consectetuer adipiscing elit</span>
                                                
                                            </li>
                                            <li className='wrongAnswer'>
                                                <b>C:</b>
                                                <span>consectetuer adipiscing elit</span>
                                                
                                            </li>
                                            <li className='rightAnswer'>
                                                <b>D:</b>
                                                <span>sed diam nonummy nibh</span>
                                                
                                            </li>
                                        </ul>


                                    </div>
                                    <div className='col-sm-12 '>
                                        <div className='queBxFoot'>
                                        <h4><b>Created by:</b> Jane Marciano (01/01/01 - 3:42pm)</h4>
                                        </div>
                                    </div>
                                </div>
                                {/* row ends */}
                            </div>
                            <div className='col_100'>
                                <div className='circQues checkieRe correct'></div>
                            </div>
                        </div>
                        {/* resultColM ends */}


                        <div className='col-sm-12 resultColM'>
                            <div className='col_100'>
                                <div className='circQues QuesNo__ '>Q2</div>
                            </div>
                            <div className='quesAns_box__'>
                                <div className='row'>
                                    <div className='col-sm-6'>
                                        <h4><b>GI-Q01</b></h4>
                                        <div className='qShwcse'>
                                            <h4><b>Question</b></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisienim ad minim veniam?</p>
                                        </div>
                                    </div>
                                    <div className='col-sm-6 bor_left'>
                                        <h4><b>Multiple Choice Answer:</b></h4>
                                        
                                        <ul className='answrShw'>
                                            <li className='wrongAnswer'>
                                                <b>A:</b>
                                                <span>Lorem ipsum</span>
                                                
                                            </li>
                                            <li className='rightAnswer'>
                                                <b>B:</b>
                                                <span>consectetuer adipiscing elit</span>
                                                
                                            </li>
                                            <li className='wrongAnswer'>
                                                <b>C:</b>
                                                <span>consectetuer adipiscing elit</span>
                                                
                                            </li>
                                            <li className='wrongAnswer'>
                                                <b>D:</b>
                                                <span>sed diam nonummy nibh</span>
                                                
                                            </li>
                                        </ul>


                                    </div>
                                    <div className='col-sm-12 '>
                                        <div className='queBxFoot'>
                                        <h4><b>Created by:</b> Jane Marciano (01/01/01 - 3:42pm)</h4>
                                        </div>
                                    </div>
                                </div>
                                {/* row ends */}
                            </div>
                            <div className='col_100'>
                                <div className='circQues checkieRe unCorrect'></div>
                            </div>
                        </div>
                        {/* resultColM ends */}



                        <div className='col-sm-12 resultColM'>
                            <div className='col_100'>
                                <div className='circQues QuesNo__ '>Q3</div>
                            </div>
                            <div className='quesAns_box__'>
                                <div className='row'>
                                    <div className='col-sm-6'>
                                        <h4><b>GI-Q01</b></h4>
                                        <div className='qShwcse'>
                                            <h4><b>Question</b></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisienim ad minim veniam?</p>
                                        </div>
                                    </div>
                                    <div className='col-sm-6 bor_left'>
                                        <h4><b>Multiple Choice Answer:</b></h4>
                                        
                                        <ul className='answrShw'>
                                            <li className='wrongAnswer'>
                                                <b>A:</b>
                                                <span>Lorem ipsum</span>
                                                
                                            </li>
                                            <li className='wrongAnswer'>
                                                <b>B:</b>
                                                <span>consectetuer adipiscing elit</span>
                                                
                                            </li>
                                            <li className='wrongAnswer'>
                                                <b>C:</b>
                                                <span>consectetuer adipiscing elit</span>
                                                
                                            </li>
                                            <li className='rightAnswer'>
                                                <b>D:</b>
                                                <span>sed diam nonummy nibh</span>
                                                
                                            </li>
                                        </ul>


                                    </div>
                                    <div className='col-sm-12 '>
                                        <div className='queBxFoot'>
                                        <h4><b>Created by:</b> Jane Marciano (01/01/01 - 3:42pm)</h4>
                                        </div>
                                    </div>
                                </div>
                                {/* row ends */}
                            </div>
                            <div className='col_100'>
                                <div className='circQues checkieRe correct'></div>
                            </div>
                        </div>
                        {/* resultColM ends */}



                        <div className='col-sm-12 resultColM'>
                            <div className='col_100'>
                                <div className='circQues QuesNo__ '>Q4</div>
                            </div>
                            <div className='quesAns_box__'>
                                <div className='row'>
                                    <div className='col-sm-6'>
                                        <h4><b>GI-Q01</b></h4>
                                        <div className='qShwcse'>
                                            <h4><b>Question</b></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisienim ad minim veniam?</p>
                                        </div>
                                    </div>
                                    <div className='col-sm-6 bor_left'>
                                        <h4><b>Multiple Choice Answer:</b></h4>
                                        
                                        <ul className='answrShw'>
                                            <li className='wrongAnswer'>
                                                <b>A:</b>
                                                <span>Lorem ipsum</span>
                                                
                                            </li>
                                            <li className='wrongAnswer'>
                                                <b>B:</b>
                                                <span>consectetuer adipiscing elit</span>
                                                
                                            </li>
                                            <li className='wrongAnswer'>
                                                <b>C:</b>
                                                <span>consectetuer adipiscing elit</span>
                                                
                                            </li>
                                            <li className='rightAnswer'>
                                                <b>D:</b>
                                                <span>sed diam nonummy nibh</span>
                                                
                                            </li>
                                        </ul>


                                    </div>
                                    <div className='col-sm-12 '>
                                        <div className='queBxFoot'>
                                        <h4><b>Created by:</b> Jane Marciano (01/01/01 - 3:42pm)</h4>
                                        </div>
                                    </div>
                                </div>
                                {/* row ends */}
                            </div>
                            <div className='col_100'>
                                <div className='circQues checkieRe unCorrect'></div>
                            </div>
                        </div>
                        {/* resultColM ends */}

                    </div> 
                    {/* row ends */}


        </React.Fragment>
        );
    }
}

export default ApplicantResult;