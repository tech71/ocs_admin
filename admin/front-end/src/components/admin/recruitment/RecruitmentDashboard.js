import React, { Component } from 'react';
import { Doughnut, Bar, Line } from 'react-chartjs-2';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import ReactTable from "react-table";
import 'react-table/react-table.css';
// import { Chart } from "react-charts";
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from '../../../config.js';
import Pagination from "../../../service/Pagination.js";
import { connect } from 'react-redux'
import RecruitmentPage from 'components/admin/recruitment/RecruitmentPage';

class Dashboard extends Component {

    constructor() {
        super();
        this.state = {
            searchVal: '',
            filterVal: '',

        }
    }


    render() {



        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two', clearableValue: false }
        ];


        // const Jobsdata = [
        //     {
        //         label: "New",
        //         data: [[0, 1], [1, 2], [2, 4], [3, 2], [4, 7]],

        //     },
        //     {
        //         label: "Overall",
        //         data: [[0, 3], [1, 1], [2, 5], [3, 6], [4, 4]],

        //     }
        // ];

        const Applicantdata = {
            labels: ['New', 'Qualified', 'Job Ready', 'Training Required'],
            datasets: [{
                data: ['167', '250', '333', '250'],
                backgroundColor: ['#ffa082', '#f76d42', '#dc4517', '#b32a00'],

            }],

        };

        const Recruitmentdata = {
            labels: ['', '', ''],
            datasets: [{
                data: ['714', '528', '465'],
                backgroundColor: ['#f76d42', '#dc4517', '#b32a00'],

            }],

        };


        const dashboardData = [
            {
                Action: 'Task',
                DueDate: '01/02/03',
                Assigned: 'Jimmy',
                LastAction: '01/02/03 - by Jimmy',
                Status: 'Pending'
            },
            {
                Action: 'To-Do',
                DueDate: '01/02/03',
                Assigned: 'Tony',
                LastAction: '01/02/03 - by Eric ',
                Status: 'Actioned'
            },
            {
                Action: 'Schedule',
                DueDate: '01/02/03',
                Assigned: 'Jessica',
                LastAction: '01/02/03 - by Jessica',
                Status: 'Pending'
            },

        ]

        const jobsData2 = {
            labels: ['', '', '', '', '', '', ''],
            datasets: [
                {
                    label: 'New',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: '#b32a00',
                    borderColor: '#b32a00',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: '#f76d42',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: '#f76d42',
                    pointHoverBorderColor: '#f76d42',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [65, 59, 80, 81, 56, 55, 40],

                },
                {
                    label: 'Overall',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: '#f76d42',
                    borderColor: '#f76d42',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: '#f76d42',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: '#f76d42',
                    pointHoverBorderColor: '#f76d42',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [50, 35, 58, 74, 20, 55, 100]
                }
            ]
        };




        return (
            <React.Fragment>

                <div className="row">
                    <div className="col-lg-12 col-md-12 no-pad back_col_cmn-">
                        <Link to={ROUTER_PATH + 'admin/dashboard'}><span className="icon icon-back1-ie"></span></Link>
                    </div>
                </div>
                {/* row ends */}

                <RecruitmentPage pageTypeParms={'dashboard'}/>
                <div className="row">
                    <div className="col-lg-12 col-md-12 main_heading_cmn-">
                        <h1> {this.props.showPageTitle}
                            <Link to='./user_management'>
                                <button className="btn hdng_btn cmn-btn1">User Management</button>
                            </Link>
                        </h1>
                    </div>
                </div>
                {/* row ends */}

                <div className="row status_row--">


                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div className="status_box1">
                            <div className="row">
                                <h4 className="hdng">Jobs Available:</h4>
                                <div className="col-lg-6 col-md-12 col-sm-12 colJ-1">
                                    <div className='grph_dv' >
                                        <Line data={jobsData2} legend={""} height={250} />
                                    </div>
                                </div>
                                <div className="col-lg-6 col-md-12 col-sm-12 colJ-1">
                                    <ul className="status_det_list">
                                        <li className="drk-color2">New = 2735</li>
                                        <li className="drk-color4">Overall = 3465</li>
                                    </ul>

                                    <div className="viewBy_dc">
                                        <h5>View By:</h5>
                                        <ul>
                                            <li>Week</li>
                                            <li>Month</li>
                                            <li>Year</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div className="status_box1">
                            <div className="row">
                                <h4 className="hdng">Applicant Status:</h4>
                                <div className="col-lg-6 col-md-12 col-sm-12 colJ-1">
                                    <div className='grph_dv'>
                                        <Doughnut data={Applicantdata} height={250} className="myDoughnut" legend={""} />
                                    </div>
                                </div>
                                <div className="col-lg-6 col-md-12 col-sm-12 colJ-1">
                                    <ul className="status_det_list">
                                        <li className="drk-color1">New = 167</li>
                                        <li className="drk-color2">Qualified = 250</li>
                                        <li className="drk-color3">Job Ready = 333</li>
                                        <li className="drk-color4">Training Required = 250</li>
                                    </ul>

                                    <div className="viewBy_dc">
                                        <h5>View By:</h5>
                                        <ul>
                                            <li>Week</li>
                                            <li>Month</li>
                                            <li>Year</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div className="status_box1">
                            <div className="row">
                                <h4 className="hdng">Recruitment Status:</h4>
                                <div className="col-lg-6 col-md-12 col-sm-12 colJ-1">
                                    <div className='grph_dv'>
                                        <Bar data={Recruitmentdata} width={'100%'} height={180} legend={""}
                                            options={{
                                                maintainAspectRatio: false
                                            }}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-6 col-md-12 col-sm-12 colJ-1">
                                    <ul className="status_det_list">
                                        <li className="drk-color2">Interviewed = 250</li>
                                        <li className="drk-color3">Trained = 333</li>
                                        <li className="drk-color4">Newly recruited = 250</li>
                                    </ul>

                                    <div className="viewBy_dc">
                                        <h5>View By:</h5>
                                        <ul>
                                            <li>Week</li>
                                            <li>Month</li>
                                            <li>Year</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                {/* row ends */}

                <div className="row sort_row1--">
                    <div className="col-lg-6 col-md-6 col-sm-6 no_pd_l srchCol_r">
                        <div className="search_bar left">
                            <input type="text" className="srch-inp" placeholder="Search.." />
                            <i className="icon icon-search2-ie"></i>
                        </div>
                    </div>

                    <div className="col-lg-6 col-md-6 col-sm-6 no_pd_r filCol_l">
                        <div className="filter_flx">
                            <div className="filter_fields__ cmn_select_dv">
                                <Select name="view_by_status"
                                    required={true} simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Search by: All"
                                    options={options}
                                    onChange={(e) => this.setState({ searchVal: e })}
                                    value={this.state.searchVal}
                                />

                            </div>

                            <div className="filter_fields__ cmn_select_dv pd_r_0_De">
                                <Select name="view_by_status "
                                    required={true} simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Filter by: Unread"
                                    options={options}
                                    onChange={(e) => this.setState({ filterVal: e })}
                                    value={this.state.filterVal}

                                />
                            </div>

                        </div>




                    </div>

                </div>
                {/* row ends */}


                <div className="row data_table_row1" >
                    <div className="col-lg-12 no-pad">
                        <div className="data_table_cmn dashboard_Table ">
                            <ReactTable
                            PaginationComponent={Pagination}
                                data={dashboardData}
                                defaultPageSize={dashboardData.length}
                                className="-striped -highlight"
                                previousText={<span className="icon icon-arrow-left privious"></span>}
nextText={<span className="icon icon-arrow-right next"></span>}
                                columns={[
                                    { Header: "Action Type", accessor: 'Action' },
                                    { Header: "Due Date", accessor: 'DueDate' },
                                    { Header: "Assigned to", accessor: 'Assigned' },
                                    { Header: "Last Actioned", accessor: 'LastAction' },
                                    { Header: "Status", accessor: 'Status' }
                                ]}
                            />
                        </div>


                    </div>
                </div>
            </React.Fragment>

        );
    }
}

const mapStateToProps = state => ({
    showPageTitle: state.RecruitmentReducer.activePage.pageTitle,
    showTypePage: state.RecruitmentReducer.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
       
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(Dashboard);