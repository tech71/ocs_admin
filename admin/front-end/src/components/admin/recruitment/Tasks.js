import React, { Component } from 'react';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import moment from 'moment';
import { PanelGroup, Panel } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import 'react-table/react-table.css';
import EditCabInfo from './EditCabInfo';

class Tasks extends Component {

    constructor() {
        super();
        this.state = {
            InterviewType: '',
            filterVal: '',
            showModal: false,
            EditCabModal:false
        }
    }

    showModal = () => {
        this.setState({ showModal: true })
    }

    closeModal = () => {
        this.setState({ showModal: false })
    }

    render() {

        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two', clearableValue: false }
        ];

        const task = [
            {
                Name: 'CAB Day',
                Date: '02/03/03',
                Assigned: 'David Smith',
                Last: '01/02/03',
                Status: 'Pending'
            },
            {
                Name: 'CAB Day',
                Date: '02/03/03',
                Assigned: 'David Smith',
                Last: '01/02/03',
                Status: 'Pending'
            },
            {
                Name: 'CAB Day',
                Date: '02/03/03',
                Assigned: 'David Smith',
                Last: '01/02/03',
                Status: 'Pending'
            },

        ];




        return (
            <React.Fragment>


                <div className="row sort_row1--">
                    <div className="col-lg-8 col-md-8 col-sm-8 no_pd_l">
                        <div className="search_bar left">
                            <input type="text" className="srch-inp" placeholder="Search.." />
                            <i className="icon icon-search2-ie"></i>
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 no_pd_r">
                        <div className="filter_flx">

                            <div className="filter_fields__ cmn_select_dv">
                                <Select name="view_by_status"
                                    required={true} simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Filter by: Unread"
                                    options={options}
                                    onChange={(e) => this.setState({ filterVal: e })}
                                    value={this.state.filterVal}

                                />
                            </div>

                        </div>
                    </div>

                </div>
                {/* row ends */}


                <div className="row">


                    <div className="col-sm-12">

                        <div className="data_table_cmn dataTab_accrdn_cmn aplcnt_table hdng_cmn2 taskTable">

                            <ReactTable
                                columns={[
                                    { Header: "Task Name:", accessor: "Name" },
                                    { Header: "Due Date:", accessor: "Date" },
                                    { Header: "Assigned to:", accessor: "Assigned" },
                                    { Header: "Last Actioned:", accessor: "Last" },
                                    { Header: "Status:", accessor: "Status" },
                                    {
                                        expander: true,
                                        Header: () => <strong></strong>,
                                        width: 55,
                                        headerStyle: { border: "0px solid #fff" },
                                        Expander: ({ isExpanded, ...rest }) =>
                                            <div className="rec-table-icon">
                                                {isExpanded
                                                    ? <i className="icon icon-arrow-down icn_ar1"></i>
                                                    : <i className="icon icon-arrow-right icn_ar1"></i>}
                                            </div>,
                                        style: {
                                            cursor: "pointer",
                                            fontSize: 25,
                                            padding: "0",
                                            textAlign: "center",
                                            userSelect: "none"
                                        },

                                    }


                                ]}

                                defaultPageSize={3}
                                data={task}
                                pageSize={task.length}
                                showPagination={false}
                                className="-striped -highlight"
                                SubComponent={() =>

                                    <div className='applicant_info1 '>

                                        <div>

                                            <div className="Invited_Attendees__">
                                                <label>Invited Attendees</label>

                                                <div className='attendees_box'>

                                                    <ul>
                                                        <li className="wd_30">Jimmy McDonald</li>
                                                        <li className="wd_20 text-center "> <span className='Attend_status1 clr_approved'>Approved</span></li>
                                                        <li className="wd_50 action_Invili">
                                                            <b>Actions:</b> Set iPad to Applicant ID
                                                            <button className='btn cmn-btn1 atndee_tskBtn right'>Send Now</button>
                                                        </li>
                                                    </ul>

                                                </div>


                                                <div className='attendees_box'>

                                                    <ul>
                                                        <li className="wd_30">Jimmy Smith</li>
                                                        <li className="wd_20 text-center "> <span className='Attend_status1 clr_pending'>Pending</span></li>
                                                        <li className="wd_50 action_Invili">
                                                            <b>Pending Applicant Pesponse</b>
                                                        </li>
                                                    </ul>

                                                </div>

                                                <div className='attendees_box'>

                                                    <ul>
                                                        <li className="wd_30">Jane Smith</li>
                                                        <li className="wd_20 text-center "> <span className='Attend_status1 clr_declined'>Declined</span></li>
                                                        <li className="wd_50 text-center">
                                                            <b>Note:</b> Not a good day or time, needs a new CAB time
                                                            <button className='btn cmn-btn1 atndee_tskBtn realct_btn'>Re-Allocate to a new CAB event</button>
                                                        </li>
                                                    </ul>

                                                </div>


                                                <div className='attendees_box'>

                                                    <ul>
                                                        <li className="wd_30">Mary Smith</li>
                                                        <li className="wd_20 text-center "> <span className='Attend_status1 clr_approved'>Approved</span></li>
                                                        <li className="wd_50 action_Invili">
                                                            <b>Actions:</b> Set iPad to Applicant ID
                                                            <button className='btn cmn-btn1 atndee_tskBtn right'>Send Now</button>
                                                        </li>
                                                    </ul>

                                                </div>

                                                <div className='attendees_box'>

                                                    <ul>
                                                        <li className="wd_30">Harriet McDonald</li>
                                                        <li className="wd_20 text-center "> <span className='Attend_status1 clr_approved'>Approved</span></li>
                                                        <li className="wd_50 action_Invili">
                                                            <b>Actions:</b> Set iPad to Applicant ID
                                                            <button className='btn cmn-btn1 atndee_tskBtn right'>Send Now</button>
                                                        </li>
                                                    </ul>

                                                </div>

                                            </div>
                                            {/* Invited_Attendees__ ends */}

                                            <div className='Day_Sub_Tasks'>

                                                <label>CAB Day Sub-Tasks:</label>

                                                <div className='Sub_Tasks_Hdng__'>
                                                    <ul className='subTsks_hdngUl'>
                                                        <li className='wd_25'>Sub-Task Name</li>
                                                        <li className='wd_15'>Due Date:</li>
                                                        <li className='wd_20'>Assigned to:</li>
                                                        <li className='wd_40'>Notes:</li>
                                                    </ul>
                                                </div>


                                                <div className="subtasks_listsBox">

                                                    <Panel>
                                                        <Panel.Heading>
                                                            <Panel.Title >
                                                                <ul className='SubTasks_panLi cmnUl_Des'>
                                                                    <li className='wd_25'>Sub-Task 1</li>
                                                                    <li className='wd_15'>01/02/03</li>
                                                                    <li className='wd_20'>John Smith</li>
                                                                    <li className='wd_40'>noteonenoteonenoteon...</li>
                                                                </ul>
                                                                <span className='icon icon-view1-ie sbtskEye'></span>
                                                            </Panel.Title>
                                                        </Panel.Heading>
                                                        <Panel.Body>
                                                            <ul className='subTasks_Action__'>

                                                                <li><span className='sbTsk_li'>Archive</span></li>
                                                                <li><span className='sbTsk_li'>Delete</span></li>
                                                                <li>
                                                                    <span className='sbTsk_li'>Complete Task:
                                                                        <label className="csTum_checkbox">
                                                                            <input type="checkbox" />
                                                                            <span className="checkmark"></span>
                                                                        </label>

                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </Panel.Body>
                                                    </Panel>


                                                    <Panel>
                                                        <Panel.Heading>
                                                            <Panel.Title >
                                                                <ul className='SubTasks_panLi cmnUl_Des'>
                                                                    <li className='wd_25'>Sub-Task 1</li>
                                                                    <li className='wd_15'>01/02/03</li>
                                                                    <li className='wd_20'>John Smith</li>
                                                                    <li className='wd_40'>noteonenoteonenoteon...</li>
                                                                </ul>
                                                                <span className='icon icon-view1-ie sbtskEye'></span>
                                                            </Panel.Title>
                                                        </Panel.Heading>
                                                        <Panel.Body>
                                                            <ul className='subTasks_Action__'>

                                                                <li><span className='sbTsk_li'>Archive</span></li>
                                                                <li><span className='sbTsk_li'>Delete</span></li>
                                                                <li>
                                                                    <span className='sbTsk_li'>Complete Task:
                                                                        <label className="csTum_checkbox">
                                                                            <input type="checkbox" />
                                                                            <span className="checkmark"></span>
                                                                        </label>

                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </Panel.Body>
                                                    </Panel>


                                                    <Panel>
                                                        <Panel.Heading>
                                                            <Panel.Title >
                                                                <ul className='SubTasks_panLi cmnUl_Des'>
                                                                    <li className='wd_25'>Sub-Task 1</li>
                                                                    <li className='wd_15'>01/02/03</li>
                                                                    <li className='wd_20'>John Smith</li>
                                                                    <li className='wd_40'>noteonenoteonenoteon...</li>
                                                                </ul>
                                                                <span className='icon icon-view1-ie sbtskEye'></span>
                                                            </Panel.Title>
                                                        </Panel.Heading>
                                                        <Panel.Body>
                                                            <ul className='subTasks_Action__'>

                                                                <li><span className='sbTsk_li'>Archive</span></li>
                                                                <li><span className='sbTsk_li'>Delete</span></li>
                                                                <li>
                                                                    <span className='sbTsk_li'>Complete Task:
                                                                        <label className="csTum_checkbox">
                                                                            <input type="checkbox" />
                                                                            <span className="checkmark"></span>
                                                                        </label>

                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </Panel.Body>
                                                    </Panel>

                                                </div>
                                                {/* subtasks_listsBox ends */}




                                            </div>
                                            {/* Day_Sub_Tasks ends */}

                                            {/* <div className='attendees_navEx'>
                                                <ul className='subTasks_Action__'>
                                                    <li><span className='sbTsk_li'>Analytics</span></li>
                                                    <li><span className='sbTsk_li'>Close Job</span></li>
                                                    <li><span className='sbTsk_li'>Edit CAB Info</span></li>
                                                    <li><span className='sbTsk_li'>Change CAB Date & Send Email</span></li>
                                                </ul>
                                            </div> */}

                                           
                                                <div class="col-sm-12 no-pad text-right mr_b_15">
                                                    <button type="submit" class="btn cmn-btn1 create_quesBtn" onClick={()=>{this.setState({EditCabModal:true})}} >Edit CAB Day</button>
                                                </div>
                                            

                                        </div>

                                    </div>



                                }
                            />

                        </div>

                    </div>






                    <div className="col-sm-12 TaskAccordion_col">


                        <button className="btn cmn-btn1 new_task_btn" onClick={this.showModal}>Create New Task</button>

                    </div>

                </div>
                {/* row ends */}





                <div className={this.state.showModal ? 'customModal show' : 'customModal'}>
                    <div className="cstomDialog widBig">

                        <h3 className="cstmModal_hdng1--">
                            New Group Interview for Applicant
                        <span className="closeModal icon icon-close1-ie" onClick={this.closeModal}></span>
                        </h3>

                        <form>

                            <div className='tskModRow1'>
                                <div className="row ">

                                    <div className="col-md-6 ">

                                        <div className="csform-group">
                                            <label className='bg_labs'>Applicant Name:</label>
                                            <input type="text" className="csForm_control" />
                                        </div>

                                        <div className="csform-group">
                                            <label className='bg_labs'>Interview Type:</label>
                                            <div className="cmn_select_dv">
                                                <Select name="view_by_status"
                                                    required={true} simpleValue={true}
                                                    searchable={false} Clearable={false}
                                                    placeholder="Select Type"
                                                    options={options}
                                                    onChange={(e) => this.setState({ InterviewType: e })}
                                                    value={this.state.InterviewType}
                                                />
                                            </div>
                                        </div>

                                        <div className="csform-group">
                                            <label className='bg_labs'>Search Date Range:</label>
                                            <div className="cmn_select_dv">
                                                <Select name="view_by_status"
                                                    required={true} simpleValue={true}
                                                    searchable={false} Clearable={false}
                                                    placeholder="Select Type"
                                                    options={options}
                                                    onChange={(e) => this.setState({ searchVal: e })}
                                                    value={this.state.searchVal}
                                                />
                                            </div>
                                        </div>

                                    </div>

                                    <div className='col-md-6 pad_l_50Ts'>

                                        <div className='tskRgHeads'>
                                            <h2><b>Next Available Interviews</b></h2>
                                            <h3>(from: 01/01/01 - to: 02/02/01)</h3>
                                        </div>
                                        <div className='avlblty_list__'>
                                            <div className='avlblty_box'>
                                                <ul>
                                                    <li>Date: <b>01/01/01</b></li>
                                                    <li>Attendees: <b>8 of 10 (2 Spots left to fill)</b></li>
                                                    <li>Recruiter in Charge: <b>Mike Smith</b></li>
                                                </ul>
                                            </div>
                                            <div className='avlblty_box'>
                                                <ul>
                                                    <li>Date: <b>01/01/01</b></li>
                                                    <li>Attendees: <b>8 of 10 (2 Spots left to fill)</b></li>
                                                    <li>Recruiter in Charge: <b>Mike Smith</b></li>
                                                </ul>
                                            </div>

                                            <div className='avlblty_box'>
                                                <ul>
                                                    <li>Date: <b>01/01/01</b></li>
                                                    <li>Attendees: <b>8 of 10 (2 Spots left to fill)</b></li>
                                                    <li>Recruiter in Charge: <b>Mike Smith</b></li>
                                                </ul>
                                            </div>


                                        </div>
                                        {/* avlblty_list__ ends */}

                                    </div>




                                </div>
                                {/* row ends */}
                            </div>

                            <div className='row bor_top inviRow'>
                                <div className='col-sm-12 text-center'>
                                    <input type='button' className='btn cmn-btn1 snd_iniBtn' value='Add Applicant and Send Invitation Email' />
                                </div>
                            </div>


                        </form>


                    </div>
                </div>
                

                <EditCabInfo showModal={this.state.EditCabModal} closeModal={()=>{this.setState({EditCabModal:false})}} />

            </React.Fragment>
        );
    }
}

export default Tasks;