import React, { Component } from "react";
import Select from "react-select-plus";
import "react-select-plus/dist/react-select-plus.css";
import moment from "moment";
import { PanelGroup, Panel } from "react-bootstrap";
import { Link } from "react-router-dom";
import ReactTable from "react-table";
import "react-table/react-table.css";
import SuccessPopUp from "./SuccessPopup";
import { connect } from "react-redux";
import RecruitmentPage from "components/admin/recruitment/RecruitmentPage";

class JobOpening extends Component {
  constructor() {
    super();
    this.state = {
      searchVal: "",
      filterVal: "",
      showModal: false,
      resumeModalShow: false,
      successPop: false
    };
  }

  showModal = () => {
    this.setState({ showModal: true });
  };

  closeModal = () => {
    this.setState({ showModal: false, resumeModalShow: false });
  };

  resumeModalShow = () => {
    this.setState({ resumeModalShow: true });
  };

  resumeModalHide = () => {
    this.setState({ resumeModalShow: false });
  };

  render() {
    var options = [
      { value: "one", label: "One" },
      { value: "two", label: "Two", clearableValue: false }
    ];

    const JobOpening = [
      {
        Position: "Qualified Carer ",
        Date: "Recurring",
        Applicants: "43",
        Category: "Casual",
        Assigned: "Johnny Mattews"
      },
      {
        Position: "Qualified Carer ",
        Date: "Recurring",
        Applicants: "43",
        Category: "Casual",
        Assigned: "Johnny Mattews"
      },
      {
        Position: "Qualified Carer ",
        Date: "Recurring",
        Applicants: "43",
        Category: "Casual",
        Assigned: "Johnny Mattews"
      }
    ];

    return (
      <React.Fragment>
        <RecruitmentPage pageTypeParms={this.props.props.match.params.page} />
        <section>
          <div className="row">
            <div className="col-lg-12 col-md-12 no-pad back_col_cmn-">
              <span
                onClick={e => window.history.back()}
                className="icon icon-back1-ie"
              />
            </div>
          </div>
          {/* row ends */}

          <div className="row">
            <div className="col-lg-12 col-md-12 main_heading_cmn-">
              <h1>Job Openings</h1>
            </div>
          </div>
          {/* row ends */}

          <div className="row action_cont_row">
            <div className="col-lg-12 col-sm-12">
              <div className="tab-content">
                <div
                  role="tabpanel"
                  className={
                    this.props.showTypePage == "jobs"
                      ? "tab-pane active"
                      : "tab-pane"
                  }
                  id="job_opening"
                >
                  <div className="tasks_comp">
                    <div className="row sort_row1--">
                      <div className="col-lg-8 col-md-8 col-sm-8 no_pd_l">
                        <div className="search_bar left">
                          <input
                            type="text"
                            className="srch-inp"
                            placeholder="Search.."
                          />
                          <i className="icon icon-search2-ie" />
                        </div>
                      </div>

                      <div className="col-lg-4 col-md-4 col-sm-4 no_pd_r">
                        <div className="filter_flx">
                          <div className="filter_fields__ cmn_select_dv">
                            <Select
                              name="view_by_status"
                              required={true}
                              simpleValue={true}
                              searchable={false}
                              Clearable={false}
                              placeholder="Filter by: Unread"
                              options={options}
                              onChange={e => this.setState({ filterVal: e })}
                              value={this.state.filterVal}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* row ends */}

                    <div className="row">
                      <div className="col-sm-12">
                        <div className="data_table_cmn dataTab_accrdn_cmn aplcnt_table hdng_cmn2 taskTable">
                          <ReactTable
                            columns={[
                              { Header: "Position:", accessor: "Position" },
                              { Header: "End Date:", accessor: "Date" },
                              { Header: "Applicants:", accessor: "Applicants" },
                              { Header: "Job Category:", accessor: "Category" },
                              { Header: "Assigned to:", accessor: "Assigned" },
                              {
                                expander: true,
                                Header: () => <strong />,
                                width: 55,
                                headerStyle: { border: "0px solid #fff" },
                                Expander: ({ isExpanded, ...rest }) => (
                                  <div className="rec-table-icon">
                                    {isExpanded ? (
                                      <i className="icon icon-arrow-down icn_ar1" />
                                    ) : (
                                      <i className="icon icon-arrow-right icn_ar1" />
                                    )}
                                  </div>
                                ),
                                style: {
                                  cursor: "pointer",
                                  fontSize: 25,
                                  padding: "0",
                                  textAlign: "center",
                                  userSelect: "none"
                                }
                              }
                            ]}
                            defaultPageSize={3}
                            data={JobOpening}
                            pageSize={JobOpening.length}
                            showPagination={false}
                            className="-striped -highlight"
                            SubComponent={() => (
                              <div className="applicant_info1 openingInfo">
                                <div className="jobMain_dets__ ">
                                  <div className="row">
                                    <div className="col-sm-6 ">
                                      <ul className="jobMaindets_ul">
                                        <li>
                                          Open Position Name:{" "}
                                          <b>Qualified Carer</b>
                                        </li>
                                        <li>
                                          Current Applicants:<b>58</b>
                                        </li>
                                      </ul>
                                      <button
                                        onClick={this.showModal}
                                        className="btn cmn-btn1 vw_aplcnt_btn eye-btn"
                                      >
                                        View Applicants
                                      </button>
                                    </div>
                                    <div className="col-sm-6 ">
                                      <p>Posted On:</p>
                                      <ul className="posted_ul12__">
                                        <li>Webform</li>
                                        <li>Seek</li>
                                        <li>Internal</li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                                {/* jobMain_dets__ ends */}

                                <div className="jobMain_foot__">
                                  <div className="row">
                                    <div className="col-md-6 col-sm-6">
                                      <p>
                                        Assigned Recruiter:<b>Johnny Smith</b>{" "}
                                      </p>
                                    </div>
                                    <div className="col-md-6 col-sm-6">
                                      <ul className="subTasks_Action__">
                                        <li>
                                          <span className="sbTsk_li">
                                            Analytics
                                          </span>
                                        </li>
                                        <li>
                                          <span className="sbTsk_li">
                                            Close Job
                                          </span>
                                        </li>
                                        <li>
                                          <span className="sbTsk_li">
                                            Edit Job
                                          </span>
                                        </li>
                                        <li>
                                          <span className="sbTsk_li">
                                            Delete Job
                                          </span>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                                {/* jobMain_foot__ ends */}
                              </div>
                            )}
                          />
                        </div>
                      </div>

                      <div className="col-sm-12 text-right">
                        <Link to="./create_job">
                          <button className="btn cmn-btn1 new_task_btn">
                            Create New Job
                          </button>
                        </Link>
                      </div>
                    </div>
                    {/* row ends */}
                  </div>
                  {/* tasks_comp ends */}
                </div>
                <div
                  role="tabpanel"
                  className={
                    this.props.showTypePage == "interviews"
                      ? "tab-pane active"
                      : "tab-pane"
                  }
                  id="job_interview"
                >
                  Coming Soon
                </div>
              </div>
            </div>
          </div>
          {/* row ends */}
        </section>

        <div
          className={"customModal " + (this.state.showModal ? " show" : " ")}
          id="JobOpening_Modal"
        >
          <div className="cstomDialog widBig">
            <span
              className="icon icon-close2-ie mdl_CloseTl cstmDLCl"
              onClick={this.closeModal}
            />
            <div className="JO_hdng2">
              <div className="row cmn_font_clr">
                <div className="col-xs-6">
                  <h4>
                    <b>Postion: </b>Qulaified Carer
                  </h4>
                  <h4>
                    <b>Assigned to: </b>Johnny Matthews
                  </h4>
                </div>
                <div className="col-xs-6 text-right">
                  <h4>
                    <b>End Date: </b>Reccuring
                  </h4>
                  <h4>
                    <b>Total Applicant: </b>43
                  </h4>
                </div>
              </div>
            </div>
            {/* JO_hdng2 ends */}

            <div className="jOpen_table_dv">
              <ul className="JoTab_hdng">
                <li className="wd_20">Applicant:</li>
                <li className="wd_15">Application Date:</li>
                <li className="wd_20">Cover Letter:</li>
                <li className="wd_25">Resume:</li>
                <li className="wd_20">Related Docs:</li>
              </ul>

              <div className="JoTab_Body">
                <div className="JoAplcnt_list__">
                  <div className="Jlst_detTab">
                    <ul className="task_hdng_dtl">
                      <li className="wd_20">Melissa Rodriguez</li>
                      <li className="wd_15">01/01/18</li>
                      <li className="wd_20">
                        mel_cover_letter_V1.pdf{" "}
                        <i
                          className="icon- icon-view1-ie see_docs"
                          onClick={this.resumeModalShow}
                        />
                      </li>
                      <li className="wd_25">
                        mel_rodriguez_resume.pdf{" "}
                        <i
                          className="icon- icon-view1-ie see_docs"
                          onClick={this.resumeModalShow}
                        />
                      </li>
                      <li className="wd_20">
                        Qulaification.pdf{" "}
                        <i
                          className="icon- icon-view1-ie see_docs"
                          onClick={this.resumeModalShow}
                        />
                      </li>
                    </ul>
                  </div>
                  <div className="jbAplc_tags__">
                    <span className="active">Interview</span>
                    <span>Shortlist</span>
                    <span>Not-Suitable</span>
                  </div>
                </div>

                <div className="JoAplcnt_list__">
                  <div className="Jlst_detTab">
                    <ul className="task_hdng_dtl">
                      <li className="wd_20">Melissa Rodriguez</li>
                      <li className="wd_15">01/01/18</li>
                      <li className="wd_20">
                        mel_cover_letter_V1.pdf{" "}
                        <i className="icon- icon-view1-ie see_docs" />
                      </li>
                      <li className="wd_25">
                        mel_rodriguez_resume.pdf{" "}
                        <i className="icon- icon-view1-ie see_docs" />
                      </li>
                      <li className="wd_20">
                        Qulaification.pdf{" "}
                        <i className="icon- icon-view1-ie see_docs" />
                      </li>
                    </ul>
                  </div>
                  <div className="jbAplc_tags__">
                    <span>Interview</span>
                    <span>Shortlist</span>
                    <span>Not-Suitable</span>
                  </div>
                </div>

                <div className="JoAplcnt_list__">
                  <div className="Jlst_detTab">
                    <ul className="task_hdng_dtl">
                      <li className="wd_20">Melissa Rodriguez</li>
                      <li className="wd_15">01/01/18</li>
                      <li className="wd_20">
                        mel_cover_letter_V1.pdf{" "}
                        <i className="icon- icon-view1-ie see_docs" />
                      </li>
                      <li className="wd_25">
                        mel_rodriguez_resume.pdf{" "}
                        <i className="icon- icon-view1-ie see_docs" />
                      </li>
                      <li className="wd_20">
                        Qulaification.pdf{" "}
                        <i className="icon- icon-view1-ie see_docs" />
                      </li>
                    </ul>
                  </div>
                  <div className="jbAplc_tags__">
                    <span>Interview</span>
                    <span>Shortlist</span>
                    <span>Not-Suitable</span>
                  </div>
                </div>

                <div className="JoAplcnt_list__">
                  <div className="Jlst_detTab">
                    <ul className="task_hdng_dtl">
                      <li className="wd_20">Melissa Rodriguez</li>
                      <li className="wd_15">01/01/18</li>
                      <li className="wd_20">
                        mel_cover_letter_V1.pdf{" "}
                        <i className="icon- icon-view1-ie see_docs" />
                      </li>
                      <li className="wd_25">
                        mel_rodriguez_resume.pdf{" "}
                        <i className="icon- icon-view1-ie see_docs" />
                      </li>
                      <li className="wd_20">
                        Qulaification.pdf{" "}
                        <i className="icon- icon-view1-ie see_docs" />
                      </li>
                    </ul>
                  </div>
                  <div className="jbAplc_tags__">
                    <span>Interview</span>
                    <span>Shortlist</span>
                    <span>Not-Suitable</span>
                  </div>
                </div>

                <div className="JoAplcnt_list__">
                  <div className="Jlst_detTab">
                    <ul className="task_hdng_dtl">
                      <li className="wd_20">Melissa Rodriguez</li>
                      <li className="wd_15">01/01/18</li>
                      <li className="wd_20">
                        mel_cover_letter_V1.pdf{" "}
                        <i className="icon- icon-view1-ie see_docs" />
                      </li>
                      <li className="wd_25">
                        mel_rodriguez_resume.pdf{" "}
                        <i className="icon- icon-view1-ie see_docs" />
                      </li>
                      <li className="wd_20">
                        Qulaification.pdf{" "}
                        <i className="icon- icon-view1-ie see_docs" />
                      </li>
                    </ul>
                  </div>
                  <div className="jbAplc_tags__">
                    <span>Interview</span>
                    <span>Shortlist</span>
                    <span>Not-Suitable</span>
                  </div>
                </div>

                <div className="JoAplcnt_list__">
                  <div className="Jlst_detTab">
                    <ul className="task_hdng_dtl">
                      <li className="wd_20">Melissa Rodriguez</li>
                      <li className="wd_15">01/01/18</li>
                      <li className="wd_20">
                        mel_cover_letter_V1.pdf{" "}
                        <i className="icon- icon-view1-ie see_docs" />
                      </li>
                      <li className="wd_25">
                        mel_rodriguez_resume.pdf{" "}
                        <i className="icon- icon-view1-ie see_docs" />
                      </li>
                      <li className="wd_20">
                        Qulaification.pdf{" "}
                        <i className="icon- icon-view1-ie see_docs" />
                      </li>
                    </ul>
                  </div>
                  <div className="jbAplc_tags__">
                    <span>Interview</span>
                    <span>Shortlist</span>
                    <span>Not-Suitable</span>
                  </div>
                </div>
              </div>
              {/* JoTab_Body ends */}
            </div>

            <div className="jobOpModal_foot__">
              <ul className="subTasks_Action__ left">
                <li>
                  <span className="sbTsk_li">View Interviewees</span>
                </li>
                <li>
                  <span className="sbTsk_li">View Shortlist</span>
                </li>
                <li>
                  <span className="sbTsk_li">View Non-Suitable</span>
                </li>
              </ul>
            </div>
          </div>
          {/* moda_dialog */}

          <div
            className={
              this.state.resumeModalShow
                ? "resume_dialog show"
                : "resume_dialog"
            }
          >
            <div className="tmpDia_hdng">
              <h3>
                <b>Currently Viewing - Applicant:</b> Melissa Rodriguez
                <br />
                <div>Resume</div>
              </h3>
              <span
                className="icon icon-close2-ie mdl_CloseTl"
                onClick={this.resumeModalHide}
              />
            </div>

            <div className="temp_showcase1__">
              <div className="thumbList_aside">
                <p>Pages:</p>

                <div className="tmp_thum_aside scrol_des">
                  <div className="tmp_thumb active">
                    <img
                      src="http://placehold.it/150x200/05adee/fff?text=Page 01"
                      alt="2"
                    />
                  </div>

                  <div className="tmp_thumb">
                    <img
                      src="http://placehold.it/150x200/05adee/fff?text=Page 02"
                      alt="2"
                    />
                  </div>

                  <div className="tmp_thumb">
                    <img
                      src="http://placehold.it/150x200/05adee/fff?text=Page 03"
                      alt="2"
                    />
                  </div>

                  <div className="tmp_thumb">
                    <img
                      src="http://placehold.it/150x200/05adee/fff?text=Page 04"
                      alt="2"
                    />
                  </div>

                  <div className="tmp_thumb">
                    <img
                      src="http://placehold.it/150x200/05adee/fff?text=Page 05"
                      alt="2"
                    />
                  </div>

                  <div className="tmp_thumb">
                    <img
                      src="http://placehold.it/150x200/05adee/fff?text=Page 06"
                      alt="2"
                    />
                  </div>

                  <div className="tmp_thumb">
                    <img
                      src="http://placehold.it/150x200/05adee/fff?text=Page 07"
                      alt="2"
                    />
                  </div>
                </div>
              </div>

              <div className="tmplate_mnShow scrol_des">
                <img
                  src="http://placehold.it/700x900/05adee/fff?text=Page 01"
                  alt="2"
                />
              </div>

              <div className="clearfix" />
            </div>
            {/* temp_showcase1__ ends */}

            <span className="dwnld_res_bt">
              <i className="icon icon-download1-ie" />
              <p>Download Resume</p>
            </span>
          </div>
        </div>
        {/* JobOpening_Modal ends */}

        <SuccessPopUp
          show={this.state.successPop}
          close={() => {
            this.setState({ successPop: false });
          }}
        >
          Your new Job has been Created and Posted
        </SuccessPopUp>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  showPageTitle: state.RecruitmentReducer.activePage.pageTitle,
  showTypePage: state.RecruitmentReducer.activePage.pageType
});

const mapDispatchtoProps = dispach => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchtoProps
)(JobOpening);
