const initialState = {
    departmentData: {
    },  
    activePage:{pageTitle:'',pageType:''} 
}

const RecruitmentReducer = (state = initialState, action) => {

    switch (action.type) {
        case 'set_recruitment_department_data':
            return { ...state, departmentData: action.departmentData };
        
        case 'set_active_page_recruitment':
            return {...state, activePage: action.value};
        
        default:
            return state
    }
}

export default RecruitmentReducer
    