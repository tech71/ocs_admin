import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import Select from 'react-select-plus';
import { ROUTER_PATH, PAGINATION_SHOW } from '../../../config.js';
import { checkItsNotLoggedIn, postData, handleChangeChkboxInput, getPermission, reFreashReactTable } from '../../../service/common.js';
import 'react-select-plus/dist/react-select-plus.css';
import ReactResponsiveSelect from 'react-responsive-select';
import Navigation from './Navigation';
import DepartmentList from './DepartmentList';
import AddStaffMember from './AddStaffMember';
import { connect } from 'react-redux';
import Pagination from "../../../service/Pagination.js";
import RecruiterDisable from './RecruiterDisable';

const caretIcon = (
    <i className="icon icon-edit1-ie"></i>
);

//
const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve) => {
        // request json
        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });
        postData('recruitment/RecruitmentDashboard/get_staff_members', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count),
                //all_count:result.all_count,
            };
            resolve(res);
        });

    });
};


class UserManagement extends Component {
    constructor() {
        checkItsNotLoggedIn(ROUTER_PATH);
        //super(props);
        super();
        this.state = {
            filterVal: '',
            cstmSelectHandler: false,
            showModal: false,
            ActiveClass: 'staffMember',
            staffList: [],
            department_option: [],
            department_option_design: [],
            desing1: [],
            filtered: '',
            alloted_dept: [],
            showDisModal: false
        }
        this.reactTable = React.createRef();
    }
    cstmSelect = () => {
        // e.preventDefault();
        this.setState({
            cstmSelectHandler: !this.state.cstmSelectHandler
        });
    }

    fetchData = (state) => {
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                staffList: res.rows,
                all_count: res.all_count,
                pages: res.pages,
                loading: false,
            });
        })
    }

    closeModal = (param) => {
        this.setState({ showModal: false });

        if (param)
            reFreashReactTable(this, 'fetchData');
    }

    componentWillReceiveProps(newProps) {
        this.setState({ department_option: newProps.DepartmentData }, () => {
            this.designDepartmentSelect()
        });
    }

    designDepartmentSelect = () => {
        let d = this.state.department_option;
        let c = [{ value: '', text: 'Select Allocations:' }];
        var x = c.concat(d);

        var optionDepartment = [];
        var tempAry = { text: 'Select Allocations:', optHeader: true }

        var designSelect = x.length > 0 ? x.map((z, index) => {
            var desingAry = {};
            if (index == 0) {
                desingAry['text'] = z.text;
                desingAry['optHeader'] = true;
            }
            else {
                desingAry['value'] = z.value;
                desingAry['text'] = z.text;
                desingAry['markup'] = this.multiSelectOptionMarkup(z.text);
            }
            return desingAry;
        }) : [];
        this.setState({ optionDepartment: designSelect });
    }

    /* designDepartmentSelect_1 = () => {
         var x = this.state.department_option;
         var optionDepartment = [];
 
         var designSelect = x.length > 0 ? x.map((z, index) => {
             var desingAry = {};
             desingAry['value'] = z.value;
             desingAry['label'] = z.text;
             return desingAry;
         }) : [];
         this.setState({ optionDepartment: x });
     }*/

    multiSelectOptionMarkup = (text) => {
        return <div>
            <span className="rrs_select"> {text}</span>
            <span className="checkbox">
                <i className="icon icon-star2-ie"></i>
            </span>
        </div>
    }

    departmentAllotedTo = (index, staffId, value) => {
        // console.log(value);
        if (value.altered) {
            var requestData = { staffId: staffId, selectedData: value };
            postData('recruitment/RecruitmentDashboard/update_alloted_department', requestData).then((result) => {
                if (result.status) {
                    var oldStaffList = this.state.staffList;
                    oldStaffList[index].alloted_dept = result.update_dept;
                    this.setState({ staffList: oldStaffList });
                } else {

                }
            });
        }
    }

    searchData = (e) => {
        e.preventDefault();
        var requestData = { srch_box: this.state.srch_box };
        this.setState({ filtered: requestData });
    }


    closeDisModal = () => {
        this.setState({ showDisModal: false })
    }

    render() {


        const tblColumns = [
            { Header: "Name", accessor: "name" },
            { Header: "Username", accessor: "username" },
            { Header: "HCMGR-ID", accessor: "ocs_id" },
            { Header: "Department", accessor: "department" },
            { Header: "Start Date", accessor: "created" },
            {
                expander: true,
                Header: () => <strong></strong>,
                width: 55,
                headerStyle: { border: "0px solid #fff" },
                Expander: ({ isExpanded, ...rest }) =>
                    <div className="rec-table-icon">
                        {isExpanded
                            ? <i className="icon icon-arrow-down icn_ar1"></i>
                            : <i className="icon icon-arrow-right icn_ar1"></i>}
                    </div>,
                style: {
                    cursor: "pointer",
                    fontSize: 25,
                    padding: "0",
                    textAlign: "center",
                    userSelect: "none"
                },
            }
        ];

        var options = [
            { value: 'one', label: 'Option 1' },
            { value: 'two', label: 'Option 2' }
        ];

        return (
            <React.Fragment>
                
                        <div className="row">
                            <div className="col-lg-12 col-md-12 no-pad back_col_cmn-">
                                <Link to={ROUTER_PATH + 'admin/recruitment/dashboard'}>
                                    <span className="icon icon-back1-ie"></span>
                                </Link>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 col-md-12 main_heading_cmn-">
                                <h1>User Management</h1>
                            </div>
                        </div>
                        {/* <Navigation /> */}
                        <div className="row action_cont_row">

                        <div className="col-lg-2 col-md-2 col-sm-2 no_pd_l asideCol1">
                                <aside>
                                    <ul className="side_menu">
                                        <li onClick={() => this.setState({ ActiveClass: 'staffMember' })} ><a className={this.state.ActiveClass && this.state.ActiveClass == 'staffMember' ? 'active' : ''} href="#staffSection" aria-controls="staffSection" role="tab" data-toggle="tab">Staff Members</a></li>
                                        <li onClick={() => this.setState({ ActiveClass: 'department' })}><a className={this.state.ActiveClass && this.state.ActiveClass == 'department' ? 'active' : ''} href="#departmentSection" aria-controls="departmentSection" role="tab" data-toggle="tab">Departments</a></li>
                                    </ul>
                                </aside>
                            </div>
                       
                            <div className="col-lg-10 col-md-10 col-sm-10 no_pd_r mainCntntCol1">
                                <div className="tab-content">
                                    <div role="tabpanel" className="tab-pane active" id="staffSection">
                                        <div className="tasks_comp ">
                                            <div className="row">
                                                <div className='col-sm-12 text-right'>
                                                    <a className="btn cmn-btn1 apli_btn__ eye-btn add_staff" onClick={() => this.setState({ showModal: true, staffData: '', pagetitile: 'Create New Staff Member' })}>Add Staff Member</a>
                                                </div>


                                                <AddStaffMember
                                                    showModal={this.state.showModal}
                                                    closeModal={this.closeModal}
                                                    staffData={this.state.staffData}
                                                    title={this.state.pagetitile}
                                                />

                                                <form method='get' onSubmit={this.searchData} autoComplete="off">
                                                    <div className='col-sm-8 mr_b_20'>
                                                        <div className="search_bar left">
                                                            <input type="text" className="srch-inp" placeholder="Job specific Search Bar.." name="srch_box" onChange={(e) => handleChangeChkboxInput(this, e)} />
                                                            <i className="icon icon-search2-ie"></i>
                                                        </div>
                                                    </div>
                                                </form>

                                                <div className='col-sm-4'>
                                                    <div className="filter_flx lab_vrt">
                                                        <label>Show:</label>
                                                        <div className="filter_fields__ cmn_select_dv">
                                                            <Select name="view_by_status"
                                                                required={true} simpleValue={true}
                                                                searchable={false} Clearable={false}
                                                                placeholder="All Applicant"
                                                                options={options}
                                                                onChange={(e) => this.setState({ filterVal: e })}
                                                                value={this.state.filterVal}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-12">
                                                    <div className="data_table_cmn dataTab_accrdn_cmn hdng_cmn2 staffTable12">
                                                        <ReactTable
                                                            PaginationComponent={Pagination}
                                                            showPagination={this.state.staffList.length > PAGINATION_SHOW ? true : false}
                                                            ref={this.reactTable}
                                                            columns={tblColumns}
                                                            manual
                                                            data={this.state.staffList}
                                                            filtered={this.state.filtered}
                                                            pages={this.state.pages}
                                                            previousText={<span className="icon icon-arrow-left privious"></span>}
                                                            nextText={<span className="icon icon-arrow-right next"></span>}
                                                            loading={this.state.loading}
                                                            onFetchData={this.fetchData}
                                                            noDataText="No Record Found"
                                                            defaultPageSize={10}
                                                            minRows={1}
                                                            className="-striped -highlight"
                                                            
                                                            collapseOnDataChange={false}
                                                            SubComponent={(props) => {
                                                                return (
                                                                    <div className='applicant_info1 training_info1 usrMng'>
                                                                        <div className='trngBoxAc usrMngBox'>
                                                                            <div className='row'>
                                                                                <div className='col-lg-5 col-md-5 col-sm-5'>
                                                                                    <div className='profDet_lSe'>
                                                                                        <div className='prof_left'>
                                                                                            <div className='profImg' style={{ backgroundImage: 'url("/assets/images/admin/dummy.png")' }}>
                                                                                                <img src="" />
                                                                                            </div>
                                                                                            <a onClick={() => this.setState({ showModal: true, staffData: props.original, id: props.original.ocs_id, pagetitile: 'Update Staff Member' })} className='btn cmn-btn1 edt_btn'>Edit</a>
                                                                                        </div>
                                                                                        <div className='prof_right'>
                                                                                            <h4 className='usName'><b>{props.original.name}</b></h4>
                                                                                            <div>{props.original.position}</div>
                                                                                            <div>HCMGR-ID: <b>{props.original.ocs_id}</b></div>

                                                                                            <div className='cntBxiE'>
                                                                                                <h5 >Contact:</h5>
                                                                                                {(props.original.PhoneInput.length > 0) ? <span>
                                                                                                    {props.original.PhoneInput.map((value, idx) => (
                                                                                                        <div key={idx + 2}>Phone ({(value.primary_phone) && value.primary_phone == 1 ? 'Primary' : 'Secondary'}):<b> {value.name}</b></div>
                                                                                                    ))} </span> : ''
                                                                                                }
                                                                                                <div className='mt-3'>
                                                                                                    {(props.original.EmailInput.length > 0) ? <span>
                                                                                                        {props.original.EmailInput.map((value, idx) => (
                                                                                                            <div key={idx + 5}>Email ({(value.primary_email) && value.primary_email == 1 ? 'Primary' : 'Secondary'}):<b> {value.name}</b></div>
                                                                                                        ))} </span> : ''
                                                                                                    }
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className='clearfix'></div>
                                                                                </div>
                                                                                <div className='col-lg-3 col-md-3 col-sm-3'>
                                                                                    <p className='hd_para'>Access Permissions:</p>
                                                                                    <button className="btn cmn-btn1 eye-btn permisnEdit ">CAB Scheduling</button>
                                                                                    <button className="btn cmn-btn1 eye-btn permisnEdit ">Phone Interviews</button>
                                                                                    <button className="btn cmn-btn1 eye-btn permisnEdit ">Group Interviews</button>
                                                                                </div>
                                                                                <div className='col-lg-3 col-md-4 col-sm-4 pd_l_30 pd_b_20'>
                                                                                    <p className='hd_para'>Allocated Departments:</p>
                                                                                    <div>
                                                                                        <label>Allocated to:</label>
                                                                                        <div className="mb-3 Cust_Sel_2 cmn_select_dv star_slct">

                                                                                            <ReactResponsiveSelect
                                                                                                multiselect
                                                                                                name="make6"
                                                                                                options={this.state.optionDepartment}
                                                                                                noSelectionLabel="Please select"
                                                                                                caretIcon={caretIcon}
                                                                                                selectedValues={(props.original.alloted_dept.length > 0) ? props.original.alloted_dept : []}
                                                                                                onChange={this.departmentAllotedTo.bind(null, props.index, props.original.ocs_id)}
                                                                                            />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div>
                                                                                        <label>Preferred:</label>
                                                                                        <div className='cstmSelect2'>
                                                                                            <div className='optSelected_cS' >
                                                                                                <span>All</span>
                                                                                            </div>
                                                                                            <div className="cstmSelect2_optionsBox " >
                                                                                                <p>Select Allocations:</p>
                                                                                                <ul className="csTm2Slct_options">
                                                                                                    <li className='active'>Select All</li>
                                                                                                    <li>Department 1</li>
                                                                                                    <li>Department 2</li>
                                                                                                    <li>Welfare</li>
                                                                                                    <li>Department 4</li>
                                                                                                    <li>Department 5</li>
                                                                                                    <li>Department 6</li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div className="row accFootRow1">
                                                                                <div className="col-md-12 col-sm-12">
                                                                                    <ul className="subTasks_Action__">
                                                                                        <li><span className="sbTsk_li">Analytics</span></li>
                                                                                        <li><span className="sbTsk_li">Pause Account</span></li>
                                                                                        <li onClick={() => { this.setState({ showDisModal: true }) }}><span className="sbTsk_li">Disable</span></li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                )
                                                            }}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div role="tabpanel" className="tab-pane" id="departmentSection">
                                        <DepartmentList />
                                    </div>
                                </div>
                            </div>
                        </div>
                   
                <RecruiterDisable showDisModal={this.state.showDisModal} closeDisModal={this.closeDisModal} />


            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    DepartmentData: state.RecruitmentReducer.departmentData
})

const mapDispatchtoProps = (dispach) => {
    return {
        //departmentData: (value) => dispach(setDepartmentData(value)),

    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(UserManagement)