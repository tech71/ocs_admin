// action for set submenu show on sidebar menu or not
export function setSubmenuShow(status) {
    return {
        type: 'set_sidebar_subMenu_Show',
        subMenuShowData: Boolean(status),
    }
}

