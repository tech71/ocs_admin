/*All dropdown of recruitment area*/

export function recruitmentStatus() {
   return [{value:'0',label:'Pending'},{value:'1',label:'Active'}];
}

export function recruitmentActionType() {
   return [{value:'0',label:'Group Interview'},{value:'1',label:'Personal Interview'}];
}

export function recruitmentLocation() {
   return [{value:'0',label:'HCM Training Facility - Training Room'},{value:'1',label:'HCM Training Facility - Interview Room'}];
}

export function recruitmentTempDropDown() {
   return [{value:'0',label:'Option 1'},{value:'1',label:'Option 2'}];
}

export function recruitmentQuestionFilter() {
   return [{ value: 'all', label: 'All Question' }, { value: 'current_question', label: 'Current Question' }, { value: 'archive_question', label: 'Current Question' }];
}



