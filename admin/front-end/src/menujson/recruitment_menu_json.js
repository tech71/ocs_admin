export const recruitmentActiveTitle = {
    schedule:'Schedule',
    task:'task',
    applicants:'Applicants',
    dashboard:'Recruitment (Admin)',
    group_interview:'Group Interview',
    cab_day:'CAB Day',
    ipad:'Ipad',
    jobs:'Jobs',
    interviews:'Interviews',
};

export const recruitmentJson = [
                { name: 'Dashboard', submenus: [], path:'/admin/recruitment/dashboard' },
                { name: 'Action', submenus: [{ name: 'Schedules', path:'/admin/recruitment/action/schedule'  }, { name: 'Tasks', path:'/admin/recruitment/action/task' }],  },
                { name: 'Jobs', submenus: [{ name: 'Jobs', path:'/admin/recruitment/job_opening/jobs'  }, { name: 'Interviews', path:'/admin/recruitment/job_opening/interviews'  }]},
                { name: 'Applicants', submenus: [], path:'/admin/recruitment/applicants' },
                { name: 'Training', submenus: [{ name: 'Group Interview', path:'/admin/recruitment/training/group_interview'}, { name: 'CAB Day', path:'/admin/recruitment/training/cab_day'  }, { name: 'Ipad', path:'/admin/recruitment/training/ipad'  }] },
            ];