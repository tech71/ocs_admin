export const crmActiveTitle = {
    crm_dashboard:'CRM Dashboard',
    prospective_participants:'Prospective Participants',
    participant_details:'Participant Details',
    participant_ability:'Participant Ability',
    participant_shift:'Shift',
    participant_funding_details:'Funding Details',
    report_onbording_analytics:'Onbording Analytics',
    report_service_analytics:'Service Analytics',
    report_location_analytics:'Location Analytics',
    schedule_user_schedule:'User Schedules',
    schedule_user_task:'User Tasks',
    user_staff_members:'Staff Members',
    user_staff_members_details:'User Management - Staff Details',
    user_departments:'Departments',
    prospective_participant_funding:'Participant Details'
};

export const crmLinkHideShowSubmenus = {"crm_user":{
  'staff_member':'staff_member_id'}
};


export const crmJson = [
        { name: 'Dashboard', submenus: [], path: '/admin/crm/participantadmin',id:'crm_dashboard' },
        { name: 'Prospective Participants', submenus: [], path: '/admin/crm/prospectiveparticipants',id:'crm_prospective_participant' },

        {
          name: 'John Smith', submenus:
            [
              { name: 'Participant Details', path: '/admin/crm/participantdetails/', pathstructure:'/admin/crm/participantdetails/:id',type:1 },
              { name: 'Participant Ability', path: '/admin/crm/participantability/', pathstructure:'/admin/crm/participantability/:id',type:1 },
              { name: 'Shift', path: '/admin/crm/shifts/', pathstructure:'/admin/crm/shifts/:id',type:1 },
              { name: 'Funding Details', path: '/admin/crm/fundingdetails/' , pathstructure:'/admin/crm/fundingdetails/:id',type:1},
            ],
            linkShow:false,
            id:'crm_participant_details',
            className:'active'
        },

        {
          name: 'Reporting', submenus:
            [
              { name: 'Onbording Analytics', path: '/admin/crm/reporting' },
              { name: 'Service Analytics', path: '#' },
              { name: 'Location Analytics', path: '/admin/crm/locationanalytics' },
            ],
            id:'crm_reports'
        },

        {
          name: 'Schedules', submenus:
            [
              { name: 'User Schedules', path: '/admin/crm/schedules' },
              { name: 'User Tasks', path: '/admin/crm/tasks' },
            ],
            id:'crm_schedules'
        },
        
        {
          name: 'User Management', submenus:
            [
              { name: 'Staff Members', path: '/admin/crm/usermangement',id:'staff_member',
                subSubMenu:
                        [
                            { name: 'Staff Details', path: '/admin/crm/StaffDetails/',pathstructure:'/admin/crm/StaffDetails/:staffId',type:1 ,'linkOnlyHide':true,id:'staff_member_id'},
                        ]
            },
              { name: 'Departments', path: '/admin/crm/departments' },
            ],
            id:'crm_user'
        }
      ];