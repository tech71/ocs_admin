import React, { Component } from 'react';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { TextBlock, MediaBlock, TextRow, RectShape, RoundShape } from 'react-placeholder/lib/placeholders';

export const customProfile = (
    <div className=''>
        <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1 mt-3">
            <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '62%', height: 20, 'borderRadius': 25, display: 'inline-block' }} />
        </div>
            <div className="col-lg-9 col-md-9 f py-1 mt-3"><RectShape defaultValue={''} ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '60%', 'borderRadius': '25px', display: 'inline-block' }} /></div></div>

        <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1">
            <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '32%', height: 20, 'borderRadius': 25, display: 'inline-block' }} /></div>
            <div className="col-lg-9 col-md-9 f py-1"><RectShape defaultValue={''} ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '40%', 'borderRadius': '25px', display: 'inline-block' }} /></div></div>

        <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1">
            <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '38%', height: 20, 'borderRadius': 25, display: 'inline-block' }} /></div>
            <div className="col-lg-9 col-md-9 f py-1"><RectShape defaultValue={''} ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '30%', 'borderRadius': '25px', display: 'inline-block' }} /></div></div>

        <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1">
            <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '42%', height: 20, 'borderRadius': 25, display: 'inline-block' }} /></div>
            <div className="col-lg-9 col-md-9 f py-1"><RectShape defaultValue={''} ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '35%', 'borderRadius': '25px', display: 'inline-block' }} /></div></div>

        <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1">
            <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '38%', height: 20, 'borderRadius': 25, display: 'inline-block' }} /></div>
            <div className="col-lg-9 col-md-9 f py-1"><RectShape defaultValue={''} ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '20%', 'borderRadius': '25px', display: 'inline-block' }} /></div></div>

        <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1">
            <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '35%', height: 20, 'borderRadius': 25, display: 'inline-block' }} /></div>
            <div className="col-lg-9 col-md-9 f py-1"><RectShape defaultValue={''} ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '40%', 'borderRadius': '25px', display: 'inline-block' }} /></div></div>

        <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1">
            <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '40%', height: 20, 'borderRadius': 25, display: 'inline-block' }} /></div>
            <div className="col-lg-9 col-md-9 f py-1"><RectShape defaultValue={''} ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '65%', 'borderRadius': '25px', display: 'inline-block' }} /></div></div>

        <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1">
            <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '38%', height: 20, 'borderRadius': 25, display: 'inline-block' }} /></div>
            <div className="col-lg-9 col-md-9 f py-1"><RectShape defaultValue={''} ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '60%', 'borderRadius': '25px', display: 'inline-block' }} /></div></div>

        <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1">
            <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '52%', height: 20, 'borderRadius': 25, display: 'inline-block' }} /></div>
            <div className="col-lg-9 col-md-9 f py-1"><RectShape defaultValue={''} ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '32%', 'borderRadius': '25px', display: 'inline-block' }} /></div></div>

        <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1">
            <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '46%', height: 20, 'borderRadius': 25, display: 'inline-block' }} /></div>
            <div className="col-lg-9 col-md-9 f py-1"><RectShape defaultValue={''} ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '60%', 'borderRadius': '25px', display: 'inline-block' }} /></div></div>

        <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1">
            <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '32%', height: 20, 'borderRadius': 25, display: 'inline-block' }} /></div>
            <div className="col-lg-9 col-md-9 f py-1"><RectShape defaultValue={''} ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '58%', 'borderRadius': '25px', display: 'inline-block' }} /></div></div>

        <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1">
            <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '50%', height: 20, 'borderRadius': 25, display: 'inline-block' }} /></div>
            <div className="col-lg-9 col-md-9 f py-1"><RectShape defaultValue={''} ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '32%', 'borderRadius': '25px', display: 'inline-block' }} /></div></div>
    </div>
);

export function custNumberLine(number) {
    var temp = []
    for (var i = 1; i < number; i++) {
        temp[i] = i;
    }
    return temp.map((name, id) => (<ReactPlaceholder ready={false} key={id + 1} showLoadingAnimation={true} style={{ "borderRadius": "25px" }} type='textRow' ><RectShape defaultValue={''} /></ReactPlaceholder>))
};

export const CustomProfileImage = (
    <table width="100%" className="user_profile">
        <tbody>
            <tr>
                <td className="width_130">
                    <div className="">
                        <RoundShape ready={false} showLoadingAnimation={true} type='round' ready={false} color='#E0E0E0' style={{ width: 100, height: 100 }}></RoundShape>
                    </div>
                </td>
                <td>
                    <ul>
                        <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '30%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
                        <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '40%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
                        <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '48%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
                        <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '42%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />

                    </ul>
                </td>
            </tr>
        </tbody>
    </table>
);

export function customHeading(width, align) {
    if (align == 'center') {
        return <h1 className="color"><RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ 'margin': '0px auto', width: width + '%', height: 20, 'borderRadius': 25, 'marginBottom': '5px',  'marginTop':'5px'  }} /></h1>
    } else {
        return <h1 className="color"><RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: width + '%', height: 20, 'borderRadius': 25, 'marginBottom': '5px', 'marginTop':'5px' }} /></h1>
    }
}


export function customNavigation() {
    return <aside className="col-lg-2 col-md-3">
        <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '100%', height: 50, 'borderRadius': 25, 'marginBottom': '5px' }} />
        <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '100%', height: 50, 'borderRadius': 25, 'marginBottom': '5px' }} />
        <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '100%', height: 50, 'borderRadius': 25, 'marginBottom': '5px' }} />
        <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '100%', height: 50, 'borderRadius': 25, 'marginBottom': '5px' }} />
        <RectShape defaultValue={''} showLoadingAnimation={true} color='#CDCDCD' style={{ width: '100%', height: 50, 'borderRadius': 25, 'marginBottom': '5px' }} />
    </aside>
};

export function customChatingLoader() {
    return <ul className="int_mail_1-D ul-1">
        <li className="bb-0"><div className=" user-received P_10_T">
            <div className="message-avatar">
                <RoundShape color='#fff' style={{ width: 40, height: 40 }} />
            </div>
            <RectShape defaultValue={''} showLoadingAnimation={true} color='#fff' style={{ width: '120px', marginTop: '15px', height: 20, 'borderRadius': 25, 'marginBottom': '5px', marginLeft: '7px' }} />
        </div>
            <div className="message P_10_T">
                <RectShape defaultValue={''} showLoadingAnimation={true} color='#fff' style={{ width: '30%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
                <div className="date_and_content">
                    <RectShape defaultValue={''} showLoadingAnimation={true} color='#fff' style={{ width: '70%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
                    <div className="timing"></div>
                </div>
            </div>
        </li>
        <RectShape defaultValue={''} showLoadingAnimation={true} color='#fff' style={{ width: '100%', height: 1, 'borderRadius': 25, 'marginBottom': '5px' }} />
        <li className="bb-0"><div className=" user-received P_10_T">
            <div className="message-avatar">
                <RoundShape color='#fff' style={{ width: 40, height: 40 }} />
            </div>
            <RectShape defaultValue={''} showLoadingAnimation={true} color='#fff' style={{ width: '120px', marginTop: '15px', height: 20, 'borderRadius': 25, 'marginBottom': '5px', marginLeft: '7px' }} />
        </div>
            <div className="message P_10_T">
                <RectShape defaultValue={''} showLoadingAnimation={true} color='#fff' style={{ width: '30%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
                <div className="date_and_content">
                    <RectShape defaultValue={''} showLoadingAnimation={true} color='#fff' style={{ width: '70%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
                    <div className="timing"></div>
                </div>
            </div>
        </li>
        <RectShape defaultValue={''} showLoadingAnimation={true} color='#fff' style={{ width: '100%', height: 1, 'borderRadius': 25, 'marginBottom': '5px' }} />
        <li className="bb-0"><div className=" user-received P_10_T">
            <div className="message-avatar">
                <RoundShape color='#fff' style={{ width: 40, height: 40 }} />
            </div>
            <RectShape defaultValue={''} showLoadingAnimation={true} color='#fff' style={{ width: '120px', marginTop: '15px', height: 20, 'borderRadius': 25, 'marginBottom': '5px', marginLeft: '7px' }} />
        </div>
            <div className="message P_10_T">
                <RectShape defaultValue={''} showLoadingAnimation={true} color='#fff' style={{ width: '30%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
                <div className="date_and_content">
                    <RectShape defaultValue={''} showLoadingAnimation={true} color='#fff' style={{ width: '70%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
                    <div className="timing"></div>
                </div>
            </div>
        </li>
    </ul>


}


export function customSingleChatingLoader() {
    return <div className="custom_scolling">
        <PlaceholderMy active="active" />
        <PlaceholderMy />
        <PlaceholderMy />
        <PlaceholderMy />
        <PlaceholderMy />

    </div>

}

const PlaceholderMy = (props) => (<div className={"mess_V1_1 " + props.active}>
        <div className="mess_v1">
            <div className="mess_vn_1">
                <div className="mess_vn_in_1">
                    <div className="mess_V_1"><RoundShape defaultValue={''} color='#777' style={{ width: 40, height: 40 }} /></div>
                    <div className="mess_V_2">
                        <div className="mess_V_a">
                            <div className="mess_V_a1"><RoundShape defaultValue={''} color='#ddd' style={{ width: '100%', height: '17px' }} /></div>
                            <div className="mess_V_a2"><RoundShape defaultValue={''}  color='#ddd' style={{ width: '80%', height: '13px' }} /></div>
                        </div>
                        <div className="mess_V_b"><RoundShape defaultValue={''} color='#ddd' style={{ width: 80, height: 10 }} /> <RoundShape color='#ddd' style={{ width: 40, height: 10 }} /></div>
                        <div className="mess_vn_2"></div>
                    </div>
                </div>
                <div className="mess_vn_in_2">
                    <p className="mb-0 mt-2"></p>
                    <div className="my-0 ML_show_fixed"><RoundShape defaultValue={''} color='#ddd' style={{ width: '100%', height: 10, marginBottom: '3px' }} /></div>
                    <div className="my-0 ML_show_fixed"><RoundShape defaultValue={''} color='#ddd' style={{ width: '80%', height: 10 }} /></div>
                </div>
            </div>
        </div>
    </div>)

export function groupChating() {
    return <div className="GroupMSG_team_v1">
        <div className="d-flex">
            <RoundShape color='#fff' style={{ width: '80%', height: '40px' }} />
        </div>
    </div>
}


export function groupChatingLoder() {
    return (
        <div>
            <div className="row pt-3">
                <div className="col-md-7">
                    <div className="Group_v1">
                        <div className="Group_v1_b">
                            <div className="mess_vn_in_1">
                                <div className="mess_V_1">
                                    <span style={{ border: '0px' }}><RoundShape color='#777' style={{ width: '100%', height: '100%' }} /></span>
                                </div>
                                <div className="mess_V_2">
                                    <div className="mess_V_a">
                                        <div className="mess_V_a1"><RoundShape color='#ddd' style={{ width: 170, height: 13, marginBottom: '3px' }} /></div>
                                    </div>
                                    <div className="mess_V_b"><RoundShape color='#ddd' style={{ width: 70, height: 10, marginBottom: '3px' }} /></div>
                                </div>
                            </div>
                            <div className="Group_v1_a Group_F_dow flex-wrap">
                                <RoundShape color='#ddd' style={{ width: '90%', height: 10, marginBottom: '3px' }} /><br />
                                <RoundShape color='#ddd' style={{ width: '80%', height: 10, marginBottom: '3px' }} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row pt-3">
                <div className="col-md-7">
                    <div className="Group_v1">
                        <div className="Group_v1_b">
                            <div className="mess_vn_in_1">
                                <div className="mess_V_1">
                                    <span style={{ border: '0px' }}><RoundShape color='#777' style={{ width: '100%', height: '100%' }} /></span>
                                </div>
                                <div className="mess_V_2">
                                    <div className="mess_V_a">
                                        <div className="mess_V_a1"><RoundShape color='#ddd' style={{ width: 170, height: 13, marginBottom: '3px' }} /></div>
                                    </div>
                                    <div className="mess_V_b"><RoundShape color='#ddd' style={{ width: 70, height: 10, marginBottom: '3px' }} /></div>
                                </div>
                            </div>
                            <div className="Group_v1_a Group_F_dow flex-wrap">
                                <RoundShape color='#ddd' style={{ width: '90%', height: 10, marginBottom: '3px' }} /><br />
                                <RoundShape color='#ddd' style={{ width: '80%', height: 10, marginBottom: '3px' }} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row pt-3 Group_replay">
                <div className="col-md-7 col-md-offset-5">
                    <div className="Group_v1">
                        <div className="Group_v1_b">
                            <div className="mess_vn_in_1">
                                <div className="mess_V_2">
                                    <div className="mess_V_b"><RoundShape color='#ddd' style={{ width: 70, height: 10, marginBottom: '3px' }} /></div>
                                    <div className="mess_V_a">
                                        <div className="mess_V_a1"><RoundShape color='#ddd' style={{ width: 170, height: 13, marginBottom: '3px' }} /></div>
                                    </div>
                                </div>
                                <div className="mess_V_1">
                                    <span style={{ border: '0px' }}><RoundShape color='#fff' style={{ width: '100%', height: '100%' }} /></span>
                                </div>
                            </div>
                            <div className="Group_v1_a Group_F_dow flex-wrap">
                                <RoundShape color='#ddd' style={{ width: '90%', height: 10, marginBottom: '3px' }} /><br />
                                <RoundShape color='#ddd' style={{ width: '80%', height: 10, marginBottom: '3px' }} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


export function imailDetailsLoder1() {

    return (
      
        <div className="mess_hadding_V1 px-4">
            <div className="row py-4">
                <div className="col-md-6 Imail_btn_left_v1"></div>
                <div className="col-md-6 Imail_btn_right_v1 text-right">
                <i className="icon " style={{background: 'none'}}><RoundShape color='#CBCBCA' style={{ width: '100%', height: '100%', marginBottom: '3px' }} /></i>
                <i className="icon " style={{background: 'none'}}><RoundShape color='#CBCBCA' style={{ width: '100%', height: '100%', marginBottom: '3px' }} /></i>
                <i className="icon " style={{background: 'none'}}><RoundShape color='#CBCBCA' style={{ width: '100%', height: '100%', marginBottom: '3px' }} /></i>
                <i className="icon " style={{background: 'none'}}><RoundShape color='#CBCBCA' style={{ width: '100%', height: '100%', marginBottom: '3px' }} /></i>
                <i className="icon " style={{background: 'none'}}><RoundShape color='#CBCBCA' style={{ width: '100%', height: '100%', marginBottom: '3px' }} /></i>
                </div>
            </div>
            <div className="col-md-12"></div>
        </div>
        

    )

}
export function imailDetailsLoder2() {

    return (
      
        <div className="mess_div_scroll" id="main_internal_details">
        <div className="mess_v1 pt-0">
            <div className="mess_vn_1">
            <RoundShape color='#ddd' style={{ width: '100%', height: 1}} />
                <div className="mess_vn_in_1 pt-3">
                    <div className="mess_V_1"><span style={{border: '0px solid #fff'}}><RoundShape color='#777' style={{ width: '100%', height: '100%', marginBottom: '3px' }} /></span></div>
                    <div className="mess_V_2">
                        <div className="mess_V_a">
                            <div className="mess_V_a1"><RoundShape color='#ddd' style={{ width: '150px', height: 15}} /></div>
                            <div className="mess_V_a2"><RoundShape color='#ddd' style={{ width: '100px', height: 15}} /></div>
                        </div>
                        <div className="mess_V_b"><RoundShape color='#ddd' style={{ width: '100px', height: 10}} /></div>
                        <div className="mess_vn_2">
                            <div className="Imail_btn_right_v1 text-right"></div>
                        </div>
                    </div>
                </div>
                <div className="mt-2 mb-3">
                <RoundShape color='#ddd' style={{ width: '100%', height: 1}} />
                </div>
                <div className="mess_vn_in_2">
                    <div className="mb-0 mt-2"></div>
                    <div className="my-0"><RoundShape color='#ddd' style={{ width: '100%', height: 10}} /></div>
                    <div className="my-2"><RoundShape color='#ddd' style={{ width: '90%', height: 10}} /></div>
                    <div className="mb-4 mt-0"><RoundShape color='#ddd' style={{ width: '50%', height: 10}} /></div>
                </div>
                <div className="my-5"></div>
                <div className="col-md-6 Imail_btn_left_v1">
                <i className="icon " style={{background: 'none'}}><RoundShape color='#CBCBCA' style={{ width: '100%', height: '100%', marginBottom: '3px' }} /></i>
                <i className="icon " style={{background: 'none'}}><RoundShape color='#CBCBCA' style={{ width: '100%', height: '100%', marginBottom: '3px' }} /></i>
                <i className="icon " style={{background: 'none'}}><RoundShape color='#CBCBCA' style={{ width: '100%', height: '100%', marginBottom: '3px' }} /></i>
                </div>
            </div>
        </div>
        
    </div>
       
    )

}