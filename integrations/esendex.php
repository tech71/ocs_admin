<?php

require_once __DIR__ . '/vendor/autoload.php';

const USERNAME = 'bfleming@oncall.com.au';
const PASSWORD = 'XVEKx4vzxJFn';

// so PHP_EOL is formatted as a newline
header('Content-Type: text/plain');

echo 'Esendex SMS API Integration' . PHP_EOL;

echo PHP_EOL;
echo '1. Authentication' . PHP_EOL;
$authentication = (new \Esendex\SessionService)->startSession(
    // this account reference will likely be hardcoded as per docs
    'EX000000',
    USERNAME,
    PASSWORD
);
var_dump($authentication);

echo PHP_EOL;
echo '2. Fetch Accounts' . PHP_EOL;
$accountService = new \Esendex\AccountService($authentication);
$accounts = $accountService->getAccounts();
if (empty($accounts)) {
    echo 'No accounts found.' . PHP_EOL;
}

foreach ($accounts as $key => $value) {
    echo '- ' . $value->reference() . PHP_EOL;
}

echo PHP_EOL;
echo '3. Send SMS' . PHP_EOL;
// please be mindful while running this part of the code - each SMS sent costs the client money
$dispatchService = new \Esendex\DispatchService($authentication);
var_dump($dispatchService->send(new \Esendex\Model\DispatchMessage(
    'ONCALL', // originator
    '', // recipient phone number
    'Hello World', // body
    \Esendex\Model\Message::SmsType
)));

echo PHP_EOL;
echo '4. Check SMS Status' . PHP_EOL;
$headerService = new \Esendex\MessageHeaderService($authentication);
$smsId = '9e862b38-9939-18ee-09ed-0f30aaa84001';
$message = $headerService->message($smsId);
echo 'The status for SMS ID ' . $smsId . ' is ' . $message->status() . PHP_EOL;
